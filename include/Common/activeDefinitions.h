//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// RBdefinitions.h:
//
//////////////////////////////////////////////////////////////////////

#pragma once

// Module name
#define INPUTGRETA_MODULE_NAME ".InputGreta"
#define INPUTWATSON_MODULE_NAME "InputWatson"
#define REALTIMEFML_MODULE_NAME "FML2BML"
#define REALTIMEBML_MODULE_NAME "BMLRealizer"
#define REALTIMEPLAYER_MODULE_NAME "player"

// Greta Message Header in the ACTIVEMQ module.
#define CLOCK_MESSAGE_HEADER ""
#define SYSTEM_FEEDBACK_MESSAGE_HEADER "Feedback"
#define AGENTSTATE_MESSAGE_HEADER "semaine.data.state.agent.intention"
#define UPDATEAGENTSTATE_MESSAGE_HEADER "semaine.data.state.agent.update"
#define COGNITIVE_FML_MESSAGE_HEADER ".Data.CognitiveFML"
#define REALTIME_PLAYER_COMMAND "semaine.data.synthesis.lowlevel.command"
#define DATA_WAV "semaine.data.synthesis.lowlevel.audio"
#define FAP_FRAME "semaine.data.synthesis.lowlevel.video.FAP"
#define BAP_FRAME "semaine.data.synthesis.lowlevel.video.BAP"
#define COMPLEX_INPUT ".Data.Complex.Input"
#define COMPLEX_OUTPUT ".Data.Complex.Output"
#define INPUT_AUDIO "semaine.data.analysis.audio"
#define INPUT_VIDEO "semaine.data.analysis.video"

#define ACTION_SELECTED_FML "semaine.data.action.selected.speechpreprocessed"
#define PLAN_BML "semaine.data.synthesis.plan"

// Sender for ActiveMQ
//#define FAP_BAP_SENDER "semaine.data.synthesis.lowlevel.video.FAP"
#define AUDIO_WAV_SENDER "semaine.data.synthesis.lowlevel.audio"
#define PLAYER_COMMAND_SENDER "semaine.data.synthesis.lowlevel"

// Header for the input messages on DataInput.Whiteboard
#define INPUT_MESSAGE_HEADER "Greta.Data.Input"


// Greta Whiteboard
#define GRETA_WHITEBOARD ".Whiteboard"
// Input Witeboard
#define INPUT_WHITEBOARD "DataInput.Whiteboard"

// Greta Message Header to send by ReactiveBehaviour BEFORE the implementation of the ActionSelection module
#define FML_MESSAGE_HEADER "semaine.data.action.selected.function"
#define BML_MESSAGE_HEADER "semaine.data.synthesis.plan.speechtimings.BML"

// Greta Message Header sent by the ReactiveBehaviour AFTER the implementation of the ActionSelection module
//#define FML_CANDIDATE_MESSAGE_HEADER "samaine.data.action.candidate.function"
//#define BML_CANDIDATE_MESSAGE_HEADER "samaine.data.action.candidate.behaviour"

#define REACTIVE_FML_MESSAGE_HEADER ".Data.ReactiveFML"
#define MIMICRY_BML_MESSAGE_HEADER ".Data.MimicryBML"

// Log messages
#define LOG_MESSAGE_CONNEXION_OK " registered to ActiveMQ"
#define LOG_MESSAGE_CONNEXION_FAILED " could not register to ActiveMQ"
