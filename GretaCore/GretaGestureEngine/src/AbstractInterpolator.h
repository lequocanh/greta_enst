//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// AbstractInterpolator.h: interface for the AbstractInterpolator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ABSTRACTINTERPOLATOR_H__522037D6_C1B8_4642_8BF1_19B98C765272__INCLUDED_)
#define AFX_ABSTRACTINTERPOLATOR_H__522037D6_C1B8_4642_8BF1_19B98C765272__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BAPFrame.h"	// Added by ClassView
#include "GestureEngineGlobals.h"

/**@#-*/
namespace GestureSpace {
/**@#+*/
	
	/** Enumeration of different possible spline types */
	enum SplineType {ALL_SPLINES,SHOULDER_SPLINE,ELBOW_SPLINE,WRIST_SPLINE,FINGER_SPLINE};
	
	/** Degree-of-freedom groups; correspond to functional separation of gesture into three parts */
	enum DOFGroup {ARM_DOFs, WRIST_DOFs, FINGER_DOFs};
	
	/** Abstract base class for interpolation functions that generate inbetween BAPFrames for arm animation */
	class AbstractInterpolator  
	{
	public:
		AbstractInterpolator(enum SideType s=r);
		virtual ~AbstractInterpolator();
		
		/** Given frame number n and DOF group t, retrieve interpolated angle values for all DOFs in t and write them to f 
		 @param f BAPFrame where interpolated angles are written to
		 @param n frame number 
		 @param t Degree of freedom group for which values should be calculated/loaded
		*/
		virtual void GetFrame(BAPFrame &f, int n, enum DOFGroup t) = 0;
		
		/** Given a vector of keyframes, initialize interpolation routines for a given DOF group */
		virtual void Init(BAPFrameVector v, enum DOFGroup st) = 0;
	
	protected:
		SideType side;
		bool output;
	};

	/**@#-*/
} // END namespace GestureSpace
/**@#+*/
#endif // !defined(AFX_ABSTRACTINTERPOLATOR_H__522037D6_C1B8_4642_8BF1_19B98C765272__INCLUDED_)
