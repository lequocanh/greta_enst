//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// NInterface.h: interface for managing the level of interest.
//
// Author: Etienne de Sevin etienne.de-sevin@telecom-paristech.fr
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Menu_Bar.H>
#include <String>


class NInterface : public Fl_Double_Window 
{
public:
	NInterface();
	virtual ~NInterface();
	void draw();
	void drawNI(float NIU, float MoyNI, const char* interPhase, const char* anticipNIU, float priority, std::string typechosen, const char* actchosen, float arousal, float valence, float potency);
	void drawLine(float x, float y, float x2, float y2); 
	//void scrollCallback(Fl_Widget* o, void *v);
	float listeNIHyst[501];
	float listeNI[501];
	float listeAr[501];
	float listeVal[501];
	float listePot[501];
	float listeMoy[501];
	const char* phase;
	const char* anticipation;
	const char* actionchosen;
	float actionpriority;
	float agentstate_buffer;
	float hysteresis;
	const char* interPhase;
	float NIU;

	//Fl_Button *buttonLeg[4];
	Fl_Slider *scrollInterest;
	Fl_Slider *scrollNumber;
	Fl_Slider *scrollNumber1;
	Fl_Slider *scrollType;
	Fl_Slider *scrollType1;
	Fl_Check_Button *buttonInterest;
	Fl_Check_Button *buttonNumber;
	Fl_Check_Button *buttonType;
	Fl_Check_Button *buttonAuto;
	Fl_Check_Button *buttonNUI;
	Fl_Check_Button *buttonEmotion;
	Fl_Check_Button *buttonNoM;
	Fl_Check_Button *buttonNoBC;
	Fl_Check_Button *buttonNoC;
	Fl_Check_Button *buttonRandom;
	Fl_Menu_Bar *menu;

	std::string legende[4];
	int fenX;
	int fenY;
	int margeYsup;
	float manualNIU;
	float oldManualNIU;
	float valueManualNIU;
	int activeManualNIU;
	int oldActiveManualNIU;
	int activeManualNumber;
	int oldActiveManualNumber;
	float manualNumber;
	float manualNumber1;
	float oldManualNumber;
	float oldManualNumber1;
	float valueManualNumber;
	float valueManualNumber1;
	float manualType;
	float oldManualType;
	float oldManualType1;
	float valueManualType;
	float valueManualType1;
	int activeManualType;
	int oldActiveManualType;
	float NUIM;
	float NUIMoy;
	int activeManualNoM;
	int activeManualAuto;
	int activeManualNUI;
	int activeManualEmotion;
	int activeManualNoBC;
	int activeManualNoC;
	int activeManualRandom;
	int oldActiveManualNoM;
	int oldActiveManualAuto;
	int oldActiveManualNUI;
	int oldActiveManualEmotion;
	int oldActiveManualNoBC;
	int oldActiveManualNoC;
	int oldActiveManualRandom;



};
