//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include ".\signal.h"
#include "MaryInterface.h"
#include "RandomGen.h"
#include <time.h>
#include "FileNames.h"

#include "AcapelaPlug.h"
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;


extern RandomGen *randomgen;
extern IniManager inimanager;
extern FileNames filenames;

/**@#-*/ 
using namespace MMSystemSpace;
/**@#+*/

TimeMarker::TimeMarker(void){concretized=false;}

TimeMarker::~TimeMarker(void){}

bool Signal::operator<(Signal& a)
{
	if(this->start<a.start)
		return true;
	return false;
}




Signal::Signal(void)
{
	start=0;
	concretized=false;
	noend=false;
	//idle=false;
	intensity=1;
	originalBMLSpeechNode = NULL;
}

Signal::Signal(const Signal &rhs)
{
	this->modality=rhs.modality;
	this->type=rhs.type;
	this->id=rhs.id;
	this->start_sym=rhs.start_sym;
	this->duration_sym=rhs.duration_sym;
	this->start=rhs.start;
	this->duration=rhs.duration;
	this->reference=rhs.reference;
	this->direction=rhs.direction;
	this->posture=rhs.posture;
	this->shape=rhs.shape;
	this->language=rhs.language;
	this->voice=rhs.voice;
	this->speed_num=rhs.speed_num;
	this->speed_sym=rhs.speed_sym;
	this->concretized=rhs.concretized;
	this->noend=rhs.noend;
	this->BMLcode=rhs.BMLcode;
	this->start=rhs.start;
	this->duration=rhs.duration;
	this->usingMary=rhs.usingMary;
	//this->excludelist=rhs.excludelist;

	this->content=rhs.content;
	this->meaning=rhs.meaning;
	this->intonation=rhs.intonation;
	this->voicequality=rhs.voicequality;
	
	//using the flag "noend" instead
	//this->idle=rhs.idle;
	
	//is it ok??
	this->intensity=rhs.intensity;
		
	if(rhs.strokes.empty()==false)
	{
		std::vector<float>::const_iterator iter;
		for(iter=rhs.strokes.begin();iter!=rhs.strokes.end();iter++)
		{
			float f=(*iter);
			this->strokes.push_back(f);
		}
	}

	if(rhs.parameters.empty()==false)
	{
		EngineParameters::const_iterator iter;
		for(iter=rhs.parameters.begin();iter!=rhs.parameters.end();iter++)
		{
			this->parameters[(*iter).second.name]=(*iter).second;
		}
	}

	if(rhs.timemarkers.empty()==false)
	{
		std::map<std::string,TimeMarker>::const_iterator iter;
		for(iter=rhs.timemarkers.begin();iter!=rhs.timemarkers.end();iter++)
		{
			this->timemarkers[(*iter).first]=(*iter).second;
		}
	}
	if(rhs.alternativeshapes.empty()==false)
	{
		std::vector<alternativeshape>::const_iterator iter;
		for(iter=rhs.alternativeshapes.begin();iter!=rhs.alternativeshapes.end();iter++)
		{
			alternativeshape *as;
			as=new alternativeshape();
			as->name=(*iter).name;
			as->content=(*iter).content;
			as->meaning=(*iter).meaning;
			as->intonation=(*iter).intonation;
			as->voicequality=(*iter).voicequality;
			as->probability=(*iter).probability;
			this->alternativeshapes.push_back(*as);
		}
	}

	if(rhs.stressingpoints.empty()==false)
	{
		std::vector<float>::const_iterator iter;
		for(iter=rhs.stressingpoints.begin();iter!=rhs.stressingpoints.end();iter++)
		{
			this->stressingpoints.push_back(*iter);
		}
	}

	this->originalBMLSpeechNode = rhs.originalBMLSpeechNode;
}

Signal::~Signal(void)
{
	if(this->parameters.empty()==false)
		this->parameters.clear();
}

float Signal::GetParamValuef(std::string name)
{
	if(this->parameters.find(name)==this->parameters.end())
	{
		printf("Signal::Warning: unknown parameter %s\n",name.c_str());
		return 0;
	}
	return this->parameters[name].GetValue();
}

void Signal::SetParameterf(std::string name, float value)
{
	if(this->parameters.find(name)==this->parameters.end())
	{
		EngineParameter p;
		p.name=name;
		p.SetValue(value);
		this->parameters[name]=p;
	}
	else
		this->parameters[name].SetValue(value);
}

void Signal::SetParameter(std::string name, std::string value)
{
	if(this->parameters.find(name)==this->parameters.end())
	{
		EngineParameter p;
		p.name=name;
		p.SetValue(atof(value.c_str()));
		this->parameters[name]=p;
	}
	else
		this->parameters[name].SetValue(atof(value.c_str()));
}

int Signal::StoreBML(XMLGenericTree *t,IniManager inimanager)
{
	XMLGenericTree *d;

	if(t==0)
		return 0;

	type=t->GetAttribute("type");
	modality=t->GetName();
	
	if(modality=="speech")
	{
		BMLcode=t->ToString();
		this->originalBMLSpeechNode = t;
		// Try to see if we can also store the DOM element
		//XMLDOMTree * domT = dynamic_cast<XMLDOMTree *>(t);
		//if (domT != NULL)
		//{
		//	this->originalBMLSpeechNode = domT->getDOMNode();
		//}
	}

	//parse file
	if(modality=="file") {
		reference=t->GetAttribute("name");
	}

	id=t->GetAttribute("id");
	
	direction=t->GetAttribute("direction");
	posture=t->GetAttribute("posture");
	shape=t->GetAttribute("shape");

	voice=t->GetAttribute("voice");
	
	language=t->GetAttribute("language");
	speed_num=t->GetAttributef("speed_num");
	speed_sym=t->GetAttribute("speed_sym");
	text=t->GetAttribute("text");
	start_sym=t->GetAttribute("start");
	start=t->GetAttributef("start");
	duration_sym=t->GetAttribute("end");
	duration=t->GetAttributef("end");

	
	if(modality=="voice") {
		if(t->HasAttribute("content"))
			content=t->GetAttributef("content");
		else content="";

		if(t->HasAttribute("meaning"))
			meaning=t->GetAttributef("meaning");
		else meaning="";

		if(t->HasAttribute("intonation"))
			intonation=t->GetAttributef("intonation");
		else intonation="";

		if(t->HasAttribute("voicequality"))
			voicequality=t->GetAttributef("voicequality");
		else voicequality="";
	}

	if(t->GetAttributef("stroke")!=0)
	{
		strokes.push_back(t->GetAttributef("stroke"));
	}

	SetParameterf("SPC.value",inimanager.GetValueFloat("ENGINE_EXPR_SPC"));
	SetParameterf("TMP.value",inimanager.GetValueFloat("ENGINE_EXPR_TMP"));
	SetParameterf("PWR.value",inimanager.GetValueFloat("ENGINE_EXPR_PWR"));
	SetParameterf("FLD.value",inimanager.GetValueFloat("ENGINE_EXPR_FLD"));
	SetParameterf("REP.value",inimanager.GetValueFloat("ENGINE_EXPR_REP"));
	
	d=t->FindNodeCalled("description");
	
	if(d!=0)
	{
		if((d->GetAttribute("level")=="1")&&(d->GetAttribute("type")=="gretabml"))
		{


			for(XMLGenericTree::iterator it = d->begin(); it != d->end(); ++it)
			{
				XMLGenericTree *iter = *it;
				if (iter->isTextNode()) continue;
	
				if(iter->GetName()=="stroke")
				{
					strokes.push_back(iter->GetAttributef("time"));
					continue;
				}
				if(iter->GetName()=="reference")
				{
					reference=iter->FindNodeCalled("text")->GetTextValue();
					continue;
				}
				if(iter->GetName()=="intensity")
				{
					intensity= atof ((iter->FindNodeCalled("text")->GetTextValue()).c_str());
					continue;
				}

				SetParameter(iter->GetName(),iter->FindNodeCalled("text")->GetTextValue());
			}
		}
	}

	if(modality=="speech")
	{
		StoreTimeMarkers(t);
	}

	return 1;
}


void Signal::AddSubElement(XMLGenericTree * element, int *uid)
{
	if (modality=="speech") {
		if (this->originalBMLSpeechNode != NULL)
		{
			//salva il nodo originale
			XMLGenericTree * speechNode = element->AddChild(originalBMLSpeechNode);
			return;
		} 
		else 
		{
			if(BMLcode!="")
			{
				element->AddText(BMLcode);
				return;
			}
		}
	}

	XMLGenericTree * modElement = element->CreateChild(modality);

	char v[30];
	if (uid != 0)
	{
		sprintf_s(v,30,"%d",*uid);
		(*uid)++;
		modElement->SetAttribute("id", this->id+"-"+v);
	}
	else
		modElement->SetAttribute("id", this->id);

	if (type != "")
		modElement->SetAttribute("type", type);
	
	
	if(modality=="speech")
	{ 
		//Elisabetta: adding speech tag when a backchannel generates an audio signal
		MaryInterface mi;
		//modElement->SetAttribute("xmlns:mary","http://mary.dfki.de/2002/MaryXML/");

		modElement->SetAttribute("type", "application/wav");
		if(this->content!="")
			modElement->SetAttribute("text", this->content);
		modElement->SetAttribute("language", mi.GetLanguage());
		modElement->SetAttribute("voice", mi.GetVoice());
		

		XMLGenericTree * vocalizationElt = modElement->CreateChild("vocalization",  "http://mary.dfki.de/2002/MaryXML");
		if(this->content!="")
			vocalizationElt->SetAttribute("name", this->content);
		if(this->meaning!="")
			vocalizationElt->SetAttribute("meaning", this->meaning);
		if(this->intonation!="")
			vocalizationElt->SetAttribute("intonation", this->intonation);
		if(this->voicequality!="")
			vocalizationElt->SetAttribute("voicequality", this->voicequality);

		XMLGenericTree * desElt = modElement->CreateChild("description");
		desElt->SetAttribute("level", "1");
		desElt->SetAttribute("type", "gretabml");

		modElement->AddText(this->content);

		return;
	}

		std::string ms = "non verbal behaviour " + reference + " on " + modality  + " modality";
		GretaLogger* l = new GretaLogger("(Core_MMSystem)", ms, "info"); 
		listLog.push_back(l);

	//add because bml is not always validate (Etienne)
	if(modality=="head")
	{
		if(type == "")
			modElement->SetAttribute("type", "NOD");
		if(direction == "")
			modElement->SetAttribute("direction", "RIGHT");
	}

	if(modality=="face")
	{
		if(type== "")
			modElement->SetAttribute("type", "MOUTH");
		if(shape== "")
			modElement->SetAttribute("shape", "flat");
	}

	if(modality=="gesture")
	{
		if(type== "")
			modElement->SetAttribute("type", "beat");
	}
	// end modif 

	sprintf_s(v,30,"%.2f",this->start);
	modElement->SetAttribute("start", v);

	sprintf_s(v,30,"%.2f",this->duration);
	modElement->SetAttribute("end", v); // OUCH! end is not duration!

	if(!strokes.empty())
	{
		sprintf_s(v,30,"%.2f",strokes[0]);
		modElement->SetAttribute("stroke", v);
	}

	if(modality=="torso")
		modElement->SetAttribute("posture", this->posture);

	XMLGenericTree * descriptionElt = modElement->CreateChild("description");
	descriptionElt->SetAttribute("level", "1");
	descriptionElt->SetAttribute("type", "gretabml");

	XMLGenericTree * refElt = descriptionElt->CreateChild("reference");
	refElt->AddText(reference);

	XMLGenericTree * intensityElt = descriptionElt->CreateChild("intensity");
	sprintf_s(v,30,"%.2f",this->intensity);
	intensityElt->AddText(v);

	if(strokes.size()>1)
			
	//CHEATING: comment this for if you have problems with multiple strokes

	{
		for(int i=1;i<(int)(strokes.size());i++)
		{
			XMLGenericTree * strokeElt = descriptionElt->CreateChild("stroke");
			sprintf_s(v,30,"%.3f",strokes[i]);
			strokeElt->SetAttribute("time", v);
		}
		
	}

	EngineParameters::iterator param;

	for(param=parameters.begin();param!=parameters.end();param++)
	{
		XMLGenericTree * paramElt = descriptionElt->CreateChild((*param).second.name);
		sprintf_s(v,30,"%.2f",GetParamValuef((*param).second.name));
		paramElt->AddText(v);
	}
}

std::string Signal::GetBML(int *uid)
{
	if(modality=="speech")
		return BMLcode;

	std::string s;
	char v[30];

	s="\t<"+modality;

	if(uid!=0)
	{
		sprintf_s(v,30,"%d",*uid);

		(*uid)++;

		s=s+" id=\""+this->id+"-"+v+"\" ";
	}
	else
		s=s+" id=\""+this->id+"\" ";

	if(type!="")
		s=s+" type=\""+type+"\"";

	sprintf_s(v,30,"%.2f",this->start);
	s=s+" start=\""+v+"\"";

	sprintf_s(v,30,"%.2f",this->duration);
	s=s+" end=\""+v+"\"";
	
	if(strokes.empty()==false)
	{
		sprintf_s(v,30,"%.2f",strokes[0]);
		s=s+" stroke=\""+v+"\"";
	}

	if(modality=="torso")
		s=s+" posture=\""+this->posture+"\"";

	s=s+">\n";

	s=s+"\t\t<description level=\"1\" type=\"gretabml\">\n";
	s=s+"\t\t<reference>"+reference+"</reference>\n";

	sprintf_s(v,30,"%.2f",this->intensity);
	s=s+"\t\t<intensity>"+v+"</intensity>\n";

	if(strokes.size()>1)
			
	//CHEATING: comment this for if you have problems with multiple strokes

	{
		for(int i=1;i<(int)(strokes.size());i++)
		{
			sprintf_s(v,30,"%.3f",strokes[i]);
			s=s+"\t\t<stroke time=\""+v+"\"/>\n";
		}
		
	}

	EngineParameters::iterator param;

	for(param=parameters.begin();param!=parameters.end();param++)
	{
		sprintf_s(v,30,"%.2f",GetParamValuef((*param).second.name));
		s=s+"\t\t\t<"+(*param).second.name+">"+v+"</"+(*param).second.name+">\n";
	}

	s=s+"\t\t</description>\n";
	s=s+"\t</"+modality+">\n";

	return s;
}

float Signal::GetTimeMarker(std::string name)
{
	if((timemarkers.find(name)!=timemarkers.end())&&(timemarkers[name].concretized==true))
		return timemarkers[name].time;
	else
		return -1;
}

void Signal::StoreTimeMarkers(XMLGenericTree *t)
{
	if(t==0)
		return;

	timemarkers.clear();
	
	for(XMLGenericTree::iterator it = t->begin();
		it != t->end();
		++it)
	{
		XMLGenericTree *iterch = *it;
		if (iterch->isTextNode()) continue;

		std::string name = iterch->GetName();
		if(name=="mark" || name=="tm")
		{
			TimeMarker tm;

			if(name=="mark") { // expect things like <ssml:mark name="s1:tm17"/>
				std::string refAndId = iterch->GetAttribute("name");
				int colon = refAndId.find(":");
				if (colon == std::string::npos) { // no colon, strange
					tm.id = refAndId;
				} else {
					std::string ref = refAndId.substr(0, colon);
					// TODO: what to do with ref? Ignoring for now
					tm.id = refAndId.substr(colon+1);
				}
			} else {
				tm.id=iterch->GetAttribute("id");
			}

			if(iterch->HasAttribute("time"))
			{
				tm.time=iterch->GetAttributef("time");
				tm.concretized=true;
			}
			timemarkers[tm.id]=tm;
		}
	}
}

void Signal::StoreTimeMarkersTimings(std::list<TimeMarker> *tml)
{
#ifdef USE_ACAPELA
	std::string tmID;
	std::list<TimeMarker>::iterator iter;
	for(iter=(*tml).begin();iter!=(*tml).end();iter++)
	{
		//if(timemarkers.find((*iter).id)!=timemarkers.end())
		tmID=timeMarkerID[(*iter).id];
		if(timemarkers.find(tmID)!=timemarkers.end())
		{
			if(timemarkers[tmID].concretized==false)
			{
				timemarkers[tmID].time=(*iter).time;
				timemarkers[tmID].concretized=true;
			}
		}
	}
#else
	std::list<TimeMarker>::iterator iter;
	for(iter=(*tml).begin();iter!=(*tml).end();iter++)
	{
		if(timemarkers.find((*iter).id)!=timemarkers.end())
		{
			if(timemarkers[(*iter).id].concretized==false)
			{
				//printf("%s=%.2f  ",(*iter).id.c_str(),(*iter).time);
				timemarkers[(*iter).id].time=(*iter).time;
				timemarkers[(*iter).id].concretized=true;
			}
		}
	}
#endif
}


std::string Signal::ToMaryXML()
{
	MaryInterface mi;
	std::string language = mi.GetLanguage();
	std::string s;
	s="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
	s=s+"<maryxml version=\"0.4\"\n";
	s=s+"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n";
	s=s+"xmlns=\"http://mary.dfki.de/2002/MaryXML\"\n";
	if(language=="") //pour mary3.6
		s=s+"xml:lang=\"en\">\n\n";
	else //pour mary4.0
		s=s+"xml:lang=\""+language+"\">\n\n";
		
	if(this->modality!="speech")
		return "";

	timeMarkerID.clear();

	XMLGenericTree *speechtag;
	//XMLSaxParser
	XMLDOMParser parser;

	parser.SetValidating(false);
	speechtag=parser.ParseBuffer((char*)this->BMLcode.c_str());

	if(speechtag==0)
		return "";

	//std::cout << "After parsing, speech tag looks like this:" << std::endl << speechtag->ToString() << std::endl;

	for(XMLGenericTree::iterator it = speechtag->begin();
		it != speechtag->end();
		++it)
	{
		XMLGenericTree *iter = *it;
		if (iter->isTextNode()) continue;

		if(iter->GetName()=="boundary")
		{
			prosodystructure ps;
			std::string bs=iter->GetAttribute("start");
			ps.start=bs.substr(bs.find_first_of(":")+1);
			std::string be=iter->GetAttribute("end");
			ps.end=be.substr(be.find_first_of(":")+1);

			if(iter->GetAttribute("type")=="LH")
				ps.type="L-H%";
			else
			if(iter->GetAttribute("type")=="L")
				ps.type="L-";
			else
			if(iter->GetAttribute("type")=="H")
				ps.type="H-";
			else
			if(iter->GetAttribute("type")=="LL")
				ps.type="L-%";
			else
			if(iter->GetAttribute("type")=="HH")
				ps.type="H-^H%";
			else
			if(iter->GetAttribute("type")=="HL")
				ps.type="H-%";
			else
				ps.type="H-^H%";

			boundaries.push_back(ps);
		}
		if(iter->GetName()=="pitchaccent")
		{
			prosodystructure ps;
			std::string bs=iter->GetAttribute("start");
			ps.start=bs.substr(bs.find_first_of(":")+1);
			std::string be=iter->GetAttribute("end");
			ps.end=be.substr(be.find_first_of(":")+1);

			if(iter->GetAttribute("type")=="Hstar")
				ps.type="H*";
			else
			if(iter->GetAttribute("type")=="Lstar")
				ps.type="L*";
			else
			if(iter->GetAttribute("type")=="LplusHstar")
				ps.type="L+H*";
			else
			if(iter->GetAttribute("type")=="LstarplusH")
				ps.type="L*+H";
			else
			if(iter->GetAttribute("type")=="HstarplusL")
				ps.type="H*+L";
			else
			if(iter->GetAttribute("type")=="HplusLstar")
				ps.type="H+L*";
			else
				ps.type="H*";

			pitchaccents.push_back(ps);
		}
	}

	for(XMLGenericTree::iterator it = speechtag->begin();
		it != speechtag->end();
		++it)
	{
		XMLGenericTree *iterspeech = *it;
		//if (iterspeech->isTextNode()) continue;

		std::string elementName = iterspeech->GetName();
		if(elementName=="mark" || elementName=="tm")
		{
			std::string idAttributeName = (elementName == "mark" ? "name" : "id");
			std::list<prosodystructure>::iterator bi;

			for(bi=pitchaccents.begin();bi!=pitchaccents.end();bi++)
			{
				if((*bi).end==iterspeech->GetAttribute(idAttributeName))
				{
					s=s+"</t>\n";
					break;
				}
			}
			
			s=s+"\t<mark name='"+iterspeech->GetAttribute(idAttributeName)+"'/>\n";
			timeMarkerID[iterspeech->GetAttribute(idAttributeName)]=iterspeech->GetAttribute(idAttributeName);

			for(bi=boundaries.begin();bi!=boundaries.end();bi++)
				if((*bi).start==iterspeech->GetAttribute(idAttributeName))
				{
					float durationmillisecs=atof((*bi).end.c_str())*1000;
					char dmc[10];
					sprintf_s(dmc,10,"%.0f",durationmillisecs);
					s=s+"<boundary tone='"+(*bi).type+"' duration='"+dmc+"'/>\n";
					break;
				}

			for(bi=pitchaccents.begin();bi!=pitchaccents.end();bi++)
				if((*bi).start==iterspeech->GetAttribute(idAttributeName))
				{
					s=s+"<t accent='"+(*bi).type+"'>\n";
					break;
				}
		}
		if(elementName=="text")
			s=s+"\t"+iterspeech->GetTextValue()+"\n";
	}
	s=s+"</maryxml>";
	//std::cout << s << std::endl;
	return s;
}

std::string Signal::ToAcapelaSTRING()
{
	std::string s;
	int tm=-1;
	char tmID[20];

	if(this->modality!="speech")
		return "";

	XMLGenericTree *speechtag;
	XMLDOMParser parser;

	parser.SetValidating(false);
	speechtag=parser.ParseBuffer((char*)this->BMLcode.c_str());

	if(speechtag==0)
		return "";

	timeMarkerID.clear();


	//for boundary and pitch accent added 29/03/2010

	for(XMLGenericTree::iterator it = speechtag->begin();
		it != speechtag->end();
		++it)
	{
		XMLGenericTree *iter = *it;
		if (iter->isTextNode()) continue;

		if(iter->GetName()=="boundary")
		{
			prosodystructure ps;
			std::string bs=iter->GetAttribute("start");
			ps.start=bs.substr(bs.find_first_of(":")+1);
			std::string be=iter->GetAttribute("end");
			ps.end=be.substr(be.find_first_of(":")+1);

			if(iter->GetAttribute("type")=="LH")
				ps.type="L-H%";
			else
			if(iter->GetAttribute("type")=="L")
				ps.type="L-";
			else
			if(iter->GetAttribute("type")=="H")
				ps.type="H-";
			else
			if(iter->GetAttribute("type")=="LL")
				ps.type="L-%";
			else
			if(iter->GetAttribute("type")=="HH")
				ps.type="H-^H%";
			else
			if(iter->GetAttribute("type")=="HL")
				ps.type="H-%";
			else
				ps.type="H-^H%";

			boundaries.push_back(ps);
		}
		if(iter->GetName()=="pitchaccent")
		{
			prosodystructure ps;
			std::string bs=iter->GetAttribute("start");
			ps.start=bs.substr(bs.find_first_of(":")+1);
			std::string be=iter->GetAttribute("end");
			ps.end=be.substr(be.find_first_of(":")+1);

			if(iter->GetAttribute("type")=="Hstar")
				ps.type="H*";
			else
			if(iter->GetAttribute("type")=="Lstar")
				ps.type="L*";
			else
			if(iter->GetAttribute("type")=="LplusHstar")
				ps.type="L+H*";
			else
			if(iter->GetAttribute("type")=="LstarplusH")
				ps.type="L*+H";
			else
			if(iter->GetAttribute("type")=="HstarplusL")
				ps.type="H*+L";
			else
			if(iter->GetAttribute("type")=="HplusLstar")
				ps.type="H+L*";
			else
				ps.type="H*";

			pitchaccents.push_back(ps);
		}
	}
	//////

	for(XMLGenericTree::iterator it = speechtag->begin(); it != speechtag->end(); ++it)
	{
		XMLGenericTree *iterspeech = *it;

		std::string elementName = iterspeech->GetName();
		if(elementName=="mark" || elementName=="tm")
		{
			tm+=1;
			sprintf(tmID, "%d", tm);
			std::string idAttributeName = (elementName == "mark" ? "name" : "id");
			timeMarkerID[(std::string)tmID]=iterspeech->GetAttribute(idAttributeName);
			
			s=s+" \\mrk="+tmID+"\\ ";
		}
		if(elementName=="text")
			s=s+" "+iterspeech->GetTextValue()+" ";
	}
	int pos = s.find_first_of("\n");
	while(pos != std::string::npos){
		s = s.replace(pos, 1, "");
		pos = s.find_first_of("\n");
	}
	std::cout <<"sapi : "<< s << std::endl;
	return s;
}

std::string Signal::ToAcapela(std::string text, std::string filename)
{
	std::string result="";
	if(text!="")
	{
		result = ToAcapelaSTRING();
		//"\\mrk=0\\ce text \\mrk=1\\ doit �tre prononc�. \\mrk=2\\est-ce que vous l'entendez\\mrk=3\\? ";
	}
	else
	{
		std::string line;
		std::ifstream myfile(filename.c_str());
		if (myfile.is_open())
		{
			while (!myfile.eof())
			{
				std::getline(myfile,line);
				result+= line + " ";
			}
			myfile.close();
		}
		else std::cout << "Unable to open file"; 
	}
	return(result);
}


int Signal::TemporizeTimeMarkers(float time)
{
#ifdef USE_ACAPELA
		if(this->modality!="speech")
		return 0;

	int i;
	std::list<TimeMarker> tml;

	AcapelaPlug *plug;

	//Create the Acapela plug with the absolute path of a destination audio file (we can not do otherwise)
	std::string exitfile=filenames.Base_File+".wav";
	if(exitfile!="")
		plug=new AcapelaPlug(inimanager.Program_Path+"output/"+exitfile);
	else
		plug=new AcapelaPlug("C:/help.wav");  // maybe it shoud have a different name, shoudn't it? :) 

	//choose a voice
	std::string voice = inimanager.GetValueString("ACAPELA_LANGUAGE");
	if( ! plug->setVoice(voice)) //try one voice
		plug->setVoice("english", "female");//if not found we search a corresponding voice
	
	
	this->usingMary=1;
	updateUsingMary();

	std::string textInSAPI;
	if(this->usingMary==1)
		textInSAPI = this->ToAcapela(this->BMLcode,"");//"\\mrk=0\\ce text \\mrk=1\\ doit �tre prononc�. \\mrk=2\\est-ce que vous l'entendez\\mrk=3\\? ";
	if(this->usingMary==3)				
		std::string temp =  this->ToAcapela("",this->reference);

	//send the text (in SAPI format)
	plug->launch(textInSAPI);

	
	//E QUESTO?!?
	//get the audio buffer
	audioBufferAcapela = 0;
	sizeaudioBufferAcapela = 0;
	plug->getAudio(&audioBufferAcapela, &sizeaudioBufferAcapela);

	

	std::string phonemesfilename;

	// CASE 1: usually
	if(this->usingMary==1)
	{
		if(this->reference!="")
		{
			phonemesfilename=this->reference;
			filenames.Phonemes_File=this->reference;			
		}
		else
		{
			phonemesfilename="tmp/from-fml-apml.pho";
			filenames.Phonemes_File="tmp/from-fml-apml.pho";
		}
	}

	if(this->usingMary==3)
	{
		if(this->reference!="")
		{
			phonemesfilename = this->reference;
			phonemesfilename =  phonemesfilename.replace(phonemesfilename.length()-4,4,".pho");
			filenames.Phonemes_File = phonemesfilename;

		}
		else
			phonemesfilename="tmp/mary-out.pho";
	}

	/////////////////////////////////////////////////////////
	//generate PHO file

	//if we do not have pho file we generate it.
	//in a case of usingMary==2 we have pho file
	//in a case of usingMary==4 we do not use info about phonems


	if ((this->usingMary!=2) && (this->usingMary!=4))
	{
		//get the viseme list (ExtractAllPhonemes for Mary)
		//printf("visemes :\n");
		std::vector<ACAPELA_VISEME> visemes = plug->getVisemeList();

		for(i=1; i<visemes.size();++i)
		{
			if(visemes[i].viseme==visemes[i-1].viseme)
			{
				visemes[i-1].viseme="000";
				visemes[i].duration+=visemes[i-1].duration;
			}
		}


		if(visemes.size()>0)
		{
			FILE *phonemesfile;
			phonemesfile=fopen(phonemesfilename.c_str(),"w");
			if(phonemesfile!=NULL)
			{
				for(i=0; i<visemes.size();++i)
					if(visemes[i].viseme!="000")
						fprintf(phonemesfile, "%s %f\n", visemes[i].viseme, visemes[i].duration/1000.0); //be carefull : somme voice bugs with time markers (we have not any viseme after the first time marker)
			}
			fclose(phonemesfile);
		}
	}

	//get the time markers (for Mary this code was in ExtractAllPhonemes function)
	//printf("time markers :\n");
	std::vector<ACAPELA_TM> tm = plug->getTimeMarkerList();
	std::vector<ACAPELA_TM>::iterator itertm;

	//for(itertm=tm.begin(); itertm!=tm.end(); itertm++)
	for(i=0; i<tm.size(); i++)
	{
		TimeMarker tmrk;
		char tmID[20];
		sprintf(tmID, "%d", tm[i].num);
		tmrk.id = (std::string) tmID;
		tmrk.time=((float)tm[i].when)/1000.0;
		tmrk.concretized=true;
		tml.push_back(tmrk); //I don't know why the first marker was added many times

		std::cout <<"time marker:"<< tm[i].num <<":"<<tmrk.time<<std::endl;
	}
	this->StoreTimeMarkersTimings(&tml);


	// how to use usingmary variable
		//
		// voice=openmary  
		//	a) as usually  -> usingmary = 1		
		//	b) if there are phonems in bml -> usingmary = 0		
		//
		//voice=realspeech
		//	a)  .pho (audiolab) and .wav -> usingmary = 2	
		//	b)  .xml (openmary out) and .wav -> usingmary = 3

		//MARY_NATURAL_SPEECH=1 is absolute = usingmary = 3


	return 1;

#else

	if(this->modality!="speech")
		return 0;

	std::list<TimeMarker> tml;

	MaryInterface mi;

	std::string maryxml;
	maryxml=this->ToMaryXML();
	
	//XMLSaxParser
	XMLDOMParser parsemary;
	XMLGenericTree *treemary;
	parsemary.SetValidating(false);
	
	//important - after parsing
	if(inimanager.GetValueString("TEXT_TO_SPEECH")=="openmary") this->usingMary=1;
	if(inimanager.GetValueString("TEXT_TO_SPEECH")=="activemary") this->usingMary=0;
//	if(this->voice=="openmary") this->usingMary=1;
//	if(this->voice=="activemary") this->usingMary=0;
	updateUsingMary();
	

	// how to use usingmary variable
		//
		// voice=openmary  
		//	a) as usually  -> usingmary = 1		
		//	b) if there are phonems in bml -> usingmary = 0		
		//
		//voice=realspeech
		//	a)  .pho (audiolab) and .wav -> usingmary = 2	
		//	b)  .xml (openmary out) and .wav -> usingmary = 3

		//MARY_NATURAL_SPEECH=1 is absolute = usingmary = 3



	//case 0 : semaine project
	if(this->usingMary==0)
	{
		mi.SetUsingMary(0);

		treemary=parsemary.ParseBuffer((char*)this->BMLcode.c_str());

		if(treemary==0)
		{
			printf("Signal:: Error parsing Mary output in TemporizeTimeMarkers\n");
			return 0;
		}

		/* Marc Schroeder, 30.07.2009: The following code sets the duration of the first boundary to 0,
		 * which causes AV asynchronicity if there is a boundary in the middle such as a comma.
		 * Commenting this out for now.
		 */
		/*
		XMLGenericTree *treapp;
		char duration[256];
		XMLDOMTree * domMary = dynamic_cast<XMLDOMTree *>(treemary);
		if (domMary != NULL) { // namespace-aware
			std::string MaryXMLNamespaceURI = "http://mary.dfki.de/2002/MaryXML";
			treapp=domMary->FindNodeCalled("boundary", MaryXMLNamespaceURI);
		} else { // TODO: remove legacy code
			treapp=treemary->FindNodeCalled("boundary");
		}
		if(treapp!=NULL)
		{
			sprintf(duration, "%d", (int)(treapp->GetAttributef("end")*1000));
			treapp->SetAttribute("duration",(std::string)duration);
			//treapp->ModifyAttributeName("end","duration");
		}
		*/
		if(treemary==0)
		{
			printf("Signal:: Error parsing Mary output\n");
			return 0;
		}
	}

	//case 1 : default
	if(this->usingMary==1)
	{
		mi.SetUsingMary(1);

		// Generation of the xml Command for Mary

		//if you use preregistered file
		//if(inimanager.GetValueInt("MARY_NATURAL_SPEECH")!=1)
		//{
		
		int kup = this->reference.find(".pho");		 
		std::string tymczas = this->reference;
		std::string  mary_out = tymczas.substr(0,kup);
		mary_out.append(".xml");
		filenames.Mary_out = mary_out;

		if(mi.MaryXMLToPhonemes(maryxml,filenames.Mary_out.c_str())==0)
		
			{
				printf("Signal:: Error running the Mary client module\n");
				return 0;
			}

		//}//end MARY_NATURAL_SPEECH
		
		//else { printf("Natural speech\n");}

		treemary=parsemary.ParseFile(filenames.Mary_out.c_str());

		if(treemary==0)
		{
			printf("Signal:: Error parsing Mary output\n");
			return 0;
		}

	}

	//case 2: pho file - do nothing
	if(this->usingMary==2)
	{
		mi.SetUsingMary(0);			
				
		//update reference file name...

	}
	

	//case 3: using an openmary file - parse it
	if(this->usingMary==3)
	{
		mi.SetUsingMary(0);			
		
		//check what the reference contains

		//std::string temp = filenames.Base_File+"/../../";
					
		std::string temp =  this->reference;
        			
		treemary=parsemary.ParseFile(this->reference);

		if(treemary==0)
		{
			printf("Signal:: Error parsing Mary output\n");
			return 0;
		}

	}

	std::string phonemesfilename;


	//CASE 0: semaine
	if(this->usingMary==0)
	{
		if(this->reference!="")
			phonemesfilename=this->reference;
		else
			if(filenames.Phonemes_File!=""){

				phonemesfilename=filenames.Phonemes_File;
				
			}
			else
			{
				phonemesfilename="tmp/from-fml-apml.pho";
				filenames.Phonemes_File="tmp/from-fml-apml.pho";
			}
	}
	

	// CASE 1: usually
	if(this->usingMary==1)
	{
		if(this->reference!=""){
			phonemesfilename=this->reference;
			filenames.Phonemes_File=this->reference;			
		}
		else {

			phonemesfilename="tmp/from-fml-apml.pho";
			filenames.Phonemes_File="tmp/from-fml-apml.pho";
		}

	}

	// case 2: .pho by hand
	//do nothing
	
	// case 3: openmary.out by hand
	if(this->usingMary==3)
	{
		if(this->reference!="")
		{
			phonemesfilename=this->reference;
			phonemesfilename =  phonemesfilename.replace(phonemesfilename.length()-4,4,".pho");
			filenames.Phonemes_File=  phonemesfilename;

		}
		else
			phonemesfilename="tmp/mary-out.pho";
	}
	
	 
	if(language=="french")	 mi.SetFrench();
	if(language=="english")  mi.SetEnglish();
	if(language=="german")   mi.SetGerman();


	/////////////////////////////////////////////////////////
	//generate PHO file

	//if we do not have pho file we generate it.
	//in a case of usingMary==2 we have pho file
	//in a case of usingMary==4 we do not use info about phonems


	if ((this->usingMary!=2) && (this->usingMary!=4))
		mi.ExtractAllPhonemes(phonemesfilename,treemary,&tml,&stressingpoints,&time);

	this->StoreTimeMarkersTimings(&tml);

	return 1;
#endif
}

int Signal::GenerateSpeech(std::string wavefilename)
{
#ifdef USE_ACAPELA
	return 1;
#else
	if(this->modality!="speech")
		return 0;
	std::string maryxml;

	maryxml=this->ToMaryXML();

	MaryInterface mi;

	//if you use 
	if(this->usingMary==1)

	//if(inimanager.GetValueInt("MARY_NATURAL_SPEECH")!=1)
	
	{

		if(mi.MaryXMLToWave(maryxml,wavefilename)==0)
		{
			printf("Signal:: Error running the Mary client module\n");
			return 0;
		}
	
	}
	
	return 1;
#endif
}


char * Signal::GenerateSpeechToChar(int *size)
{
#ifdef USE_ACAPELA
	if(audioBufferAcapela!=0){
		*size = sizeaudioBufferAcapela;
		return audioBufferAcapela;
	}
	else
	{
		printf("Signal:: Error running Acapela TTS\n");
		return NULL;
	}

#else

	if(this->modality!="speech")
		return 0;
	std::string maryxml;

	maryxml=this->ToMaryXML();

	MaryInterface mi;

	char * output=mi.MaryXMLToWaveInChar(maryxml,size);
	
	if (output == NULL) 
	{
		printf("Signal:: Error running the Mary client module\n");
		return NULL;
	}

	return output;
#endif
}


int Signal::Temporize(std::vector<Signal> &signals,void *speech,int *loopnum)
{
	if(concretized==true)
		return 1;

	//if(duration==0)
	//	return 0;

	(*loopnum)++;

	if((*loopnum)>=11)
	{
		printf("Signal::Error too many levels of recursion in signal temporization (%d) \n", *loopnum);
		return 0;
	}

	start=-1;
	duration=-1;
	
	if(start_sym.find_first_not_of("0123456789.-")==std::string::npos)
	{
		start=atof(start_sym.c_str());
	}
	if(duration_sym!="")
	{
		if(duration_sym.find_first_not_of("0123456789.-")==std::string::npos)
		{
			duration=atof(duration_sym.c_str());
		}
	}

	if(start==-1)
	{
		TemporizeAttribute(&start,&start_sym,signals,speech,loopnum);
	}

	if((duration==-1)&&(duration_sym!=""))
	{
		TemporizeAttribute(&duration,&duration_sym,signals,speech,loopnum);
		if(duration!=-1)
			duration=duration-start;

	}
	
	if(start==-1)
		return 0;

	if(duration<0)
	{
		//TRICKY: fake duration for the realtime version of the engine
		duration_sym="6.0";
		duration=6.0;
		//idle=true;
		noend=true;
	}
	else
	{
		noend=false;
	}

	concretized=true;

	return 1;
}


int Signal::Temporize(std::vector<Signal*> &signals,void *speech,int *loopnum)
{
	if(concretized==true)
		return 1;

	if(duration==0)
		return 0;

	(*loopnum)++;

	if((*loopnum)>=11)
	{
		printf("Signal::Error too many levels of recursion in signal temporization (%d)\n", *loopnum);
		return 0;
	}

	start=-1;
	duration=-1;
	
	if(start_sym.find_first_not_of("0123456789.-")==std::string::npos)
	{
		start=atof(start_sym.c_str());
	}
	if(duration_sym!="")
	{
		if(duration_sym.find_first_not_of("0123456789.-")==std::string::npos)
		{
			duration=atof(duration_sym.c_str());
		}
	}

	if(start==-1)
	{
		TemporizeAttribute(&start,&start_sym,signals,speech,loopnum);
	}

	if((duration==-1)&&(duration_sym!=""))
	{
		TemporizeAttribute(&duration,&duration_sym,signals,speech,loopnum);
		if(duration!=-1)
			duration=duration-start;

	}
	
	if(start==-1)
		return 0;

	if(duration<0)
	{
		//TRICKY: fake duration for the realtime version of the engine
		duration_sym="6.0";
		duration=6.0;
		//idle=true;
		noend=true;
	}
	else
	{
		noend=false;
	}

	concretized=true;

	return 1;
}


int Signal::TemporizeAttribute(float *attribute,std::string *attribute_sym,std::vector<Signal> &signals,void *speech,int *loopnum)
{
	std::string referto;
	std::string referattr;

	referto=(*attribute_sym).substr(0,(*attribute_sym).find_first_of(":"));
	referattr=(*attribute_sym).substr((*attribute_sym).find_first_of(":")+1);

	if(speech==0)
		return 0;

	if(referto!="")
	{
		if(((Signal*)speech)->id!=referto)
		{
			Signal *ref;
			ref=GetSignal(signals,referto);
			if(ref!=0)
			{
				if(ref->Temporize(signals,speech,loopnum)!=0)
				{
					if(referattr=="start")
						*attribute=ref->start;
					if(referattr=="end")
						*attribute=ref->start+ref->duration;
				}
				else
				{
					printf("MMSystem::Cannot concretize signal: %s-%s\n",id.c_str());
					return 0;
				}
			}
		}
		else
		{
			*attribute=((Signal*)speech)->GetTimeMarker(referattr);
		}
	}
	return 1;
}


int Signal::TemporizeAttribute(float *attribute,std::string *attribute_sym,std::vector<Signal*> &signals,void *speech,int *loopnum)
{
	std::string referto;
	std::string referattr;

	referto=(*attribute_sym).substr(0,(*attribute_sym).find_first_of(":"));
	referattr=(*attribute_sym).substr((*attribute_sym).find_first_of(":")+1);

	if(referto!="")
	{
		if(((Signal*)speech)->id!=referto)
		{
			Signal *ref;
			ref=GetSignal(signals,referto);
			if(ref!=0)
			{
				if(ref->Temporize(signals,speech,loopnum)!=0)
				{
					if(referattr=="start")
						*attribute=ref->start;
					if(referattr=="end")
						*attribute=ref->start+ref->duration;
				}
				else
				{
					printf("MMSystem::Cannot concretize signal: %s-%s\n",id.c_str());
					return 0;
				}
			}
		}
		else
		{
			*attribute=((Signal*)speech)->GetTimeMarker(referattr);
		}
	}
	return 1;
}

Signal *Signal::GetSignal(std::vector<Signal> &signals,std::string id)
{
	std::vector<Signal>::iterator iter;
	for(iter=signals.begin();iter!=signals.end();iter++)
	{
		if((*iter).id==id)
			return &(*iter);
	}
	return 0;
}

Signal *Signal::GetSignal(std::vector<Signal*> &signals,std::string id)
{
	std::vector<Signal*>::iterator iter;
	for(iter=signals.begin();iter!=signals.end();iter++)
	{
		if((*iter)->id==id)
			return (*iter);
	}
	return 0;
}

void Signal::ShuffleShape()
{
	if(this->alternativeshapes.empty()==true)
		return;
	
	double p;
	p = randomgen->GetRand01();

	float accum;
	accum=0;
	alternativeshape store;
	std::vector<alternativeshape>::iterator iter;
	for(iter=this->alternativeshapes.begin();iter!=this->alternativeshapes.end();iter++)
	{
		accum=accum+(*iter).probability;
		if(p<accum)
		{
			store.content=this->content;
			store.intonation=this->intonation;
			store.meaning=this->meaning;
			store.voicequality=this->voicequality;
			store.name=this->reference;
			
			this->content=(*iter).content;
			this->intonation=(*iter).intonation;
			this->meaning=(*iter).meaning;
			this->voicequality=(*iter).voicequality;
			this->reference=(*iter).name;
			
			(*iter).content=store.content;
			(*iter).intonation=store.intonation;
			(*iter).meaning=store.meaning;
			(*iter).voicequality=store.voicequality;
			(*iter).name=store.name;
			break;
		}
	}
}

void Signal::Clear()
{
	this->modality="";
	this->type="";
	this->direction="";
	this->voice="";
	this->language="";
	this->text="";
	this->start=0;
	this->duration=0;
	this->start_sym="";
	this->duration_sym="";
	this->BMLcode="";
	this->id="";
	this->posture="";
	this->pitchaccents.clear();
	this->boundaries.clear();
	this->shape="";
	this->speed_num=0;
	this->speed_sym="";
	this->strokes.clear();
	this->timemarkers.clear();
	this->parameters.clear();

	this->content="";
	this->meaning="";
	this->intonation="";
	this->voicequality="";

	this->intensity=1;
	//this->idle=false;
	this->noend=false;
}

float Signal::GetIntensity(){return intensity;}
   /** 
    * It allows todefine the value of the intensity.
    * 
    */
void Signal::SetIntensity(float intensity1){this->intensity=intensity1;}


Signal * Signal::clone()
{
	Signal *newone = new Signal();

	newone->modality=this->modality;
	newone->type=this->type;
	newone->id=this->id;
	newone->start_sym=this->start_sym;
	newone->duration_sym=this->duration_sym;
	newone->start=this->start;
	newone->duration=this->duration;
	newone->reference=this->reference;
	newone->direction=this->direction;
	newone->posture=this->posture;
	newone->shape=this->shape;
	newone->language=this->language;
	newone->voice=this->voice;
	newone->speed_num=this->speed_num;
	newone->speed_sym=this->speed_sym;
	newone->concretized=this->concretized;
	newone->noend=this->noend;
	newone->BMLcode=this->BMLcode;
	newone->start=this->start;
	newone->duration=this->duration;
	newone->content=this->content;
	newone->meaning=this->meaning;
	newone->intonation=this->intonation;
	newone->voicequality=this->voicequality;
	//newone->excludelist=this->excludelist;
	
	//newone->idle=this->idle;
	newone->noend=this->noend;
	
	newone->intensity=this->intensity;
		
	if(this->strokes.empty()==false)
	{
		std::vector<float>::const_iterator iter;
		for(iter=this->strokes.begin();iter!=this->strokes.end();iter++)
		{
			float f=(*iter);
			newone->strokes.push_back(f);
		}
	}

	if(this->parameters.empty()==false)
	{
		EngineParameters::const_iterator iter;
		for(iter=this->parameters.begin();iter!=this->parameters.end();iter++)
		{
			newone->parameters[(*iter).second.name]=(*iter).second;
		}
	}

	if(this->timemarkers.empty()==false)
	{
		std::map<std::string,TimeMarker>::const_iterator iter;
		for(iter=this->timemarkers.begin();iter!=this->timemarkers.end();iter++)
		{
			newone->timemarkers[(*iter).first]=(*iter).second;
		}
	}
	if(this->alternativeshapes.empty()==false)
	{
		std::vector<alternativeshape>::const_iterator iter;
		for(iter=this->alternativeshapes.begin();iter!=this->alternativeshapes.end();iter++)
		{
			alternativeshape *as;
			as=new alternativeshape();
			as->name=(*iter).name;
			as->content=(*iter).content;
			as->meaning=(*iter).meaning;
			as->intonation=(*iter).intonation;
			as->voicequality=(*iter).voicequality;
			as->probability=(*iter).probability;
			newone->alternativeshapes.push_back(*as);
		}
	}

	if(this->stressingpoints.empty()==false)
	{
		std::vector<float>::const_iterator iter;
		for(iter=this->stressingpoints.begin();iter!=this->stressingpoints.end();iter++)
		{
			newone->stressingpoints.push_back(*iter);
		}
	}

	return newone;
}

bool Signal::intersect(float begin, float end)
{

	if ( (this->start<=begin) && (this->start+ this->duration >= begin) ) return true;
	if ( (this->start<=end) && (this->start+ this->duration >= end) ) return true;
	return false;
}

bool Signal::intersect(float begin)
{
	if ( (this->start<=begin) && (this->start+ this->duration >= begin) ) return true;
	
	return false;
}

void Signal::updateUsingMary()
{	
		//do not use open mary at all
		if ( (inimanager.GetValueString("TEXT_TO_SPEECH")=="activemary") && (usingMary==0) ) usingMary=0;
		
		//use openmary
		if ( (inimanager.GetValueString("TEXT_TO_SPEECH")=="openmary") && (usingMary==1) ) usingMary=1;
		
		//do not change this line
		int kup = reference.find(".pho");

		//do not use open mary, use pho file
		if ( (inimanager.GetValueString("TEXT_TO_SPEECH")=="realspeech")  && ( kup > 0 )) usingMary=2;

		//do not change this line
		kup = reference.find(".xml");

		//do not use openmary,  pre generated xml-output file
		if ( (inimanager.GetValueString("TEXT_TO_SPEECH")=="realspeech") && ( kup > 0 ) ) usingMary=3;
		
		kup = reference.find(".wav");

		//do not use openmary,  use the wav file without lips movements (this can be added later from fap file!)
		if ( (inimanager.GetValueString("TEXT_TO_SPEECH")=="realspeech") && ( kup > 0 ) ) usingMary=4;
	}