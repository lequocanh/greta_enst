//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// PlayerReceiver.h: interface for the PlayerReceiver class.
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#ifndef SEMAINE_COMPONENTS_GRETA_PLAYERRECEIVER_H
#define SEMAINE_COMPONENTS_GRETA_PLAYERRECEIVER_H

#include "RealtimePlayerWindow.h"
#include "CommandsManager.h"
#include "AnimationManager.h"

#include <semaine/config.h>

#include <cms/CMSException.h>

#include <semaine/components/Component.h>
#include <semaine/cms/sender/Sender.h>
#include <semaine/cms/receiver/Receiver.h>
#include <semaine/cms/receiver/StateReceiver.h>
#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/sender/XMLSender.h>
#include "XercesTool.h"

//#include "CentralClock.h"


using namespace cms;
using namespace semaine::components;
using namespace semaine::cms::sender;
using namespace semaine::cms::receiver;

namespace semaine {
namespace components {
namespace greta {

class PlayerReceiver : public Component
{
public:
	PlayerReceiver(const std::string & componentName, bool isInput, bool isOutput, RealtimePlayerWindow* rpw, CentralClock *pc) //, AnimationManager* am,//FramesManager* fm, CommandsManager* cm, CentralClock *pc) 
		throw (CMSException);
	virtual ~PlayerReceiver();

	CentralClock *pc;
	RealtimePlayerWindow* rpw;
	AnimationManager* am;//FramesManager* fm;

	//CommandsManager* cm;

	int sound_time;

	void audioEnded(std::string id);
	void fapEnded(std::string id);
	void bapEnded(std::string id);
	void fapstart(std::string id);
	void bapstart(std::string id);
	void audiostart(std::string id);
	void setFinished(int finish);

protected:
	virtual void react(SEMAINEMessage * m) throw (CMSException);
	virtual void act() throw(CMSException);
	XERCES_CPP_NAMESPACE::DOMDocument* writeFeedback(std::string type, std::string data, std::string id, int time);
	void customStartIO() throw(std::exception);

private:
	StateReceiver * contextReceiver;
	Receiver * fapReceiver;
	Receiver * bapReceiver;
	Receiver * wavReceiver;
	Receiver * commandReceiver;
	XMLSender * callbackAudioSender;
	XMLSender * callbackBAPSender;
	XMLSender * callbackFAPSender;

	int previous_time;
	int finished;
	long itercall;
	long audiocreationID;
	std::string audioID;
	long fapcreationID;
	std::string fapID;
	long bapcreationID;
	std::string bapID;
	std::string characterDisplaying;
	long long old_time;

};

} // namespace greta
} // namespace components
} // namespace semaine


#endif
