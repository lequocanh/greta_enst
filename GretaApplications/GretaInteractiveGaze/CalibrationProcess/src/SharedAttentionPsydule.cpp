// Shared-Attention system
// Christopher Peters

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <math.h>

#include "SharedAttentionPsydule.h"
#include "IniManager.h"
#include "XMLGenericParser.h"
#include "SDL_thread.h"
#include "SDL_net.h"
#include "RandomGen.h"
#include "EngineParameter.h"

extern IniManager inimanager;
RandomGen *randomgen=0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SharedAttentionPsydule::SharedAttentionPsydule(GLViewer *glV, std::string name, std::string host, int port) 
{

	
	commint = new Psydule(name, host, port);
	
	printf("\nRegistering GazeDetector module (%s) with Psyclone server %s: %d ...", name.c_str(), host.c_str(), port);
	this->name=name;
	this->host=host;
	this->port=port;
	this->connected = false;

	this->lastMessageCheck = clock();
	this->messageCheckFrequencySec = 0.1;	//check for a message every 100 ms
	this->glViewer = glV;
}

void SharedAttentionPsydule::enableCalibrateMode(void)
{
	this->glViewer->enableCalibrationMode();
}

SharedAttentionPsydule::~SharedAttentionPsydule()
{
	deleteAllRecievedMessages();
	this->sendAdminMessage("SharedAttentionPsydule.Shutdown");
}

void SharedAttentionPsydule::checkForMessage(int timeoutMS)
{
	if (this->connected) readMessage(commint->ReceiveMessage(timeoutMS));	
}

void SharedAttentionPsydule::deleteAllRecievedMessages(void)
{
	for (int i = 0; i < recievedMessages.size(); i ++)
	{
		delete recievedMessages[i];
	}
}

int SharedAttentionPsydule::readMessage(CommunicationMessage *msg)
{
	std::string type;
	std::string content;

	if(msg==NULL) return(0);

	type = msg->getType();
	content= msg->getContent();

	//printf("\nRecieved message type %s with content %s", type.c_str(), content.c_str());
	//printf("\nRecieved message (%s): ", type.c_str());

	PsycloneMessage *newMsg = new PsycloneMessage;
	newMsg->handled = false;
	if (strcmp(content.c_str(),"")!=0)
	{
		newMsg->type.assign(msg->getType().c_str());
	}
	else newMsg->type.assign("ERROR: no type");

	if (strcmp(content.c_str(),"")!=0)
	{
		newMsg->data.assign(msg->getContent().c_str());
	}
	else newMsg->data.assign("");

	this->recievedMessages.push_back(newMsg);

	std::string contentStr;
	contentStr.assign(content.c_str());

	if(strcmp(content.c_str(),"")!=0)
	{
		if(strcmp(msg->getType().c_str(),"Greta.Data.GazeHeadDir")==0)
		{
			//printf("Message type was: Greta.Data.GazeHeadDir");
			//get the coordinates from the string
			std::string::size_type loc = contentStr.find_first_of(" ");  
			if( loc == std::string::npos ) 
			{
				//printf("\nError found in message", loc);
				return false;
			}
			int size = contentStr.size();
			float xDir = atof(contentStr.substr(0, loc).c_str());
			float yDir = atof(contentStr.substr(loc + 1, size).c_str());
			//printf("X: %3.5f   Y: %3.5f",xDir,yDir);

			if (this->glViewer->theScene->calibrator->isCalibrateModeEnabled)
			{
				this->glViewer->theScene->calibrator->addEntry(xDir,yDir);
			}
			//only set the cursor position according to messages when it is not in test mode (needs to be changed...)
			else if (!this->glViewer->testMode && this->glViewer->theScene->calibrator->calibrated) 
			{
				this->glViewer->convertGazeDirToScreenCoords(xDir,yDir);
			}
		}
	}
}

bool SharedAttentionPsydule::registerPsyduleConnection(void)
{
	std::list<std::string> datatypes;
	datatypes.push_back("Greta.Admin.Gaze");
	datatypes.push_back("Greta.Data.GazeRaw");
	datatypes.push_back("Greta.Data.GazeHeadDir");
	
	if(commint->Register("Greta.Whiteboard",datatypes)!=0)
	{
		printf("\nRegistration successful!\n");
		this->connected = true;
		this->sendAdminMessage("SharedAttentionPsydule.Startup");
		return true;
	}
	else
	{
		printf("\nError: Could not register to Psyclone\n");
		printf("\nPlease ensure that the Psyclone server is running\n");
		this->connected = false;
		return false;
	}
}

void SharedAttentionPsydule::sendAdminMessage(std::string msg)
{
	if (this->connected) commint->PostString(msg, "Greta.Whiteboard", "Greta.Admin.Gaze");
	else printf("\nUnable to send Psydule message: not connected");
}