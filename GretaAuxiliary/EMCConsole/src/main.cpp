//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "ZignTrackConverter.h"
#include "OptiTrackConverter.h"
#include "FAPwriter.h"

//#include "XercesTool.h"

//#include "EnterfaceCaptureMotion.h"
//#include "IniManager.h"
//#include "DataContainer.h"

//DataContainer *datacontainer;
//IniManager inimanager;
//std::string ini_filename;

int main(int argc, char ** argv){

	std::string filename="c:\\mc\\normalisedElisabettaNEW2";
	std::string directory="c:\\mc\\new2.txt";
	std::string which="new";

	if (argc==3)
	{
		which=argv[1];
		filename=argv[2];
		directory="";


	} else 
		if (argc==4)
		{
			which=argv[1];
			filename=argv[2];
			directory=argv[3];

		} else {

			printf("First argument - version (zign, opti, new), second - filenamebase, third argument - mask");
			//return 1;		

		}

		if (which.compare("zign")==0)
		{

			ZignTrackConverter con9;

			//input
			std::string path = filename + ".txt";

			con9.readMCMessage( (char*)path.c_str() );

			con9.readmask(directory);

			FAPwriter fw9;

			//output
			path = filename+ ".fap";
			fw9.WriteToFile(con9.converttoFAPs(), path, 25);


		}


		if (which.compare("opti")==0)
		{

			OptiTrackConverter con9;

			//input
			std::string path = filename + ".txt";

			con9.readMCMessage( (char*)path.c_str() );
			
			con9.readmask(directory);

			FAPwriter fw9;

			//output
			path = filename+ ".fap";
			fw9.WriteToFile(con9.converttoFAPs(), path, 25);

		}

		
		// NEW
		if (which.compare("new")==0)
		{

			OptiTrackConverter con9;

			//input
			std::string path = filename + ".txt";

			con9.readMHMessage( (char*)path.c_str() );
			
			con9.readmask(directory);

			FAPwriter fw9;

			//output
			path = filename+ ".fap";
			fw9.WriteToFile(con9.converttoFAPs2(), path, 25);

		}

		return 0;

}
