//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// CommunicationInterface.h: interface for the Psydule and Activule classes.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#include "CommunicationMessage.h"

#include <string>
#include <list>
#include <map>
#include <fstream>


/**
*
* class CommuncationInterface
*
*/

class CommunicationInterface
{
public:

	/**
	*
	* contructor 
	*
	* @param  name
	* @param  std::string host
	* @param  int port
	*/

	CommunicationInterface(){};

	/**
	*
	* destructor 
	*/

	virtual ~CommunicationInterface(){};


	/**
	* this method 
	* 
	*
	* @return 
	* @param  whiteboard
	* @param  std::string datatype
	*/

	virtual int Register(std::string whiteboard, std::string datatype) = 0;


	/**
	* this method 
	* 
	*
	* @return 
	* @param  whiteboard
	* @param  std::list<std::string> datatypes
	*/

	virtual int Register(std::string whiteboard, std::list<std::string> datatypes) = 0;
	virtual int RegisterSender(std::map<std::string, std::string> sendertypename) = 0;
	virtual long long get_time() = 0;
	virtual std::string getGretaName(std::string GretaName) = 0;
	virtual void setIsInputisOutput(bool isInput, bool isOutput) = 0;
	

	/**
	* this method 
	* 
	*
	* @return 
	* @param  tosend
	* @param  std::string whiteboard
	* @param  std::string datatype
	*/

	virtual int PostString(std::string tosend, std::string whiteboard, std::string datatype) = 0;;

	/**
	* this method 
	* 
	*
	* @return 
	* @param  timeout
	*/

	virtual std::string ReceiveString(int timeout) = 0;

	/**
	* this method 
	* 
	*
	* @return 
	* @param  timeout
	*/

	virtual CommunicationMessage* ReceiveMessage(int timeout) = 0;
	

	/**
	* this method 
	* 
	*
	* @return 
	* @param  filename
	* @param msg
	*/

	virtual int WriteBinaryFile(std::string filename, CommunicationMessage *msg) = 0;


	/**
	* this method 
	* 
	*
	* @return 
	* @param  fapfilename
	* @param  std::string whiteboard
	* @param  std::string datatype
	*/

	virtual int SendFAPs(std::string fapfilename, std::string whiteboard, std::string datatype) = 0;

	/**
	* this method 
	* 
	*
	* @return 
	* @param  bapfilename
	* @param  std::string whiteboard
	* @param  std::string datatype
	*/

	virtual int SendBAPs(std::string bapfilename, std::string whiteboard, std::string datatype) = 0;;

	/**
	* this method 
	* 
	*
	* @return 
	* @param  binfilename
	* @param  std::string whiteboard
	* @param  std::string datatype
	*/

	virtual int SendBinaryFile(std::string binfilename, std::string whiteboard, std::string datatype) = 0;

	/**
	* this method 
	* 
	*
	* @return 
	* @param  tosend
	* @param  std::string whiteboard
	* @param  std::string datatype
	* @param  size
	*/

	virtual int  PostSpeechFromChar(char* tosend, std::string whiteboard, std::string datatype,int * size) = 0;


	std::string name,host;
	int port;
};



