/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gretaTests;

import com.cmlabs.air.JavaAIRPlug;
import com.cmlabs.air.Message;
import com.cmlabs.air.Time;

/**
 *
 * @author Andre
 */
public class PsycloneSender implements Sender{

    private JavaAIRPlug plug;
    private String host="127.0.0.1";
    private int port = 10000;
    private String me = "GretaTest";
    private String whiteboard = "Greta.Whiteboard";
    private String BMLDest = "Greta.Data.BML";
    private String FMLDest = "Greta.Data.FML";
    private String xml =
                    "<module name=\""+me+"\" allowselftriggering=\"yes\">"+
                    "    <spec>"+
                    "        <context name=\"Psyclone.System.Ready\">"+
                    "            <phase id=\"Phase One\">" +
                    "                <post to=\"Greta.Whiteboard\" type=\""+BMLDest+"\" />" +
                    "                <post to=\"Greta.Whiteboard\" type=\""+FMLDest+"\" />"  +
                    "            </phase>" +
                    "        </context>"+
                    "    </spec>"+
                    "</module>";

    private boolean initialized = false;
    private boolean connected = false;
    private boolean registered = false;

    public void connect() {
        
        if(plug!=null && isConnected())
            return ;

        plug = new JavaAIRPlug(me, host, port);
        if (!plug.init())
            System.out.println("Could not connect to the Server on " + host + " on port " + port + ".");
        else{
            initialized = true;
            if (!plug.openTwoWayConnectionTo(whiteboard))
                System.out.println("Could not open callback connection to Greta.Whiteboard.");
            else{
                connected = true;
                if (!plug.sendRegistration(xml)) 
                    System.out.println("Could not register for messages. Exit.");
                else{
                    registered = true;
                    System.out.println("Ready to send messages...");
                }
            }
        }
    }

    public void sendBML(String BML) {
        send(BML,BMLDest);
    }

    public void sendFML(String FML) {
        send(FML,FMLDest);
    }

    public boolean isConnected() {
        return registered && connected && initialized;
    }

    private void send(String message, String where){
        if(!isConnected())
            return ;
        Message msg_ans = new Message(me,whiteboard, where);
        try{
            msg_ans.content = message;
            boolean posted = plug.postOutputMessage(msg_ans);
            if (!posted)
                System.out.println("I could not post message...");
            else{
                Time now = new Time();
                System.out.println("Message posted at:" + now.printTime());
            }
        }
        catch (Exception e){
            System.out.println("I could not generate answer..."+ e);
        }
    }
}
