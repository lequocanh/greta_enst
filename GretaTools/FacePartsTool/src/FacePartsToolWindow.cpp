//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// FacePartsToolWindow.cpp: implementation of the FacePartsToolWindow class.
//
//////////////////////////////////////////////////////////////////////
#define toimplement

#include "FacePartsToolWindow.h"
#include <stdio.h>
#include <string.h>
#include <list>
#include <string>
#include <fl/Fl_File_Chooser.h>
#include <FL/Fl.H>
#include "IniManager.h"
#include ".\facepartstoolwindow.h"

extern IniManager inimanager;

FILE *face_log;


void left_eye_move(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setLeftEyePosition(
				w->l_eye_x->value(),
				w->l_eye_y->value(),
				w->l_eye_z->value());
}

void right_eye_move(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setRightEyePosition(
				w->r_eye_x->value(),
				w->r_eye_y->value(),
				w->r_eye_z->value());
}

void scale_eyes(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setScaleEyes(w->eyes_scale->value());
}

void teeth_move(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setJawPosition(
				w->teeth_x->value(),
				w->teeth_y->value(),
				w->teeth_z->value());
}

void scale_teeth(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setScaleJaw(w->teeth_scale->value());
}

void hair_move(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setHairPosition(
				w->hair_x->value(),
				w->hair_y->value(),
				w->hair_z->value());
}

void scale_hair(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setScaleHair(
				w->scale_hair_x->value(),
				w->scale_hair_y->value(),
				w->scale_hair_z->value());
}

void head_move(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setPosition(
				w->head_x->value(),
				w->head_y->value(),
				w->head_z->value());
}

void scale_head(Fl_Roller* s, FacePartsToolWindow* w){
	w->glutw->getAgent()->getHead()->setScale(w->head_scale->value());
}

FacePartsToolWindow::FacePartsToolWindow():Fl_Double_Window(10,20,700,600,"FacePartsTool")
{
	this->size_range(700,600,0,0,1,1,0);

	box_eyes= new Fl_Box(FL_BORDER_BOX, 5, 18, 200, 150,"Eyes");
	box_eyes->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);

//left eye
	l_eye_x = new Fl_Roller(115,30,80,15,"Left : X");
	l_eye_x->callback((Fl_Callback *)left_eye_move,this);
	initCounter(l_eye_x);

	l_eye_y = new Fl_Roller(115,65,80,15,"Left : Y");
	l_eye_y->callback((Fl_Callback *)left_eye_move,this);
	initCounter(l_eye_y);

	l_eye_z = new Fl_Roller(115,100,80,15,"Left : Z");
	l_eye_z->callback((Fl_Callback *)left_eye_move,this);
	initCounter(l_eye_z);
	
//right eye
	r_eye_x = new Fl_Roller(15,30,80,15,"Right : X");
	r_eye_x->callback((Fl_Callback *)right_eye_move,this);
	initCounter(r_eye_x);

	r_eye_y = new Fl_Roller(15,65,80,15,"Right : Y");
	r_eye_y->callback((Fl_Callback *)right_eye_move,this);
	initCounter(r_eye_y);

	r_eye_z = new Fl_Roller(15,100,80,15,"Right : Z");
	r_eye_z->callback((Fl_Callback *)right_eye_move,this);
	initCounter(r_eye_z);
//scale eyes
	eyes_scale = new Fl_Roller(65,135,80,15,"scale");
	eyes_scale->callback((Fl_Callback *)scale_eyes,this);
	initCounter(eyes_scale);





	box_teeth= new Fl_Box(FL_BORDER_BOX, 5, 188, 200, 110,"Teeth");
	box_teeth->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);

//teeth
	teeth_x = new Fl_Roller(15,200,80,15,"X");
	teeth_x->callback((Fl_Callback *)teeth_move,this);
	initCounter(teeth_x);

	teeth_y = new Fl_Roller(15,230,80,15,"Y");
	teeth_y->callback((Fl_Callback *)teeth_move,this);
	initCounter(teeth_y);

	teeth_z = new Fl_Roller(15,260,80,15,"Z");
	teeth_z->callback((Fl_Callback *)teeth_move,this);
	initCounter(teeth_z);
//scale teeth
	teeth_scale = new Fl_Roller(115,230,80,15,"scale");
	teeth_scale->callback((Fl_Callback *)scale_teeth,this);
	initCounter(teeth_scale);





	box_hair= new Fl_Box(FL_BORDER_BOX, 5, 318, 200, 110,"Hair");
	box_hair->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);

//hair
	hair_x = new Fl_Roller(15,330,80,15,"X");
	hair_x->callback((Fl_Callback *)hair_move,this);
	initCounter(hair_x);

	hair_y = new Fl_Roller(15,360,80,15,"Y");
	hair_y->callback((Fl_Callback *)hair_move,this);
	initCounter(hair_y);

	hair_z = new Fl_Roller(15,390,80,15,"Z");
	hair_z->callback((Fl_Callback *)hair_move,this);
	initCounter(hair_z);
//scale hair
	scale_hair_x = new Fl_Roller(115,330,80,15,"scale X");
	scale_hair_x->callback((Fl_Callback *)scale_hair,this);
	initCounter(scale_hair_x);
	
	scale_hair_y = new Fl_Roller(115,360,80,15,"scale Y");
	scale_hair_y->callback((Fl_Callback *)scale_hair,this);
	initCounter(scale_hair_y);
	
	scale_hair_z = new Fl_Roller(115,390,80,15,"scale Z");
	scale_hair_z->callback((Fl_Callback *)scale_hair,this);
	initCounter(scale_hair_z);




	box_head=new Fl_Box(FL_BORDER_BOX, 5, 448, 200, 110,"Head");
	box_head->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);

//head
	head_x = new Fl_Roller(15,460,80,15,"X");
	head_x->callback((Fl_Callback *)head_move,this);
	initCounter(head_x);

	head_y = new Fl_Roller(15,490,80,15,"Y");
	head_y->callback((Fl_Callback *)head_move,this);
	initCounter(head_y);

	head_z = new Fl_Roller(15,520,80,15,"Z");
	head_z->callback((Fl_Callback *)head_move,this);
	initCounter(head_z);
//scale head
	head_scale = new Fl_Roller(115,490,80,15,"scale");
	head_scale->callback((Fl_Callback *)scale_head,this);
	initCounter(head_scale);


	switchhair=new Fl_Button(5,570,65,20,"hide hair");
	hairOn=true;
	switchwireframe=new Fl_Button(75,570,65,20,"wireframe");
	wireframeOn=false;

	save=new Fl_Button(145,570,60,20,"save");


	glutw=new PlayerOgreView(215,5,480,590,"test");
}



void FacePartsToolWindow::show(){
	Fl_Double_Window::show();

	glutw->setBackgroundColour(0.5,0.5,0.5);

	l_eye_x->value(glutw->getAgent()->getHead()->getLeftEyePosition_X());
	l_eye_y->value(glutw->getAgent()->getHead()->getLeftEyePosition_Y());
	l_eye_z->value(glutw->getAgent()->getHead()->getLeftEyePosition_Z());
	r_eye_x->value(glutw->getAgent()->getHead()->getRightEyePosition_X());
	r_eye_y->value(glutw->getAgent()->getHead()->getRightEyePosition_Y());
	r_eye_z->value(glutw->getAgent()->getHead()->getRightEyePosition_Z());
	eyes_scale->value(glutw->getAgent()->getHead()->getScaleEyes());
	teeth_x->value(glutw->getAgent()->getHead()->getJawPosition_X());
	teeth_y->value(glutw->getAgent()->getHead()->getJawPosition_Y());
	teeth_z->value(glutw->getAgent()->getHead()->getJawPosition_Z());
	teeth_scale->value(glutw->getAgent()->getHead()->getScaleJaw());
	hair_x->value(glutw->getAgent()->getHead()->getHairPosition_X());
	hair_y->value(glutw->getAgent()->getHead()->getHairPosition_Y());
	hair_z->value(glutw->getAgent()->getHead()->getHairPosition_Z());
	scale_hair_x->value(glutw->getAgent()->getHead()->getScaleHair_X());
	scale_hair_y->value(glutw->getAgent()->getHead()->getScaleHair_Y());
	scale_hair_z->value(glutw->getAgent()->getHead()->getScaleHair_Z());
	head_x->value(glutw->getAgent()->getHead()->getPosition_X());
	head_y->value(glutw->getAgent()->getHead()->getPosition_Y());
	head_z->value(glutw->getAgent()->getHead()->getPosition_Z());
	head_scale->value(glutw->getAgent()->getHead()->getScale());

}

void FacePartsToolWindow::initCounter( Fl_Roller * counter){
	counter->maximum(1000);
	counter->minimum(-1000);
	counter->step(0.01);
	counter->type(FL_HORIZONTAL);
}

int FacePartsToolWindow::handle(int event)
{
	char v[10];
	if(Fl::event_button()==FL_LEFT_MOUSE)
	{
		if(event==FL_PUSH)
		{
			if(Fl::event_inside(switchhair))
			{
				glutw->getAgent()->SwitchHair();
				hairOn = !hairOn;
				if(hairOn)
					switchhair->copy_label("hide hair");
				else
					switchhair->copy_label("show hair");
			}
			if(Fl::event_inside(switchwireframe))
			{
				wireframeOn = !wireframeOn;
				if(wireframeOn){
					glutw->getAgent()->getAgentCamera()->setModeWireframe();
					switchwireframe->copy_label("solid");
				}
				else{
					glutw->getAgent()->getAgentCamera()->setModeSolid();
					switchwireframe->copy_label("wireframe");
				}
			}
			if(Fl::event_inside(save))
			{
				this->WriteFile();
			}
		}
	}
	return Fl_Double_Window::handle(event);
}

void FacePartsToolWindow::draw()
{
	glutw->size(this->w()-220,this->h()-10);
	Fl_Double_Window::draw();
}


FacePartsToolWindow::~FacePartsToolWindow()
{
}

void FacePartsToolWindow::WriteFile(void)
{
	Fl_File_Chooser *chooser;
	chooser= new Fl_File_Chooser(inimanager.Program_Path.c_str(),"face definition files (*.xml)",2,"select a face definition file");
	chooser->show();
	while (chooser->shown())
		Fl::wait();
	std::string s;
	s="";
	if(chooser->value()!=0)
		s=chooser->value();
	if(s!="")
	{
		FILE* f = fopen( s.c_str(), "w");

		if(f!=NULL)
		{
			fprintf(f,"<?xml version=\"1.0\"?>\n");
			fprintf(f,"<!DOCTYPE PARTS_DATA SYSTEM \"../facedefinition.dtd\" []>\n");
			fprintf(f,"<PARTS_DATA>\n");
			fprintf(f,"<HEADPOSITION x=\"%.3f",head_x->value());
			fprintf(f,"\" y=\"%.3f",head_y->value());
			fprintf(f,"\" z=\"%.3f",head_z->value());
			fprintf(f,"\" scale=\"%.3f",head_scale->value());
			fprintf(f,"\"/>\n");
			fprintf(f,"<LEFT_EYE x=\"%.3f",l_eye_x->value());
			fprintf(f,"\" y=\"%.3f",l_eye_y->value());
			fprintf(f,"\" z=\"%.3f",l_eye_z->value());
			fprintf(f,"\"/>\n<RIGHT_EYE x=\"%.3f",r_eye_x->value());
			fprintf(f,"\" y=\"%.3f",r_eye_y->value());
			fprintf(f,"\" z=\"%.3f",r_eye_z->value());
			fprintf(f,"\"/>\n<EYE_SCALE factor=\"%.3f" ,eyes_scale->value());
			fprintf(f,"\"/>\n<TEETHS-TONGUE x=\"%.3f",teeth_x->value());
			fprintf(f,"\" y=\"%.3f" ,teeth_y->value());
			fprintf(f,"\" z=\"%.3f",teeth_z->value());
			fprintf(f,"\" scale=\"%.3f",teeth_scale->value());
			fprintf(f,"\"/>\n<HAIRPOSITION x=\"%.3f",hair_x->value());
			fprintf(f,"\" y=\"%.3f",hair_y->value());
			fprintf(f,"\" z=\"%.3f",hair_z->value());
			fprintf(f,"\" scalex=\"%.3f",scale_hair_x->value());
			fprintf(f,"\" scaley=\"%.3f",scale_hair_y->value());
			fprintf(f,"\" scalez=\"%.3f",scale_hair_z->value());
			fprintf(f,"\"/>\n</PARTS_DATA>\n");

			fclose(f);
		}

	}
	delete chooser;
}
