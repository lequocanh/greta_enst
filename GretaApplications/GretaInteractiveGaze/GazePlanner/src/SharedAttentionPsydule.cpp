// Shared-Attention system
// Christopher Peters

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <math.h>

#include "SharedAttentionPsydule.h"
#include "IniManager.h"
#include "XMLGenericParser.h"
#include "SDL_thread.h"
#include "SDL_net.h"
#include "RandomGen.h"
#include "EngineParameter.h"

extern IniManager inimanager;
RandomGen *randomgen=0;

extern int SCREENX;
extern int SCREENY;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SharedAttentionPsydule::SharedAttentionPsydule(Scene *scene, std::string name, std::string host, int port) 
{
	
	commint = new Psydule(name, host, port);
	
	printf("\nRegistering GazeDetector module (%s) with Psyclone server %s: %d ...", name.c_str(), host.c_str(), port);
	this->name=name;
	this->host=host;
	this->port=port;
	this->connected = false;

	this->lastMessageCheck = clock();
	this->messageCheckFrequencySec = 0.1;	//check for a message every 100 ms
	this->myscene = scene;

	this->fixationList = new FixationList();	
	this->currentFixation = NULL;

}

void SharedAttentionPsydule::enableCalibrateMode(void)
{
	//this->glViewer->enableCalibrationMode();
}

SharedAttentionPsydule::~SharedAttentionPsydule()
{
	deleteAllRecievedMessages();
	this->sendAdminMessage("SharedAttentionPsydule.Shutdown");
}

void SharedAttentionPsydule::checkForMessage(int timeoutMS)
{
	if (this->connected) readMessage(commint->ReceiveMessage(timeoutMS));	
}

void SharedAttentionPsydule::deleteAllRecievedMessages(void)
{
	for (int i = 0; i < recievedMessages.size(); i ++)
	{
		delete recievedMessages[i];
	}
}

int SharedAttentionPsydule::readMessage(CommunicationMessage *msg)
{
	std::string type;
	std::string content;

	if(msg==NULL) return(0);

	type = msg->getType();
	content= msg->getContent();

	//printf("\nRecieved message type %s with content %s", type.c_str(), content.c_str());
	//printf("\nRecieved message (%s): ", type.c_str());

	PsycloneMessage *newMsg = new PsycloneMessage;
	newMsg->handled = false;
	if (strcmp(content.c_str(),"")!=0)
	{
		newMsg->type.assign(msg->getType().c_str());
	}
	else newMsg->type.assign("ERROR: no type");

	if (strcmp(content.c_str(),"")!=0)
	{
		newMsg->data.assign(msg->getContent().c_str());
	}
	else newMsg->data.assign("");

	this->recievedMessages.push_back(newMsg);

	std::string contentStr;
	contentStr.assign(content.c_str());

	if(strcmp(content.c_str(),"")!=0)
	{
		if(strcmp(msg->getType().c_str(),"Greta.Data.GazeHeadDir")==0)
		{
			//printf("Message content %s", content.c_str());
			//get the coordinates from the string
			std::string::size_type loc = contentStr.find_first_of(" ");  
			if( loc == std::string::npos ) 
			{
				//printf("\nError found in message", loc);
				return false;
			}
			int size = contentStr.size();
			float xDir = atof(contentStr.substr(0, loc).c_str());
			float yDir = atof(contentStr.substr(loc + 1, size).c_str());
			printf("\n before conf X: %3.5f   Y: %3.5f \n",xDir,yDir);

			//if (myscene->calibrator->isCalibrateModeEnabled)
			if (false)
			{
				myscene->calibrator->addEntry(xDir,yDir);
			}
			//only set the cursor position according to messages when it is not in test mode (needs to be changed...)
			
			else 
		
			//if (!myscene->testMode && myscene->calibrator->calibrated) 
				
			//if (myscene->calibrator->calibrated) 
			if (true) 
			{
				//convert coord
				convertGazeDirToScreenCoords(xDir,yDir);
				//update vao
				
				//ogni sec if vao jest observed more than one sec send bml to bml engine				
				//to chyba nie tu
			}

		}
	}
}

bool SharedAttentionPsydule::registerPsyduleConnection(void)
{
	std::list<std::string> datatypes;
	datatypes.push_back("Greta.Admin.Gaze");
	datatypes.push_back("Greta.Data.GazeRaw");
	datatypes.push_back("Greta.Data.GazeHeadDir");
	datatypes.push_back("Greta.Data.BML");
	
	if(commint->Register("Greta.Whiteboard",datatypes)!=0)
	{
		printf("\nRegistration successful!\n");
		this->connected = true;
		this->sendAdminMessage("SharedAttentionPsydule.Startup");
		return true;
	}
	else
	{
		printf("\nError: Could not register to Psyclone\n");
		printf("\nPlease ensure that the Psyclone server is running\n");
		this->connected = false;
		return false;
	}
}

void SharedAttentionPsydule::sendAdminMessage(std::string msg)
{
	if (this->connected) commint->PostString(msg, "Greta.Whiteboard", "Greta.Admin.Gaze");
	else printf("\nUnable to send Psydule message: not connected");
}



void SharedAttentionPsydule::createCurrentFixation(int xScreen, int yScreen, bool outsideLeft, bool outsideRight, bool outsideTop, bool outsideBottom)
{
	bool outsideScreen = outsideLeft || outsideRight || outsideTop || outsideBottom;
	if (!outsideScreen)
	{
		//see what object falls under those coordinates, putting the result into 'selectedID'
		//pickObject(xScreen,this->viewPort[3] - yScreen);

		
		selectedID=myscene->pickobject(xScreen,yScreen);

		//!!!
		//update the objects associated VAO
		myscene->updatePickedObjectsVAO(this->selectedID);
	}
	else
	{
		this->selectedID = -1;
		//update the special 'outside' VAO
		
		myscene->outsideScreenVAO->update(clock(), myscene->lastSelectedObjID == -1);
		myscene->lastSelectedObjID = -1;
	}

	//add the fixation to a list and store it as the current one
	this->currentFixation = this->fixationList->addFixation(clock(), xScreen, yScreen, this->selectedID, outsideLeft, outsideRight, outsideTop, outsideBottom);
}

// take this
void SharedAttentionPsydule::convertGazeDirToScreenCoords(float xDir, float yDir)
{
	//given a calibrated boundary, gets the screen coordinates in terms of the xDir and yDir
	//remember xDir is rotation around xAxis
	float xRange = myscene->bvs.headRightVal - myscene->bvs.headLeftVal;
	float yRange = myscene->bvs.headTopVal - myscene->bvs.headBottomVal;

	int xScreen, yScreen;

	//test to see if it is outside screen extents
	bool xOutside = false;
	bool yOutside = false;
	bool outsideLeft = false;
	bool outsideRight = false;
	bool outsideBottom = false;
	bool outsideTop = false;
	if (yDir < myscene->bvs.headLeftVal) 
	{
	xScreen = -1; 
	outsideLeft = true; 
	xOutside = true;
	}
	else 
		if (yDir > myscene->bvs.headRightVal) {
			xScreen = SCREENX + 1; 
			outsideRight = true; 
			xOutside = true;
		}

	if (xDir < myscene->bvs.headBottomVal) {
		yScreen = -1; 
		outsideBottom = true; 
		yOutside = true;
	}
	else 
		if (xDir > myscene->bvs.headTopVal) 
		{
			yScreen = SCREENY + 1; 
			outsideTop = true; 
			yOutside = true;
		}

	float xPercent, yPercent;
	if (!xOutside)
	{
		xPercent = (yDir - myscene->bvs.headLeftVal)/xRange;
		xScreen = xPercent * (float)SCREENX;
		//xScreen = this->viewPort[2]/2;
	}
	if (!yOutside)
	{
		yPercent = (xDir - myscene->bvs.headBottomVal)/yRange;
		yScreen = yPercent * (float)SCREENY;
		//yScreen = this->viewPort[3]/2;
	}
	//printf("\nxDir: %3.5f yDir: %3.5f converted to screen coords (%d, %d)", xDir, yDir, xScreen, yScreen);

	//create a fixation from the screen coordinates
	this->createCurrentFixation(xScreen, yScreen, outsideLeft, outsideRight, outsideTop, outsideBottom);
	
	//this->damage(1);
}

std::string *SharedAttentionPsydule::getSelectedIDObjectName(void)
{
	if (selectedID == -1) return NULL;
	if (myscene->getObjectByID(selectedID) == NULL) return NULL;
	return (myscene->getObjectByID(selectedID)->getName());
}

int SharedAttentionPsydule::getSelectedID(void)
{
	return selectedID;
}

