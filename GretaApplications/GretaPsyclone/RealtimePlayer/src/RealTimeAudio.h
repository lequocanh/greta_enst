#pragma once

#include "SDL\SDL_audio.h"
#include <stdlib.h>
/*
 * This class is based on SDL Library and audio.h/audio.c (UNIX version) from GretaPlayer project.
 * It works for wave from OpenMary output.
 * To use it with other sounds, you probably need to change it. 
 *
 */
class RealTimeAudio
{
private:
	static SDL_AudioSpec *desired_audio;
	static SDL_AudioSpec *obtained_audio;
	static SDL_AudioSpec wav_spec;
	static Uint32 wav_length;
	static Uint8 *wav_buffer;
	static int prev_audio_pos;
	static int prev_time;
	static int prev_frame;
	static int prev_mill;

	static Uint32 audio_len;
	static Uint8 *audio_pos;

	static bool initialized;
	static bool loaded;

public:
	static void init(void);
	static void load_wave(char*,int);
	static void play_wave(int);
	static void audio_callback(void*, Uint8*, int);
};
