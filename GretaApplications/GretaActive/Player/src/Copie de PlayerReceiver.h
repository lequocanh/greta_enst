//#ifndef SEMAINE_COMPONENTS_GRETA_PLAYERRECEIVER_H
//#define SEMAINE_COMPONENTS_GRETA_PLAYERRECEIVER_H

#include <semaine/config.h>

#include <cms/CMSException.h>

#include <semaine/components/Component.h>
#include <semaine/cms/receiver/Receiver.h>
#include "FramesManager.h"
#include "CommandsManager.h"
#include "CentralClock.h"

//semaine::components::Component

using namespace cms;
using namespace semaine::components;
using namespace semaine::cms::sender;
using namespace semaine::cms::receiver;

namespace semaine {
namespace components {
namespace greta {

class PlayerReceiver : public Component
{
public:
	PlayerReceiver(const std::string & componentName, bool isInput, bool isOutput,FramesManager* fm, CommandsManager* cm,CentralClock *pc) throw (CMSException);
	virtual ~PlayerReceiver();

	FramesManager* fm;
	CommandsManager* cm;
	CentralClock *pc;
	int sound_time;

protected:
	virtual void react(SEMAINEMessage * m) throw (CMSException);

private:
	Receiver * fapReceiver;
	Receiver * bapReceiver;
	Receiver * wavReceiver;
	Receiver * commandReceiver;

	std::string GretaName;
	int previous_time;

};

} // namespace greta
} // namespace components
} // namespace semaine