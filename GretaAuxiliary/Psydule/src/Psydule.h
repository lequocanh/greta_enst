//Copyright 1999-2009 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// Psydule.h: interface for the Psydule class.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "Communicator.h"
#include "Component.h"
#include "CppAIRPlug.h"
#include "CommunicationInterface.h"
#include "psyduleDefinitions.h"
#include "Logger.h"

#include <string>
#include <list>
#include <map>
#include <fstream>


using namespace cmlabs;

/**
*
* class :Psydule
*
*/

class Psydule : public CommunicationInterface
{
private:

	CppAIRPlug *plug1;
	
public:

	/**
	*
	* contructor 
	*
	* @param  name
	* @param  JString host
	* @param  int port
	*/

	Psydule(std::string name, std::string host, int port);

	/**
	*
	* destructor 
	*/

	virtual ~Psydule();


	/**
	* this method 
	* 
	*
	* @return 
	* @param  whiteboard
	* @param  JString datatype
	*/

	int Register(std::string whiteboard, std::string datatype);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  rawregistrationstring
	*/

	int RawRegister(std::string rawregistrationstring);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  whiteboard
	* @param  std::list<JString> datatypes
	*/

	int Register(std::string whiteboard, std::list<std::string> datatypes);
	int RegisterSender(std::map<std::string, std::string> sendertypename);
	long long get_time();

	std::string getGretaName(std::string GretaName);
	void setIsInputisOutput(bool isInput, bool isOutput){};

	/**
	* this method 
	* 
	*
	* @return 
	* @param  filename
	* @param  JString whiteboard
	* @param  JString datatype
	*/

	int PostFile(std::string filename, std::string whiteboard, std::string datatype);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  tosend
	* @param  JString whiteboard
	* @param  JString datatype
	*/

	int PostString(std::string tosend, std::string whiteboard, std::string datatype);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  timeout
	*/

	std::string ReceiveString(int timeout);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  timeout
	*/

	CommunicationMessage* ReceiveMessage(int timeout);
	
	/**
	* this method 
	* 
	*
	* @return 
	* @param  filename
	* @param msg
	*/

	int WriteBinaryFile(std::string filename, CommunicationMessage *msg);


	/**
	* this method 
	* 
	*
	* @return 
	* @param  fapfilename
	* @param  JString whiteboard
	* @param  JString datatype
	*/

	int Psydule::SendFAPs(std::string fapfilename, std::string whiteboard, std::string datatype);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  bapfilename
	* @param  JString whiteboard
	* @param  JString datatype
	*/

	int SendBAPs(std::string bapfilename, std::string whiteboard, std::string datatype);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  binfilename
	* @param  JString whiteboard
	* @param  JString datatype
	*/

	int SendBinaryFile(std::string binfilename, std::string whiteboard, std::string datatype);

	/**
	* this method 
	* 
	*
	* @return 
	* @param  tosend
	* @param  JString whiteboard
	* @param  JString datatype
	* @param  size
	*/

	int PostSpeechFromChar(char* tosend, std::string whiteboard, std::string datatype,int * size);

	Logger *log;

};
