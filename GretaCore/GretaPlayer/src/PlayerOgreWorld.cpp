#include "PlayerOgreWorld.h"
#include "IniManager.h"
#include "XMLDOMParser.h"

extern IniManager inimanager;

PlayerOgreWorld::PlayerOgreWorld():ogreSceneManager(0),freeCamera(0),agent(0),listener(0){
#ifdef NDEBUG //releaze
	ogreRoot = new Ogre::Root("Plugins.cfg");
	Ogre::LogManager::getSingleton().setLogDetail(Ogre::LoggingLevel::LL_LOW); //ogre will be quiet
#else //debug
	ogreRoot = new Ogre::Root("Plugins_d.cfg");
#endif
	Ogre::RenderSystem *renderSystem;
	if(inimanager.GetValueInt("PLAYER_DIRECT3D9")>0)
		renderSystem = ogreRoot->getRenderSystemByName("Direct3D9 Rendering Subsystem");
	else
		renderSystem = ogreRoot->getRenderSystemByName("OpenGL Rendering Subsystem");
    ogreRoot->setRenderSystem(renderSystem);
    ogreRoot->initialise(false);
}

PlayerOgreWorld::~PlayerOgreWorld(){
}

void PlayerOgreWorld::initWorld(){

	ogreSceneManager = ogreRoot->createSceneManager("TerrainSceneManager");
	ogreRoot->addResourceLocation("./Ogre/media","FileSystem",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,true);
	worldRootNode = ogreSceneManager->getRootSceneNode()->createChildSceneNode("worldRoot",Ogre::Vector3(0,0,0));
	background = new Ogre::ColourValue(0,0,0);

	freeCamera = new PlayerOgreCamera(true,ogreSceneManager,worldRootNode,"FreeCamera");

	if(inimanager.GetValueInt("PLAYER_ONLYFACE")){
		freeCamera->setPosition(11, 13, 150);
		freeCamera->yaw(-0.6);
	}
	else{
		freeCamera->setPosition(0, 50, 260);
		freeCamera->yaw(-0.6);
	}
}

PlayerOgreCamera* PlayerOgreWorld::getCamera(std::string characterType, std::string cameraType){
	PlayerOgreAgent* a = NULL;
	if(characterType.compare("agent")==0)
		a = agent;
	if(characterType.compare("listener")==0)
		a = listener;
	if(a==NULL){
		if(freeCamera==0){
			freeCamera = new PlayerOgreCamera(true,ogreSceneManager,worldRootNode,"FreeCamera");
			freeCamera->setPosition(0, 50, 200);
			freeCamera->lookAt(0, 50, 0);
			freeCamera->yaw(-1.571);
		}
		return freeCamera;
	}
	if(cameraType.compare("eyes")==0)
		return a->getEyesCamera();
	return a->getAgentCamera();
}

void PlayerOgreWorld::initAgents(bool realtime){
	agentList = new PlayerOgreSetOfCharacter(ogreSceneManager, worldRootNode, realtime);
	agentList->addAgent(inimanager.GetValueString("CHARACTER_SPEAKER"));
	setCharacterSpeaker(inimanager.GetValueString("CHARACTER_SPEAKER"));
	agent->MoveAgent(0,0,-50);
	if(inimanager.GetValueInt("PLAYER_SHOWLISTENER")){
		agentList->addAgent(inimanager.GetValueString("CHARACTER_LISTENER"),"listener");
		setCharacterListener(inimanager.GetValueString("CHARACTER_LISTENER"));
		listener->MoveAgent(0,0,50);
		listener->getNode()->setOrientation(Ogre::Quaternion(Ogre::Degree(180),Ogre::Vector3(0,1,0)));
	}

}
void PlayerOgreWorld::setCharacterSpeaker(std::string characterName){
	setCharacter(&agent, characterName);
}
void PlayerOgreWorld::setCharacterListener(std::string characterName){
	setCharacter(&listener, characterName, "listener");
}

void PlayerOgreWorld::setCharacter(PlayerOgreAgent** theAgent, std::string characterName,std::string type){
	Ogre::Vector3 position(0,0,0);
	if((*theAgent) != NULL){
		(*theAgent)->setVisible(false);
		position = (*theAgent)->getNode()->getPosition();
	}
	PlayerOgreAgent* newAgent = agentList->getAgent(characterName,type);
	if(newAgent != NULL){
		if((*theAgent) != NULL) {
			newAgent->getNode()->setOrientation(
				(*theAgent)->getNode()->getOrientation());
			newAgent->getNode()->setScale(
				(*theAgent)->getNode()->getScale());
		}
		(*theAgent) = newAgent;
		(*theAgent)->getNode()->setPosition(position);
	}
	if((*theAgent) != NULL) (*theAgent)->setVisible(true);
}


void PlayerOgreWorld::update(){
	if(agent!=0) agent->update();
	if(listener!=0) listener->update();
}

void PlayerOgreWorld::setScene(std::string sceneName){
	std::string filename = inimanager.Program_Path+"Ogre/scene/"+sceneName+".xml";

	XMLDOMParser parserXML;
	parserXML.SetValidating(true);

	XMLGenericTree* sceneRoot = parserXML.ParseFile(filename);
	if(sceneRoot == NULL){
		printf("Warning : probleme with loading file %s\n",filename.c_str());
		sceneRoot = parserXML.ParseFile(inimanager.Program_Path+"Ogre/scene/empty.xml");
	}
	int light_count=0;
	int object_count=0;
	for(XMLGenericTree::iterator iter=sceneRoot->begin();iter!=sceneRoot->end();++iter){
		XMLGenericTree* child = (*iter);
		// agent's position, orientation and scaling :
		if(child->GetName() == "agent"){
			setObjectSetting(agent->getNode(),
				child->FindNodeCalled("position"),
				child->FindNodeCalled("orientation"),
				child->FindNodeCalled("scale"));
		}
		// listener's position, orientation and scaling :
		else if(child->GetName() == "listener" && listener != NULL){
			setObjectSetting(listener->getNode(),
				child->FindNodeCalled("position"),
				child->FindNodeCalled("orientation"),
				child->FindNodeCalled("scale"));
		}
		// lights :
		else if(child->GetName() == "ambient_light")
			ogreSceneManager->setAmbientLight(Ogre::ColourValue(child->GetAttributef("r"),child->GetAttributef("g"),child->GetAttributef("b")));
		else if(child->GetName() == "light"){
			std::ostringstream light_name;
			light_name << "Light_" << light_count;
			Ogre::Light* l = ogreSceneManager->createLight(light_name.str());
			worldRootNode->attachObject(l);
			l->setPosition(child->GetAttributef("x"),child->GetAttributef("y"),child->GetAttributef("z"));
			XMLGenericTree* diffuse = child->FindNodeCalled("diffuse");
			l->setDiffuseColour(diffuse->GetAttributef("r"), diffuse->GetAttributef("g"), diffuse->GetAttributef("b"));
			XMLGenericTree* specular = child->FindNodeCalled("specular");
			l->setSpecularColour(specular->GetAttributef("r"), specular->GetAttributef("g"), specular->GetAttributef("b"));
			++light_count;
		}
		// objects :
		else if(child->GetName() == "object"){
			std::ostringstream node_name,object_name;
			node_name << "Object_Node_" << object_count;
			object_name << "Object_" << object_count;
			Ogre::SceneNode* objectNode = worldRootNode->createChildSceneNode(node_name.str());
			Ogre::Entity* object = ogreSceneManager->createEntity(object_name.str(), child->GetAttribute("mesh"));
			if(child->GetAttribute("material") != "")
				object->setMaterialName(child->GetAttribute("material"));
			objectNode->attachObject(object);
			setObjectSetting(objectNode,
				child->FindNodeCalled("position"),
				child->FindNodeCalled("orientation"),
				child->FindNodeCalled("scale"));
			++object_count;
		}
		// shadow :
		else if(child->GetName() == "shadow"){
			ogreSceneManager->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
			ogreSceneManager->setShadowTextureSize(1024);
			ogreSceneManager->setShadowColour(Ogre::ColourValue(
				child->GetAttributef("r"),
				child->GetAttributef("g"),
				child->GetAttributef("b")));
			ogreSceneManager->setShadowDirLightTextureOffset(0.00001);
			ogreSceneManager->setShadowFarDistance(100000000);
		}
		// sky : 
		else if(child->GetName() == "sky" ){
			ogreSceneManager->setSkyDome(true, 
				child->GetAttribute("material"), 
				atoi(child->GetAttribute("curve").c_str()), 
				atoi(child->GetAttribute("tilling").c_str()),
				atoi(child->GetAttribute("distance").c_str()));
		}
		// fog;
		else if(child->GetName() == "fog"){
			ogreSceneManager->setFog(Ogre::FOG_LINEAR, 
				Ogre::ColourValue(child->GetAttributef("r"),child->GetAttributef("g"),child->GetAttributef("b")),
				0,child->GetAttributef("start"),child->GetAttributef("end"));
		}
		// background colour :
		else if(child->GetName() == "background_color"){
			setBackgroundColour(child->GetAttributef("r"), child->GetAttributef("g"), child->GetAttributef("b"));
		}
		// terrain :
		else if(child->GetName() == "terrain"){
			ogreRoot->addResourceLocation(child->GetAttribute("resource_path"),"FileSystem",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,true);
			ogreSceneManager->setWorldGeometry(child->GetAttribute("file"));
			worldRootNode->setPosition(
				-child->GetAttributef("position_x"),
				-child->GetAttributef("position_y"),
				-child->GetAttributef("position_z"));

		}
	}
}

void PlayerOgreWorld::setObjectSetting(Ogre::SceneNode* objectNode, XMLGenericTree* position, XMLGenericTree* orientation, XMLGenericTree* scale){
			objectNode->setPosition(position->GetAttributef("x"),position->GetAttributef("y"),position->GetAttributef("z"));
			Ogre::Quaternion q = 
				Ogre::Quaternion(Ogre::Degree(orientation->GetAttributef("z")),Ogre::Vector3(0,0,1)) *
				Ogre::Quaternion(Ogre::Degree(orientation->GetAttributef("y")),Ogre::Vector3(0,1,0)) * 
				Ogre::Quaternion(Ogre::Degree(orientation->GetAttributef("x")),Ogre::Vector3(1,0,0));
			objectNode->setOrientation(q);
			objectNode->setScale(scale->GetAttributef("x"),scale->GetAttributef("y"),scale->GetAttributef("z"));
			
}
