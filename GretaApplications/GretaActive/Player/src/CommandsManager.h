#pragma once

#include <vector>
#include "JThread.h"
#include "JSemaphore.h"
#include "FAPFrame.h"
#include "BAPframe.h"
#include "CentralClock.h"

//using namespace cmlabs;

class RTPlayerCommand
{
public:
	bool operator<(RTPlayerCommand& c){if(this->time<c.time)return true; else return false;}
	std::string name;
	std::string arguments;
	int time;
};

class CommandsManager
{
public:

	CommandsManager(CentralClock *pc);
	virtual ~CommandsManager();
	

	void AddCommand(std::string name,std::string arguments);
	bool GetCommand(std::string &returnc);

private:
	CentralClock *pc;

	cmlabs::JMutex mutex;

	std::vector<RTPlayerCommand> commands;
};