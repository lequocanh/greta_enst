/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gretaTests;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Andre
 */
public class TestFrame extends JFrame{

    static String greta_path = ".";
    static String FMLDirectory = "fml";
    static String BMLDirectory = "bml";

    static String frameName = "Greta tests";
    static Font plain = new Font("Arial", Font.PLAIN, 12);
    static Font bold = new Font("Arial", Font.BOLD, 13);
    static Dimension buttonSize = new Dimension(125,25);

    static ArrayList<TestParams> globalTests;
    static ArrayList<TestParams> supplementaryEmotion;
    static ArrayList<TestParams> supplementaryBackcannel;
    static ArrayList<TestParams> supplementaryFunction;
    static ArrayList<TestParams> supplementarySpeech;
    static ArrayList<TestParams> supplementaryFace;
    static ArrayList<TestParams> supplementaryHead;
    static ArrayList<TestParams> supplementaryGesture;
    static ArrayList<TestParams> supplementaryGaze;
    static ArrayList<TestParams> supplementaryTorso;
    static final int NOFILE = 0;
    static final int FML = 1;
    static final int BML = 2;


    ArrayList<Sender> senders = new ArrayList<Sender>();
    
    ArrayList<TestParams> currentList; //pointer to a list of test
    int currentIndex=-1;
    int currentFileType = NOFILE;

    private JButton globalTestsButton;
    private JButton supplementaryEmotionButton;
    private JButton supplementaryBackcannelButton;
    private JButton supplementaryFunctionButton;
    private JButton supplementarySpeechButton;
    private JButton supplementaryGazeButton;
    private JButton supplementaryFaceButton;
    private JButton supplementaryHeadButton;
    private JButton supplementaryGestureButton;
    private JButton supplementaryTorsoButton;
    private JButton next;
    private JButton last;
    private JButton send;
    private JButton menu;
    private JPanel textPanel;
    private JTextArea message;
    private JPanel startPanel;
    private String fileTestDef = "LIST.xml";


    public TestFrame(){
        super(frameName);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 250);
        setLocation(200, 100);
        setResizable(false);
        initTests();

        //start Panel :
        startPanel = new JPanel();
        startPanel.setBackground(getBackground());
        startPanel.setLayout(new GridLayout(5,2));

        globalTestsButton = new JButton("General Tests");
        globalTestsButton.setFont(bold);
        globalTestsButton.setPreferredSize(buttonSize);
        globalTestsButton.setForeground(new Color(150,0,0));
        globalTestsButton.addActionListener(new TestStarter(globalTests));

        supplementaryEmotionButton = new JButton("Emotions");
        supplementaryEmotionButton.setFont(plain);
        supplementaryEmotionButton.setPreferredSize(buttonSize);
        supplementaryEmotionButton.addActionListener(new TestStarter(supplementaryEmotion));

        supplementaryBackcannelButton = new JButton("BackChannels");
        supplementaryBackcannelButton.setFont(plain);
        supplementaryBackcannelButton.setPreferredSize(buttonSize);
        supplementaryBackcannelButton.addActionListener(new TestStarter(supplementaryBackcannel));

        supplementaryFunctionButton = new JButton("FML Functions");
        supplementaryFunctionButton.setFont(plain);
        supplementaryFunctionButton.setPreferredSize(buttonSize);
        supplementaryFunctionButton.addActionListener(new TestStarter(supplementaryFunction));

        supplementarySpeechButton = new JButton("Speech");
        supplementarySpeechButton.setFont(plain);
        supplementarySpeechButton.setPreferredSize(buttonSize);
        supplementarySpeechButton.addActionListener(new TestStarter(supplementarySpeech));

        supplementaryGazeButton = new JButton("Gaze");
        supplementaryGazeButton.setFont(plain);
        supplementaryGazeButton.setPreferredSize(buttonSize);
        supplementaryGazeButton.addActionListener(new TestStarter(supplementaryGaze));
        
        supplementaryFaceButton = new JButton("Face");
        supplementaryFaceButton.setFont(plain);
        supplementaryFaceButton.setPreferredSize(buttonSize);
        supplementaryFaceButton.addActionListener(new TestStarter(supplementaryFace));
        
        supplementaryHeadButton = new JButton("Head");
        supplementaryHeadButton.setFont(plain);
        supplementaryHeadButton.setPreferredSize(buttonSize);
        supplementaryHeadButton.addActionListener(new TestStarter(supplementaryHead));
        
        supplementaryGestureButton = new JButton("Gesture");
        supplementaryGestureButton.setFont(plain);
        supplementaryGestureButton.setPreferredSize(buttonSize);
        supplementaryGestureButton.addActionListener(new TestStarter(supplementaryGesture));
        
        supplementaryTorsoButton = new JButton("Torso");
        supplementaryTorsoButton.setFont(plain);
        supplementaryTorsoButton.setPreferredSize(buttonSize);
        supplementaryTorsoButton.addActionListener(new TestStarter(supplementaryTorso));
        
        JPanel temp = new JPanel();
        temp.add(globalTestsButton); //ligne 1 column 1
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryGazeButton);//ligne 1 column 2
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryEmotionButton);//ligne 2 column 1
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryFaceButton);//ligne 2 column 2
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryBackcannelButton);//ligne 3 column 1
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryHeadButton);//ligne 3 column 2
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryFunctionButton);//ligne 4 column 1
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryGestureButton);//ligne 4 column 2
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementarySpeechButton);//ligne 5 column 1
        startPanel.add(temp);
        temp = new JPanel();
        temp.add(supplementaryTorsoButton);//ligne 5 column 2
        startPanel.add(temp);

        //Message Panel :
        message = new JTextArea();
        message.setLineWrap(true);
        message.setEditable(false);
        message.setAutoscrolls(true);
        message.setBackground(getBackground());
        textPanel = new JPanel();
        textPanel.setLayout(new GridLayout(1,1));
        textPanel.add(message);

        //Button Panel :
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBackground(getBackground());
        menu = new JButton("Menu");
        menu.setFont(plain);
        menu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                currentIndex = 0;
                updatePanel();
            }
        });
        buttonPanel.add(menu);

        last = new JButton("<<");
        last.setFont(plain);
        last.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                displayLast();
            }
        });
        buttonPanel.add(last);

        send = new JButton("Send");
        send.setFont(plain);
        send.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                boolean isBML = currentFileType == BML;
                boolean isFML = currentFileType == FML;
                if((!isBML && !isFML))
                    return;
                String XML = loadFile(currentList.get(currentIndex).name,isFML);
                if(isBML)
                    sendBML(XML);
                if(isFML)
                    sendFML(XML);
            }
        });
        buttonPanel.add(send);

        next = new JButton(">>");
        next.setFont(plain);
        next.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                displayNext();
            }
        });
        buttonPanel.add(next);
        add(buttonPanel,BorderLayout.SOUTH);

        displayNext();
    }

    private void initTests(){
        //Starts
        globalTests = new ArrayList<TestParams>();
        addTest(globalTests,"","start","","");
        supplementaryEmotion = new ArrayList<TestParams>();
        addTest(supplementaryEmotion,"","start","","");
        supplementaryBackcannel = new ArrayList<TestParams>();
        addTest(supplementaryBackcannel,"","start","","");
        supplementaryFunction = new ArrayList<TestParams>();
        addTest(supplementaryFunction,"","start","","");
        supplementarySpeech = new ArrayList<TestParams>();
        addTest(supplementarySpeech,"","start","","");
        supplementaryFace = new ArrayList<TestParams>();
        addTest(supplementaryFace,"","start","","");
        supplementaryHead = new ArrayList<TestParams>();
        addTest(supplementaryHead,"","start","","");
        supplementaryGesture = new ArrayList<TestParams>();
        addTest(supplementaryGesture,"","start","","");
        supplementaryGaze = new ArrayList<TestParams>();
        addTest(supplementaryGaze,"","start","","");
        supplementaryTorso = new ArrayList<TestParams>();
        addTest(supplementaryTorso,"","start","","");

        //content (read in a file)
        readTests();

        //ends
        addTest(globalTests,"","end","\n\tGeneral tests are done.","General");
        addTest(supplementaryEmotion,"","end","\n\tTests about emotions are done.","Emotion");
        addTest(supplementaryBackcannel,"","end","\n\tTests about backchannels are done.","Backchannel");
        addTest(supplementaryFunction,"","end","\n\tTests about communicative functions are done.","Functions");
        addTest(supplementarySpeech,"","end","\n\tTests about speech are done.","Speech");
        addTest(supplementaryFace,"","end","\n\tTests about face are done.","Face");
        addTest(supplementaryHead,"","end","\n\tTests about head are done.","Head");
        addTest(supplementaryGesture,"","end","\n\tTests about gestures are done.","Gesture");
        addTest(supplementaryGaze,"","end","\n\tTests about gaze are done.","Gaze");
        addTest(supplementaryTorso,"","end","\n\tTests about torso are done.","Torso");

        currentList = globalTests;
    }
    private static void addTest(ArrayList<TestParams> list,String filename, String type, String message, String parentList){
        list.add(new TestParams(filename,type,message,parentList));
    }

    public void displayNext(){
        if(currentIndex<currentList.size()-1){
            ++currentIndex;
            updatePanel();
        }
        else
            currentIndex = currentList.size()-1;
    }
    public void displayLast(){
        if(currentIndex>0){
            --currentIndex;
            updatePanel();
        }
        else
            currentIndex = 0;
    }
    private void updatePanel(){
        message.setText(currentList.get(currentIndex).message);

        //update file type
        if(currentList.get(currentIndex).type.equalsIgnoreCase("BML"))
            currentFileType = BML;
        else {
            if(currentList.get(currentIndex).type.equalsIgnoreCase("FML"))
                currentFileType = FML;
            else
                currentFileType = NOFILE;
        }
        //update send button
        send.setEnabled(currentFileType != NOFILE);

        //update bounds panels
        if(currentList.get(currentIndex).type.equalsIgnoreCase("start")){
            add(startPanel, BorderLayout.CENTER);
            startPanel.setVisible(true);
            textPanel.setVisible(false);
            menu.setEnabled(false);
            next.setEnabled(false);
            last.setEnabled(false);
            setTitle(frameName);
        }
        else{
            add(textPanel, BorderLayout.CENTER);
            startPanel.setVisible(false);
            textPanel.setVisible(true);
            menu.setEnabled(true);
            last.setEnabled(true);
            next.setEnabled( ! currentList.get(currentIndex).type.equalsIgnoreCase("end"));
            setTitle(frameName+" - "+currentList.get(currentIndex).list+" ("+currentIndex+"/"+(currentList.size()-1)+")");
        }
    }

    public void connect(){
        for(Sender sender : senders)
            sender.connect();
    }
    public void sendFML(String FML){
        for(Sender sender : senders){
            sender.sendFML(FML);
        }
    }
    public void sendBML(String BML){
        for(Sender sender : senders){
            sender.sendBML(BML);
        }
    }

    private void readTests(){
        readTests(fileTestDef);
    }

    private static void readTests(String file){
        boolean inTestListsMarkup = false;
        ArrayList<TestParams> list = null;
        String testFile = "",
               testType = "",
               testDescription = "";
        int fileIndex, typeIndex, descIndex;
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String line;
            String listName = "General";
            while((line = in.readLine()) != null){
                if(!inTestListsMarkup && line.contains("<lists") && (!line.contains("/>")))
                    inTestListsMarkup = true;
                if(inTestListsMarkup){
                    if(line.contains("</lists"))
                        inTestListsMarkup = false;
                    if(list == null){
                        if(line.contains("<general")){
                            list = globalTests;
                            listName = "General";
                        }
                        if(line.contains("<emotions")){
                            list = supplementaryEmotion;
                            listName = "Emotion";
                        }
                        if(line.contains("<backchannels")){
                            list = supplementaryBackcannel;
                            listName = "Backcannel";
                        }
                        if(line.contains("<FMLFunctions")){
                            list = supplementaryFunction;
                            listName = "Functions";
                        }
                        if(line.contains("<speech")){
                            list = supplementarySpeech;
                            listName = "Speech";
                        }
                        if(line.contains("<face")){
                            list = supplementaryFace;
                            listName = "Face";
                        }
                        if(line.contains("<head")){
                            list = supplementaryHead;
                            listName = "Head";
                        }
                        if(line.contains("<gestures")){
                            list = supplementaryGesture;
                            listName = "Gesture";
                        }
                        if(line.contains("<gaze")){
                            list = supplementaryGaze;
                            listName = "Gaze";
                        }
                        if(line.contains("<torso")){
                            list = supplementaryTorso;
                            listName = "Torso";
                        }
                    }
                    else{
                        if(line.contains("</general") ||
                           line.contains("</emotions") ||
                           line.contains("</backchannels") ||
                           line.contains("</FMLFunctions") ||
                           line.contains("</speech") ||
                           line.contains("</face") ||
                           line.contains("</head") ||
                           line.contains("</gestures") ||
                           line.contains("</gaze") ||
                           line.contains("</torso"))
                            list = null;
                        else{
                            if(line.contains("<test ")){
                                String[] splited = line.split("\"");
                                fileIndex = -1;
                                typeIndex = -1;
                                descIndex = -1;
                                for(int i=0;i<splited.length;++i){
                                    if(splited[i].contains(" file="))
                                        fileIndex = i;
                                    if(splited[i].contains(" type="))
                                        typeIndex = i;
                                    if(splited[i].contains(" description="))
                                        descIndex = i;
                                }
                                if(fileIndex > -1 && fileIndex < splited.length-1)
                                    testFile = splited[fileIndex+1];
                                else
                                    testFile = "";

                                if(typeIndex > -1 && typeIndex < splited.length-1)
                                    testType = splited[typeIndex+1];
                                else
                                    testType = "";

                                if(descIndex > -1 && descIndex < splited.length-1){
                                    testDescription = splited[descIndex+1];
                                    testDescription = testDescription.replaceAll("\\\\t", "\t");
                                    testDescription = testDescription.replaceAll("\\\\n", "\n");
                                }
                                else
                                    testDescription = "";

                                addTest(list, testFile, testType, testDescription, listName);
                            }
                        }

                    }
                }
            }
            in.close();
        }
        catch(Exception e) {
            System.out.println(e);
        }


    }
    
    public static String loadFile(String fileName, boolean isFML){
        String fileContent = "";
        String completeFileName = greta_path+"/"+fileName;
        String temp = "";
        if(isFML)
            temp = "\"fml.dtd\"";
        else
            temp = "\"bml.dtd\"";
        try {
            BufferedReader in = new BufferedReader(new FileReader(completeFileName));
            String line;
            while((line = in.readLine()) != null){
                if(line.contains(temp)){
                    if(isFML)
                        line=line.replace(temp, "\""+FMLDirectory+"/fml.dtd\"" );
                    else
                        line=line.replace(temp, "\""+BMLDirectory+"/bml.dtd\"" );
                }
                fileContent+=line+"\n";
            }
            in.close();
        }
        catch(Exception e) {
            System.out.println(e);
        }

        return fileContent;
    }

    private class TestStarter implements ActionListener{
        private ArrayList<TestParams> testList;
        TestStarter(ArrayList<TestParams> list){
            testList = list;
        }
        public void actionPerformed(ActionEvent e) {
            currentList = testList;
            connect();
            displayNext();
        }
    }

}
