// Shared-Attention system
// Christopher Peters

#ifndef _CALIBRATION_H
#define _CALIBRATION_H

#include <list>

#include <Fl/Fl.h>
#include <FL/Gl.h>
#include <Gl/Glu.h>

#include "sharedattention.h"

/*
	bool isCalibrateModeEnabled;			//is it in calibrate mode? true = yes
	struct
	{
		int cursorPosList[9];
		int currentCursorPos;
		float timeLookedAt;

		float screenTopAngle;		//angle for top of screen and bottom of screen
		float screenBottomAngle;
		
		float screenLeftAngle;		//angle for left of screen and right of screen
		float screenRightAngle;
	} CalibrationDetails;
*/

class Calibrator
{
  public:
	Calibrator();
	~Calibrator();

	void reset();
	void enable(void);
	void startRecording(long currentTime);
	void addEntry(float headXDir, float headYDir);
	void update(long currentTime);
	BoundaryValues calcBoundaryValues(void);

	long startTime;
	bool isCalibrateModeEnabled;
	int currentCursorPosition;
	int currentCursorState;
	int timePerTrial;
	bool calibrated;

	std::list<float> topHeadCoords;
	std::list<float> leftHeadCoords;
	std::list<float> rightHeadCoords;
	std::list<float> bottomHeadCoords;
};

#endif