//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ConstraintContainer.h: interface for the ConstraintContainer class.
//
//////////////////////////////////////////////////////////////////////
//modifications:
//
#pragma once

#include "MultimodalEmotionConstraint.h"
#include "XMLGenericTree.h"

#include <string>

/**
* class :ConstraintsContainer
*
*/

class NewConstraintsContainer 
{

private:

	MultimodalEmotionConstraints mecs ;


	/**
	*  
	* translates a parses node to constraint node
	*
	* @return a tree of constraints
	* @param  a node of parsing tree
	*/

	ConsNode* NewConstraintsContainer::translate(XMLGenericTree *con_iter);

public:


	/**
	* contructor 
	*
	*/

	NewConstraintsContainer ();

	/**
	*  
	* parses behavior and constraint sets 
	*
	* @return 
	* @param  file_name
	*/

	int init (std::string file_name);

	/**
	* destructor 
	*/

	virtual ~NewConstraintsContainer ();

	/**
	*  
	* 
	*
	* @return 
	*/

	MultimodalEmotionConstraints getMultimodalEmotionConstraints();

	/**
	*  
	* 
	*
	*/

	void printAll();

	/**
	*  
	* 
	*
	* @return 
	* @param  emotion
	* @param  std::string signalname
	*/

	float getRepetivity(std::string emotion, std::string signalname);

	/**
	*  
	* 
	*
	* @return 
	* @param  emotion
	* @param  std::string signalname
	*/

	float getProbability_start(std::string emotion, std::string signalname);

	/**
	*  
	* 
	*
	* @return 
	* @param  emotion
	* @param  std::string signalname
	*/

	float getProbability_end(std::string emotion, std::string signalname);

	/**
	*  
	* 
	*
	* @return 
	* @param  emotion
	* @param  std::string signalname
	*/

	float getOccurence(std::string emotion, std::string signalname);

	/**
	*  
	* 
	*
	* @return 
	* @param  emotion
	* @param  std::string signalname
	*/

	float getMax_duration(std::string emotion, std::string signalname);

	/**
	*  
	* 
	*
	* @return 
	* @param  emotion
	*/

	MultimodalEmotionConstraint* getMultimodalEmotionConstraint(std::string emotion);

	
	

};

