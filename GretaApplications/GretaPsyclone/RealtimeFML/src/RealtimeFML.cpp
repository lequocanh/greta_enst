//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// RealtimeFML.cpp: implementation of the RealtimeFML class.
//
//////////////////////////////////////////////////////////////////////

#include "DataContainer.h"
#include "RealtimeFML.h"
#include "RandomGen.h"
#include "FileNames.h"

#include <string>
#include <vector>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;


extern DataContainer *datacontainer;
extern IniManager inimanager;
extern FileNames filenames;

RandomGen *rando;

//timeat 1000?
int DELAY = 1000;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RealtimeFML::RealtimeFML(std::string name, std::string host, int port, std::string GretaName) : Psydule(name, host, port)
{

	// registered packet list
	std::list<std::string> registeredGretaDatatypes;

	registeredGretaDatatypes.push_back(GretaName+CLOCK_MESSAGE_HEADER);
	registeredGretaDatatypes.push_back(GretaName+ACTION_SELECTED_FML);
	registeredGretaDatatypes.push_back(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER);

	// registration GretaWiteboard
	if(Register(GretaName+GRETA_WHITEBOARD,registeredGretaDatatypes)!=0)
	{
		std::cout << GretaName+REALTIMEFML_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_OK << std::endl;		
	}
	else
	{
		std::cout << GretaName+REALTIMEFML_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_FAILED<< std::endl;
		exit(0);
	}
	std::cout << "Registration succesful" << std::endl;
	
	this->GretaName=getGretaName(GretaName);


	pc=new CentralClock();


	agentname="";
	
	// Agent Engine is created once!!
	faae=new FMLAPML_AgentEngine();
	
	while(true)
	{
		Sleep(5);

		CommunicationMessage *msg_last;
		CommunicationMessage msg_fml, msg_clock, msg_agent;
		msg_last=ReceiveMessage(10);
		ReadMessage(msg_last);

		std::list<GretaLogger*>::iterator iterlog;

		for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
		{
			std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
			if((*iterlog)->getLevel()=="debug")
				this->log->debug(msg);
			if((*iterlog)->getLevel()=="info")
				this->log->info(msg);
			if((*iterlog)->getLevel()=="warn")
				this->log->warn(msg);
			if((*iterlog)->getLevel()=="error")
				this->log->error(msg);
		}
		listLog.clear();

	}
}

RealtimeFML::~RealtimeFML()
{
	delete faae;
}

int RealtimeFML::ReadMessage(CommunicationMessage *msg)
{
	std::string content, type;
	
	if(msg==NULL) return(0);

	content=msg->getContent();
	type=msg->getType();
	
	if(strcmp(content.c_str(),"")!=0)
	{
		std::string app=content;

		if(strcmp(msg->getType().c_str(),(GretaName+CLOCK_MESSAGE_HEADER).c_str())==0)
		{
			int time;
			time=atoi(msg->getContent().c_str());		
			pc->SetTime(time);
		}
	
		if(strcmp(type.c_str(),(GretaName+ACTION_SELECTED_FML/*FML_MESSAGE_HEADER*/).c_str())==0)
		{
			std::string bml;
			std::string time;
			int starttime=0;
			std::string fml=content;
			bml=CalculateFMLAnimation(content);
			//printf("%s",bml.c_str());
		
			SendBML(bml);
		}	
		else
		if(strcmp(type.c_str(),(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER).c_str())==0)
		{
			if(app.substr(0,app.find_first_of("="))=="agent_identity.agent.name")
			{
				agentname=app.substr(app.find_first_of("=")+1);
			}
		}
		return 1;
	}
	else
		return 0;
}

std::string RealtimeFML::CalculateFMLAnimation(std::string content)
{
	std::string bml;
	std::string baseline;
	if(agentname!="")
		baseline="mmsystem/" + agentname +".xml";
	else
	{
		int pos=(int)(inimanager.GetValueString("BASELINE_DEFAULT").find("mmsystem"));
		if (pos!=std::string::npos)
			baseline=inimanager.GetValueString("BASELINE_DEFAULT").substr(pos,inimanager.GetValueString("BASELINE_DEFAULT").length());
		else
			baseline="mmsystem/baseline_default.xml";
	}
	bml=faae->Execute("", baseline.c_str(), (char*)content.c_str(), 1);

	printf("%s\n", bml.c_str());
 
	return(bml);
}

void RealtimeFML::SendBML(std::string bml)
{
	//printf("BML\n%s\n\n", bml.c_str());
	if (bml!="")
		PostString(bml,GretaName+GRETA_WHITEBOARD,GretaName+BML_MESSAGE_HEADER);
}
