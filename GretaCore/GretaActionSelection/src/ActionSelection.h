//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// ActionSelection.h: code for adapting and choosing the right BCs.
//
// Author: Etienne de Sevin etienne.de-sevin@telecom-paristech.fr
//
//////////////////////////////////////////////////////////////////////
#pragma once

//#include <iostream>

#include "NInterface.h"
#include "RandomGen.h"
#include <String>

class ActionSelection 
{
public:

	/** 
    * Class default contructor.
    * 
    */
	ActionSelection();
	/** 
    * Class destructor.
    * 
    */
	virtual ~ActionSelection();

	std::string ActionSelection::choice(); //CentralClock *pc);
	/** 
    * Receive informations from the Backchannels
    * 
    */
	bool ActionSelection::receiveBC();
	/** 
    * Receive informations from the Mimicry
    * 
    */
	bool ActionSelection::receiveM();
	/** 
    * Receive informations from the Cognitive BC
    * 
    */
	bool ActionSelection::receiveC();
	/** 
    * draw the NI in real time
    * 
    */
	//void ActionSelection::drawNI();
	/** 
    * define the phase of the interaction
    * 
    */
	void ActionSelection::Phase();
	/** 
    * anticipation of the NIU
    * 
    */
	void ActionSelection::anticipationNIU();
	/** 
    * probalility of BC
    * 
    */
	void ActionSelection::probabilityBC();
	/** 
    * influence of personality on BC
    * 
    */
	void ActionSelection::personalityBC();

	static float ActionSelection::normalize(float value, float minI1, float maxI1, float minI2=0.0, float maxI2=1.0);

	std::string ActionSelection::getZone();
	void ActionSelection::setZone(std::string zone);
	float ActionSelection::getPriorityRule();
	void ActionSelection::setPriorityRule(float priorityRule);
	float ActionSelection::getPriority();
	void ActionSelection::setPriority(float priority);
	std::string ActionSelection::getBCtype();
	void ActionSelection::setBCtype(std::string BCtype);
	float ActionSelection::getNIU();
	void ActionSelection::setNIU(float NIU);
	std::string ActionSelection::getContent();
	void ActionSelection::setContent(std::string content);
	std::string ActionSelection::getTypeChosen();
	void ActionSelection::setTypeChosen(std::string typeChosen);
	std::string ActionSelection::getBMLFMLsend();
	void ActionSelection::setBMLFMLsend(std::string BMLFMLsend);
	float ActionSelection::getStability();
	void ActionSelection::setStability(float stable);
	float ActionSelection::getExtroversion();
	void ActionSelection::setExtroversion(float extro);
	int ActionSelection::getNchosen();
	void ActionSelection::setNchosen(int num);
	float ActionSelection::getMoyNIU();
	void ActionSelection::setMoyNIU(float moni);
	const char* ActionSelection::getanticipNIU();
	void ActionSelection::setanticipNIU(const char* antni);
	const char* ActionSelection::getinterPhase();
	void ActionSelection::setinterPhase(const char* inph);
	const char* ActionSelection::getactchosen();
	void ActionSelection::setactchosen(const char* actch);
	float ActionSelection::getAgentArousal();
	void ActionSelection::setAgentArousal(float agentArous);
	float ActionSelection::getUserArousal();
	void ActionSelection::setUserArousal(float userArous);
	float ActionSelection::getAgentValence();
	void ActionSelection::setAgentValence(float agentValen);
	float ActionSelection::getUserValence();
	void ActionSelection::setUserValence(float userValen);
		
	RandomGen *randgen;


private :

	std::string zone;
	float priorityRule;
	std::string BCtype;
	int speaking;
	float NIU;
	float probBC;
	float probM;
	float probC;
	int waiting;
	std::string content;
	std::string BMLFMLsend;
	std::string typeChosen;
	std::string old_typeChosen;
	std::string old_zoneBC;
	std::string old_zoneM;
	std::string bml;
	std::string fml;
	std::string type;
	const char* anticipNIU;
	const char* interPhase;
	const char* actchosen;
	std::string behaviour_old[15];
	std::string candidate_actions[50];
	std::string candidate_actions_zone[50];
	std::string storeFMLBML[50];
	float candidate_actions_priority[50];
	float hysteresis;
	float old_NIU;
	float agentstate_buffer;
	float average_NUI;
	float som_NUI;
	float randomvalue;
	float variationNIU;
	float satisfactionNIU;
	float varNIU;
	float satisNIU;
	float averageNI;
	int skipped;
	int il;
	int it;
	int numend;
	float MoyNI;
	int iter_action;
	int iter_react;
	int iter_NI;
	int iter_satis;
	int iprob;
	int anticiploop;
	int satisfactionloop;
	int inFront;
	int beginning;
	int leavingFront;
	int number_chosen;
	time_t startD;
	time_t endD;
	time_t startEnding;
	time_t endEnding;
	double difEnding;
	double difD;
	float priority;
	float calc_priority;
	float timeChosen;
	float randomBC;
	int iterview;
	int printoutput;
	double hyst;
	float extroverted;
	float stability;
	float userValence;
	float userArousal;
	float agentValence;
	float agentArousal;

	//ofstream output(".\\decision\\NI.txt");

};




