SEMAINE VISUAL FEATURE EXTRACTOR AND ANALYSER COMPONENTS

Semaine visual feature extractor components consist of FaceDetector, 2DGlobalHeadMotionEstimator and FacialPointDetector. Semaine analyser components based on visual features extracted consist of NodShakeAnalyser and ArousalValenceAnalyser. 
These components are available as an executable functioning as part of the overall SEMAINE system 2.0. They come as one installable executable for all components that rely on visual information processing and analysis.
In addition to the general Semaine system setup, the visual feature extractors and analysers require the following:
1.	Windows Vista 

2. A camera with the following specifications:
	2.1. An Allied Vision Technologies IEEE-1394 firewire camera (e.g., Stingray) compatible with Windows Vista and OpenCV 2.0. This requires the camera to be recognized in the Device Manager as 1394 Digital Camera which can be done by downloading and installing the CMU 1394 Digital Camera  Driver (http://www.cs.cmu.edu/~iwan/1394/). If after the installation the camera is not recognized as CMU 1394 Digital Camera:
 a)	The IEEE 1394 Bus host controller named RICOH OHCI Compliant IEEE 1394 host controller should be uninstalled;
 b)	�Scan for hardware changes� should be done;
 c)	In the imaging devices list, there should be now a 1394 Digital Camera. The driver for this camera should be updated by choosing the CMU 1394 Digital Camera Driver as the location for this update.      
	
	OR
	
	2.2. A Windows Vista and OpenCV 2.0 compatible webcam such as Microsoft LifeCam VX-5000. 

 
3.	The provided config file needs to be modified to incorporate the folder for the trained models. The models should be placed in that directory.

4.	The frequency of the EMMA message containing information regarding the arousal and valence values can be set (in terms of number of video frames) by modifying the variable winAV in the config file. The default values has been set to winAV=30.

SETTING UP THE CONFIG FILE

�	visualisation: Whether the facial feature extraction should be displayed to the user or not. DEFAULT is, visualisation =1, facial feature extraction should be displayed/visualised. visualisation =0 facial feature extraction should not be displayed/visualised
�	framerate: The frame rate of the camera used. DEFAULT is, framerate=50.0, for Stingray camera.
�	cameraNr: The camera number. DEFAULT is, cameraNr=0
�	winAV: the size of the window (in video frames) for valence and arousal analysis. DEFAULT is winAV=30.
�	modelDir: where the trained models are located. DEFAULT is modelDir=C:/Program Files/HCI^2 group, Imperial College London/Semaine Video Components/
�	visualisationSize: Defines whether the display window should be large or small. DEFAULT visualisationSize=0, which means display should be in a small window, visualisationSize=1 means that display will be in a large window.
�	grabberType=1 defines the camera used to be a firewire camera (e.g., Stingray). grabberType=0 defines the camera used to be an OpenCV+Windows Vista compatible USB camera such as Microsoft LifeCam VX-5000 (see http://opencv.willowgarage.com/wiki/Welcome/OS for a list of compatible cameras). 
DEFAULT is grabberType=0.

If you use this system for your research, you should acknowledge this by citing the following paper:

M. Schroeder, E. Bevacqua, F. Eyben, H. Gunes, D. Heylen, M. Ter Maat, S. Pammi, M. Pantic, C. Pelachaud, B. Schuller, E. De Sevin, M. Valsar, M. Wollmer, "A Demonstration of Audiovisual Sensitive Artificial Listeners", Proc. of Int. Conf. on Affective Computing and Intelligent Interaction, Vol. 1, pp. 263-264, 2009. 