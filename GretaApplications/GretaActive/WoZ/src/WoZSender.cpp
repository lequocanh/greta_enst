//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorPlanner.cpp: implementation of the BehaviorPlanner class. 
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#include "WoZSender.h"

#include <cstdlib>
#include <sstream>

#include <semaine/cms/exceptions/MessageFormatException.h>
#include <decaf/lang/System.h>


WoZSender::WoZSender(const std::string & componentName, bool isInput, bool isOutput) throw(CMSException) :
Component(componentName,isInput,isOutput)
{

	nameSender = new semaine::cms::sender::Sender("semaine.data.synthesis.plan", "AGENT", getName());
	senders.push_back(nameSender);
	stateSender = new semaine::cms::sender::Sender("semaine.data.synthesis.plan", "STATE", getName());
	senders.push_back(stateSender);
	turnSender = new semaine::cms::sender::Sender("semaine.data.synthesis.plan", "TURN", getName());
	senders.push_back(turnSender);
}

WoZSender::~WoZSender()
{
	delete nameSender;
	delete stateSender;
	delete turnSender;
}

void WoZSender::react(SEMAINEMessage * m) throw (CMSException)
{
}