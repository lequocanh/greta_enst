//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <stdio.h>

#include <FL/Fl.H>

#include <FL/Fl_Box.H>
#include "IniManager.h"
#include <FL/glut.H>

#include "DataContainer.h"
#include "xercesTool.h"
#include "ScoreWindow.h"

ScoreWindow *window;
IniManager inimanager;
DataContainer *datacontainer;
std::string ini_filename,directory;

#ifdef NDEBUG
#include <shellapi.h>
bool cvtLPW2stdstring(std::string& s, const LPWSTR pw, UINT codepage = CP_ACP){
	bool res = false;
	char* p = 0;
	int bsz;
	bsz = WideCharToMultiByte(codepage, 0, pw,-1,0,0,0,0);
	if (bsz > 0) {
		p = new char[bsz];
		int rc = WideCharToMultiByte(codepage,0,pw,-1,p,bsz,0,0);
		if (rc != 0) {
			p[bsz-1] = 0;
			s = p;
			res = true;
		}
	}
	delete [] p;
	return res;
}

int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInstance,
     LPSTR     lpszCmdParm,
     int       nCmdShow
)
{
	LPWSTR *szArglist;
	int nArgs;
	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if (nArgs==2){
		cvtLPW2stdstring(ini_filename,szArglist[1]);
		directory="";
	}
	else{
		if(nArgs==3){
			cvtLPW2stdstring(ini_filename,szArglist[1]);
			cvtLPW2stdstring(directory,szArglist[2]);
		}

#else
int main (int argc, char *argv[]){
	if (argc==2){
		ini_filename=argv[1];
		directory="";
	}
	else{
		if (argc==3){
			ini_filename=argv[1];
			directory=argv[2];
		} 
#endif
		else {
			ini_filename="greta.ini";
			directory="";
		}
	}
	XercesTool::startupXMLTools();
	// Get current flag
	//int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );

	// Turn on check function at every alloc/dealloc
	//tmpFlag |= _CRTDBG_CHECK_ALWAYS_DF;

	// Set flag to the new value
	//_CrtSetDbgFlag( tmpFlag );

	//datacontainer needs some data..
	inimanager.ReadIniFile(ini_filename);
	
	datacontainer = new DataContainer();

	//load faces, gestures etc
	//Radek!!!
	int code=datacontainer->initAll(directory);
	//int code=datacontainer->initFMLEngine();
	if (code==0) {
		printf("Problem : out \n");
		exit(1);
	}

	window = new ScoreWindow();
	
	window->show();
  
	Fl::run();

	XercesTool::shutdownXMLTools();
	delete window;
}