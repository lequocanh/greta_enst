// Shared-Attention system
// Christopher Peters

#include "calibration.h"

Calibrator::Calibrator()
{
	this->timePerTrial = 3000;
	//this->calibrated = false;
	this->calibrated = true;
	this->reset();
}

Calibrator::~Calibrator()
{

}

void Calibrator::reset()
{
	printf("\nReset calibrator");
	this->topHeadCoords.clear();
	this->leftHeadCoords.clear();
	this->rightHeadCoords.clear();
	this->bottomHeadCoords.clear();
	this->currentCursorPosition = CURSOR_CENTER;
	this->currentCursorState = CURSOR_READY;
	this->startTime = -1;
	this->isCalibrateModeEnabled = false;
}

void Calibrator::startRecording(long currentTime)
{
	this->isCalibrateModeEnabled = true;
	this->currentCursorState = CURSOR_RECORDING;
	this->startTime = currentTime;
	printf("\nStarted calibrator: current cursor state is now %d", this->currentCursorState);
}

void Calibrator::enable(void)
{
	this->isCalibrateModeEnabled = true;
	this->currentCursorState == CURSOR_READY;
}

void Calibrator::addEntry(float headXDir, float headYDir)
{
	if (!this->isCalibrateModeEnabled) return;	
	if (this->currentCursorState == CURSOR_READY) return;

	//printf("\nAdding entry to calibration");
		
	//headXDir is around trhe x-axis
	switch (this->currentCursorPosition)
	{
		case CURSOR_CENTER:

			break;
		case CURSOR_TOPLEFT:
			this->leftHeadCoords.push_back(headYDir);
			this->topHeadCoords.push_back(headXDir);
			break;
		case CURSOR_UP:
			this->topHeadCoords.push_back(headXDir);
			break;
		case CURSOR_TOPRIGHT:
			this->rightHeadCoords.push_back(headYDir);
			this->topHeadCoords.push_back(headXDir);
			break;
		case CURSOR_RIGHT:
			this->rightHeadCoords.push_back(headXDir);
			break;
		case CURSOR_BOTTOMRIGHT:
			this->rightHeadCoords.push_back(headYDir);
			this->bottomHeadCoords.push_back(headXDir);
			break;
		case CURSOR_DOWN:
			this->bottomHeadCoords.push_back(headXDir);
			break;
		case CURSOR_BOTTOMLEFT:
			this->leftHeadCoords.push_back(headYDir);
			this->bottomHeadCoords.push_back(headXDir);
			break;
		case CURSOR_LEFT:
			this->leftHeadCoords.push_back(headYDir);
			break;
	}
}

void Calibrator::update(long currentTime)
{
	if (!this->isCalibrateModeEnabled) return;	
	if (this->currentCursorState == CURSOR_READY) return;

	//see if we are finished this trial
	if ((currentTime - this->startTime) > timePerTrial)
	{
		this->currentCursorState = CURSOR_READY;
		this->startTime = -1;
		this->currentCursorPosition++;
		//see if we are finished calibration phase altogether
		if (this->currentCursorPosition > 4)//not 8
		{
			this->calcBoundaryValues();
			this->calibrated = true;
			this->reset();
		}
	}
}

//this one rint all stuff
BoundaryValues Calibrator::calcBoundaryValues(void)
{
	printf("\nCalculating boundary values...");
	BoundaryValues bvs;
	//go through lists and average all values
	int num = 0; float val = 0;
	std::list<float>::iterator fb = this->topHeadCoords.begin();
	std::list<float>::iterator fe = this->topHeadCoords.end();
	for (num = 0, val = 0; fb != fe ; fb++,num++)
	{
		val += (*fb);
	}
	bvs.headTopVal = val/(float)num;

	fb = this->bottomHeadCoords.begin();
	fe = this->bottomHeadCoords.end();
	for (num = 0, val = 0; fb != fe ; fb++,num++)
	{
		val += (*fb);
	}
	bvs.headBottomVal = val/(float)num;

	fb = this->leftHeadCoords.begin();
	fe = this->leftHeadCoords.end();
	for (num = 0, val = 0; fb != fe ; fb++,num++)
	{
		val += (*fb);
	}
	bvs.headLeftVal = val/(float)num;

	fb = this->rightHeadCoords.begin();
	fe = this->rightHeadCoords.end();
	for (num = 0, val = 0; fb != fe ; fb++,num++)
	{
		val += (*fb);
	}
	bvs.headRightVal = val/(float)num;

	printf("\nBoundary values are \n top: %lf \n bottom: %lf \n left: %lf \n right: %lf \n", bvs.headTopVal, bvs.headBottomVal, bvs.headLeftVal, bvs.headRightVal);
	return bvs;
}