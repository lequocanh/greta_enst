//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ClockModuleWindow.h: interface for the ClockModuleWindow class.
//
//////////////////////////////////////////////////////////////////////
#ifndef ACTIVE_REALTIME_PLAYER_WINDOW
#define ACTIVE_REALTIME_PLAYER_WINDOW

#define PLAYEROGRE

#pragma once

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include <stdio.h>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Output.H>
#include "FL/Fl.H"
#include "FAPFrame.h"
#include "BAPframe.h"
#ifndef PLAYEROGRE
#include "PlayerFLTKGLWindow.h"
#endif

//#include "FramesManager.h"
#include "AnimationManager.h"

#include "CommandsManager.h"
//#include "PlayerReceiver.h"
#include "CentralClock.h"
#include "JSemaphore.h"
#include "IniManager.h"

#include <semaine/cms/exceptions/MessageFormatException.h>
#include <decaf/lang/System.h>

using namespace semaine::cms::exceptions;

#ifdef PLAYEROGRE
#include "PlayerOgreView.h"
#endif


class RealtimePlayerWindow : public Fl_Double_Window  
{
public:

	RealtimePlayerWindow(CentralClock *pc);
	virtual ~RealtimePlayerWindow();
	int handle(int e);
	void draw();
	void resize(int x, int y, int w, int h);
	//Fl_Multiline_Output *output;
	Fl_Output *timeoutput;
	Fl_Output *fapbufferoutput;
	Fl_Output *bapbufferoutput;
	int x_old, y_old, w_old, h_old;
	bool full;
#ifdef PLAYEROGRE
	PlayerOgreView *glwindow;
#else
	PlayerFLTKGLWindow *glwindow;
#endif
	AnimationManager* am;//	FramesManager *fm;
	//CommandsManager *cm;

	//semaine::components::greta::PlayerReceiver *receiver;
	CentralClock *pc;
	cmlabs::JMutex agentaspectmutex;

	BAPFrame bf;
	FAPFrame ff;

};

#endif