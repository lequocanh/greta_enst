#include "AnimationManager.h"
#include "GretaLogger.h"
#include "RealTimeAudio.h"
#include "PlayerReceiver.h"

extern std::list<GretaLogger*> listLog;
extern semaine::components::greta::PlayerReceiver *playerreceiver;

AnimationManager::AnimationManager(CentralClock *centralClock, int maximal_number_of_animations):cmlabs::JThread()
{
	maxsize = maximal_number_of_animations;
	clock = centralClock;
	toPlay = NULL;
	indexFAPtoPlay = 0;
	indexBAPtoPlay = 0;
	audioToPlay = 2;
	fapPlaying = false;
	bapPlaying = false;
	recent = NULL;
}

AnimationManager::~AnimationManager(){}

Animation* AnimationManager::createAnimation(std::string id){
	Animation* newAnimation = new Animation(id);
	waitingQueue.push_back(newAnimation);
	recent = newAnimation;
	return newAnimation;
}

AnimationHistory* AnimationManager::findInHistory(std::string id){
	for(int i=0; i<history.size(); ++i){
		if(history[i]->getID() == id){
			return history[i];
		}
	}
	return NULL;
}

void AnimationManager::waitingQueueToReadyQueue(std::string id){
	mutex.enterMutex();
	Animation* toMove;
	int index=0;
	while(index<waitingQueue.size() && waitingQueue[index]->getID()!=id)
		++index;
	if(index==waitingQueue.size() || waitingQueue.empty()){ //not in waiting queue, already in queue ?
		index = 0;
		while(index<queue.size() && queue[index]->getID()!=id)
			++index;
		if(index==queue.size() || queue.empty()){ //so where...
			mutex.leaveMutex();
			return;
		}
		else
			toMove = this->pullout(index);
	}
	else{
		toMove=waitingQueue[index];
		waitingQueue.erase(waitingQueue.begin()+index);
	}
	pullon(toMove);
	mutex.leaveMutex();
}

Animation* AnimationManager::pullout(int index){
	if(queue.empty()) return NULL;
	Animation* temp = queue[index];
	queue.erase(queue.begin()+index);
	return temp;
}

int AnimationManager::pullon(Animation* anim){
	if(queue.empty()){
		queue.push_back(anim);
		return 0;
	}
	int index = 0;
	vector<Animation*>::iterator iter = queue.begin();
	while(iter!= queue.end() && (**iter) < (*anim)){
		++index;
		++iter;
	}
	queue.insert(iter, anim);
	return index;
}

void AnimationManager::run(){
	while(true){
		Sleep(10);
		//check for animation to play
		checkNextAnimationToPlay();

		mutex.enterMutex();
		for(int i=queue.size()-1; i>-1; --i){
			// remove dead animations
			if(queue.at(i)->getDeadTime() < clock->GetTime()){
				sendDead(queue.at(i)->getID());
				history.push_back(new AnimationHistory(queue.at(i)->getID(), clock->GetTime(), DEAD));
				Animation* todelete = pullout(i);
				if(todelete!=NULL){
					if(recent!=NULL && recent->getID() == todelete->getID())
						recent = NULL;
					delete todelete;
				}
			}
			// decrease priority
			// TODO : call animation->decreasePriority();
		}
		mutex.leaveMutex();
	}
}
	
Animation* AnimationManager::getAnimationAlsoPlaying(std::string id){
	if(toPlay!=NULL && toPlay->getID()==id)
		return toPlay;
	return getAnimation(id);
}

Animation* AnimationManager::getAnimation(std::string id){
	if(recent!=NULL && recent->getID()==id)
		return recent;
	for(int i=0; i<queue.size(); ++i){
		if(queue[i]->getID() == id){
			recent = queue[i];
			return recent;
		}
	}
	for(int i=0; i<waitingQueue.size(); ++i){
		if(waitingQueue[i]->getID() == id){
			recent = waitingQueue[i];
			return recent;
		}
	}
	return NULL;
}

void AnimationManager::addFAPFrames(std::string id, std::string textFrames){
	std::string parser(textFrames);
	std::string twolines;
	std::string label;
	if(parser[parser.size()-1]!='\n')
		parser+="\n";
	int newlinePosition = parser.find_first_of("\n");
	label = parser.substr(0,newlinePosition);
	parser = parser.substr(newlinePosition+1);
	while(parser!=""){
		//mask
		newlinePosition = parser.find_first_of("\n") + 1;
		twolines = parser.substr(0,newlinePosition);
		parser = parser.substr(newlinePosition);
		//values
		newlinePosition = parser.find_first_of("\n") + 1;
		twolines += parser.substr(0,newlinePosition);
		parser = parser.substr(newlinePosition);
		//create frame
		FAPFrame frame;
		frame.ReadFromBuffer((char*)twolines.c_str());
		frame.use_at = frame.framenumber*40;
		frame.id=label;
		addFAPFrame(id, frame);
	}
}

void AnimationManager::addFAPFrame(std::string id, FAPFrame frame){
	mutex.enterMutex();
	Animation* target = getAnimationAlsoPlaying(id);
	if(target == NULL){
		AnimationHistory* targetHistory = findInHistory(id);
		if(targetHistory == NULL)
			target = createAnimation(id);
		else{
			listLog.push_back(new GretaLogger("Active_Player", 
			"the animation "+ id + (targetHistory->getStat()==ENDED ? " is finish":(targetHistory->getStat()==DEAD ? " is dead":" is deleted")), 
			"warn"));
			mutex.leaveMutex();
			return;
		}
	}
	target->add(frame);
	mutex.leaveMutex();
}

void AnimationManager::addBAPFrames(std::string id, std::string textFrames){
	std::string parser(textFrames);
	std::string mask;
	std::string values;
	std::string label;
	if(parser[parser.size()-1]!='\n')
		parser+="\n";
	int newlinePosition = parser.find_first_of("\n");
	label = parser.substr(0,newlinePosition);
	parser = parser.substr(newlinePosition+1);
	while(parser!=""){
		//mask
		newlinePosition = parser.find_first_of("\n");
		mask = parser.substr(0,newlinePosition);
		parser = parser.substr(newlinePosition+1);
		//values
		newlinePosition = parser.find_first_of("\n");
		values = parser.substr(0,newlinePosition);
		parser = parser.substr(newlinePosition+1);
		//create frame
		BAPFrame frame;
		frame.ReadFrom2Lines(mask, values);
		frame.id=label;
		addBAPFrame(id, frame);
	}
}

void AnimationManager::addBAPFrame(std::string id, BAPFrame frame){
	mutex.enterMutex();
	Animation* target = getAnimationAlsoPlaying(id);
	if(target == NULL){
		AnimationHistory* targetHistory = findInHistory(id);
		if(targetHistory == NULL)
			target = createAnimation(id);
		else{
			listLog.push_back(new GretaLogger("Active_Player", 
			"the animation "+ id + (targetHistory->getStat()==ENDED ? " is finish":(targetHistory->getStat()==DEAD ? " is dead":" is deleted")), 
			"warn"));
			mutex.leaveMutex();
			return;
		}
	}
	target->add(frame);
	mutex.leaveMutex();
}

void AnimationManager::addAudio(std::string id, char* bufferAudio, int bufferSize){
	mutex.enterMutex();
	Animation* target = getAnimation(id);
	if(target == NULL){
		AnimationHistory* targetHistory = findInHistory(id);
		if(targetHistory == NULL)
			target = createAnimation(id);
		else{
			listLog.push_back(new GretaLogger("Active_Player", 
			"the animation "+ id + (targetHistory->getStat()==ENDED ? " is finish":(targetHistory->getStat()==DEAD ? " is dead":" is deleted")), 
			"warn"));
			mutex.leaveMutex();
			return;
		}
	}
	target->setAudio(bufferAudio,bufferSize);
	mutex.leaveMutex();
}

void AnimationManager::setStartInfos(std::string id, double priority, _time_ startTime, _time_ lifeTime, bool hasAudio){
	mutex.enterMutex();
	Animation* target = getAnimation(id);
	if(target == NULL){
		AnimationHistory* targetHistory = findInHistory(id);
		if(targetHistory == NULL){
			listLog.push_back(new GretaLogger("Active_Player", "No data was received for animation "+id+" before the start informations", "warn"));
			target = createAnimation(id);
		}
		else{
			listLog.push_back(new GretaLogger("Active_Player", 
			"the animation "+ id + (targetHistory->getStat()==ENDED ? " is finish":(targetHistory->getStat()==DEAD ? " is dead":" is deleted")), 
			"warn"));
			mutex.leaveMutex();
			return;
		}
	}
	target->setStartInfos(priority, startTime, lifeTime, hasAudio);
	mutex.leaveMutex();
	waitingQueueToReadyQueue(id);
	checkNextAnimationToPlay();
}


bool AnimationManager::getBAPFrame(BAPFrame &frame){
	mutex.enterMutex();
	if(toPlay!=NULL && indexBAPtoPlay>=0 && indexBAPtoPlay<toPlay->getBAPFramesNumber()){//index bounds
		while(indexBAPtoPlay*40+started_at<clock->GetTime() && indexBAPtoPlay<(toPlay->getBAPFramesNumber())){ //is the frame to play now
			++indexBAPtoPlay;
		}
		if(indexBAPtoPlay<(toPlay->getBAPFramesNumber()))
			frame=toPlay->getBAPFrame(indexBAPtoPlay);
		if(!bapPlaying){
			sendBAPStarted(toPlay->getID());
			bapPlaying=true;
		}
		//++indexBAPtoPlay;
		if(bapPlaying && indexBAPtoPlay>=toPlay->getBAPFramesNumber()){
			this->sendBAPEnded(toPlay->getID());
			bapPlaying=false;
		}
		mutex.leaveMutex();
		return true;
	}
	if(toPlay!=NULL && bapPlaying && indexBAPtoPlay>=toPlay->getBAPFramesNumber()){
		this->sendBAPEnded(toPlay->getID());
		bapPlaying=false;
	}
	mutex.leaveMutex();
	return false;
}
bool AnimationManager::getFAPFrame(FAPFrame &frame){
	mutex.enterMutex();
	if(toPlay!=NULL && indexFAPtoPlay>=0 && indexFAPtoPlay<toPlay->getFAPFramesNumber()){//index bounds
		while(indexFAPtoPlay*40+started_at<=clock->GetTime() && indexFAPtoPlay<(toPlay->getFAPFramesNumber())){ //is the frame to play now
			++indexFAPtoPlay;
		}
		if(indexFAPtoPlay<(toPlay->getFAPFramesNumber()))
			frame=toPlay->getFAPFrame(indexFAPtoPlay);
		//++indexFAPtoPlay;
		if(!fapPlaying){
			sendFAPStarted(toPlay->getID());
			fapPlaying=true;
		}
		if(fapPlaying && indexFAPtoPlay>=toPlay->getFAPFramesNumber()){
			this->sendFAPEnded(toPlay->getID());
			fapPlaying=false;
		}
		mutex.leaveMutex();
		return true;
	}
	if(toPlay!=NULL && fapPlaying && indexFAPtoPlay>=toPlay->getFAPFramesNumber()){
		this->sendFAPEnded(toPlay->getID());
		fapPlaying=false;
	}
	mutex.leaveMutex();
	return false;
}

void AnimationManager::playAudio(){
	if(audioToPlay==0){
		RealTimeAudio::play_wave(0);
		audioToPlay=1;
		sendAudioStarted(toPlay->getID());
	}
}

void AnimationManager::audioEnded(){
	if(audioToPlay==1){
		audioToPlay = 2;
		sendAudioEnded(toPlay->getID());
	}
}

void AnimationManager::checkNextAnimationToPlay(){
	mutex.enterMutex();
	//check if animation is finish
	if(toPlay!=NULL 
	  && audioToPlay==2 
	  && indexBAPtoPlay>=toPlay->getBAPFramesNumber() 
	  && indexFAPtoPlay>=toPlay->getFAPFramesNumber()){
		history.push_back(new AnimationHistory(toPlay->getID(), clock->GetTime(), ENDED));
		sendEnded(toPlay->getID());
		if(recent!=NULL && recent->getID()==toPlay->getID())
			recent = NULL;
		delete toPlay;
		toPlay = NULL;
	}
	//if no animation to play, we check to a new one
	if(toPlay==NULL && !queue.empty() && queue[0]->getStartTime()<=clock->GetTime() && (queue[0]->getHasAudio() == (queue[0]->getBufferAudioSize() > 0))){
		//std::cout << "\n" << queue[0]->getHasAudio() << "  "<< (queue[0]->getBufferAudioSize() > 0) << "\n";
		toPlay = pullout();
		if(toPlay->getBufferAudioSize()>47){
			RealTimeAudio::load_wave(toPlay->getBufferAudio(), toPlay->getBufferAudioSize());
			audioToPlay = 0;
		}
		else
			audioToPlay=2;
		indexFAPtoPlay=0;
		indexBAPtoPlay=0;
		fapPlaying=false;
		bapPlaying=false;
		started_at=clock->GetTime();
		sendStarted(toPlay->getID());
	}
	mutex.leaveMutex();
}

void AnimationManager::deleteAnimation(std::string id){
	mutex.enterMutex();
	Animation* target = getAnimation(id);
	if(target==NULL){//not in queue
		if(toPlay->getID()==id)
			sendDeleted(id,1);
		else{
			AnimationHistory* targetHistory = findInHistory(id);
			if(targetHistory==NULL)
				sendDeleted(id,5);
			else{
				switch (targetHistory->getStat()){
					case ENDED : sendDeleted(id,2);break;
					case DEAD : sendDeleted(id,3);break;
					case DELETED : sendDeleted(id,4);break;
				}
			}
		}
	}
	else{
		int index = 0; //search in queue
		while(index<queue.size() && queue[index]->getID()!=id)
			++index;
		if(index==queue.size() || queue.empty()){ //not in queue
			index = 0; //search in waiting queue
			while(index<waitingQueue.size() && waitingQueue[index]->getID()!=id)
				++index;
			if(index==waitingQueue.size() || waitingQueue.empty()) //not in waiting queue
				sendDeleted(id,5);//it's weird
			else{
				waitingQueue.erase(waitingQueue.begin()+index);
				history.push_back(new AnimationHistory(id, clock->GetTime(), DELETED));
				sendDeleted(id,0);
			}
		}
		else{
			queue.erase(queue.begin()+index);
			history.push_back(new AnimationHistory(id, clock->GetTime(), DELETED));
			sendDeleted(id,0);
		}
		if(recent!=NULL && target->getID()!=recent->getID())
			recent=NULL;
		delete target;
	}
	mutex.leaveMutex();
}

void AnimationManager::stop(){
	mutex.enterMutex();
	//clear all :
	while(!queue.empty()){
		deleteAnimation(queue.at(0)->getID());
	}
	while(!waitingQueue.empty()){
		deleteAnimation(waitingQueue.at(0)->getID());
	}
	//stop and store animation :
//TODO: get the real playing FAP and BAP. is assumed here that the frames are complete.
	RealTimeAudio::stop_wave();
	FAPFrame fap;
	BAPFrame bap;
	if(toPlay!=NULL){
		//last FAP frame:
		if(toPlay->getFAPFramesNumber()>0){
			if(indexFAPtoPlay<toPlay->getFAPFramesNumber())
				fap = toPlay->getFAPFrame(indexFAPtoPlay);
			else
				fap = toPlay->getFAPFrame(toPlay->getFAPFramesNumber()-1);
		}
		//last BAP frame:
		if(toPlay->getBAPFramesNumber()>0){
			if(indexBAPtoPlay<toPlay->getBAPFramesNumber())
				bap = toPlay->getBAPFrame(indexBAPtoPlay);
			else
				bap = toPlay->getBAPFrame(toPlay->getBAPFramesNumber()-1);
		}
		history.push_back(new AnimationHistory(toPlay->getID(), clock->GetTime(), STOPPED));
		sendStopped(toPlay->getID(),bap,fap);
		delete toPlay;
	}
	recent = NULL;
	toPlay = NULL;
	mutex.leaveMutex();
}

void AnimationManager::incrementIDInHistory(){
	mutex.enterMutex();
	for(int i=0; i<history.size(); ++i)
		history[i]->incrementID();
	mutex.leaveMutex();
}


void AnimationManager::sendStarted(std::string id){
		printf("AnimationManager : %s started at %d\n", id.c_str(), clock->GetTime());
	}

void AnimationManager::sendAudioStarted(std::string id){
	playerreceiver->audiostart(id);
	//	printf("AnimationManager : %s audio started at %d\n", id.c_str(), clock->GetTime());
	}

void AnimationManager::sendFAPStarted(std::string id){
	playerreceiver->fapstart(id);
//	printf("AnimationManager : %s fap started at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendBAPStarted(std::string id){
	playerreceiver->bapstart(id);
//	printf("AnimationManager : %s bap started at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendAudioEnded(std::string id){
	playerreceiver->audioEnded(id);
//	printf("AnimationManager : %s audio ended at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendFAPEnded(std::string id){
	playerreceiver->fapEnded(id);
//	printf("AnimationManager : %s fap ended at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendBAPEnded(std::string id){
	playerreceiver->bapEnded(id);
//	printf("AnimationManager : %s bap ended at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendEnded(std::string id){
	printf("AnimationManager : %s ended at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendDead(std::string id){
	printf("AnimationManager : %s is dead at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendDeleted(std::string id, int success){
	if(success==0) printf("AnimationManager : %s deleted at %d\n", id.c_str(), clock->GetTime());
}

void AnimationManager::sendStopped(std::string id, BAPFrame bapframe, FAPFrame fapframe){
	printf("AnimationManager : %s stopped at %d\n", id.c_str(), clock->GetTime());
}