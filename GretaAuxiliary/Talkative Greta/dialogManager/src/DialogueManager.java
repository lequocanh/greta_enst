import demo.*;

/**
 * Class that manages the dialog, and more precisely:
 * - the transformation of simple text message to FML or BML message
 * - the triggering of a specific demo
 * 
 * @author Magalie
 *
 */

public class DialogueManager {
	
	/* Connexion with psyclone */
	private Connexion _connexion;
	
	public DialogueManager(Connexion c){
		this._connexion=c;
	}
	
	/**
	 * Method that transforms a simple text message to an FML message understandable by Greta and send the FML message to psyclone
	 * @param message simple text message
	 */
	public void sendMessageFML(String message){
		
		/* If the simple text message received should trigger a scripted demo, it's not necessary to construct a FML message */
		if(this.triggerDemo(message)){
			return;
		}

		MessageFML fmlMessage=new MessageFML(message);
		String contentMessage=fmlMessage.getFMLMessage();
		System.out.println("Message send to Greta"+contentMessage);
		this._connexion.sendFML(contentMessage);
	}
	
	/**
	 * Method that send the text written by the user to the dialog engine (through psyclone) to know the response of Greta 
	 * @param message Message written by the user
	 */
	public void sendTextIn(String message){
		this._connexion.sendText(message);
	}
	
	/**
	 * Method that check if Greta should play a specific demo and triggers the specific demo
	 * @param message message received (can be a simple text that Greta should say or a specific keywords that should triggers a demo
	 * @return true if a demi is triggered, else false
	 */
	public boolean triggerDemo(String message){
		/* check if it's a demo */
		DemoExample demo=ChatInterface._dataBaseDemo.getDemoToExecute(message);
		if(demo==null){
			return false;
		}else{
			if(demo.isBML())
				this._connexion.sendBML(demo.getBmlFml());
			else
				this._connexion.sendFML(demo.getBmlFml());
			return true;
		}	
	}
	
}
