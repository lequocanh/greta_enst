//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

/** it's the agent
*   the agent can be created, moved around, rotated
*   it has a status, can begin or stop talking
*   everything starts from this class
*
* @see Joint, BAPFrame
* @author Maurizio Mancini, manmau@yahoo.com
* @author Bjoern Hartmann, bjoern@graphics.cis.upenn.edu
* @author Massimo Bilvi, bilvi@gsmbox.it
* @author Stefano Pasquariello
* @version 1.0
*/

//***************************NOTE***********************************
//*  this class is the result of some different works              *
//*  written in both C and C++ code                                *
//*  it may be very difficult to understand how it works           *
//*  all the C functions are declared extern                       *
//*  and I tryed to change all the global variables into member    *
//*  variables                                                     *
//*  to do this I've had to modify C functions passing a lot       *
//*  of pointers in their argument list, but it works very well!   *
//******************************************************************

#if !defined(AFX_AGENT_H__26D28CE3_E1DF_4A48_805B_06F42C36A931__INCLUDED_)
#define AFX_AGENT_H__26D28CE3_E1DF_4A48_805B_06F42C36A931__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <yvals.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <fstream>
#include <gl/glaux.h> //some 3D routines to draw spheres, etc
//#include "PerlinNoise.h"
#include "onlyforface/init.h" //initialization file for the face
#include "onlyforbody/PlayerJoint.h" //body joints
//#include "body files/BAPFrame.h" //BAP frames
#include "onlyforbody/BodyVertexGroup.h" //body vertices groups
#include <time.h>
#include "FAPFrame.h"
#include "FAPData.h"
#include "onlyforface/FaceData.h"
#include "onlyforface/ApplyFAPs.h"
#include "BAPData.h"
//#include "BAPConverter.h"
#include "onlyforbody/BodyData.h"
#include "PlayerPerlinNoise.h"

using namespace std;

class SharedAttentionAgentAspect
{
public:
	std::string name;//agent's name
	std::string character;

	SharedAttentionAgentAspect(std::string name,std::string character,bool realtime=false);//constructor

	virtual ~SharedAttentionAgentAspect();//destructor

	void SetWriteAVI();
	
	//moves the agent to a given 3D position
	void MoveAgent(float x,float y,float z);
	void MoveForward(float quantity);
	void RotateOf(float x,float y,float z);
	//rotates agent by given angles
	void RotateAgent(float x,float y,float z);
	int SharedAttentionAgentAspect::ExportFace(std::string NameFile, int fapframe_index);
	int SharedAttentionAgentAspect::ExportFaceVRML(std::string NameFile, int fapframe_index);
	//assigns an animation file to the agent
	//until now an animation is stored into 3 files at the same time
	//one BAP + one FAP + one WAV file
	//so the given name is only a base name and the three file names
	//are the joining of the given name with ".bap", ".fap", ".wav"
	int AssignFile(char* name);
	int AssignFAPBuffer(char* fapbuffer);
	int AssignBAPBuffer(char* bapbuffer);
//	int ReadFapFile(char *fapfilename);
//	int ReadFapBuffer(char *fapfilename);
	float SharedAttentionAgentAspect::CalibrateFaps(int fapnum);
	void ReloadFile();
	char* GetWavFileName();
	std::string GetBaseFileName();
	int GetTotalNumberOfFrames();
	int GetFapFps();

	//tells the agent to begin or stop talking
	void StartTalking();
	void StopTalking();
	void EnableAudio(bool audio);
	void OneFrameUp();
	void OneFrameDown();

	//draws the agent (OpenGL)
	void draw();
	void drawGreta(int fapframe_index, int BAPFrame_index);
	void display_teeths_eyes_tongue(int fapframe_index);

	//gets agent's status
	char* GetStatus();
	int GetCurrentFrame();
	void SetCurrentFrame(int c);

	void LoadFAPFrame(FAPFrame *f);

	//switches the visualization of some agent's body parts
	void SwitchSkin();
	void SwitchIFAPU();
	void SwitchEyes();
	void SwitchSkeleton();
	void SwitchHair();
	void SwitchWire1();
	void SwitchWire2();

	//gets the visualization state of some agent's body parts
	bool GetSkinState();
	bool GetEyesState();
	bool GetBodyState();
	bool GetHairState();

	//gets the agent's position and rotation
	point3d GetPosition();
	point3d GetRotation();

	void SetSkinColor(float r,float g,float b);

	void SetHairColor(float r,float g,float b);

	float skincolor_red,skincolor_green,skincolor_blue;
	float haircolor_red,haircolor_green,haircolor_blue;

	bool WRITEAVI;
	bool realtime;

public: 
	
	float started_animation;

private:


	
	bool audio,audioenabled;
	clock_t agent_created;
	float local_time;

	std::string openedfile;
	GLint hair_list;//FACE:OpenGL list to draw hair
	GLint HairGen3DObjectList();
	/*
	GLint inf_lashes_list;//FACE:OpenGL list to draw inf lashes
	GLint sup_lashes_list;//FACE:OpenGL list to draw sup lashes
	GLint sim_inf_lashes_list;
	GLint sim_sup_lashes_list;
	*/
	bool flag_wire[2];//FACE:wireframe rendering
	bool flag_shade[3];//visualization of face skin,eyes(and teeth,thongue),body skin
	bool flag_hair;//visualization of hair
	bool flag_faceskintexturemapping;
	bool flag_skeleton;

	PlayerJoint* skeletonroot;//BODY:pointer to the body's root joint	
	PlayerJoint* pJoints[NUMJOINTS]; //BODY: direct access pointers to different joints
	
	point3d agentpos;//agent'sposition
	point3d agentrot;//agent's rotation
	point3d zero;//point (0,0,0)
	int AnimationActualFrame;

	
	float t_eyelid_rotation_l,b_eyelid_rotation_l;
	float t_eyelid_rotation_r,b_eyelid_rotation_r;

	int flag_rotation;

	float rot_head_pitch,rot_head_yaw,rot_head_roll; //new
	
	/////////////////////////////////

	GLuint head_texture;//array of agent's textures
	GLuint body_texture;//array of agent's textures
	GLuint eyes_texture;//array of agent's textures
	GLuint eyelashes_texture;//array of agent's textures
	GLuint lips_texture;//array of agent's textures

	int use_eyes_textures;//FACE:flag that activate eyes' texturing
	void RenderEyes(int fapframe_index);
	void RenderEyesTextures();
	void RenderSphere( float c_x, float c_y, float c_z, double r, int n );
public:
	

	ApplyFAPs *applyfaps;
	FaceData *facedata;
	FAPdata *FAPs;
	BAPData *BAPs;
	pointtype misc_point[SKIN_VERTEX_NUMBER2];
	GLint flag_surface1[69];
	GLint flag_surface2[12];
	std::string fap_filename, bap_filename, wav_filename;
	int enable_sound;

	int animationfps;
	int maxframes;

	void LoadBAPFrame(int index,BAPFrame *inputBAPFrame);//BODY:loads a single BAP frame into the body joints
private:
	std::string status;//agent's status
	void DrawBody();//draws only the body
	BodyData *bodydata;	

	int global_fps;
	bool check_fps();

	bool initFAPs(void);
	bool init_face(void);
	void DrawFace(int fapframe_index);

	void DisplayFaceSkin();
	void DisplayFaceSkinWithTexture();
	bool init_face(char *buffer);//FACE:initializes face structures and reads FAP file

	AUX_RGBImageRec *LoadBMP(char *Filename);//loads texture
	unsigned char* AddAlpha(unsigned char *src, int size);//activate texture's alpha channel
	int SharedAttentionAgentAspect::LoadGLTextures(std::string filename,bool alpha,GLuint *texture_dest);
	void InitGLTextures();//initializes textures, not used
	float PerlinStep1;
	float PerlinFactor1;
	float PerlinStep2;
	float PerlinFactor2;
	PlayerPerlinNoise noise1,noise2;
	int noisecounter1;
	int noisecounter2;
	void RestartAnimation();
	float restartingtime;
	float stoppingtime;
};

#endif // !defined(AFX_AGENT_H__26D28CE3_E1DF_4A48_805B_06F42C36A931__INCLUDED_)