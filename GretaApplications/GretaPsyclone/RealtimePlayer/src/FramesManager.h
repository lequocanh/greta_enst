#pragma once

#include <vector>
#include "JThread.h"
#include "JSemaphore.h"
#include "FAPFrame.h"
#include "BAPFrame.h"
#include "CentralClock.h"

//using namespace cmlabs;

class FramesManager : public cmlabs::JThread
{
public:

	FramesManager(CentralClock *pc);
	virtual ~FramesManager();
	

	void AddFAPFrame(FAPFrame ff);
	void AddFAPFrame(std::string object);
	void AddBAPFrame(BAPFrame bf);
	void AddBAPFrame(std::string object);

	void AddFAPFrames(std::vector<FAPFrame> ffs);
	void AddBAPFrames(std::vector<BAPFrame> bfs);

	bool GetFAPFrame(FAPFrame &returnff);
	bool GetBAPFrame(BAPFrame &returnbf);

	int GetFAPSize();
	int GetBAPSize();

	void EmptyBuffer();

	void DeleteDiff(std::string label);

	bool IsFapFrameAt(int time);

	FAPFrame *GetFapFrameAt(int time);

	void run();

	bool sortff;
	bool sortbf;
	bool collectgarbage;

	CentralClock *pc;
	
	int lastfapframe;
	int lastbapframe;
private:
	std::vector<FAPFrame> fapframes;
	std::vector<BAPFrame> bapframes;

	cmlabs::JMutex mutex;

	
};