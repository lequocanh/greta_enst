#include "NaoPlayer.h"

extern IniManager inimanager;	

float jointvalues[] = 
	{-0.3, 0, // "HeadYaw", "HeadPitch"
	0.2, 0.2, -2.0, -1.30, 1.50, 0, //"LShoulderPitch", "LShoulderRoll", "LElbowYaw", "LElbowRoll", "LWristYaw", "LHand"
	0, 0, -0.8, 1.3, -0.6, 0, //"LHipYawPitch", "LHipRoll", "LHipPitch", "LKneePitch", "LAnklePitch", "LAnkleRoll"
	0, 0, -0.70, 1.30, -0.60, 0, //"RHipYawPitch", "RHipRoll", "RHipPitch", "RKneePitch", "RAnklePitch", "RAnkleRoll"
	1.20, 0, 0, 0, 0, 0};//"RShoulderPitch", "RShoulderRoll", "RElbowYaw", "RElbowRoll", "RWristYaw", "RHand"


void NaoPlayer::init()
{	
	this->my_robot.motion_StiffnessOn();
	//this->my_robot.motion_PoseZero();
	this->my_robot.motion_PoseInit();
}

void NaoPlayer::end()
{
	this->my_robot.Relax();
	this->my_robot.motion_StiffnessOff();
}

NaoPlayer::NaoPlayer()
{

	
	std::string host;
	int port;


	host=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	port=inimanager.GetValueInt("PSYCLONE_PORT");	
	this->GretaName=inimanager.GetValueString("GRETA_NAME").c_str();

	this->module=new Psydule(GretaName+NAOPLAYER_MODULE_NAME ,host,port);
	
	this->module->setIsInputisOutput(false,true);
	this->GretaName=this->module->getGretaName(GretaName);

	std::list<std::string> datatypes;

	datatypes.push_back(GretaName+BAP_KEY_FRAME);	


	if(module->Register(GretaName+GRETA_WHITEBOARD,datatypes)!=0)
	{
		printf("registered to communication interface\n");
		//SDL_CreateThread(realtimeplayerreceiver,this);
	}
	else
	{
		printf("could not register to communication interface\n ");
		module=0;
		
		exit(1);
	}



}

NaoPlayer::~NaoPlayer(void)
{
	
}

vector<float> NaoPlayer::converter(std::string Greta_motion)
{
	vector<float> motion_values;


	std::string received;
	std::string twolines;
	std::string label;
	received=Greta_motion.c_str();
	
	if(received[received.size()-1]!='\n')
		received+="\n";
	
	
	label=received.substr(0,received.find_first_of("\n"));
	received=received.substr(received.find_first_of("\n")+1);
	std::string first;
	std::string second;
	while(received!="")
	{

		BAPFrame bf;

		first    = received.substr(0,received.find_first_of("\n"));
		received = received.substr(received.find_first_of("\n")+1);
		second   = received.substr(0,received.find_first_of("\n"));
		received = received.substr(received.find_first_of("\n")+1);
		cout<<"First: "<<first<<endl;
		cout<<"Second: "<<second<<endl<<endl;

		bf.ReadFrom2Lines(first, second);

		jointvalues[2] = bf.GetBAPAngle("l_shoulder.rotateY",radians);
		jointvalues[3] = bf.GetBAPAngle("l_shoulder.rotateZ",radians);
		jointvalues[4] = bf.GetBAPAngle("l_elbow.rotateY",radians);
		jointvalues[5] = bf.GetBAPAngle("l_elbow.rotateX",radians);
		jointvalues[6] = bf.GetBAPAngle("l_wrist.rotateY",radians);

		// CAN KIEM TRA GIA TRI MIN, MAX CUA TUNG CU DONG, min < jointvalue < max
	
		jointvalues[20] = bf.GetBAPAngle("r_shoulder.rotateY",radians);
		jointvalues[21] = bf.GetBAPAngle("r_shoulder.rotateZ",radians);
		jointvalues[22] = bf.GetBAPAngle("r_elbow.rotateY",radians);
		jointvalues[23] = bf.GetBAPAngle("r_elbow.rotateX",radians);
		jointvalues[24] = bf.GetBAPAngle("r_wrist.rotateY",radians);



		//flexion		= f.GetBAPAngle("r_elbow.rotateX",degrees);
		//twist		= f.GetBAPAngle("r_elbow.rotateY",degrees);

		//for(int i=2;i<=6;i++) cout<<jointvalues[i]<<" "; cout<<std::endl;
			int size = sizeof( jointvalues ) / sizeof( jointvalues[0] );
			vector<float> PositionVector(jointvalues, &jointvalues[size]);
		
			my_robot.make_motion(PositionVector);
	}



//jointnames[2] = string("LShoulderPitch"); // shoulder_y = l_shoulder_twisting = a[36]
//jointnames[3] = string("LShoulderRoll");  // shoulder_z = l_shoulder_abduct = a[34]
//jointnames[4] = string("LElbowYaw");      // elbow_y = l_elbow_twisting = a[40]
//jointnames[5] = string("LElbowRoll");	  // elbow_x = l_elbow_flexion = a[38]
//jointnames[6] = string("LWristYaw");	  // wrist_y = l_wrist_twisting = a[46]
	cout<<"ket thuc"<<endl;
	return motion_values;
}

void NaoPlayer::run()
{
	std::string content, type;
	//long long lasttime=pc->GetTime();
	cout<<"Receive key frames ..."<<endl;

	int n = 0;
	while((n!=13) && (n!=27))
	{
	     if (kbhit()) n=getch();

      
		if(module==0)
			break;	

		CommunicationMessage *msg;
		msg=module->ReceiveMessage(5);

		if(msg!=0)
		{
			
			content=msg->getContent();
			type=msg->getType();
		
			if(strcmp(type.c_str(),(GretaName+BAP_KEY_FRAME).c_str())==0)
			{

				vector<float> values = this->converter(content);


			}

		}
	}

	
}
