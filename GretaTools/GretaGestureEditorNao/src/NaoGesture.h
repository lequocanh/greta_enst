#pragma once

#include "GestureEngine.h"

#include "NaoMotion.h"
#include "XMLGenericTree.h"

using namespace GestureSpace;

class NaoGesture
{
public:
	NaoGesture(void);
	~NaoGesture(void);

	void LoadFile();   //Load a txt file in the gesture repertory 

	void PrintScreen();// Print header information of txt file on the screen 

	void PrintScreenKeyFrames(GestureSpace::GesturePhaseVector *phasesVector); // Print information of keyframes from txt file on the screen

	vector<float> LeftArm(GesturePhase *phase, vector<float> JointValues, float *lHand); 

	vector<float> RightArm(GesturePhase *phase, vector<float> JointValues, float *rHand);

	vector<float>  Converter(GesturePhase *phase, vector<float> vecxyz, float *hand_opend, float *hand_dir, float SPC, float TMP);

	void Interpret();

	void Interpret(GestureSpace::Gesture *gesture);

	void Interpret(GestureSpace::Gesture *gesture, GestureSpace::GesturePhaseVector *phasesVector);

	void Print_angles();

public:
	//void run(){while(true) Sleep(10);}

	GestureSpace::Gesture *gesture; // gesture has two phases: phases and phasesAssym for right and left side correspondingly when the gesture is assymetric
	GestureSpace::GesturePhaseVector *phasesVector; // = &gesture->phases

	NaoMotion my_robot;

	float *lHand;
	float *rHand;
	vector<float> lVec6;
	vector<float> rVec6;

	vector<vector<float>> leftPath;
	vector<vector<float>> rightPath;
	vector<float> rightTimes;
	vector<float> leftTimes;

	XMLGenericTree *xmltree;
};
