//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// MultimodalEmotionConstraint.cpp: implementation of the MultimodalEmotionConstraint class.
//
//////////////////////////////////////////////////////////////////////

#include "MultimodalEmotionConstraint.h"


MultimodalEmotionConstraint::MultimodalEmotionConstraint ()
{
	signals = new std::vector<ConstraintSignal>;
	//constraints = new std::vector<Constraint>;
	cons_trees = new std::vector<ConsNode *>;
}

MultimodalEmotionConstraint::~MultimodalEmotionConstraint ()
{
	//delete signals;
	//delete constrees;
}

std::string MultimodalEmotionConstraint::getName()
{
	return name;
}

void MultimodalEmotionConstraint::setName(std::string name1)
{
	this->name=name1;
}

//void MultimodalEmotionConstraint::addConstraint(Constraint  constraint1)
//{
//	constraints->push_back(constraint1);
//}

//std::vector<Constraint>* MultimodalEmotionConstraint::getConstraints()
//{   
//	if (constraints->empty()==true) return NULL;
//	return constraints;
//}


int MultimodalEmotionConstraint::getIdOfSignal(std::string name){

int id=-1;

	//find id of the signal
	std::vector<ConstraintSignal>::iterator signal_iter;
	
	for(signal_iter=signals->begin();signal_iter!=signals->end();signal_iter++) {

		if (strcmp((*signal_iter).getName().c_str(),name.c_str())==0) 
			id=(*signal_iter).getId();
			
	}

return id;

}




//std::vector<Constraint>* MultimodalEmotionConstraint::getConstraintsOfOneSignal(std::string name){
//
//	int id=getIdOfSignal(name);
//
//	//find relevant cons	
//	std::vector<Constraint> *good_cons;
//	good_cons = new std::vector<Constraint>;
//
//	std::vector<Constraint>::iterator cons_iter;
//	for(cons_iter=constraints->begin();cons_iter!=constraints->end();cons_iter++) {
//		
//		if (((*cons_iter).getArg1Id()==id)||((*cons_iter).getArg2Id()==id)) 
//		
//			good_cons->push_back(*cons_iter);
//	}
//	return  good_cons;
//}

void MultimodalEmotionConstraint::addConstraintSignal(ConstraintSignal constraintsignal){
	signals->push_back(constraintsignal);
}

//
//void MultimodalEmotionConstraint::addConstraint(Constraint *constraint) {
//	constraints->push_back(*constraint);
//}
	
std::vector<ConstraintSignal>* MultimodalEmotionConstraint::getConstraintSignals(){
	return signals;
}

ConstraintSignal *MultimodalEmotionConstraint::getConstraintSignal(std::string name)
{
	std::vector<ConstraintSignal>::iterator signal_iter;
	for(signal_iter=signals->begin();signal_iter!=signals->end();signal_iter++) 
	{
		if (strcmp((*signal_iter).getName().c_str(),name.c_str())==0) 
			return &((ConstraintSignal)(*signal_iter));
	} //end of for

	return NULL;
}


//the second version of the algorithm


std::vector<ConsNode*>*  MultimodalEmotionConstraint::getNewConstraintsOfOneSignal(std::string name){

	int id=getIdOfSignal(name);

	//find relevant cons	
	std::vector<ConsNode*> *good_cons;
	good_cons = new std::vector<ConsNode*>;

	std::vector<ConsNode*>::iterator cons_iter;
	for(cons_iter = cons_trees->begin();cons_iter!=cons_trees->end();cons_iter++) {
		
		// all the id's of the cons	
		std::vector<int>::iterator concerns_iter;
			
		for(concerns_iter = (*(*cons_iter)).concerns.begin(); concerns_iter!=(*(*cons_iter)).concerns.end();concerns_iter++) {

			//if given id occurs on the list
			//take this nodecons

			if ((*concerns_iter)==id) 
					good_cons->push_back(*cons_iter);		
		}		
	}

	return  good_cons;
}//end of getNewConstraintsOfOneSignal


void MultimodalEmotionConstraint::find_adequate(ConsNode*  consnode, ConsNode*  temp){

	// is an argument
	//
	if ( temp->getArgId()!=-1 )
			consnode->concerns.push_back(temp->getArgId());		
	

	//exists always concerns the other not my element..even if it is on rightside

	if ( (temp->getLeft()!=NULL) && (temp->getConsNodeOperator()!= exist ) ) 
		find_adequate(consnode,temp->getLeft());

	// with include there is deadlock
	// surely it serves for exclude
	// preceded, rightinc, ??

	//TODO: if include contains or it does not work correcly
	//if ( (temp->getRight()!=NULL) && (temp->getConsNodeOperator()!= include ) )		
	
	if (temp->getRight()!=NULL)
		find_adequate(consnode,temp->getRight());
}

void MultimodalEmotionConstraint::addNewConstraint(ConsNode*  consnode)
{	
	//RUN THE TREE
	//ConsNode *temp=consnode;

	find_adequate(consnode,consnode);

	//do {
	//	
	//	//FILL THE (left) "CONCERN LIST"
	//	//check if only the left son or both should be on the list

	//	if (temp->getArgId()!=-1) consnode->concerns.push_back(temp->getArgId());		
	//	
	//	temp=temp->getLeft();
	//	
	//}
	//while (temp!=NULL);
	//
	//temp=consnode;

	//do {
	//	
	//	//FILL THE (right) "CONCERN LIST"
	//	//check if only the left son or both should be on the list

	//	if (temp->getArgId()!=-1) consnode->concerns.push_back(temp->getArgId());
	//			
	//	temp=temp->getRight();		
	//}
	//while (temp!=NULL);

 cons_trees->push_back(consnode);	
}

std::vector<ConsNode*>* MultimodalEmotionConstraint::getNewConstraints()
{   
	if (cons_trees->empty()==true) return NULL;
	return cons_trees;
}


void MultimodalEmotionConstraint::fill_right(int id, float start_time, float stop_time){
	
	std::vector<ConsNode*>::iterator cons_iter;
	
	//for every constree...
	for(cons_iter = cons_trees->begin();cons_iter!=cons_trees->end();cons_iter++) 
		
		(*cons_iter)->fill_right(id,start_time,stop_time);
		

}

void MultimodalEmotionConstraint::fill(int id, float start_time, float stop_time){
	
	std::vector<ConsNode*>::iterator cons_iter;
	
	//for every constree...
	for(cons_iter = cons_trees->begin();cons_iter!=cons_trees->end();cons_iter++) 		
		(*cons_iter)->fill(id,start_time,stop_time);
		
}


void MultimodalEmotionConstraint::clean(){
	
	std::vector<ConsNode*>::iterator cons_iter;
	
	//for every constree...
	for(cons_iter = cons_trees->begin();cons_iter!=cons_trees->end();cons_iter++) 
				(*cons_iter)->clean();
		
}
