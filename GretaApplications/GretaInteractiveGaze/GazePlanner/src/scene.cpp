// Shared-Attention system
// Christopher Peters

#include "scene.h"

extern IniManager inimanager;

extern int SCREENX;
extern int SCREENY;


Scene::Scene(void)
{
	//setup the scene objects	

	Named3DObject  *obj = new Named3DObject("GRETA", OBJECT_GRETA, -0.05, 0.2,-10.0, 0.1, 0.1);
	VAO* vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) 
		printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	//obj = new Named3DObject("BACKGROUND", OBJECT_BACKGROUND, 0.0, 0.0,-100.0, 0.1, 0.1, 0.1);
	//vao = new VAO(obj); obj->vao = vao;
	//if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	//else delete obj;
	
	//obj = new Named3DObject("TABLE", OBJECT_TABLE, 0.0, -0.18,-3.0, 0.3, 0.3, 0.3);
	//vao = new VAO(obj); obj->vao = vao;
	//if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	//else delete obj;
	
	//obj = new Named3DObject("REDBOX", OBJECT_BOX, 0.05, -0.35,  -1.1, 0.8, 0.0, 0.0);
	//vao = new VAO(obj); obj->vao = vao;
	//if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	//else delete obj;

	obj = new Named3DObject("GREENBOX", OBJECT_BOX, -0.5,-0.5, 0.5, 0.1, 0.1);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	obj = new Named3DObject("BLUEBOX", OBJECT_BOX, 0.40, -0.5, 0.5, 0.1, 0.1);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	//obj = new Named3DObject("YELLOWBOX", OBJECT_BOX, -0.4, -0.3,  -1.1, 0.8, 0.8, 0.0);
	//vao = new VAO(obj); obj->vao = vao;
	//if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	//else delete obj;

	outsideScreenVAO = new VAO(NULL, true);

	this->gretaAgent=NULL;
	this->calibrator = new Calibrator();

	this->lastSelectedObjID = -1;

	//example calibration values
	this->bvs.headBottomVal = -0.5;	//-0.463624
	this->bvs.headTopVal = 0.5;		//0.236161
	this->bvs.headLeftVal = -0.5;		//-0.520153
	this->bvs.headRightVal = 0.5;		//0.333079
}

Scene::~Scene()
{
	for (int i = 0; i < this->objects.size(); i++)
	{
		delete this->objects[i];
	}
}

int Scene::pickobject(int x, int y)
{
	printf(" INPUT X %i, INPUT Y %i \n", x,y);

	for (int i = 0; i < this->objects.size(); i++)
	{
		
		printf("OBJECT %i, x1 %f, x2 %f, y1 %f, y2 %f \n", i,this->objects[i]->position[0],objects[i]->position[0]+objects[i]->height, this->objects[i]->position[1], objects[i]->position[1]+objects[i]->width);

		printf("OBJECT %i, x1 %f, x2 %f, y1 %f, y2 %f \n", i,(this->objects[i]->position[0]+0.5)*SCREENX,(objects[i]->position[0]+objects[i]->height+0.5)*SCREENX, ( this->objects[i]->position[1] +0.5)*SCREENY , (objects[i]->position[1]+objects[i]->width+0.5)*SCREENY );


		if (
			( (this->objects[i]->position[0]+0.5)*SCREENX < x)
			&&
		   (  (objects[i]->position[0]+objects[i]->height+0.5)*SCREENX > x)
			&&
		   ( ( this->objects[i]->position[1] +0.5)*SCREENY < y)
			&&
		   (  (objects[i]->position[1]+objects[i]->width+0.5) *SCREENY > y)
			)
		{
			printf("Im looking at  %i \n",objects[i]->getID() );
		
			return this->objects[i]->getID();
		}
	}//end for

	return -1;
}


bool Scene::addNewObject(Named3DObject *no)
{
	//check if the name is there already
	Named3DObject *check = this->getObjectByName(no->getName());
	if (check != NULL) return false;

	this->objects.push_back(no);
	return true;
}

Named3DObject *Scene::getObjectByID(int ID)
{
	for (int i = 0; i < this->objects.size(); i++)
	{
		if (this->objects[i]->getID() == ID) return this->objects[i];
	}
	return NULL;
}

Named3DObject *Scene::getObjectByName(std::string *name)
{
	for (int i = 0; i < this->objects.size(); i++)
	{
		if (this->objects[i]->getName()->compare(name->c_str()) == 0) return this->objects[i];
	}
	return NULL;
}

void Scene::drawSceneObjects(void)
{
	
	if (this->gretaAgent == NULL)
	{
		
		this->gretaAgent = new SharedAttentionAgentAspect("speaker",inimanager.GetValueString("CHARACTER_SPEAKER"),false);//realtime
		this->gretaAgent->AssignFile("empty");
		this->gretaAgent->EnableAudio(false);	
		
		this->gretaAgent->AssignFile("c:\\empty1");
		
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		
		//this->gretaAgent->MoveAgent(0,0,-100);
		//this->gretaAgent->MoveForward(10);
		//this->gretaAgent->RotateAgent(0,180,0);
	
	}

	for (int i = 0; i < this->objects.size(); i++)
	{
		this->drawObjectByIndex(i);
	}

	//this->gretaAgent->draw();		
	
}

void Scene::drawObjectByIndex(int index)
{
	if (this->objects[index] != NULL) 
	{
		if (this->objects[index]->type != OBJECT_GRETA)
		{
			this->objects[index]->draw();
		}
		else	//draw Greta
		{
			//glEnable(GL_LIGHTING);
		
			glLoadName(this->objects[index]->objectID);

			glPushMatrix();
			glTranslatef(this->objects[index]->position[0], this->objects[index]->position[1], this->objects[index]->position[2]);
			
			glScalef(0.007,0.007,0.007);
			
			glEnable(GL_LIGHTING);
			this->gretaAgent->draw();
			glDisable(GL_LIGHTING);

			glPopMatrix();
		}
	}
}
 //!!!
void Scene::updatePickedObjectsVAO(int objectID)
{
	if (objectID < 0) return;
	Named3DObject *obj = NULL;
	if ((obj = this->getObjectByID(objectID)) != NULL)
	{
		if (obj->vao != NULL) obj->vao->update(clock(), this->lastSelectedObjID == objectID);
		this->lastSelectedObjID = objectID;
	}
	else printf("\nError: selected objectID not found...");
}