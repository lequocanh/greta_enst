/*
 *  GRETAException.h
 *  semaine
 *
 *  Created by Marc Schröder on 09.09.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#ifndef GRETAEXCEPTION_H
#define GRETAEXCEPTION_H

#include <string>

namespace excep {

class GRETAException : public std::exception
{
public:
		inline GRETAException(const std::string & text) throw()
		{
			message = text;
		}
		
		inline ~GRETAException() throw() {}

		const char * what() { return message.c_str(); }

protected:
	std::string message;

};

} //excep namespace

#endif

