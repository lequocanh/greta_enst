import com.cmlabs.air.*;


/**
 * Class that starts the connection with psyclone to send FML message and receive message from the dialog engine
 * 
 * @author Magalie
 *
 */
public class Connexion {

	private JavaAIRPlug plug;
	private String ip_number;
	
	private ChatInterface _chatInterface;
	private Message _incomingMessage;
	
	private DialogueManager _dial;
	
	public Connexion(ChatInterface ci){
		this.ip_number="127.0.0.1";
		this._chatInterface=ci;
		
	}
	public void run() {
        this.connect(ip_number,10000);                
        
    }
	
	public void setDialogueManager(DialogueManager dial){
		this._dial=dial;
	}
	
	/**
	 * Connection to psyclone
	 * @param host
	 * @param port
	 */
	public void connect(String host, int port) {
        
        plug = new JavaAIRPlug("SimpleBMLSender", host, port);
        if (!plug.init()) {
            System.out.println("Could not connect to the Server on " + host + " on port " + port + ". Exit.");
            System.exit(1);
        }
        
        
        if (!plug.openTwoWayConnectionTo("Greta.Whiteboard")) {
            System.out.println("Could not open callback connection to Greta.Whiteboard. Exit.");
            System.exit(1);
        }
        
        String xml = "<module name=\"SimpleBMLSender\" allowselftriggering=\"yes\">"+
                "              <spec>"+
                "                 <context name=\"Psyclone.System.Ready\">"+
                 "   <phase id=\"Phase One\">" +
                 "                      <post to=\"Greta.Whiteboard\" type=\"Greta.Data.TEXT.IN\" />" +
                 "						<trigger from=\"Greta.Whiteboard\" type=\"Greta.Data.TEXT.OUT\" />" +
                "                       <post to=\"Greta.Whiteboard\" type=\"Greta.Data.BML\" />" +
                "                       <post to=\"Greta.Whiteboard\" type=\"Greta.Data.FML\" />"  +
                "                       <post to=\"Greta.Whiteboard\" type=\"Greta.RealtimePlayer.Command\" />"   +"</phase>" + "   </context>"+
                "              </spec>"+
                "           </module>";
        
        
        if (!plug.sendRegistration(xml)) {
            System.out.println("Could not register for messages. Exit.");
            System.exit(1);
        } else {
            System.out.println("Ready to send messages...");
        }
        
        /* Thread to wait from incoming message from the dialog engine (the text that Greta should say. 
         * We can receive a specific keyword to trigger a demo or a simple 
         * text message that we have to transform in FML to be said by Greta*/
        Thread th=new Thread(){
        	public void run(){
        		while (true) {
        	     	   if ((_incomingMessage = plug.waitForNewMessage(50)) != null) {
        	     		   System.out.println("--INTEFACE_DIALOGUE: "+_incomingMessage.content+ " type of message " + _incomingMessage.type + " from " +  _incomingMessage.from);
        	     		
        	     		   /* If a message is received, we check if we have to trigger a demo or not */
        	     		   if(ChatInterface._dataBaseDemo.getDemoToExecute(_incomingMessage.content)!=null){
        	     			   /* Trigger of a specific demo, display of the text on teh chat interface associated to the demo */
        	     			  _chatInterface.newGretaMessageToDispay(ChatInterface._dataBaseDemo.getDemoToExecute(_incomingMessage.content).gettextTellByGreta());
        	     		   }else{
        	     			   /* Display on the chat interface of the text Greta will say */
        	     			   _chatInterface.newGretaMessageToDispay(_incomingMessage.content);
        	     		   }
        	     		   /* send the text message to the dialog manager for a conversion in FML */
        	     		   _dial.sendMessageFML(_incomingMessage.content);
        	     	   }
        		}
        	}
        };
        th.start();     
    } 
        
	
	/**
	 * Method to send a BML message on psyclone
	 * 
	 * @param send Message in BML to send on psyclone
	 */
	public void sendBML(String send) {
           Time now = new Time();
            Message msg_ans = new Message("SimpleBMLSender","Greta.Whiteboard", "Greta.Data.BML");
            try{
                msg_ans.content = send;
                boolean posted = plug.postOutputMessage(msg_ans);
                if (!posted) {
                    System.out.println("I could not post message...");                
                }  else {
                    now = new Time();
                    System.out.println("Message posted at:" + now.printTime());     
                }
            }
            catch (Exception myexception){
                System.out.println("I could not generate answer..."+ myexception);                
        }
	}
    
	/**
	 * Method to send a FML message on psyclone
	 * 
	 * @param send Message in FML to send on psyclone
	 */
	public void sendFML(String send) {
            Time now = new Time();
            Message msg_ans = new Message("SimpleBMLSender","Greta.Whiteboard", "Greta.Data.FML");
            try{
                msg_ans.content = send;
                boolean posted = plug.postOutputMessage(msg_ans);
                if (!posted) {
                    System.out.println("I could not post message...");                
                }  else {
                    now = new Time();
                    System.out.println("Message posted at:" + now.printTime());     
                }
            }
            catch (Exception myexception){
                System.out.println("I could not generate answer..."+ myexception);                
        }
    }
    
	/**
	 * Method to send a text message on psyclone
	 * 
	 * @param send Message (simple text without FML or BML tag) to send on psyclone
	 */
	public void sendText(String send) {
        Time now = new Time();
        Message msg_ans = new Message("SimpleBMLSender","Greta.Whiteboard", "Greta.Data.TEXT.IN");
        try{
            msg_ans.content = send;
            boolean posted = plug.postOutputMessage(msg_ans);
            if (!posted) {
                System.out.println("I could not post message...");                
            }  else {
                now = new Time();
                System.out.println("Message posted at:" + now.printTime());     
            }
        }
        catch (Exception myexception){
            System.out.println("I could not generate answer..."+ myexception);                
        }
	}
}
