#pragma once

#include <stdio.h>
#include "altypes.h"
#include "alptr.h"
#include <alproxy.h>
#include <conio.h>
#include <alerror.h>
#include <albroker.h>

using namespace std;

#define PI 3.141

#define AXIS_MASK_X 1
#define AXIS_MASK_Y 2
#define AXIS_MASK_Z 4
#define AXIS_MASK_WX 8
#define AXIS_MASK_WY 16
#define AXIS_MASK_WZ 32

#define INTERPOLATION_TYPE 1
#define	SECONDS 1
#define	MASK  7
#define	SPACE 0 // Task space {SPACE_TORSO = 0, SPACE_WORLD = 1, SPACE_SUPPORT_LEG = 2 }

class NaoMotion
{
public:
	NaoMotion();
	NaoMotion(std::string parentBrokerIP, int parentBrokerPort);
	~NaoMotion(void);

private:
	std::string parentBrokerIP;
	int parentBrokerPort;

	AL::ALProxy *proxy;

	vector<float> ZeroPosition;
	vector<float> InitPosition;

public:
	void Init();
	void SetIP(std::string ip);
	void Setport(int port);
	void StiffnessOn();
	void StiffnessOff();
	void PoseInit();
	void PoseZero();
	void OpenHand();

public:
	void make_motion_left_arm(vector<float> lVec6, float *lHand);
	void make_motion_right_arm(vector<float> rVec6, float *rHand);
};
