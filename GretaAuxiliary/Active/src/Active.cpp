//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// Active.cpp: implementation of the Active class.
//
//////////////////////////////////////////////////////////////////////

#include "Active.h"
#include <iostream>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

using namespace cms;
using namespace semaine::components;
using namespace semaine::cms::sender;
using namespace semaine::cms::receiver;
using namespace semaine::cms::message;

Active::Active(std::string name, std::string host, int port) : CommunicationInterface()
{
	this->name=name;
	printf("connesction: %s...\n", name.c_str());
	comp = new Element(name);
}
	

Active::~Active()
{
	delete comp;
}

int Active::Register(std::string whiteboard, std::string datatype)
{
/*
	receiver = new Receiver(datatype);
	receivers.push_back(receiver);  // to set up properly
	sender = new BMLSender("semaine.data.synthesis.plan", getName());
	senders.push_back(bmlSender);


	//psyclone
	
	std::list<std::string> types;
	types.push_back(datatype);
	return Register(whiteboard,types);
	*/
	return 0;
}

int Active::RawRegister(std::string rawregistrationstring)
{
	/*
	if (!plug1->init())
	{
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name.c_str());
		return 0;
	}
	if (plug1->isServerAvailable())
	{
		plug1->sendRegistration(JString(rawregistrationstring.c_str()));
		plug1->start();
	}
	else {
		printf("Could not connect to Psyclone for registering %s...\n\n", name.c_str());
		return 0;
	}
	printf("Psyclone: Connected to whiteboard\n");
	*/
	return 1;
}

int Active::Register(std::string whiteboard, std::list<std::string> datatypes)
{
	std::list<std::string>::iterator iter;
	for(iter=datatypes.begin();iter!=datatypes.end();iter++)
	{
		if((*iter)!="")
		{
			Receiver *receiver = new Receiver((*iter).c_str());
			comp->receivers.push_back(receiver);  // to set up properly
		}

	}

	return 1;
}

int Active::RegisterSender(std::map<std::string, std::string> sendertypename)
{
	std::map<std::string, std::string>::iterator itermap;
	for(itermap=sendertypename.begin();itermap!=sendertypename.end();itermap++)
	{ 
		if((*itermap).first!="")
		{
			Sender *sender = new Sender((*itermap).second.c_str(), (*itermap).first.c_str(), name.c_str());
			comp->senders.push_back(sender);  // to set up properly
		}
	}

	try {
		std::cerr << "starting component " << name << "..." << std::endl;
		comp->startIO();
		std::cerr << "    ... " << name << " ready." << std::endl;
	} catch (std::exception & ex) {
		comp->log->error("Cannot startup component:", &ex);
		try {
			comp->state = comp->STATE_FAILURE;
			comp->meta.reportState(comp->state, "Cannot startup component:", &ex);
		} catch (CMSException & me) {
			comp->log->error("cannot report failure state", & me);
		}
		return 1;
	}

	return 1;
}



CommunicationMessage* Active::ReceiveMessage(int timeout)
{
	CommunicationMessage* cm1;
	cm1=comp->receive();

	return cm1;
}


std::string Active::ReceiveString(int timeout)
{
	CommunicationMessage* cm1;
	cm1=comp->receive();

	return cm1->getContent();
}

long long Active::get_time()
{
	return comp->meta.getTime();
}

std::string Active::getGretaName(std::string GretaName)
{
	return "";
}

void Active::setIsInputisOutput(bool isInput, bool isOutput)
{
	comp->isInput=isInput;
	comp->isOutput=isOutput;
}

int Active::PostFile(std::string filename, std::string whiteboard, std::string datatype)
{
	
	std::string line;
	std::string tosend="";

	std::ifstream inputfile(filename.c_str());
	if(inputfile.is_open())
	{
		while((inputfile.rdstate()&std::ios::eofbit)==0)
		{
			std::getline(inputfile,line,'\n');
			if(line!="")
			{
				tosend+=line.c_str();
				tosend+="\n";
			}
		}
	}
	else
	{
		printf("File %s not found\n", filename.c_str());
		return 0;
	}
	inputfile.close();

	if(comp->state==comp->STATE_READY)
	{
		std::string type=datatype.substr(datatype.find_last_of(".")+1, datatype.length());
		std::list<Sender *>::iterator iter;
		for(iter=comp->senders.begin();iter!=comp->senders.end();iter++)
		{
			if((*iter)->getDatatype()==type)
				(*iter)->sendTextMessage(tosend.c_str(),comp->meta.getTime());
		}
		return 1;
	}
	else
	{
		printf("no connection!!\n");
		return 0;
	}

}

int Active::PostString(std::string tosend, std::string whiteboard, std::string datatype)
{
	if(comp->state==comp->STATE_READY)
	{
		std::string type=datatype.substr(datatype.find_last_of(".")+1, datatype.length());
		std::list<Sender *>::iterator iter;
		for(iter=comp->senders.begin();iter!=comp->senders.end();iter++)
		{
			if((*iter)->getDatatype()==type)
			{
				(*iter)->sendTextMessage(tosend.c_str(),comp->meta.getTime());
			}
		}
		return 1;
	}
	else
	{
		printf("no connection!!\n");
		return 0;
	}
}

int Active::PostSpeechFromChar(char* tosend, std::string whiteboard, std::string datatype, int *size)
{
	//if(tosend!=NULL)
	//	PostString(tosend, whiteboard, datatype);
	return 1;

/*	DataSample *ds;

	ds=new DataSample("Raw","");
	
	//TO DO : CHANGE IT
	ds->size=(*size);	
	ds->data=tosend;

	//ds->data=very_long_speech;
	//ds->size=very_long;		

	Message* msg = new Message(JString(this->name.c_str()) , JString(whiteboard.c_str()), JString(datatype.c_str()));

	msg->object=ds;

	plug1->postMessage(JString(whiteboard.c_str()),msg,"");
	
	return 1;*/
} 


int Active::SendFAPs(std::string fapfilename, std::string whiteboard, std::string datatype)
{
	this->PostFile(fapfilename,whiteboard,datatype);
	return 1;
}

int Active::SendBAPs(std::string bapfilename, std::string whiteboard, std::string datatype)
{
	PostFile(bapfilename,whiteboard,datatype);
	return 1;
}


int Active::SendBinaryFile(std::string binfilename, std::string whiteboard, std::string datatype)
{
	/*
	std::string line;
	JString tosend="";
	int size;
	
	FILE *inputfile;

	inputfile=fopen(binfilename.c_str(),"rb");

	DataSample *ds;

	int i;
	i=0;

	if(inputfile!=0)
	{

		fseek(inputfile, 0, SEEK_END);
		size = ftell(inputfile);
		fseek(inputfile, 0, SEEK_SET);
		
		ds=new DataSample("Raw","");
		ds->data=(char*)malloc(size+1);

		while(!feof(inputfile))
		{
			ds->data[i]=(char)fgetc(inputfile);
			i++;
		}
	}
	else
	{
		printf("Binary file %s not found\n", binfilename.c_str());
		return 0;
	}

	fclose(inputfile);
	
	ds->size=i;

	Message* msg = new Message(JString(this->name.c_str()), JString(whiteboard.c_str()), JString(datatype.c_str()));
	
	msg->object=ds;
	
	plug1->postMessage(JString(whiteboard.c_str()),msg,"");

	//delete ds->data;
	*/
	return 1;
}


int Active::WriteBinaryFile(std::string filename, CommunicationMessage *msg)
{
	//save data to a wave file
	FILE *f;
	char *audio;
	int size;
	f=fopen(filename.c_str(),"wb");
	if(f!=0)
	{
		audio=msg->getData();
		size=msg->getSize();

		if(audio!=0)
		{
			fwrite(audio, 1, size, f);
			//for(int i=0;i<size;i++)
			//	fputc(audio[i],f);
			//sprintf_s((char*)agent->wav_filename.c_str(),filename.c_str());
		}
		else
		{
			printf("ERROR: received empty wavefile!\n");
			return 0;
		}
		fclose(f);
	}
	else
		return 0;
	return 1;
}
