// Shared-Attention system
// Christopher Peters

#include "glviewer.h"

int GLscreenXRes;
int GLscreenYRes;

GLViewer::GLViewer(int X, int Y, int W, int H, const char *L) : Fl_Gl_Window(X, Y, W, H, L) 
{
	mode(FL_RGB | FL_ALPHA | FL_DEPTH | FL_DOUBLE);

	//this->viewPort[0] = X;	//X,Y is offset of OpenGL port from top-left of window,
	//this->viewPort[1] = Y;    //but not same as GL viewport params
	this->viewPort[0] = 0;
	this->viewPort[1] = 0;
	this->viewPort[2] = 1217; //1738;	//who knows why ... setting to window W,H values above does not work correctly (gives bigger window) - these vals are corrrect
	this->viewPort[3] = 753; //1154	//could be to do with maximising window...

	//this->viewPort[2] = 1738;	//who knows why ... setting to window W,H values above does not work correctly (gives bigger window) - these vals are corrrect
	//this->viewPort[3] = 1154;	//could be to do with maximising window...

	this->nearPlane = 1.0;

	GLscreenXRes = X;
	GLscreenYRes = Y;

	/*
	RECT rect; HWND hwnd; GetClientRect(hwnd, &rect); 
	// rect.top and rect.left will always be 0, 0, respectively.    The width and height are in rect.right and rect.bottom. 
	
	//For the screen size in pixels on Win32:
	int width = GetSystemMetrics(SM_CXSCREEN); int height = GetSystemMetrics(SM_CYSCREEN); 
	*/
	this->lastDisplay = clock();
	this->displayFrequencySec = 0.1;	//check for a message every 100 ms

	light_ambient_0[0] = 0.2;
	light_ambient_0[1] = 0.2;
	light_ambient_0[2] = 0.2;
	light_ambient_0[3] = 1.0;//{0.2, 0.2, 0.2, 1.0}; //{0.15, 0.15, 0.15, 1.0};  //0.4
	light_diffuse_0[0] = 0.7;
	light_diffuse_0[1] = 0.7;
	light_diffuse_0[2] = 0.7;
	light_diffuse_0[3] = 1.0;//{0.90, 0.80, 0.80, 1.0};//{0.70, 0.60, 0.60, 1.0};//{0.80, 0.80, 0.80, 1.0};
	light_specular_0[0] = 1.0;
	light_specular_0[1] = 1.0;
	light_specular_0[2] = 1.0;
	light_specular_0[3] = 1.0;
	light_position_0[0] = -20.0;
	light_position_0[1] = 60.1;
	light_position_0[2] = 50.2;
	light_position_0[3] = 1.0; // lower the light for highlight effect on eyes

	this->selectedID = -1;
	this->selectedSubID = -1;

	this->theScene = new Scene();
	this->fixationList = new FixationList();
	this->testMode = false;
	this->currentFixation = NULL;
}

std::string *GLViewer::getSelectedIDObjectName(void)
{
	if (this->selectedID == -1) return NULL;
	if (this->theScene->getObjectByID(selectedID) == NULL) return NULL;
	return (this->theScene->getObjectByID(selectedID)->getName());
}

void GLViewer::InitializeGL()
	{
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	//glDepthFunc(GL_ALWAYS);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_CULL_FACE);
	glShadeModel(GL_SMOOTH);
	glClearColor(.1f, .1f, .1f, 1);
	glEnable(GL_DEPTH_TEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient_0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse_0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular_0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position_0);	
}

void GLViewer::draw()
{
	static float t = 0.0;
	static bool firstTime = true;
	if (firstTime) {
		InitializeGL();
		firstTime = false;
	}// if

	glClearColor(0,0,0,0);          // clear the window to black
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the color and depth buffer
	// view transformations
	// draw something
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, this->viewPort[2], this->viewPort[3]);

	this->applyProjection();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();	
	
	//need to get these in order to do unprojection in other code later on
	glGetDoublev(GL_MODELVIEW_MATRIX, this->model_view);
	glGetDoublev(GL_PROJECTION_MATRIX, this->projection);
	glGetIntegerv(GL_VIEWPORT, this->viewPort);

	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	if (this->theScene->calibrator->isCalibrateModeEnabled)
	{
		this->drawCursor(this->theScene->calibrator->currentCursorPosition);
	}
	else
	{
		//draw stuff here
		this->theScene->drawSceneObjects();

		//draw overlays and hints
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		
		this->drawFixations();
		//draw the cursor according to the current fixation
		this->drawCrosshair();
	}

	//this->drawBasis();
	
	this->drawAttentionGraph();
	this->drawNormalisedAttentionGraph();

	glPopMatrix();

	glFlush();
}

void GLViewer::drawCursor(int cursorPos)
{
	glPushMatrix();
	switch(cursorPos)
	{
		case CURSOR_CENTER:
			
			break;
		case CURSOR_LEFT:
			glTranslatef(-0.40,0.0,0.0);	
			break;
		case CURSOR_RIGHT:
			glTranslatef(0.40,0.0,0.0);	
			break;
		case CURSOR_UP:
			glTranslatef(0.0,0.40,0.0);	
			break;
		case CURSOR_DOWN:
			glTranslatef(0.0,-0.40,0.0);	
			break;
		case CURSOR_TOPLEFT:
			glTranslatef(-0.40,0.40,0.0);
			break;
		case CURSOR_TOPRIGHT:
			glTranslatef(0.40,0.40,0.0);
			break;
		case CURSOR_BOTTOMRIGHT:
			glTranslatef(0.40,-0.40,0.0);
			break;
		case CURSOR_BOTTOMLEFT:
			glTranslatef(-0.40,-0.40,0.0);
			break;
	}
	if (this->theScene->calibrator->currentCursorState == CURSOR_READY) glColor3f(1.0,1.0,1.0);
	else glColor3f(1.0,0.0,0.0);

	glPointSize(30.0);
	glBegin(GL_POINTS);
		glVertex3f(0.0,0.0,-2.0);
	glEnd();
	glPopMatrix();
}

void GLViewer::enableCalibrationMode(void)
{
	this->theScene->calibrator->reset();	//reset
	this->theScene->calibrator->enable();	//and then start
}

void GLViewer::drawCrosshair(void)
{
	int xIn, yIn;
	if (this->currentFixation == NULL)
	{
		xIn = this->viewPort[2] / 2.0; 
		yIn = this->viewPort[3] / 2.0;
	}
	else 
	{
		xIn = this->currentFixation->screenCoords[0];
		yIn = this->currentFixation->screenCoords[1];
	}
	double posX, posY, posZ;
	this->screenToWorldspaceCoords(xIn, yIn, posX, posY, posZ);
	//printf("\nCursor drawn at %lf %lf (screen coords %d %d)", posX, posY, this->cursorScreenX, this->cursorScreenY);

	//int x = this->cursorScreenX;
	//int y = this->cursorScreenY;
	//if x,y is off the screen, then draw an arrow
	//if ((x > this->viewPort[2]) || (y > this->viewPort[3]) || (x < 0.0) || (y < 0.0))
	if (currentFixation != NULL)
	{
		if (currentFixation->outsideScreen)
		{
			//   0   up
			//   1   top left
			//   2   left
			//   3   bottom left
			//   4   down
			//   5   bottom right
			//   6   right
			//   7   top right
			
			int w = -1;
			if (this->currentFixation->outsideLeft)
			{
				if (this->currentFixation->outsideBottom) w = 3;  //bottom left
				else if (this->currentFixation->outsideTop) w = 1;//top left
				else w = 2; //left
			}
			else if (this->currentFixation->outsideRight)
			{
				if (this->currentFixation->outsideBottom) w = 5;  //bottom right
				else if (this->currentFixation->outsideTop) w = 7;//top right
				else w = 6; //right
			}
			else
			{
				if (this->currentFixation->outsideBottom) w = 4;  //down
				else if (this->currentFixation->outsideTop) w = 0;//up
			}
			//find out what side of the screen and draw an arrow

			float rot = w * 45.0;
			glPushMatrix();

				glTranslatef(0,0, - (nearPlane + 0.0001));
				this->drawArrow(rot);

			glPopMatrix();
			return;
		}
	}	//currentFixation != NULL

	//draw the crosshair
	float start = 0.15;
	float end = 0.5;

	glPushMatrix();

		glTranslatef(posX,posY, - (nearPlane + 0.0001));
		glScalef(0.02, 0.02, 0.02);

		glLineWidth(4.0);
		glColor3f(1.0,0.0,0.0);
		glBegin(GL_LINES);
			glVertex3f(0.0,start,0.0);
			glVertex3f(0.0,end,0.0);

			glVertex3f(0.0,-start,0.0);
			glVertex3f(0.0,-end,0.0);

			glVertex3f(-start,0.0,0.0);
			glVertex3f(-end,0.0,0.0);

			glVertex3f(start,0.0,0.0);
			glVertex3f(end,0.0,0.0);
		glEnd();

	glPopMatrix();
}

int GLViewer::handle(int event)
{

  static int key = 0;

  switch(event) 
  {
	case FL_ENTER:
		//printf("\nEvent enter");
		this->take_focus();
		if (this->testMode) window()->cursor(FL_CURSOR_CROSS);
		else window()->cursor(FL_CURSOR_DEFAULT);
		return(1);	  
	case FL_LEAVE:
		//printf("\nEvent leave");
		window()->cursor(FL_CURSOR_DEFAULT);		// 'window()->cursor()' needed on WIN32 for 1.1.6 and older.
		return(1);
	case FL_PUSH:
		make_current();
		//set mouse cursor accordingly
		if (this->testMode) window()->cursor(FL_CURSOR_CROSS);
		else window()->cursor(FL_CURSOR_DEFAULT);

		if (this->testMode) this->createCurrentFixation(Fl::event_x(),this->viewPort[3]-Fl::event_y());

		/*
		if (Fl::event_button() == FL_LEFT_MOUSE)
		{
			//do picking stuff here
			pickObject(Fl::event_x(),Fl::event_y());

			printf("\nSelected ID is %d", this->selectedID);
			if (this->theScene->getObjectByID(selectedID) != NULL)
			{
				printf(" (%s)", this->theScene->getObjectByID(selectedID)->getName()->c_str());
			}
			//if its a hierarchical name, then get the second component			
		}
		*/
		redraw();
		return(1);
	case FL_DRAG:
		//set mouse cursor accordingly
		return 1;
	case FL_RELEASE:
		return 1;
	case FL_FOCUS: //focus and unfocus MUST be defined here in order for keyboard events to be picked
				   //up from FL_KEYBOARD
		return 1;
	case FL_UNFOCUS:
		return 1;
	case FL_KEYBOARD:	
		key = Fl::event_key();
		switch (key) {

			case 's': //change to space bar
				if (!this->theScene->calibrator->isCalibrateModeEnabled)
				{
					this->enableCalibrationMode();
				}
				return 1;
			case 'c':
				if (this->theScene->calibrator->isCalibrateModeEnabled)
				{
					this->theScene->calibrator->startRecording(clock());
				}
				this->redraw();
				return 1;
		
			case FL_Left:

				return 1;

			case FL_Right:

				return 1;
			}
	case FL_SHORTCUT:
		return 0;

	default:
		//printf("\nEvent was %d",event,FL_KEYBOARD);
		return Fl_Gl_Window::handle(event);
	}        
}

void GLViewer::drawBasis(void)
{
	glLineWidth(3.0);
	glPointSize(10.0);
	
	glBegin(GL_LINES);
		glColor3f(1.0,0.0,0.0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(1.0,0.0,0.0);

		glVertex3f(1.0,0.0,0.0);
		glVertex3f(0.75,0.25,0.0);
		glVertex3f(1.0,0.0,0.0);
		glVertex3f(0.75,-0.25,0.0);


		glColor3f(0.0,1.0,0.0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,1.0,0.0);

		glVertex3f(0.0,1.0,0.0);
		glVertex3f(-0.25,0.75,0.0);
		glVertex3f(0.0,1.0,0.0);
		glVertex3f(0.25,0.75,0.0);


		glColor3f(0.0,0.0,1.0);
		glVertex3f(0.0,0.0,0.0);
		glVertex3f(0.0,0.0,1.0);

    	glVertex3f(0.0,0.0,1.0);
		glVertex3f(0.0,-0.25,0.75);
		glVertex3f(0.0,0.0,1.0);
		glVertex3f(0.0,0.25,0.75);

	glEnd();
	
	glBegin(GL_POINTS);
		glColor3f(1.0,1.0,1.0);
		glVertex3f(0.0,0.0,0.0);
	glEnd();

	glLineWidth(1.0);
}

void GLViewer::applyProjection(void)
{
	//this is used both when drawing objects and when calculating hit locations

	//glFrustum( -0.5, 0.5, -0.5, 0.5, nearPlane, 1000.0);
	//gluPerspective(45.0, this->viewPort[2] / this->viewPort[3], 0.1, 100.0);
	glOrtho(-0.5, 0.5, -0.5, 0.5, 0.1, 1000.0);

	//gluPerspective(45.0f, (GLfloat) (viewPort[2]-viewPort[0])/(GLfloat) (viewPort[3]-viewPort[1]), nearPlane, 1000.0f);
}

// take this
int processHits (GLint hits, GLuint buffer[])
{
   unsigned int i, j;
   GLuint names, *ptr, minZ,*ptrNames, numberOfNames;

   if (hits == 0) return -1;
   //printf ("Hits = %d\n", hits);
   ptr = (GLuint *) buffer;
   minZ = 0xffffffff;

   for (i = 0; i < hits; i++) {	
	  printf("\nHit %d: ",i);
      names = *ptr;
	  printf("%d Names, ",names);
	  ptr++;
	  printf("Depth of %d ",*ptr);
	  if (*ptr < minZ) {
		  numberOfNames = names;
		  minZ = *ptr;
		  ptrNames = ptr+2;
	  }
	  ptr += names+2;
	}
  //printf ("Number of names: %d", numberOfNames);
  if (numberOfNames == 0) return -1;
  //printf ("The closest hit names are ");
  ptr = ptrNames;
  for (j = 0; j < numberOfNames; j++,ptr++) {
     //printf ("%d ", *ptr);
  }
  //printf ("\n");
  return (*ptrNames);
}

int GLViewer::pickObject(int screenX, int screenY)
{
  long hits;
  GLuint selectBuf[1024];
  GLuint closest;
  GLuint dist;

  glSelectBuffer(1024, selectBuf);
  (void) glRenderMode(GL_SELECT);
  glInitNames();

  /* Because LoadName() won't work with no names on the stack */
  glPushName(0);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //here, 4,4 is the picking region - too small may make it hard to get small objs, too large
  //may result in too many hits  
  //(doubleX, doubleY) center of picking region in window coordinates
  //(delX, delY) width and height of picking region in window coordinates
  gluPickMatrix(screenX, this->viewPort[3] - screenY, 1, 1, (GLint *)this->viewPort);

  //drawing stuff from the main draw function
  this->applyProjection();

  glMatrixMode(GL_MODELVIEW);

  //Apply camera transformation here

  ///draw stuff here
  this->theScene->drawSceneObjects();

 hits = glRenderMode(GL_RENDER);
  //printf("\nGot %d hits",hits);

  int hit = processHits(hits, selectBuf);
	this->selectedID = hit;
	return hit;
}

void GLViewer::screenToWorldspaceCoords(const int x, const int y, double &pos3D_x, double &pos3D_y, double &pos3D_z)
{
	//here (0,0) is the bottom left of the screen

	// get 3D coordinates based on window coordinates	
	gluUnProject(x, y, 0.01, this->model_view, this->projection, this->viewPort,&pos3D_x, &pos3D_y, &pos3D_z);
	//printf("\nWS position is %lf %lf %lf", pos3D_x, pos3D_y, pos3D_z);

	/*
	//get window coordiantes from 3D coordinates
	GLdouble win_x, win_y, win_z;
	gluProject(1.0,1.0,-1.0, this->model_view, this->projection, this->viewPort, &win_x, &win_y, &win_z);
	printf("\nScreen coords are %lf %lf %lf", win_x, win_y, win_z);
	*/
}

void GLViewer::drawFixations(void)
{
	FixationEntry *fe = NULL;
	long diffNew = 0;
	long diffOld = 0;
	float redShadeOld = 0.0;
	float redShadeNew = 0.0;

	double prevPos3D_x; double prevPos3D_y; double prevPos3D_z;
	double pos3D_x; double pos3D_y; double pos3D_z;
	for (int i = 0; i < this->fixationList->fixationEntries.size(); i++)
	{
		if (i > 0)
		{
			fe = this->fixationList->fixationEntries[i];
			if (fe != NULL)
			{
				//get the difference between current time and time of fixation
				diffNew = clock() - fe->time;
				redShadeNew = 1.0 / (diffNew / CLOCKS_PER_SEC);
				if (redShadeNew < 0.2) redShadeNew = 0.2; 

				diffOld = clock() - this->fixationList->fixationEntries[i-1]->time;
				redShadeOld = 1.0 / (diffOld / CLOCKS_PER_SEC);
				if (redShadeOld < 0.2) redShadeOld = 0.2;

				glBegin(GL_LINES);
					glColor3f(redShadeOld, 0.0,0.0);
					glVertex3f(prevPos3D_x, prevPos3D_y, prevPos3D_z);
					screenToWorldspaceCoords(fe->screenCoords[0], fe->screenCoords[1], pos3D_x, pos3D_y, pos3D_z);
					glColor3f(redShadeNew, 0.0,0.0);
					glVertex3f(pos3D_x, pos3D_y, pos3D_z);
				glEnd();

				prevPos3D_x = pos3D_x; prevPos3D_y = pos3D_y; prevPos3D_z = pos3D_z;
			}
		}
		else
		{
			fe = this->fixationList->fixationEntries[i];
			if (fe != NULL)
			{
				this->screenToWorldspaceCoords(fe->screenCoords[0], fe->screenCoords[1], prevPos3D_x, prevPos3D_y, prevPos3D_z);
			}
		}
	}
}
// take this
void GLViewer::convertGazeDirToScreenCoords(float xDir, float yDir)
{
	//given a calibrated boundary, gets the screen coordinates in terms of the xDir and yDir
	//remember xDir is rotation around xAxis
	float xRange = this->theScene->bvs.headRightVal - this->theScene->bvs.headLeftVal;
	float yRange = this->theScene->bvs.headTopVal - this->theScene->bvs.headBottomVal;

	int xScreen, yScreen;

	//test to see if it is outside screen extents
	bool xOutside = false;
	bool yOutside = false;
	bool outsideLeft = false;
	bool outsideRight = false;
	bool outsideBottom = false;
	bool outsideTop = false;
	if (yDir < this->theScene->bvs.headLeftVal) {xScreen = -1; outsideLeft = true; xOutside = true;}
	else if (yDir > this->theScene->bvs.headRightVal) {xScreen = this->viewPort[2] + 1; outsideRight = true; xOutside = true;}

	if (xDir < this->theScene->bvs.headBottomVal) {yScreen = -1; outsideBottom = true; yOutside = true;}
	else if (xDir > this->theScene->bvs.headTopVal) {yScreen = this->viewPort[3] + 1; outsideTop = true; yOutside = true;}

	float xPercent, yPercent;
	if (!xOutside)
	{
		xPercent = (yDir - this->theScene->bvs.headLeftVal)/xRange;
		xScreen = xPercent * (float)this->viewPort[2];
		//xScreen = this->viewPort[2]/2;
	}
	if (!yOutside)
	{
		yPercent = (xDir - this->theScene->bvs.headBottomVal)/yRange;
		yScreen = yPercent * (float)this->viewPort[3];
		//yScreen = this->viewPort[3]/2;
	}
	//printf("\nxDir: %3.5f yDir: %3.5f converted to screen coords (%d, %d)", xDir, yDir, xScreen, yScreen);

	//create a fixation from the screen coordinates
	this->createCurrentFixation(xScreen, yScreen, outsideLeft, outsideRight, outsideTop, outsideBottom);
	
	this->damage(1);
}

void GLViewer::createCurrentFixation(int xScreen, int yScreen, bool outsideLeft, bool outsideRight, bool outsideTop, bool outsideBottom)
{
	bool outsideScreen = outsideLeft || outsideRight || outsideTop || outsideBottom;
	if (!outsideScreen)
	{
		//see what object falls under those coordinates, putting the result into 'selectedID'
		pickObject(xScreen,this->viewPort[3] - yScreen);


		//!!!
		//update the objects associated VAO
		this->theScene->updatePickedObjectsVAO(this->selectedID);
	}
	else
	{
		this->selectedID = -1;
		//update the special 'outside' VAO
		this->theScene->outsideScreenVAO->update(clock(), this->theScene->lastSelectedObjID == -1);
		this->theScene->lastSelectedObjID = -1;
	}

	//add the fixation to a list and store it as the current one
	this->currentFixation = this->fixationList->addFixation(clock(), xScreen, yScreen, this->selectedID, outsideLeft, outsideRight, outsideTop, outsideBottom);
}

void GLViewer::drawArrow(float rotation)
{
	glPushMatrix();
	glLineWidth(4.0);
	glColor3f(1.0,0.0,0.0);

	glRotatef(rotation, 0,0,1);
	glScalef(0.1,0.1,0.1);

	float size = 1.0;
	glBegin(GL_LINES);

		glVertex3f(0.0, (size * 0.5), 0.0);
		glVertex3f(0.0, -(size * 0.5), 0.0);

		glVertex3f(0.0, (size * 0.5), 0.0);
		glVertex3f(-0.2, (size * 0.5) -0.4, 0.0);

		glVertex3f(0.0,  (size * 0.5), 0.0);
		glVertex3f(0.2, (size * 0.5)  -0.4, 0.0);
	glEnd();

	glPopMatrix();
}

void GLViewer::drawAttentionGraph(void)
{
	if (this->fixationList->fixationEntries.size() == 0) return;
	long totalTimeSoFar = clock() - this->fixationList->startTime;

	glPushMatrix();
	//glTranslatef(-4.0,4.0,-1.0);
	glTranslatef(-0.464,0.32,-1.0);

	glBegin(GL_POLYGON);
		glColor3f(1.0,1.0,1.0);
		glVertex2f(-0.01,-0.01);
		glVertex2f(0.15,-0.01);
		glVertex2f(0.15,0.11);
		glVertex2f(-0.01,0.11);
	glEnd();

	float height = 0.0;
	float xPos = 0.0;
	long lookedAtTime = 0;

	float barWidth = 0.01;
	float barSpacing = 0.02;
	float maxBarHeight = 0.1;
	//for each object
	for (int i = 0; i < this->theScene->objects.size(); i++, xPos += barSpacing)
	{
		lookedAtTime = this->theScene->objects[i]->vao->totalTimeLookedAt;
		height = ((double)lookedAtTime/(double)totalTimeSoFar) * maxBarHeight;
		printf("\nTime looked at for object %d: %d Total: %d (height is %f)", i, lookedAtTime, totalTimeSoFar, height);
		glColor3f(this->theScene->objects[i]->colour[0], this->theScene->objects[i]->colour[1], this->theScene->objects[i]->colour[2]);
		
		glBegin(GL_POLYGON);
			glVertex2f(xPos,0.0);
			glVertex2f(xPos + barWidth, 0.0);
			glVertex2f(xPos + barWidth, height);
			glVertex2f(xPos, height);
		glEnd();
	}
	glPopMatrix();

	printf("\nTotal time so far: %d", totalTimeSoFar);
}

void GLViewer::drawNormalisedAttentionGraph(void)
{
	if (this->fixationList->fixationEntries.size() == 0) return;
	long totalTimeSoFar = clock() - this->fixationList->startTime;

	glPushMatrix();
	//glTranslatef(-4.0,4.0,-1.0);
	glTranslatef(-0.464,0.12,-1.0);

	glBegin(GL_POLYGON);
		glColor3f(1.0,1.0,1.0);
		glVertex2f(-0.01,-0.01);
		glVertex2f(0.15,-0.01);
		glVertex2f(0.15,0.11);
		glVertex2f(-0.01,0.11);
	glEnd();

	float height = 0.0;
	float xPos = 0.0;
	long lookedAtTime = 0;

	float barWidth = 0.01;
	float barSpacing = 0.02;
	float maxBarHeight = 0.1;

	std::vector<float> vals;
	float maxSoFar = -1.0;
	float val = 0.0;
	//for each object
	for (int i = 0; i < this->theScene->objects.size(); i++, xPos += barSpacing)
	{
		lookedAtTime = this->theScene->objects[i]->vao->totalTimeLookedAt;
		val = (double)lookedAtTime/(double)totalTimeSoFar;
		vals.push_back(val);
		if (val > maxSoFar) maxSoFar = val;
	}

	xPos = 0.0;
	for (int i = 0; i < this->theScene->objects.size(); i++, xPos += barSpacing)
	{
		glColor3f(this->theScene->objects[i]->colour[0], this->theScene->objects[i]->colour[1], this->theScene->objects[i]->colour[2]);
		
		height = (vals[i]/maxSoFar) * maxBarHeight;

		glBegin(GL_POLYGON);
			glVertex2f(xPos,0.0);
			glVertex2f(xPos + barWidth, 0.0);
			glVertex2f(xPos + barWidth, height);
			glVertex2f(xPos, height);
		glEnd();
	}
	glPopMatrix();
}

void GLViewer::idle(void)
{
	if (this->theScene->calibrator->isCalibrateModeEnabled) this->theScene->calibrator->update(clock());
	this->damage(1);
}