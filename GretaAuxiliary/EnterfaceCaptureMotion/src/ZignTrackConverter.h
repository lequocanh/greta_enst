#pragma once 

#include "Converter.h"

class ZignTrackConverter : public Converter
{
	public:

	ZignTrackConverter();
	virtual ~ZignTrackConverter();

	bool readMCMessage(char *mcfilename);
    std::vector<FAPFrame> *converttoFAPs();


	//first code not used any more
	bool oldreadMCMessage(char *mcfilename);
	std::vector<FAPFrame> *oldconverttoFAPs();

	//other
	bool BReadMCMessage(char *mcfilename);
	std::vector<FAPFrame> *HeadRotationConverttoFAPs();


private:
	

};