//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// WoZWindow.cpp: implementation of the WoZWindow class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include "WoZWindow.h"
#include "FL/Fl.H"
#include "GretaLogger.h"

using namespace cmlabs;

extern std::list<GretaLogger*> listLog;

extern IniManager inimanager;

void selectedMimicryValue(Fl_Widget* w, WoZWindow* v)
{
	char value[255];
	sprintf(value,"%.2f",v->mimic_level->value());
	v->mimic_level_output->value(value);
}


WoZWindow::WoZWindow():Fl_Window(200,200,600,330,"Wizard of Oz")
//WoZWindow::WoZWindow():Fl_Window(200,200,300,280,"Wizard of Oz")
{
	int i;
	std::string host;
	int port;
	char value[256];
	
	inimanager.ReadIniFile("greta.ini");

	raise_eyebrows_prob=0.5;
	nod_prob[0]=0.18;
	nod_prob[1]=0.18;
	nod_prob[2]=0.18;
	smile_prob=0.6;

	randgen = new RandomGen();

	/*
	
	mimic_level=new Fl_Slider(40,300,150,20,"mimicry level");
	mimic_level->type(FL_HOR_NICE_SLIDER);
	mimic_level->align(FL_ALIGN_TOP|FL_ALIGN_LEFT);
	mimic_level->bounds(0,1);
	mimic_level->step(0.1);
	mimic_level->value(0.5);
	mimic_level->callback((Fl_Callback *)selectedMimicryValue,this);
	mimic_level_output=new Fl_Output(192,300,40,20);
	sprintf(value,"%.2f",mimic_level->value());
	mimic_level_output->value(value);

	sendMimicryLevel=new Fl_Button(235,300,50,20,"send");

	*/

	sendPoppy=new Fl_Button(40,20,100,100,"POPPY");
	sendSpyke=new Fl_Button(40,140,100,100,"SPYKE");
	sendPrudence=new Fl_Button(160,20,100,100,"PRUDENCE");
	sendObadiah=new Fl_Button(160,140,100,100,"OBADIAH");

	sendPositive=new Fl_Button(300,20,100,100,"POSITIVE");
	sendNegative=new Fl_Button(300,140,100,100,"NEGATIVE");

	//sendObadiah->hide();
	//sendPrudence->hide();
	
	sendturn=new Fl_Button(450,20,100,50,"stop");
	sendSmile=new Fl_Button(460/*+i*100*/,110,80,30,"smile");
	sendBC=new Fl_Button(460/*+i*100*/,160,80,30,"BC");

	sendEvent=new Fl_Button(460,210,80,30,"event");

	/*
	smileLabel[0]="S";
	smileLabel[1]="S+R";
	smileLabel[2]="S+N";
	smileLabel[3]="S+R+N";

	for(i=0;i<4;i++)
		sendSmile[i]=new Fl_Button(190+i*100,50,80,30,smileLabel[i].c_str());

	for(i=4;i<8;i++)
		sendSmile[i]=new Fl_Button(190+(i-4)*100,150,80,30,smileLabel[i-4].c_str());
	*/

	host=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	port=inimanager.GetValueInt("PSYCLONE_PORT");

	psydule=new Psydule("Greta.WoZWindow",host,port);

	Positive=new AgentState(LoadAgentState("listener/Positive.xml"));
	Negative=new AgentState(LoadAgentState("listener/Negative.xml"));
	Poppy=new AgentState(LoadAgentState("listener/Poppy.xml"));
	Spyke=new AgentState(LoadAgentState("listener/Spyke.xml"));
	Prudence=new AgentState(LoadAgentState("listener/Prudence.xml"));
	Obadiah=new AgentState(LoadAgentState("listener/Obadiah.xml"));

//CLOCK_MESSAGE_HEADER
	if(psydule->Register("Greta.Whiteboard","")!=0)
		printf("registered to Psyclone\n");
	else
	{
		printf("could not register to Psyclone\n");
		psydule=0;
	}

	psyduleOutput=new Psydule("Greta.WoZWindowOutput",host,port);

	if(psyduleOutput->Register("DataInput.Whiteboard","")!=0)
		printf("registered to DataInput Psyclone\n");
	else
	{
		printf("could not register to DataInput Psyclone\n");
		psyduleOutput=0;
	}
	
}

WoZWindow::~WoZWindow()
{
}

void WoZWindow::draw()
{
	Fl_Window::draw();
}

int WoZWindow::handle(int e)
{
	int i;
	/*
	if(Fl::event_key()==FL_Left)
	{
		SendBML(1);
		printf("va a cagare!!\n");
	}
	if(Fl::event_key()==FL_Right)
	{
		SendBML(0);
		printf("va a cagare due volte!!\n");
	}
	*/

	if(Fl::event_button()==FL_LEFT_MOUSE)
	{
		if(e==FL_PUSH)
		{
	//		for (i=0;i<8;i++)
	//		{
				if(Fl::event_inside(sendSmile))
				{
					SendBML(1);
					//break;
				}
	//		}

			if(Fl::event_inside(sendBC))
			{
				SendBML(0);
			}
			if(Fl::event_inside(sendPositive))
			{
				SendAgentState("Positive");
			}
			if(Fl::event_inside(sendNegative))
			{
				SendAgentState("Negative");
			}
			if(Fl::event_inside(sendturn))
			{
				SendAgentTurn((std::string)sendturn->label());
				if(sendturn->label()=="stop")
					sendturn->label("start");
				else
					sendturn->label("stop");
				this->redraw();
			}
			if(Fl::event_inside(sendEvent))
			{
				SendEvent();
			}
			if(Fl::event_inside(sendPoppy))
			{
				SendAgentState("Poppy");
			}
			if(Fl::event_inside(sendSpyke))
			{
				SendAgentState("Spyke");
			}
			
			if(Fl::event_inside(sendPrudence))
			{
				SendAgentState("Prudence");
			}
			if(Fl::event_inside(sendObadiah))
			{
				SendAgentState("Obadiah");
			}
//			if(Fl::event_inside(sendMimicryLevel))
//			{
//				SendMimicryLevel();
//			}
		}
	}

	return Fl_Window::handle(e);
}


void WoZWindow::SendEvent()
{
	psyduleOutput->PostString("head:head_nod;", "DataInput.Whiteboard", "Greta.Data.Input");
}

void WoZWindow::SendBML(int smile)
{
	std::string bml;
	bml=WriteBML(smile);
	psydule->PostString(bml, "Greta.Whiteboard", "Greta.Data.BML");
}

std::string WoZWindow::LoadAgentState(std::string filepath)
{
	std::string state;
	std::string line;
	if(filepath=="")
	{
		printf("WoZ: error parsing %s\n",filepath.c_str());
		return "";
	}
	std::ifstream inputfile(filepath.c_str());
	if(inputfile.is_open())
	{
		while((inputfile.rdstate()&std::ios::eofbit)==0)
		{
			std::getline(inputfile,line,'\n');
			state=state+line+"\n";
		}
		inputfile.close();
	}
	else 
		return "";
	return state;
}

void WoZWindow::SendAgentTurn(std::string inturn)
{
	std::string turn;
	if(inturn=="start")
		turn="agent_turn=listener";
	else
		turn="agent_turn=speaker";
	psydule->PostString(turn, "Greta.Whiteboard", "Greta.Data.UpdateAgentState");
}

void WoZWindow::SendAgentState(std::string SAL)
{
	int i;
	char value[255];
	std::string opname;
	std::string emotion;
	std::string name="";
	AgentState *agentstate;

	if(SAL=="Positive")
	{
		name="POSITIVE";
		emotion="joy";
		agentstate=Poppy;
	}
	if(SAL=="Negative")
	{
		name="NEGATIVE";
		emotion="anger";
		agentstate=Spyke;
	}
	if(SAL=="Poppy")
	{
		name="POPPY";
		emotion="joy";
		agentstate=Poppy;
	}
	if(SAL=="Prudence")
	{
		name="PRUDENCE";
		emotion="neutral";
		agentstate=Prudence;
	}
	if(SAL=="Obadiah")
	{
		name="OBADIAH";
		emotion="sadness";
		agentstate=Obadiah;
	}
	if(SAL=="Spyke")
	{
		name="SPYKE";
		emotion="anger";
		agentstate=Spyke;
	}

	name="agent_identity.agent.name=" + name;
	emotion="emotion_state.emotion.name=" + emotion;

//	opname="mimicry_level.mimicry.value";
//	sprintf(value,"%.2f",mimic_level->value());
//	agentstate->internalstate.SetAttrValue(opname, (std::string)value);

	std::string msg=agentstate->internalstate.PrintXML();
	psydule->PostString(msg, "Greta.Whiteboard", "Greta.Data.AgentState");
	psydule->PostString(name, "Greta.Whiteboard", "Greta.Data.UpdateAgentState");
	psydule->PostString(emotion, "Greta.Whiteboard", "Greta.Data.UpdateAgentState");
}

void WoZWindow::SendMimicryLevel()
{
	char value[256];

	sprintf(value,"%.2f",mimic_level->value());
	std::string mimicrylevel="mimicry_level.mimicry.value=" + (std::string)value;

	psydule->PostString(mimicrylevel, "Greta.Whiteboard", "Greta.Data.UpdateAgentState");
}

std::string WoZWindow::WriteBML(int smile)
{
	std::string bml="";
	char s[256];
	float randomvalue;
	std::string head="";
	std::string face="";
	float dur;

	while(face=="" && head=="")
	{
		randomvalue=randgen->GetRand01();
		dur=0.5*randomvalue + 2;
		
		if(smile==1)
		{
			if(randomvalue<smile_prob)
				face="mouth=little_smile";
			else
				face="mouth=little_smile_open"; 
		}

		if(randomvalue<=raise_eyebrows_prob)
		{
			if(face=="")
				face="eyes=raise_eyebrows";
			else
			{
				if(face=="mouth=little_smile")
					face="affect=liking";
				else
					face="affect=liking_open";
			}
		}

		randomvalue=randgen->GetRand01();

		if(randomvalue>=0 && randomvalue<nod_prob[0])
			head="head=head_nod";

		if(randomvalue>=nod_prob[0] && randomvalue<nod_prob[0]+nod_prob[1])
			head="head=head_nod1";

		if(randomvalue>=nod_prob[0]+nod_prob[1] && randomvalue<nod_prob[0]+nod_prob[1]+nod_prob[2])
			head="head=head_nod2";

	}

	bml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	bml+="<!DOCTYPE bml SYSTEM \"bml/bml.dtd\" []>\n";
	bml+="<bml>\n";

	sprintf(s,"%.2f",dur);

	if(head!="")
	{
		bml+="<head id=\"sig1\"  "; // < MODALITY id="sigINDEX" start="0.000" end="1.5" stroke="1.0">
		bml+="start=\"0.0\" end=\"" + (std::string)s + "\" stroke=\"1.0\">\n";

		bml+="<description level=\"1\" type=\"gretabml\">\n";
		bml+="<reference>" + head + "</reference>\n";
		bml+="<intensity>1.00</intensity>\n";
		bml+="<FLD.value>0.70</FLD.value>\n";
		bml+="<PWR.value>0.20</PWR.value>\n";
		bml+="<REP.value>0.00</REP.value>\n";
		bml+="<SPC.value>0.00</SPC.value>\n";
		bml+="<TMP.value>0.40</TMP.value>\n";
		bml+="</description>\n";
		bml+="</head>\n";
	}

	if(face!="")
	{
		bml+="<face id=\"sig2\"  "; // < MODALITY id="sigINDEX" start="0.000" end="1.5" stroke="1.0">
		bml+="start=\"0.0\" end=\"" + (std::string)s + "\" stroke=\"1.0\">\n";

		bml+="<description level=\"1\" type=\"gretabml\">\n";
		bml+="<reference>" + face + "</reference>\n";
		bml+="<intensity>1.00</intensity>\n";
		bml+="<FLD.value>0.70</FLD.value>\n";
		bml+="<PWR.value>0.20</PWR.value>\n";
		bml+="<REP.value>0.00</REP.value>\n";
		bml+="<SPC.value>0.00</SPC.value>\n";
		bml+="<TMP.value>0.40</TMP.value>\n";
		bml+="</description>\n";
		bml+="</face>\n";
	}

	bml+="</bml>\n";

	return(bml);
}

/*
void WoZWindow::SendFAPsBAPs()
{
	FILE *fid;

	fid=fopen("Ciccio.fap", "r");

	fscanf(fid,

	//int last_frame_number;
	std::string msg,label;
	int frameind;
	char tm[256];
	int i;
	int maxlength;
	clock_t faptimeat,baptimeat;


	//FAPwriter fw;

	//BAPwriter bw;
	//bw.WriteToFile(bapframes, "provabap.bap");

	if (start_frame_time==-1) faptimeat=pc.GetTime()+DELAY;
	else faptimeat=start_frame_time;

	baptimeat=faptimeat;

	sprintf(tm,"%d",pc.GetTime());

	label=tm;

	msg="DELETEDIFF ";

	msg+=label;

	msg+=" ";

	sprintf(tm,"%d",faptimeat-1);

	msg+=tm;


	//printf("MASSAGE TO SEND: \n%s\n", msg.c_str());
	commint->PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+REALTIME_PLAYER_COMMAND);

	msg="";

	frameind=0;

	maxlength=0;

	if((bapframes!=0)&&(fapframes!=0))
	{
		if((*bapframes).size()>(*fapframes).size())
			maxlength=(*bapframes).size();
		else
			maxlength=(*fapframes).size();
	}
	else
	{
		if(bapframes!=0)
			maxlength=(*bapframes).size();
		if(fapframes!=0)
			maxlength=(*fapframes).size();
	}

	while(frameind<maxlength)
	{
		if(fapframes!=0)
		{
			msg="";

			msg+=label+"\n";

			//printf("label: %s\n", tm);

			for(i=0;i<MAXFRAMESENT;i++)
			{
				if ((i+frameind)<(*fapframes).size())
				{
					(*fapframes)[i+frameind].framenumber=faptimeat;
					msg+=(*fapframes)[i+frameind].toString();
					//last_frame_number=(i+frameind);
				}
				else
					break;
				faptimeat+=40;
			}
			commint->PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+FAP_FRAME);
		}

		if(bapframes!=0)
		{
			msg="";

			msg+=label+"\n";

			for(i=0;i<MAXFRAMESENT;i++)
			{

				if ((i+frameind)<(*bapframes).size())
				{
					(*bapframes)[i+frameind]->SetFrameNumber(baptimeat);
					msg+=(*bapframes)[i+frameind]->WriteBAP();
				}
				else
					break;
				baptimeat+=40;
			}

			commint->PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+BAP_FRAME);
		}

		frameind+=MAXFRAMESENT;
	}

	//return last_frame_number/FPS*1000+faptimeat;
	lastfapframetime=faptimeat-40;
	lastbapframetime=baptimeat-40;

}


int WoZWindow::ReadSmile() 
{
	int i;

	smileFiles[0]="Ciccio.fap";
	smileFiles[1]="Ciccio.fap";
	smileFiles[2]="Ciccio.fap";
	smileFiles[3]="Ciccio.fap";
	smileFiles[4]="Ciccio.fap";
	smileFiles[5]="Ciccio.fap";
	smileFiles[6]="Ciccio.fap";
	smileFiles[7]="Ciccio.fap";

	for(i=0;i<1;i++)
	{
		std::string line;
		std::ifstream myfile(smileFiles[i].c_str());
		if (myfile.is_open())
		{
			while (! myfile.eof() )
			{
				FAPFrame *newfapframe = new FAPFrame();
				std::getline (myfile,line);
				smiles[i]+=line + " ";
				std::getline (myfile,line);
				newfapframe->ReadFromBuffer((char *)smiles[i].c_str());
				smilevec.push_back(*newfapframe);
			}
			myfile.close();
		}

		else 
		{
		  std::cout << "Unable to open file " + smileFiles[i]; 
		  return 0;
		}
	}
	smilevec[0].Print();
	return 1;
}

*/