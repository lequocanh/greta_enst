#pragma once

#include "GestureEngine.h"


using namespace GestureSpace;

class NaoGesture
{
public:
	NaoGesture(void);
	~NaoGesture(void);

	void LoadFile();   //Load a txt file in the gesture repertory 

	void PrintScreen();// Print header information of txt file on the screen 

	void PrintScreenKeyFrames(GestureSpace::GesturePhaseVector *phasesVector); // Print information of keyframes from txt file on the screen

	void LeftArm(GesturePhase *phase); 

	void RightArm(GesturePhase *phase);

	vector<float> Converter(GesturePhase *phase, vector<float> vecxyz, float *hand_open, float *hand_dir, float SPC, float TMP);

	void Interpret();

	void Interpret(GestureSpace::GestureVector* gestureVector);

	void Interpret(GestureSpace::Gesture *gesture);

	void Print_angles();

public:
	//void run(){while(true) Sleep(10);}

	GestureSpace::Gesture *gesture; // gesture has two phases: phases and phasesAssym for right and left side correspondingly when the gesture is assymetric
	GestureSpace::GesturePhaseVector *phasesVector; // = &gesture->phases
	
	float *lHand;
	float *rHand;
	vector<float> lVec6;
	vector<float> rVec6;

	vector<vector<float>> leftPath;
	vector<vector<float>> rightPath;
	vector<float> rightTimes;
	vector<float> leftTimes;
};
