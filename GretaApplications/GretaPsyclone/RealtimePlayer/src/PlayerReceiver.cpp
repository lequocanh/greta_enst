#include "PlayerReceiver.h"
#include "IniManager.h"
#include "RealTimeAudio.h"
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;

extern IniManager inimanager;

PlayerReceiver::PlayerReceiver(Psydule *module, FramesManager* fm, CommandsManager* cm,CentralClock *pc, std::string GretaName):cmlabs::JThread()
{
	this->module=module;
	this->fm=fm;
	this->pc=pc;
	this->cm=cm;
	this->GretaName=GretaName;
	sound_time=0;
}


PlayerReceiver::~PlayerReceiver()
{
}

void PlayerReceiver::run()
{
	
	std::string content, type;
	long long lasttime=pc->GetTime();
	while(true)
	{
		
		if(module==0)
			break;	

		CommunicationMessage *msg;
		msg=module->ReceiveMessage(5);

		if(msg!=0)
		{
			content=msg->getContent();
			type=msg->getType();
			if(strcmp(msg->getType().c_str(),(GretaName+CLOCK_MESSAGE_HEADER).c_str())==0)
			{
				int time;
				time=atoi(msg->getContent().c_str());	
				pc->SetTime(time);
			}

			if(strcmp(type.c_str(),(GretaName+FAP_FRAME).c_str())==0)
			{
				fm->AddFAPFrame(content);
			}
			if(strcmp(type.c_str(),(GretaName+BAP_FRAME).c_str())==0)
			{
				fm->AddBAPFrame(content);
			}

			if(strcmp(type.c_str(),(GretaName+REALTIME_PLAYER_COMMAND).c_str())==0)
			{
				std::string command;
				command=content.c_str();
				cm->AddCommand(command.substr(0,command.find_first_of(" ")),
					command.substr(command.find_first_of(" ")+1));
			}
			
			if(strcmp(type.c_str(),(GretaName+DATA_WAV).c_str())==0)
			{
				/*
				std::string filename;
				char number[12];
				sprintf(number,"%d", pc->GetTime());
				filename=inimanager.Program_Path+"output/rtplayer.wav";
				this->module->WriteBinaryFile(filename,msg);
				//*/
				RealTimeAudio::load_wave(msg->getData(),msg->getSize());
			}
			std::list<GretaLogger*>::iterator iterlog;

			for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
			{
				std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
				if((*iterlog)->getLevel()=="debug")
					this->module->log->debug(msg);
				if((*iterlog)->getLevel()=="info")
					this->module->log->info(msg);
				if((*iterlog)->getLevel()=="warn")
					this->module->log->warn(msg);
				if((*iterlog)->getLevel()=="error")
					this->module->log->error(msg);
			}
			listLog.clear();
		}
	}
}