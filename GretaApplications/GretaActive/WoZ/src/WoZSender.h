//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// WoZSender.h: interface for the WoZSender class.
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#ifndef SEMAINE_COMPONENTS_GRETA_WOZ_H
#define SEMAINE_COMPONENTS_GRETA_WOZ_H

#include <semaine/config.h>

#include <cms/CMSException.h>

#include <semaine/components/Component.h>
#include <semaine/cms/sender/Sender.h>


using namespace cms;
using namespace semaine::components;
using namespace semaine::cms::sender;

class WoZSender : public Component
{
public:
	WoZSender(const std::string & componentName, bool isInput, bool isOutput) throw (CMSException);
	virtual ~WoZSender();
	Sender *getNameSender(){return nameSender;}
	Sender *getStateSender(){return stateSender;}
	Sender *getTurnSender(){return turnSender;}
	long long setTime(){return meta.getTime();}
protected:
	virtual void react(SEMAINEMessage * m) throw (CMSException);

private:

	Sender * nameSender;
	Sender * stateSender;
	Sender * turnSender;

};

#endif

