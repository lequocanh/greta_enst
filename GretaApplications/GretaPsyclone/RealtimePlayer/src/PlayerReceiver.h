#pragma once


#include "FramesManager.h"
#include "CommandsManager.h"
#include "JThread.h"
#include "CentralClock.h"
#include "CommunicationMessage.h"
#include "Psydule.h"

class PlayerReceiver : public cmlabs::JThread  
{
public:
	PlayerReceiver(Psydule *module,FramesManager* fm, CommandsManager* cm,CentralClock *pc,std::string GretaName);
	virtual ~PlayerReceiver();
	void run();
	Psydule *module;
	FramesManager* fm;
	CommandsManager* cm;
	CentralClock *pc;
	int sound_time;
private:
	std::string GretaName;
	int previous_time;

};
