#ifndef _COLORS_H
#define _COLORS_H

#define	COLOR_NUMBER		256

#define	WHITE_COLOR			1., 1., 1.
#define	WHITE				-1
#define	SKIN_COLOR			1, 1, 1
#define	SKIN				-2
#define	RED_COLOR			10., .0, .0
#define	RED					-3
#define	GREEN_COLOR			.0, 10., .0
#define	GREEN				-4
#define	BLUE_COLOR			.0, .0, 10.
#define	BLUE				-5
#define BLACK_COLOR			 .0, .0 , .0
#define BLACK				-6

/////////////
///////////// COLORS
/////////////

#define NONAME_0_COLOR		.10, .10, .0		// 100
#define NONAME_1_COLOR		.20, .10, .0		// 101
#define NONAME_2_COLOR		.30, .20, .0		// 102
#define NONAME_3_COLOR		.40, .20, .10		// 103
#define NONAME_4_COLOR		.50, .30, .10		// 104
#define NONAME_5_COLOR		.60, .30, .10		// 105
#define NONAME_6_COLOR		.70, .40, .20		// 106
#define NONAME_7_COLOR		.80, .40, .20		// 107
#define NONAME_8_COLOR		.90, .50, .20		// 108
#define NONAME_9_COLOR		1.0, .50, .30		// 109
#define NONAME_10_COLOR		.10, .60, .30		// 110
#define NONAME_11_COLOR		.20, .60, .30		// 111
#define NONAME_12_COLOR		.30, .70, .40		// 112
#define NONAME_13_COLOR		.40, .70, .40		// 113
#define NONAME_14_COLOR		.50, .80, .40		// 114
#define NONAME_15_COLOR		.60, .80, .50		// 115
#define NONAME_16_COLOR		.70, .90, .50		// 116
#define NONAME_17_COLOR		.80, .90, .50		// 117
#define NONAME_18_COLOR		.90, .0, .60		// 118
#define NONAME_19_COLOR		1.0, .0, .60		// 119
#define NONAME_20_COLOR		.10, .10, .60		// 120
#define NONAME_21_COLOR		.20, .10, .70		// 121
#define NONAME_22_COLOR		.30, .20, .70		// 122
#define NONAME_23_COLOR		.40, .20, .70		// 123
#define NONAME_24_COLOR		.50, .30, .80		// 124
#define NONAME_25_COLOR		.60, .30, .80		// 125
#define NONAME_26_COLOR		.70, .40, .80		// 126
#define NONAME_27_COLOR		.80, .40, .90		// 127
#define NONAME_28_COLOR		.90, .50, .90		// 128
#define NONAME_29_COLOR		1.0, .50, .90		// 129
#define NONAME_30_COLOR		.10, .60, .0		// 130
#define NONAME_31_COLOR		.20, .60, .0		// 131
#define NONAME_32_COLOR		.30, .70, .0		// 132
#define NONAME_33_COLOR		.40, .70, .10		// 133
#define NONAME_34_COLOR		.50, .80, .10		// 134
#define NONAME_35_COLOR		.60, .80, .10		// 135
#define NONAME_36_COLOR		.70, .90, .20		// 136
#define NONAME_37_COLOR		.80, .90, .20		// 137
#define NONAME_38_COLOR		.90, .0, .20		// 138
#define NONAME_39_COLOR		1.0, .0, .30		// 139
#define NONAME_40_COLOR		.10, .10, .30		// 140
#define NONAME_41_COLOR		.20, .10, .30		// 141
#define NONAME_42_COLOR		.30, .20, .40		// 142
#define NONAME_43_COLOR		.40, .20, .40		// 143
#define NONAME_44_COLOR		.50, .30, .40		// 144
#define NONAME_45_COLOR		.60, .30, .50		// 145
#define NONAME_46_COLOR		.70, .40, .50		// 146
#define NONAME_47_COLOR		.80, .40, .50		// 147
#define NONAME_48_COLOR		.90, .50, .60		// 148
#define NONAME_49_COLOR		1.0, .50, .60		// 149
#define NONAME_50_COLOR		.10, .60, .60		// 150
#define NONAME_51_COLOR		.20, .60, .70		// 151
#define NONAME_52_COLOR		.30, .70, .70		// 152
#define NONAME_53_COLOR		.40, .70, .70		// 153
#define NONAME_54_COLOR		.50, .80, .80		// 154
#define NONAME_55_COLOR		.60, .80, .80		// 155
#define NONAME_56_COLOR		.70, .90, .80		// 156
#define NONAME_57_COLOR		.80, .90, .90		// 157
#define NONAME_58_COLOR		.90, .0, .90		// 158
#define NONAME_59_COLOR		1.0, .0, .90		// 159
#define NONAME_60_COLOR		.10, .10, .0		// 160
#define NONAME_61_COLOR		.20, .10, .0		// 161
#define NONAME_62_COLOR		.30, .20, .0		// 162
#define NONAME_63_COLOR		.40, .20, .10		// 163
#define NONAME_64_COLOR		.50, .30, .10		// 164
#define NONAME_65_COLOR		.60, .30, .10		// 165
#define NONAME_66_COLOR		.70, .40, .20		// 166
#define NONAME_67_COLOR		.80, .40, .20		// 167
#define NONAME_68_COLOR		.90, .50, .20		// 168

/////////////
///////////// COLOR INDICES
/////////////

#define NONAME_0		69
#define NONAME_1		1
#define NONAME_2		2
#define NONAME_3		3
#define NONAME_4		4
#define NONAME_5		5
#define NONAME_6		6
#define NONAME_7		7
#define NONAME_8		8
#define NONAME_9		9
#define NONAME_10		10
#define NONAME_11		11
#define NONAME_12		12
#define NONAME_13		13
#define NONAME_14		14
#define NONAME_15		15
#define NONAME_16		16
#define NONAME_17		17
#define NONAME_18		18
#define NONAME_19		19
#define NONAME_20		20
#define NONAME_21		21
#define NONAME_22		22
#define NONAME_23		23
#define NONAME_24		24
#define NONAME_25		25
#define NONAME_26		26
#define NONAME_27		27
#define NONAME_28		28
#define NONAME_29		29
#define NONAME_30		30
#define NONAME_31		31
#define NONAME_32		32
#define NONAME_33		33
#define NONAME_34		34
#define NONAME_35		35
#define NONAME_36		36
#define NONAME_37		37
#define NONAME_38		38
#define NONAME_39		39
#define NONAME_40		40
#define NONAME_41		41
#define NONAME_42		42
#define NONAME_43		43
#define NONAME_44		44
#define NONAME_45		45
#define NONAME_46		46
#define NONAME_47		47
#define NONAME_48		48
#define NONAME_49		49
#define NONAME_50		50
#define NONAME_51		51
#define NONAME_52		52
#define NONAME_53		53
#define NONAME_54		54
#define NONAME_55		55
#define NONAME_56		56
#define NONAME_57		57
#define NONAME_58		58
#define NONAME_59		59
#define NONAME_60		60
#define NONAME_61		61
#define NONAME_62		62
#define NONAME_63		63
#define NONAME_64		64
#define NONAME_65		65
#define NONAME_66		66
#define NONAME_67		67
#define NONAME_68		68

#endif	// _COLORS_H