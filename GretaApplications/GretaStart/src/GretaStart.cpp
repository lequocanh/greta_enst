//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// WoZWindow.cpp: implementation of the WoZWindow class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include "FL/Fl.H"
#include "GretaStart.h"
#include <Windows.h>
#include <direct.h>


void selectdebug(Fl_Widget* w, GretaStart* v)
{
	v->debug->value(1);
	v->release->value(0);
}

void selectrelease(Fl_Widget* w, GretaStart* v)
{
	v->debug->value(0);
	v->release->value(1);
}

void selectactive(Fl_Widget* w, GretaStart* v)
{
	v->active->value(1);
	v->psyclone->value(0);
	v->actionSelection->deactivate();
	v->inter->deactivate();
	v->actionSelection->value(0);
	v->inter->value(0);
}

void selectpsyclone(Fl_Widget* w, GretaStart* v)
{
	v->psyclone->value(1);
	v->active->value(0);
	v->actionSelection->activate();
	v->inter->activate();
	v->actionSelection->value(0);
	v->inter->value(0);
}

void selectstart(Fl_Widget* w, GretaStart* v)
{
	v->StartSystem();
}

void selectstop(Fl_Widget* w, GretaStart* v)
{
	printf("%s\n", v->path);
	char *p=v->path;
	strcat(p,"\\stop");
	system(p); 
	_unlink("stop.bat");
}

GretaStart::GretaStart():Fl_Window(200,200,500,330,"Greta Start")
{
	modules=new Fl_Box(FL_BORDER_BOX,10,30,480,150,"MODULES");
	modules->align(FL_ALIGN_LEFT|FL_ALIGN_TOP);

	path=(char*)malloc(10000*sizeof(char));
	_getcwd(path,10000);

	listenerIntentPlanner=new Fl_Check_Button(40,50,20,20,"Listenet Intent Planner");
	listenerIntentPlanner->align(FL_ALIGN_RIGHT);
	actionSelection=new Fl_Check_Button(40,80,20,20,"Action Selection");
	actionSelection->align(FL_ALIGN_RIGHT);
	behaviourPlanner=new Fl_Check_Button(40,110,20,20,"Behaviour Planner");
	behaviourPlanner->align(FL_ALIGN_RIGHT);
	behaviourRealizer=new Fl_Check_Button(300,50,20,20,"Behaviour Realizer");
	behaviourRealizer->align(FL_ALIGN_RIGHT);
	player=new Fl_Check_Button(300,80,20,20,"Player");
	player->align(FL_ALIGN_RIGHT);
	inter=new Fl_Check_Button(300,110,20,20,"Interface");
	inter->align(FL_ALIGN_RIGHT);
	debug=new Fl_Round_Button(40,145,20,20,"debug");
	debug->align(FL_ALIGN_RIGHT);
	debug->callback((Fl_Callback *)selectdebug,this);
	debug->value(1);
	release=new Fl_Round_Button(300,145,20,20,"release");
	release->align(FL_ALIGN_RIGHT);
	release->callback((Fl_Callback *)selectrelease,this);


	communicationSystem=new Fl_Box(FL_BORDER_BOX,10,220,480,60,"COMMUNICATION SYSTEM");
	communicationSystem->align(FL_ALIGN_LEFT|FL_ALIGN_TOP);

	active=new Fl_Round_Button(40,240,20,20,"ActiveMQ");
	active->align(FL_ALIGN_RIGHT);
	active->callback((Fl_Callback *)selectactive,this);
	psyclone=new Fl_Round_Button(300,240,20,20,"Psyclone");
	psyclone->align(FL_ALIGN_RIGHT);
	psyclone->callback((Fl_Callback *)selectpsyclone,this);
	psyclone->value(1);


	startGreta=new Fl_Button(100,290,80,30,"START");
	startGreta->callback((Fl_Callback *)selectstart,this);

	stopGreta=new Fl_Button(300,290,80,30,"STOP");
	stopGreta->callback((Fl_Callback *)selectstop,this);
}

GretaStart::~GretaStart()
{
}

void GretaStart::draw()
{
	Fl_Window::draw();
}

int GretaStart::handle(int e)
{
	return Fl_Window::handle(e);
}


void GretaStart::StartSystem()
{
	FILE *fid, *idf;
	std::string postfix="";
	std::string module="";
	std::string module1="";
	int l=0;
	char *p=(char*)malloc(10000*sizeof(char));;

	fid=fopen("launch.bat","w");
	idf=fopen("stop.bat","w");

	if(psyclone->value()==1)
	{
		fprintf(fid,"%s","cd .\\psyclone\\\n");
		fprintf(fid,"%s","start psyclone spec=\"GretaEVOSpec.xml\" verbose=0\n");
		fprintf(fid,"%s","cd ..\n");
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");

		fprintf(idf,"%s","taskkill /f /im Psyclone.exe\n");//\ntaskkill /f /im cmd.exe

		if(debug->value()==1)
		{
			module="start .\\ClockPsycloneDebug.exe\n";
			fprintf(idf,"%s","taskkill /f /im ClockPsycloneDebug.exe\n");
		}
		else
		{
			module="start .\\ClockPsyclone.exe\n";
			fprintf(idf,"%s","taskkill /f /im ClockPsyclone.exe\n");
		}

		fprintf(fid,"%s",module.c_str());
		fprintf(fid,"%s",".\\utils\\WAIT.exe 3\n\n");
		postfix="Psyclone";
	}
	else
	{
		fprintf(fid,"%s","cd .\\active\\\n");
		fprintf(fid,"%s","\"start actimemq\n");
		fprintf(fid,"%s","cd ..\n");
		fprintf(fid,"%s","./utils/WAIT.exe 5\n\n");
		postfix="Active";

		fprintf(idf,"%s","taskkill /f /im activemq.exe\n");
	}

	if(debug->value()==1)
		postfix+="Debug.exe";
	else
		postfix+=".exe";

	if(listenerIntentPlanner->value()==1)
	{
		module="start .\\ListenerIntentPlanner" + postfix + "\n";
		fprintf(fid,"%s",module.c_str());
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");

		module1="taskkill /f /im ListenerIntentPlanner" + postfix + "\n";
		fprintf(idf,"%s",module1.c_str());

		l=1;
	}

	if(actionSelection->value()==1)
	{
		module="start .\\ActionSelection" + postfix + "\n";
		fprintf(fid,"%s",module.c_str());
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");

		module1="taskkill /f /im ActionSelection" + postfix + "\n";
		fprintf(idf,"%s",module1.c_str());

		l=1;
	}

	if(behaviourPlanner->value()==1)
	{
		module="start .\\BehaviorPlanner" + postfix + "\n";
		fprintf(fid,"%s",module.c_str());
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");

		module1="taskkill /f /im BehaviorPlanner" + postfix + "\n";
		fprintf(idf,"%s",module1.c_str());

		l=1;
	}

	if(behaviourRealizer->value()==1)
	{
		module="start .\\BehaviorRealizer" + postfix + "\n";
		fprintf(fid,"%s",module.c_str());
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");
		
		module1="taskkill /f /im BehaviorRealizer" + postfix + "\n";
		fprintf(idf,"%s",module1.c_str());

		l=1;
	}

	if(player->value()==1)
	{
		module="start .\\RealtimePlayer" + postfix + "\n";
		fprintf(fid,"%s",module.c_str());
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");
		
		module1="taskkill /f /im RealtimePlayer" + postfix + "\n";
		fprintf(idf,"%s",module1.c_str());

		l=1;
	}

	if(inter->value()==1)
	{
		fprintf(fid,"cd .\\..\\GretaAuxiliary\\interface\\dist\n");
		fprintf(fid,"start start.bat\n");
		fprintf(fid,"%s",".\\utils\\WAIT.exe 5\n\n");
		
		fprintf(idf,"cd .\\..\\GretaAuxiliary\\interface\\dist\n");
		fprintf(idf,"taskkill /f start.bat\n");

		l=1;
	}

	fclose(fid);
	fclose(idf);

	strcpy(p,path);
	strcat(p,"\\launch");

	system(p); 

	_unlink("launch.bat");
}

