//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include <conio.h>

#include "IniManager.h"
#include "DataContainer.h"
#include "BehaviorRealizer.h"
#include <XercesTool.h>
#include <semaine/util/XMLTool.h>

#include <semaine/components/Component.h>
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;



DataContainer *datacontainer;
IniManager inimanager;
std::string ini_filename;

int main(int argc, char ** argv)
{	
	if(argc==2)
	{
		ini_filename=argv[1];
	} else {
		ini_filename="greta_activemq.ini";	
	}

	XercesTool::startupXMLTools();
	semaine::util::XMLTool::startupXMLTools();

	inimanager.ReadIniFile(ini_filename);
	datacontainer = new DataContainer();
	
	int code=datacontainer->initBMLEngine();
	if (code==0) {
		printf("Problem : out \n");
		exit(1);
	}

	/*semaine::util::XMLTool::startupXMLTools();
	std::list<semaine::components::Component *> comps;
	comps.push_back(new semaine::components::greta::BehaviorRealizer("BehaviorRealizer", false, false));
	semaine::system::ComponentRunner cr(comps);
	cr.go();
	cr.waitUntilCompleted();*/

	try{
		semaine::components::greta::BehaviorRealizer br("BehaviorRealizer", false, false);
		getch();
	}
	catch(CMSException & e) {
		printf("\n*********************************************************\nActiveMQ is not started. The application will be stopped\n*********************************************************");
		//getch();
		MessageBox(0, "ActiveMQ is not started. The application will be stopped", "Warning", MB_OK | MB_ICONWARNING);
		_exit(0);
	}

	XercesTool::shutdownXMLTools();
	semaine::util::XMLTool::shutdownXMLTools();
}
