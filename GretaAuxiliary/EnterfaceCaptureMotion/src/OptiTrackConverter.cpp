#include "OptiTrackConverter.h"

#include "Converter.h"

#include <stdio.h>
#include <iostream>
#include <fstream>


OptiTrackConverter::OptiTrackConverter() : Converter()
{
	framerate=25;	
	for (int i=0; i<69; i++) {
		mask[i]=1000.0;
	}

}

OptiTrackConverter::~OptiTrackConverter()
{
}

bool OptiTrackConverter::readMCMessage(char *mcfilename)
{

	FILE *fapfile;

	if(!(fapfile = fopen(mcfilename,"r"))){
		printf("WARNING: can't read %s\n",mcfilename);
		return false;
	}

	int framenumber=0;
	//float  time=0.0;

	float x1b=0.0,y1b=0.0,z1b=0.0;
	float x2b=0.0,y2b=0.0,z2b=0.0;
	float x3b=0.0,y3b=0.0,z3b=0.0;
	float x4b=0.0,y4b=0.0,z4b=0.0;
	float x5b=0.0,y5b=0.0,z5b=0.0;
	float x6b=0.0,y6b=0.0,z6b=0.0;
	float x7b=0.0,y7b=0.0,z7b=0.0;
	float x8b=0.0,y8b=0.0,z8b=0.0;
	float x9b=0.0,y9b=0.0,z9b=0.0;
	float x10b=0.0,y10b=0.0,z10b=0.0;
	float x11b=0.0,y11b=0.0,z11b=0.0;
	float x12b=0.0,y12b=0.0,z12b=0.0;
	float x13b=0.0,y13b=0.0,z13b=0.0;
	float x14b=0.0,y14b=0.0,z14b=0.0;
	float x15b=0.0,y15b=0.0,z15b=0.0;
	float x16b=0.0,y16b=0.0,z16b=0.0;
	float x17b=0.0,y17b=0.0,z17b=0.0;
	float x18b=0.0,y18b=0.0,z18b=0.0;
	float x19b=0.0,y19b=0.0,z19b=0.0;
	float x20b=0.0,y20b=0.0,z20b=0.0;
	float x21b=0.0,y21b=0.0,z21b=0.0;
	float x22b=0.0,y22b=0.0,z22b=0.0;
	float x23b=0.0,y23b=0.0,z23b=0.0;
	float x24b=0.0,y24b=0.0,z24b=0.0;
	float x25b=0.0,y25b=0.0,z25b=0.0;
	float x26b=0.0,y26b=0.0,z26b=0.0;
	float x27b=0.0,y27b=0.0,z27b=0.0;

	//rotations?

	//float x30b=0.0,y30b=0.0,z30b=0.0;
	//float x31b=0.0,y31b=0.0,z31b=0.0;


	float x1=0.0,y1=0.0,z1=0.0;
	float x2=0.0,y2=0.0,z2=0.0;
	float x3=0.0,y3=0.0,z3=0.0;
	float x4=0.0,y4=0.0,z4=0.0;
	float x5=0.0,y5=0.0,z5=0.0;
	float x6=0.0,y6=0.0,z6=0.0;
	float x7=0.0,y7=0.0,z7=0.0;
	float x8=0.0,y8=0.0,z8=0.0;
	float x9=0.0,y9=0.0,z9=0.0;
	float x10=0.0,y10=0.0,z10=0.0;
	float x11=0.0,y11=0.0,z11=0.0;
	float x12=0.0,y12=0.0,z12=0.0;
	float x13=0.0,y13=0.0,z13=0.0;
	float x14=0.0,y14=0.0,z14=0.0;
	float x15=0.0,y15=0.0,z15=0.0;
	float x16=0.0,y16=0.0,z16=0.0;
	float x17=0.0,y17=0.0,z17=0.0;
	float x18=0.0,y18=0.0,z18=0.0;
	float x19=0.0,y19=0.0,z19=0.0;
	float x20=0.0,y20=0.0,z20=0.0;
	float x21=0.0,y21=0.0,z21=0.0;
	float x22=0.0,y22=0.0,z22=0.0;
	float x23=0.0,y23=0.0,z23=0.0;
	float x24=0.0,y24=0.0,z24=0.0;
	float x25=0.0,y25=0.0,z25=0.0;
	float x26=0.0,y26=0.0,z26=0.0;
	float x27=0.0,y27=0.0,z27=0.0;

	//rotations

	//float x30=0.0,y30=0.0,z30=0.0;
	//float x31=0.0,y31=0.0,z31=0.0;

	char mystring[10000];

	fgets (mystring , 9999, fapfile);

	sscanf(mystring, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 		  

		&x1b, &y1b, &z1b, 
		&x2b, &y2b, &z2b, 
		&x3b, &y3b, &z3b, 
		&x4b, &y4b, &z5b, 
		&x5b, &y5b, &z5b, 
		&x6b, &y6b, &z6b, 
		&x7b, &y7b, &z7b, 
		&x8b, &y8b, &z8b, 
		&x9b, &y9b, &z9b, 
		&x10b, &y10b, &z10b, 
		&x11b, &y11b, &z11b, 
		&x12b, &y12b, &z12b, 
		&x13b, &y13b, &z13b, 
		&x14b, &y14b, &z14b, 
		&x15b, &y15b, &z15b, 
		&x16b, &y16b, &z16b, 
		&x17b, &y17b, &z17b, 
		&x18b, &y18b, &z18b, 
		&x19b, &y19b, &z19b, 
		&x20b, &y20b, &z20b, 
		&x21b, &y21b, &z21b, 
		&x22b, &y22b, &z22b, 
		&x23b, &y23b, &z23b, 
		&x24b, &y24b, &z24b, 
		&x25b, &y25b, &z25b,
		&x26b, &y26b, &z26b,
		&x27b, &y27b, &z27b
		);

	//change this
	float nosex0 = x25b;
	float nosey0 = y25b;


	CapturedFrame *temp = new CapturedFrame(framenumber,0.0);

	std::string label1="tymczas";

	temp->addPoint(x1b,y1b,z1b,1,label1);
	temp->addPoint(x2b,y2b,z2b,2,label1);
	temp->addPoint(x3b,y3b,z3b,3,label1);
	temp->addPoint(x4b,y4b,z4b,4,label1);
	temp->addPoint(x5b,y5b,z5b,5,label1);
	temp->addPoint(x6b,y6b,z6b,6,label1);
	temp->addPoint(x7b,y7b,z7b,7,label1);
	temp->addPoint(x8b,y8b,z8b,8,label1);
	temp->addPoint(x9b,y9b,z9b,9,label1);
	temp->addPoint(x10b,y10b,z10b,10,label1);
	temp->addPoint(x11b,y11b,z11b,11,label1);
	temp->addPoint(x12b,y12b,z12b,12,label1);
	temp->addPoint(x13b,y13b,z13b,13,label1);
	temp->addPoint(x14b,y14b,z14b,14,label1);
	temp->addPoint(x15b,y15b,z15b,15,label1);
	temp->addPoint(x16b,y16b,z16b, 16,label1);
	temp->addPoint(x17b,y17b,z17b,17,label1);
	temp->addPoint(x18b,y18b,z18b,18,label1);
	temp->addPoint(x19b,y19b,z19b,19,label1);
	temp->addPoint(x20b,y20b,z20b,20,label1);
	temp->addPoint(x21b,y21b,z21b,21,label1);
	temp->addPoint(x22b,y22b,z22b,22,label1);
	temp->addPoint(x23b,y23b,z23b,23,label1);
	temp->addPoint(x24b,y24b,z24b,24,label1);
	temp->addPoint(x25b,y25b,z25b,25,label1);
	temp->addPoint(x26b,y26b,z26b,26,label1);
	temp->addPoint(x27b,y27b,z27b,27,label1);

	temp->addPoint(0,0,0,28,label1);

	//temp->addPoint(x29b,y29b,z29b,29,label1);
	//temp->addPoint(x30b,y30b,z30b,30,label1);
	//temp->addPoint(x30b,y30b,z30b,30,label1);

	capturedframes.push_back(temp);

	while( fgets (mystring , 9999, fapfile) )
	{

		//continue read the frames
		sscanf(mystring, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 		  
			&x1, &y1, &z1, 
			&x2, &y2, &z2, 
			&x3, &y3, &z3, 
			&x4, &y4, &z5, 
			&x5, &y5, &z5, 
			&x6, &y6, &z6, 
			&x7, &y7, &z7, 
			&x8, &y8, &z8, 
			&x9, &y9, &z9, 
			&x10, &y10, &z10, 
			&x11, &y11, &z11, 
			&x12, &y12, &z12, 
			&x13, &y13, &z13, 
			&x14, &y14, &z14, 
			&x15, &y15, &z15, 
			&x16, &y16, &z16, 
			&x17, &y17, &z17, 
			&x18, &y18, &z18, 
			&x19, &y19, &z19, 
			&x20, &y20, &z20, 
			&x21, &y21, &z21, 
			&x22, &y22, &z22, 
			&x23, &y23, &z23, 
			&x24, &y24, &z24, 
			&x25, &y25, &z25,
			&x26, &y26, &z26,
			&x27, &y27, &z27	
			);

		//change this
		float nosexcurrent = x25;
		float noseycurrent = y25;

		//float nosex = nosex0 - nosexcurrent;
		//float nosey = nosey0 - noseycurrent;

		float nosex = 0;
		float nosey = 0;


		CapturedFrame *temp = new CapturedFrame(framenumber,0.0);

		std::string label1="tymczas";

		temp->addPoint(x1b-x1-nosex,y1b-y1-nosey,z1b-z1,1,label1);
		temp->addPoint(x2b-x2-nosex,y2b-y2-nosey,z2b-z2,2,label1);
		temp->addPoint(x3b-x3-nosex,y3b-y3-nosey,z3b-z3,3,label1);
		temp->addPoint(x4b-x4-nosex,y4b-y4-nosey,z4b-z4,4,label1);
		temp->addPoint(x5b-x5-nosex,y5b-y5-nosey,z5b-z5,5,label1);
		temp->addPoint(x6b-x6-nosex,y6b-y6-nosey,z6b-z6,6,label1);
		temp->addPoint(x7b-x7-nosex,y7b-y7-nosey,z7b-z7,7,label1);
		temp->addPoint(x8b-x8-nosex,y8b-y8-nosey,z8b-z8,8,label1);
		temp->addPoint(x9b-x9-nosex,y9b-y9-nosey,z9b-z9,9,label1);
		temp->addPoint(x10b-x10-nosex,y10b-y10-nosey,z10b-z10,10,label1);
		temp->addPoint(x11b-x11-nosex,y11b-y11-nosey,z11b-z11,11,label1);
		temp->addPoint(x12b-x12-nosex,y12b-y12-nosey,z12b-z12,12,label1);
		temp->addPoint(x13b-x13-nosex,y13b-y13-nosey,z13b-z13,13,label1);
		temp->addPoint(x14b-x14-nosex,y14b-y14-nosey,z14b-z14,14,label1);
		temp->addPoint(x15b-x15-nosex,y15b-y15-nosey,z15b-z15,15,label1);
		temp->addPoint(x16b-x16-nosex,y16b-y16-nosey,z16b-z16,16,label1);
		temp->addPoint(x17b-x17-nosex,y17b-y17-nosey,z17b-z17,17,label1);
		temp->addPoint(x18b-x18-nosex,y18b-y18-nosey,z18b-z18,18,label1);
		temp->addPoint(x19b-x19-nosex,y19b-y19-nosey,z19b-z19,19,label1);
		temp->addPoint(x20b-x20-nosex,y20b-y20-nosey,z20b-z20,20,label1);
		temp->addPoint(x21b-x21-nosex,y21b-y21-nosey,z21b-z21,21,label1);
		temp->addPoint(x22b-x22-nosex,y22b-y22-nosey,z22b-z22,22,label1);
		temp->addPoint(x23b-x23-nosex,y23b-y23-nosey,z23b-z23,23,label1);
		temp->addPoint(x24b-x24-nosex,y24b-y24-nosey,z24b-z24,24,label1);
		temp->addPoint(x25b-x25-nosex,y25b-y25-nosey,z25b-z25,25,label1);
		temp->addPoint(x26b-x26-nosex,y26b-y26-nosey,z26b-z26,26,label1);
		temp->addPoint(x27b-x27-nosex,y27b-y27-nosey,z27b-z27,27,label1);

		temp->addPoint(0,0,0,28,label1);


		//temp->addPoint(x29b-x29-nosex,y29b-y29-nosey,z29b-z29,29,label1);
		//temp->addPoint(x30b-x30-nosex,y30b-y30-nosey,z30b-z30,30,label1);

		capturedframes.push_back(temp);

	}//end of while 

	fclose(fapfile);

	return true;

}



bool OptiTrackConverter::readHeadMCMessage(char *mcfilename)
{

	FILE *fapfile;

	if(!(fapfile = fopen(mcfilename,"r"))){
		printf("WARNING: can't read %s\n",mcfilename);
		return false;
	}

	int framenumber=0;
	//float  time=0.0;

	float x1b=0.0,y1b=0.0,z1b=0.0;
	float x2b=0.0,y2b=0.0,z2b=0.0;
	float x3b=0.0,y3b=0.0,z3b=0.0;
	float x4b=0.0,y4b=0.0,z4b=0.0;
	float x5b=0.0,y5b=0.0,z5b=0.0;
	float x6b=0.0,y6b=0.0,z6b=0.0;
	float x7b=0.0,y7b=0.0,z7b=0.0;
	float x8b=0.0,y8b=0.0,z8b=0.0;
	float x9b=0.0,y9b=0.0,z9b=0.0;
	float x10b=0.0,y10b=0.0,z10b=0.0;
	float x11b=0.0,y11b=0.0,z11b=0.0;
	float x12b=0.0,y12b=0.0,z12b=0.0;
	float x13b=0.0,y13b=0.0,z13b=0.0;
	float x14b=0.0,y14b=0.0,z14b=0.0;
	float x15b=0.0,y15b=0.0,z15b=0.0;
	float x16b=0.0,y16b=0.0,z16b=0.0;
	float x17b=0.0,y17b=0.0,z17b=0.0;
	float x18b=0.0,y18b=0.0,z18b=0.0;
	float x19b=0.0,y19b=0.0,z19b=0.0;
	float x20b=0.0,y20b=0.0,z20b=0.0;
	float x21b=0.0,y21b=0.0,z21b=0.0;
	float x22b=0.0,y22b=0.0,z22b=0.0;
	float x23b=0.0,y23b=0.0,z23b=0.0;
	float x24b=0.0,y24b=0.0,z24b=0.0;
	float x25b=0.0,y25b=0.0,z25b=0.0;
	float x26b=0.0,y26b=0.0,z26b=0.0;
	float x27b=0.0,y27b=0.0,z27b=0.0;
	float x28b=0.0,y28b=0.0,z28b=0.0;

	//rotations?



	float x1=0.0,y1=0.0,z1=0.0;
	float x2=0.0,y2=0.0,z2=0.0;
	float x3=0.0,y3=0.0,z3=0.0;
	float x4=0.0,y4=0.0,z4=0.0;
	float x5=0.0,y5=0.0,z5=0.0;
	float x6=0.0,y6=0.0,z6=0.0;
	float x7=0.0,y7=0.0,z7=0.0;
	float x8=0.0,y8=0.0,z8=0.0;
	float x9=0.0,y9=0.0,z9=0.0;
	float x10=0.0,y10=0.0,z10=0.0;
	float x11=0.0,y11=0.0,z11=0.0;
	float x12=0.0,y12=0.0,z12=0.0;
	float x13=0.0,y13=0.0,z13=0.0;
	float x14=0.0,y14=0.0,z14=0.0;
	float x15=0.0,y15=0.0,z15=0.0;
	float x16=0.0,y16=0.0,z16=0.0;
	float x17=0.0,y17=0.0,z17=0.0;
	float x18=0.0,y18=0.0,z18=0.0;
	float x19=0.0,y19=0.0,z19=0.0;
	float x20=0.0,y20=0.0,z20=0.0;
	float x21=0.0,y21=0.0,z21=0.0;
	float x22=0.0,y22=0.0,z22=0.0;
	float x23=0.0,y23=0.0,z23=0.0;
	float x24=0.0,y24=0.0,z24=0.0;
	float x25=0.0,y25=0.0,z25=0.0;
	float x26=0.0,y26=0.0,z26=0.0;
	float x27=0.0,y27=0.0,z27=0.0;
	float x28=0.0,y28=0.0,z28=0.0;

	//rotations

	//float x30=0.0,y30=0.0,z30=0.0;
	//float x31=0.0,y31=0.0,z31=0.0;

	char mystring[10000];

	fgets (mystring , 9999, fapfile);

	sscanf(mystring, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 		  

		&x1b, &y1b, &z1b, 
		&x2b, &y2b, &z2b, 
		&x3b, &y3b, &z3b, 
		&x4b, &y4b, &z5b, 
		&x5b, &y5b, &z5b, 
		&x6b, &y6b, &z6b, 
		&x7b, &y7b, &z7b, 
		&x8b, &y8b, &z8b, 
		&x9b, &y9b, &z9b, 
		&x10b, &y10b, &z10b, 
		&x11b, &y11b, &z11b, 
		&x12b, &y12b, &z12b, 
		&x13b, &y13b, &z13b, 
		&x14b, &y14b, &z14b, 
		&x15b, &y15b, &z15b, 
		&x16b, &y16b, &z16b, 
		&x17b, &y17b, &z17b, 
		&x18b, &y18b, &z18b, 
		&x19b, &y19b, &z19b, 
		&x20b, &y20b, &z20b, 
		&x21b, &y21b, &z21b, 
		&x22b, &y22b, &z22b, 
		&x23b, &y23b, &z23b, 
		&x24b, &y24b, &z24b, 
		&x25b, &y25b, &z25b,
		&x26b, &y26b, &z26b,
		&x27b, &y27b, &z27b,
		&x28b, &y28b, &z28b
		);

	//change this
	float nosex0 = x25b;
	float nosey0 = y25b;

	//DO NOT ADD THE FIRST FRAME - IT IS NEUTRAL FRAME~~~

	//CapturedFrame *temp = new CapturedFrame(framenumber,0.0);

	//std::string label1="tymczas";

	//temp->addPoint(x1b,y1b,z1b,1,label1);
	//temp->addPoint(x2b,y2b,z2b,2,label1);
	//temp->addPoint(x3b,y3b,z3b,3,label1);
	//temp->addPoint(x4b,y4b,z4b,4,label1);
	//temp->addPoint(x5b,y5b,z5b,5,label1);
	//temp->addPoint(x6b,y6b,z6b,6,label1);
	//temp->addPoint(x7b,y7b,z7b,7,label1);
	//temp->addPoint(x8b,y8b,z8b,8,label1);
	//temp->addPoint(x9b,y9b,z9b,9,label1);
	//temp->addPoint(x10b,y10b,z10b,10,label1);
	//temp->addPoint(x11b,y11b,z11b,11,label1);
	//temp->addPoint(x12b,y12b,z12b,12,label1);
	//temp->addPoint(x13b,y13b,z13b,13,label1);
	//temp->addPoint(x14b,y14b,z14b,14,label1);
	//temp->addPoint(x15b,y15b,z15b,15,label1);
	//temp->addPoint(x16b,y16b,z16b, 16,label1);
	//temp->addPoint(x17b,y17b,z17b,17,label1);
	//temp->addPoint(x18b,y18b,z18b,18,label1);
	//temp->addPoint(x19b,y19b,z19b,19,label1);
	//temp->addPoint(x20b,y20b,z20b,20,label1);
	//temp->addPoint(x21b,y21b,z21b,21,label1);
	//temp->addPoint(x22b,y22b,z22b,22,label1);
	//temp->addPoint(x23b,y23b,z23b,23,label1);
	//temp->addPoint(x24b,y24b,z24b,24,label1);
	//temp->addPoint(x25b,y25b,z25b,25,label1);
	//temp->addPoint(x26b,y26b,z26b,26,label1);
	//temp->addPoint(x27b,y27b,z27b,27,label1);
	//temp->addPoint(x28b,y28b,z28b,28,label1);
	//
	////temp->addPoint(x29b,y29b,z29b,29,label1);
	////temp->addPoint(x30b,y30b,z30b,30,label1);
	////temp->addPoint(x30b,y30b,z30b,30,label1);

	//capturedframes.push_back(temp);


	while( fgets (mystring , 9999, fapfile) )
	{

		//continue read the frames
		sscanf(mystring, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f", 		  
			&x1, &y1, &z1, 
			&x2, &y2, &z2, 
			&x3, &y3, &z3, 
			&x4, &y4, &z5, 
			&x5, &y5, &z5, 
			&x6, &y6, &z6, 
			&x7, &y7, &z7, 
			&x8, &y8, &z8, 
			&x9, &y9, &z9, 
			&x10, &y10, &z10, 
			&x11, &y11, &z11, 
			&x12, &y12, &z12, 
			&x13, &y13, &z13, 
			&x14, &y14, &z14, 
			&x15, &y15, &z15, 
			&x16, &y16, &z16, 
			&x17, &y17, &z17, 
			&x18, &y18, &z18, 
			&x19, &y19, &z19, 
			&x20, &y20, &z20, 
			&x21, &y21, &z21, 
			&x22, &y22, &z22, 
			&x23, &y23, &z23, 
			&x24, &y24, &z24, 
			&x25, &y25, &z25,
			&x26, &y26, &z26,
			&x27, &y27, &z27,	
			&x28, &y28, &z28	
			);

		//change this
		float nosexcurrent = x25;
		float noseycurrent = y25;

		//float nosex = nosex0 - nosexcurrent;
		//float nosey = nosey0 - noseycurrent;

		float nosex = 0;
		float nosey = 0;


		CapturedFrame *temp = new CapturedFrame(framenumber,0.0);

		std::string label1="tymczas";

		temp->addPoint(x1b-x1-nosex,y1b-y1-nosey,z1b-z1,1,label1);
		temp->addPoint(x2b-x2-nosex,y2b-y2-nosey,z2b-z2,2,label1);
		temp->addPoint(x3b-x3-nosex,y3b-y3-nosey,z3b-z3,3,label1);
		temp->addPoint(x4b-x4-nosex,y4b-y4-nosey,z4b-z4,4,label1);
		temp->addPoint(x5b-x5-nosex,y5b-y5-nosey,z5b-z5,5,label1);
		temp->addPoint(x6b-x6-nosex,y6b-y6-nosey,z6b-z6,6,label1);
		temp->addPoint(x7b-x7-nosex,y7b-y7-nosey,z7b-z7,7,label1);
		temp->addPoint(x8b-x8-nosex,y8b-y8-nosey,z8b-z8,8,label1);
		temp->addPoint(x9b-x9-nosex,y9b-y9-nosey,z9b-z9,9,label1);
		temp->addPoint(x10b-x10-nosex,y10b-y10-nosey,z10b-z10,10,label1);
		temp->addPoint(x11b-x11-nosex,y11b-y11-nosey,z11b-z11,11,label1);
		temp->addPoint(x12b-x12-nosex,y12b-y12-nosey,z12b-z12,12,label1);
		temp->addPoint(x13b-x13-nosex,y13b-y13-nosey,z13b-z13,13,label1);
		temp->addPoint(x14b-x14-nosex,y14b-y14-nosey,z14b-z14,14,label1);
		temp->addPoint(x15b-x15-nosex,y15b-y15-nosey,z15b-z15,15,label1);
		temp->addPoint(x16b-x16-nosex,y16b-y16-nosey,z16b-z16,16,label1);
		temp->addPoint(x17b-x17-nosex,y17b-y17-nosey,z17b-z17,17,label1);
		temp->addPoint(x18b-x18-nosex,y18b-y18-nosey,z18b-z18,18,label1);
		temp->addPoint(x19b-x19-nosex,y19b-y19-nosey,z19b-z19,19,label1);
		temp->addPoint(x20b-x20-nosex,y20b-y20-nosey,z20b-z20,20,label1);
		temp->addPoint(x21b-x21-nosex,y21b-y21-nosey,z21b-z21,21,label1);
		temp->addPoint(x22b-x22-nosex,y22b-y22-nosey,z22b-z22,22,label1);
		temp->addPoint(x23b-x23-nosex,y23b-y23-nosey,z23b-z23,23,label1);
		temp->addPoint(x24b-x24-nosex,y24b-y24-nosey,z24b-z24,24,label1);
		temp->addPoint(x25b-x25-nosex,y25b-y25-nosey,z25b-z25,25,label1);
		temp->addPoint(x26b-x26-nosex,y26b-y26-nosey,z26b-z26,26,label1);
		temp->addPoint(x27b-x27-nosex,y27b-y27-nosey,z27b-z27,27,label1);		
		temp->addPoint(x28b-x28-nosex,y28b-y28-nosey,z28b-z28,28,label1);

		//temp->addPoint(x29b-x29-nosex,y29b-y29-nosey,z29b-z29,29,label1);
		//temp->addPoint(x30b-x30-nosex,y30b-y30-nosey,z30b-z30,30,label1);

		capturedframes.push_back(temp);

	}//end of while 

	fclose(fapfile);

	return true;

}




std::vector<FAPFrame> * OptiTrackConverter::converttoFAPs()
{
	std::vector<FAPFrame> *fapframevector;

	fapframevector = new std::vector<FAPFrame>;

	std::vector<CapturedFrame*>::iterator iter;

	int index=0;
	//(*fapframevector).resize(capturedframes.size());

	for(iter=capturedframes.begin();iter!=capturedframes.end();iter++)
	{


		if (index<2){

			FAPFrame *temp = new FAPFrame();
			temp->SetFAP(0, index, true);
			temp->SetFAP(1, 0, false);
			temp->SetFAP(2, 0, false);

			temp->framenumber=index++;

			fapframevector->push_back(*temp);
		}

		else

		{

			FAPFrame *temp = new FAPFrame();

			temp->framenumber=index;
			temp->SetFAP(0, index, true);
			index++;

			temp->SetFAP(1, 0, false);
			temp->SetFAP(2, 0, false);

			float vert = 1.0;
			float avert = -1.0*vert;

			float ahor= 1.0;
			float hor=  -1.0 * ahor;

			float upperface =  -1.0;
			float lowerface =  -1.0;

			//check it 27

			if ( ((CapturedFrame*) (*iter))->points[27]->y >0 )
				temp->SetFAP(3, (int) (lowerface * mask[3] * (avert)*((CapturedFrame*) (*iter))->points[27]->z ), true);		
			else	
				temp->SetFAP(3,  (int) ( lowerface * 0.5*mask[3] * (avert)*((CapturedFrame*) (*iter))->points[27]->z ), true);		

			//upper lip
			temp->SetFAP(4,  (int) (lowerface * mask[4] *(avert)*((CapturedFrame*) (*iter))->points[20]->z ), true);

			//lower lip
			//temp->SetFAP(5,  (1)*(lowerface) *(vert)*  ( ( (CapturedFrame*) (*iter) ) -> points[26]->z ) );

			temp->SetFAP(5,  (int) ( lowerface * mask[5] *(vert)*  ( (((CapturedFrame*) (*iter))->points[25]->z) + (((CapturedFrame*) (*iter))->points[23]->z) )/1.5 ) , true);

			//temp->SetFAP(6,  (lowerface) *(ahor)* ((CapturedFrame*) (*iter))->points[19]->x, true);
			//temp->SetFAP(7,  (lowerface) *(hor)* ((CapturedFrame*) (*iter))->points[20]->x, true);

			//upper left
			float tee = ((CapturedFrame*) (*iter))->points[19]->z;

			tee = lowerface * mask[8] *(avert)*((CapturedFrame*) (*iter))->points[19]->z ;

			int teee = lowerface * mask[8] *(avert)*((CapturedFrame*) (*iter))->points[19]->z ;

			temp->SetFAP(8,  (int) ( lowerface *mask[8] *(avert)*((CapturedFrame*) (*iter))->points[19]->z ), true);
			//upper irgt
			temp->SetFAP(9,  (int) (lowerface *mask[9] *(avert)*((CapturedFrame*) (*iter))->points[21]->z ), true);

			//lower left
			temp->SetFAP(10, (int) (lowerface *mask[10] *(vert)*((CapturedFrame*) (*iter))->points[25]->z ), true);
			//lower right
			temp->SetFAP(11, (int) (lowerface *mask[11] *(vert)*((CapturedFrame*) (*iter))->points[23]->z ), true);

			//temp->SetFAP(12, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[19]->y, true);
			//temp->SetFAP(13, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[20]->y, true);

			//27
			temp->SetFAP(14, (int) ( lowerface *mask[14] *((CapturedFrame*) (*iter))->points[27]->y ), true);

			temp->SetFAP(15, (int) (lowerface *mask[15] *(ahor)*((CapturedFrame*) (*iter))->points[27]->x ), true);

			//temp->SetFAP(16, 2*(lowerface) *(vert)* ( (((CapturedFrame*) (*iter))->points[17]->y) + (((CapturedFrame*) (*iter))->points[18]->y) )/2 , true);
			//temp->SetFAP(17, 2*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[14]->z, true);

			//27
			temp->SetFAP(18, (int) ( lowerface *mask[18] *(vert)*((CapturedFrame*) (*iter))->points[27]->z ), true);

			temp->SetFAP(19, 0, true);
			temp->SetFAP(20, 0, true);

			temp->SetFAP(21, 0, true);
			temp->SetFAP(22, 0, true);
			temp->SetFAP(23, 0, true);
			temp->SetFAP(24, 0, true);
			temp->SetFAP(25, 0, true);
			temp->SetFAP(26, 0, true);
			temp->SetFAP(27, 0, true);
			temp->SetFAP(28, 0, true);
			temp->SetFAP(29, 0, true);
			temp->SetFAP(30, 0, true);

			//temp->SetFAP(31, (upperface) * (vert)*((CapturedFrame*) (*iter))->points[4]->y, true);
			//temp->SetFAP(32, (upperface) *(vert)*((CapturedFrame*) (*iter))->points[4]->y, true);

			temp->SetFAP(31, (int) ( upperface * mask[31] * (vert) * ((CapturedFrame*) (*iter))->points[5]->z) , true);
			temp->SetFAP(32, (int) ( upperface * mask[32] *( vert) * ((CapturedFrame*) (*iter))->points[6]->z) , true);

			temp->SetFAP(33, (int) ( upperface * mask[33] * (vert) * ((CapturedFrame*) (*iter))->points[5]->z) , true);
			temp->SetFAP(34, (int) ( upperface * mask[34] * (vert) * ((CapturedFrame*) (*iter))->points[6]->z) , true);

			temp->SetFAP(35, (int) ( upperface * mask[35] * (vert) * ((CapturedFrame*) (*iter))->points[7]->z) , true);
			temp->SetFAP(36, (int) ( upperface * mask[36] * (vert) * ((CapturedFrame*) (*iter))->points[9]->z) , true);


			//check it maybe y!!!


			if ( ((CapturedFrame*) (*iter))->points[5]->x > 0 )
				temp->SetFAP ( 37, (int) ( upperface * mask[37] *  ( (CapturedFrame*) (*iter))->points[5]->x ) , true);
			else
				temp->SetFAP(37,0,true );

			if ( ((CapturedFrame*) (*iter))->points[6]->x > 0 )
				temp->SetFAP(38, (int) ( upperface * mask[38] *  ( (CapturedFrame*) (*iter))->points[6]->x) , true);
			else
				temp->SetFAP(38, 0, true);


			//temp->SetFAP (37,  (upperface) *(hor)*  ( (CapturedFrame*) (*iter))->points[5]->x, true);

			//temp->SetFAP(38, (upperface) * (ahor)* ( (CapturedFrame*) (*iter))->points[6]->x, true);

			//temp->SetFAP(37, 50*(upperface) *(ahor)* ( ((CapturedFrame*) (*iter))->points[5]->z), true);
			//temp->SetFAP(38, 50*(upperface) *(-1)*(hor)*  ( ( (CapturedFrame*) (*iter) )->points[6]->z), true);


			temp->SetFAP(39, (int) ( lowerface *mask[39] *(hor)*((CapturedFrame*) (*iter))->points[18]->x ) , true);
			temp->SetFAP(40, (int) ( lowerface *mask[40] *(ahor)*((CapturedFrame*) (*iter))->points[16]->x ) , true);

			temp->SetFAP(41, (int) ( lowerface *mask[41] *(vert)*((CapturedFrame*) (*iter))->points[15]->z ) , true);
			temp->SetFAP(42, (int) ( lowerface *mask[42] *(vert)*((CapturedFrame*) (*iter))->points[17]->z ) , true);

			temp->SetFAP(43, 0, true);
			temp->SetFAP(44, 0, true);
			temp->SetFAP(45, 0, true);
			temp->SetFAP(46, 0, true);
			temp->SetFAP(47, 0, true);

			//up down	 (9)		
			temp->SetFAP(48,  (int) (lowerface *mask[48] *(hor)*((CapturedFrame*) (*iter))->points[28]->y ), true);
			//temp->SetFAP(48,0,true);

			//left right
			temp->SetFAP(49, (int) ( lowerface *mask[49] *(vert)*((CapturedFrame*) (*iter))->points[28]->x ), true);
			//temp->SetFAP(49,0,true);

			//przekszywienie
			temp->SetFAP(50,  (int) ( lowerface *mask[50] *((CapturedFrame*) (*iter))->points[28]->z ), true);
			//temp->SetFAP(50,0,true);

			//upper center
			temp->SetFAP(51, (int) ( lowerface *mask[51] *(avert)*((CapturedFrame*) (*iter))->points[20]->z ), true);

			//temp->SetFAP(52, (lowerface) *(vert)*  ( ( (CapturedFrame*) (*iter) )->points[26]->z), true);

			//lowerlip
			temp->SetFAP(52, (int) (lowerface *mask[52] *(vert)*( (((CapturedFrame*) (*iter))->points[23]->z) + (((CapturedFrame*) (*iter))->points[25]->z) )/2 ), true);

			//corner
			temp->SetFAP(53, (int) ( lowerface *mask[53] *(hor)*((CapturedFrame*) (*iter))->points[22]->x), true);

			//cornre
			temp->SetFAP(54, (int) (lowerface *mask[54] *(ahor)*((CapturedFrame*) (*iter))->points[24]->x), true);

			//temp->SetFAP(55, (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[15]->y, true);
			//temp->SetFAP(56, (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[16]->y, true);
			//upper left

			int temp55 = (int) (lowerface *mask[55] *(avert)*((CapturedFrame*) (*iter))->points[19]->z);
			//upperleft
			int temp56 = (int) (lowerface *mask[56] *(avert)*((CapturedFrame*) (*iter))->points[21]->z);

			//upper center
			int temp51 = (int) (lowerface * mask[51] *(avert)*((CapturedFrame*) (*iter))->points[20]->z);

			if (temp55<0) temp55 = std::max ( (int)(temp55*0.5),  temp51); 
			if (temp56<0) temp56 = std::max ( (int)(temp56*0.5) , temp51); 

			temp->SetFAP(55, temp55, true);
			temp->SetFAP(56, temp56, true);

			//lower right
			temp->SetFAP(57, (int) (lowerface *mask[57] *(vert)*((CapturedFrame*) (*iter))->points[23]->z), true);
			//lower left
			temp->SetFAP(58, (int) (lowerface *mask[58] *(vert)*((CapturedFrame*) (*iter))->points[25]->z), true);

			//cornes
			temp->SetFAP(59, (int) (lowerface *mask[59]*(vert)*((CapturedFrame*) (*iter))->points[22]->z), true);
			//corners
			temp->SetFAP(60, (int) (lowerface *mask[60]*(vert)*((CapturedFrame*) (*iter))->points[24]->z), true);

			//temp->SetFAP(61, (lowerface) *(hor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			//temp->SetFAP(62, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			//temp->SetFAP(63, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[25]->y , true);
			//temp->SetFAP(64, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[25]->x, true);

			temp->SetFAP(61, 0, true);
			temp->SetFAP(62, 0, true);
			temp->SetFAP(63, 0, true);
			temp->SetFAP(64, 0, true);

			temp->SetFAP(65, 0, true);
			temp->SetFAP(66, 0, true);
			temp->SetFAP(67, 0, true);
			temp->SetFAP(68, 0, true);



			////check it 27
			//if ( ((CapturedFrame*) (*iter))->points[27]->y >0 )
			//	temp->SetFAP(3,  1.9 * (lowerface) * (avert)*((CapturedFrame*) (*iter))->points[27]->y , true);		
			//else
			//	temp->SetFAP(3,  1.3 * (lowerface) * (avert)*((CapturedFrame*) (*iter))->points[27]->y , true);		

			////upper lip
			//temp->SetFAP(4,  3*(lowerface) *(avert)*((CapturedFrame*) (*iter))->points[20]->y, true);
			//
			////lower lip
			//temp->SetFAP(5,  3* (lowerface) *(vert)*  ( ( (CapturedFrame*) (*iter) ) -> points[24]->y ) );

			////temp->SetFAP(5,  3*(lowerface) *(vert)*  ( (((CapturedFrame*) (*iter))->points[25]->y) + (((CapturedFrame*) (*iter))->points[23]->y) )/2  , true);
			//
			////temp->SetFAP(6,  (lowerface) *(ahor)* ((CapturedFrame*) (*iter))->points[19]->x, true);
			////temp->SetFAP(7,  (lowerface) *(hor)* ((CapturedFrame*) (*iter))->points[20]->x, true);
			//
			//temp->SetFAP(8,  3*(lowerface) *(avert)*((CapturedFrame*) (*iter))->points[19]->y, true);
			//temp->SetFAP(9,  3*(lowerface) *(avert)*((CapturedFrame*) (*iter))->points[21]->y, true);
			//
			//temp->SetFAP(10, 3*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[23]->y, true);
			//temp->SetFAP(11, 3*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[25]->y, true);
			//
			////temp->SetFAP(12, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[19]->y, true);
			////temp->SetFAP(13, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[20]->y, true);

			////27
			//temp->SetFAP(14, (lowerface) *((CapturedFrame*) (*iter))->points[27]->z, true);
			//temp->SetFAP(15, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[27]->x, true);

			////temp->SetFAP(16, 2*(lowerface) *(vert)* ( (((CapturedFrame*) (*iter))->points[17]->y) + (((CapturedFrame*) (*iter))->points[18]->y) )/2 , true);
			////temp->SetFAP(17, 2*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[14]->z, true);
			//
			////27
			//temp->SetFAP(18, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[27]->y, true);
			//
			//temp->SetFAP(19, 0, true);
			//temp->SetFAP(20, 0, true);

			//temp->SetFAP(21, 0, true);
			//temp->SetFAP(22, 0, true);
			//temp->SetFAP(23, 0, true);
			//temp->SetFAP(24, 0, true);
			//temp->SetFAP(25, 0, true);
			//temp->SetFAP(26, 0, true);
			//temp->SetFAP(27, 0, true);
			//temp->SetFAP(28, 0, true);
			//temp->SetFAP(29, 0, true);
			//temp->SetFAP(30, 0, true);

			////temp->SetFAP(31, (upperface) * (vert)*((CapturedFrame*) (*iter))->points[4]->y, true);
			////temp->SetFAP(32, (upperface) *(vert)*((CapturedFrame*) (*iter))->points[4]->y, true);

			//temp->SetFAP(31, 1.3*(upperface) * (vert)*((CapturedFrame*) (*iter))->points[5]->y, true);
			//temp->SetFAP(32, 1.3*(upperface) *(vert)*((CapturedFrame*) (*iter))->points[6]->y, true);

			//temp->SetFAP(33, (upperface) *(vert)*((CapturedFrame*) (*iter))->points[7]->y, true);
			//temp->SetFAP(34, (upperface) *(vert)*((CapturedFrame*) (*iter))->points[9]->y, true);

			//temp->SetFAP(35, (0.5)*(upperface) *(vert)*((CapturedFrame*) (*iter))->points[7]->y, true);
			//temp->SetFAP(36, (0.5)*(upperface) *(vert)*((CapturedFrame*) (*iter))->points[9]->y, true);


			////check it maybe y!!!

			//if ( ((CapturedFrame*) (*iter))->points[5]->y >0 )
			//	temp->SetFAP (37, -4* (upperface) *  ( (CapturedFrame*) (*iter))->points[5]->x, true);
			//else
			//	temp->SetFAP(37,0,true );

			//if ( ((CapturedFrame*) (*iter))->points[6]->y >0 )
			//	temp->SetFAP(38, -4*(upperface) *  ( (CapturedFrame*) (*iter))->points[6]->x, true);
			//else
			//	temp->SetFAP(38, 0, true);

			////temp->SetFAP(37, 50*(upperface) *(ahor)* ( ((CapturedFrame*) (*iter))->points[5]->z), true);
			////temp->SetFAP(38, 50*(upperface) *(-1)*(hor)*  ( ( (CapturedFrame*) (*iter) )->points[6]->z), true);


			//temp->SetFAP(39, (lowerface) *(hor)*((CapturedFrame*) (*iter))->points[16]->x, true);
			//temp->SetFAP(40, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[18]->x, true);

			//temp->SetFAP(41, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[15]->y, true);
			//temp->SetFAP(42, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[17]->y, true);
			//
			//temp->SetFAP(43, 0, true);
			//temp->SetFAP(44, 0, true);
			//temp->SetFAP(45, 0, true);
			//temp->SetFAP(46, 0, true);
			//temp->SetFAP(47, 0, true);

			//	//up down	 (9)		
			////temp->SetFAP(48,  13 * (lowerface) *(hor)*((CapturedFrame*) (*iter))->points[30]->y, true);
			//temp->SetFAP(48,0,true);

			////left right
			////temp->SetFAP(49,  3* (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[30]->z, true);
			//temp->SetFAP(49,0,true);

			////przekszywienie
			////temp->SetFAP(50,  2 * (lowerface) *((CapturedFrame*) (*iter))->points[30]->x, true);
			//temp->SetFAP(50,0,true);


			//temp->SetFAP(51, 2*(lowerface) *(avert)*((CapturedFrame*) (*iter))->points[20]->y, true);

			//temp->SetFAP(52, 2*(lowerface) *(vert)*  ( ( (CapturedFrame*) (*iter) )->points[26]->y), true);
			//
			////temp->SetFAP(52, 2*(lowerface) *(vert)*( (((CapturedFrame*) (*iter))->points[17]->y) + (((CapturedFrame*) (*iter))->points[18]->y) )/2, true);
			//
			//if ( ((CapturedFrame*) (*iter))->points[22]->x >0 )
			//	temp->SetFAP(53, 1*(lowerface) *(hor)*((CapturedFrame*) (*iter))->points[22]->x, true);
			//else
			//	temp->SetFAP(53, 0.7*(lowerface) *(hor)*((CapturedFrame*) (*iter))->points[22]->x, true);
			//
			//if ( ((CapturedFrame*) (*iter))->points[24]->x >0 )
			//	temp->SetFAP(54, 0.7*(lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[24]->x, true);
			//else
			//	temp->SetFAP(54, 1*(lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[24]->x, true);

			////temp->SetFAP(55, (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[15]->y, true);
			////temp->SetFAP(56, (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[16]->y, true);
			//
			//int temp55 = (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[19]->y;
			//
			//int temp56 = (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[21]->y;

			//int temp51 = 2*(lowerface) *(avert)*((CapturedFrame*) (*iter))->points[20]->y;

			//if (temp55<0) temp55 = std::max ( (int)(temp55*0.5),  temp51); 
			//if (temp56<0) temp56 = std::max ( (int)(temp56*0.5) , temp51); 

			//temp->SetFAP(55, temp55, true);
			//temp->SetFAP(56, temp56, true);
			//
			//temp->SetFAP(57, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[25]->y, true);
			//temp->SetFAP(58, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[23]->y, true);
			//
			//temp->SetFAP(59, 1.9*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[24]->y, true);
			//temp->SetFAP(60, 1.9*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[22]->y, true);

			////temp->SetFAP(61, (lowerface) *(hor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			////temp->SetFAP(62, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			////temp->SetFAP(63, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[25]->y , true);
			////temp->SetFAP(64, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			//
			//temp->SetFAP(61, 0, true);
			//temp->SetFAP(62, 0, true);
			//temp->SetFAP(63, 0, true);
			//temp->SetFAP(64, 0, true);

			//temp->SetFAP(65, 0, true);
			//temp->SetFAP(66, 0, true);
			//temp->SetFAP(67, 0, true);
			//temp->SetFAP(68, 0, true);

			fapframevector->push_back(*temp);

		}//end of else

	}//end of for


	std::vector<FAPFrame>::iterator iter1;

	int inde=0;	

	for(iter1=fapframevector->begin();iter1!=fapframevector->end();iter1++)
	{

		for (int ii=3;ii<69;ii++){

			((FAPFrame)(*iter1)).GetFAP(ii);

			((FAPFrame)(*iter1)).SetFAP(ii,0,true);

			if ( ((FAPFrame)(*iter1)).GetFAP(ii) < fslimits[1][ii] ) 
			{
				printf("%d: %d %d.",ii, ((FAPFrame)(*iter1)).GetFAP(ii), fslimits[1][ii] );			
				((FAPFrame)(*iter1)).SetFAP(ii, fslimits[1][ii] ,true);
				(*fapframevector)[inde].SetFAP(ii, fslimits[1][ii] ,true);

			}

			if ( ((FAPFrame)(*iter1)).GetFAP(ii) > fslimits[0][ii] ) 
			{
				printf("%d: %d %d.",ii, ((FAPFrame)(*iter1)).GetFAP(ii), fslimits[0][ii] );			
				((FAPFrame)(*iter1)).SetFAP(ii, fslimits[0][ii] ,true);
				(*fapframevector)[inde].SetFAP(ii, fslimits[0][ii] ,true);

			}
		}

		printf("\n");

		inde++;

	}
	printf("\n"); 

	return fapframevector;

}




bool OptiTrackConverter::readMHMessage(char *mcfilename)
{

	FILE *fapfile;

	if(!(fapfile = fopen(mcfilename,"r"))){
		printf("WARNING: can't read %s\n",mcfilename);
		return false;
	}

	int framenumber=0;
	//float  time=0.0;

	float x1b=0.0,y1b=0.0,z1b=0.0;
	float x2b=0.0,y2b=0.0,z2b=0.0;
	float x3b=0.0,y3b=0.0,z3b=0.0;
	float x4b=0.0,y4b=0.0,z4b=0.0;
	float x5b=0.0,y5b=0.0,z5b=0.0;
	float x6b=0.0,y6b=0.0,z6b=0.0;
	float x7b=0.0,y7b=0.0,z7b=0.0;
	float x8b=0.0,y8b=0.0,z8b=0.0;
	float x9b=0.0,y9b=0.0,z9b=0.0;
	float x10b=0.0,y10b=0.0,z10b=0.0;
	float x11b=0.0,y11b=0.0,z11b=0.0;
	float x12b=0.0,y12b=0.0,z12b=0.0;
	float x13b=0.0,y13b=0.0,z13b=0.0;
	float x14b=0.0,y14b=0.0,z14b=0.0;
	float x15b=0.0,y15b=0.0,z15b=0.0;
	float x16b=0.0,y16b=0.0,z16b=0.0;
	float x17b=0.0,y17b=0.0,z17b=0.0;
	float x18b=0.0,y18b=0.0,z18b=0.0;
	float x19b=0.0,y19b=0.0,z19b=0.0;
	float x20b=0.0,y20b=0.0,z20b=0.0;
	float x21b=0.0,y21b=0.0,z21b=0.0;
	float x22b=0.0,y22b=0.0,z22b=0.0;
	float x23b=0.0,y23b=0.0,z23b=0.0;
	float x24b=0.0,y24b=0.0,z24b=0.0;
	float x25b=0.0,y25b=0.0,z25b=0.0;
	float x26b=0.0,y26b=0.0,z26b=0.0;
	float x27b=0.0,y27b=0.0,z27b=0.0;

	//rotations?

	//float x30b=0.0,y30b=0.0,z30b=0.0;
	//float x31b=0.0,y31b=0.0,z31b=0.0;


	float x1=0.0,y1=0.0,z1=0.0;
	float x2=0.0,y2=0.0,z2=0.0;
	float x3=0.0,y3=0.0,z3=0.0;
	float x4=0.0,y4=0.0,z4=0.0;
	float x5=0.0,y5=0.0,z5=0.0;
	float x6=0.0,y6=0.0,z6=0.0;
	float x7=0.0,y7=0.0,z7=0.0;
	float x8=0.0,y8=0.0,z8=0.0;
	float x9=0.0,y9=0.0,z9=0.0;
	float x10=0.0,y10=0.0,z10=0.0;
	float x11=0.0,y11=0.0,z11=0.0;
	float x12=0.0,y12=0.0,z12=0.0;
	float x13=0.0,y13=0.0,z13=0.0;
	float x14=0.0,y14=0.0,z14=0.0;
	float x15=0.0,y15=0.0,z15=0.0;
	float x16=0.0,y16=0.0,z16=0.0;
	float x17=0.0,y17=0.0,z17=0.0;
	float x18=0.0,y18=0.0,z18=0.0;
	float x19=0.0,y19=0.0,z19=0.0;
	float x20=0.0,y20=0.0,z20=0.0;
	float x21=0.0,y21=0.0,z21=0.0;
	float x22=0.0,y22=0.0,z22=0.0;
	float x23=0.0,y23=0.0,z23=0.0;
	float x24=0.0,y24=0.0,z24=0.0;
	float x25=0.0,y25=0.0,z25=0.0;
	float x26=0.0,y26=0.0,z26=0.0;
	float x27=0.0,y27=0.0,z27=0.0;

	//rotations

	//float x30=0.0,y30=0.0,z30=0.0;
	//float x31=0.0,y31=0.0,z31=0.0;

	char mystring[10000];

	fgets (mystring , 9999, fapfile);

	//frame number
	sscanf(mystring, "%f",&x1b);
	//x1
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x1b, &y1b, &z1b);
	//x2
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x2b, &y2b, &z2b);
	//x3
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x3b, &y3b, &z3b);
	//x4
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x4b, &y4b, &z4b);
	//x5
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x5b, &y5b, &z5b);
	//x6
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x6b, &y6b, &z6b);
	//x7
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x7b, &y7b, &z7b);
	//x8
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x8b, &y8b, &z8b);
	//x9
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x9b, &y9b, &z9b);
	//x10
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x10b, &y10b, &z10b);
	//x11
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x11b, &y11b, &z11b);
	//x12
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x12b, &y12b, &z12b);
	//x13
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x13b, &y13b, &z13b);
	//x14
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x14b, &y14b, &z14b);
	//x15
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x15b, &y15b, &z15b);
	//x16
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x16b, &y16b, &z16b);
	//x17
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x17b, &y17b, &z17b);
	//x18
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x18b, &y18b, &z18b);
	//x19
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x19b, &y19b, &z19b);
	//x20
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x20b, &y20b, &z20b);
	//x21
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x21b, &y21b, &z21b);
	//x22
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x22b, &y22b, &z22b);
	//x23
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x23b, &y23b, &z23b);
	//x24
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x24b, &y24b, &z24b);
	//x25
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x25b, &y25b, &z25b);
	//x26
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x26b, &y26b, &z26b);
	//x27
	fgets (mystring , 9999, fapfile);
	sscanf(mystring, "%f %f %f",&x27b, &y27b, &z27b);

	//x28
	//sscanf(mystring, "%f %f %f",&x28b, &y28b, &z28b);


	//change this
	float nosex0 = x25b;
	float nosey0 = y25b;

	CapturedFrame *temp = new CapturedFrame(framenumber,0.0);

	std::string label1="tymczas";

	//change to zeros - the first frame should be neutral ;-)

	/*
	temp->addPoint(0,0,0,1,label1);
	temp->addPoint(x2b,y2b,z2b,2,label1);
	temp->addPoint(x3b,y3b,z3b,3,label1);
	temp->addPoint(x4b,y4b,z4b,4,label1);
	temp->addPoint(x5b,y5b,z5b,5,label1);
	temp->addPoint(x6b,y6b,z6b,6,label1);
	temp->addPoint(x7b,y7b,z7b,7,label1);
	temp->addPoint(x8b,y8b,z8b,8,label1);
	temp->addPoint(x9b,y9b,z9b,9,label1);
	temp->addPoint(x10b,y10b,z10b,10,label1);
	temp->addPoint(x11b,y11b,z11b,11,label1);
	temp->addPoint(x12b,y12b,z12b,12,label1);
	temp->addPoint(x13b,y13b,z13b,13,label1);
	temp->addPoint(x14b,y14b,z14b,14,label1);
	temp->addPoint(x15b,y15b,z15b,15,label1);
	temp->addPoint(x16b,y16b,z16b, 16,label1);
	temp->addPoint(x17b,y17b,z17b,17,label1);
	temp->addPoint(x18b,y18b,z18b,18,label1);
	temp->addPoint(x19b,y19b,z19b,19,label1);
	temp->addPoint(x20b,y20b,z20b,20,label1);
	temp->addPoint(x21b,y21b,z21b,21,label1);
	temp->addPoint(x22b,y22b,z22b,22,label1);
	temp->addPoint(x23b,y23b,z23b,23,label1);
	temp->addPoint(x24b,y24b,z24b,24,label1);
	temp->addPoint(x25b,y25b,z25b,25,label1);
	temp->addPoint(x26b,y26b,z26b,26,label1);
	temp->addPoint(x27b,y27b,z27b,27,label1);
	*/

	temp->addPoint(0,0,0,1,label1);
	temp->addPoint(0,0,0,2,label1);
	temp->addPoint(0,0,0,3,label1);
	temp->addPoint(0,0,0,4,label1);
	temp->addPoint(0,0,0,5,label1);
	temp->addPoint(0,0,0,6,label1);
	temp->addPoint(0,0,0,7,label1);
	temp->addPoint(0,0,0,8,label1);
	temp->addPoint(0,0,0,9,label1);
	temp->addPoint(0,0,0,10,label1);
	temp->addPoint(0,0,0,11,label1);
	temp->addPoint(0,0,0,12,label1);
	temp->addPoint(0,0,0,13,label1);
	temp->addPoint(0,0,0,14,label1);
	temp->addPoint(0,0,0,15,label1);
	temp->addPoint(0,0,0,16,label1);
	temp->addPoint(0,0,0,17,label1);
	temp->addPoint(0,0,0,18,label1);
	temp->addPoint(0,0,0,19,label1);
	temp->addPoint(0,0,0,20,label1);
	temp->addPoint(0,0,0,21,label1);
	temp->addPoint(0,0,0,22,label1);
	temp->addPoint(0,0,0,23,label1);
	temp->addPoint(0,0,0,24,label1);
	temp->addPoint(0,0,0,25,label1);
	temp->addPoint(0,0,0,26,label1);
	temp->addPoint(0,0,0,27,label1);

	//no head movement?
	temp->addPoint(0,0,0,28,label1);

	capturedframes.push_back(temp);

	int licznik=0;

	while( fgets (mystring , 9999, fapfile) )
	{

		licznik++;

		//frame number
		sscanf(mystring, "%f",&x1);
		//x1
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x1, &y1, &z1);
		//x2
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x2, &y2, &z2);
		//x3
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x3, &y3, &z3);
		//x4
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x4, &y4, &z4);
		//x5
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x5, &y5, &z5);
		//x6
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x6, &y6, &z6);
		//x7
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x7, &y7, &z7);
		//x8
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x8, &y8, &z8);
		//x9
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x9, &y9, &z9);
		//x10
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x10, &y10, &z10);
		//x11
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x11, &y11, &z11);
		//x12
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x12, &y12, &z12);
		//x13
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x13, &y13, &z13);
		//x14
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x14, &y14, &z14);
		//x15
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x15, &y15, &z15);
		//x16
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x16, &y16, &z16);
		//x17
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x17, &y17, &z17);
		//x18
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x18, &y18, &z18);
		//x19
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x19, &y19, &z19);
		//x20
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x20, &y20, &z20);
		//x21
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x21, &y21, &z21);
		//x22
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x22, &y22, &z22);
		//x23
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x23, &y23, &z23);
		//x24
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x24, &y24, &z24);
		//x25
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x25, &y25, &z25);
		//x26
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x26, &y26, &z26);
		//x27
		fgets (mystring , 9999, fapfile);
		sscanf(mystring, "%f %f %f",&x27, &y27, &z27);

		//x28
		//sscanf(mystring, "%f %f %f",&x28, &y28, &z28);


		//change this
		float nosexcurrent = x25;
		float noseycurrent = y25;

		//float nosex = nosex0 - nosexcurrent;
		//float nosey = nosey0 - noseycurrent;

		float nosex = 0;
		float nosey = 0;


		CapturedFrame *temp = new CapturedFrame(framenumber,0.0);

		std::string label1="tymczas";

		//current frame - neutral frame
		temp->addPoint(x1-x1b,y1-y1b,z1-z1b,1,label1);
		temp->addPoint(x2-x2b,y2-y2b,z2-z2b,2,label1);
		temp->addPoint(x3-x3b,y3-y3b,z3-z3b,3,label1);
		temp->addPoint(x4-x4b,y4-y4b,z4-z4b,4,label1);
		temp->addPoint(x5-x5b,y5-y5b,z5-z5b,5,label1);
		temp->addPoint(x6-x6b,y6-y6b,z6-z6b,6,label1);
		temp->addPoint(x7-x7b,y7-y7b,z7-z7b,7,label1);
		temp->addPoint(x8-x8b,y8-y8b,z8-z8b,8,label1);
		temp->addPoint(x9-x9b,y9-y9b,z9-z9b,9,label1);
		temp->addPoint(x10-x10b,y10-y10b,z10-z10b,10,label1);
		temp->addPoint(x11-x11b,y11-y11b,z11-z11b,11,label1);
		temp->addPoint(x12-x12b,y12-y12b,z12-z12b,12,label1);
		temp->addPoint(x13-x13b,y13-y13b,z13-z13b,13,label1);
		temp->addPoint(x14-x14b,y14-y14b,z14-z14b,14,label1);
		temp->addPoint(x15-x15b,y15-y15b,z15-z15b,15,label1);
		temp->addPoint(x16-x16b,y16-y16b,z16-z16b,16,label1);
		temp->addPoint(x17-x17b,y17-y17b,z17-z17b,17,label1);
		temp->addPoint(x18-x18b,y18-y18b,z18-z18b,18,label1);
		temp->addPoint(x19-x19b,y19-y19b,z19-z19b,19,label1);
		temp->addPoint(x20-x20b,y20-y20b,z20-z20b,20,label1);
		temp->addPoint(x21-x21b,y21-y21b,z21-z21b,21,label1);
		temp->addPoint(x22-x22b,y22-y22b,z22-z22b,22,label1);
		temp->addPoint(x23-x23b,y23-y23b,z23-z23b,23,label1);
		temp->addPoint(x24-x24b,y24-y24b,z24-z24b,24,label1);
		temp->addPoint(x25-x25b,y25-y25b,z25-z25b,25,label1);
		temp->addPoint(x26-x26b,y26-y26b,z26-z26b,26,label1);
		temp->addPoint(x27-x27b,y27-y27b,z27-z27b,27,label1);

		temp->addPoint(0,0,0,28,label1);

		//temp->addPoint(x29b-x29-nosex,y29b-y29-nosey,z29b-z29,29,label1);
		//temp->addPoint(x30b-x30-nosex,y30b-y30-nosey,z30b-z30,30,label1);

		capturedframes.push_back(temp);

	}//end of while 

	printf("number of frames is: %d \n",licznik);

	fclose(fapfile);

	return true;

}


std::vector<FAPFrame> * OptiTrackConverter::converttoFAPs2()
{
	std::vector<FAPFrame> *fapframevector;

	fapframevector = new std::vector<FAPFrame>;

	std::vector<CapturedFrame*>::iterator iter;

	int index=0;
	//(*fapframevector).resize(capturedframes.size());

	for(iter=capturedframes.begin();iter!=capturedframes.end();iter++)
	{


		if (index<2){

			FAPFrame *temp = new FAPFrame();
			temp->SetFAP(0, index, true);
			temp->SetFAP(1, 0, false);
			temp->SetFAP(2, 0, false);

			temp->framenumber=index++;

			fapframevector->push_back(*temp);
		}

		else

		{

			FAPFrame *temp = new FAPFrame();

			temp->framenumber=index;
			temp->SetFAP(0, index, true);
			index++;

			temp->SetFAP(1, 0, false);
			temp->SetFAP(2, 0, false);

			float vert = 1.0;
			float avert = -1.0*vert;

			float ahor= 1.0;
			float hor=  -1.0 * ahor;


			//check it 27

			if ( ((CapturedFrame*) (*iter))->points[27]->y >0 )
				temp->SetFAP(3, (int) ( mask[3] * (avert)*((CapturedFrame*) (*iter))->points[27]->y ), true);		
			else	
				temp->SetFAP(3,  (int) ( 0.5*mask[3] * (avert)*((CapturedFrame*) (*iter))->points[27]->y ), true);		

			//upper lip
			temp->SetFAP(4,  (int) (mask[4] *(avert)*((CapturedFrame*) (*iter))->points[20]->y ), true);

			//lower lip
			//temp->SetFAP(5,  (1)*(lowerface) *(vert)*  ( ( (CapturedFrame*) (*iter) ) -> points[26]->z ) );

			temp->SetFAP(5,  (int) (  mask[5] *(vert)*  ( (((CapturedFrame*) (*iter))->points[25]->y) + (((CapturedFrame*) (*iter))->points[23]->y) )/1.5 ) , true);

			//temp->SetFAP(6,  (lowerface) *(ahor)* ((CapturedFrame*) (*iter))->points[22]->x, true);
			//temp->SetFAP(7,  (lowerface) *(hor)* ((CapturedFrame*) (*iter))->points[24]->x, true);


			temp->SetFAP(8,  (int) (mask[8] *(avert)*((CapturedFrame*) (*iter))->points[19]->y ), true);
			//upper irgt
			temp->SetFAP(9,  (int) (mask[9] *(avert)*((CapturedFrame*) (*iter))->points[21]->y ), true);

			//lower left
			temp->SetFAP(10, (int) (mask[10] *(vert)*((CapturedFrame*) (*iter))->points[25]->y ), true);
			//lower right
			temp->SetFAP(11, (int) (mask[11] *(vert)*((CapturedFrame*) (*iter))->points[23]->y ), true);

			//12 and 13
			temp->SetFAP(12, (int) (mask[12] *(vert)*((CapturedFrame*) (*iter))->points[22]->y ), true);
			temp->SetFAP(13, (int) (mask[13] *(vert)*((CapturedFrame*) (*iter))->points[24]->y ), true);

			//jaw movewment on z

			//temp->SetFAP(14, (int) (mask[12] *(vert)*((CapturedFrame*) (*iter))->points[22]->y ), true);

			//jaw movewment on x

			//temp->SetFAP(15, (int) (mask[13] *(vert)*((CapturedFrame*) (*iter))->points[24]->y ), true);


			//temp->SetFAP(16, 2*(lowerface) *(vert)* ( (((CapturedFrame*) (*iter))->points[17]->y) + (((CapturedFrame*) (*iter))->points[18]->y) )/2 , true);
			//temp->SetFAP(17, 2*(lowerface) *(vert)*((CapturedFrame*) (*iter))->points[14]->z, true);

			//27
			temp->SetFAP(18, (int) ( mask[18] *(vert)*((CapturedFrame*) (*iter))->points[27]->z ), true);

			temp->SetFAP(19, 0, true);
			temp->SetFAP(20, 0, true);

			temp->SetFAP(21, 0, true);
			temp->SetFAP(22, 0, true);
			temp->SetFAP(23, 0, true);
			temp->SetFAP(24, 0, true);
			temp->SetFAP(25, 0, true);
			temp->SetFAP(26, 0, true);
			temp->SetFAP(27, 0, true);
			temp->SetFAP(28, 0, true);
			temp->SetFAP(29, 0, true);
			temp->SetFAP(30, 0, true);

			//temp->SetFAP(31, (upperface) * (vert)*((CapturedFrame*) (*iter))->points[4]->y, true);
			//temp->SetFAP(32, (upperface) *(vert)*((CapturedFrame*) (*iter))->points[4]->y, true);

			temp->SetFAP(31, (int) (  mask[31] * (vert) * ((CapturedFrame*) (*iter))->points[5]->y) , true);
			temp->SetFAP(32, (int) ( mask[32] *( vert) * ((CapturedFrame*) (*iter))->points[6]->y) , true);

			temp->SetFAP(33, (int) (  mask[33] * (vert) * ((CapturedFrame*) (*iter))->points[5]->y) , true);
			temp->SetFAP(34, (int) (  mask[34] * (vert) * ((CapturedFrame*) (*iter))->points[6]->y) , true);

			temp->SetFAP(35, (int) (  mask[35] * (vert) * ((CapturedFrame*) (*iter))->points[7]->y) , true);
			temp->SetFAP(36, (int) (  mask[36] * (vert) * ((CapturedFrame*) (*iter))->points[9]->y) , true);


			//check it maybe y!!!


			if ( ((CapturedFrame*) (*iter))->points[5]->x > 0 )
				temp->SetFAP ( 37, (int) (  mask[37] *(ahor)*  ( (CapturedFrame*) (*iter))->points[5]->x ) , true);
			else
				temp->SetFAP(37,0,true );

			if ( ((CapturedFrame*) (*iter))->points[6]->x > 0 )
				temp->SetFAP(38, (int) ( mask[38] *(hor)* ( (CapturedFrame*) (*iter))->points[6]->x) , true);
			else
				temp->SetFAP(38, 0, true);


			//temp->SetFAP (37,  (upperface) *(hor)*  ( (CapturedFrame*) (*iter))->points[5]->x, true);

			//temp->SetFAP(38, (upperface) * (ahor)* ( (CapturedFrame*) (*iter))->points[6]->x, true);

			//temp->SetFAP(37, 50*(upperface) *(ahor)* ( ((CapturedFrame*) (*iter))->points[5]->z), true);
			//temp->SetFAP(38, 50*(upperface) *(-1)*(hor)*  ( ( (CapturedFrame*) (*iter) )->points[6]->z), true);


			temp->SetFAP(39, (int) ( mask[39] *(hor)*((CapturedFrame*) (*iter))->points[18]->x ) , true);
			temp->SetFAP(40, (int) ( mask[40] *(ahor)*((CapturedFrame*) (*iter))->points[16]->x ) , true);

			temp->SetFAP(41, (int) ( mask[41] *(vert)*((CapturedFrame*) (*iter))->points[15]->y ) , true);
			temp->SetFAP(42, (int) ( mask[42] *(vert)*((CapturedFrame*) (*iter))->points[17]->y ) , true);

			temp->SetFAP(43, 0, true);
			temp->SetFAP(44, 0, true);
			temp->SetFAP(45, 0, true);
			temp->SetFAP(46, 0, true);
			temp->SetFAP(47, 0, true);

			//up down	 (9)		
			temp->SetFAP(48,  (int) (mask[48] *(hor)*((CapturedFrame*) (*iter))->points[28]->y ), true);
			//temp->SetFAP(48,0,true);

			//left right
			temp->SetFAP(49, (int) ( mask[49] *(vert)*((CapturedFrame*) (*iter))->points[28]->x ), true);
			//temp->SetFAP(49,0,true);

			//przekszywienie
			temp->SetFAP(50,  (int) ( mask[50] *((CapturedFrame*) (*iter))->points[28]->z ), true);
			//temp->SetFAP(50,0,true);

			//upper center
			temp->SetFAP(51, (int) ( mask[51] *(avert)*((CapturedFrame*) (*iter))->points[20]->y ), true);

			//temp->SetFAP(52, (lowerface) *(vert)*  ( ( (CapturedFrame*) (*iter) )->points[26]->z), true);

			//lowerlip
			temp->SetFAP(52, (int) (mask[52] *(vert)*( (((CapturedFrame*) (*iter))->points[23]->y) + (((CapturedFrame*) (*iter))->points[25]->y) )/2 ), true);

			//corner
			temp->SetFAP(53, (int) ( mask[53] *(hor)*((CapturedFrame*) (*iter))->points[22]->x), true);

			//cornre
			temp->SetFAP(54, (int) (mask[54] *(ahor)*((CapturedFrame*) (*iter))->points[24]->x), true);

			//temp->SetFAP(55, (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[15]->y, true);
			//temp->SetFAP(56, (lowerface) *(avert)*((CapturedFrame*) (*iter))->points[16]->y, true);
			//upper left

			int temp55 = (int) (mask[55] *(avert)*((CapturedFrame*) (*iter))->points[19]->y);
			//upperleft
			int temp56 = (int) (mask[56] *(avert)*((CapturedFrame*) (*iter))->points[21]->y);

			//upper center
			int temp51 = (int) (mask[51] *(avert)*((CapturedFrame*) (*iter))->points[20]->y);

			if (temp55<0) temp55 = std::max ( (int)(temp55*0.5),  temp51); 
			if (temp56<0) temp56 = std::max ( (int)(temp56*0.5) , temp51); 

			temp->SetFAP(55, temp55, true);
			temp->SetFAP(56, temp56, true);

			//lower right
			temp->SetFAP(57, (int) (mask[57] *(vert)*((CapturedFrame*) (*iter))->points[23]->y), true);
			//lower left
			temp->SetFAP(58, (int) (mask[58] *(vert)*((CapturedFrame*) (*iter))->points[25]->y), true);

			//cornes
			temp->SetFAP(59, (int) (mask[59]*(vert)*((CapturedFrame*) (*iter))->points[22]->y), true);
			//corners
			temp->SetFAP(60, (int) (mask[60]*(vert)*((CapturedFrame*) (*iter))->points[24]->y), true);

			//temp->SetFAP(61, (lowerface) *(hor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			//temp->SetFAP(62, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[25]->x, true);
			//temp->SetFAP(63, (lowerface) *(vert)*((CapturedFrame*) (*iter))->points[25]->y , true);
			//temp->SetFAP(64, (lowerface) *(ahor)*((CapturedFrame*) (*iter))->points[25]->x, true);

			temp->SetFAP(61, 0, true);
			temp->SetFAP(62, 0, true);
			temp->SetFAP(63, 0, true);
			temp->SetFAP(64, 0, true);

			temp->SetFAP(65, 0, true);
			temp->SetFAP(66, 0, true);
			temp->SetFAP(67, 0, true);
			temp->SetFAP(68, 0, true);

			fapframevector->push_back(*temp);

		}//end of else

	}//end of for



	// consider limits
	
	/*std::vector<FAPFrame>::iterator iter1;

	int inde=0;	

	for(iter1=fapframevector->begin();iter1!=fapframevector->end();iter1++)
	{

		for (int ii=3;ii<69;ii++){

			((FAPFrame)(*iter1)).GetFAP(ii);

			((FAPFrame)(*iter1)).SetFAP(ii,0,true);

			if ( ((FAPFrame)(*iter1)).GetFAP(ii) < fslimits[1][ii] ) 
			{
				printf("%d: %d %d.",ii, ((FAPFrame)(*iter1)).GetFAP(ii), fslimits[1][ii] );			
				((FAPFrame)(*iter1)).SetFAP(ii, fslimits[1][ii] ,true);
				(*fapframevector)[inde].SetFAP(ii, fslimits[1][ii] ,true);

			}

			if ( ((FAPFrame)(*iter1)).GetFAP(ii) > fslimits[0][ii] ) 
			{
				printf("%d: %d %d.",ii, ((FAPFrame)(*iter1)).GetFAP(ii), fslimits[0][ii] );			
				((FAPFrame)(*iter1)).SetFAP(ii, fslimits[0][ii] ,true);
				(*fapframevector)[inde].SetFAP(ii, fslimits[0][ii] ,true);

			}
		}

		printf("\n");

		inde++;

	}
	printf("\n"); 
	*/

	
	return fapframevector;

}

