#ifndef PLAYEROGREBODYSKELETON_H
#define PLAYEROGREBODYSKELETON_H

#include <Ogre.h>
#include "BAPFrame.h"
#include "BAPTypes.h"

class PlayerOgreBodySkeleton{
public:
	PlayerOgreBodySkeleton(Ogre::Skeleton* skel);
	void ApplyBAPFrame(BAPFrame* bf);
	Ogre::Skeleton* getSkeleton(){ return bodySkeleton; }
	void Reset();

private:
	Ogre::Skeleton* bodySkeleton;

	double** values;//values of the frame currently displayed
	bool* mask;

	int numBones;
	//mapping between skeleton and BAPs
	int BAPtoBONE[NUMBAPS+1][2];

	Ogre::Quaternion* originalOrientation; //must be the default position of MPEG-4
	Ogre::Quaternion* originalderivedOrientation; //(parents orientation)

	void applyValues();
};

#endif


