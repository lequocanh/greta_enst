//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// FacePartsToolWindow.h: interface for the FacePartsToolWindow class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FacePartsToolWindow_H__FE981889_170B_11D9_9C92_FEB87D89E47F__INCLUDED_)
#define AFX_FacePartsToolWindow_H__FE981889_170B_11D9_9C92_FEB87D89E47F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Output.H>
#include <fl/fl_counter.h>
#include <fl/Fl_Box.h>
#include <fl/fl_value_output.h>
#include "PlayerFLTKWindow.h"
#include "PlayerOgreView.h"
#include <FL/Fl_Roller.H>

class FacePartsToolWindow : public Fl_Double_Window  
{
public:
	FacePartsToolWindow();
	virtual ~FacePartsToolWindow();
	PlayerOgreView *glutw;
	int handle(int event);
	void draw();
	
	Fl_Roller * l_eye_x;
	Fl_Roller * l_eye_y;
	Fl_Roller * l_eye_z;

	Fl_Roller * r_eye_x;
	Fl_Roller * r_eye_y;
	Fl_Roller * r_eye_z;

	Fl_Roller * eyes_scale;

	Fl_Roller * teeth_x;
	Fl_Roller * teeth_y;
	Fl_Roller * teeth_z;

	Fl_Roller * teeth_scale;

	Fl_Roller * hair_x;
	Fl_Roller * hair_y;
	Fl_Roller * hair_z;

	Fl_Roller * scale_hair_x;
	Fl_Roller * scale_hair_y;
	Fl_Roller * scale_hair_z;

	Fl_Roller * head_x;
	Fl_Roller * head_y;
	Fl_Roller * head_z;

	Fl_Roller * head_scale;
	
private:
	

	Fl_Button *switchhair;
	Fl_Button *switchwireframe;
	Fl_Button *save;

	Fl_Box *box_eyes;
	Fl_Box *box_teeth;
	Fl_Box *box_head;
	Fl_Box *box_hair;

	bool wireframeOn;
	bool hairOn;
	
public:
	void WriteFile(void);
	void show();
	static void initCounter( Fl_Roller * counter); 

};

#endif // !defined(AFX_FacePartsToolWindow_H__FE981889_170B_11D9_9C92_FEB87D89E47F__INCLUDED_)
