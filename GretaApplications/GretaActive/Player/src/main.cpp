//Copyright 1999-2010 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "RealtimePlayerWindow.h"
#include "PlayerReceiver.h"
#include "CentralClock.h"
#include "IniManager.h"
#include "RandomGen.h"
#include "FL/Fl.H"
#include "FileNames.h"
#include "XercesTool.h"
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;


FILE *agent_log;
FileNames filenames;
IniManager inimanager;
RandomGen *randomgen;
std::string ini_filename;
semaine::components::greta::PlayerReceiver *playerreceiver;

/*int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInstance,
     LPSTR     lpszCmdParm,
     int       nCmdShow
)*/
void main(int argc, char ** argv)
{
	XercesTool::startupXMLTools();
	XMLTool::startupXMLTools();
	/*if (argc==2)
	{
		ini_filename=argv[1];
	} else {
		ini_filename="greta.ini";
	}*/
	ini_filename="greta_activemq.ini";
	inimanager.ReadIniFile(ini_filename);
	randomgen=new RandomGen();

	//sert a ne pas avoir de personnage listener dans semaine:
	inimanager.SetValueInt("PLAYER_SHOWLISTENER", 0);

	try{
		CentralClock pc;
		RealtimePlayerWindow rpw(&pc);
		playerreceiver = new semaine::components::greta::PlayerReceiver("PlayerOgre", false, true ,&rpw, &pc ); //,rpw.am,//rpw.fm, rpw.cm,&(rpw.pc));
		playerreceiver->setFinished(0);
		rpw.show();	
		Fl::wait();
		rpw.glwindow->setCharacterSpeaker("Obadiah");
		rpw.glwindow->setCharacterSpeaker("Spike");
		rpw.glwindow->setCharacterSpeaker("Poppy");
		rpw.glwindow->setCharacterSpeaker("Prudence");
		playerreceiver->setFinished(1);
		Fl::run();
		_exit(0);

	}
	catch(CMSException & e) {
		printf("\n*********************************************************\nActiveMQ is not started. The application will be stopped\n*********************************************************");
		MessageBox(0, _T("ActiveMQ is not started. The application will be stopped"), _T("Warning"), MB_OK | MB_ICONWARNING);
		_exit(0);
	}
	XercesTool::shutdownXMLTools();
	XMLTool::shutdownXMLTools();
}
