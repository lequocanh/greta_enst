// Shared-Attention system
// Christopher Peters

#include "scene.h"

extern IniManager inimanager;

Scene::Scene(void)
{
	//setup the scene objects	
	Named3DObject  *obj = new Named3DObject("GRETA", OBJECT_GRETA, 0.0, -0.3,  -10.0, 0.0, 0.0, 0.0);
	VAO* vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	obj = new Named3DObject("BACKGROUND", OBJECT_BACKGROUND, 0.0, 0.0,  -100.0, 0.1, 0.1, 0.1);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;
	
	obj = new Named3DObject("TABLE", OBJECT_TABLE, 0.0, -0.18,  -3.0, 0.3, 0.3, 0.3);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	
	obj = new Named3DObject("REDBOX", OBJECT_BOX, 0.05, -0.35,  -1.1, 0.8, 0.0, 0.0);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	obj = new Named3DObject("GREENBOX", OBJECT_BOX, -0.15, -0.17,  -1.1, 0.0, 0.8, 0.0);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	obj = new Named3DObject("BLUEBOX", OBJECT_BOX, 0.40, -0.25,  -1.1, 0.0, 0.0, 0.8);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	obj = new Named3DObject("YELLOWBOX", OBJECT_BOX, -0.4, -0.3,  -1.1, 0.8, 0.8, 0.0);
	vao = new VAO(obj); obj->vao = vao;
	if (this->addNewObject(obj)) printf("\nAdded new object to scene (%s %d)\n", obj->getName()->c_str(), obj->getID());
	else delete obj;

	outsideScreenVAO = new VAO(NULL, true);

	this->gretaAgent=NULL;
	this->calibrator = new Calibrator();

	this->lastSelectedObjID = -1;

	//example calibration values
	this->bvs.headBottomVal = -0.476717;	//-0.463624
	this->bvs.headTopVal = 0.216475;		//0.236161
	this->bvs.headLeftVal = -0.393575;		//-0.520153
	this->bvs.headRightVal = 0.447276;		//0.333079
}

Scene::~Scene()
{
	for (int i = 0; i < this->objects.size(); i++)
	{
		delete this->objects[i];
	}
}

bool Scene::addNewObject(Named3DObject *no)
{
	//check if the name is there already
	Named3DObject *check = this->getObjectByName(no->getName());
	if (check != NULL) return false;

	this->objects.push_back(no);
	return true;
}

Named3DObject *Scene::getObjectByID(int ID)
{
	for (int i = 0; i < this->objects.size(); i++)
	{
		if (this->objects[i]->getID() == ID) return this->objects[i];
	}
	return NULL;
}

Named3DObject *Scene::getObjectByName(std::string *name)
{
	for (int i = 0; i < this->objects.size(); i++)
	{
		if (this->objects[i]->getName()->compare(name->c_str()) == 0) return this->objects[i];
	}
	return NULL;
}

void Scene::drawSceneObjects(void)
{
	
	if (this->gretaAgent == NULL)
	{
		
		this->gretaAgent = new SharedAttentionAgentAspect("speaker",inimanager.GetValueString("CHARACTER_SPEAKER"),false);//realtime
		this->gretaAgent->AssignFile("empty");
		this->gretaAgent->EnableAudio(false);
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookat");		
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		
}
	else 
	{

	 if (this->objects[4]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookdown");
		this->objects[4]->vao->TimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
		  }
if (this->objects[0]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookat");
		this->objects[0]->vao->TimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
	 
}
if (this->objects[1]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookat");
		this->objects[1]->vao->TimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
		 }

if (this->objects[2]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookat");
		this->objects[2]->vao->TimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
	 
}

if (this->objects[3]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookdown");
		this->objects[3]->vao->TimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
	for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
	 }


 	
if (this->objects[5]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookdownleft");
		//this->objects[5]->vao->totalTimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
			for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
	
 }
if (this->objects[6]->vao->TimeLookedAt>3000)
{
		this->gretaAgent->AssignFile("g:\\code\\greta\\bin\\shared\\lookdownright");
		//this->objects[6]->vao->totalTimeLookedAt=0;
		this->gretaAgent->started_animation=0;
	 	this->gretaAgent->StartTalking();
		hours=clock();
		for (int i = 0; i < this->objects.size(); i++)this->objects[i]->vao->TimeLookedAt=0;
		
	
 }
		
}//end of else

	//this->gretaAgent->MoveAgent(0,0,-100);
		//this->gretaAgent->MoveForward(10);
		//this->gretaAgent->RotateAgent(0,180,0);
	
	for (int i = 0; i < this->objects.size(); i++)
	{
		this->drawObjectByIndex(i);
	}

	//this->gretaAgent->draw();		
	
}

void Scene::drawObjectByIndex(int index)
{
	if (this->objects[index] != NULL) 
	{
		if (this->objects[index]->type != OBJECT_GRETA)
		{
			this->objects[index]->draw();
		}
		else	//draw Greta
		{

			//glEnable(GL_LIGHTING);
		
			glLoadName(this->objects[index]->objectID);

			glPushMatrix();
			glTranslatef(this->objects[index]->position[0], this->objects[index]->position[1], this->objects[index]->position[2]);
			
			glScalef(0.007,0.007,0.007);
			
			glEnable(GL_LIGHTING);
			this->gretaAgent->draw();
			glDisable(GL_LIGHTING);

			glPopMatrix();
		}
	}
}

void Scene::updatePickedObjectsVAO(int objectID)
{
	if (objectID < 0) return;
	Named3DObject *obj = NULL;
	if ((obj = this->getObjectByID(objectID)) != NULL)
	{
		if (obj->vao != NULL) obj->vao->update(clock(), this->lastSelectedObjID == objectID);
		this->lastSelectedObjID = objectID;
	}
	else printf("\nError: selected objectID not found...");
}