#include "Converter.h"

#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;


Converter::Converter()	
{

	framerate=25;

	fslimits=(int**)malloc(sizeof(int*)*2);
	for(unsigned int i=0;i<2;i++)
	{

		fslimits[i]=(int*)malloc(sizeof(int)*69);
		for(unsigned int j=0;j<69;j++)
			fslimits[i][j]=0;
	}

	//<fap nr="0" min="0" max="0" masking="0"/>	
	fslimits[0][0]=0;
	fslimits[1][0]=0;
	//<fap nr="1" min="0" max="0" masking="0"/>
	fslimits[0][1]=0;
	fslimits[1][1]=0;
	//<fap nr="2" min="0" max="0" masking="0"/>
	fslimits[0][2]=0;
	fslimits[1][2]=0;
	//<fap nr="3" min="255" max="-150" masking="1"/>
	fslimits[0][3]=500;
	fslimits[1][3]=-100;
	//<fap nr="4" min="45" max="-45" masking="1"/>
	fslimits[0][4]=700;
	fslimits[1][4]=-500;
	//<fap nr="5" min="300" max="-540" masking="1"/>
	fslimits[0][5]=200;
	fslimits[1][5]=-2000;
	//<fap nr="6" min="50" max="-40" masking="1"/>
	fslimits[0][6]=10000;
	fslimits[1][6]=-10000;
	//<fap nr="7" min="50" max="-40" masking="1"/>
	fslimits[0][7]=10000;
	fslimits[1][7]=-10000;
	//<fap nr="8" min="0" max="0" masking="1"/>
	fslimits[0][8]=10000;
	fslimits[1][8]=-10000;
	//<fap nr="9" min="0" max="0" masking="1"/>
	fslimits[0][9]=10000;
	fslimits[1][9]=-10000;
	//<fap nr="10" min="0" max="0" masking="1"/>
	fslimits[0][10]=10000;
	fslimits[1][10]=-10000;
	//<fap nr="11" min="0" max="0" masking="1"/>
	fslimits[0][11]=10000;
	fslimits[1][11]=-10000;
	//<fap nr="12" min="0" max="0" masking="1"/>
	fslimits[0][12]=0;
	fslimits[1][12]=0;;
	//<fap nr="13" min="0" max="0" masking="1"/>
	fslimits[0][13]=0;;
	fslimits[1][13]=0;;
	//<fap nr="14" min="0" max="0" masking="0"/>
	fslimits[0][14]=10000;;
	fslimits[1][14]=-10000;;
	//<fap nr="15" min="0" max="0" masking="0"/>
	fslimits[0][15]=10000;;
	fslimits[1][15]=-10000;;
	//<fap nr="16" min="0" max="0" masking="0"/>
	fslimits[0][16]=10000;;
	fslimits[1][16]=-10000;;
	//<fap nr="17" min="200" max="0" masking="1"/>
	fslimits[0][17]=10000;;
	fslimits[1][17]=-10000;;
	//<fap nr="18" min="0" max="0" masking="1"/>
	fslimits[0][18]=600;;
	fslimits[1][18]=-250;;
	//<fap nr="19" min="200" max="-256" masking="1"/>
	fslimits[0][19]=10000;;
	fslimits[1][19]=-10000;;
	//<fap nr="20" min="200" max="-256" masking="1"/>
	fslimits[0][20]=10000;;
	fslimits[1][20]=-10000;;
	//<fap nr="21" min="100" max="-100" masking="1"/>
	fslimits[0][21]=10000;;
	fslimits[1][21]=-10000;;
	//<fap nr="22" min="100" max="-100" masking="1"/>
	fslimits[0][22]=10000;;
	fslimits[1][22]=-10000;;
	//<fap nr="23" min="0" max="0" masking="0"/>
	fslimits[0][23]=10000;;
	fslimits[1][23]=-10000;;
	//<fap nr="24" min="0" max="0" masking="0"/>
	fslimits[0][24]=10000;;
	fslimits[1][24]=-10000;;
	//<fap nr="25" min="0" max="0" masking="0"/>
	fslimits[0][25]=10000;;
	fslimits[1][25]=-10000;;
	//<fap nr="26" min="0" max="0" masking="0"/>
	fslimits[0][26]=10000;;
	fslimits[1][26]=-10000;;
	//<fap nr="27" min="0" max="0" masking="0"/>
	fslimits[0][27]=10000;;
	fslimits[1][27]=-10000;;
	//<fap nr="28" min="0" max="0" masking="0"/>
	fslimits[0][28]=10000;;
	fslimits[1][28]=-10000;;
	//<fap nr="29" min="0" max="0" masking="0"/>
	fslimits[0][29]=10000;;
	fslimits[1][29]=-10000;;
	//<fap nr="30" min="0" max="0" masking="0"/>
	fslimits[0][30]=10000;;
	fslimits[1][30]=-10000;;
	//<fap nr="31" min="220" max="-110" masking="1"/>
	fslimits[0][31]=220;
	fslimits[1][31]=-110;
	//<fap nr="32" min="220" max="-110" masking="1"/>
	fslimits[0][32]=220;
	fslimits[1][32]=-110;
	//<fap nr="33" min="140" max="-55" masking="1"/>
	fslimits[0][33]=250;
	fslimits[1][33]=-250;
	//<fap nr="34" min="140" max="-55" masking="1"/>
	fslimits[0][34]=250;
	fslimits[1][34]=-250;
	//<fap nr="35" min="70" max="0" masking="1"/>
	fslimits[0][35]=200;
	fslimits[1][35]=-200;
	//<fap nr="36" min="70" max="0" masking="1"/>
	fslimits[0][36]=200;
	fslimits[1][36]=-200;
	//<fap nr="37" min="110" max="0" masking="1"/>
	fslimits[0][37]=300;
	fslimits[1][37]=-100;
	//<fap nr="38" min="110" max="0" masking="1"/>
	fslimits[0][38]=300;
	fslimits[1][38]=-100;
	//<fap nr="39" min="0" max="0" masking="0"/>
	fslimits[0][39]=200;;
	fslimits[1][39]=-200;;
	//<fap nr="40" min="0" max="0" masking="0"/>
	fslimits[0][40]=200;;
	fslimits[1][40]=-200;;
	//<fap nr="41" min="100" max="0" masking="1"/>
	fslimits[0][41]=400;;
	fslimits[1][41]=0;;
	//<fap nr="42" min="100" max="0" masking="1"/>
	fslimits[0][42]=400;;
	fslimits[1][42]=0;;
	//<fap nr="43" min="0" max="0" masking="0"/>
	fslimits[0][43]=10000;;
	fslimits[1][43]=-10000;;
	//<fap nr="44" min="0" max="0" masking="0"/>
	fslimits[0][44]=10000;;
	fslimits[1][44]=-10000;;
	//<fap nr="45" min="0" max="0" masking="0"/>
	fslimits[0][45]=10000;;
	fslimits[1][45]=-10000;;
	//<fap nr="46" min="0" max="0" masking="0"/>
	fslimits[0][46]=10000;;
	fslimits[1][46]=-10000;;
	//<fap nr="47" min="0" max="0" masking="0"/>
	fslimits[0][47]=10000;;
	fslimits[1][47]=-10000;;
	//<fap nr="48" min="0" max="0" masking="0"/>
	fslimits[0][48]=30000;;
	fslimits[1][48]=-30000;;
	//<fap nr="49" min="0" max="0" masking="0"/>
	fslimits[0][49]=60000;;
	fslimits[1][49]=-60000;;
	//<fap nr="50" min="0" max="0" masking="0"/>
	fslimits[0][50]=20000;;
	fslimits[1][50]=-20000;;
	//<fap nr="51" min="150" max="0" masking="1"/>
	fslimits[0][51]=10000;;
	fslimits[1][51]=-350;	
	//<fap nr="52" min="180" max="0" masking="1"/>
	fslimits[0][52]=10000;;
	fslimits[1][52]=-10000;;
	//<fap nr="53" min="50" max="-32" masking="1"/>
	fslimits[0][53]=200;
	fslimits[1][53]=-130;;
	//<fap nr="54" min="50" max="-32" masking="1"/>
	fslimits[0][54]=200;;
	fslimits[1][54]=-130;;
	//<fap nr="55" min="0" max="-120" masking="1"/>
	fslimits[0][55]=1000;;
	fslimits[1][55]=-170;;
	//<fap nr="56" min="0" max="-120" masking="1"/>
	fslimits[0][56]=1000;;
	fslimits[1][56]=-170;;
	//<fap nr="57" min="0" max="0" masking="1"/>
	fslimits[0][57]=900;;
	fslimits[1][57]=-1000;;
	//<fap nr="58" min="0" max="0" masking="1"/>
	fslimits[0][58]=1000;
	fslimits[1][58]=-1000;
	//<fap nr="59" min="200" max="-130" masking="1"/>
	fslimits[0][59]=400;
	fslimits[1][59]=-200;
	//<fap nr="60" min="200" max="-130" masking="1"/>
	fslimits[0][60]=400;;
	fslimits[1][60]=-200;;
	//<fap nr="61" min="140" max="0" masking="1"/>
	fslimits[0][61]=140;;
	fslimits[1][61]=-50;;
	//<fap nr="62" min="140" max="0" masking="1"/>
	fslimits[0][62]=140;;
	fslimits[1][62]=-50;;
	//<fap nr="63" min="0" max="0" masking="0"/>
	fslimits[0][63]=0;;
	fslimits[1][63]=0;;
	//<fap nr="64" min="0" max="0" masking="0"/>
	fslimits[0][64]=0;;
	fslimits[1][64]=0;;
	//<fap nr="65" min="0" max="0" masking="0"/>
	fslimits[0][65]=10000;;
	fslimits[1][65]=-10000;;
	//<fap nr="66" min="0" max="0" masking="0"/>
	fslimits[0][66]=10000;;
	fslimits[1][66]=-10000;;
	//<fap nr="67" min="0" max="0" masking="0"/>
	fslimits[0][67]=10000;;
	fslimits[1][67]=-10000;;
	//<fap nr="68" min="0" max="0" masking="0"/>
	fslimits[0][68]=10000;;
	fslimits[1][68]=-10000;;

 
	
};


Converter::~Converter(){};


bool Converter::readMCMessage(char *mcfilename)
{
	return false;
}

std::vector<FAPFrame> *Converter::converttoFAPs()
{
	return NULL;
}

bool Converter::readmask(std::string filename)
{
	//int * tab = new int [69];
	
	FILE *fapfile;

	if(!(fapfile = fopen(filename.c_str(),"r"))){
		printf("WARNING: can't read %s\n",filename);
		return false;
	}
	
	char mystring[100];
	
	for (int i=1;i<69;i++)
	{
		float temp=0;
		fgets (mystring ,99,fapfile);
		temp=atof(mystring);
		//sscanf(mystring, "%f %f %f", temp);
		mask[i]=(float)temp;
	}	

	return true;
}