/*
 *  SystemConfigurationException.h
 *  semaine
 *
 *  Created by Marc Schröder on 09.09.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#ifndef SYSTEMCONFIGURATIONEXCEPTION_H
#define SYSTEMCONFIGURATIONEXCEPTION_H

#include "GRETAException.h"

namespace excep {

class SystemConfigurationException : public GRETAException
{
public:
		inline SystemConfigurationException(const std::string & text) throw() :
		GRETAException(text)
		{
		}
		
		inline ~SystemConfigurationException() throw() {}

};

} //excep namespace

#endif

