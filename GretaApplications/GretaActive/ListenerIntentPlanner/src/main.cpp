//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <conio.h>
#include <list>

#include "ListenerIntentPlanner.h"
#include "DataContainer.h"
#include "RandomGen.h"
#include "IniManager.h"
#include "CentralClock.h"
#include "XercesTool.h"
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;

DataContainer *datacontainer;
IniManager inimanager;
std::string ini_filename;
std::string directory;
CentralClock *pc;
RandomGen *ran;




void main(int argc, char ** argv)
{
	XercesTool::startupXMLTools();
	XMLTool::startupXMLTools();

	if (argc==2)
	{
		ini_filename=argv[1];
		directory="";
	} else if (argc==3)
	{
		ini_filename=argv[1];
		directory=argv[2];
	} else {
        ini_filename="greta_activemq.ini";
		directory="";
	}

	

	//datacontainer needs some data..
	inimanager.ReadIniFile(ini_filename);
	datacontainer = new DataContainer();
	pc = new CentralClock();

	ran=new RandomGen();
	int code=datacontainer->initAll(directory);
	if (code==0) 
		{
		printf("Problem : out \n");
		exit(1);
	}

	//semaine::components::greta::ListenerIntentPlanner *ip = new semaine::components::greta::ListenerIntentPlanner("ListenerIntentPlanner", false, false);
	try{
		ListenerIntentPlanner lip("ListenerIntentPlanner", false, false);
		getch();
	}
	catch(CMSException & e) {
		printf("\n*********************************************************\nActiveMQ is not started. The application will be stopped\n*********************************************************");
		//getch();
		MessageBox(0, _T("ActiveMQ is not started. The application will be stopped"), _T("Warning"), MB_OK | MB_ICONWARNING);
		_exit(0);
	}
	XercesTool::shutdownXMLTools();
	XMLTool::shutdownXMLTools();
}



