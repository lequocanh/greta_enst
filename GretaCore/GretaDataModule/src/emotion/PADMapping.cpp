

#include "PADMapping.h"


//To Do: Parse xml file

/**
*  
* 
* constructor
* 
*/

PADMapping::PADMapping()
{
}

/**
*  
* 
* build the mapping
* @return  error code
*/

int PADMapping::init(){


	//-.51	.59	.25	Anger	-++

	PADElement *anger = new PADElement(-0.51, 0.59, 0.25,"angerP","anger");
	this->padtable["anger"]= anger;


	//-.22	-.70	.25	bored	- - +

	PADElement *bored = new PADElement(-0.22,-0.70,0.25,"boredP","bored");
	this->padtable["bored"]= bored;

	//-.64	.6	-.43	FEAR	-+-

	PADElement *fear = new PADElement(-0.64,0.6,-0.43,"fearP","fear");
	this->padtable["fear"]= fear;

	//-.3	.1	-.6	SHAME/EMB	-+-

	PADElement *embarrassment = new PADElement(-0.3,0.1,-0.6,"embarrassmentP","embarrassment");
	this->padtable["embarrassment"]= embarrassment;


	//-.2	-.1	-.1	tension	---

	PADElement *tension = new PADElement(-0.2,-0.1,-0.1,"tensionP","tension");
	this->padtable["tension"]= tension;

	//-.5	-.3	-.4	sadn	---

	PADElement *sadness = new PADElement(-0.5,-0.3,-0.4,"sadnessP","sadness");
	this->padtable["sadness"]= sadness;

	//.4	.2	.1	JOY	+++

	PADElement *joy = new PADElement(0.4,0.2,0.1,"joyP","joy");
	this->padtable["joy"]= joy;

	//.4	.3	.3	PRIDE	+++

	PADElement *pride = new PADElement(0.4,0.3,0.3,"prideP","pride");
	this->padtable["pride"]= pride;

	//.2	-.3	.4	RELIEF	+ - +

	PADElement *relief = new PADElement(0.2,-0.3,0.4,"reliefP","relief");
	this->padtable["relief"]= relief;

	//.4	.3	-.3	joyful surprise	++-

	PADElement *joysurprise = new PADElement(0.4, 0.3,-0.3,"surpriseP","surprise");
	this->padtable["surprise"]= joysurprise;

	//	std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;
	//	for(iter=mecs.begin();iter!=mecs.end();iter++)

	return 1;

}


/**
*  
* 
* destructor
* 
*/

PADMapping::~PADMapping()
{
}

/**
*  
* 
* finds an element
* @param label
* @return 
* 
*/

PADElement* PADMapping::getPADelement(std::string label){

	std::map<std::string,PADElement*>::const_iterator iter;

	for(iter=this->padtable.begin();iter!=this->padtable.end();iter++)
	{
		if (label.compare( (*iter).first)==0 )
			return this->padtable[label];	
	}

	return NULL;

}//end of getpadelement
