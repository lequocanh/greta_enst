#include "Animation.h"

void Animation::init(std::string id)//, int bapPendingPacks_, int fapPendingPacks_, int audioPendingPacks_)
{
	bufferAudio_size = 0;
	bapframes.clear();
	fapframes.clear();
	animID = id;
	bufferAudio = 0;
	hasAudio = false;
	//bapPendingPacks = bapPendingPacks_;
	//fapPendingPacks = fapPendingPacks_;
	//audioPendingPacks = audioPendingPacks_;
	stat = WAITING;
}

Animation::Animation(std::string id)//, int bapPendingPacks_, int fapPendingPacks_, int audioPendingPacks_)
{
	init(id);//,bapPendingPacks_,fapPendingPacks_,audioPendingPacks_);
}

Animation::Animation(){
	init("");//,0,0,0);
	setStartInfos(0,0,0, false);
}

Animation::~Animation(){
	fapframes.clear();
	bapframes.clear();
	if(bufferAudio!=NULL)
		delete[] bufferAudio;
}

void Animation::setStartInfos(double priority_, _time_ startTime_, _time_ lifeTime_, bool hasAudio_){
	priority = priority_;
	lifeTime = lifeTime_;
	startTime = startTime_;
	hasAudio = hasAudio_;
	stat = READY;
}

void Animation::setAudio(char* buffer, int size){
	bufferAudio_size = size;
	bufferAudio = (char*)malloc(bufferAudio_size*sizeof(char)); 
	for(int i=0;i<bufferAudio_size;i++)
		bufferAudio[i] = buffer[i];
}

void Animation::add(BAPFrame frame){
	if(bapframes.empty()){
		bapframes.push_back(frame);
		return;
	}
	int index = 0;
	while(index<bapframes.size() && frame.GetFrameNumber()>bapframes[index].GetFrameNumber())
		++index;
	bapframes.insert(bapframes.begin()+index,frame);
}

void Animation::add(FAPFrame frame){
	if(fapframes.empty()){
		fapframes.push_back(frame);
		return;
	}
	int index = 0;
	while(index<fapframes.size() && frame.framenumber>fapframes[index].framenumber)
		++index;
	fapframes.insert(fapframes.begin()+index,frame);

}
