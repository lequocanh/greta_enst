import java.util.ArrayList;
import java.util.List;

/**
 * Class that constructs a FML message (understandable by Greta) given a simple text message
 * 
 * @author Magalie
 *
 */
public class MessageFML {
	
	/* langage of Greta */
	public static String langage="english";
	
	/* Counter for time marker */
	private int _tmFin;
	
	/* Constructed fml messaage */
	private String _finalmessageFML;
	
	/* initial simple text message */
	private String _initialTextmessage;
	/* message modified with BML tag */
	private String _messageWithBMLTags;
	/* message modified with FML tag */
	private String _messageWithFMLTags;
	
	/* time marker corresponding to ponctuation */
	private List<Integer> _idPoints;
	private List<Integer> _idPointInterrogation;
	private List<Integer> _idPointExclamation;
	private List<Integer> _idVirgule;
	
	/**
	 * Method that construct the FML message 
	 * 
	 * @param message simple text message that will be transformed to FML message 
	 */
	public MessageFML(String message){
		this._idPoints=new ArrayList<Integer>();
		this._idPointInterrogation=new ArrayList<Integer>();
		this._idPointExclamation=new ArrayList<Integer>();
		this._idVirgule=new ArrayList<Integer>();
		
		this._initialTextmessage=message;
		this._tmFin=1;
		this._finalmessageFML=this.createFMLMessage(message);
		
		
	}
	
	/**
	 * Method that return the constructed FML message
	 * @return fml message
	 */
	public String getFMLMessage(){
		return this._finalmessageFML;
	}

	/**
	 * Method that construct the FML message 
	 * @param message initial simple text message
	 * @return
	 */
	public String createFMLMessage(String message){
		String message2=message.replace("\n"," ");
		String message3=message2.replace("  "," ");
		
		
		this._messageWithBMLTags=putPonctutationGesture(message3);
		
		this._messageWithFMLTags=this.putFMLTag();
		
		String messageToSend="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><!DOCTYPE fml-apml SYSTEM \"fml-apml.dtd\" []> <fml-apml><bml> <speech id=\"s1\" start=\"0.0\" language=\""
			+langage+"\" voice=\"openmary\" text=\""+message3+"\"><description level=\"1\" type=\"gretabml\"><reference>tmp/from-fml-apml2.pho</reference></description><tm id=\"tm1\"/> "
			+this._messageWithBMLTags+" </speech></bml>"+this._messageWithFMLTags+"</fml-apml>";
		
		return messageToSend;

	}
	
	/**
	 * Method to automatically added gestures:
	 * (see documentation for the precise algorithm used to generate gesture  
	 * 
	 * @return resulting FML message with gesture tags
	 */
	public String putFMLTag(){
		StringBuffer s=new StringBuffer("<fml>");
		int p=0;
		/* put turn taking */
		s.append("<turntaking id=\"p"+p+"\" type=\"takethefloor\" start=\"s1:tm1\" end=\"1.0\"/>");
		p++;
		
		/* put perfomative-greetings if message contains hello */
		if(this._initialTextmessage.toLowerCase().contains("hello") ||
				this._initialTextmessage.toLowerCase().contains("good morning")
				|| this._initialTextmessage.toLowerCase().contains("good night")
				|| this._initialTextmessage.toLowerCase().contains("good afternoon")
				|| this._initialTextmessage.toLowerCase().contains("bye")
				|| this._initialTextmessage.toLowerCase().contains("Hi there")){
			s.append("<performative id=\"p"+p+"\" type=\"greet\" start=\"s1:tm1\" end=\"1.0\"/>");
			p++;
		}
		
		
		/* put backchannelagreement if sentence contains yes*/
		if(this._initialTextmessage.toLowerCase().contains("yes")){
			int begin=this._messageWithBMLTags.toLowerCase().indexOf("yes");
			//insert time marker
			int tmY=1;
			String tm1="<tm id=\"tmY"+tmY+"\"/>";

			
			StringBuffer sb=new StringBuffer(this._messageWithBMLTags.toLowerCase());
			sb.insert(begin,tm1);
			
			this._messageWithBMLTags=sb.toString();
			
			s.append("<backchannel id=\"p"+p+"\" type=\"agreement\" start=\"s1:tmY1\" end=\"1.0\"/>");
			p++;
		}
		
		
		/* put backchannel-disagreement if sentence contains no*/
		if(this._initialTextmessage.toLowerCase().contains("no ")){
			int begin=this._messageWithBMLTags.toLowerCase().indexOf("no");
			
			int tmN=1;
			String tm1="<tm id=\"tmN"+tmN+"\"/>";

			
			StringBuffer sb=new StringBuffer(this._messageWithBMLTags.toLowerCase());
			
			sb.insert(begin,tm1);
			
			this._messageWithBMLTags=sb.toString();
			
			s.append("<backchannel id=\"p"+p+"\" type=\"disagreement\" start=\"s1:tmN1\" end=\"1.0\"/>");
			p++;
		}
		
		/* put backchannel-not undertsand*/
		String no_understand=noUndertsandStatment();
		if(no_understand!=null){
			int begin=this._messageWithBMLTags.toLowerCase().indexOf(no_understand);
			if(begin<0)
				begin=0;
			System.out.println("Index no understand "+begin+" no understand message "+no_understand+ "initial message "+this._initialTextmessage);
			String tm1="<tm id=\"tmNU1\"/>";
			StringBuffer sb=new StringBuffer(this._messageWithBMLTags.toLowerCase());
			sb.insert(begin,tm1);
			
			this._messageWithBMLTags=sb.toString();
			s.append("<backchannel id=\"p"+p+"\" type=\"no_understanding\" start=\"s1:tmNU1\" end=\"1.0\"/>");
			p++;
			
		}
		
		/* put backchannel-undertsand*/
		String yes_understand=undertsandStatment();
		if(yes_understand!=null){
			int begin=this._messageWithBMLTags.toLowerCase().indexOf(yes_understand);
			if(begin<0)
				begin=0;
			String tm1="<tm id=\"tmYU1\"/>";
			StringBuffer sb=new StringBuffer(this._messageWithBMLTags.toLowerCase());
			sb.insert(begin,tm1);
			
			this._messageWithBMLTags=sb.toString();
			s.append("<backchannel id=\"p"+p+"\" type=\"understanding\" start=\"s1:tmYU1\" end=\"1.0\"/>");
			p++;
			
		}
		
		/* Put gestures on questions */
		for(int i=0;i<this._idPointInterrogation.size();i++){
			int tm=this._idPointInterrogation.get(i)-1;
			if(tm<0)
				tm=0;
			
			double random=Math.random()*3;
			if(random<1)
				s.append("<backchannel id=\"p"+p+"\" type=\"interest\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
			else{
				if(random<2){
					s.append("<backchannel id=\"p"+p+"\" type=\"liking\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
				}else{
					s.append("<performative id=\"p"+p+"\" type=\"propose\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
				}
			}
			p++;
		}
		
		/* Put gestures on point */
		for(int i=0;i<this._idPoints.size();i++){
			int tm=this._idPoints.get(i)-1;
			if(tm<0)
				tm=0;
			
			double random=Math.random()*3;
			if(random<1)
				s.append("<backchannel id=\"p"+p+"\" type=\"belief\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
			else{
				if(random<2){
					s.append("<performative id=\"p"+p+"\" type=\"inform\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
				}else{
					s.append("<certainty id=\"p"+p+"\" type=\"certain\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
				}
			}
			p++;
		}
		
		
		
		/* Put gestures on exlamation point */
		for(int i=0;i<this._idPointExclamation.size();i++){
			int tm=this._idPointExclamation.get(i)-1;
			if(tm<0)
				tm=0;
			
			double random=Math.random()*3;
			if(random<1)
				s.append("<backchannel id=\"p"+p+"\" type=\"disbelief\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
			else{
				if(random<2){
					s.append("<backchannel id=\"p"+p+"\" type=\"no_interest\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
				}else{
					s.append("<backchannel id=\"p"+p+"\" type=\"disliking\" start=\"s1:tm"+tm+"\" end=\"1.0\"/>");
				}
			}
			p++;
		}
		
		
		s.append("<turntaking id=\"p"+p+"\" type=\"givethefloor\" start=\"s1:tm"+this._tmFin+"\" end=\"1.0\"/>");
		s.append("</fml>");
		return s.toString();
	}
	
	
	/** 
	 * Method that put time marker in the sentence 
	 * @param message message with the time marker at the position of ponctuation
	 * @return
	 */
	public String putPonctutationGesture(String message){
		
		StringBuffer ponctuatedMessage=new StringBuffer("<tm id=\"tm1\"/>");
				
		this._tmFin=2;
		Character c;
		for(int i=0;i<message.length();i++){
			c=message.charAt(i);
			
			if(c=='.'){
				ponctuatedMessage.append("<tm id=\"tm"+this._tmFin+"\"/>");
				this._idPoints.add(this._tmFin);
				this._tmFin++;
			}else{
				if(c=='!'){
					ponctuatedMessage.append("<tm id=\"tm"+this._tmFin+"\"/>");
					this._idPointExclamation.add(this._tmFin);
					this._tmFin++;
				}else{
					if(c=='?'){
						ponctuatedMessage.append("<tm id=\"tm"+this._tmFin+"\"/>");
						this._idPointInterrogation.add(this._tmFin);
						this._tmFin++;
					}else{
						if(c ==','){
							ponctuatedMessage.append("<tm id=\"tm"+this._tmFin+"\"/>");
							this._idVirgule.add(this._tmFin);
							this._tmFin++;
						}else{
							ponctuatedMessage.append(c);
							
						}
					}
				}
			}
		}
		this._tmFin--;
		
		int b=1;
		for(int i=0;i<this._idPoints.size();i++){
			ponctuatedMessage.append("<boundary id=\"b"+b+"\" type=\"LL\" start=\"s1:tm"+this._idPoints.get(i)+"\" end='1.0'/>");
			b++;
		}
		

		for(int i=0;i<this._idPointExclamation.size();i++){
			ponctuatedMessage.append("<boundary id=\"b"+b+"\" type=\"H\" start=\"s1:tm"+this._idPointExclamation.get(i)+"\" end='1.0'/>");
			b++;
		}
		
		for(int i=0;i<this._idPointInterrogation.size();i++){
			ponctuatedMessage.append("<boundary id=\"b"+b+"\" type=\"HH\" start=\"s1:tm"+this._idPointInterrogation.get(i)+"\" end='1.0'/>");
			b++;
		}
		
		for(int i=0;i<this._idVirgule.size();i++){
			ponctuatedMessage.append("<boundary id=\"b"+b+"\" type=\"LH\" start=\"s1:tm"+this._idVirgule.get(i)+"\" end='1.0'/>");
			b++;
		}
		
		return ponctuatedMessage.toString();
		
	}
	
	/**
	 * Method that return the String corresponding to a not understand sentence in the initial message, else null
	 * @return the String corresponding to a not understand sentence, else null
	 */
	public String noUndertsandStatment(){
		if(this._initialTextmessage.contains("I do not understand"))
			return "I do not understand";
		if(this._initialTextmessage.contains("I lost my train of thought"))
			return "I lost my train of thought";
		if(this._initialTextmessage.contains("That is a very original thought"))
			return "That is a very original thought";
		if(this._initialTextmessage.contains("We have never talked about it before"))
			return "We have never talked about it before";
		if (this._initialTextmessage.contains("Try saying that with more or less context"))
			return "Try saying that with more or less context";
		if(this._initialTextmessage.contains("Not many people express themselves that way"))
			return "Not many people express themselves that way";
		if(this._initialTextmessage.contains("I will mention that to my botmaster"))
			return "I will mention that to my botmaster";
		if (this._initialTextmessage.contains("My brain pattern set does not have a response for that"))
			return "My brain pattern set does not have a response for that";
		if(this._initialTextmessage.contains("What you said was either too complex or too simple for me"))
			return "What you said was either too complex or too simple for me";
		if(this._initialTextmessage.contains("A deeper algorithm is needed to respond to that correctly"))
			return "A deeper algorithm is needed to respond to that correctly";
		if(this._initialTextmessage.contains("I don't understand"))
			return "I don't understand";
		if(this._initialTextmessage.contains("I only hear that type of response less than five percent of the time"))
			return "I only hear that type of response less than five percent of the time";
		if(this._initialTextmessage.contains("My brain uses AIML to format responses to your inputs, but I don't have one for that"))
			return "My brain uses AIML to format responses to your inputs, but I don't have one for that";
		if(this._initialTextmessage.contains("My brain contains more than 22,000 patterns, but not one that matches your last input"))
			return "My brain contains more than 22,000 patterns";
		if(this._initialTextmessage.contains("That remark was either too complex or too simple for me"))
			return "That remark was either too complex or too simple for me";
		
		return null;
	}
	
	
	/**
	 * Method that return the String corresponding to an understand sentence in the initial message, else null
	 * @return the String corresponding to an understand sentence, else null
	 */
	public String undertsandStatment(){
		if(this._initialTextmessage.contains("I understand")){
			return "I understand";
		}
		return null;
		
	}

	
	
	
}
