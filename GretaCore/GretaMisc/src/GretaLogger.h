//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// GretaLogger.h: interface for the GretaLogger class.
//
//////////////////////////////////////////////////////////////////////


#ifndef GRETALOGGER_H
#define GRETALOGGER_H

#include <string>

class GretaLogger {

public:
	GretaLogger(std::string project, std::string message, std::string level);
	virtual ~GretaLogger();

	std::string getProject();
	std::string getMessage();
	std::string getLevel();


private:

	std::string project;
	std::string message;
	std::string level;

}; // class GretaLogger


#endif

