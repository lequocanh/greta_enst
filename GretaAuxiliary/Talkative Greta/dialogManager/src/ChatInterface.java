
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import demo.*;




/**
 * Class to construct the chat interface that enables the user to dialog with Greta by text message
 * 
 * Class contains the main method to execute the program
 * @author Magalie 
 */

public class ChatInterface extends JFrame{

	/* Set of demo that the user can trigger (scripted response) */
	public static DataBaseDemo _dataBaseDemo;
	
	
	JPanel _panelP;
	JEditorPane _gretaMessage;
	JButton _boutonOk;
	JTextArea _userMessage;
	
	
	DialogueManager _dialogueManager;

	public ChatInterface(){
		super();
		this.build();
		_dataBaseDemo=new DataBaseDemo();
		
	}
	
	/**
	 * Method that construct the interface 
	 * @author Magalie
	 */
	private void build(){
		this.setTitle("Talk to Greta"); 
		this.setSize(600,900); 
		this.setLocationRelativeTo(null); 
		this.setResizable(false); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		this.setLayout(new BorderLayout(10,10));
		
		Border compound = BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED), BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		Font f = new Font("Times new roman", Font.BOLD, 20);
		
		this._panelP = new JPanel();
		this._panelP.setLayout(new GridLayout(2,1,10,10));
		
		
		/* Panel for the Greta's message */
		JPanel panelGreta=new JPanel();
		panelGreta.setLayout(new BorderLayout());
		panelGreta.setBorder(compound);
		JLabel labelMessageGreta = new JLabel("Greta's message");
		labelMessageGreta.setFont(f);
		panelGreta.add(labelMessageGreta, BorderLayout.NORTH);
		this._gretaMessage=new JTextPane();
		this._gretaMessage.setText("Greta: ");
		this._gretaMessage.setBackground(this.getBackground());
		this._gretaMessage.setEnabled(false);
		this._gretaMessage.setDisabledTextColor(Color.black);
		panelGreta.add(this._gretaMessage, BorderLayout.CENTER);
		this._panelP.add(panelGreta);
		
		/* Panel for the user's message */
		JPanel panelUser=new JPanel();
		panelUser.setLayout(new GridLayout(3,1,10,10));
		panelUser.setBorder(compound);
		JLabel labelMessageUser = new JLabel("Your message");
		labelMessageUser.setFont(f);
		panelUser.add(labelMessageUser);
		this._userMessage=new JTextArea();
		this._userMessage=new JTextArea();
		this._boutonOk= new JButton("Send the message to Greta");
		
		this._boutonOk.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String messageUser=_userMessage.getText();
				_dialogueManager.sendTextIn(messageUser);
				
				_userMessage.setText("");
			}
			
		});
		panelUser.add(this._userMessage);
		panelUser.add(this._boutonOk);
		this._panelP.add(panelUser);
		
		this.add(this._panelP, BorderLayout.CENTER);
		this.add(new JLabel(" "), BorderLayout.WEST);
		this.add(new JLabel(" "), BorderLayout.EAST);
		this.add(new JLabel(" "), BorderLayout.SOUTH);
		this.add(new JLabel(" "), BorderLayout.NORTH);
		
	}
	
	/**
	 * Method that set the dialog manager
	 * 
	 * @param d DialogueManager
	 */
	public void setDialogueManager(DialogueManager d){
		this._dialogueManager=d;
	}
	
	
	/**
	 * Method that display on the chat interface the text message said by Greta 
	 * @param m String message said by Greta
	 */
	public void newGretaMessageToDispay(String m){
		this._gretaMessage.setText("Greta: "+m);
	}
	
	/**
	 * Method that executes the program:
	 * - launch the chat interface
	 * - start the connection to psyclone
	 * - start the dialog manager
	 * 
	 * @param argc
	 */
	public static void main(String argc[]){
		
		ChatInterface cI=new ChatInterface();
		cI.setVisible(true);
		
		Connexion conx=new Connexion(cI);
		conx.run();
		
		DialogueManager dial=new DialogueManager(conx);
		cI.setDialogueManager(dial);
		conx.setDialogueManager(dial);
	}
	

	
}
