#pragma once

#include "GestureEngine.h"

#include "NaoRobot.h"

using namespace GestureSpace;

class NaoGesture
{
public:
	NaoGesture(void);
	~NaoGesture(void);

	void LoadFile();   //Load a txt file in the gesture repertory 

	void PrintScreen();// Print header information of txt file on the screen 

	void PrintScreenKeyFrames(GestureSpace::GesturePhaseVector *phasesVector); // Print information of keyframes from txt file on the screen

	vector<float> LeftArm(GesturePhase *phase, vector<float> JointValues, float *lHand); 

	vector<float> RightArm(GesturePhase *phase, vector<float> JointValues, float *rHand);

	void Converter(GesturePhase *phase, vector<float> JointValues, SideType side); // Calculate absolute angle value for joints of robot corresponding to a phase of gesture

	void Interpret();

	void Print_angles();

public:
	GestureSpace::Gesture *gesture; // gesture has two phases: phases and phasesAssym for right and left side correspondingly when the gesture is assymetric
	GestureSpace::GesturePhaseVector *phasesVector; // = &gesture->phases

	NaoRobot my_robot;
};
