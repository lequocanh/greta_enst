#include "PlayerOgreHead.h"

PlayerOgreHead::PlayerOgreHead(Ogre::SceneManager *sceneManager, Ogre::SceneNode *parent, Ogre::String name,Ogre::String faceMeshFileName, Ogre::String hairMeshFileName):
headNode(0),headPartsNode(0),hairNode(0),hair(0),faceNode(0),face(0),l_EyeNode(0),l_eye(0),r_EyeNode(0),r_eye(0),skull(0){
	headNode = parent->createChildSceneNode(name+"_headNode");
	rotationNode = headNode->createChildSceneNode(name+"_rotNode");
	headPartsNode = rotationNode->createChildSceneNode(name+"_headPartsNode");

	//instanciation the face
	showFace = true;
	faceNode = headPartsNode->createChildSceneNode(name+"_faceNode");
	faceMeshFile = faceMeshFileName;
	faceEntityName = name+"_face";
	//the mesh of the face is loaded later

	//instanciation eyes
	showEyes = true;
	//	left
	l_EyeNode = headPartsNode->createChildSceneNode(name+"_l_EyeNode");
	l_eye = sceneManager->createEntity(name+"_l_eye", "eye.mesh");
	l_EyeNode->attachObject(l_eye);
	//	right
	r_EyeNode = headPartsNode->createChildSceneNode(name+"_r_EyeNode");
	r_eye = sceneManager->createEntity(name+"_r_eye", "eye.mesh");
	r_EyeNode->attachObject(r_eye);

	//instanciation hair
	showHair = true;
	hairNode = headPartsNode->createChildSceneNode(name+"_hairNode");
	if(hairMeshFileName!=""){
		hair = sceneManager->createEntity(name+"_hair", hairMeshFileName);
		hairNode->attachObject(hair);
	}
	
	//jaw
	jawNode = headPartsNode->createChildSceneNode(name+"_jawNode");
	sup_jaw = sceneManager->createEntity(name+"_sup_jaw", "sup_jaw.mesh");
	glottis = sceneManager->createEntity(name+"_glottis", "glottis.mesh");
	jawNode->attachObject(sup_jaw);
	jawNode->attachObject(glottis);

	inf_jawNode = jawNode->createChildSceneNode(name+"_inf_jawNode");
	inf_jaw = sceneManager->createEntity(name+"_inf_jaw", "inf_jaw.mesh");
	inf_jawNode->attachObject(inf_jaw);

	//create animation of tongue
	std::string tongueName = name+"_tongue";
	std::string Tongue_Animation = "Tongue_Animation";

	Ogre::MeshPtr tongueMesh = Ogre::MeshManager::getSingleton().load("tongue.mesh",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	Ogre::VertexAnimationTrack* track;
	if(!tongueMesh->hasAnimation(Tongue_Animation)){
		Ogre::Animation* anim = tongueMesh->createAnimation(Tongue_Animation,0);
		track = anim->createVertexTrack(0/*it's in shared geomety*/,Ogre::VAT_POSE);
		//update changes
		Ogre::VertexData* vdata = tongueMesh->sharedVertexData;
		Ogre::VertexDeclaration* newdcl = vdata->vertexDeclaration->getAutoOrganisedDeclaration(false, true);
		vdata->reorganiseBuffers(newdcl);
	}
	else{
		//we need to create an Ogre::Animation for each Ogre::Entity using this mesh
		Tongue_Animation += "_"+tongueName;
		track = tongueMesh->createAnimation(Tongue_Animation,0)->createVertexTrack(0/*it's in shared geomety*/,Ogre::VAT_POSE);
	}

	tongueKeyFrame = track->createVertexPoseKeyFrame(0);
	for(int i=0; i<5; i++) //we have 7 poses in this mesh
		tongueKeyFrame->addPoseReference(i, 0.0);
	
	//loading the tongue
	tongue = sceneManager->createEntity(tongueName, "tongue.mesh");
	inf_jawNode->attachObject(tongue);

	tongueAnimState = tongue->getAnimationState(Tongue_Animation);
	tongueAnimState->setTimePosition(0);
	tongueAnimState->setEnabled(true);

	//initialize FAP values
	for(int i=0; i<69; i++)
		FAPValues[i] = 0;

}

void PlayerOgreHead::addAccessory(Ogre::SceneManager *sceneManager,std::string meshName,std::string entityName, double x, double y, double z, double factor){
	Ogre::SceneNode *objectNode = headPartsNode->createChildSceneNode(Ogre::Vector3(x,y,z));
	objectNode->attachObject(sceneManager->createEntity(entityName,meshName));
	objectNode->scale(factor,factor,factor);
	
}

Ogre::Vector3* PlayerOgreHead::getFaceCenter(){
	return &(face->getWorldBoundingBox(true).getCenter());
}
double PlayerOgreHead::getFaceHight(){
	return face->getWorldBoundingBox(true).getSize().y;
}

double PlayerOgreHead::getFaceDeep(){
	return face->getWorldBoundingBox(true).getSize().z;
}

void PlayerOgreHead::updateSkullLink(){
	if(skull!=0){
		//set the orientation from the skull
		rotationNode->setOrientation(skull->_getDerivedOrientation());
		//set the position from the skull
		rotationNode->setPosition(skull->_getDerivedPosition());
	}
}


void PlayerOgreHead::SwitchEyes(){
	showEyes = !showEyes;
	if(r_eye!=0) r_eye->setVisible(showEyes);
	if(l_eye!=0) l_eye->setVisible(showEyes);
	if(sup_jaw!=0) sup_jaw->setVisible(showEyes);
	if(inf_jaw!=0) inf_jaw->setVisible(showEyes);
	if(tongue!=0) tongue->setVisible(showEyes);
	if(glottis!=0) glottis->setVisible(showEyes);
}
void PlayerOgreHead::SwitchHair(){
	showHair = !showHair;
	if(hair!=0) hair->setVisible(showHair);
}
void PlayerOgreHead::SwitchSkin(){
	showFace = !showFace;
	if(face!=0) face->setVisible(showFace);
}

//////////////////////////////////////////////////
// may be modify the following functions
// they are based on the old code of greta player
void PlayerOgreHead::createFAPPoses(std::string fdpFileName, std::string indicesFileName,Ogre::SceneManager* sceneManager){
//*
	
	
	Ogre::MeshPtr faceMesh = Ogre::MeshManager::getSingleton().load(faceMeshFile,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	unsigned short target = faceMesh->getSubMesh(0)->useSharedVertices ? 0 : 1; //the face geometry must be the first submesh !
	// we create the animation
	Ogre::VertexAnimationTrack* track;
	int poseIndex = 0;
	std::string fap_animation = "FAP_Animation";
	if(!faceMesh->hasAnimation(fap_animation)){
		//get the face geometry from the face entity
		std::vector<Ogre::Vector3> vertices;
		getVertices(&vertices);

		//initialize groups of vertices
		std::vector<std::vector<int>> groups;
		readIndices(&groups,indicesFileName);

		//get the FDPs of the face
		std::vector<std::vector<Ogre::Vector3>> FDPs;
		readFDPs(&FDPs, &vertices, fdpFileName);
		double MNS_ = JAWSVERTICALMOVEMENT*jawNode->getScale().x;
		//initialize FAP units
		double IRISD = fabs(FDPs[3][1].y - FDPs[3][3].y) / 1024.0; // Iris Diameter
		double ES    = fabs(((FDPs[3][1].x - FDPs[3][2].x) + (FDPs[3][3].x - FDPs[3][4].x)) / 2.0) / 1024.0; // Eye Separation
		double ENS   = fabs((FDPs[3][1].y + FDPs[3][3].y + FDPs[3][2].y + FDPs[3][4].y ) / 4.0 - FDPs[9][15].y) / 1024; // Eye - Nose Separation
		double MNS   = fabs(FDPs[9][15].y - FDPs[2][2].y) / 1024.0; // Mouth - Nose Separation
		double MW    = fabs(FDPs[8][3].x - FDPs[8][4].x) / 1024.0; // Mouth Width 
		double MH	 = fabs(FDPs[8][1].y - FDPs[8][2].y) / 1024.0; // Mouth Height
		double LCB   = fabs(FDPs[8][3].z - FDPs[5][1].z) / 1024.0;
		double RCB   = fabs(FDPs[8][4].z - FDPs[5][2].z) / 1024.0;
		double LLP   = fabs(FDPs[8][2].z - FDPs[2][10].z) / 1024.0;
		double ULP   = fabs(FDPs[8][1].z - FDPs[9][15].z) / 1024.0;
		double FI    = /*fabs((FDPs[4][1].z + FDPs[4][2].z) / 2.0 - FDPs[6][1].z) / 1024.0;
		double FI_   = */fabs((FDPs[4][1].z + FDPs[4][2].z - FDPs[4][4].z - FDPs[4][3].z) / 2.0) / 1024.0 *0.9;
		//double JH    = fabs(FDPs[8][2].y - FDPs[2][1].y) / 1024.0;
		double UL    = fabs(FDPs[8][1].y - FDPs[2][2].y) / 1024.0; //Upper Lip
		double LL    = fabs(FDPs[8][2].y - FDPs[2][3].y) / 1024.0; //Lower Lip
	
		// creation of poses :
		std::vector<bool> verticesInPose;
		verticesInPose.resize(vertices.size());
		
		numberOfPoses = 46; //this values can be modified
		poses = (Ogre::Pose**) malloc(sizeof(Ogre::Pose*)*numberOfPoses);
		indexOfPose = (int*) malloc(sizeof(int)*numberOfPoses);
		FAPOfPoses = (int*) malloc(sizeof(int)*numberOfPoses);
printf("create poses for facial animation");

//FAP 1 : viseme (not used)
//FAP 2 : expression (not used)
//FAP 3 : Vertical jaw displacement (MNS)
		poses[0] = faceMesh->createPose(target,"FAP_3");
		FAPOfPoses[0] = 3;
		indexOfPose[0] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 20,   1, 2, 39, 40, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 67, 68);
		createPose(poses[0],&vertices,&verticesInPose, &(FDPs[2][1]), Y,   1.0/16.0, 1, 0.25,   3.5,   -MNS_);
//FAP 4 : Vertical top middle inner lip displacement (MNS)
		poses[1] = faceMesh->createPose(target,"FAP_4");
		FAPOfPoses[1] = 4;
		indexOfPose[1] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,14,    33, 34, 35, 36, 37, 38, 45, 46, 47, 48, 49, 50, 51, 52);
		createPose(poses[1],&vertices,&verticesInPose, &(FDPs[2][2]), Y,  1.0/3.0, 0.1, 1,   1,   -0.72*MNS);
//FAP 5 : Vertical bottom middle inner lip displacement (Greta use MH instade of MNS)
		poses[2] = faceMesh->createPose(target,"FAP_5");
		FAPOfPoses[2] = 5;
		indexOfPose[2] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,10,    41, 42, 43, 44, 55, 56, 57, 58, 59, 60);
		createPose(poses[2],&vertices,&verticesInPose, &(FDPs[2][3]), Y,   1, 4, 1,   1.5,   0.561*MH);
//FAP 6 : Horizontal displacement of left inner lip corner (Greta use MNS instade of MW)
		FAP_6 = faceMesh->createPose(target,"FAP_6");
		index_Pose_FAP_6 = poseIndex++;
		clearAndAdd(&verticesInPose,&groups, 13,    1, 33, 35, 37, 39, 41, 43, 47, 49, 51, 53, 55, 59);
		createPose(FAP_6,&vertices,&verticesInPose, &(FDPs[2][4]), X,   1, 1, 1,   1,   -0.3*MNS, 2);
//FAP 7 : Horizontal displacement of right inner lip corner (Greta use MNS instade of MW)
		FAP_7 = faceMesh->createPose(target,"FAP_7");
		index_Pose_FAP_7 = poseIndex++;
		clearAndAdd(&verticesInPose,&groups, 13,    2, 34, 36, 38, 40, 42, 44, 48, 50, 52, 54, 56, 60);
		createPose(FAP_7,&vertices,&verticesInPose, &(FDPs[2][5]), X,   1, 1, 1,   1,   0.3*MNS, 2);
//FAP 8 : Vertical displacement of midpoint between left corner and middle of top inner lip (MNS)
		poses[3] = faceMesh->createPose(target,"FAP_8");
		FAPOfPoses[3] = 8;
		indexOfPose[3] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,14,    33, 34, 35, 36, 37, 38, 45, 46, 47, 48, 49, 50, 51, 52);
		createPose(poses[3],&vertices,&verticesInPose, &(FDPs[2][6]), Y,   0.1, 0.5, 0.2,   1,   -0.3*MNS, 4);
//FAP 9 : Vertical displacement of midpoint between right corner and middle of top inner lip (MNS)
		poses[4] = faceMesh->createPose(target,"FAP_9");
		FAPOfPoses[4] = 9;
		indexOfPose[4] = poseIndex++;
		createPose(poses[4],&vertices,&verticesInPose, &(FDPs[2][7]), Y,   0.1, 0.5, 0.2,   1,   -0.3*MNS, 4);
//FAP 10 : Vertical displacement of midpoint between left corner and middle of bottom inner lip (MNS)
		poses[5] = faceMesh->createPose(target,"FAP_10");
		FAPOfPoses[5] = 10;
		indexOfPose[5] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,14,    39, 40, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 57, 58);
		createPose(poses[5],&vertices,&verticesInPose, &(FDPs[2][9]), Y,   1, 2, 1.0/1.1,   1.5,   0.3*MNS);
printf(".");
//FAP 11 : Vertical displacement of midpoint between right corner and middle of bottom inner lip (MNS)
		poses[6] = faceMesh->createPose(target,"FAP_11");
		FAPOfPoses[6] = 11;
		indexOfPose[6] = poseIndex++;
		createPose(poses[6],&vertices,&verticesInPose, &(FDPs[2][8]), Y,   1, 2, 1.0/1.1,   1.5,   0.3*MNS);
//FAP 12 : Vertical displacement of left inner lip corner (MNS)
		FAP_12_0 = faceMesh->createPose(target,"FAP_12_0");
		index_Pose_FAP_12_0 = poseIndex++;
		clearAndAdd(&verticesInPose,&groups, 13,    1, 33, 35, 37, 39, 41, 43, 47, 49, 51, 53, 55, 59);
		createPose(FAP_12_0,&vertices,&verticesInPose, &(FDPs[2][4]), Y,   1, 1, 1,   1,   0.3*MNS, 2);
		FAP_12_1 = faceMesh->createPose(target,"FAP_12_1");
		index_Pose_FAP_12_1 = poseIndex++;
		createPose(FAP_12_1,&vertices,&verticesInPose, &(FDPs[2][4]), Z,   1, 1, 1,   1,   -0.09*LCB, 2);
//FAP 13 : Vertical displacement of right inner lip corner (MNS)
		FAP_13_0 = faceMesh->createPose(target,"FAP_13_0");
		index_Pose_FAP_13_0 = poseIndex++;
		clearAndAdd(&verticesInPose,&groups, 13,    2, 34, 36, 38, 40, 42, 44, 48, 50, 52, 54, 56, 60);
		createPose(FAP_13_0,&vertices,&verticesInPose, &(FDPs[2][5]), Y,   1, 1, 1,   1,   0.3*MNS, 2);
		FAP_13_1 = faceMesh->createPose(target,"FAP_13_1");
		index_Pose_FAP_13_1 = poseIndex++;
		createPose(FAP_13_1,&vertices,&verticesInPose, &(FDPs[2][5]), Z,   1, 1, 1,   1,   -0.09*LCB, 2);
//FAP 14 : Depth displacement of jaw (MNS)
		poses[7] = faceMesh->createPose(target,"FAP_14");
		FAPOfPoses[7] = 14;
		indexOfPose[7] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,20,    1, 2, 39, 40, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 67, 68);
		createPose(poses[7],&vertices,&verticesInPose, &(FDPs[2][1]), Z,   1.0/16.0, 1, 0.25,   3.5,   MNS_);
//FAP 15 : Side to side displacement of jaw (MNS)
		poses[8] = faceMesh->createPose(target,"FAP_15");
		FAPOfPoses[8] = 15;
		indexOfPose[8] = poseIndex++;
		createPose(poses[8],&vertices,&verticesInPose, &(FDPs[2][1]), X,   1.0/16.0, 1, 0.25,   3.5,   -MNS_);//-MW);
//FAP 16 : Depth displacement of bottom middle lip (MNS)
		poses[9] = faceMesh->createPose(target,"FAP_16");
		FAPOfPoses[9] = 16;
		indexOfPose[9] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,10,     41, 42, 43, 44, 55, 56, 57, 58, 59, 60);
		createPose(poses[9],&vertices,&verticesInPose, &(FDPs[2][3]), Z,   1, 4, 1,   1.5,   3.523*LLP);
		add(&verticesInPose,&(groups[39]));add(&verticesInPose,&(groups[40]));add(&verticesInPose,&(groups[51]));
		add(&verticesInPose,&(groups[52]));add(&verticesInPose,&(groups[53]));add(&verticesInPose,&(groups[54]));
		createPose(poses[9],&vertices,&verticesInPose, &(FDPs[8][2]), Z,   0.2, 3, 1,   1.5,  1.761*LLP);
//FAP 17 : Depth displacement of top middle lip (MNS)
		poses[10] = faceMesh->createPose(target,"FAP_17");
		FAPOfPoses[10] = 17;
		indexOfPose[10] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,14,     33, 34, 35, 36, 37, 38, 45, 46, 47, 48, 49, 50, 51, 52);
		createPose(poses[10],&vertices,&verticesInPose, &(FDPs[2][2]), Z,   1.0/3.0, 0.1, 1,   1,   ULP);//0.6*MNS);//1.38*ULP);
		FAP_17_ = faceMesh->createPose(target,"FAP_17_");
		index_Pose_FAP_17_ = poseIndex++;
		add(&verticesInPose,&(groups[3]));add(&verticesInPose,&(groups[4]));
		createPose(FAP_17_,&vertices,&verticesInPose, &(FDPs[8][1]), Z,   1, 7, 1,   2.5,  0.2*MNS);//0.46*ULP);
//FAP 18 : Upward and compressing movement of the chin (MNS)
		poses[11] = faceMesh->createPose(target,"FAP_18");
		FAPOfPoses[11] = 18;
		indexOfPose[11] = poseIndex++;
		clearAndAdd(&verticesInPose,&groups,18,     39, 40, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 67, 68);
		createPose(poses[11],&vertices,&verticesInPose, &(FDPs[2][10]), Y,   1.0/12.0, 1, 1,   1,   MNS);
		createPose(poses[11],&vertices,&verticesInPose, &(FDPs[2][10]), Z,   1.0/12.0, 1, 1,   1,   1.057*LLP);
//FAP 19 : Vertical displacement of top left eyelid (IRISD)
		poses[12] = faceMesh->createPose(target,"FAP_19");
		FAPOfPoses[12] = 19;
		indexOfPose[12] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,    9, 11);
		createPose2(poses[12],&vertices,&verticesInPose, &(FDPs[3][1]), Y,   1.0, 1.0, 1.0,  5.3,   -IRISD);
		clearAndAdd(&verticesInPose, &groups, 1, 11);
		scalepose(poses[12],&verticesInPose,Y,0.4);
		//createPose2(poses[12],&vertices,&verticesInPose, &(FDPs[3][1]), Y,   1, 1, 1,  5.3,   -0.4*IRISD);
//FAP 20 : Vertical displacement of top right eyelid (IRISD)
		poses[13] = faceMesh->createPose(target,"FAP_20");
		FAPOfPoses[13] = 20;
		indexOfPose[13] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,    10,12);
		createPose2(poses[13],&vertices,&verticesInPose, &(FDPs[3][2]), Y,   1.0, 1.0, 1.0,  5.3,   -IRISD);
		clearAndAdd(&verticesInPose, &groups, 1, 12);
		scalepose(poses[13],&verticesInPose,Y,0.4);
		//createPose2(poses[13],&vertices,&verticesInPose, &(FDPs[3][2]), Y,   1, 1, 1,  5.3,   -0.4*IRISD);
printf(".");
//FAP 21 : Vertical displacement of bottom left eyelid (IRISD)
		poses[14] = faceMesh->createPose(target,"FAP_21");
		FAPOfPoses[14] = 21;
		indexOfPose[14] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,     5, 7);
		createPose2(poses[14],&vertices,&verticesInPose, &(FDPs[3][3]), Y,   1.0, 1.0, 1.0,  5.3,   IRISD);
		clearAndAdd(&verticesInPose, &groups, 1, 7);
		scalepose(poses[14],&verticesInPose,Y,0.4);
		//createPose2(poses[14],&vertices,&verticesInPose, &(FDPs[3][3]), Y,   1, 1, 1,  5.3,   0.4*IRISD);
//FAP 22 : Vertical displacement of bottom right eyelid (IRISD)
		poses[15] = faceMesh->createPose(target,"FAP_22");
		FAPOfPoses[15] = 22;
		indexOfPose[15] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,    6, 8);
		createPose2(poses[15],&vertices,&verticesInPose, &(FDPs[3][4]), Y,   1.0, 1.0, 1.0,  5.3,   IRISD);
		clearAndAdd(&verticesInPose, &groups, 1,    8);
		scalepose(poses[15],&verticesInPose,Y,0.4);
		//createPose2(poses[15],&vertices,&verticesInPose, &(FDPs[3][4]), Y,   1, 1, 1,  5.3,   0.4*IRISD);
//FAP 23 -> 30 : not here (eyes)
printf(".");
//FAP 31 : Vertical displacement of left inner eyebrow (ENS)
		poses[16] = faceMesh->createPose(target,"FAP_31");
		FAPOfPoses[16] = 31;
		indexOfPose[16] = poseIndex++;
			clearAndAdd(&verticesInPose, &groups, 8,      13, 15, 19, 21, 27, 29, 31,11);
		createPose(poses[16],&vertices,&verticesInPose, &(FDPs[4][1]), Y,   1.7, 0.5, 1,   2.6,  ENS, 2);
		FAP_31_ = faceMesh->createPose(target,"FAP_31_");
		index_Pose_FAP_31_ = poseIndex++;
		//add(&verticesInPose,&(groups[11]));
		createPose(FAP_31_,&vertices,&verticesInPose, &(FDPs[4][1]), Z,   1.7, 0.5, 1, 2.6, 0.1*FI, 2);
		clearAndAdd(&verticesInPose, &groups, 1,   11);
		scalepose(poses[16],&verticesInPose,Y,0.5);
		//createPose(poses[16],&vertices,&verticesInPose, &(FDPs[4][1]), Y,   1.7, 0.5, 1,   2.6,  0.5*ENS, 2);
//FAP 32 : Vertical displacement of right inner eyebrow (ENS)
		poses[17] = faceMesh->createPose(target,"FAP_32");
		FAPOfPoses[17] = 32;
		indexOfPose[17] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 8,      14, 16, 20, 22, 28, 30, 32, 12);
		createPose(poses[17],&vertices,&verticesInPose, &(FDPs[4][2]), Y,   1.7, 0.5, 1,   2.6,  ENS, 2);
		FAP_32_ = faceMesh->createPose(target,"FAP_32_");
		index_Pose_FAP_32_ = poseIndex++;
		//add(&verticesInPose,&(groups[12]));
		createPose(FAP_32_,&vertices,&verticesInPose, &(FDPs[4][2]), Z,   1.7, 0.5, 1, 2.6, 0.1*FI, 2);
		clearAndAdd(&verticesInPose, &groups, 1, 12);
		scalepose(poses[17],&verticesInPose,Y,0.5);
		//createPose(poses[17],&vertices,&verticesInPose, &(FDPs[4][2]), Y,   1.7, 0.5, 1,   2.6,  0.5*ENS, 2);
//FAP 33 : Vertical displacement of left middle eyebrow (ENS)
		poses[18] = faceMesh->createPose(target,"FAP_33");
		FAPOfPoses[18] = 33;
		indexOfPose[18] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 11,      13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 11);
		createPose(poses[18],&vertices,&verticesInPose, &(FDPs[4][3]), Y,   2, 0.5, 1,   3.5,  ENS, 2);
		FAP_33_ = faceMesh->createPose(target,"FAP_33_");
		index_Pose_FAP_33_ = poseIndex++;
		//add(&verticesInPose,&(groups[11]));
		createPose(FAP_33_,&vertices,&verticesInPose, &(FDPs[4][3]), Z,   2, 0.5, 1,   3.5,  0.1*FI, 2);
		clearAndAdd(&verticesInPose, &groups, 1, 11);
		scalepose(poses[18],&verticesInPose,Y,0.6);
		//createPose(poses[18],&vertices,&verticesInPose, &(FDPs[4][3]), Y,   2, 0.5, 1,   3.5,  0.36*ENS, 2);
//FAP 34 : Vertical displacement of right middle eyebrow (ENS)
		poses[19] = faceMesh->createPose(target,"FAP_34");
		FAPOfPoses[19] = 34;
		indexOfPose[19] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 11,    14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 12);
		createPose(poses[19],&vertices,&verticesInPose, &(FDPs[4][4]), Y,   2.0, 0.5, 1.0,   3.5,  ENS, 2);
		FAP_34_ = faceMesh->createPose(target,"FAP_34_");
		index_Pose_FAP_34_ = poseIndex++;
		//add(&verticesInPose,&(groups[12]));
		createPose(FAP_34_,&vertices,&verticesInPose, &(FDPs[4][4]), Z,   2.0, 0.5, 1.0,   3.5,  0.1*FI, 2);
		clearAndAdd(&verticesInPose, &groups, 1, 12);
		scalepose(poses[19],&verticesInPose,Y,0.6);
		//createPose(poses[19],&vertices,&verticesInPose, &(FDPs[4][4]), Y,   2.0, 0.5, 1.0,   3.5,  0.6*ENS, 2);
//FAP 35 : Vertical displacement of left outer eyebrow (ENS)
		poses[20] = faceMesh->createPose(target,"FAP_35");
		FAPOfPoses[20] = 35;
		indexOfPose[20] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 8,   15, 17, 21, 23, 25, 27, 31, 11);
		createPose(poses[20],&vertices,&verticesInPose, &(FDPs[4][5]), Y,   1, 1, 1,   3,  ENS, 2);
		FAP_35_ = faceMesh->createPose(target,"FAP_35_");
		index_Pose_FAP_35_ = poseIndex++;
		//add(&verticesInPose,&(groups[11]));
		createPose(FAP_35_,&vertices,&verticesInPose, &(FDPs[4][5]), Z,   1, 1, 1,   3,  0.3*FI, 2);
		clearAndAdd(&verticesInPose, &groups, 1, 11);
		scalepose(poses[20],&verticesInPose,Y,0.3);
		//createPose(poses[20],&vertices,&verticesInPose, &(FDPs[4][5]), Y,   1, 1, 1,   3,  0.3*ENS, 2);
//FAP 36 : Vertical displacement of right outer eyebrow (ENS)
		poses[21] = faceMesh->createPose(target,"FAP_36");
		FAPOfPoses[21] = 36;
		indexOfPose[21] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 8,   16, 18, 22, 24, 26, 28, 32, 12);
		createPose(poses[21],&vertices,&verticesInPose, &(FDPs[4][6]), Y,   1, 1, 1,   3,  ENS, 2);
		FAP_36_ = faceMesh->createPose(target,"FAP_36_");
		index_Pose_FAP_36_ = poseIndex++;
		//add(&verticesInPose,&(groups[12]));
		createPose(FAP_36_,&vertices,&verticesInPose, &(FDPs[4][6]), Z,   1, 1, 1,   3,  0.3*FI, 2);
		clearAndAdd(&verticesInPose, &groups, 1, 12);
		scalepose(poses[21],&verticesInPose,Y,0.3);
		//createPose(poses[21],&vertices,&verticesInPose, &(FDPs[4][6]), Y,   1, 1, 1,   3,  0.3*ENS, 2);
//FAP 37 : Horizontal displacement of left eyebrow (Greta use ENS instade of ES)
		poses[22] = faceMesh->createPose(target,"FAP_37");
		FAPOfPoses[22] = 37;
		indexOfPose[22] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,   19, 29);
		createPose(poses[22],&vertices,&verticesInPose, &(FDPs[4][1]), Z,   10, 2.0/3.0, 1, 2.3, 0.4*ENS, 1, PI_/2.0);
		//createPose(poses[22],&vertices,&verticesInPose, &(FDPs[4][2]), X,   1, 0.5, 1, 2.5, 0.8*ENS, 4);
		//FAP_31x37 = faceMesh->createPose(target,"FAP_31x37");
		//index_Pose_FAP_31x37 = poseIndex++;
		////createPose(FAP_31x37,&vertices,&verticesInPose, &(FDPs[4][1]), Y,   10, 2/3, 1,  2.3,  0.6*ENS*ENS, 1, PI_/2);
		clearAndAdd(&verticesInPose, &groups, 8,    11, 13, 15, 19, 21, 27, 29, 31);
		createPose(poses[22],&vertices,&verticesInPose, &(FDPs[4][1]), Z,   1.7, 0.5, 1, 2.6, 0.119*FI, 2);
		createPose(poses[22],&vertices,&verticesInPose, &(FDPs[4][1]), X,   1.7, 0.5, 1, 2.6, -ENS, 4);
		clearAndAdd(&verticesInPose, &groups, 11,   11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31);
		createPose(poses[22],&vertices,&verticesInPose, &(FDPs[4][3]), X,   2, 0.5, 1,   3.5,  -0.576*ES, 2); //here it' ES ??
		//clearAndAdd(&verticesInPose, &groups, 2,   20, 30);
		//createPose(poses[22],&vertices,&verticesInPose, &(FDPs[4][2]), Z,   10, 2/3, 1, 2.3, 0.4*ENS, 1, PI_/2);
//FAP 38 : Horizontal displacement of right eyebrow (Greta use ENS instade of ES)
		poses[23] = faceMesh->createPose(target,"FAP_38");
		FAPOfPoses[23] = 38;
		indexOfPose[23] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,   20, 30);
		createPose(poses[23],&vertices,&verticesInPose, &(FDPs[4][2]), Z,   10, 20/3.0, 1, 2.3, 0.4*ENS, 1, PI_/2.0);
		//createPose(poses[23],&vertices,&verticesInPose, &(FDPs[4][1]), X,   1, 0.5, 1, 2.5, -0.8*ENS, 4);
		//FAP_32x38 = faceMesh->createPose(target,"FAP_32x38");
		//index_Pose_FAP_32x38 = poseIndex++;
		//createPose(FAP_32x38,&vertices,&verticesInPose, &(FDPs[4][2]), Y,   10, 2/3, 1,  2.3,  0.6*ENS*ENS, 1, PI_/2);
		clearAndAdd(&verticesInPose, &groups, 8,      12, 14, 16, 20, 22, 28, 30, 32);
		createPose(poses[23],&vertices,&verticesInPose, &(FDPs[4][2]), Z,   1.7, 0.5, 1, 2.6, 0.119*FI, 2);
		createPose(poses[23],&vertices,&verticesInPose, &(FDPs[4][2]), X,   1.7, 0.5, 1, 2.6, ENS, 4);
		clearAndAdd(&verticesInPose, &groups, 11,   12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32);
		createPose(poses[23],&vertices,&verticesInPose, &(FDPs[4][4]), X,   2, 0.5, 1,   3.5,  0.576*ES, 2);//here it' ES ??
		//clearAndAdd(&verticesInPose, &groups, 2,   19, 29);
		//createPose(poses[23],&vertices,&verticesInPose, &(FDPs[4][1]), Z,   10, 2/3, 1, 2.3, 0.4*ENS, 1, PI_/2);
//FAP 39 : Horizontal displacement of left cheeck (Greta use ENS instade of ES)
		poses[24] = faceMesh->createPose(target,"FAP_39");
		FAPOfPoses[24] = 39;
		indexOfPose[24] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 12,   1, 35, 37, 39, 41, 47, 49, 51, 53, 55, 59, 67);
		createPose(poses[24],&vertices,&verticesInPose, &(FDPs[5][1]), X,   1, 0.5, 1, 3, ENS, 2);
		FAP_39_ = faceMesh->createPose(target,"FAP_39_");
		index_Pose_FAP_39_ = poseIndex++;
		createPose(FAP_39_,&vertices,&verticesInPose, &(FDPs[5][1]), Z,   1, 0.5, 1, 3, 0.5*LCB, 2);
//FAP 40 : Horizontal displacement of right cheeck (Greta use ENS instade of ES)
		poses[25] = faceMesh->createPose(target,"FAP_40");
		FAPOfPoses[25] = 40;
		indexOfPose[25] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 12,    2, 36, 38, 40, 42, 48, 50, 52, 54, 56, 60, 68);
		createPose(poses[25],&vertices,&verticesInPose, &(FDPs[5][2]), X,   1, 0.5, 1, 3, -ENS, 2);
		FAP_40_ = faceMesh->createPose(target,"FAP_40_");
		index_Pose_FAP_40_ = poseIndex++;
		createPose(FAP_40_,&vertices,&verticesInPose, &(FDPs[5][2]), Z,   1, 0.5, 1, 3, 0.5*RCB, 2);
printf(".");
//FAP 41 : Vertical displacement of left cheek (ENS)
		poses[26] = faceMesh->createPose(target,"FAP_41");
		FAPOfPoses[26] = 41;
		indexOfPose[26] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 5,   1, 3, 7, 49, 51);
		createPose(poses[26],&vertices,&verticesInPose, &(FDPs[5][3]), Y,   1, 1, 1, 3, ENS, 2);
		FAP_41_ = faceMesh->createPose(target,"FAP_41_");
		index_Pose_FAP_41_ = poseIndex++;
		createPose(FAP_41_,&vertices,&verticesInPose, &(FDPs[5][3]), Z,   1, 1, 1, 3, -0.1*LCB, 2);
//FAP 42 : Vertical displacement of right cheek (ENS)
		poses[27] = faceMesh->createPose(target,"FAP_42");
		FAPOfPoses[27] = 42;
		indexOfPose[27] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 5,   2, 4, 8, 50, 52);
		createPose(poses[27],&vertices,&verticesInPose, &(FDPs[5][4]), Y,   1, 1, 1, 3, ENS, 2);
		FAP_42_ = faceMesh->createPose(target,"FAP_42_");
		index_Pose_FAP_42_ = poseIndex++;
		createPose(FAP_42_,&vertices,&verticesInPose, &(FDPs[5][4]), Z,   1, 1, 1, 3, -0.1*RCB, 2);
//FAP 43 -> 71 : not here (tongue)
//FAP 48 49 50 : not here (head rotations)
printf(".");
//FAP 51 : Vertical top middle outer lip displacement (Greta use UL instade of MNS)
		poses[28] = faceMesh->createPose(target,"FAP_51");
		FAPOfPoses[28] = 51;
		indexOfPose[28] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 16,    3, 4, 33, 34, 35, 36, 37, 38, 45, 46, 47, 48, 49, 50, 51, 52);
		createPose(poses[28],&vertices,&verticesInPose, &(FDPs[8][1]), Y,   1, 7, 1,   2.5,  -1.8*UL);//-0.6*UL);
//FAP 52 : Vertical bottom middle outer lip displacement (Greta use LL instade of MNS)
		poses[29] = faceMesh->createPose(target,"FAP_52");
		FAPOfPoses[29] = 52;
		indexOfPose[29] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 16,    39, 40, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60);
		createPose(poses[29],&vertices,&verticesInPose, &(FDPs[8][2]), Y,   0.2, 3, 1,   1.5,  2.1*LL);//0.7*LL);
//FAP 53 : Horizontal displacement of left outer lip corner (MW)
		poses[30] = faceMesh->createPose(target,"FAP_53");
		FAPOfPoses[30] = 53;
		indexOfPose[30] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 15,   1, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][3]), X,   1, 1, 1,   3,  MW, 2);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][3]), Z,   1, 1, 1,   3,  -3.0*LCB, 2);
		clearAndAdd(&verticesInPose, &groups, 16,   3, 4, 33, 34, 35, 36, 37, 38, 45, 46, 47, 48, 49, 50, 51, 52);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][1]), Y,   1, 7, 1,   2.5,  -0.06*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   33, 35, 37, 45, 47, 49, 51);//10,   33, 34, 35, 36, 37, 38, 45, 47, 49, 51);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][5]), Y,   1, 1.5, 1,   1.5,  -0.03*MW);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][5]), X,   1, 1.5, 1,   1.5,  0.08*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   34, 36, 38, 46, 48, 50, 52);//10,   33, 34, 35, 36, 37, 38, 46, 48, 50, 52);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][6]), Y,   1, 1.5, 1,   1.5,  -0.03*MW);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][6]), X,   1, 1.5, 1,   1.5,  -0.08*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   39, 41, 43, 53, 55, 57, 59);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][7]), Y,   1, 1.5, 1,   1.5,  0.03*MW);//0.04*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   40, 42, 44, 54, 56, 58, 60);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][8]), Y,   1, 1.5, 1,   1.5,  0.03*MW);//0.04*MW);
		clearAndAdd(&verticesInPose, &groups, 14,   39, 40, 41, 42, 43, 44, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60);
		createPose(poses[30],&vertices,&verticesInPose, &(FDPs[8][2]), Y,  1.0/3.0, 2, 1,   1.5,   0.08*MW);
//FAP 54 : Horizontal displacement of right outer lip corner (MW)
		poses[31] = faceMesh->createPose(target,"FAP_54");
		FAPOfPoses[31] = 54;
		indexOfPose[31] = poseIndex++;
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][2]), Y,  1.0/3.0, 2, 1,   1.5,   0.08*MW);
		clearAndAdd(&verticesInPose, &groups, 15,    2, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][4]), X,   1, 1, 1,   3,  -MW, 2);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][4]), Z,   1, 1, 1,   3,  -3.0*RCB, 2);
		clearAndAdd(&verticesInPose, &groups, 16,    3, 4, 33, 34, 35, 36, 37, 38, 45, 46, 47, 48, 49, 50, 51, 52);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][1]), Y,   1, 7, 1,   2.5,  -0.06*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   33, 35, 37, 45, 47, 49, 51);//10,   33, 34, 35, 36, 37, 38, 45, 47, 49, 51);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][5]), Y,   1, 1.5, 1,   1.5,  -0.03*MW);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][5]), X,   1, 1.5, 1,   1.5,  0.08*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   34, 36, 38, 46, 48, 50, 52);//10,   33, 34, 35, 36, 37, 38, 46, 48, 50, 52);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][6]), Y,   1, 1.5, 1,   1.5,  -0.03*MW);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][6]), X,   1, 1.5, 1,   1.5,  -0.08*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   39, 41, 43, 53, 55, 57, 59);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][7]), Y,   1, 1.5, 1,   1.5,  0.03*MW);//0.04*MW);
		clearAndAdd(&verticesInPose, &groups, 7,   40, 42, 44, 54, 56, 58, 60);
		createPose(poses[31],&vertices,&verticesInPose, &(FDPs[8][8]), Y,   1, 1.5, 1,   1.5, 0.03*MW);// 0.04*MW);
//FAP 55 : Vertical displacement of midpoint between left corner and middle of top outer lip (MNS)
		poses[32] = faceMesh->createPose(target,"FAP_55");
		FAPOfPoses[32] = 55;
		indexOfPose[32] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 7,   33, 35, 37, 45, 47, 49, 51);
		clear(&verticesInPose, &groups, 7,   34, 36, 38, 46, 48, 50, 52); //to delete common vertices with FAP_56
		createPose(poses[32],&vertices,&verticesInPose, &(FDPs[8][5]), Y,   1, 1.5, 1,   1.5,  -2.7*UL);//-0.9*UL);//MNS);
		FAP_55_ = faceMesh->createPose(target,"FAP_55_");
		index_Pose_FAP_55_ = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1, 3);
		createPose(FAP_55_,&vertices,&verticesInPose, &(FDPs[9][1]), Y,   1, 1, 1,   1,  -0.5*MNS);
		add(&verticesInPose,&(groups[1]));add(&verticesInPose,&(groups[7]));
		createPose(FAP_55_,&vertices,&verticesInPose, &(FDPs[5][3]), X,   1, 1, 1,   2.2,  0.2*MNS);
		createPose(FAP_55_,&vertices,&verticesInPose, &(FDPs[5][3]), Z,   1, 1, 1,   2.2,  -0.2*MNS);
//FAP 56 : Vertical displacement of midpoint between right corner and middle of top outer lip (MNS)
		poses[33] = faceMesh->createPose(target,"FAP_56");
		FAPOfPoses[33] = 56;
		indexOfPose[33] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 7,   34, 36, 38, 46, 48, 50, 52);
		createPose(poses[33],&vertices,&verticesInPose, &(FDPs[8][6]), Y,   1, 1.5, 1,   1.5,  -2.7*UL);//-0.9*UL);//MNS);
		FAP_56_ = faceMesh->createPose(target,"FAP_56_");
		index_Pose_FAP_56_ = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1, 4);
		createPose(FAP_56_,&vertices,&verticesInPose, &(FDPs[9][2]), Y,   1, 1, 1,   1,  -0.5*MNS);
		add(&verticesInPose,&(groups[2]));add(&verticesInPose,&(groups[8]));
		createPose(FAP_56_,&vertices,&verticesInPose, &(FDPs[5][4]), X,   1, 1, 1,   2.2,  -0.2*MNS);
		createPose(FAP_56_,&vertices,&verticesInPose, &(FDPs[5][4]), Z,   1, 1, 1,   2.2,  -0.2*MNS);
//FAP 57 : Vertical displacement of midpoint between left corner and middle of bottom outer lip (MNS)
		poses[34] = faceMesh->createPose(target,"FAP_57");
		FAPOfPoses[34] = 57;
		indexOfPose[34] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 7,   39, 41, 43, 53, 55, 57, 59);
		createPose(poses[34],&vertices,&verticesInPose, &(FDPs[8][7]), Y,   1, 1.5, 1,   1.5,  0.3*LL);//0.1*LL);
//FAP 58 : Vertical displacement of midpoint between right corner and middle of bottom outer lip (MNS)
		poses[35] = faceMesh->createPose(target,"FAP_58");
		FAPOfPoses[35] = 58;
		indexOfPose[35] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 7,   40, 42, 44, 54, 56, 58, 60);
		createPose(poses[35],&vertices,&verticesInPose, &(FDPs[8][8]), Y,   1, 1.5, 1,   1.5,  0.3*LL);//0.1*LL);
//FAP 59 : Vertical displacement of left outer lip corner (MNS)
		poses[36] = faceMesh->createPose(target,"FAP_59");
		FAPOfPoses[36] = 59;
		indexOfPose[36] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 15,    1, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59);
		createPose(poses[36],&vertices,&verticesInPose, &(FDPs[8][3]), Y,   1, 1, 1,   3,  MNS, 2);
		FAP_59_ = faceMesh->createPose(target,"FAP_59_");
		index_Pose_FAP_59_ = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 3,   1,3,7);
		createPose(FAP_59_,&vertices,&verticesInPose, &(FDPs[5][3]), X,   1, 1, 1,   2.2,  -0.2*MNS);
		createPose(FAP_59_,&vertices,&verticesInPose, &(FDPs[5][3]), Z,   1, 1, 1,   2.2,  0.2*MNS);
//FAP 60 : Vertical displacement of right outer lip corner (MNS)
		poses[37] = faceMesh->createPose(target,"FAP_60");
		FAPOfPoses[37] = 60;
		indexOfPose[37] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 15,    2, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60);
		createPose(poses[37],&vertices,&verticesInPose, &(FDPs[8][4]), Y,   1, 1, 1,   3,  MNS, 2);
		FAP_60_ = faceMesh->createPose(target,"FAP_60_");
		index_Pose_FAP_60_ = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 3,   2,4,8);
		createPose(FAP_60_,&vertices,&verticesInPose, &(FDPs[5][4]), X,   1, 1, 1,   2.2,  0.2*MNS);
		createPose(FAP_60_,&vertices,&verticesInPose, &(FDPs[5][4]), Z,   1, 1, 1,   2.2,  0.2*MNS);
printf(".");
//FAP 61 : Horizontal displacement of left side of nose (Greta use MW instade of ENS)
		poses[38] = faceMesh->createPose(target,"FAP_61");
		FAPOfPoses[38] = 61;
		indexOfPose[38] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1,  3);
		createPose(poses[38],&vertices,&verticesInPose, &(FDPs[9][1]), X,   1, 1, 1,   1,  MW);// ENS);
//FAP 62 : Horizontal displacement of right side of nose (Greta use MW instade of ENS)
		poses[39] = faceMesh->createPose(target,"FAP_62");
		FAPOfPoses[39] = 62;
		indexOfPose[39] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1,  4);
		createPose(poses[39],&vertices,&verticesInPose, &(FDPs[9][2]), X,   1, 1, 1,   1,  -MW);//-ENS);
//FAP 63 : Vertical displacement of nose tip (Greta use MW instade of ENS)
		poses[40] = faceMesh->createPose(target,"FAP_63");
		FAPOfPoses[40] = 63;
		indexOfPose[40] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 2,  3,4);
		createPose(poses[40],&vertices,&verticesInPose, &(FDPs[9][3]), Y,   1, 0.25, 1,   1.5,   MW);//ENS);
//FAP 64 : Horizontal displacement of nose tip (Greta use MW instade of ENS)
		poses[41] = faceMesh->createPose(target,"FAP_64");
		FAPOfPoses[41] = 64;
		indexOfPose[41] = poseIndex++;
		createPose(poses[41],&vertices,&verticesInPose, &(FDPs[9][3]), X,   1, 0.25, 1,   1.5,   -MW);//-ENS);
//FAP 65 : Vertical displacement of left ear (ENS)
		poses[42] = faceMesh->createPose(target,"FAP_65");
		FAPOfPoses[42] = 65;
		indexOfPose[42] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1,  61);
		createPose(poses[42],&vertices,&verticesInPose, &(FDPs[10][1]), Y,   8, 1, 1,   4,  ENS);
//FAP 66 : Vertical displacement of right ear (ENS)
		poses[43] = faceMesh->createPose(target,"FAP_66");
		FAPOfPoses[43] = 66;
		indexOfPose[43] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1,  62);
		createPose(poses[43],&vertices,&verticesInPose, &(FDPs[10][2]), Y,   8, 1, 1,   4,  ENS);
//FAP 67 : Horizontal displacement of left ear (ENS)
		poses[44] = faceMesh->createPose(target,"FAP_67");
		FAPOfPoses[44] = 67;
		indexOfPose[44] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1,  61);
		createPose(poses[44],&vertices,&verticesInPose, &(FDPs[10][3]), X,   8, 1, 1,   4,  ENS);
//FAP 68 : Horizontal displacement of right ear (ENS)
		poses[45] = faceMesh->createPose(target,"FAP_68");
		FAPOfPoses[45] = 68;
		indexOfPose[45] = poseIndex++;
		clearAndAdd(&verticesInPose, &groups, 1,  62);
		createPose(poses[45],&vertices,&verticesInPose, &(FDPs[10][4]), X,   8, 1, 1,   4,  -ENS);
printf(".\n");

	
		Ogre::Animation* anim = faceMesh->createAnimation(fap_animation,0);
		track = anim->createVertexTrack(target,Ogre::VAT_POSE);
		//this part is needed to update the mesh whith the created poses and animation
		Ogre::VertexData* vdata = target==0 ? faceMesh->sharedVertexData : faceMesh->getSubMesh(0)->vertexData;
		Ogre::VertexDeclaration* newdcl = vdata->vertexDeclaration->getAutoOrganisedDeclaration(false, true);
		vdata->reorganiseBuffers(newdcl);

///////////////////////////////////////////
// save the mesh :
/*
		Ogre::String newfileName = "greta_animated.mesh";
		Ogre::String message = "Save animation in file "+newfileName;
		if(MessageBox(0,message.c_str(),"Greta ask", MB_OKCANCEL | MB_ICONQUESTION)==IDOK){
			Ogre::MeshSerializer *meshserializer = new Ogre::MeshSerializer();
			meshserializer->exportMesh(faceMesh.getPointer(),"C:/CODE_GRETA/working/bin/Ogre/media/"+newfileName);
			printf("FAP saved\n");
		}
//*/
	}
	else{
		getPosesIndicesInMesh(faceMesh);
		//we need to create an Ogre::Animation for each Ogre::Entity using this mesh
		fap_animation += "_"+faceEntityName;
		Ogre::Animation* anim = faceMesh->createAnimation(fap_animation,0);
		track = anim->createVertexTrack(target,Ogre::VAT_POSE);
	}

	manualKeyFrame = track->createVertexPoseKeyFrame(0);
	for(int i=0; i<faceMesh->getPoseCount(); i++)
		manualKeyFrame->addPoseReference(i, 0.0);

	// finaly, the face entity is created and the animation is initialized
	//*/
	face = sceneManager->createEntity(faceEntityName, faceMeshFile);
	faceNode->attachObject(face);
	//*
	manualAnimState = face->getAnimationState(fap_animation);
	manualAnimState->setTimePosition(0);
	manualAnimState->setEnabled(true);

//*/	
}

void PlayerOgreHead::applyFAP(FAPFrame* fap){
//*
	for(int i=0; i<69; i++)
		if(fap->GetMask(i))
			FAPValues[i] = fap->GetFAP(i);

	bool modified = false;
	bool tongueModified = false;
	for(int i=0; i<numberOfPoses;i++)
		if(fap->GetMask(FAPOfPoses[i])){
			modified=true;
			manualKeyFrame->updatePoseReference(indexOfPose[i], FAPValues[FAPOfPoses[i]]);
		}
	Ogre::Quaternion q;
	//eyes rotations :
	//	left
	if(fap->GetMask(23) || fap->GetMask(25)){
		q = Ogre::Quaternion(Ogre::Radian(FAPValues[23]/100000.0),Ogre::Vector3(0,1,0)) *
			Ogre::Quaternion(Ogre::Radian(FAPValues[25]/100000.0),Ogre::Vector3(1,0,0));
		l_EyeNode->setOrientation(q);
	}
	//	right
	if(fap->GetMask(24) || fap->GetMask(26)){
		q = Ogre::Quaternion(Ogre::Radian(FAPValues[24]/100000.0),Ogre::Vector3(0,1,0)) *
			Ogre::Quaternion(Ogre::Radian(FAPValues[26]/100000.0),Ogre::Vector3(1,0,0));
		r_EyeNode->setOrientation(q);
	}
	//head rotations:
	if(fap->GetMask(48) || fap->GetMask(49) || fap->GetMask(50)){
		double pitch = FAPValues[48]/100000.0;
		double yaw = FAPValues[49]/100000.0;
		double roll = FAPValues[50]/100000.0;
		if(skull==NULL){
			q = Ogre::Quaternion(Ogre::Radian(roll),Ogre::Vector3(0,0,1)) *
				Ogre::Quaternion(Ogre::Radian(pitch),Ogre::Vector3(1,0,0)) * 
				Ogre::Quaternion(Ogre::Radian(yaw),Ogre::Vector3(0,1,0));
			rotationNode->setOrientation(q);
		}
		else{
			//to not have a separation between head and neck,
			Ogre::Quaternion qvc1 = Ogre::Quaternion(Ogre::Radian(roll*5.5/10.0),Ogre::Vector3(0,0,1)) *
									Ogre::Quaternion(Ogre::Radian(pitch*4.0/6.0),Ogre::Vector3(1,0,0)) * 
									Ogre::Quaternion(Ogre::Radian(yaw*6.0/8.0),Ogre::Vector3(0,1,0));
			Ogre::Quaternion qvc2 = Ogre::Quaternion(Ogre::Radian(roll*0.5/10.0),Ogre::Vector3(0,0,1)) *
									Ogre::Quaternion(Ogre::Radian(pitch/6.0),Ogre::Vector3(1,0,0)) * 
									Ogre::Quaternion(Ogre::Radian(yaw/8.0),Ogre::Vector3(0,1,0));
			Ogre::Quaternion qvc3 = Ogre::Quaternion(Ogre::Radian(roll*4.0/10.0),Ogre::Vector3(0,0,1)) *
									Ogre::Quaternion(Ogre::Radian(pitch/6.0),Ogre::Vector3(1,0,0)) * 
									Ogre::Quaternion(Ogre::Radian(yaw/8.0),Ogre::Vector3(0,1,0));

			Ogre::Bone * vc1 = (Ogre::Bone *) (skull->getParent());
			Ogre::Bone * vc2 = (Ogre::Bone *) (vc1->getParent());
			Ogre::Bone * vc3 = (Ogre::Bone *) (vc2->getParent());
			vc1->setOrientation(qvc1);
			vc2->setOrientation(qvc2);
			vc3->setOrientation(qvc3);
			vc3->_update(true,false);
			updateSkullLink();
		}
	} 
	//jaw movements
	if(fap->GetMask(3) || fap->GetMask(14) || fap->GetMask(15)){
		inf_jawNode->setPosition(-FAPValues[15]*JAWSVERTICALMOVEMENT,-FAPValues[3]*JAWSVERTICALMOVEMENT,FAPValues[14]*JAWSVERTICALMOVEMENT);
	}
	
	//tongue animation
	if(fap->GetMask(43)){
		tongueModified=true;
		tongueKeyFrame->updatePoseReference(0 //index of corresponding pose in the mesh file
										, FAPValues[43]);
	}
	if(fap->GetMask(44)){
		tongueModified=true;
		tongueKeyFrame->updatePoseReference(1 //index of corresponding pose in the mesh file
										, FAPValues[44]);
	}
	if(fap->GetMask(45)){
		tongueModified=true;
		tongueKeyFrame->updatePoseReference(2 //index of corresponding pose in the mesh file
										, FAPValues[45]);
	}
	if(fap->GetMask(46)){
		tongueModified=true;
		if(FAPValues[46]>0){
			tongueKeyFrame->updatePoseReference(3, FAPValues[46]);
			tongueKeyFrame->updatePoseReference(4, 0.0);
		}
		else{
			tongueKeyFrame->updatePoseReference(4, FAPValues[46]);
			tongueKeyFrame->updatePoseReference(3, 0.0);
		}
	}
	if(fap->GetMask(47)){
		tongueModified=true;
		if(FAPValues[47]>0){
			tongueKeyFrame->updatePoseReference(5, FAPValues[47]);
			tongueKeyFrame->updatePoseReference(6, 0.0);
		}
		else{
			tongueKeyFrame->updatePoseReference(6, FAPValues[47]);
			tongueKeyFrame->updatePoseReference(5, 0.0);
		}
	}

	
	//FAPs applied under specific conditions :
		//FAP 6 applied when FAP 53 == 0.0
	if(fap->GetMask(6) || fap->GetMask(53)){
		if(FAPValues[53]==0.0)
			manualKeyFrame->updatePoseReference(index_Pose_FAP_6, FAPValues[6]);
		else
			manualKeyFrame->updatePoseReference(index_Pose_FAP_6, 0.0);
		modified=true;
	}
	
		//FAP 7 applied when FAP 54 == 0.0
	if(fap->GetMask(7) || fap->GetMask(54)){
		if(FAPValues[54]==0.0)
			manualKeyFrame->updatePoseReference(index_Pose_FAP_7, FAPValues[7]);
		else
			manualKeyFrame->updatePoseReference(index_Pose_FAP_7, 0.0);
		modified=true;
	}
	
		//FAP 12 applied when FAP 59 == 0.0
	if(fap->GetMask(12) || fap->GetMask(59)){
		if(FAPValues[59]==0.0){
			manualKeyFrame->updatePoseReference(index_Pose_FAP_12_0, FAPValues[12]);
			if(FAPValues[12]<0)
				manualKeyFrame->updatePoseReference(index_Pose_FAP_12_1, FAPValues[12]);
			else
				manualKeyFrame->updatePoseReference(index_Pose_FAP_12_1, 0);
		}
		else{
			manualKeyFrame->updatePoseReference(index_Pose_FAP_12_0, 0.0);
			manualKeyFrame->updatePoseReference(index_Pose_FAP_12_1, 0.0);
		}
		modified=true;
	}

		//FAP 13 applied when FAP 60 == 0.0
	if(fap->GetMask(13) || fap->GetMask(60)){
		if(FAPValues[60]==0.0){
			manualKeyFrame->updatePoseReference(index_Pose_FAP_13_0, FAPValues[13]);
			if(FAPValues[13]<0)
				manualKeyFrame->updatePoseReference(index_Pose_FAP_13_1, FAPValues[13]);
			else
				manualKeyFrame->updatePoseReference(index_Pose_FAP_13_1, 0.0);
		}
		else{
			manualKeyFrame->updatePoseReference(index_Pose_FAP_13_0, 0.0);
			manualKeyFrame->updatePoseReference(index_Pose_FAP_13_1, 0.0);
		}
		modified=true;
	}
	
		//FAP 17_ with 3.5 applied when FAP 17 > 0 
	if(fap->GetMask(17)){
		if(FAPValues[17] > 0)
			manualKeyFrame->updatePoseReference(index_Pose_FAP_17_, 3.5*FAPValues[17]);
		else
			manualKeyFrame->updatePoseReference(index_Pose_FAP_17_, FAPValues[17]);
	}
		//FAP 31_ applied with absolute value
	if(fap->GetMask(31))
		manualKeyFrame->updatePoseReference(index_Pose_FAP_31_, fabs(FAPValues[31]));
		//modified=true is not required because FAP 31 is applied before
		//FAP 32_ applied with absolute value
	if(fap->GetMask(32))
		manualKeyFrame->updatePoseReference(index_Pose_FAP_32_, fabs(FAPValues[32]));
		//FAP 33_ applied with absolute value
	if(fap->GetMask(33))
		manualKeyFrame->updatePoseReference(index_Pose_FAP_33_, fabs(FAPValues[33]));
		//FAP 34_ applied with absolute value
	if(fap->GetMask(34))
		manualKeyFrame->updatePoseReference(index_Pose_FAP_34_, fabs(FAPValues[34]));
		//FAP 35_ applied with absolute value
	if(fap->GetMask(35))
		manualKeyFrame->updatePoseReference(index_Pose_FAP_35_, fabs(FAPValues[35]));
		//FAP 36_ applied with absolute value
	if(fap->GetMask(36))
		manualKeyFrame->updatePoseReference(index_Pose_FAP_36_, fabs(FAPValues[36]));
		//applied with FAP 31 * FAP 37
	//if(fap->GetMask(31) || fap->GetMask(37))
	//	manualKeyFrame->updatePoseReference(index_Pose_FAP_31x37, FAPValues[31]*FAPValues[37]);
		//applied with FAP 32 * FAP 38
	//if(fap->GetMask(32) || fap->GetMask(38))
	//	manualKeyFrame->updatePoseReference(index_Pose_FAP_32x38, FAPValues[32]*FAPValues[38]);
		//FAP 39_ applied when FAP 39 > 0
	if(fap->GetMask(39)){
		if(FAPValues[39] > 0) manualKeyFrame->updatePoseReference(index_Pose_FAP_39_, FAPValues[39]);
		else manualKeyFrame->updatePoseReference(index_Pose_FAP_39_, 0.0);
	}
		//FAP 40_ applied when FAP 40 > 0
	if(fap->GetMask(40)){
		if(FAPValues[40] > 0) manualKeyFrame->updatePoseReference(index_Pose_FAP_40_, FAPValues[40]);
		else manualKeyFrame->updatePoseReference(index_Pose_FAP_40_, 0.0);
	}
		//FAP 41_ with -5 applied when FAP 41 > 0 
	if(fap->GetMask(41)){
		if(FAPValues[41] > 0)
			manualKeyFrame->updatePoseReference(index_Pose_FAP_41_, -5.0*FAPValues[41]);
		else
			manualKeyFrame->updatePoseReference(index_Pose_FAP_41_, FAPValues[41]);
	}
		//FAP 42_ applied with -5 when FAP 42 > 0 
	if(fap->GetMask(42)){
		if(FAPValues[42] > 0)
			manualKeyFrame->updatePoseReference(index_Pose_FAP_42_, FAPValues[42]);
		else
			manualKeyFrame->updatePoseReference(index_Pose_FAP_42_, -5.0*FAPValues[42]);
	}

		//FAP 55_ applied when FAP 55 < 0
	if(fap->GetMask(55)){
		if(FAPValues[55] < 0) manualKeyFrame->updatePoseReference(index_Pose_FAP_55_, FAPValues[55]);
		else manualKeyFrame->updatePoseReference(index_Pose_FAP_55_, 0.0);
	}
		//FAP 56_ applied when FAP 56 < 0
	if(fap->GetMask(56)){
		if(FAPValues[56] < 0) manualKeyFrame->updatePoseReference(index_Pose_FAP_56_, FAPValues[56]);
		else manualKeyFrame->updatePoseReference(index_Pose_FAP_56_, 0.0);
	}
		//FAP 59_ applied when FAP 59 > 0
	if(fap->GetMask(59)){
		if(FAPValues[59] > 0) manualKeyFrame->updatePoseReference(index_Pose_FAP_59_, FAPValues[59]);
		else manualKeyFrame->updatePoseReference(index_Pose_FAP_59_, 0.0);
	}
		//FAP 60_ applied when FAP 60 > 0
	if(fap->GetMask(60)){
		if(FAPValues[60] > 0) manualKeyFrame->updatePoseReference(index_Pose_FAP_60_, FAPValues[60]);
		else manualKeyFrame->updatePoseReference(index_Pose_FAP_60_, 0.0);
	}
	if(modified)
		manualAnimState->getParent()->_notifyDirty();
	if(tongueModified)
		tongueAnimState->getParent()->_notifyDirty();
	//*/
}

void PlayerOgreHead::readIndices(std::vector<std::vector<int>>* groups, std::string fileName){
	//from FaceData::ReadFaceIndices(std::string path) in FaceData.cpp
	FILE* f;
	if(!(f=fopen(fileName.c_str(), "r")))
		printf("ERROR: cant read \"%s\"\n", fileName.c_str());
	else {
		groups->resize(69);
		char cbuf[10000];
		sprintf_s(cbuf,10000,"");
		int v, size;
		for(int i=1;i<69;i++){
			// On lit la zone "v" concernee:	# %d
			while(cbuf[0]!='#') fgets(cbuf,10000,f);
			sscanf(cbuf, "# %d", &v);
			// On lit la taille de cette zone:	SIZE %d
			fgets(cbuf,10000,f);
			sscanf(cbuf,"SIZE %d", &size);
			// Initialisation de la table de tailles et Allocation memoire
			(*groups)[v].resize(3*size);
			// Lecture des donn�es
			for(int j=0;j<size;j++){
				fgets(cbuf,10000,f);
				sscanf(cbuf,"%d %d %d", &((*groups)[v][j*3]), &((*groups)[v][j*3+1]), &((*groups)[v][j*3+2]));
			}
		}
		fclose(f);
	}
}
void PlayerOgreHead::readFDPs(std::vector<std::vector<Ogre::Vector3>> *FDPs, std::vector<Ogre::Vector3> *vertices, Ogre::String fileName){
	//from FDPdata::ReadFaceFDPs(const char *path, const float **vtxData) in FAPU_FDPs.cpp
	FILE* f;
	if(!(f=fopen(fileName.c_str(),"r")))
		printf("FDP ERROR: cant read %s\n", fileName.c_str());
	else{
		FDPs->resize(12);
		char cbuf[1024];
		// find out the group size (number of FP's in each group)
		fgets(cbuf,1024,f);
		while(strcmp(cbuf,"#START\n"))
			fgets(cbuf,1024,f);
		for(int i=2;i<=11;i++){
			int id, size;
			fgets(cbuf,1024,f);
			sscanf(cbuf, "GROUP %d %d", &id, &size);
			(*FDPs)[id].resize(size+1);
		}
		while(strcmp(cbuf,"END\n")!=0)
			fgets(cbuf,1024,f);
		while(cbuf[0]=='#')
			fgets(cbuf,1024,f);

		// read in the FDPs for each group
		float f0, f1, f2;
		int ref;
		for(unsigned int i=2;i<(*FDPs).size();i++){
			fgets(cbuf,1024,f);
			for(unsigned int j=1;j<(*FDPs)[i].size();j++){
				fgets(cbuf,1024,f);
				sscanf(cbuf, "%f %f %f #%i", &f0, &f1, &f2, &ref);
				if(ref<0)
					(*FDPs)[i][j] = Ogre::Vector3(f0,f1,f2);
				else 
					(*FDPs)[i][j] = (*vertices)[ref];
			}
		}
		fclose(f);
	}
}


void PlayerOgreHead::getVertices(std::vector<Ogre::Vector3> *vertices){
	Ogre::MeshPtr faceMesh = Ogre::MeshManager::getSingleton().load(faceMeshFile,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME); 
	Ogre::VertexData* vertex_data = faceMesh->getSubMesh(0)->useSharedVertices ? faceMesh->sharedVertexData : faceMesh->getSubMesh(0)->vertexData;
	const Ogre::VertexElement* posElem = vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
	Ogre::HardwareVertexBufferSharedPtr vbuf = vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());
	unsigned char* vertex = static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
	float* pReal;
	vertices->resize(vertex_data->vertexCount);

	for (size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize()){
		posElem->baseVertexPointerToElement(vertex, &pReal);
		(*vertices)[j] = Ogre::Vector3(pReal[0],pReal[1],pReal[2]);
	}
	vbuf->unlock();
}

void PlayerOgreHead::clear(std::vector<bool> *v){
	for(unsigned int i=0;i<v->size();i++)
		(*v)[i]=false;
}
void PlayerOgreHead::clear(std::vector<bool> *v, std::vector<int> *indices){
	for(unsigned int i=0;i<indices->size();i++)
		(*v)[(*indices)[i]] = false;
}
void PlayerOgreHead::add(std::vector<bool> *v, std::vector<int> *indices){
	for(unsigned int i=0;i<indices->size();i++)
		(*v)[(*indices)[i]] = true;
}

void PlayerOgreHead::clearAndAdd(std::vector<bool> *v, std::vector<std::vector<int>> *groups,int numberOfGroups,...){// int* whichGroups, int numberOfGroups){
	va_list varg;
	clear(v);
	va_start(varg, numberOfGroups);
	for(int i=0; i<numberOfGroups; i++)
		add(v,&((*groups)[va_arg(varg, int)]));
	va_end(varg);
}
void PlayerOgreHead::clear(std::vector<bool> *v, std::vector<std::vector<int>> *groups,int numberOfGroups,...){// int* whichGroups, int numberOfGroups){
	va_list varg;
	va_start(varg, numberOfGroups);
	for(int i=0; i<numberOfGroups; i++)
		clear(v,&((*groups)[va_arg(varg, int)]));
	va_end(varg);
}
void PlayerOgreHead::createPose( Ogre::Pose *pose, std::vector<Ogre::Vector3> *geomety, std::vector<bool> *zones, Ogre::Vector3 *FDP, axeType axe, double x, double y, double z, double div, double coef, int power,double pi_){
	double val;
	double x_ = 0;
	double y_ = 0;
	double z_ = 0;
	double* axe_ = &x_; // X by default
	double scalex = headPartsNode->getScale().x;
	double scaley = headPartsNode->getScale().y;
	double scalez = headPartsNode->getScale().z;
	//scale div :
	div = div/(2*headPartsNode->getScale().x);
	if (axe==Y) axe_ = &y_;
	if (axe==Z) axe_ = &z_;
	Ogre::Pose::VertexOffsetMap offsets = pose->getVertexOffsets();
	for(unsigned int i=0;i<zones->size();i++){
		if((*zones)[i]){
			val = sqrt( pow(((*geomety)[i].x-FDP->x),2) *x +
						pow(((*geomety)[i].y-FDP->y),2) *y +
						pow(((*geomety)[i].z-FDP->z),2) *z )
				/ div;
			if(val<pi_){
				*axe_ = pow((1.0+cos(val))*0.5,power)*coef;
				if(offsets.count(i)==0) // there is no offset for the vertex i in this pose
					pose->addVertex(i,Ogre::Vector3(x_,y_,z_));
				else
					pose->addVertex(i,offsets[i]+Ogre::Vector3(x_,y_,z_));
			}
		}
	}
}
void PlayerOgreHead::createPose2( Ogre::Pose *pose, std::vector<Ogre::Vector3> *geomety, std::vector<bool> *zones, Ogre::Vector3 *FDP, axeType axe, double x, double y, double z, double div, double coef){
	double val;
	double x_ = 0;
	double y_ = 0;
	double z_ = 0;
	double* axe_ = &x_; // X by default
	if (axe==Y) axe_ = &y_;
	if (axe==Z) axe_ = &z_;
	//scale div :
	div = div/(2*headPartsNode->getScale().x);

	Ogre::Pose::VertexOffsetMap offsets = pose->getVertexOffsets();
	for(unsigned int i=0;i<zones->size();i++){
		if((*zones)[i]){
			val = sqrt( pow(((*geomety)[i].x-FDP->x),2) *x +
						pow(((*geomety)[i].y-FDP->y),2) *y +
						pow(((*geomety)[i].z-FDP->z),2) *z )
				/ div;
			if(val<PI_/4.0){
				*axe_ = (cos(val)-sqrt(2.0)/2.0)*3.41*coef; 
				if(offsets.count(i)==0) // there is no offset for the vertex i in this pose
					pose->addVertex(i,Ogre::Vector3(x_,y_,z_));
				else
					pose->addVertex(i,offsets[i]+Ogre::Vector3(x_,y_,z_));
				
			}
		}
	}
}

void PlayerOgreHead::scalepose( Ogre::Pose *pose, std::vector<bool> *zones, axeType axe, double factor){
	double x_ = 1;
	double y_ = 1;
	double z_ = 1;
	double* axe_ = &x_; // X by default
	if (axe==Y) axe_ = &y_;
	if (axe==Z) axe_ = &z_;
	*axe_ = factor;
	Ogre::Pose::VertexOffsetMap offsets = pose->getVertexOffsets();
	for(unsigned int i=0;i<zones->size();i++){
		if((*zones)[i]){
				if(offsets.count(i)!=0) 
					pose->addVertex(i,Ogre::Vector3(offsets[i].x * x_,
													offsets[i].y * y_,
													offsets[i].z * z_));
				
		}
	}
}

void PlayerOgreHead::getPosesIndicesInMesh(Ogre::MeshPtr pMesh){
	numberOfPoses = 0;
	int tmpFAPOfPoses[69]; //69 : max number of FAPs
	int tmpIndexOfPose[69]; //69 : max number of FAPs
	for(unsigned int indexInMesh=0; indexInMesh<pMesh->getPoseCount(); indexInMesh++){
		std::string nameOfPose = pMesh->getPose(indexInMesh)->getName();
		if(nameOfPose.substr(0,4)=="FAP_"){
			nameOfPose = nameOfPose.substr(4);
			//find Poses used under specifics conditions:
			if      (nameOfPose=="6")
				index_Pose_FAP_6 = indexInMesh;
			else{ if(nameOfPose=="7")
				index_Pose_FAP_7 = indexInMesh;
			else{ if(nameOfPose=="12_0")
				index_Pose_FAP_12_0 = indexInMesh;
			else{ if(nameOfPose=="12_1")
				index_Pose_FAP_12_1 = indexInMesh;
			else{ if(nameOfPose=="13_0")
				index_Pose_FAP_13_0 = indexInMesh;
			else{ if(nameOfPose=="13_1")
				index_Pose_FAP_13_1 = indexInMesh;
			else{ if(nameOfPose=="17_")
				index_Pose_FAP_17_ = indexInMesh;
			else{ if(nameOfPose=="31_")
				index_Pose_FAP_31_ = indexInMesh;
			else{ if(nameOfPose=="32_")
				index_Pose_FAP_32_ = indexInMesh;
			else{ if(nameOfPose=="33_")
				index_Pose_FAP_33_ = indexInMesh;
			else{ if(nameOfPose=="34_")
				index_Pose_FAP_34_ = indexInMesh;
			else{ if(nameOfPose=="35_")
				index_Pose_FAP_35_ = indexInMesh;
			else{ if(nameOfPose=="36_")
				index_Pose_FAP_36_ = indexInMesh;
			else{ if(nameOfPose=="39_")
				index_Pose_FAP_39_ = indexInMesh;
			else{ if(nameOfPose=="40_")
				index_Pose_FAP_40_ = indexInMesh;
			else{ if(nameOfPose=="41_")
				index_Pose_FAP_41_ = indexInMesh;
			else{ if(nameOfPose=="42_")
				index_Pose_FAP_42_ = indexInMesh;
			else{ if(nameOfPose=="55_")
				index_Pose_FAP_55_ = indexInMesh;
			else{ if(nameOfPose=="56_")
				index_Pose_FAP_56_ = indexInMesh;
			else{ if(nameOfPose=="59_")
				index_Pose_FAP_59_ = indexInMesh;
			else{ if(nameOfPose=="60_")
				index_Pose_FAP_60_ = indexInMesh;
			//other FAPs
			else{
				tmpFAPOfPoses[numberOfPoses] = atoi(nameOfPose.c_str());
				tmpIndexOfPose[numberOfPoses] = indexInMesh;
				++numberOfPoses;
			}}}}}}}}}}}}}}}}}}}}}
		}
	}
	FAPOfPoses  = (int*) malloc((numberOfPoses)*sizeof(int));
	indexOfPose = (int*) malloc((numberOfPoses)*sizeof(int));
	for(int i=0; i<numberOfPoses; i++){
		FAPOfPoses[i]  = tmpFAPOfPoses[i];
		indexOfPose[i] = tmpIndexOfPose[i];
	}
}