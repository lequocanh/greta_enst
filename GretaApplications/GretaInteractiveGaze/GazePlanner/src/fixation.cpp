// Shared-Attention system
// Christopher Peters

#include "fixation.h"

FixationEntry *FixationList::addFixation(long time, int x, int y, int objID, bool outsideLeft, bool outsideRight, bool outsideTop, bool outsideBottom)
{
	if (this->fixationEntries.size() == 0) this->startTime = clock();

	FixationEntry *f = new FixationEntry;
	f->screenCoords[0] = x;
	f->screenCoords[1] = y;
	f->selectedID = objID;
	f->time = time;
	f->outsideTop = outsideTop;
	f->outsideBottom = outsideBottom;
	f->outsideLeft = outsideLeft;
	f->outsideRight= outsideRight;
	f->outsideScreen = (outsideLeft || outsideRight || outsideTop || outsideBottom);

	this->fixationEntries.push_back(f);
	
	printf("\n********************************");
	printf("\nCreated a new fixation");
	printf("\nX: %d  Y: %d  ObjID: %d", f->screenCoords[0],f->screenCoords[1], f->selectedID);
	if (f->outsideScreen) printf("\nOUTSIDE SCREEN");
	printf("\n********************************");
	return f;
}