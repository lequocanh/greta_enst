/*
 *  Element.cpp
 *  semaine
 *
 *  Created by Marc Schröder on 12.09.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#include "Element.h"

#include <cassert>


const std::string Element::STATE_STARTING = "starting";
const std::string Element::STATE_READY = "ready";
const std::string Element::STATE_STOPPED = "stopped";
const std::string Element::STATE_FAILURE = "failure";


Element::Element(const std::string & componentName) throw (CMSException) :
	name(componentName),
	_exitRequested(false),
	waitingTime(100),
	state(STATE_STOPPED),
	meta(componentName)
{
	log = CMSLogger::getLog(name);
	state = STATE_STARTING;
	meta.reportState(state);
	isInput=false;
	isOutput=false;

}

Element::~Element()
{
   std::list<Receiver *>::iterator i;
	for (i = receivers.begin(); i != receivers.end(); i++) { 
		Receiver * r = *i;
		delete r;
	}
   std::list<Sender *>::iterator j;
	for (j = senders.begin(); j != senders.end(); j++) { 
		Sender * s = *j;
		delete s;
	}
	delete log;
}

void Element::messageAvailableFrom(Receiver * source)
{
	synchronized (&mutex) {
		inputWaiting.push(source);
		mutex.notify();
	}
}

void Element::startIO() throw(CMSException)
{
	long long startTime = decaf::lang::System::currentTimeMillis();
	meta.reportTopics(receivers, senders, isInput, isOutput);//isInput, isOutput);

    std::list<Receiver *>::iterator i;
	for (i = receivers.begin(); i != receivers.end(); i++) { 
		Receiver * r = *i;
		r->setMessageListener(this);
		r->startConnection();
	}

	std::list<Sender *>::iterator j;
	for (j = senders.begin(); j != senders.end(); j++) { 
		Sender * s = *j;
		s->startConnection();
	}

	long long endTime = decaf::lang::System::currentTimeMillis();
	long long startupDuration = endTime - startTime;
	state = STATE_READY;
	std::stringstream buf;
	buf << "Startup took " << startupDuration << " ms";
	meta.reportState(state, buf.str());

}


//SEMAINEMessage * Element::receive()
CommunicationMessage* Element::receive()
{
	meta.IamAlive();
	// Check at every call that the total system is ready
	synchronized (&meta) {
		while (!meta.isSystemReady()) {
			log->info("waiting for system to become ready");
			try {
				meta.wait();
			} catch (decaf::lang::Exception & ie) {
			}
			if (meta.isSystemReady()) {
				log->info("system ready - let's go");
			}
		}
	}

	Receiver * r = NULL;
	synchronized (&mutex) {
		if (inputWaiting.empty()) {
		// block until input becomes available
		mutex.wait(waitingTime);
		}
		// check again
		if (!inputWaiting.empty()) {
			r = inputWaiting.front();
			inputWaiting.pop();
		}
	}
	if (r == NULL) return NULL; 
	//assert(receivers.contains(r)); // the receiver that alerted us is not one of our receivers;
	SEMAINEMessage * message = r->getMessage();
	assert(message != NULL); // Receiver alerted me but has no message

	if(message==NULL) return NULL;
	CommunicationMessage* cm1=new CommunicationMessage();
	// Need to decide whether it is a text message or a binary message:
	const TextMessage * tm = dynamic_cast<const TextMessage*>(message->getMessage());
	const BytesMessage * bm = dynamic_cast<const BytesMessage*>(message->getMessage());
	if (tm != NULL) {
		cm1->setContent(tm->getText());
	} else if (bm != NULL) {
		const unsigned char * bodyBytes = bm->getBodyBytes();
		long size = bm->getBodyLength();
		char * copy = (char *) malloc(size);
		memcpy(copy, bodyBytes, size);
		cm1->setData(copy);
		cm1->setSize(size);
	}
	//printf("TopicName+DataType: %s\n",(message->getTopicName() + message->getDatatype()).c_str());
	cm1->setType(message->getTopicName());
	delete message;

	return(cm1);
	
/*	try {
		// Now, do something meaningful with the message,
		// and possibly send output via the Senders.
		return(message);
	} catch (CMSException & e) {
		log->error("error when trying to react", &e);
		try {
			state = STATE_FAILURE;
			meta.reportState(state);
		} catch (CMSException & me) {
			log->error("cannot report failure state", & me);
		}
		return NULL;
	}*/
}

bool Element::exitRequested()
{
	synchronized (&mutex) {
		return _exitRequested;
	}
	return false; // just to keep compiler happy
}

void Element::requestExit()
{
	synchronized (&mutex) {
		_exitRequested = true;
		mutex.notify();
	}
}

