// Shared-Attention system
// Christopher Peters

#ifndef _FIXATION_H
#define _FIXATION_H

#include <vector>
#include <time.h>

struct FixationEntry
{
	int selectedID;		//if any, -1 for no object
	int screenCoords[2];
	long time;
	bool classification;	//movement or fixation - not used yet
	
	bool outsideScreen;
	bool outsideTop;
	bool outsideBottom;
	bool outsideLeft;
	bool outsideRight;
};

class FixationList
{
  public:
	FixationList(){startTime = -1;}
	~FixationList();

	FixationEntry *addFixation(long time, int x, int y, int objID, bool outsideLeft = false, bool outsideRight = false, bool outsideTop = false, bool outsideBottom = false);

	long startTime;	//start time is from whenever the first fixation is entered
	std::vector<FixationEntry *> fixationEntries;
};


#endif