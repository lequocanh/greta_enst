//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// RBdefinitions.h:
//
//////////////////////////////////////////////////////////////////////

#pragma once

// Module name
#define INPUTGRETA_MODULE_NAME ".InputGreta"
#define INPUTWATSON_MODULE_NAME "InputWatson"
#define REALTIMEFML_MODULE_NAME ".RealtimeFMLPsydule"
#define REALTIMEBML_MODULE_NAME ".RealtimeBMLPsydule"
#define REALTIMEPLAYER_MODULE_NAME ".RealtimePlayerPsydule"
#define NAOPLAYER_MODULE_NAME ".NaoPlayerPsydule"

// Greta Message Header in the Psyclone module.
#define CLOCK_MESSAGE_HEADER ".Data.Clock"
#define SYSTEM_FEEDBACK_MESSAGE_HEADER ".System.Feedback"
#define AGENTSTATE_MESSAGE_HEADER ".Data.AgentState"
#define UPDATEAGENTSTATE_MESSAGE_HEADER ".Data.UpdateAgentState"
#define COGNITIVE_FML_MESSAGE_HEADER ".Data.CognitiveFML"
#define REALTIME_PLAYER_COMMAND ".RealtimePlayer.Command"
#define DATA_WAV ".Data.WAV"
#define FAP_FRAME ".Data.FAPFrame"
#define BAP_FRAME ".Data.BAPFrame"
#define BAP_KEY_FRAME ".Data.BAPKeyFrame"
#define FAP_KEY_FRAME ".Data.FAPKeyFrame"
#define COMPLEX_INPUT ".Data.Complex.Input"
#define COMPLEX_OUTPUT ".Data.Complex.Output"

// Header for the input messages on DataInput.Whiteboard
#define INPUT_MESSAGE_HEADER "Greta.Data.Input"

// Sender for ActiveMQ
#define FAP_BAP_SENDER "semaine.data.synthesis.lowlevel.video"
#define AUDIO_WAV_SENDER "semaine.data.synthesis.lowlevel.audio"
#define PLAYER_COMMAND_SENDER "semaine.data.synthesis.lowlevel"
#define PLAN_BML "semaine.data.synthesis.plan"

// Greta Whiteboard
#define GRETA_WHITEBOARD ".Whiteboard"
// Input Witeboard
#define INPUT_WHITEBOARD "DataInput.Whiteboard"

// Greta Message Header to send
#define FML_MESSAGE_HEADER ".Data.FML"
#define ACTION_SELECTED_FML ".Data.FML"
#define BML_MESSAGE_HEADER ".Data.BML"


#define REACTIVE_FML_MESSAGE_HEADER ".Data.ReactiveFML"
#define MIMICRY_BML_MESSAGE_HEADER ".Data.MimicryBML"

// Log messages
#define LOG_MESSAGE_CONNEXION_OK " registered to Psyclone"
#define LOG_MESSAGE_CONNEXION_FAILED " could not register to Psyclone"
