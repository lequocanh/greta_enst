rem -- starting psyclone
cd psyclone\
start /min psyclone spec="GretaEVOSpec.xml" verbose=0
cd ..
utils\WAIT.exe 3

rem -- starting Greta
start Psyclone_Clock.exe
start Psyclone_ListenerIntentPlanner.exe
start Psyclone_BehaviorPlanner.exe
start Psyclone_BehaviorRealizer.exe
start Psyclone_WoZ.exe
start Psyclone_Player.exe

cd ..\GretaAuxiliary\interface\dist
java -classpath ./;./AbsoluteLayout.jar;./JavaAIRPlug.jar;./interface.jar interfacepkg.NewJFrame

:: the console wait for the end of the java interface
:: when we close it, all applications are killed :

taskkill /f /im Psyclone_Player.exe
taskkill /f /im Psyclone_WoZ.exe
taskkill /f /im Psyclone_BehaviorRealizer.exe
taskkill /f /im Psyclone_BehaviorPlanner.exe
taskkill /f /im Psyclone_ListenerIntentPlanner.exe
taskkill /f /im Psyclone_Clock.exe
taskkill /f /im psyclone.exe



