//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// XMLDOMTree.cpp: implementation of the XMLDOMTree class.
//
//////////////////////////////////////////////////////////////////////

#include "XMLDOMTree.h"

using namespace XERCES_CPP_NAMESPACE;

//for general comments refer to the header file

///////////////////////////////////////////////////////////////////
// Inner class: iterator
///////////////////////////////////////////////////////////////////

XMLDOMTree::dom_iterator::
dom_iterator(XMLDOMTree *p,DOMNode *start)
  : _parent(p), _current(getNextValid(start)) { }

DOMNode * XMLDOMTree::dom_iterator::getNextValid(DOMNode *n) {
	while (n != NULL && 
		   n->getNodeType() != DOMNode::ELEMENT_NODE &&
		   n->getNodeType() != DOMNode::TEXT_NODE) {
		n = n->getNextSibling();
	}
	return n;
}

XMLDOMTree::impl * XMLDOMTree::dom_iterator::clone() const {
	return new dom_iterator(_parent, _current);
}

bool XMLDOMTree::dom_iterator::equal(const XMLDOMTree::impl &that) const {
	return (_current ==
			dynamic_cast<const dom_iterator &>(that)._current);
}

XMLDOMTree::element XMLDOMTree::dom_iterator::get() const {
	if (_current == NULL) return NULL;

	DOMElement * ce = dynamic_cast<DOMElement *>(_current);
	if(ce!=NULL){
		return new XMLDOMTree(this->_parent, ce);
	}

	DOMText * ct = dynamic_cast<DOMText *>(_current);
	if(ct!=NULL){
		return new XMLDOMTree(this->_parent, ct);
	}

	throw excep::GRETAException("Unexpected child type\n");
}

void XMLDOMTree::dom_iterator::next() {
	_current = getNextValid(_current->getNextSibling());
}

XMLDOMTree::polyit XMLDOMTree::begin() {
	DOMElement *me = dynamic_cast<DOMElement *>(mynode);
	if (me == NULL)
		throw excep::GRETAException("Expected Element node\n");

	return XMLDOMTree::polyit(
		new dom_iterator(this, me->getFirstChild()));
}

XMLDOMTree::polyit XMLDOMTree::end() {
	return XMLDOMTree::polyit(new dom_iterator(this, NULL));
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
XMLDOMTree::XMLDOMTree(std::string name, std::string nameSpace)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = XercesTool::newDocument(name, nameSpace);
	mynode=doc->getDocumentElement();
}

XMLDOMTree::XMLDOMTree(DOMDocument *mydom):XMLGenericTree()
{
	mynode=mydom->getDocumentElement();
}

XMLDOMTree::XMLDOMTree(XMLDOMTree *parent, DOMElement *mydom)
	: XMLGenericTree()
{
	mynode=mydom;
	// a single linked list of all allocated XMLDOMTree nodes
	// belonging to the same Document 
	nextAllocedChild = parent->nextAllocedChild;
	parent->nextAllocedChild = this;
}

XMLDOMTree::XMLDOMTree(XMLDOMTree *parent, DOMText *mydom)
	: XMLGenericTree()
{
	mynode=mydom;
	// a single linked list of all allocated XMLDOMTree nodes
	// belonging to the same Document 
	nextAllocedChild = parent->nextAllocedChild;
	parent->nextAllocedChild = this;
}

XMLDOMTree::~XMLDOMTree()
{
	if (nextAllocedChild == NULL) {
		DOMDocument *mydoc = mynode->getOwnerDocument();
		mydoc->release();
	} else {
		delete nextAllocedChild;
	}
}


DOMNode * XMLDOMTree::getDOMNode()
{
	return mynode;
}

bool XMLDOMTree::isTextNode()
{
	DOMText * mt = dynamic_cast<DOMText *>(mynode);
	return mt!=NULL;
}

/*
void XMLDOMTree::addChild(XMLGenericTree *e)
{
	throw excep::SystemConfigurationException("Do not call this method!");
/*	if(e!=0)
	{
		
		e->parent=this;
		child.push_back(e);	
	}
}
*/

void XMLDOMTree::Print()
{
	printf("%s\n",ToString().c_str());	
}

XMLDOMTree* XMLDOMTree::FindText()
{
	DOMText * mt = dynamic_cast<DOMText *>(mynode);
	if(mt!=NULL)
		return this;

	DOMElement * mn = dynamic_cast<DOMElement *>(mynode);
	if(mn==NULL)
		throw excep::MessageFormatException("expected DOM element, got "+std::string(typeid(*mynode).name()));
	DOMText * txt = XercesTool::getFirstTextNode(mn);
	if(txt==NULL) return NULL;
	XMLDOMTree * nt = new XMLDOMTree(this, txt);

	return nt;
}
XMLGenericTree* XMLDOMTree::FindNodeCalled(std::string n, std::string nameSpace){
	if (n == "text") return FindText();
	return FindElement(n, nameSpace);
}

XMLDOMTree* XMLDOMTree::FindElement(std::string n, const std::string & namespaceURI)
{
	DOMElement * mn = dynamic_cast<DOMElement *>(mynode);
	if(mn==NULL)
		return NULL;
//			throw excep::MessageFormatException("expected DOM element, got "+std::string(typeid(*mynode).name()));

	if(XercesTool::getLocalName(mn)==n)
		return this;
	else
	{
		DOMElement * elt = XercesTool::getFirstElementByLocalNameNS(mn, n, namespaceURI);
		if(elt==NULL)
			return NULL;
	
		XMLDOMTree *newelt = new XMLDOMTree(this, elt);
		return newelt;
	}

}


XMLGenericTree* XMLDOMTree::GetParent()
{

	DOMNode* parent = mynode->getParentNode();
	if (parent==NULL)
		return NULL;
	
	DOMElement * mn = dynamic_cast<DOMElement *>(parent);

	if (mn!=NULL)
	{
		XMLGenericTree *pt = new XMLDOMTree(this, mn);
		return pt;
	}
/*
	DOMDocument * doc = dynamic_cast<DOMDocument *>(parent);
	if (doc!=NULL)
	{
		XMLGenericTree *pt1 =
			new XMLDOMTree(this, doc->getDocumentElement());
		return pt1;
		// ??? it equals this !
		//if the parent is a document, the node hasn't any parent node, so the function must return null
	}
*/
	return NULL;

}

std::string XMLDOMTree::GetName()
{
	// For elements, return the local name, i.e. the part of the tag name after a namespace prefix.
	// For example, in the tagname "ssml:mark", the local name would be "mark".
	DOMElement * mn = dynamic_cast<DOMElement *>(mynode);
	if(mn!=NULL)
		return XercesTool::getLocalName(mn);

	DOMText * dt = dynamic_cast<DOMText *>(mynode);
	if(dt!=NULL)
		return "text";

	throw excep::GRETAException("Expected Element or Text node\n");
}

std::string XMLDOMTree::GetTextValue()
{
	DOMText * dt = dynamic_cast<DOMText *>(mynode);
	if(dt==NULL)
		throw excep::GRETAException("expected Text Node \n");
	std::string content = XercesTool::getTextContent(mynode);
	return content;
}

void XMLDOMTree::SetTextValue(std::string text)
{
	DOMText * dt = dynamic_cast<DOMText *>(mynode);
	if(dt==NULL)
		throw excep::GRETAException("expected Text Node \n");
	XercesTool::setTextContent(dt, text);
}

std::string XMLDOMTree::GetAttribute(std::string n)
{
	DOMElement * mn = dynamic_cast<DOMElement *>(mynode);
	if(mn==NULL)
		throw excep::MessageFormatException("expected DOM element, got "+std::string(typeid(*mynode).name()));

	return XercesTool::getAttribute(mn,n);
}

float XMLDOMTree::GetAttributef(std::string n)
{
	std::string a=GetAttribute(n);
	if(a!="")
	{
		return (float)atof(a.c_str());
	}
	return 0;
}

void XMLDOMTree::SetAttribute(std::string n, std::string value)
{
	DOMElement * mn = dynamic_cast<DOMElement *>(mynode);
	if(mn==NULL)
		throw excep::MessageFormatException("expected DOM element, got "+std::string(typeid(*mynode).name()));
	
	XercesTool::setAttribute(mn, n, value);
}

bool XMLDOMTree::HasAttribute(std::string n)
{
	DOMElement * mn = dynamic_cast<DOMElement *>(mynode);
	if(mn==NULL)
		return false;
//		throw excep::MessageFormatException("expected DOM element, got "+std::string(typeid(*mynode).name()));

	return XercesTool::hasAttribute(mn,n);
}


void XMLDOMTree::SaveToFile(FILE *outputfile)
{
	fprintf(outputfile,"%s\n",ToString().c_str());
}

std::string XMLDOMTree::ToString()
{
	//if the parent hasn't parent, we try to return all the document.
	if(GetParent()==NULL){
		DOMDocument *dom = mynode->getOwnerDocument();
		if(dom!=NULL)
			return XercesTool::dom2string(dom);
	}
	return XercesTool::dom2string(mynode);
}

void XMLDOMTree::Save(std::string filename)
{
	outputfile=fopen(filename.c_str(),"w");
	if(outputfile==0)
		return;
	SaveToFile(outputfile);
	fclose(outputfile);
	outputfile=0;
}

XMLGenericTree* XMLDOMTree::AddChild(XMLGenericTree* child){
	XMLDOMTree* futurChild = dynamic_cast<XMLDOMTree*>(child);
	if(futurChild==NULL)
		throw excep::GRETAException("The child must be an XMLDOMTree\n");
	DOMDocument* mydoc = mynode->getOwnerDocument();
	if(mydoc==NULL)
		throw excep::GRETAException("The parent element must belong to a document\n");
	DOMElement* newChild = (DOMElement *) mydoc->importNode(futurChild->mynode, true);
	mynode->appendChild(newChild);
	return new XMLDOMTree(this,newChild);
}
void XMLDOMTree::AddText(std::string text){
	DOMElement * element = dynamic_cast<DOMElement *>(mynode);
	if(element==NULL)
		throw excep::GRETAException("expected DOM element\n");
	DOMText* textChild = XercesTool::appendChildTextNode(mynode, text);
	new XMLDOMTree(this,textChild);
}

XMLGenericTree* XMLDOMTree::CreateChild(std::string name, std::string nameSpace){
	DOMElement* element = dynamic_cast<DOMElement *>(mynode);
	if(element==NULL)
		throw excep::GRETAException("expected DOM element\n");
	DOMElement* newChild = XercesTool::appendChildElement(mynode, name, nameSpace);
	return new XMLDOMTree(this,newChild);
}
