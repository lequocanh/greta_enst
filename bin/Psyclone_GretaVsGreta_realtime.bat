cd .\psyclone\
start psyclone spec="GretaVsGretaEVOSpec.xml" verbose=4
cd ..
.\utils\WAIT.exe 3

start .\Psyclone_ClockDebug.exe greta1.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorPlannerDebug.exe greta1.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorRealizerDebug.exe greta1.ini
.\utils\WAIT.exe 1

start  Psyclone_PlayerDebug.exe greta1.ini
.\utils\WAIT.exe 1

start .\Psyclone_PromConnectionDebug.exe greta1.ini
.\utils\WAIT.exe 1

start .\Psyclone_ClockDebug.exe greta2.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorPlannerDebug.exe greta2.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorRealizerDebug.exe greta2.ini
.\utils\WAIT.exe 1

start  Psyclone_PlayerDebug.exe greta2.ini
.\utils\WAIT.exe 1

start .\Psyclone_PromConnectionDebug.exe greta2.ini 
.\utils\WAIT.exe 1

start .\Psyclone_ClockDebug.exe greta3.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorPlannerDebug.exe greta3.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorRealizerDebug.exe greta3.ini
.\utils\WAIT.exe 1

start  Psyclone_PlayerDebug.exe greta3.ini
.\utils\WAIT.exe 1

start .\Psyclone_PromConnectionDebug.exe greta3.ini 
.\utils\WAIT.exe 1

start .\Psyclone_ClockDebug.exe greta4.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorPlannerDebug.exe greta4.ini
.\utils\WAIT.exe 1

start .\Psyclone_BehaviorRealizerDebug.exe greta4.ini
.\utils\WAIT.exe 1

start  Psyclone_PlayerDebug.exe greta4.ini
.\utils\WAIT.exe 1

start .\Psyclone_PromConnectionDebug.exe greta4.ini 
.\utils\WAIT.exe 1

cd ..\GretaAuxiliary\interface\dist
java -classpath ./;./AbsoluteLayout.jar;./JavaAIRPlug.jar;./interface.jar interfacepkg.NewJFrame

:: the console wait for the end of the java interface
:: when we close it, all applications are killed :

taskkill /f /im Psyclone_PlayerDebug.exe
taskkill /f /im Psyclone_WoZ.exe
taskkill /f /im Psyclone_BehaviorRealizerDebug.exe
taskkill /f /im Psyclone_BehaviorPlannerDebug.exe
taskkill /f /im Psyclone_PromConnectionDebug.exe
taskkill /f /im Psyclone_ClockDebug.exe
taskkill /f /im psyclone.exe

