// Shared-Attention system
// Christopher Peters

#ifndef _SCENE_H
#define _SCENE_H

#include <stdio.h>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>
#include <FL/Gl.h>
#include <Gl/Glu.h>

#include "IniManager.h"
#include "SharedAttentionAgentAspect.h"
#include "sharedattention.h"
#include "Calibration.h"
#include "objects.h"
#include "attention.h"

class Scene
{
  public:
	Scene(void);
	~Scene();

	bool addNewObject(Named3DObject *no);
	Named3DObject *getObjectByID(int ID);
	Named3DObject *getObjectByIndex(int index){return this->objects[index];}
	Named3DObject *getObjectByName(std::string *name);;

	void drawSceneObjects(void);
	void drawObjectByID(int ID);
	void drawObjectByIndex(int index);

	void updatePickedObjectsVAO(int objectID);

	Calibrator *calibrator;
	BoundaryValues bvs;

	int lastSelectedObjID;	//the ID of the Named3dObj selected during the last fixation
	VAO* outsideScreenVAO;
long hours;
	std::vector<Named3DObject *> objects;



	SharedAttentionAgentAspect *gretaAgent;
};

#endif