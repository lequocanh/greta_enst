//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ConstraintsContainer.cpp: implementation of the ConstraintsContainer class.
//
//////////////////////////////////////////////////////////////////////
// created on ...  by : Radek  (niewiadomski@iut.univ-paris8.fr)

//modifications:
//

#include "NewConstraintsContainer.h"
//#include "Constraint.h"
#include "MultimodalEmotionConstraint.h"
#include "XMLDOMParser.h"
#include <vector>

NewConstraintsContainer::NewConstraintsContainer () 
{
}



ConsNode* NewConstraintsContainer::translate(XMLGenericTree *con_iter)
{

	//init constree
	ConsNode *constree = new ConsNode();

	//if the node is arg
	if(strcmp(con_iter->GetName().c_str(),"arg")==0) 
	{

		//id and type
		int temp_id = -1; //default no id

		std::string temp = (*con_iter).GetAttribute("id");
		if( temp!="") temp_id=( (int) atoi(temp.c_str()) );// if "" means no id defined
				
		temp = (*con_iter).GetAttribute("type");

		if (temp!="") constree->setArg(temp_id,temp);
			else constree->setArg(temp_id,"none");

		//value
		float temp_value = ((float)atof((*con_iter).GetAttribute("value").c_str()));
		constree->setValue(temp_value);

		//operator
		constree->setConsNodeOperator("unkown");
		
	}//end if arg


	//if the node is arg
	if(strcmp(con_iter->GetName().c_str(),"con")==0) {

		//set operator									
		std::string tymczas =(*con_iter).GetAttribute("type");
		if (tymczas!="") constree->setConsNodeOperator(tymczas);
		else constree->setConsNodeOperator("unkown");

		//id and type
		constree->setArg(-1,"none");  //default no id, no type

		//value
		constree->setValue(-1); //default no value

	}//end if con


	//check if the code is correct in both cases "arg" and "con"

	//par default no sons
	constree->setLeft(NULL);
	constree->setRight(NULL);		
		
	// take children
//	std::list<XMLGenericTree*> child = (*con_iter).child;

	//if there are some
//	if(!child.empty()) 
//	{

	XMLGenericTree * chld;
	int counter=0;
//		std::list<XMLGenericTree*>::iterator iter;
//		for(iter=child.begin();iter!=child.end();iter++) //for each child
	for(XMLGenericTree::iterator iter=con_iter->begin();iter != con_iter->end(); ++iter) //for each child
	{
			chld = *iter;
			if (chld->isTextNode()) continue;
			//take a child..

			//is it first or second?
			if (counter==0) constree->setLeft(translate(chld));
			if (counter==1) constree->setRight(translate(chld));
			if (counter==2) // trow exception error: too much children
				;

			counter++;

		}//end for

//	}//empty child

	return constree;

}//end of translate


int NewConstraintsContainer::init (std::string file_name)
{
	if(file_name=="")
	{
		printf("ConstraintsContainer: no filename given\n");		
		return 0;
		//throw Exception("ConstraintsContainer: no filename given",1);
	}

	XMLDOMParser p;
	XMLGenericTree *t;
	p.SetValidating(true);
	t=p.ParseFile(file_name);

	if(t==0)
	{
		printf("Error parsing file %s\n",file_name.c_str());		
		return 0;
		//throw Exception("Error parsing file",2);
	}


	if(!t==0) 
	{

//		std::list<XMLGenericTree*>::iterator emotions_iter;
//		for(emotions_iter=(t)->child.begin();emotions_iter!=(t)->child.end();emotions_iter++) //for each singal

		XMLGenericTree * emotion;

		for(XMLGenericTree::iterator emotions_iter=(t)->begin();
			emotions_iter!=(t)->end();
			++emotions_iter) //for each singal
		{
			emotion = *emotions_iter;
			if (emotion->isTextNode()) continue;

/*		
		if((*emotions_iter)->attributes.empty()) 
			{

		 
			} 

			else 

			{
*/
			if (emotion->HasAttribute("emotion"))
			{
				//read atributes
				std::string emotion_name = emotion->GetAttribute("emotion");

//				std::list<XMLGenericTree*>::iterator iter;

//				std::list<XMLGenericTree*> child = (*emotions_iter)->child;

//				if(!child.empty())
				{
					MultimodalEmotionConstraint mec;		
					mec.setName(emotion_name);

					XMLGenericTree * chld;
					for(XMLGenericTree::iterator iter=emotion->begin();iter!=emotion->end();++iter) //for each child
					{
						chld = *iter;
						if (chld->isTextNode()) continue;
				
						//if it has signals 

						if(strcmp(chld->GetName().c_str(),"signals")==0) 
						{

//							std::list<XMLGenericTree*>::iterator signals_iter;

//							for(signals_iter=(*iter)->child.begin();signals_iter!=(*iter)->child.end();signals_iter++) //for each singal
							XMLGenericTree * sign;
							for(XMLGenericTree::iterator signals_iter=chld->begin();
								signals_iter!=chld->end();
								++signals_iter) //for each signal
							{
								sign = *signals_iter;
								if (sign->isTextNode()) continue;

								ConstraintSignal signal1;

								signal1.setProbability_end((float)atof(sign->GetAttribute("probability_end").c_str()));

								signal1.setId(atoi(sign->GetAttribute("id").c_str()));
								signal1.setOccurence((float)atof(sign->GetAttribute("occurence").c_str()));
								signal1.setRepetivity((float)atof(sign->GetAttribute("repetitivity").c_str()));
								signal1.setMax_duration((float)atof(sign->GetAttribute("max_duration").c_str()));
								
								signal1.setMin_duration((float)atof(sign->GetAttribute("min_duration").c_str()));

								signal1.setProbability_start((float)atof(sign->GetAttribute("probability_start").c_str()));

								signal1.setName(sign->GetAttribute("name").c_str());

								mec.addConstraintSignal(signal1);

							}//end for

						}//end if

						//if it has cons
						if ((strcmp(chld->GetName().c_str(),"cons")==0)
							&&(chld->begin() != chld->end())) 
						{
							
							//each son of "cons" is a constree
							
//							std::list<XMLGenericTree*>::iterator con_iter;
//							for(con_iter=(*iter)->child.begin();con_iter!=(*iter)->child.end();con_iter++) //for each child

							XMLGenericTree * con;
							for(XMLGenericTree::iterator con_iter=chld->begin();
								con_iter!= chld->end();
								++con_iter) //for each child
							{
								con = *con_iter;
								if (con->isTextNode()) continue;
					
								//take an element
																		
									//translate it to cons
										//if it has sons translate them too..
										//no sons? return cons
										
 								ConsNode *constree = translate(con);																																
								
								//add to mec

								mec.addNewConstraint(constree);																		
								
							}//end of for cons'ach

							//add mec to list

							this->mecs[mec.getName()]=mec;

						} //end of if cons


					}//end of childs


				}//end of if empty

			}

		}//end for emotions

	}//end of if==0

	return 1;

}//end of init


NewConstraintsContainer::~NewConstraintsContainer ()
{
}

MultimodalEmotionConstraints NewConstraintsContainer::getMultimodalEmotionConstraints()
{
	return this->mecs;
}

void NewConstraintsContainer::printAll()
{
	//does not work with a new version

	//if(mecs.empty()==false)
	//{
	//	std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;
	//	for(iter=mecs.begin();iter!=mecs.end();iter++)
	//	{
	//		printf("Name : %s \n", (*iter).first.c_str());	


	//		MultimodalEmotionConstraint mec=((*iter).second);

	//		std::vector<Constraint> *constraints = mec.getConstraints();

	//		std::vector<Constraint>::iterator constraint_iter;

	//		for(constraint_iter=constraints->begin();constraint_iter!=constraints->end();constraint_iter++)
	//		{

	//			printf("constraint type, %s \n", (*constraint_iter).getConstraintType().c_str());
	//			printf("arg1id, %i \n", (*constraint_iter).getArg1Id());
	//			printf("arg2id, %i \n", (*constraint_iter).getArg2Id());
	//			printf("arg1type, %i \n", (*constraint_iter).getArg1Type());
	//			printf("arg2type, %i \n", (*constraint_iter).getArg2Type());

	//		}//end of for						

	//		std::vector<ConstraintSignal> *signals = mec.getConstraintSignals();

	//		std::vector<ConstraintSignal>::iterator signal_iter;

	//		for(signal_iter=signals->begin();signal_iter!=signals->end();signal_iter++)
	//		{

	//			printf("signal name, %s \n", (*signal_iter).getName().c_str());
	//			printf("signal id, %i \n", (*signal_iter).getId());
	//			printf("signal max duration, %f \n", (*signal_iter).getMax_duration());
	//			printf("signal repetitivity, %f \n", (*signal_iter).getRepetivity());
	//			printf("signal prob start, %f \n", (*signal_iter).getProbability_start());
	//			printf("signal prob end, %f \n", (*signal_iter).getProbability_end());
	//			printf("signal occurence, %f \n", (*signal_iter).getOccurence());

	//		}//end of for						

	//	}//end of for
	//}
}


float NewConstraintsContainer::getRepetivity(std::string emotion, std::string signalname)
{

	if(mecs.empty()==false)

	{
		std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;

		for(iter=mecs.begin();iter!=mecs.end();iter++)
		{

			std::string current_emotion_name = (*iter).first;

			MultimodalEmotionConstraint * mec = &(mecs[iter->first]);

			std::vector<ConstraintSignal> *signals = mec->getConstraintSignals();

			std::vector<ConstraintSignal>::iterator signals_iter;

			for(signals_iter=signals->begin();signals_iter!=signals->end();signals_iter++){

				std::string current_signal_name=(*signals_iter).clone().getName();

				if((current_emotion_name.compare(emotion)==0)  &&( current_signal_name.compare(signalname)==0 ) ){

					return (*signals_iter).getRepetivity();				
				}

			}//end of for						

		}//end of for
	}

	return 0.0f;

}


float NewConstraintsContainer::getProbability_start(std::string emotion, std::string signalname)
{

	if(mecs.empty()==false)

	{
		std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;

		for(iter=mecs.begin();iter!=mecs.end();iter++)
		{

			std::string current_emotion_name = (*iter).first;

			MultimodalEmotionConstraint * mec = &(mecs[iter->first]);

			std::vector<ConstraintSignal> *signals = mec->getConstraintSignals();

			std::vector<ConstraintSignal>::iterator signals_iter;

			for(signals_iter=signals->begin();signals_iter!=signals->end();signals_iter++)
			{

				std::string current_signal_name=(*signals_iter).clone().getName();

				if((current_emotion_name.compare(emotion)==0)  && ( current_signal_name.compare(signalname)==0 ) )
				{

					return (*signals_iter).getProbability_start();				
				}

			}//end of for						

		}//end of for
	}

	return 0.0f;

}

float NewConstraintsContainer::getProbability_end(std::string emotion, std::string signalname)
{

	if(mecs.empty()==false)

	{
		std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;

		for(iter=mecs.begin();iter!=mecs.end();iter++)
		{

			std::string current_emotion_name = (*iter).first;

			MultimodalEmotionConstraint * mec = &(mecs[iter->first]);

			std::vector<ConstraintSignal> * signals = mec->getConstraintSignals();

			std::vector<ConstraintSignal>::iterator signals_iter;

			for(signals_iter=signals->begin();signals_iter!=signals->end();signals_iter++)
			{

				std::string current_signal_name=((*signals_iter).clone()).getName();

				if( ( current_emotion_name.compare(emotion)==0 ) &&( current_signal_name.compare(signalname)==0 ) )
				{

					return (*signals_iter).getProbability_end();				
				}

			}//end of for						

		}//end of for
	}

	return 0.0f;

}

float NewConstraintsContainer::getOccurence(std::string emotion, std::string signalname)	
{

	if(mecs.empty()==false)

	{
		std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;

		for(iter=mecs.begin();iter!=mecs.end();iter++)
		{

			std::string current_emotion_name = (*iter).first;

			MultimodalEmotionConstraint * mec = &(mecs[iter->first]);

			std::vector<ConstraintSignal> *signals = mec->getConstraintSignals();

			std::vector<ConstraintSignal>::iterator signals_iter;

			for(signals_iter=signals->begin();signals_iter!=signals->end();signals_iter++)
			{

				std::string current_signal_name=(*signals_iter).clone().getName();

				if((current_emotion_name.compare(emotion)==0) &&( current_signal_name.compare(signalname)==0 ) )
				{

					return (*signals_iter).getOccurence();				
				}

			}//end of for						

		}//end of for
	}

	return 0.0f;

}

float NewConstraintsContainer::getMax_duration(std::string emotion, std::string signalname)
{

	if (signalname.compare("")==0) return 0.0;

	if(mecs.empty()==false)

	{
		std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;

		for(iter=mecs.begin();iter!=mecs.end();iter++)
		{

			std::string current_emotion_name = (*iter).first;

			MultimodalEmotionConstraint * mec = &(mecs[iter->first]);

			std::vector<ConstraintSignal> *signals = mec->getConstraintSignals();

			std::vector<ConstraintSignal>::iterator signals_iter;

			for(signals_iter=signals->begin();signals_iter!=signals->end();signals_iter++)
			{

				std::string current_signal_name=(*signals_iter).clone().getName();

				//TO DO : there is an error if the behavior set desciption is not compatible with the constraints set desciption

				if((current_emotion_name.compare(emotion)==0)  && ( current_signal_name.compare(signalname)==0 ) )
				{

					return (*signals_iter).getMax_duration();				
				}

			}//end of for						

		}//end of for
	}

	return 0.0f;

}


MultimodalEmotionConstraint* NewConstraintsContainer::getMultimodalEmotionConstraint(std::string emotion)	
{

	if(mecs.empty()==false)

	{
		std::map<std::string,MultimodalEmotionConstraint>::const_iterator iter;

		for(iter=mecs.begin();iter!=mecs.end();iter++)
		{
			std::string current_emotion_name = (*iter).first;

			MultimodalEmotionConstraint * mec = &(mecs[iter->first]);

			if (current_emotion_name.compare(emotion)==0) return mec;

		}

	}

	return NULL;
}
