//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// ActionSelection.h: code for adapting and choosing the right BCs.
//
// Author: Etienne de Sevin etienne.de-sevin@telecom-paristech.fr
//
//////////////////////////////////////////////////////////////////////
#pragma once

#if !defined(ACTIONSELECTIONCONNECTOR_H)
#define _ACTIONSELECTIONCONNECTOR_H

#include "ActionSelection.h"
#include <cms/CMSException.h>
#include "RandomGen.h"

#include <semaine/components/Component.h>
#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/message/SEMAINEMessage.h>
#include <semaine/cms/receiver/Receiver.h>
#include <semaine/cms/receiver/FMLReceiver.h>
#include <semaine/cms/receiver/BMLReceiver.h>
#include <semaine/cms/sender/BMLSender.h>
#include <semaine/cms/sender/FMLSender.h>
#include <semaine/cms/receiver/StateReceiver.h>


using namespace cms;
using namespace semaine::components;
using namespace semaine::cms::sender;
using namespace semaine::cms::receiver;

namespace semaine {
namespace components {
namespace actionselectionconnector {


class ActionSelectionConnector : public Component
{
public:

	/** 
    * Class default contructor.
    * 
    */
	ActionSelectionConnector(const std::string & componentname, bool isInput, bool isOutput) throw (CMSException);
	/** 
    * Class destructor.
    * 
    */
	virtual ~ActionSelectionConnector();

protected:
	void initConnection() throw (CMSException);
	void ReadMessage(SEMAINEMessage *msg) throw (CMSException);
	void choiceSend() throw (CMSException);

	virtual void react(SEMAINEMessage * m) throw (CMSException);
	virtual void act() throw (CMSException);
	void customStartIO() throw(std::exception);

private :
	
	FMLReceiver * fmlReceiver;
	BMLReceiver * bmlReceiver;
	StateReceiver * UilReceiver;
	StateReceiver * agentSpeakingReceiver;
	StateReceiver * agentTurnReceiver;
	StateReceiver * contextReceiver;
	Receiver * commandReceiver;
	BMLSender * bmlSender;
	FMLSender * fmlSender;
	Receiver * audioReceiver;
	Receiver * FAPReceiver;
	std::string choice;
	ActionSelection AS;
	float NIU;
	float old_NIU;
	int fapend;
	int audioend;
	std::string speaking;
	std::string agentTurn;
	time_t startD;
	time_t endD;
	double difD;
	int waiting;
	int hascontent;
	std::string agentname;
	int fapendtime;
	int timestart;
	std::string idaction[50];
	std::string eventaction[50];
	long startaction[50];
	int iterXML;
	float arousal;
	float valence;
	float potency;
	float hyst;
	RandomGen *randgen;
	float randomBC;



};

} // namespace dummy
} // namespace components
} // namespace semaine

#endif


