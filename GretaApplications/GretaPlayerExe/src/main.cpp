
#include "PlayerFLTKWindow.h"

#include "IniManager.h"
#include <FL/Fl.H>
#include "XercesTool.h"

IniManager inimanager;
#ifdef NDEBUG
#include <shellapi.h>
bool cvtLPW2stdstring(std::string& s, const LPWSTR pw, UINT codepage = CP_ACP){
	bool res = false;
	char* p = 0;
	int bsz;
	bsz = WideCharToMultiByte(codepage, 0, pw,-1,0,0,0,0);
	if (bsz > 0) {
		p = new char[bsz];
		int rc = WideCharToMultiByte(codepage,0,pw,-1,p,bsz,0,0);
		if (rc != 0) {
			p[bsz-1] = 0;
			s = p;
			res = true;
		}
	}
	delete [] p;
	return res;
}

int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInstance,
     LPSTR     lpszCmdParm,
     int       nCmdShow
)
{
	LPWSTR *szArglist;
	int nArgs;
	std::string ini_filename="";
	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if (nArgs==2)
		cvtLPW2stdstring(ini_filename,szArglist[1]);
#else
int main (int argc, char *argv[]){
	std::string ini_filename="";
	if (argc==2)
		ini_filename=argv[1];
#endif
	else
		ini_filename="greta.ini";
	inimanager.ReadIniFile(ini_filename);
	printf("Chargement iniFile Ok\n");

	XercesTool::startupXMLTools();

	PlayerFLTKWindow *p;
	p=new PlayerFLTKWindow();
	printf("Cr�ation player Ok\n");
	p->show();
	printf("Affichage player Ok\n");
  
	Fl::run();

	XercesTool::shutdownXMLTools();
	delete p;
}

