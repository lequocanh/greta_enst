#include "PlayerOgreView.h"
#include <FL/x.H>
#include <FL/fl.h>
#include "BAPFrame.h"

extern IniManager inimanager;


void playerOgreIDLE(void* v){
	Sleep(5);
	((PlayerOgreView*)v)->redraw();
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PlayerOgreView::PlayerOgreView(int x, int y, int w, int h, const char *label, bool autoupdate, bool realtime):Fl_Window(x,y,w,h,label),
ogreRenderWindow(0), ogreViewport(0),pCamera(0),ogreWorld(0),pib(0){
	this->realtime=realtime;
	filetoload="";
	Initialize(x,y,w,h,autoupdate);
}

PlayerOgreView::PlayerOgreView(int x, int y, int w, int h, const char *label,std::string filename, bool autoupdate, bool realtime):Fl_Window(x,y,w,h,label),
ogreRenderWindow(0), ogreViewport(0),pCamera(0),ogreWorld(0),pib(0){
	this->realtime=realtime;
	filetoload=filename;
	Initialize(x,y,w,h,autoupdate);
}

PlayerOgreView::PlayerOgreView():Fl_Window(10,20,375,250,"Greta player"),
ogreRenderWindow(0), ogreViewport(0),pCamera(0),ogreWorld(0),pib(0)
{
	this->size_range(375,250,1280,1024,1,1,0);
}

PlayerOgreView::~PlayerOgreView(){
}

//////////////////////////////////////////////////////////////////////
// Functions
//////////////////////////////////////////////////////////////////////


void PlayerOgreView::show(){
	Fl_Window::show();
	if(!ogreWorld)
		initOgreSystem();
}

void PlayerOgreView::initOgreSystem(){
	//Ogre's initialisation
	ogreWorld = new PlayerOgreWorld();

	Ogre::NameValuePairList viewConfig;
	//anti aliasing :
	viewConfig["FSAA"] = "4";  // Level 4 anti aliasing

	//to synchronize with display frequency
	viewConfig["vsync"]="true";

	//window handle :
	viewConfig["externalWindowHandle"] = Ogre::StringConverter::toString( (unsigned int)(fl_xid(this)) );
	//create the render window
    ogreRenderWindow = ogreWorld->getRoot()->createRenderWindow("Ogre rendering window", w(), h(), false, &viewConfig);


	ogreWorld->initWorld();
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	//create agents
	ogreWorld->initAgents(realtime);
	//create the scene
	ogreWorld->setScene(inimanager.GetValueString("PLAYER_TERRAIN"));
	//get the view to the main agent
	loadCamera("agent","free");

	if(inimanager.GetValueInt("PLAYER_SHOWLISTENER")){
		loadCamera("world","free");
	}
	//to get the agent's view of its right eye :
	//loadCamera("agent","eyes");
}

void PlayerOgreView::loadCamera(std::string characterType, std::string cameraType){
	pCamera = ogreWorld->getCamera(characterType,cameraType);
	pCamera->setAspect(w(),h());
	if(ogreViewport==0){
		ogreViewport = ogreRenderWindow->addViewport(pCamera->getOgreCamera());
	}
	else
		ogreViewport->setCamera(pCamera->getOgreCamera());
	ogreViewport->setBackgroundColour(*(ogreWorld->getBackgroundColour()));
}
void PlayerOgreView::setCharacterSpeaker(std::string characterName){
	ogreWorld->setCharacterSpeaker(characterName);
	if(pCamera->getOgreCamera()->getName() != "FreeCamera") // not world's camera
		loadCamera("agent", "free");
}
void PlayerOgreView::resize(int x, int y, int w, int h){
	if(ogreRenderWindow){
		ogreRenderWindow->resize(w, h);
		ogreRenderWindow->windowMovedOrResized();
	}
	if(pCamera)
		pCamera->setAspect(w,h);
	Fl_Window::resize(x,y,w,h);
}

void PlayerOgreView::draw(){
	if(filetoload!=""){
		ogreWorld->getAgent()->AssignFile((char*)filetoload.c_str());
		filetoload="";
	}
	if(outputframenumber!=0){
		if(ogreWorld->getAgent()->GetBaseFileName()=="empty")
			outputframenumber->value("no file");
		else{
			sprintf_s(usefulstring,20,"%d/%d (%.2f)",
					  ogreWorld->getAgent()->GetCurrentFrame()+1,
					  ogreWorld->getAgent()->GetTotalNumberOfFrames(),
					  ogreWorld->getAgent()->GetCurrentFrame()*
					  (1.0f/
					  ogreWorld->getAgent()->GetFapFps()));
			outputframenumber->value(usefulstring);
		}
	}
	if(outputframerate!=0){
		if(frameaccumulator==49){
			sprintf_s(usefulstring,20,"%.1f fps",ogreRenderWindow->getLastFPS());
			outputframerate->value(usefulstring);
			frameaccumulator=0;
		}
		else
			frameaccumulator++;
	}

	ogreWorld->update();
	ogreWorld->getRoot()->renderOneFrame();


	if(capturing){
		ogreRenderWindow->copyContentsToMemory(*pib);
		avirecorder.writeFrame(pib->data);
		if(strcmp(getAgent()->GetStatus(),"ready")==0)
			this->EndCapture();
	}
}

void PlayerOgreView::Initialize(int x, int y, int w, int h, bool autoupdate){
	camerasettings.ReadIniFile("OgreCamera.ini");
	if(autoupdate==true)
		Fl::add_idle(playerOgreIDLE,this);
	outputframenumber=0;
	outputframerate=0;
	previoustime=0;
	frameaccumulator=0;
	capturing=false;
}

void PlayerOgreView::MoveCamera(float px,float py,float pz,float rx,float ry,float rz){
	if(pCamera->isMovable()){
		pCamera->resetPosNRot();
		pCamera->move(px,py,pz);
		pCamera->pitch(rx);
		pCamera->yaw(ry);
		pCamera->roll(rz);
		//redraw();
	}
}


void PlayerOgreView::RemoveIdle(){
	Fl::remove_idle(playerOgreIDLE,this);
}


int PlayerOgreView::handle(int event){
	//bool changed = false;
	if(event==FL_PUSH){
		posX = Fl::event_x();
		posY = Fl::event_y();
		return 1;
	}
	if(event == FL_MOUSEWHEEL && pCamera->isMovable()){
		pCamera->move(0,0,Fl::event_dy()*10);
		//changed = true;
	}
	if(event==FL_DRAG && pCamera->isMovable()){
		if(Fl::event_button()==FL_LEFT_MOUSE){
			pCamera->move((posX-Fl::event_x())*0.2,(Fl::event_y()-posY)*0.2,0);
			posX = Fl::event_x();
			posY = Fl::event_y();
			//changed = true;
		}
		if(Fl::event_button()==FL_MIDDLE_MOUSE){
			pCamera->move(0,0,(Fl::event_y()-posY)*0.2);
			pCamera->roll(0.001*(posX-Fl::event_x()));
			posX = Fl::event_x();
			posY = Fl::event_y();
			//changed = true;
		}
		if(Fl::event_button()==FL_RIGHT_MOUSE){
			pCamera->pitch(0.01*(posY-Fl::event_y()));
			pCamera->yaw(0.01*(posX-Fl::event_x()));
			posX = Fl::event_x();
			posY = Fl::event_y();
			//changed = true;
		}
	}
	//if(changed)
		//redraw();
	return Fl_Window::handle(event);
}

void PlayerOgreView::SetOutputControl(Fl_Output*framenumber,Fl_Output*framerate){
	outputframenumber=framenumber;
	outputframerate=framerate;
}

void PlayerOgreView::SetCameraView(int i){
	if(pCamera->isMovable()){
		char number[3];
		float px,py,pz,rx,ry,rz;
		sprintf_s(number,3,"%d",i+1);
		std::string parameter;
		parameter=std::string(number)+"_POS_X";
		px=camerasettings.GetValueFloat(parameter);
		parameter=std::string(number)+"_POS_Y";
		py=camerasettings.GetValueFloat(parameter);
		parameter=std::string(number)+"_POS_Z";
		pz=camerasettings.GetValueFloat(parameter);
		parameter=std::string(number)+"_ROT_X";
		rx=camerasettings.GetValueFloat(parameter);
		parameter=std::string(number)+"_ROT_Y";
		ry=camerasettings.GetValueFloat(parameter);
		parameter=std::string(number)+"_ROT_Z";
		rz=camerasettings.GetValueFloat(parameter);
		MoveCamera(px,py,pz,rx,ry,rz);
	}
}

void PlayerOgreView::SaveCameraSettings(int i){
	if(pCamera->isMovable()){
		char number[3];
		sprintf_s(number,3,"%d",i+1);
		Ogre::Vector3 pos = pCamera->getOgreCamera()->getPosition();
		camerasettings.SetValueFloat(std::string(number)+"_POS_X",pos.x);
		camerasettings.SetValueFloat(std::string(number)+"_POS_Y",pos.y);
		camerasettings.SetValueFloat(std::string(number)+"_POS_Z",pos.z);
		
		camerasettings.SetValueFloat(std::string(number)+"_ROT_X",pCamera->getPitchValue());
		camerasettings.SetValueFloat(std::string(number)+"_ROT_Y",pCamera->getYawValue());
		camerasettings.SetValueFloat(std::string(number)+"_ROT_Z",pCamera->getRollValue());

		camerasettings.WriteIniFile("OgreCamera.ini");
	}
}

void PlayerOgreView::invalidate(){}


void PlayerOgreView::StartCapture(){
	if(capturing)
		return;
	if((getAgent()!=0)&&(strcmp(getAgent()->GetStatus(),"ready")==0)){
		int wwarp=(w()/4)*4;
		int hwarp=(h()/4)*4;
		if(pib==0)
			pib = new Ogre::PixelBox(wwarp,hwarp,1,Ogre::PixelFormat::PF_R8G8B8);
		else{
			pib->bottom = hwarp;
			pib->right = wwarp;
			pib->rowPitch = wwarp;
			pib->slicePitch = wwarp*hwarp;
		}
		pib->data = malloc(wwarp*hwarp*3);

		avirecorder.setNumberOfFrame(getAgent()->GetTotalNumberOfFrames());
		if(avirecorder.open((char*)(getAgent()->GetBaseFileName()+".avi").c_str(),0,hwarp,0,wwarp,25)){
			getAgent()->StopTalking();
			getAgent()->WRITEAVI=true;
			getAgent()->SetCurrentFrame(0);
			getAgent()->StartTalking();
			if(getListener()!=0){
				getListener()->StopTalking();
				getListener()->WRITEAVI=true;
				getListener()->SetCurrentFrame(0);
				getListener()->StartTalking();
			}
			capturing=true;
			printf("started capturing\n");
		}
		else
			printf("problem initializing avi file.\n");
	}
}


void PlayerOgreView::EndCapture(){
	if(getAgent()!=0){
		if(strcmp(getAgent()->GetStatus(),"ready")==0)
			if(capturing==true){
				capturing=false;
				getAgent()->WRITEAVI=false;
				if(getListener()!=0)
					getListener()->WRITEAVI=false;
				avirecorder.close();
				printf("avifile closed\n");

				printf("adding audio\n");
				if(avirecorder.add_wav((getAgent()->GetBaseFileName()+".wav").c_str()))
					printf("done\n");
				else
					printf("problem with wav file!\n");
			}
	}
}


