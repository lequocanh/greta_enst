#ifndef PLAYEROGREAGENT_H
#define PLAYEROGREAGENT_H

#include <ogre.h>
#include "PlayerOgreCamera.h"
#include "PlayerOgreBodySkeleton.h"
#include "PlayerOgreHead.h"
#include "BAPFrame.h"
#include "BAPData.h"
#include "FAPData.h"


class PlayerOgreAgent{
public:
	
	bool WRITEAVI;
	FAPData *FAPs;
	BAPData *BAPs;

	PlayerOgreAgent(Ogre::SceneManager *sceneManager,Ogre::SceneNode *rootNode,std::string name,std::string character,bool realtime=false);
	PlayerOgreCamera* getEyesCamera(){return eyesCamera;}
	PlayerOgreCamera* getAgentCamera(){return agentCamera;}
	PlayerOgreHead * getHead(){return head;}
	Ogre::SceneNode* getNode(){ return agentNode; }
	std::string GetBaseFileName(){ return openedfile; }
	double getYPosition(){return body==0 ? -170 /*default value*/ : body->getMesh()->getBounds().getMinimum().y;}
	void SetCurrentFrame(int c){ AnimationActualFrame = c; }
	int GetCurrentFrame(){ return AnimationActualFrame; }
	int GetTotalNumberOfFrames(){ return maxframes; }
	char* GetStatus(){return (char*)status.c_str();}
	void MoveAgent(double x, double y,double z){ agentNode->translate(Ogre::Vector3(x,y,z)); }
	int GetFapFps(){ return FAPs->fps; }
	char* GetWavFileName(){ return (char*)wav_filename.c_str(); }

	void ReloadFile();
	void StartTalking();
	void StopTalking();
	void OneFrameUp();
	void OneFrameDown();
	void EnableAudio(bool enable);
	void AssignFile(char* filename);
	void LoadBAPFrame(int index,BAPFrame *inputBAPFrame);
	void LoadFAPFrame(FAPFrame *f){head->applyFAP(f);}
	void update();

	void setVisible(bool visible){agentNode->setVisible(visible);}
	void SwitchHair(){head->SwitchHair();}
	void SwitchSkin(){head->SwitchSkin();}
	void SwitchEyes(){head->SwitchEyes();}
	void SwitchSkeleton();

private:
	std::string name;
	std::string openedfile;
	std::string fap_filename;
	std::string bap_filename;
	std::string wav_filename;
	std::string status;
	clock_t agent_created;
	double started_animation;
	double stoppingtime;
	double local_time;
	int AnimationActualFrame;
	int BAPframeDisplayed;
	int FAPframeDisplayed;
	int maxframes;
	int animationfps;
	bool audioenabled;
	bool audio;
	bool realtime;
	bool showBody;
	
	Ogre::SceneNode *agentNode;
	Ogre::SceneNode *bodyNode;
	Ogre::Entity *body;
	PlayerOgreCamera *eyesCamera;
	PlayerOgreCamera *agentCamera;
	PlayerOgreBodySkeleton *skeleton;
	PlayerOgreHead *head;
	bool check_fps();
	bool initFAPs();
	void RestartAnimation();
	void LoadFAPFrameIndex(int index);


//TODO
public:
	void SwitchIFAPU(){printf("PlayerOgreAgent::SwitchIFAPU not implemented\n");}
	void SetHairColor(double r, double g, double b){printf("PlayerOgreAgent::SetHairColor not implemented\nIt's better to change the material of hair\n");}
	int ExportFace(std::string NameFile, int fapframe_index){printf("PlayerOgreAgent::ExportFace not implemented\n"); return 1;}
	int ExportFaceVRML(std::string NameFile, int fapframe_index){printf("PlayerOgreAgent::ExportFaceVRML not implemented\n"); return 1;}
};
#endif


