//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorRealizer.cpp: implementation of the BehaviorRealizer class. 
// Created by Elisabetta Bevacqua on 30.06.09
//////////////////////////////////////////////////////////////////////


#include "BehaviorRealizer.h"
#include "FAPwriter.h"
#include "RandomGen.h"
#include "FileNames.h"
#include "IdleMovements.h"
//#include <libxml/xmlreader.h>

#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/exceptions/MessageFormatException.h>
#include <semaine/datatypes/xml/BML.h>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;


#define MAXFRAMESENT 10

extern IniManager inimanager;
extern FileNames filenames;
RandomGen *randogen;
RandomGen *rando;

int DELAY = 300;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

namespace semaine {
namespace components {
namespace greta {

BehaviorRealizer::BehaviorRealizer(const std::string & componentName, bool isInput, bool isOutput) throw(CMSException) :
	Component(componentName,isInput,isOutput)
{
	bmlReceiver = new Receiver("semaine.data.synthesis.plan.speechtimings");
	receivers.push_back(bmlReceiver); 
	bmlBCReceiver = new Receiver("semaine.data.action.selected.behaviour");
	receivers.push_back(bmlBCReceiver); 
	fapSender = new Sender("semaine.data.synthesis.lowlevel.video.FAP", "FAP", getName());
	senders.push_back(fapSender);
	bapSender = new Sender("semaine.data.synthesis.lowlevel.video.BAP", "BAP", getName());
	senders.push_back(bapSender);
	commandSender = new Sender("semaine.data.synthesis.lowlevel.command", "command", getName());
	senders.push_back(commandSender);

	orphan_cont = 0;
	this->start();
}

BehaviorRealizer::~BehaviorRealizer()
{
	delete bmlReceiver;
	delete fapSender;
	delete bapSender;
	delete commandSender;
}

void BehaviorRealizer::customStartIO() throw(std::exception) 
{
	randogen = new RandomGen();

	//Modication Etienne 29/01/10: useful ?
	//xmlTextReaderSchemaValidate(xmlNewTextReaderFilename("mmsystem/lexicon.xml"), "mmsystem/xsd/behaviorsets.xsd");

	previous_time=0;
	fapframes=0;
	bapframes=0;
	//all_fapframes=0;
	fapframeswithoutnoise=0;
 	filenames.Base_File="output/RealtimeBML";
		
	FPS=25.0f;

	NoEndFaceSignals = false;
	
	if(inimanager.GetValueInt("REALTIME_GESTUREENGINE")==0)
		last_bap_frame=0;
	else
		last_bap_frame=new BAPFrame();

	DELAY = inimanager.GetValueInt("REALTIME_DELAY");
	initialization=true;
	itercommand = 0;
	iterfap = 0;
	iterbap = 0;
	iterfapidle = 0;
	itercommandidle = 0;
	iterbapidle = 0;
	iteridle = 0;
	idledone = 0;
	difD = 0.0;
	randidle = 0.0;
	priority = 0.0;
	startat = 0;
	lifetime = 0; 
	hasAudio = false;

	//pc.SetTime(meta.getTime());

	//Memory test
	/*
	int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );
	tmpFlag |= _CRTDBG_CHECK_ALWAYS_DF;
	_CrtSetDbgFlag( tmpFlag );*/
}


void BehaviorRealizer::act() throw(CMSException)
{
	pc.SetTime(meta.getTime());
	long long time=pc.GetTime();
	if(time<lasttime || initialization)
	{
		last_fap_frame_time=time;
		last_bap_frame_time=time;
		fapframes=0;
		fapframeswithoutnoise=0;
		bapframes=0;
		all_bapframes=0;
		initialization=false;
	}
	lasttime=time;
	SendIdle();
}

void BehaviorRealizer::react(SEMAINEMessage * m) throw(CMSException)
{
		
	SEMAINEXMLMessage * xm = (SEMAINEXMLMessage *)(m);
	if (xm == NULL) {
		throw MessageFormatException("expected XML message, got "+std::string(typeid(*xm).name()));
	}
	bmlID = m->getContentID();
	if (bmlID=="") 
	{
		std::ostringstream newID;
		newID << "_bml_orphan_" << orphan_cont;	
		bmlID = newID.str();
		orphan_cont+=1;
	}

	bmlcreationID = m->getContentCreationTime();

	bool isBML = xm->getDatatype() == "BML";

	//std::string content=xm->getText();

	//bmlengine.Execute("",(char *)content.c_str(),0,last_bap_frame,&last_fap_frame,true);

	if (isBML) 
	{	
		filenames.Phonemes_File="";
		std::string content=xm->getText();

		hasAudio = false;
		if (content.find("speech") != -1)
		//if(bmlengine.speech!=0)
			hasAudio = true;
		//std::cout << hasAudio << "\n";

		findCurrentFrame(fapframes);		

		if(bmlengine.Execute("",(char *)content.c_str(),0,last_bap_frame,&last_fap_frame,true)!=0)
		{
			if(inimanager.GetValueInt("REALTIME_GESTUREENGINE")==1)
				bmlengine.GetMPlanner()->GetCopyLastBAPFrame(last_bap_frame);
			//is an infinite signal?
			NoEndFaceSignals=false;
			NoEndFaceSignals=bmlengine.hasNoEndFaceSignals();

			faceengine=bmlengine.GetFaceEngine();
			fapframes=faceengine->GetAnimation();
			fapframeswithoutnoise=faceengine->GetAnimationWithoutNoise();
	

			mplanner=bmlengine.GetMPlanner();
			bapframes=mplanner->GetAnimation();			

			if (NoEndFaceSignals==true)
			{	
				//cut the animation
				int size=fapframes->size();
				fapframes->resize(floor((double)(size/2)));
				fapframeswithoutnoise->resize(floor((double)(size/2)));
				//if (bapframes->size()==size) bapframes->resize(floor((double)(size/2)));
			}
			
			//et=clock();
			//printf("%i\n", et-st);

			int start_time = pc.GetTime()+DELAY;

							
			/*if (bmlengine.speech!=0) 
			{				
				//sound_file_name.copy(iter->reference.c_str());													 				

				char str[20];
				std::string command;

				sprintf(str,"%d",start_time+ (int) (bmlengine.speech_delay*1000));
			
				command="PLAYWAVAT ";
				command+=str;
				
				std::ostringstream out;
				out << xm->getContentID() << "_command_"<< itercommand;				
				std::string contentID = out.str();
				std::cout << contentID << "\n";
				//PostString(command, "semaine.data.synthesis.lowlevel.command");
				commandSender->sendTextMessage(command, meta.getTime(), xm->getEventType(), contentID, xm->getContentCreationTime());
				itercommand = itercommand + 1;
			}*/

			SendFAPsBAPs(start_time,last_fap_frame_time,last_bap_frame_time, bmlID, 0.5, 0, 1000);	
			//last_fap_frame_time est le dernier fap envoye
			
			/*std::ostringstream out;
			out << xm->getContentID() << "_command_"<< itercommand << "\n";
			std::string contentID = out.str();
			std::cout << contentID << "\n";
			std::ostringstream oss;
			oss << "TIMESTART " << start_time << "\n";
			oss << "TIMEFAPEND " << last_fap_frame_time << "\n";
			oss << "TIMEBAPEND " << last_bap_frame_time << "\n";
			std::string timestartend;
			timestartend=oss.str();
			//std::cout << timestartend << "\n";
			commandSender->sendTextMessage(timestartend, meta.getTime(), xm->getEventType(), contentID, xm->getContentCreationTime());
			itercommand = 0;*/
			
		}
	}

	std::list<GretaLogger*>::iterator iterlog;

	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			log->debug(msg);
		if((*iterlog)->getLevel()=="info")
			log->info(msg);
		if((*iterlog)->getLevel()=="warn")
			log->warn(msg);
		if((*iterlog)->getLevel()=="error")
			log->error(msg);
	}
	listLog.clear();
}


int BehaviorRealizer::SendIdle()
{

	if (pc.GetTime() > last_fap_frame_time && idledone == 0) 
	{
		idledone = 1;
		time(&startD);
		//idle has no audio for the moment
		hasAudio = false;

		//if ((fapframes!=0)&&(fapframes->empty()==false)) lastframe=(*((fapframes->end())-1)).clone();
		//findCurrentFrame(fapframeswithoutnoise);

		//IdleMovements *idlemovements = new IdleMovements();
		//generate five seconds of continuos expressions
		//idlemovements->generate(last_fap_frame,5.0);
		if (fapframes==0)
			fapframes = new vector<FAPFrame>();
		if (fapframes!=0){
			fapframes->clear();
		
			FAPFrame f0, f1, f2, f3, f4, f5, f6, f7;
			f0.DeactivateAll();
			f1.DeactivateAll();
			f2.DeactivateAll();
			f3.DeactivateAll();
			f4.DeactivateAll();
			f5.DeactivateAll();
			f6.DeactivateAll();
			f7.DeactivateAll();
			f0.SetFAP(19,0);
			f0.SetFAP(20,0);
			fapframes->push_back(f0);
			f1.SetFAP(19,1024/3);
			f1.SetFAP(20,1024/3);
			fapframes->push_back(f1);
			f2.SetFAP(19,1024*2/3);
			f2.SetFAP(20,1024*2/3);
			fapframes->push_back(f2);	
			f3.SetFAP(19,1024);
			f3.SetFAP(20,1024);
			fapframes->push_back(f3);
			f4.SetFAP(19,1024);
			f4.SetFAP(20,1024);
			fapframes->push_back(f4);
			f5.SetFAP(19,1024*2/3);
			f5.SetFAP(20,1024*2/3);
			fapframes->push_back(f5);
			f6.SetFAP(19,1024/3);
			f6.SetFAP(20,1024/3);
			fapframes->push_back(f6);
			f7.SetFAP(19,0);
			f7.SetFAP(20,0);
			fapframes->push_back(f7);
		}

			

		//fapframes=idlemovements->GetAnimation();
		//fapframeswithoutnoise=idlemovements->GetAnimationWithoutNoise();

		//bapframes=new BAPFrameVector (0, &BAPFrame());		
		//if ((bapframes!=0) && (bapframes->empty()==false))  bapframes->clear();

		/*std::vector<FAPFrame>::iterator iter;
		for(iter=fapframes->begin();iter!=fapframes->end();iter++)  {
		printf( "lastframe 48 %i, 49 %i, 50 %i /n",lastframe.FAPs[48].value,lastframe.FAPs[49].value,lastframe.FAPs[50].value);

		BAPFrame* AnimationFrame = new BAPFrame();				
				bapframes->push_back(AnimationFrame);
			}
		*/

		int start_time = 0;

		if (last_fap_frame_time==0.0f)
			start_time=pc.GetTime() + DELAY;
		else
			start_time = last_fap_frame_time;
	
		//printf("start_time %d\n", start_time);

		if(bapframes!=0)
			bapframes->clear();

		randidle = randogen->GetRand01()*3;
		//std::cout << randidle << "\n";

		
		std::ostringstream out;
		out << "idle_" << iteridle;
		std::string ididle = out.str();

		SendFAPsBAPs(start_time,last_fap_frame_time,last_bap_frame_time, ididle, 0.5, 0, 1000);
		iteridle = iteridle + 1;
		itercommand = 0;		
	}

	if(idledone == 1)
	{
		time(&endD);
		difD = difftime(endD, startD);
	}	

	if (idledone==1 && difD >= 2+randidle)  
		idledone=0;
	
	return 1;
}


void BehaviorRealizer::findCurrentFrame(FAPFrameVector *frames)
{

	last_fap_frame.ActivateAndSetAllFAPsTo(0);

	//if something was displayed before find a start frame of new animation
	if	((frames!=0) && (!frames->empty())) 
	{
								
		//it can be last frame of the prevoius animation				
		last_fap_frame=(*((frames->end())-3)).clone();
	

		//add condition if the animation is not finished yet
		//if (last_frame_time > pc.GetTime() + DELAY) {
								
		//or it can be the frame that is displayed at the moment
		int  index = (int)(last_fap_frame_time - pc.GetTime() -DELAY)*FPS/1000;				
						
		if (((*frames).size()>index)&&(index > (int)0 ))  
			last_fap_frame=(*frames)[(*frames).size()-index].clone();															
		
	}
				
}


void BehaviorRealizer::SendFAPsBAPs(int start_frame_time,int &lastfapframetime,int &lastbapframetime, std::string idbml, double priority1, long long startat1, long long lifetime1)
{
	//int last_frame_number;
	std::string msg,label;
	int frameind;
	char tm[256];
	int i;
	int maxlength;
	clock_t faptimeat,baptimeat;


	//FAPwriter fw;

	//BAPwriter bw;
	//bw.WriteToFile(bapframes, "provabap.bap");

	if (start_frame_time==-1) faptimeat=pc.GetTime()+DELAY;
	else faptimeat=start_frame_time;

	baptimeat=faptimeat;

	sprintf(tm,"%d",pc.GetTime());

	/* DELETEDIFF
	label=tm;
	msg="DELETEDIFF ";
	msg+=label;
	msg+=" ";
	sprintf(tm,"%d",faptimeat-1);
	msg+=tm;
	PostString(msg, "semaine.data.synthesis.lowlevel.command");
	std::ostringstream out;
	out << idbml << "_command_"<< itercommand;
	std::string contentID = out.str();
	std::cout << contentID << "\n";
	//commandSender->sendTextMessage(msg, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, contentID, bmlcreationID);
	itercommand = itercommand + 1;
	//*/
	msg="";

	frameind=0;

	maxlength=0;

	if((bapframes!=0)&&(fapframes!=0))
	{
		if((*bapframes).size()>(*fapframes).size())
			maxlength=(*bapframes).size();
		else
			maxlength=(*fapframes).size();
	}
	else
	{
		if(bapframes!=0)
			maxlength=(*bapframes).size();
		if(fapframes!=0)
			maxlength=(*fapframes).size();
	}

	int nbpacksended = 0;
	//TODO : values passed in function parameters

	if(idbml.find("uap") != -1)
	{
		priority = 1.0;
		startat = 0;//now, as soon as possible
		lifetime = 3000; //in miliseconds
	}
	else if(idbml.find("lip") != -1)
	{
		priority = 0.5;
		startat = 0;//now, as soon as possible
		lifetime = 1500; //in miliseconds
	}
	else if(idbml.find("idle") != -1)
	{
		priority = 0;
		startat = 0;//now, as soon as possible
		lifetime = 500; //in miliseconds
	}
	else
	{
		priority = 0.5;
		startat = 0;//now, as soon as possible
		lifetime = 1500; //in miliseconds
	}
	//\TODO
	while(frameind<maxlength)
	{
		if(nbpacksended==1){
			std::ostringstream outID;
			outID << idbml << "_command_" << itercommand++;
			std::string contentID = outID.str();
			std::ostringstream out;
			out << "STARTAT " << startat << "\nLIFETIME " << lifetime << "\nPRIORITY " << priority << "\nHASAUDIO " << hasAudio << "\n";
			std::string commandContent = out.str();
			commandSender->sendTextMessage(commandContent, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, contentID, bmlcreationID);
		}
		if(fapframes!=0 && frameind<fapframes->size())
		{
			msg="";

			msg+=label+"\n";

			//printf("label: %s\n", tm);

			for(i=0;i<MAXFRAMESENT;i++)
			{
				if ((i+frameind)<(*fapframes).size())
				{
					//(*fapframes)[i+frameind].framenumber=faptimeat;
					//for queueing
					(*fapframes)[i+frameind].framenumber=i+frameind;
					msg+=(*fapframes)[i+frameind].toString();
					//last_frame_number=(i+frameind);
				}
				else
					break;
				faptimeat+=40;
			}
			//PostString(msg, "semaine.data.synthesis.lowlevel.FAP");
			std::ostringstream out;
			out << idbml << "_fap_"<< iterfap;
			std::string contentID = out.str();
			//std::cout << contentID << "\n";
			fapSender->sendTextMessage(msg, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, contentID, bmlcreationID);
			iterfap = iterfap + 1;
		}

		if(bapframes!=0 && frameind<bapframes->size())
		{
			msg="";

			msg+=label+"\n";

			for(i=0;i<MAXFRAMESENT;i++)
			{

				if ((i+frameind)<(*bapframes).size())
				{
					//(*bapframes)[i+frameind]->SetFrameNumber(baptimeat);
					//for queueing
					(*bapframes)[i+frameind]->SetFrameNumber(i+frameind);
					msg+=(*bapframes)[i+frameind]->WriteBAP();
				}
				else
					break;
				baptimeat+=40;
			}

			//PostString(msg, "semaine.data.synthesis.lowlevel.BAP");
			std::ostringstream out;
			out << idbml << "_bap_"<< iterbap;
			std::string contentID = out.str();
			//std::cout << contentID << "\n";
			bapSender->sendTextMessage(msg, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, contentID, bmlcreationID);
			iterbap = iterbap + 1;
		}

		frameind+=MAXFRAMESENT;
		++nbpacksended;
	}

	//we need to repete this part of the code here because we may have less than 1 pack. 
	//in this case, we don't send the start info before
	if(nbpacksended<=1){
		std::ostringstream outID;
		outID << idbml << "_command_" << itercommand++;
		std::string contentID = outID.str();
		std::ostringstream out;
		out << "STARTAT " << startat << "\nLIFETIME " << lifetime << "\nPRIORITY " << priority << "\nHASAUDIO " << hasAudio << "\n";
		std::string commandContent = out.str();
		commandSender->sendTextMessage(commandContent, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, contentID, bmlcreationID);
	}

	//std::cout << frameind << " " << (*fapframes).size();
	//return last_frame_number/FPS*1000+faptimeat;
	lastfapframetime=faptimeat-40;
	lastbapframetime=baptimeat-40;
	iterbap = 0;
	iterfap = 0;

 
}

} // namespace greta
} // namespace components
} // namespace semaine
