//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "cv.h"
#include "highgui.h"
#include "math.h"
#include "cvcam.h"

#include "GazeDetector.h"
#include "GazeDetectorPsydule.h"
#include "IniManager.h"

IniManager inimanager;
GazeDetectorPsydule* gdp;
SteliosGazeDetector* sgd;

void main()
{
	sgd = new SteliosGazeDetector();
	
	//connect to Psyclone
	std::string host;
	int port;

	inimanager.ReadIniFile("greta.ini");
	host=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	port=inimanager.GetValueInt("PSYCLONE_PORT");

	printf("\nIni file specifies Psyclone server %s: %d", host.c_str(), port);


//STELIOS: I load some training results at the beginning

	FILE *hor_coord_p,*ver_coord_p;
	FILE *hor_coord_pL,*ver_coord_pL;
	FILE *hor_coord_pM,*ver_coord_pM;

//---For each characteristic, read the x and y coordinate maps of the mean vector fields. 
//---The vector fields correspond to the left and right eye areas and the mouth
//---the represent the vectors of each pixel, pointing to the closest edge. The code
//---reads the mean x and y coordinates of such vector fields, extracted from a set of
//---training images
//char face_no[100];



	float** hor_coord=fmatrix(0,26-1,0,26-1);
	float** ver_coord=fmatrix(0,26-1,0,26-1);
	hor_coord_p=fopen("vector_field/VFREx.txt","r");
	ver_coord_p=fopen("vector_field/VFREy.txt","r");
	float** hor_coordM=fmatrix(0,12,0,35);
	float** ver_coordM=fmatrix(0,WIND1-1,0,WIND2-1);
	hor_coord_pM=fopen("vector_field/VFMx.txt","r");
	ver_coord_pM=fopen("vector_field/VFMy.txt","r");
	float** hor_coordL=fmatrix(0,26-1,0,26-1);
	float** ver_coordL=fmatrix(0,26-1,0,26-1);
	hor_coord_pL=fopen("vector_field/VFLEx.txt","r");
	ver_coord_pL=fopen("vector_field/VFLEy.txt","r");

int i,j;

//-----------------------------------------------------

	for (i=0;i<25;i++)
	{
		for (j=0;j<25;j++)
		{
			fscanf(ver_coord_p,"%f",&ver_coord[i][j]);
			fscanf(ver_coord_pL,"%f",&ver_coordL[i][j]);
			fscanf(hor_coord_p,"%f",&hor_coord[i][j]);
			fscanf(hor_coord_pL,"%f",&hor_coordL[i][j]);
		}
	}


//-----------------------------------------------------

	for (i=0;i<13-1;i++)
	{
		for (j=0;j<36-1;j++)
		{
		fscanf(ver_coord_pM,"%f",&ver_coordM[i][j]);
		fscanf(hor_coord_pM,"%f",&hor_coordM[i][j]);
		}
	}



  	float* poza=fvector(0,1000);
	float* dist=fvector(0,1000);
	float* gazing=fvector(0,1000);
	float* state=fvector(0,1000);
	int interval=0;
	int fr=0, count=0, fps=25;
	float sumpose, sumdist, sumgaze;


	//new object
	gdp = new GazeDetectorPsydule("Greta.GazeDetectorPsydule",host,port);

	long timeNow = clock();


	sgd->initialiseNewCapture();

	FrameResults frameResults;

	for (int i = 0; i < 1000000; i++)
{
for (int fr_c = 0; fr_c < 100; fr_c++)
	{
	
		frameResults = sgd->processNewFrame( hor_coord,ver_coord, hor_coordL, 
										      ver_coordL, hor_coordM,ver_coordM,fr_c);


		//poza[count]=sqrt(  pow(frameResults.ppx,2) + pow(frameResults.ppy,2)   );
		//gazing[count]=fabs(frameResults.px);//sqrt(  pow(frameResults.px,2) + pow(frameResults.px,2)   );
		//dist[count]=frameResults.dist_monitor;

		if (frameResults.valid)
		{
			//dirty trick
			if (frameResults.ppx>0.5) frameResults.ppx=0.49;
			if (frameResults.ppx<-0.5) frameResults.ppx=-0.49;
			if (frameResults.ppy>0.5) frameResults.ppy=0.49;
			if (frameResults.ppy<-0.5) frameResults.ppy=-0.49;

			gdp->sendGazeHeadDirMessage(frameResults.ppx,frameResults.ppy,frameResults.dist_monitor);
			//printf("\npx=%3.3f\tpy=%3.3f\tppx=%3.3f\tppy=%3.3f\tdistance=%3.3f",frameResults.px,frameResults.py,frameResults.ppx,frameResults.ppy,frameResults.dist_monitor);
			printf("\nppx=%3.5f\tppy=%3.5f\tdistance=%3.5f",frameResults.ppx,frameResults.ppy,frameResults.dist_monitor);
		}
		else printf("\nInvalid frameresults");
	}
	
	}
	
	getchar();

	sgd->finishCapture();




	/*
	//to a sample OpenCV capture
	int key = 0;
	CvCapture* capture = NULL;
	capture = cvCaptureFromCAM( -1 );

	if( !capture ) {
		fprintf( stderr, "ERROR: capture is NULL \n" );
		while(1);
		return;
	}

	//create the video output window
	cvNamedWindow( "output", CV_WINDOW_AUTOSIZE );
	IplImage* frame_or = cvQueryFrame( capture );

	while(1)
	{
		if( !cvGrabFrame( capture )) break;
		frame_or = cvRetrieveFrame( capture );
		if( !frame_or )	break;		

		cvShowImage( "output", frame_or );	

		//have to insert this, or else laptop camera capture will not work
		key=cvWaitKey(20); 

		//if its time for an update, send it
		if ((clock() - timeNow) > (CLOCKS_PER_SEC * 0.5))
		{
			timeNow = clock();
			//send random gaze coordinates
			gdp->sendGazeCoordsMessage(rand() %	1971 - 50, rand() %	1251 - 50);
		}
	}
	delete gdp;
	*/
}