//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// WoZWindow.cpp: implementation of the WoZWindow class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <semaine/cms/exceptions/MessageFormatException.h>
#include <cstdlib>
#include <sstream>
#include <decaf/lang/System.h>

#include "WoZWindow.h"
#include "WoZSender.h"
#include "FL/Fl.H"


//WoZWindow::WoZWindow():Fl_Window(200,200,600,330,"Wizard of Oz")
WoZWindow::WoZWindow():Fl_Window(200,200,300,280,"Wizard of Oz")
{
	int i;
	std::string host;
	int port;
	char value[256];

	ws = new WoZSender("WoZSender", false, false);

	sendPoppy=new Fl_Button(40,20,100,100,"POPPY");
	sendSpyke=new Fl_Button(40,140,100,100,"SPYKE");
	sendPrudence=new Fl_Button(160,20,100,100,"PRUDENCE");
	sendObadiah=new Fl_Button(160,140,100,100,"OBADIAH");

	sendPositive=new Fl_Button(300,20,100,100,"POSITIVE");
	sendNegative=new Fl_Button(300,140,100,100,"NEGATIVE");

	//sendObadiah->hide();
	//sendPrudence->hide();

	Positive=new AgentState(LoadAgentState("listener/Positive.xml"));
	Negative=new AgentState(LoadAgentState("listener/Negative.xml"));
	Poppy=new AgentState(LoadAgentState("listener/Poppy.xml"));
	Spyke=new AgentState(LoadAgentState("listener/Spyke.xml"));
	Prudence=new AgentState(LoadAgentState("listener/Prudence.xml"));
	Obadiah=new AgentState(LoadAgentState("listener/Obadiah.xml"));
	
	sendturn=new Fl_Button(450,20,100,50,"stop");
	sendSmile=new Fl_Button(460/*+i*100*/,110,80,30,"smile");
	sendBC=new Fl_Button(460/*+i*100*/,160,80,30,"BC");

	sendEvent=new Fl_Button(460,210,80,30,"event");

}

WoZWindow::~WoZWindow()
{
}

void WoZWindow::draw()
{
	Fl_Window::draw();
}

int WoZWindow::handle(int e)
{
	int i;

	if(Fl::event_button()==FL_LEFT_MOUSE)
	{
		if(e==FL_PUSH)
		{
			if(Fl::event_inside(sendBC))
			{
			}
			if(Fl::event_inside(sendPositive))
			{
				SendAgentState("Positive");
			}
			if(Fl::event_inside(sendNegative))
			{
				SendAgentState("Negative");
			}
			if(Fl::event_inside(sendturn))
			{
				SendAgentTurn((std::string)sendturn->label());
				if(sendturn->label()=="stop")
					sendturn->label("start");
				else
					sendturn->label("stop");
				this->redraw();
			}
			if(Fl::event_inside(sendPoppy))
			{
				SendAgentState("Poppy");
			}
			if(Fl::event_inside(sendSpyke))
			{
				SendAgentState("Spyke");
			}
			
			if(Fl::event_inside(sendPrudence))
			{
				SendAgentState("Prudence");
			}
			if(Fl::event_inside(sendObadiah))
			{
				SendAgentState("Obadiah");
			}
		}
	}

	return Fl_Window::handle(e);
}


std::string WoZWindow::LoadAgentState(std::string filepath)
{
	std::string state;
	std::string line;
	if(filepath=="")
	{
		printf("WoZ: error parsing %s\n",filepath.c_str());
		return "";
	}
	std::ifstream inputfile(filepath.c_str());
	if(inputfile.is_open())
	{
		while((inputfile.rdstate()&std::ios::eofbit)==0)
		{
			std::getline(inputfile,line,'\n');
			state=state+line+"\n";
		}
		inputfile.close();
	}
	else 
		return "";
	return state;
}

void WoZWindow::SendAgentTurn(std::string inturn)
{
	std::string turn;
	if(inturn=="start")
		turn="agent_turn=listener";
	else
		turn="agent_turn=speaker";
	ws->getTurnSender()->sendTextMessage(turn, ws->setTime());
}

void WoZWindow::SendAgentState(std::string SAL)
{
	int i;
	char value[255];
	std::string opname;
	std::string emotion;
	std::string name="";
	AgentState *agentstate;

	if(SAL=="Positive")
	{
		name="POSITIVE";
		emotion="joy";
		agentstate=Poppy;
	}
	if(SAL=="Negative")
	{
		name="NEGATIVE";
		emotion="anger";
		agentstate=Spyke;
	}
	if(SAL=="Poppy")
	{
		name="POPPY";
		emotion="joy";
		agentstate=Poppy;
	}
	if(SAL=="Prudence")
	{
		name="PRUDENCE";
		emotion="neutral";
		agentstate=Prudence;
	}
	if(SAL=="Obadiah")
	{
		name="OBADIAH";
		emotion="sadness";
		agentstate=Obadiah;
	}
	if(SAL=="Spyke")
	{
		name="SPYKE";
		emotion="anger";
		agentstate=Spyke;
	}

	name="agent_identity.agent.name=" + name;

	std::string msg=agentstate->internalstate.PrintXML();

	ws->getStateSender()->sendTextMessage(msg, ws->setTime());
	ws->getNameSender()->sendTextMessage(name, ws->setTime());
}

