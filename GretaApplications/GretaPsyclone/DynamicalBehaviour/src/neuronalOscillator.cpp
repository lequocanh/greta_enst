#include "neuronalOscillator.h"

neuronalOscillator::neuronalOscillator()
{
	neuronOne=1;
	neuronTwo=-1;
	inhibOsc=0;
	freqOsc=(float)0.04; // period for freqOsc = 0.04 -> 20 sec (0.08 -> 10 sec)
}

neuronalOscillator::neuronalOscillator(float freq)
{
	neuronOne=1;
	neuronTwo=0;
//	neuronThree=-1;
	inhibOsc=0;
	freqOsc=freq;
}

neuronalOscillator::~neuronalOscillator(void)
{
}

void neuronalOscillator::updateOscillator()
{
     float tmp=neuronTwo;
//	 neuronThree = neuronThree + freqOsc*neuronTwo;
	 neuronTwo = neuronTwo + freqOsc*neuronOne; //- freqOsc*neuronThree;
	 neuronOne = neuronOne - freqOsc*tmp - inhibOsc;
	 
//	 if (neuronThree>1)
//		 neuronThree=1;
//	 else if (neuronThree<-1)
//		 neuronThree=-1;
	 if (neuronTwo>1)
		 neuronTwo=1;
	 else if (neuronTwo<-1)
		 neuronTwo=-1;
	 if (neuronOne>1)
		 neuronOne=1;
	 else if (neuronOne<-1)
		 neuronOne=-1; 
}

float neuronalOscillator::getActivation(void)
{
	return neuronOne;
}

float neuronalOscillator::getInhib(void)
{
	return inhibOsc;
}

void neuronalOscillator::setInhib(float inhib)
{
	inhibOsc=inhib;
}

float neuronalOscillator::getFreq()
{
	return freqOsc;
}

void neuronalOscillator::setFreq(float freq)
{
	freqOsc=freq;
}