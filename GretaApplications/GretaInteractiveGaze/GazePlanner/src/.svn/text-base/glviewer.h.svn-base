// Shared-Attention system
// Christopher Peters


#ifndef _GL_VIEWER_H
#define _GL_VIEWER_H

#include <Fl/Fl.h>
#include <FL/Gl.h>
#include <Gl/Glu.h>
#include <FL/Fl_Gl_Window.h>
#include <stdio.h>
#include <math.h>

#include "scene.h"
#include "fixation.h"
#include "sharedattention.h"

class GLViewer : public Fl_Gl_Window {
  public:
	GLViewer(int X, int Y, int W, int H, const char *L);

	int getSelectedID(void){return this->selectedID;}
	std::string *getSelectedIDObjectName(void);

	void enableCalibrationMode(void);

	bool testMode;			//test mode connects the mouse to the gaze position

	GLfloat light_ambient_0[4];
	GLfloat light_diffuse_0[4];
	GLfloat light_specular_0[4];
	GLfloat light_position_0[4];


	long lastDisplay;	//last time we checked for a message
	long displayFrequencySec;	//check for a waiting message every x

	void idle(void);
	void convertGazeDirToScreenCoords(float xDir, float yDir);

	Scene *theScene;

private:

    void draw();
    int handle(int event);
    void InitializeGL(void);
	void drawBasis(void);

	void drawFixations(void);

	void drawCrosshair(void);
	void drawArrow(float rotation);

	int pickObject(int screenX, int screenY);	//use opengl pick to select an object
	void screenToWorldspaceCoords(const int x, const int y, double &pos3Dx, double &pos3Dy, double &pos3Dz);

	void applyProjection(void);

	float nearPlane;

	GLint viewPort[4];
	GLdouble model_view[16];
	GLdouble projection[16];

	int selectedID;
	int selectedSubID;

	long updateSec;

	void createCurrentFixation(int xScreen, int yScreen, bool outsideLeft = false, bool outsideRight = false, bool outsideTop = false, bool outsideBottom = false);
	FixationList *fixationList;
	FixationEntry *currentFixation;

	void drawCursor(int cursorPos);

	void drawAttentionGraph(void);
	void drawNormalisedAttentionGraph(void);
};

#endif