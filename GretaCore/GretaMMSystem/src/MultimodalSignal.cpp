//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <math.h>
#include "MultimodalSignal.h"
#include "RandomGen.h"
#include "NewExpressionContainer.h"
#include "NewConstraintsContainer.h"
#include "ConsNode.h"

#include "DataContainer.h"
#include "MultimodalEmotionConstraint.h"

//#define RDEBUG

extern RandomGen *randomgen;
extern DataContainer *datacontainer;

/**@#-*/
using namespace MMSystemSpace;
/**@#+*/

MultimodalSignal::MultimodalSignal(void)
{
	preference=0;
}

MultimodalSignal::MultimodalSignal(const MultimodalSignal &rhs)
{
	std::vector<Signal>::const_iterator iter;
	for(iter=rhs.begin();iter!=rhs.end();iter++)
	{
		Signal s(*iter);
		this->push_back(s);
	}
	preference=rhs.preference;
}


MultimodalSignal::~MultimodalSignal(void)
{
}

bool MultimodalSignal::UseModality(std::string modalityname)
{
	bool found;
	std::vector<Signal>::iterator iter;
	found=false;
	for(iter=this->begin();iter!=this->end();iter++)
		if((*iter).modality==modalityname)
		{
			found=true;
			break;
		}
		return found;
}

float MultimodalSignal::ComputePreference(Dynamicline *dl)
{
	float p;
	std::vector<Signal>::iterator iter;
	p=0.0f;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		//if(dl->GetAttrValue((*iter).modality+".preference.value")>p)
		//	p=dl->GetAttrValue((*iter).modality+".preference.value");
		p=p+dl->GetAttrValue((*iter).modality+".preference.value");
	}
	this->preference=p;
	return p;
}

std::list<std::string> MultimodalSignal::UsedModalities()
{
	std::list<std::string> usedmodalities;
	std::vector<Signal>::iterator iter;
	usedmodalities.clear();
	for(iter=this->begin();iter!=this->end();iter++)
	{
		if((*iter).concretized)
			usedmodalities.push_back((*iter).modality);
	}
	return usedmodalities;
}


void MultimodalSignal::AddSubElements(XMLGenericTree * element, int *uid)
{
	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		if((*iter).concretized)
		{
			(*iter).AddSubElement(element, uid);
		}
	}

}

std::string MultimodalSignal::GetBML(int *uid)
{
	std::string bml;

	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		if((*iter).concretized)
			bml=bml+(*iter).GetBML(uid);
	}

	return bml;
}

void MultimodalSignal::Temporize(float start,float duration)
{
	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		(*iter).start=start;
		(*iter).strokes.clear();
		(*iter).duration=duration;
		(*iter).concretized=true;
	}
}

void MultimodalSignal::Temporize(float start,float stroke,float duration)
{
	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		(*iter).start=start;
		(*iter).duration=duration;
		(*iter).strokes.clear();
		(*iter).strokes.push_back(stroke);
		(*iter).concretized=true;
	}
}

void MultimodalSignal::Temporize(float start,std::vector<float> strokes,float duration)
{
	int strokesnum;
	strokesnum=(int)strokes.size();

	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		(*iter).strokes.clear();
		(*iter).start=start;
		(*iter).duration=duration;
		if(strokes.empty())
		{
			(*iter).concretized=true;
			continue;
		}
		if((*iter).parameters.find("REP.value")==(*iter).parameters.end())
		{
			(*iter).strokes=strokes;
			(*iter).concretized=true;
			continue;
		}
		float rep=(*iter).parameters["REP.value"].GetValue();
		rep=(rep+1.0f)/2.0f;
		if(rep==0)
		{
			//(*iter).concretized=false;
			(*iter).strokes.clear();
			continue;
		}

		if((rep<=1.0)&&(rep>=0.86))
		{
			(*iter).strokes=strokes;
			(*iter).concretized=true;
			continue;
		}

		float strokeshighestnum=strokes.size();

		strokeshighestnum=rep*strokeshighestnum;

		if(strokeshighestnum<1.0f)
		{
			(*iter).strokes.push_back(strokes[strokes.size()/2]);
			(*iter).concretized=true;
			continue;
		}

		float strokefrequency;
		strokefrequency=duration/strokeshighestnum;
		float time;
		time=0;
		std::vector<float>::iterator stroke;
		for(stroke=strokes.begin();stroke!=strokes.end();stroke++)
		{
			if((*stroke)>time)
			{
				(*iter).strokes.push_back(float(*stroke));
				time+=strokefrequency;
			}
		}
		(*iter).concretized=true;
	}
}

void MultimodalSignal::AssignNameToSignals(std::string name)
{
	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		(*iter).id=name;
	}
}

void MultimodalSignal::AssignParameters(Baseline *bl)
{
	std::vector<Signal>::iterator iter;
	//printf("dynamicline:\n");
	//bl->Print();

	for(iter=this->begin();iter!=this->end();iter++)
	{
		std::map<std::string,EngineParameterSet>::iterator iter2;
		std::string modname;
		for(iter2=bl->begin();iter2!=bl->end();iter2++)
		{
			modname=(*iter2).second.name.substr(0,(*iter2).second.name.find_first_of("."));
			if(modname==(*iter).modality)
			{
				EngineParameters::iterator iter3;
				for(iter3=(*iter2).second.mp.begin();iter3!=(*iter2).second.mp.end();iter3++)
				{
					EngineParameter *mp;
					mp=new EngineParameter();
					mp->name=(*iter3).second.name;
					mp->SetValue((*iter3).second.GetValue());
					(*iter).parameters[mp->name]=(*mp);
				}
			}
		}
	}
}

bool MultimodalSignal::IsActivable(Dynamicline *dl,float importance)
{
	float oac;
	float *oacp;
	std::vector<Signal>::iterator iter;

	if(this->empty())
		return false;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		oacp=(*dl)[(*iter).modality].GetAttrAddress("OAC.value");
		if(oacp==0)
			return false;

		oac=*oacp;

		//oac=oac+((randomgen->GetRand01()*0.3f)-0.15f);

		if(oac<randomgen->GetRand01())
			return false;

		//if((1-importance)>(oac))
		//	return false;
	}
	return true;
}


void MultimodalSignal::SaveCSV(float time,FILE *preferencefile,FILE *gesturefile,FILE *torsofile,FILE *headfile,FILE *facefile,FILE *gazefile)
{
	FILE *f;
	float gestp,torsp,facep,headp,gazep;
	char store[255];
	int i;

	gestp=-999;
	torsp=-999;
	facep=-999;
	headp=-999;
	gazep=-999;

	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		if((*iter).modality=="gesture")
		{
			f=gesturefile;
			gestp=(*iter).parameters["preference.value"].GetValue();
		}
		if((*iter).modality=="torso")
		{
			f=torsofile;
			torsp=(*iter).parameters["preference.value"].GetValue();
		}
		if((*iter).modality=="head")
		{
			f=headfile;
			headp=(*iter).parameters["preference.value"].GetValue();
		}
		if((*iter).modality=="face")
		{
			f=facefile;
			facep=(*iter).parameters["preference.value"].GetValue();
		}
		if((*iter).modality=="gaze")
		{
			f=gazefile;
			gazep=(*iter).parameters["preference.value"].GetValue();
		}

		sprintf_s(store,255,"%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
			(*iter).start,
			(*iter).parameters["preference.value"].GetValue(),
			(*iter).parameters["OAC.value"].GetValue(),
			(*iter).parameters["SPC.value"].GetValue(),
			(*iter).parameters["TMP.value"].GetValue(),
			(*iter).parameters["FLD.value"].GetValue(),
			(*iter).parameters["PWR.value"].GetValue(),
			(*iter).parameters["REP.value"].GetValue());

		fprintf(f,"%s",store);
		sprintf_s(store,255,"%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
			(*iter).start+(*iter).duration,
			(*iter).parameters["preference.value"].GetValue(),
			(*iter).parameters["OAC.value"].GetValue(),
			(*iter).parameters["SPC.value"].GetValue(),
			(*iter).parameters["TMP.value"].GetValue(),
			(*iter).parameters["FLD.value"].GetValue(),
			(*iter).parameters["PWR.value"].GetValue(),
			(*iter).parameters["REP.value"].GetValue());

		fprintf(f,"%s",store);
	}

	sprintf_s(store,255,"%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
		(*iter).start,gestp,torsp,headp,facep,gazep);
	for(i=0;i<(int)strlen(store);i++)
	{
		if(store[i]=='.')
		{
			store[i]=',';
		}
	}
	fprintf(preferencefile,"%s",store);
	sprintf_s(store,255,"%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",
		(*iter).start+(*iter).duration,gestp,torsp,headp,facep,gazep);
	for(i=0;i<(int)strlen(store);i++)
	{
		if(store[i]=='.')
		{
			store[i]=',';
		}
	}
	fprintf(preferencefile,"%s",store);
}

void MultimodalSignal::ShuffleShapes()
{
	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		(*iter).ShuffleShape();
	}
}

void MultimodalSignal::AssignStrokesFromCommAct(MMSystemSpace::CommunicativeIntention *ca)
{
	MMSystemSpace::CommunicativeIntention *contextaccent;

	contextaccent=ca->GetPitchAccentFromContext();

	if(contextaccent!=0)
	{
		float stroke;
		stroke=contextaccent->start-ca->start+(contextaccent->duration*0.40);
		this->Temporize(ca->start,stroke,ca->duration);
	}
	else
	{
		this->Temporize(ca->start,ca->duration);
	}
}

void MultimodalSignal::AssignStrokesFromStressVector(MMSystemSpace::CommunicativeIntention *ca,std::vector<float> *stressvector)
{
	std::vector<float> strokes;
	std::vector<float>::iterator iter;
	float stresstime;
	float prevstresstime=ca->start+0.3;
	for(iter=stressvector->begin();iter!=stressvector->end();iter++)
	{
		stresstime=(*iter)-0.2;
		if(stresstime<ca->start)
			continue;
		if(stresstime>(ca->start+ca->duration))
			continue;

		if((stresstime-prevstresstime)>0.2)
		{
			strokes.push_back(stresstime-ca->start);
			prevstresstime=stresstime;
		}
	}
	if(strokes.empty()==false)
		this->Temporize(ca->start,strokes,ca->duration);
	else
		this->Temporize(ca->start,ca->duration);
}


void MultimodalSignal::AddIntensity(float intensity1)
{
	std::vector<Signal>::iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		(*iter).SetIntensity(intensity1);
	}

}

void MultimodalSignal::addTimeConstraints(std::string emotion, int enter)
{
	if (enter>5) {
		this->clear();
		printf("Sorry, it was not possible to generate MS-expression");
		return;
	}

	/*
	the signals have only the reference defined : verify

	if((*iter).reference!="")
	{
	classname=(*iter).reference.substr(0,(*iter).reference.find_first_of("="));
	instancename=(*iter).reference.substr((*iter).reference.find_first_of("=")+1);
	}

	*/

	//ask for the data from datacontainer
	//NewExpressionContainer *newexpressions = datacontainer->getNewExpressionContainer();	

	NewConstraintsContainer *constraintscontainer = datacontainer->getContraints();

	MultimodalEmotionConstraint *mec = constraintscontainer->getMultimodalEmotionConstraint(emotion);

	//if there is no cons for this emotion
	//nothing to do MultimodalSignal is not changed

	if (mec==NULL) {
		printf("No constraints defined for %s \n", emotion.c_str());
		return; 
	}

	if (mec->getNewConstraints()==NULL) {
		printf("No constraints defined for %s \n", emotion.c_str());
		return; 
	}

	//else clone this
	MultimodalSignal clone = this->clone();

	//if no success
	MultimodalSignal emergency = this->clone();

	int counter3=0;
	std::vector<Signal>::iterator iterxxx2;

	for(iterxxx2=emergency.begin();iterxxx2!=emergency.end();iterxxx2++)
	{ 
		counter3++;
	}

	//printf("resultatbeg %i", counter3);

	//cancel the list of signals of this
	this->clear();

	//each signal should have time duration at the probability of the occurence, and of repetition
	//if the signal is not present in exe it should have additional constraints defined in te contraint file

	std::vector<Signal>::iterator iter;

	//decide the nunber of n-units on the base of baseline and time of commact

	float start = 0;
	float stop = 0;

	//what about the timemarkers????
	//find time constraints	
	for(iter=clone.begin();iter!=clone.end();iter++)
	{
		if (start==0) start =(*iter).start;
		else if (start> (*iter).start ) start =(*iter).start;
		if (stop < (*iter).start + (*iter).duration ) stop =(*iter).start + (*iter).duration;		
	}

	float time = stop - start;

	//at the moment we use n = 1 sec
	int n = floor(time);

	//turn number
	int turn = 0;

	//time of one turn - at the moment it is about 1 sec
	float time_unit= time/n;

	// cancel the signals that are shorter than one unit and create probability list

	//to do

	//create vectors of probabilities
	//+1 becaese of index from 1 to clone.size

	//if an empty expression is not used 
	int probabilities_size = (int)clone.size()+1;

	std::map<std::string,float> probabilities_start;
	std::map<std::string, float> probabilities_end;
	std::map<std::string, float> current_probabilities;

	int index1 = 1;


	//to do: check if signals are defined, if not out from the list
	//code crashes here if there is a label in behavior or constraintset that is not defined

	std::vector<Signal>::iterator iterxxx;

	for(iterxxx=clone.begin();iterxxx!=clone.end();iterxxx++)
	{
		std::string temp = iterxxx->reference;		

		float max_duration=constraintscontainer->getMax_duration(emotion,temp);

		//check it
		if (max_duration<time_unit) 
		{
			//check the index

			//TO DO : erase command crashes the system

			//clone.erase(iterxxx);

			//temporal solution
			//put 0 for the probability

			std::string temp_name= findName(temp);			

			probabilities_start[temp_name] = 0.0;

			probabilities_end[temp_name] = 0.0;

			current_probabilities[temp_name] =  0.0;


		}
		else 
		{

			//update the start and end probabilities

			std::string temp_name= findName(temp);			

			probabilities_start[temp_name] = constraintscontainer->getProbability_start(emotion,temp);

			probabilities_end[temp_name] = constraintscontainer->getProbability_end(emotion,temp);

			current_probabilities[temp_name] =  constraintscontainer->getProbability_start(emotion,temp);

			//attention: some probabilities [x][y] are 0 = we cannot not use corresponding signal x
		} //end of else

	} //end of for


	//clone is a full list of signals at disposal
	//temporal is like clone but with some signals cancelled (=avoid infinite loop)

	//generate a  temporal list of signals
	MultimodalSignal temporal=clone.clone();

	//start of the algortithm
	int singals_in_turn=0;

	Signal startsignal;
	startsignal.reference="faceexp=neutral";
	//startsignal.value="neutral";
	startsignal.type="face";
	//startsignal.function="affect";

	//add something at the begining
	startsignal.start=0;
	startsignal.duration = 1;
	startsignal.id="neutral";
	startsignal.concretized=true;	
	startsignal.modality="face";

	//not needed
	startsignal.strokes.push_back( 0.1 );								

	this->push_back(startsignal);

	//turn++; 

	float last_added_time=0;

	while (turn<n) 
	{

		bool success=false;

		//A. choose the signal (consider the probability of occurence)

		//choose from temporal

		if (!(temporal.empty())) {

			float result=randomgen->GetRand01();

			MultimodalSignal short_list;

			for(iter=temporal.begin();iter!=temporal.end();iter++)	
			{ //for all signals in temporal

				//printf("temP %s \n", (*iter).reference.c_str() );
				//printf("prob %f '\n", current_probabilities[findName(iter->reference)] );

				//result is a random value
				if (current_probabilities[findName(iter->reference)] > result ) 
				{
					///printf(" added %s \n", (*iter) .reference.c_str());
					short_list.push_back( *((*iter).clone() ) );
				}//end of if
			}//end of for all temporals

			if (short_list.size()==0) 
			{
				//all is possible in next turn

				temporal=clone.clone();

				turn++;

				//refresh probability current!!!

				//update the probabilities next turn probabilities
				float factor =  (float) ( (float)  (turn+1)   / (float) n ) ;

				if ( (factor < 1.0f) && (factor >= 0.0f) )
				{	
					for(iter=clone.begin();iter!=clone.end();iter++)	//for all signals in clone 
					{	
						std::string temp_name = findName(iter->reference);

						//three cases:
						//start=end
						if (probabilities_start[temp_name]==probabilities_end[temp_name])
							current_probabilities[temp_name] = probabilities_start[temp_name] ;
						//start < end
						if (probabilities_start[temp_name] < probabilities_end[temp_name]) 
						{						
							float distance = probabilities_end[temp_name]-probabilities_start[temp_name] ;
							current_probabilities[temp_name] = probabilities_start[temp_name] + distance * factor ;
						}
						//start > end										
						if (probabilities_start[temp_name] > probabilities_end[temp_name])
						{						
							float distance = probabilities_start[temp_name] - probabilities_end[temp_name];
							current_probabilities[temp_name] = probabilities_start[temp_name] - distance * factor ;
						}

						if (current_probabilities[temp_name]>1.0f) current_probabilities[temp_name]=1;
						if (current_probabilities[temp_name]<0.0f) current_probabilities[temp_name]=0;


					}//end of for

				}//end of if factor


			}
			else 
			{

				float resul=randomgen->GetRand01();

				Signal *chosen;

				for (int i=1; i<=(int)(short_list.size()); i++) 
				{

					float lower_interval = ( (float) 1/short_list.size() ) * (float) (i-1) ;
					float upper_interval = ( (float) 1/short_list.size() ) * (float) (i) ;

					if  ( ( lower_interval  < resul) && (resul < upper_interval ) ) 
					{									
						chosen = new Signal ( (Signal) short_list[i-1]); 

						i=(int)short_list.size()+1; //exit from for
					}
				} //end of for

				float result=randomgen->GetRand01();

				float begin_time = (turn + result) * time_unit;

				//check the contraints with "this" (does the signal can be the first one or it has to preceded by the other signal)

				//if the signal starts before the other of the same id is finished then the second one is illegal
				bool goodornot=true;
				std::vector<Signal>::const_iterator iterq;

				for(iterq=this->begin();iterq!=this->end();iterq++)
				{	
					if ( ( (*iterq).reference==chosen->reference) && ( ((*iterq).start + (*iterq).duration)  > begin_time ) ) 
						goodornot=false;

				}//end for

#ifdef RDEBUG
				printf("\n Candidate's id: %s", chosen->reference.c_str());
#endif

				// no time back
				// no continuation of existing signal
				// cons are satisfied

				if ( (begin_time >= last_added_time) && (goodornot==true) && (checkCons(chosen->reference, begin_time, 0, mec )==true) )
					//if (checkStartCons(chosen->reference, begin_time,  mec )==true) 
				{						

#ifdef RDEBUG
					printf(" Proposed start time : %f", begin_time);
#endif

					//choose the duration and check contraints again

					float max_duration =  (mec->getConstraintSignal(chosen->reference))->getMax_duration();
					float min_duration =  (mec->getConstraintSignal(chosen->reference))->getMin_duration();

					float distance = max_duration - min_duration;

					float result = randomgen->GetRand01();
					result = sqrt(result);

					float x1 =  min_duration + (result*distance) /2.0f;
					float x2 =  max_duration - (result*distance) /2.0f;

					//printf("min %f, max %f, x1, %f, x2 %f \n", min_duration, max_duration, begin_time + x1, begin_time + x2);

					//duration
					float duration_time= 0.0f;
					result=randomgen->GetRand01();
					if (result>0.5f) duration_time=x1; else duration_time=x2;

					//is it a good duration_time?
					if  ( ( duration_time>0 )  && ( begin_time + duration_time <= time ) && (checkCons (chosen->reference, begin_time, duration_time, mec ) == true) ) 
						//if ( ( checkStopCons (chosen->reference, begin_time, duration_time, mec ) == true) && ( begin_time + duration_time <= time ) )
					{ 

						// duration is ok and endcons are true...

#ifdef RDEBUG
						printf(" Proposed duration : %f", duration_time);
#endif

						//be sure that next signal starts non earlier than this one
						last_added_time=begin_time;

						//set start and stop
						chosen->start=begin_time;							
						//chosen.start_sym

						chosen->duration = duration_time;
						//chosen.duration_sym

						//do I need to fill some other properties??
						chosen->concretized=true;


						//TO DO : find strokes

						//printf("before %i \n", chosen->strokes.size());					

						//							if (chosen->strokes.empty()==false)	{
						//
						//								std::vector<float> new_strokes;
						//
						//								std::vector<float>::iterator iter;															
						//								for(iter=chosen->strokes.begin();iter!=chosen->strokes.end();iter++)
						//								{
						//								
						//									float f=(*iter);
						//									int found =0;
						//									
						//									//printf("stokes is :%f ", f);
						//
						//									//if stroke is included
						//									
						//									if (!( (f<=begin_time) || (f >= duration_time) ) )									
						//									{
						//										if (found==0) 
						//										{	
						//											new_strokes.push_back(f);
						//											found=1;							
						//										}
						//									}
						//								}//end for
						//
						//								chosen->strokes=new_strokes;
						//}
						//							//	//delete other strokes
						//							//	int index1=0;
						//							//	for (iter=chosen->strokes.begin();iter!=chosen->strokes.end();)
						//							//	{
						//							//		index1++;
						//							//		if(index1>1) chosen->strokes.erase(iter);
						//							//	}
						//
						//							//}//end if strokes are empty
						//
						//							//if no strokes


						chosen->strokes.clear();

						if ((chosen->strokes.empty()==true) || (chosen->strokes.size()==0)) 
						{
							chosen->strokes.resize(0);

							//check it: stokes are relative or absolute
							float stroke_time = 0.1 * duration_time;
							chosen->strokes.push_back(stroke_time);								
						}

						//...but if torso do not use strokes

						//system crashes here
						/*if ((chosen->modality.empty())||(chosen->modality!="")||(chosen->modality!="torso"))
						{	
						std::vector<float>::iterator iter;
						for (iter=chosen->strokes.begin();iter!=chosen->strokes.end();) 
						chosen->strokes.erase(iter);
						}*/


						//ADD the SIGNAL to MSE:

						//BECOUSE IT HAS START AND END
						chosen->concretized=true;

						this->push_back(*chosen);
						singals_in_turn++;

						//we fill both sides
						//the last signal of the id=id is in a tree
						//the alg is ok if there are no signals of the same id that overlap

						//check it!						
						//fill constreee with if the signal added to list

						mec->fill(mec->getIdOfSignal(chosen->reference), begin_time, duration_time);						

						//UPADATE the probabilities : (consider the possibility of repetition, (???of the continuation!!!!,) change the probabilities of occurence)												

						float factor =  (float) ( (float)  (turn+1)   / (float) n ) ;

						if ( (factor < 1.0f) && (factor >= 0.0f) )
						{	
							for(iter=clone.begin();iter!=clone.end();iter++)	//for all signals in clone 
							{	
								std::string temp_name = findName(iter->reference);

								float current_repetivity = constraintscontainer->getRepetivity(emotion, iter->reference ) ; 			

								//if it is an emement that can not be repeated - I put 0 as a probability
								if  (iter->reference.compare(chosen->reference) == 0 )
									if (current_repetivity==0.0f)  
									{							

										probabilities_start[temp_name]=0.0f;
										probabilities_end[temp_name]=0.0f;
									}

									//three cases:
									//start=end
									if (probabilities_start[temp_name]==probabilities_end[temp_name])
										current_probabilities[temp_name] = probabilities_start[temp_name] ;
									//start < end
									if (probabilities_start[temp_name] < probabilities_end[temp_name]) 
									{						
										float distance = probabilities_end[temp_name]-probabilities_start[temp_name] ;
										current_probabilities[temp_name] = probabilities_start[temp_name] + distance * factor ;
									}
									//start > end										
									if (probabilities_start[temp_name] > probabilities_end[temp_name])
									{						
										float distance = probabilities_start[temp_name] - probabilities_end[temp_name];
										current_probabilities[temp_name] = probabilities_start[temp_name] - distance * factor ;
									}

									if (current_probabilities[temp_name]>1.0f) current_probabilities[temp_name]=1;
									if (current_probabilities[temp_name]<0.0f) current_probabilities[temp_name]=0;


							}//end of for

						}//end of if factor

						//after adding a signal and updating the prob
						//do we want to add a new signal in this turn 

						//to do : cHECK WHY oac IS DIFFERENT FOR DIFFERENT SIGNALS OF the same EMOTION

						//which parameters should be used
						float overall = (*chosen).parameters["OAC.value"].GetValue();

						//3 - becouse max 3 signals at the moment possible - true ?
						//supposing that the OAC is from the interval -1,1

						if (((float)(overall+1)*(float)(3 - singals_in_turn))>2.0f) 
						{
							//debug
							//do not use it again in this turn						
							std::string temp_name = findName(chosen->reference);
							current_probabilities[temp_name] = 0.0f ;

							success=true;
							//continue; // I DO NOT EXIT to while....
						}
						else 
						{

							//all is possible in next turn

							temporal=clone.clone();
							turn++; //or more if the expressivity is low

							//refresh probability current!!!

							//update the probabilities next turn probabilities
							float factor =  (float) ( (float)  (turn+1)   / (float) n ) ;

							if ( (factor < 1.0f) && (factor >= 0.0f) )
							{	
								for(iter=clone.begin();iter!=clone.end();iter++)	//for all signals in clone 
								{	
									std::string temp_name = findName(iter->reference);

									//three cases:
									//start=end
									if (probabilities_start[temp_name]==probabilities_end[temp_name])
										current_probabilities[temp_name] = probabilities_start[temp_name] ;
									//start < end
									if (probabilities_start[temp_name] < probabilities_end[temp_name]) 
									{						
										float distance = probabilities_end[temp_name]-probabilities_start[temp_name] ;
										current_probabilities[temp_name] = probabilities_start[temp_name] + distance * factor ;
									}
									//start > end										
									if (probabilities_start[temp_name] > probabilities_end[temp_name])
									{						
										float distance = probabilities_start[temp_name] - probabilities_end[temp_name];
										current_probabilities[temp_name] = probabilities_start[temp_name] - distance * factor ;
									}

									if (current_probabilities[temp_name]>1.0f) current_probabilities[temp_name]=1;
									if (current_probabilities[temp_name]<0.0f) current_probabilities[temp_name]=0;

								}//end of for

							}//end of if factor

							success=true;					
						}//end of else


					}//end of if checkstop is true 

					else {

						//if we are here it means that chosen is not a good signal becouse it cannot be finshed in time
						//the signal duration is too long or
						//the endcons are not satisfied

						//CHOOSE DIFFERENT duration_time

						//refaire checkend...
						//do 

						//divide duration_time by two till is > min.duration

						//while {endtime < min.duration or checkend==ok)

						//REFAIRE TOUT???


					}//end of else checkstop true

				}  // end if checkstart == true 

				else  //check start is false....
				{

					//if we are here it means that chosen is not a good signal becouse it cannot be started

					//cons for start can not be satisfied!

					//so: cancel a from the proposal list and choose a signal again					

					//do not use it again in this turn						
					std::string temp_name = findName(chosen->reference);
					current_probabilities[temp_name] = 0.0f ;				

				}//end of else

			}//end of else if short list is empty

		}// end of	(!(temporal.empty())) 

		else { 

			//IF TEMPORAL IS EMPTY:

			//all is possible in next turn

			temporal=clone.clone();

			turn++;

			//refresh probability current!!!

			//update the probabilities next turn probabilities
			float factor =  (float) ( (float)  (turn+1)   / (float) n ) ;

			if ( (factor < 1.0f) && (factor >= 0.0f) )
			{	
				for(iter=clone.begin();iter!=clone.end();iter++)	//for all signals in clone 
				{	
					std::string temp_name = findName(iter->reference);

					//three cases:
					//start=end
					if (probabilities_start[temp_name]==probabilities_end[temp_name])
						current_probabilities[temp_name] = probabilities_start[temp_name] ;
					//start < end
					if (probabilities_start[temp_name] < probabilities_end[temp_name]) 
					{						
						float distance = probabilities_end[temp_name]-probabilities_start[temp_name] ;
						current_probabilities[temp_name] = probabilities_start[temp_name] + distance * factor ;
					}
					//start > end										
					if (probabilities_start[temp_name] > probabilities_end[temp_name])
					{						
						float distance = probabilities_start[temp_name] - probabilities_end[temp_name];
						current_probabilities[temp_name] = probabilities_start[temp_name] - distance * factor ;
					}

					if (current_probabilities[temp_name]>1.0f) current_probabilities[temp_name]=1;
					if (current_probabilities[temp_name]<0.0f) current_probabilities[temp_name]=0;


				}//end of for

			}//end of if factor


		}

	}// End of while


	//add the empty singal at the end if THERE IS A LOt OF EMPTY SPACE at and

	if (turn*time_unit<time - 0.1)
	{
		//create empty signal
		Signal empty;
		empty.id="neutral";
		empty.start=turn*time_unit;
		empty.duration_sym=(float)time - (float)(turn*time_unit);
		empty.duration=(float)time - (float)(turn*time_unit);

		//printf(" turn*time_unit, %f ",turn*time_unit);
		//printf("time , %f ",time);

		empty.reference="faceexp=neutral";
		empty.concretized=true;
		empty.strokes.push_back( 0.1 *empty.duration );	
		empty.intensity=1;
		empty.type="face";
		empty.modality="face";
		empty.concretized=true;

		this->push_back(empty);

	}//end of if

	//to do: fill gaps



	//refill constrees
	mec->clean();

	//check how many signals

	int counter=0;
	int counter2=0;

	std::vector<Signal>::iterator iterxxx1;

	for(iterxxx1=this->begin();iterxxx1!=this->end();iterxxx1++)
	{ 
		counter++;
	}

	#ifdef RDEBUG

	//to do: check why emergency is sometimes empty
	std::vector<Signal>::iterator iterxxx3;

	for(iterxxx3=emergency.begin();iterxxx3!=emergency.end();iterxxx3++)
	{ 
		counter2++;
	}

	printf("result %i, %i", counter, counter2);
	#endif


	//recurency if the result is poor

	if (counter<time*0.3){

		#ifdef RDEBUG
		printf("INSIDE: ");
		#endif

		emergency.addTimeConstraints(emotion,++enter);
		this->clear();

		std::vector<Signal>::iterator iterxxx4;

		for(iterxxx4=emergency.begin();iterxxx4!=emergency.end();iterxxx4++)
		{ 
			Signal s(*iterxxx4);		
			this->push_back(s);
		}

	}

	//clean procedures:

	//clean emergency


}//end of addTimecontstraints




std::string MultimodalSignal::findName(std::string reference) 
{
	std::string new_name;
	if( reference!="" )
	{							
		if ( reference.find_first_of("=")!=0 ) 
			new_name = reference.substr (reference.find_first_of("=")+1, reference.length()- reference.find_first_of("=") - 1);
		else new_name=reference;
	}

	return new_name;
}


//
//NOT USED BY GRETA
//

//time in seconds (and not in time-units)	
bool MultimodalSignal::checkStartCons(std::string reference, float begin_time, MultimodalEmotionConstraint *mec)
{

	//std::vector<Constraint>* good_cons = mec->getConstraintsOfOneSignal(reference);

	////good_cons = mec->getConstraints();	

	//int id = mec->getIdOfSignal (reference);


	////for all relevant cons
	//std::vector<Constraint>::iterator cons_iter;
	//for(cons_iter=good_cons->begin();cons_iter!=good_cons->end();cons_iter++) {

	//	//and all signals chosen for the moment
	//	std::vector<Signal>::iterator signals_iter;
	//	for(signals_iter=this->begin();signals_iter!=this->end();signals_iter++) {


	//		int signal_id = mec->getIdOfSignal( (*signals_iter).reference );


	//		if (
	//			( (signal_id==cons_iter->getArg1Id()) &&(id==cons_iter->getArg2Id()) ) 
	//			|| 
	//			( (signal_id==cons_iter->getArg2Id()) &&(id==cons_iter->getArg1Id()) ) 
	//			)
	//		{

	//			//suppose that the first argument = a candicate

	//			/*
	//			a cons is of type 1 

	//			<con type="minus">
	//			<arg id="this" type="start"/> 
	//			<arg id="other" type="start"/> 
	//			<lessthan value="0"/> 
	//			</con>  

	//			start(this)-start(other) < x
	//			start(this) < x + start(other)

	//			*/

	//			/*
	//			if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isLessThan()==true) &&
	//			(cons_iter->getArg1Type()==1) &&
	//			(cons_iter->getArg1Id()==id)&&
	//			(cons_iter->getArg2Type()==1)&&
	//			(cons_iter->getArg2Id()==signal_id)
	//			) 
	//			{

	//			if ( !( begin_time <  (*signals_iter).start) ) return false;

	//			}// end of condition 1

	//			*/

	//			/*
	//			a cons is of type 2 

	//			<con type="minus">
	//			<arg id="this" type="start"/> 
	//			<arg id="other" type="start"/> 
	//			<equal value="0"/> 
	//			</con>  

	//			start(this)-start(other) = x
	//			start(this) =  start(other)

	//			*/

	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isEqual()==true) &&
	//				(cons_iter->getArg1Type()==1) &&
	//				(cons_iter->getArg1Id()==id)&&
	//				(cons_iter->getArg2Type()==1)&&
	//				(cons_iter->getArg2Id()==signal_id)
	//				) 
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) ) 
	//				{
	//					if (
	//						!(begin_time == (*signals_iter).start ) 
	//						) return false;
	//				}
	//			}// end of condition 2
	//			/*
	//			a cons is of type 3 

	//			<con type="minus">
	//			<arg id="this" type="start"/> 
	//			<arg id="other" type="start"/> 
	//			<morethan value="0"/> 
	//			</con>  

	//			start(this)-start(other) > x
	//			start(this) > x + start(other)
	//			*/

	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isMoreThan()==true) &&
	//				(cons_iter->getArg1Type()==1) &&
	//				(cons_iter->getArg1Id()==id)&&
	//				(cons_iter->getArg2Type()==1)&&
	//				(cons_iter->getArg2Id()==signal_id)
	//				) 
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//					if ( 
	//						( 
	//						!( begin_time >  (*signals_iter).start ) 
	//						) 
	//						)
	//						return false;

	//			}// end of condition 3

	//			/*
	//			a cons is of type 4

	//			<con type="minus">
	//			<arg id="this" type="start"/> 
	//			<arg id="other" type="stop"/> 
	//			<lessthan value="0"/> 
	//			</con>  

	//			start(this)-stop(other) < x
	//			start(this) < x + stop(other)
	//			*/

	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isLessThan()==true) &&
	//				(cons_iter->getArg1Type()==1) &&
	//				(cons_iter->getArg1Id()==id)&&
	//				(cons_iter->getArg2Type()==2)&&
	//				(cons_iter->getArg2Id()==signal_id)
	//				) 
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start +  (*signals_iter).duration ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//					if (
	//						!(begin_time <  (*signals_iter).start +  (*signals_iter).duration) 
	//						) return false;

	//			}// end of condition 4

	//			/*
	//			a cons is of type 5

	//			<con type="minus">
	//			<arg id="this" type="start"/> 
	//			<arg id="other" type="stop"/> 
	//			<equal value="0"/> 
	//			</con>  

	//			start(this)-stop(other) = x
	//			start(this) = x + stop(other)

	//			*/

	//			//if a cons is of type 5
	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)

	//			//contraditory conditions
	//			//if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isEqual()==true) &&
	//				(cons_iter->getArg1Type()==1) &&
	//				(cons_iter->getArg1Id()==id)&&
	//				(cons_iter->getArg2Type()==2)&&
	//				(cons_iter->getArg2Id()==signal_id)
	//				) 
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start +  (*signals_iter).duration ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//					if (
	//						!(begin_time ==  (*signals_iter).start +  (*signals_iter).duration) 
	//						) return false;

	//			}// end of condition 5


	//			/*
	//			a cons is of type 6

	//			<con type="minus">
	//			<arg id="this" type="start"/> 
	//			<arg id="other" type="stop"/> 
	//			<morethan value="0"/> 
	//			</con>  

	//			start(this)-stop(other) > x
	//			start(this) > x + stop(other)
	//			*/

	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isMoreThan()==true) &&
	//				(cons_iter->getArg1Type()==1) &&
	//				(cons_iter->getArg1Id()==id)&&
	//				(cons_iter->getArg2Type()==2)&&
	//				(cons_iter->getArg2Id()==signal_id)
	//				) 
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start +  (*signals_iter).duration ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)

	//				//the condition is not valid signals cannot be intersected
	//				//if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//				if (
	//					!(begin_time >  (*signals_iter).start +  (*signals_iter).duration) 
	//					) return false;

	//			}// end of condition 6

	//			//suppose that the second argument = a  candicate

	//			/*
	//			a cons is of type 7

	//			<con type="minus">
	//			<arg id="other" type="start"/> 
	//			<arg id="this" type="start"/> 						
	//			<lessthan value="0"/> 
	//			</con>  

	//			start(other) - start(this) < x
	//			start(this) > start(other)

	//			*/

	//			//if a cons is of type 7
	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isLessThan()==true) &&
	//				(cons_iter->getArg2Type()==1) &&
	//				(cons_iter->getArg2Id()==id)&&
	//				(cons_iter->getArg1Type()==1)&&
	//				(cons_iter->getArg1Id()==signal_id)
	//				) 
	//			{

	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//					if ( !(begin_time >  (*signals_iter).start) ) return false;

	//			}// end of condition 7

	//			/*
	//			a cons is of type 8

	//			<con type="minus">
	//			<arg id="other" type="start"/> 
	//			<arg id="this" type="start"/> 						
	//			<equalthan value="0"/> 
	//			</con>  

	//			start(other) - start(this) = 0
	//			start(this) = start(other)

	//			*/
	//			//if a cons is of type 8
	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isEqual()==true) &&
	//				(cons_iter->getArg2Type()==1) &&
	//				(cons_iter->getArg2Id()==id)&&
	//				(cons_iter->getArg1Type()==1)&&
	//				(cons_iter->getArg1Id()==signal_id)
	//				) {

	//					//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start  ); 

	//					//or it is a global con
	//					//or it is a local con and so the intersection has to be true
	//					//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//					if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//						if (
	//							!(begin_time == (*signals_iter).start ) 
	//							) return false;

	//			}// end of condition 8


	//			/*
	//			a cons is of type 9

	//			<con type="minus">
	//			<arg id="other" type="start"/> 
	//			<arg id="this" type="start"/> 						
	//			<morethan value="0"/> 
	//			</con>  

	//			start(other)-start(this) > x
	//			start(this) < start(other)

	//			*/

	//			/*
	//			//if a cons is of type 9
	//			if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isMoreThan()==true) &&
	//			(cons_iter->getArg2Type()==1) &&
	//			(cons_iter->getArg2Id()==id)&&
	//			(cons_iter->getArg1Type()==1)&&
	//			(cons_iter->getArg1Id()==signal_id)
	//			) 
	//			{

	//			if ( !(begin_time <  (*signals_iter).start) ) return false;

	//			}// end of condition 9
	//			*/

	//			/*
	//			a cons is of type 7

	//			<con type="minus">
	//			<arg id="other" type="stop"/> 
	//			<arg id="this" type="start"/> 						
	//			<lessthan value="0"/> 
	//			</con>  

	//			stop(other) - start(this) < x
	//			start(this) > stop(other)

	//			*/
	//			//if a cons is of type 10
	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isLessThan()==true) &&
	//				(cons_iter->getArg2Type()==1) &&
	//				(cons_iter->getArg2Id()==id)&&
	//				(cons_iter->getArg1Type()==2)&&
	//				(cons_iter->getArg1Id()==signal_id)
	//				)
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start +  (*signals_iter).duration ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)

	//				//again contraditory conditions
	//				//if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//				if (
	//					!(begin_time >  (*signals_iter).start +  (*signals_iter).duration) 
	//					) return false;

	//			}// end of condition 10

	//			/*
	//			a cons is of type 7

	//			<con type="minus">
	//			<arg id="other" type="stop"/> 
	//			<arg id="this" type="start"/> 						
	//			<equal value="0"/> 
	//			</con>  

	//			stop(other) - start(this) = x
	//			start(this) = stop(other)

	//			*/
	//			//if a cons is of type 11
	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isEqual()==true) &&
	//				(cons_iter->getArg2Type()==1) &&
	//				(cons_iter->getArg2Id()==id)&&
	//				(cons_iter->getArg1Type()==2)&&
	//				(cons_iter->getArg1Id()==signal_id)
	//				) 
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start +  (*signals_iter).duration ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)

	//				//contradotory condition
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//					if (
	//						!(begin_time ==  (*signals_iter).start +  (*signals_iter).duration) 
	//						) return false;

	//			}// end of condition 11
	//			/*
	//			a cons is of type 7

	//			<con type="minus">
	//			<arg id="other" type="stop"/> 
	//			<arg id="this" type="start"/> 						
	//			<morethan value="0"/> 
	//			</con>  

	//			stop(other) - start(this) > x
	//			start(this) < stop(other)

	//			*/
	//			//if a cons is of type 12
	//			if (
	//				(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//				(cons_iter->isMoreThan()==true) &&
	//				(cons_iter->getArg2Type()==1) &&
	//				(cons_iter->getArg2Id()==id)&&
	//				(cons_iter->getArg1Type()==2)&&
	//				(cons_iter->getArg1Id()==signal_id)
	//				)
	//			{
	//				//printf(" begin_time %f , signal time %f \n", begin_time, (*signals_iter).start +  (*signals_iter).duration ); 

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time) ) )
	//					if (
	//						!(begin_time <  (*signals_iter).start +  (*signals_iter).duration) 
	//						) return false;

	//			}// end of condition 12

	//		}//end if

	//	}//end of for

	//}//end of for


	return true;
}//end of checkStartCons



//time in seconds (and not in time-units)	
bool MultimodalSignal::checkStopCons(std::string reference, float begin_time, float duration_time, MultimodalEmotionConstraint *mec){

	//std::vector<Constraint>* good_cons = mec->getConstraintsOfOneSignal(reference);
	//int id = mec->getIdOfSignal (reference);

	////for all relevant cons
	//std::vector<Constraint>::iterator cons_iter;
	//for(cons_iter=good_cons->begin();cons_iter!=good_cons->end();cons_iter++) {

	//	//and all signals chosen for the moment
	//	std::vector<Signal>::iterator signals_iter;
	//	for(signals_iter=this->begin();signals_iter!=this->end();signals_iter++) {

	//		int signal_id = mec->getIdOfSignal( (*signals_iter).reference );

	//		//suppose that the first argument= a  candicate

	//		/*
	//		a cons is of type 1

	//		<con type="minus">
	//		<arg id="this" type="stop"/> 						
	//		<arg id="other" type="stop"/> 

	//		<lessthan value="0"/> 
	//		</con>  

	//		stop(this) - stop(other) < x
	//		stop(this) < stop(other)

	//		*/
	//		//if a cons is of type 1
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isLessThan()==true) &&
	//			(cons_iter->getArg1Type()==2) &&
	//			(cons_iter->getArg1Id()==id)&&
	//			(cons_iter->getArg2Type()==2)&&
	//			(cons_iter->getArg2Id()==signal_id)
	//			) 
	//		{

	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)

	//			//this condition is always true as if stop(x)<stop(y) -> x and y intersect
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if ( ! (duration_time <  ( (*signals_iter).start +  (*signals_iter).duration) ) ) return false;

	//		}// end of condition 1

	//		/*
	//		a cons is of type 1

	//		<con type="minus">
	//		<arg id="this" type="stop"/> 						
	//		<arg id="other" type="stop"/> 

	//		<equal value="0"/> 
	//		</con>  

	//		stop(this) - stop(other) = x
	//		stop(this) = stop(other)

	//		*/
	//		//if a cons is of type 2
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isEqual()==true) &&
	//			(cons_iter->getArg1Type()==2) &&
	//			(cons_iter->getArg1Id()==id)&&
	//			(cons_iter->getArg2Type()==2)&&
	//			(cons_iter->getArg2Id()==signal_id)
	//			) {

	//				//or it is a global con
	//				//or it is a local con and so the intersection has to be true
	//				//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//				if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//					if (
	//						!(
	//						duration_time ==( (*signals_iter).start +  (*signals_iter).duration ) 
	//						) 
	//						) return false;

	//		}// end of condition 2

	//		/*
	//		a cons is of type 1

	//		<con type="minus">
	//		<arg id="this" type="stop"/> 						
	//		<arg id="other" type="stop"/> 

	//		<morethan value="0"/> 
	//		</con>  

	//		stop(this) - stop(other) > x
	//		stop(this) > stop(other)

	//		*/
	//		//if a cons is of type 3
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isMoreThan()==true) &&
	//			(cons_iter->getArg1Type()==2) &&
	//			(cons_iter->getArg1Id()==id)&&
	//			(cons_iter->getArg2Type()==2)&&
	//			(cons_iter->getArg2Id()==signal_id)
	//			)
	//		{

	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if ( 
	//					! (
	//					duration_time > ( (*signals_iter).start +  (*signals_iter).duration )
	//					) 
	//					) return false;

	//		}// end of condition 3


	//		/*

	//		<con type="minus">
	//		<arg id="this" type="stop"/> 
	//		<arg id="other" type="start"/> 
	//		<lessthan value="0"/> 
	//		</con>  

	//		stop(this)-start(other) < x
	//		stop(this) < x + start(other)

	//		*/

	//		/*			
	//		if (
	//		(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//		(cons_iter->isLessThan()==true) &&
	//		(cons_iter->getArg1Type()==2) &&
	//		(cons_iter->getArg1Id()==id)&&
	//		(cons_iter->getArg2Type()==1)&&
	//		(cons_iter->getArg2Id()==signal_id)
	//		) 

	//		{


	//		if (
	//		! (
	//		duration_time <  (*signals_iter).start 
	//		)
	//		) return false;

	//		}// end of condition 4
	//		*/


	//		//if a cons is of type 5
	//		/*

	//		<con type="">
	//		<arg id="this" type="stop"/> 
	//		<arg id="other" type="start"/> 
	//		<equal value="0"/> 
	//		</con>  

	//		stop(this)-start(other) = x
	//		stop(this) = x + start(other)

	//		*/

	//		/*
	//		if (
	//		(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//		(cons_iter->isEqual()==true) &&
	//		(cons_iter->getArg1Type()==2) &&
	//		(cons_iter->getArg1Id()==id)&&
	//		(cons_iter->getArg2Type()==1)&&
	//		(cons_iter->getArg2Id()==signal_id)
	//		) 
	//		{

	//		//check the condition: a < b + x, x = 0 or x = cons_iter->value
	//		if (
	//		!(
	//		duration_time ==  (*signals_iter).start  
	//		)
	//		) 
	//		return false;

	//		}// end of condition 5
	//		*/

	//		/*

	//		<con type="">
	//		<arg id="this" type="stop"/> 
	//		<arg id="other" type="start"/> 
	//		<morethan value="0"/> 
	//		</con>  

	//		stop(this)-start(other) > x
	//		stop(this) > x + start(other)

	//		*/

	//		//if a cons is of type 6
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isMoreThan()==true) &&
	//			(cons_iter->getArg1Type()==2) &&
	//			(cons_iter->getArg1Id()==id)&&
	//			(cons_iter->getArg2Type()==1)&&
	//			(cons_iter->getArg2Id()==signal_id)
	//			) 
	//		{


	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if (
	//					!(
	//					duration_time >  (*signals_iter).start 
	//					)
	//					) return false;

	//		}// end of condition 6

	//		//suppose that the second argument = a  candicate
	//		/*

	//		<con type="">
	//		<arg id="other" type="stop"/> 
	//		<arg id="this" type="stop"/> 
	//		<lessthan value="0"/> 
	//		</con>  

	//		stop(other)-stop(this) < x
	//		stop(this) > x + stop(other)

	//		*/
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isLessThan()==true) &&
	//			(cons_iter->getArg2Type()==2) &&
	//			(cons_iter->getArg2Id()==id)&&
	//			(cons_iter->getArg1Type()==2)&&
	//			(cons_iter->getArg1Id()==signal_id)
	//			) 
	//		{

	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if ( 
	//					!(
	//					duration_time > ( (*signals_iter).start +  (*signals_iter).duration )
	//					) 
	//					) return false;

	//		}// end of condition 7

	//		/*
	//		<con type="">
	//		<arg id="other" type="stop"/> 
	//		<arg id="this" type="stop"/> 
	//		<equal value="0"/> 
	//		</con>  

	//		stop(other)-stop(this) = x
	//		stop(this) = x + stop(other)

	//		*/
	//		//if a cons is of type 8
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isEqual()==true) &&
	//			(cons_iter->getArg2Type()==2) &&
	//			(cons_iter->getArg2Id()==id)&&
	//			(cons_iter->getArg1Type()==2)&&
	//			(cons_iter->getArg1Id()==signal_id)
	//			)
	//		{

	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if (
	//					!(
	//					duration_time == ( (*signals_iter).start +  (*signals_iter).duration )
	//					) 
	//					) return false;

	//		}// end of condition 8

	//		/*
	//		<con type="">
	//		<arg id="other" type="stop"/> 
	//		<arg id="this" type="stop"/> 
	//		<morethan value="0"/> 
	//		</con>  

	//		stop(other)-stop(this) > x
	//		stop(this) < x + stop(other)

	//		*/
	//		//if a cons is of type 9
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isMoreThan()==true) &&
	//			(cons_iter->getArg2Type()==2) &&
	//			(cons_iter->getArg2Id()==id)&&
	//			(cons_iter->getArg1Type()==2)&&
	//			(cons_iter->getArg1Id()==signal_id)
	//			)
	//		{

	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)

	//			//this condition is always true as if stop(x)<stop(y) -> x and y intersect
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if (
	//					!(
	//					duration_time <  ( (*signals_iter).start +  (*signals_iter).duration )
	//					)
	//					) return false;

	//		}// end of condition 9

	//		/*

	//		<con type="">
	//		<arg id="other" type="start"/> 
	//		<arg id="this" type="stop"/> 
	//		<lessthan value="0"/> 
	//		</con>  

	//		start(other)-stop(this) < x
	//		stop(this) > x + start(other)

	//		*/
	//		//if a cons is of type 10
	//		if (
	//			(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//			(cons_iter->isLessThan()==true) &&
	//			(cons_iter->getArg2Type()==2) &&
	//			(cons_iter->getArg2Id()==id)&&
	//			(cons_iter->getArg1Type()==1)&&
	//			(cons_iter->getArg1Id()==signal_id)
	//			) 
	//		{

	//			//or it is a global con
	//			//or it is a local con and so the intersection has to be true
	//			//if it is not a global con and the interesection is false then do not check the condition (true by default as con cannot be applied in this case)
	//			if ( (strcmp(cons_iter->getConstraintRange().c_str(),"global")==0) || ( (*signals_iter).intersect(begin_time,duration_time) ) )
	//				if (
	//					!(
	//					duration_time >  (*signals_iter).start
	//					) 
	//					) return false;

	//		}// end of condition 10

	//		/*			
	//		<con type="minus">
	//		<arg id="other" type="start"/> 
	//		<arg id="this" type="stop"/> 
	//		<equal value="0"/> 
	//		</con>  

	//		start(other) - stop(this)= 0
	//		stop(this) =  start(other)

	//		*/


	//		/*

	//		//if a cons is of type 11
	//		if (
	//		(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//		(cons_iter->isEqual()==true) &&
	//		(cons_iter->getArg2Type()==2) &&
	//		(cons_iter->getArg2Id()==id)&&
	//		(cons_iter->getArg1Type()==1)&&
	//		(cons_iter->getArg1Id()==signal_id)
	//		) {

	//		if (
	//		!(
	//		duration_time ==  (*signals_iter).start 
	//		)
	//		) return false;

	//		}// end of condition 11
	//		*/
	//		/*			
	//		<con type="minus">
	//		<arg id="other" type="start"/> 
	//		<arg id="this" type="stop"/> 
	//		<morethan value="0"/> 
	//		</con>  

	//		start(other) - stop(this) > 0
	//		stop(this) <  start(other)

	//		*/
	//		/*
	//		//if a cons is of type 12
	//		if (
	//		(strcmp(cons_iter->getConstraintType().c_str(), "minus")==0)&&
	//		(cons_iter->isMoreThan()==true) &&
	//		(cons_iter->getArg2Type()==2) &&
	//		(cons_iter->getArg2Id()==id)&&
	//		(cons_iter->getArg1Type()==1)&&
	//		(cons_iter->getArg1Id()==signal_id)
	//		) 
	//		{

	//		if (
	//		!(
	//		duration_time <  (*signals_iter).start 
	//		)
	//		) return false;

	//		}// end of condition 12
	//		*/


	//	}//end of for

	//}//end of for

	return true;

}//end of checkEndCons




MultimodalSignal MultimodalSignal::clone ()
{

	//check it
	MultimodalSignal cloned;

	std::vector<Signal>::const_iterator iter;
	for(iter=this->begin();iter!=this->end();iter++)
	{
		Signal s(*iter);
		cloned.push_back(s);
	}
	cloned.preference=this->preference;

	return cloned;
}

//new version extended

bool MultimodalSignal::checkCons(std::string reference, float begin_time, float duration_time, MultimodalEmotionConstraint *mec){

	//start or end cons
	bool end_cons=false;

	if(duration_time==0) end_cons=false; 
	else end_cons=true;

	//get all relevant signals
	std::vector<ConsNode*>* good_cons = mec->getNewConstraintsOfOneSignal(reference);
	int id = mec->getIdOfSignal (reference);

	//clone good_cons	
	//for security we do not want to have left part 
	//even if on the left part it should not be nothing dangerous
	// the rules A or (B and C) are not approved

	std::vector<ConsNode*>* cloned_good_cons =new std::vector<ConsNode*>;

	//clone good_cons
	std::vector<ConsNode*>::iterator cons_iter;

	for(cons_iter=good_cons->begin();cons_iter!=good_cons->end();cons_iter++) 
		cloned_good_cons->push_back( (*cons_iter)->clone());

	//result
	// 0 - false, 1 - true

	int satisfied=1;

	//for all relevant cons

	//cons_iter is one constreee

	// redefinition : std::vector<ConsNode*>::iterator cons_iter;

	//find the current list of signals with constraints and put in to vector of the type ConstraintSignal
	std::vector<ShortSignal*>* temp_animation =new std::vector<ShortSignal*>;

	std::vector<Signal>::const_iterator iter;

	for(iter=this->begin();iter!=this->end();iter++)
	{	

		if ( ( (*iter).start>=0) && ((*iter).duration>0) )
		{
			//create temp_cs			
			ShortSignal * temp = new ShortSignal();

			temp->start=(*iter).start;
			temp->end = ( (*iter).start + (*iter).duration);

			//check it
			temp->label = (*iter).reference;
			temp->id = mec->getIdOfSignal( (*iter).reference );

			temp_animation->push_back(temp);
		}

	}

	for(cons_iter=cloned_good_cons->begin();cons_iter!=cloned_good_cons->end();cons_iter++) 
	{

		//we fill both sides
		//the last signal of the id=id is in a tree
		//the alg is ok if there are no signals of the same id that overlap
		//check it!

		(*cons_iter)->fill(id, begin_time, duration_time);
		satisfied *= (*cons_iter)->evaluate(id,temp_animation);

	}//end for

	//cloned_good_cons empty

	//check it;
	cloned_good_cons->clear();	

#ifdef RDEBUG
	printf(" is satisfied: %d (%f, %f) ", satisfied, begin_time, duration_time);
#endif

	if (satisfied==1)
	{

		//we fill both sides
		//the last signal of the id=id is in a tree
		//the alg is ok if there are no signals of the same id that overlap
		//check it!
		//fill constree if added to list

		//if only at the end of testing no!!!		
		//if (duration_time>0) mec->fill(id, begin_time,duration_time);

		return true;

	}

	//else
	return false;

}//end of checkcons



void MultimodalSignal::print(){

	std::vector<Signal>::iterator iter;

	for(iter=this->begin();iter!=this->end();iter++)	
	{			
		printf("Name %s , ", 	iter->reference.c_str() );
		printf("start %f , ", 	iter->start );
		printf("end %f , ", 	(iter->duration +iter->start));
		if (iter->strokes.empty()==false) 
			printf("stroke %f \n", iter->strokes.at(0) ); 
		else printf("no stroke defined \n");
	}

}