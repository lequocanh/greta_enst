//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// TorsoGestuary.cpp: implementation of the  TorsoGestuary class.
//
//////////////////////////////////////////////////////////////////////

#include "TorsoGestuary.h"
#include "TorsoGesture.h"

using namespace TorsoSpace;

TorsoGestuary::TorsoGestuary(){}

TorsoGestuary::~TorsoGestuary(){
	gestuary.clear();
}

std::map<std::string,TorsoGesture> TorsoGestuary::getGestuary(){return gestuary;}


int TorsoGestuary::LoadGestuary(std::string gestuaryfilename, TorsoMovementPoint restposition)
{
	XMLDOMParser p;
	XMLGenericTree *t;
	bool found_stroke;

	p.SetValidating(true);
	t=p.ParseFile(gestuaryfilename);
	if(t==0)
	{
		delete t;
		return 0;
	}
	
//	std::list<XMLGenericTree*>::iterator iter;

//	for(iter=t->child.begin();iter!=t->child.end();iter++)
	XMLGenericTree * chld;
	for(XMLGenericTree::iterator chldit=t->begin(); chldit != t->end(); ++chldit)
	{
		chld = *chldit;
		if (chld->isTextNode()) continue;
		found_stroke=false;
		
		TorsoGesture tg(chld->GetAttribute("id"));

		XMLGenericTree *torsonode;
		torsonode=chld->FindNodeCalled("torso");
		if(torsonode==NULL) continue;
		//std::list<XMLGenericTree*>::iterator iter2;
		//for(iter2=torsonode->child.begin();iter2!=torsonode->child.end();iter2++)
		XMLGenericTree * torsoChild;
		for(XMLGenericTree::iterator torsoit=torsonode->begin();
			torsoit != torsonode->end();
			++torsoit)
		{
			torsoChild = *torsoit;
			if (torsoChild->isTextNode()) continue;
			TorsoGesturePhase tp;
			if(!torsoChild->HasAttribute("phase")) continue;
			tp.type=tp.GetPhaseId(torsoChild->GetAttribute("phase"));
			if(tp.type==stroke)
				found_stroke=true;
			if(tp.type==unknown_phase)
				printf("TorsoEngine::Error unknown phase %s in gesture %s\n",
				torsoChild->GetAttribute("phase").c_str(),tg.id.c_str());
			else
			{
				if(tp.type==preparation)
				{
					tp.points.push_back(restposition);
				}
				tp.trajectory=tp.GetTrajectoryId(torsoChild->GetAttribute("trajectory"));
//				std::list<XMLGenericTree*>::iterator iter3;
//				for(iter3=(*iter2)->child.begin();iter3!=(*iter2)->child.end();iter3++)
				XMLGenericTree * c3;
				for(XMLGenericTree::iterator c3it=torsoChild->begin();
					c3it != torsoChild->end();
					++c3it)
				{	
					c3 = *c3it;
					if (c3->isTextNode()) continue;
					if(c3->GetName()=="spine")
					{
						if(c3->GetAttribute("manner")=="closed")
							tp.stance=closed;
						else
							tp.stance=opened;
					}
					if(c3->GetName()=="timing")
					{
						tp.Tdefault=c3->GetAttributef("default");
						tp.Tmin=c3->GetAttributef("min");
						tp.Tmax=c3->GetAttributef("max");
						tp.Tduration=c3->GetAttributef("duration");
						tp.Tfixed=(int)c3->GetAttributef("fixed");
					}
					if(c3->GetName()=="point")
					{
						TorsoMovementPoint tmp;
						tmp.Hposition=tmp.GetHpositionId(c3->GetAttribute("left_right"));
						tmp.Vposition=tmp.GetVpositionId(c3->GetAttribute("forward_backward"));
						tmp.Sposition=tmp.GetSpositionId(c3->GetAttribute("tilt"));
						tmp.SpineRotation=tmp.GetSpineRotationId(c3->GetAttribute("spine_rotation"));
						tmp.compensatehead=c3->GetAttributef("compensate_head")!=0;
						tmp.compensateshoulders=c3->GetAttributef("compensate_shoulders")!=0;
						tmp.spatial_fixed=c3->GetAttributef("spatial_fixed")!=0;
						if(tmp.LoadPose(tp.stance,c3->GetAttributef("scaling"))==0)
						{
							break;
						}
						if((tp.type!=stroke)&&(tp.points.size()>1))
							printf("TorsoEngine::Warning Multiple points phase in Gesture %s (only strokes can have multiple points)\n",
							tg.id.c_str());
						else
							if(tp.type==retraction)
								printf("TorsoEngine::Warning Retraction phase cannot contain poses (gesture %s)\n",
								tg.id.c_str());
							else
								tp.points.push_back(tmp);
					}
				}
				tg.phases.push_back(tp);
			}
		}
		if(found_stroke==true)
			this->gestuary[tg.id]=tg;
		else
			printf("TorsoEngine::Warning Skipping gesture %s (no stroke phase found)\n",tg.id.c_str());
	}

	delete t;
	return 1;
}