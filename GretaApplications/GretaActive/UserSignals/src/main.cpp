//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include <conio.h>

#include "UserSignals.h"
#include "XercesTool.h"
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;

void main(int argc, char ** argv)
{
	XercesTool::startupXMLTools();
	semaine::util::XMLTool::startupXMLTools();

	try{
		semaine::components::greta::UserSignals us("UserSignal", false, false);
		getch();
	}
	catch(CMSException & e) {
		printf("\n*********************************************************\nActiveMQ is not started. The application will be stopped.\n*********************************************************");
		getch();
		//MessageBox(0, "ActiveMQ is not started. The application will be stopped", "Warning", MB_OK | MB_ICONWARNING);
		_exit(0);
	} 
	XercesTool::shutdownXMLTools();
	semaine::util::XMLTool::shutdownXMLTools();
}
