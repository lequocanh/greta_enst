//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// XercesXMLGenericParser.cpp: implementation of the XercesXMLGenericParser class.
//
//////////////////////////////////////////////////////////////////////

#include "XercesXMLGenericParser.h"
#include <Windows.h>
#include <stdio.h>
#include <process.h>
#include <iostream>
#include <fstream>



//for general comments refer to the header file

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

XercesXMLGenericParser::XercesXMLGenericParser()
{
	result_tree=0;
	validating=true;
}

XercesXMLGenericParser::~XercesXMLGenericParser()
{

}

XMLGenericTree *XercesXMLGenericParser::ParseFile(std::string name)
{
	return 0;
}

/*
void GretaXmlTextReaderErrorFunc (void * arg, const char * msg, xmlParserSeverities severity, xmlTextReaderLocatorPtr locator)
{
	printf("Error parsing file:\n line %d column %d\n",
		xmlTextReaderGetParserLineNumber((xmlTextReaderPtr)arg),
		xmlTextReaderGetParserColumnNumber((xmlTextReaderPtr)arg));
}
*/

XMLGenericTree *XercesXMLGenericParser::ParseFileWithXSD(std::string name,std::string xsdpath)
{
	return 0;
}

XMLGenericTree *XercesXMLGenericParser::ParseBufferWithXSD(char *buffer,std::string xsdpath)
{
	return 0;
}

XMLGenericTree *XercesXMLGenericParser::ParseBuffer(char *buffer)
{
	return 0;
}

void XercesXMLGenericParser::SetValidating(bool v)
{
	validating=v;
}

XMLGenericTree *XercesXMLGenericParser::CreateTree(void *readerpointer)
{	
	return 0;
}


