#include "NaoGesture.h"

#include "IniManager.h"
#include "GestureEngine.h"
#include <stdio.h>
#include <string.h>
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <fl/fl_widget.h>
#include <FL/fl_draw.H>
#include <FL/Fl_File_Chooser.H>
#include <crtdbg.h>

#include "DataContainer.h"
extern DataContainer *datacontainer;

using namespace GestureSpace;

float arm_x[]={0.12, 0.09, 0.06, 0.03, 0.0};
float arm_y[]={0.19, 0.16, 0.13, 0.10, 0.07,0.04,0.00};
float arm_z[]={0.10, 0.12, 0.14};

const char * arm_x_look_up[]={"XEP", "XP", "XC", "XCC", "XOppC"};
const char * arm_y_look_up[]={"YUpperEP", "YUpperP", "YUpperC", "YCC", "YLowerC","YLowerP","YLowerEP"};
const char * arm_z_look_up[]={"ZNear", "ZMiddle", "ZFar"};


const char * phasetype_look_up[]={"PHASETYPE_DEFAULT", "PREPARATION", "PRE_STROKE_HOLD","STROKE",
								"STROKE_START", "STROKE_END","POST_STROKE_HOLD", "RETRACTION"};

const char * form_look_up[]={"form_default","form_fist","form_open","form_point1","form_point2",
								"form_2apart","form_openapart"};

const char * palm_look_up[]={"PalmDefault","PalmUp","PalmDown","PalmInwards","PalmOutwards","PalmAway","PalmTowards","PalmNone"};

const char * fingerbase_look_up[]={"FBDefault","FBUp","FBDown","FBInwards","FBOutwards","FBAway","FBTowards","FBNone"};

/*
	enum HandShapeType 		{shape_default=0, shape_form, shape_symbol};
	enum FormType  			{form_default, form_fist, form_open, form_point1, form_point2, form_2apart, form_openapart};
	enum FormThumbType 		{thumb_default, thumb_away, thumb_over};
	enum BendType 			{bend_default, bend_straight, bend_angled, bend_curved, bend_closed};
	enum SymbolType 		{symbol_default, symbol_1_open, symbol_2_open, symbol_3_open, symbol_1_closed, symbol_2_closed, symbol_3_closed};
	enum SymbolOpenType 	{open_default, open_thumbout, open_thumbin};
	enum SymbolClosedType   {closed_default, closed_straight, closed_inside, closed_tight};
	enum FingerType			{finger_default, thumb, index, middle, ring, pinky};
*/
NaoGesture::NaoGesture(void)
{
	gesture=new Gesture();
	phasesVector = &(gesture->phases);

	//ADDME: should we really start with 1 phase?
	GesturePhase *phase=new GesturePhase();
	phase->Start();
	phase->Finalize();
	phasesVector->push_back(phase);

	phase=new GesturePhase();
	phase->Start();
	phase->Finalize();
	gesture->phasesAssym.push_back(phase);
}

NaoGesture::~NaoGesture(void)
{
}

//Calculate absolute angle value for joints of robot corresponding to a phase of gesture
/*
Angle range for the head joints:

Joint name 	Motion 	Range (degrees)
HeadYaw 	Head joint twist (Z) 	-120 to 120
HeadPitch 	Head joint front and back (Y) 	-39 to 30

Angle range for the Left Arm joints:

Joint name 	Motion 	Range (degrees)
LShoulderPitch 	Left shoulder joint front and back (Y) 	-120 to 120
LShoulderRoll 	Left shoulder joint right and left (Z) 	0 to 95
LElbowYaw 	Left shoulder joint twist (X) 	-120 to 120
LElbowRoll 	Left elbow joint (Z) 	-90 to 0
LWristYaw 	Left wrist joint (X) 	-105 to 105
LHand 	Left hand 	Open And Close

Angle range for the Right Arm joints:

Joint name 	Motion 	Range (degrees)
RShoulderPitch 	Right shoulder joint front and back (Y) 	-120 to 120
RShoulderRoll 	Right shoulder joint right and left (Z) 	-95 to 0
RElbowYaw 	Right shoulder joint twist (X) 	-120 to 120
RElbowRoll 	Right elbow joint (Z) 	0 to 90
RWristYaw 	Right wrist joint (X) 	-105 to 105
RHand 	Right hand 	Open And Close

Angle range of the Left Leg joints:

Joint name 	Motion 	Range (degrees)
LHipYawPitch* 	Left hip joint twist (Y-Z 45�) 	-44 to 68
LHipRoll 	Left hip joint right and left (X) 	-25 to 45
LHipPitch 	Left hip joint front and back (Y) 	-104.5 to 28.5
LKneePitch 	Left knee joint (Y) 	-5 to 125
LAnklePitch 	Left ankle joint front and back (Y) 	-70.5 to 54
LAnkleRoll 	Left ankle joint right and left (X) 	-45 to 25

Angle range of the Right Leg joints:

Joint name 	Motion 	Range (degrees)
RHipYawPitch* 	Right hip joint twist (Y-Z 45�) 	-68 to 44
RHipRoll 	Right hip joint right and left (X) 	-45 to 25
RHipPitch 	Right hip joint front and back (Y) 	-104.5 to 28.5
RKneePitch 	Right knee joint (Y) 	-5 to 125
RAnklePitch 	Right ankle joint front and back (Y) 	-70.5 to 54
RAnkleRoll 	Right ankle joint right and left (X) 	-25 to 45
Note: 	

* LHipYawPitch and RHipYawPitch are physically just one motor so they cannot be controlled independently. 
*/

/**********************************************************/
vector<float> NaoGesture::LeftArm(GesturePhase *phase, vector<float> Vec6, float *lHand)
{
	/* Huong dan: 
		Doi voi Greta thi can co 3 thong tin:
		Co tay - WRIST, Ban tay - HAND va Canh tay - ARM.
		Thuc chat co the quy ra gom 2 thong tin chinh: vi tri
		cua ban tay va hinh dang cua ban tay (va huong ban tay).
		Doi voi NAo, truoc mat, minh se chi quan tam den vi tri
		cua ban tay.
		Nghia la toa do X,Y,Z cua thanh phan ARM

		Tien hanh: 
		Buoc 1. Chi su dung vi tri cua ban tay
		Buoc 2. Chi su dung huong cua ban tay (hinh dang)

		Va lam the nao de tan dung duoc ca huong nguoi (Torso Orientation)
	*/

	/****1. Hinh dang ban tay****
	form_look_up[]={"form_default","form_fist","form_open",
					"form_point1","form_point2","form_2apart",
					"form_openapart"};

	Khong quan tam den ngon cai (thumb shape)
	*************************/
	if(phase->GetPHand()->type==shape_form) // Truong hop hinh dang cua ban tay duoc dinh nghia theo dang form
	{	
		BasicForm* pForm = (BasicForm*) phase->GetPHand()->shape;

		if(pForm->type == form_open || pForm->type == form_openapart) // close hand form_default, form_fist, form_open, form_point1, form_point2, form_2apart, form_openapart
		{
			*lHand = 1; 
		}
		else // open hand 100%
		{
			*lHand = 0;
		}
	}

	if((phase)->GetPHand()->type==shape_symbol) // Truong hop hinh dang cua ban tay duoc dinh nghia theo dang symbol
	{
		BasicSymbol* pSymbol = (BasicSymbol*) phase->GetPHand()->shape;
		
		if(pSymbol->type==symbol_1_open     // open 70%
			|| pSymbol->type==symbol_2_open
			|| pSymbol->type==symbol_3_open)
		{
			*lHand = 0.7;
		}
		else  // symbol_1_close
			// open 30%
		{
			*lHand = 0.3;
		}
	}		

	/**** 2. Vi tri ban tay****
	    Huong tu trong sang trai(phai): arm_x={"XEP", "XP", "XC", "XCC", "XOppC"};
		Huong len xuong: arm_y={"YUpperEP", "YUpperP", "YUpperC", "YCC", "YLowerC","YLowerP","YLowerEP"};
		Huong ra phia truoc mat: arm_z={"ZNear", "ZMiddle", "ZFar"};		
		C: Center	- Chinh giua
		CC: Center-Center	
		P: Periphery (ngoai bien)
		EP: Extreme Periphery (rat ngoai bien)
		Opp: Opposite Side (phia doi dien, vat cheo sang ben kia)
	****************************/
	//enum ArmX {XEP=0, XP, XC, XCC, XOppC, XDefault};
	//enum ArmY {YUpperEP=0, YUpperP, YUpperC, YCC, YLowerC, YLowerP, YLowerEP,YDefault};
	//enum ArmZ {ZNear=0, ZMiddle, ZFar,ZDefault};

// Z cua Greta la truc X cua Nao
// Y cua Greta la truc Z cua Nao
// X cua Greta la truc Y cua Nao
	

	Vec6[0] = arm_z[phase->GetPArm()->AbstractZ];
	Vec6[1] = arm_x[phase->GetPArm()->AbstractX];
	Vec6[2] = arm_y[phase->GetPArm()->AbstractY];




	/**** 3. Huong long ban tay****
	[WRIST <PalmType> <FingerBaseType>]
	enum PalmType {PalmDefault, PalmUp,PalmDown, PalmAway,PalmTowards, PalmInwards,PalmOutwards,PalmNone};
	enum FingerBaseType {FBDefault, FBUp, FBDown, FBAway, FBTowards, FBInwards, FBOutwards,FBNone};

	****************************/

			return Vec6;
}

/**********************************************************/
vector<float> NaoGesture::RightArm(GesturePhase *phase, vector<float> Vec6, float *rHand)
{
	if(phase->GetPHand()->type==shape_form) // Truong hop hinh dang cua ban tay duoc dinh nghia theo dang form
	{	
		BasicForm* pForm = (BasicForm*) phase->GetPHand()->shape;

		if(pForm->type == form_open || pForm->type == form_openapart) // close hand form_default, form_fist, form_open, form_point1, form_point2, form_2apart, form_openapart
		{
			*rHand = 1; // open hand
		}
		else // close hand
		{
			*rHand = 0;
		}
	}

	if((phase)->GetPHand()->type==shape_symbol) // Truong hop hinh dang cua ban tay duoc dinh nghia theo dang symbol
	{
		BasicSymbol* pSymbol = (BasicSymbol*) phase->GetPHand()->shape;
		
		if(pSymbol->type==symbol_1_open     // open 70%
			|| pSymbol->type==symbol_2_open
			|| pSymbol->type==symbol_3_open)
		{
			*rHand = 0.7;
		}
		else  // symbol_1_close
			// open 30%
		{
			*rHand = 0.3;
		}
	}		

	Vec6[0] = arm_z[phase->GetPArm()->AbstractZ];
	Vec6[1] = (-1)*(arm_x[phase->GetPArm()->AbstractX]);
	Vec6[2] = arm_y[phase->GetPArm()->AbstractY];

	return Vec6;

}

/**********************************************************/

void NaoGesture::Converter(GestureSpace::GesturePhase *phase, std::vector<float> JointValues, SideType side)
{


}

void NaoGesture::Interpret()
{
	std::cout << "Start calculation of joint angles ... " <<endl;
	std::vector<GesturePhase*>::iterator iter;

	// two arms have different movements
	if(gesture->GetSide() == assym) 
	{
		// calculate for the left arm (phasesAssym)
		for(iter=gesture->phasesAssym.begin(); iter!=gesture->phasesAssym.end();iter++)
		{
			vector<float> lVec6(6,0);
			float *lHand = new float(0);
			lVec6 = this->LeftArm(*iter, lVec6, lHand);		
			this->my_robot.make_motion_left_arm(lVec6, lHand);
		}

		// calculate for the right arm (phases)
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vector<float> rVec6(6,0);
			float *rHand = new float(0);
			rVec6 = this->RightArm(*iter, rVec6, rHand);
			this->my_robot.make_motion_right_arm(rVec6, rHand);
		}
	}
	
	// two arms have the same movements
	if(gesture->GetSide()==both_sides)
	{
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vector<float> lVec6(6,0);
			float *lHand = new float(0);
			lVec6 = this->LeftArm(*iter, lVec6, lHand);		
			this->my_robot.make_motion_left_arm(lVec6, lHand);
			//SleepMs(20);
			

			vector<float> rVec6(6,0);
			float *rHand = new float(0);
			rVec6 = this->RightArm(*iter, rVec6, rHand);
			this->my_robot.make_motion_right_arm(rVec6, rHand);

			for(int i=0;i<3;i++) cout<<lVec6[i]<<" : "; cout<<endl;
			for(int i=0;i<3;i++) cout<<rVec6[i]<<" : "; cout<<endl<<endl;
			
			
		}
	}

	// Only left arm
	if(gesture->GetSide()==l)
	{
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vector<float> lVec6(6,0);
			float *lHand = new float(0);
			lVec6 = this->LeftArm(*iter, lVec6, lHand);		
			this->my_robot.make_motion_left_arm(lVec6, lHand);
		}
	}
	
	// Only right arm
	if(gesture->GetSide()==r)
	{
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vector<float> rVec6(6,0);
			float *rHand = new float(0);
			rVec6 = this->RightArm(*iter, rVec6, rHand);
			this->my_robot.make_motion_right_arm(rVec6, rHand);
		}

	}

	// Retrace arm positions

			this->my_robot.motion_PoseZero();

}

void NaoGesture::Print_angles()
{
	this->my_robot.print_body_angles();
}
// Print information of keyframes on the screen
void NaoGesture::PrintScreen()
{
	// Print header information
		std::cout << "GESTURECLASS " << gesture->GetClass() << "\n";
		std::cout << "GESTUREINSTANCE " << gesture->GetInstance() << "\n";
		std::cout << "DURATION " << gesture->GetDuration() << "\n";
		if(gesture->GetSide()==l)
			std::cout << "SIDE LEFT\n";
		if(gesture->GetSide()==r)
			std::cout << "SIDE RIGHT\n";
		if(gesture->GetSide()==both_sides)
			std::cout << "SIDE BOTH\n";
		if(gesture->GetSide()==assym)
			std::cout << "SIDE ASSYM\n";
		
		std::cout << "\n";

		PrintScreenKeyFrames(&gesture->phases);

		if(gesture->GetSide()==assym){
			std::cout << "OTHERARM\n\n";
			PrintScreenKeyFrames(&gesture->phasesAssym);
		}
}
void NaoGesture::PrintScreenKeyFrames(GesturePhaseVector *phasesVector)
{
	std::cout << "Number of keyframes: " << phasesVector->size() << endl;
		std::vector<GesturePhase*>::iterator iter;
		for(iter=phasesVector->begin();iter!=phasesVector->end();iter++)
		{
			if((*iter)->GetStoredPose()!="")
			{
				std::cout << "STOREDPOSE " << (*iter)->GetStoredPose() << " " << (*iter)->time << "\n";
				continue; 
			}
			std::cout << "SideType: " << (*iter)->GetSide() << "\n";
			std::cout << "STARTFRAME " << (*iter)->time << "\n";
			std::cout << "FRAMETYPE " << phasetype_look_up[(*iter)->type] << "\n";
			
			std::cout << "ARM";

			std::cout <<" " << arm_x_look_up[(*iter)->GetPArm()->AbstractX];

			if((*iter)->GetPArm()->fixedX!=0)
				std::cout << ":FIXED";

			std::cout <<" " << arm_y_look_up[(*iter)->GetPArm()->AbstractY];
			
			if((*iter)->GetPArm()->fixedY!=0)
				std::cout << ":FIXED";
			
			std::cout <<" " << arm_z_look_up[(*iter)->GetPArm()->AbstractZ];
			
			if((*iter)->GetPArm()->fixedZ!=0)
				std::cout << ":FIXED";

			std::cout << "\n";

			if(((*iter)->GetPHand()->type==shape_default)&&((*iter)->GetPHand()->fixedForm==false))
				std::cout << "HAND FORM_DEFAULT ADDNOISE\n";
			if(((*iter)->GetPHand()->type==shape_default)&&((*iter)->GetPHand()->fixedForm==true))
				std::cout << "HAND FORM_DEFAULT:FIXED ADDNOISE\n";

			std::string s,t;

			if((*iter)->GetPHand()->type==shape_form)
			{	
				BasicForm* pForm = (BasicForm*) (*iter)->GetPHand()->shape;

				s=form_look_up[pForm->type];

				std::cout << "HAND " << s;

				if(((*iter)->GetPHand()->fixedForm==true)
					&&((pForm->type==form_fist)||(pForm->type==form_open)))
					std::cout << ":FIXED";

				if(pForm->thumb==thumb_default)
					s="thumb_default";
				if(pForm->thumb==thumb_away)
					s="thumb_away";
				if(pForm->thumb==thumb_over)
					s="thumb_over";

				std::cout << " " << s << " ADDNOISE\n";
			}

			if((*iter)->GetPHand()->type==shape_symbol)
			{
				BasicSymbol* pSymbol = (BasicSymbol*) (*iter)->GetPHand()->shape;
				if(pSymbol->type==symbol_default)
					s="symbol_default";
				if(pSymbol->type==symbol_1_open)
					s="symbol_1_open";
				if(pSymbol->type==symbol_2_open)
					s="symbol_2_open";
				if(pSymbol->type==symbol_3_open)
					s="symbol_3_open";
				if(pSymbol->type==symbol_1_closed)
					s="symbol_1_closed";
				if(pSymbol->type==symbol_2_closed)
					s="symbol_2_closed";
				if(pSymbol->type==symbol_3_closed)
					s="symbol_3_closed";
				
				if(s.find("open")!=std::string::npos)
				{
					if(pSymbol->opentype==open_default)
						t="open_default";
					if(pSymbol->opentype==open_thumbout)
						t="open_thumbout";
					if(pSymbol->opentype==open_thumbin)
						t="open_thumbin";
				}
				else
				{
					if(pSymbol->opentype==closed_default)
						t="closed_default";
					if(pSymbol->opentype==closed_straight)
						t="closed_straight";
					if(pSymbol->opentype==closed_inside)
						t="closed_inside";
					if(pSymbol->opentype==closed_tight)
						t="closed_tight";
				}
				std::cout << "HAND " << s << " " << t << " ADDNOISE\n";
			}

			s=fingerbase_look_up[(*iter)->GetPWrist()->GetFromFingerBase()];

			t=palm_look_up[(*iter)->GetPWrist()->GetFromPalm()];

			std::cout << "WRIST " << s << " " << t << " ";

			if((*iter)->GetPWrist()->GetWristMagnitude()<1)
			{
				s=fingerbase_look_up[(*iter)->GetPWrist()->GetFingerBase()];

				t=palm_look_up[(*iter)->GetPWrist()->GetPalm()];

				std::cout << s << " " << t << " " << (*iter)->GetPWrist()->GetWristMagnitude() << "\n";
			}

			std::cout << "\n";
			if((*iter)->HasTransition())
				std::cout << ((*iter)->GetCurvedTransition()->toString(true));
			std::cout << "ENDFRAME\n\n";
		}
}

// Read information of keyframes from a gesture file.
void NaoGesture::LoadFile()
{
	Fl_File_Chooser *chooser;
	chooser= new Fl_File_Chooser("gestures","gesture files (*.txt)",0,"select a gesture file");
	chooser->show();
	while (chooser->shown())
		Fl::wait();

	if(chooser->value()==0)
	{
		return;
	}

    //gesturereader = new GestureReader();

	delete gesture;

	gesture=new Gesture();

	gesture->Load(chooser->value());
	
	phasesVector = &(gesture->phases);
	if(phasesVector->empty()){
		GesturePhase *phase=new GesturePhase();
		phase->Start();
		phase->Finalize();
		phasesVector->push_back(phase);
	}
	if(gesture->phasesAssym.empty()){
		GesturePhase *phase=new GesturePhase();
		phase->Start();
		phase->Finalize();
		gesture->phasesAssym.push_back(phase);
	}
}
