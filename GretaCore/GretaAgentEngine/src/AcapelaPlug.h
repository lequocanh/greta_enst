#define USE_ACAPELA

/********************
 * example to use AcapelaPlug :
 ********************
 * first, you need to uncomment #define USE_ACAPELA
 * (first line in AcapelaPlug.h)
 * then, in your programme :
 ********************

#include "AcapelaPlug.h"
#include <stdio.h>

int main (int argc, char *argv[]){
#ifdef USE_ACAPELA
	//initialize dlls (it takes around 250 ms). must be call once (but this function checks it and is call it in the contructor of AcapelaPlug).
	AcapelaPlug::initAcapela();

	//Create the Acapela plug with the absolute path of a destination audio file (we can not do otherwise)
	AcapelaPlug plug ("C:/toto.wav");

	//choose a voice
	if( ! plug.setVoice("Julie")) //try one voice
		plug.setVoice("french", "female");//if not found we search a corresponding voice

	//send the text (in SAPI format)
	std::string textInSAPI = "\\mrk=0\\ce text \\mrk=1\\ doit �tre prononc�. \\mrk=2\\est-ce que vous l'entendez\\mrk=3\\? ";
	plug.launch(textInSAPI);

	//get the viseme list
	printf("visemes :\n");
	std::vector<ACAPELA_VISEME> visemes = plug.getVisemeList();
	for(int i=0; i<visemes.size();++i)
		printf("viseme %s   duration %d\n", visemes[i].viseme, visemes[i].duration); //be carefull : somme voice bugs with time markers (we have not any viseme after the first time marker)

	//get the time markers
	printf("time markers :\n");
	std::vector<ACAPELA_TM> tm = plug.getTimeMarkerList();
	for(int i=0; i<tm.size();++i)
		printf("num %d   when %d\n", tm[i].num, tm[i].when); //I don't know why the first marker was added many times

	//get the audio buffer
	char * buffer = 0;
	int size = 0;
	plug.getAudio(buffer, &size);
#else
	//do what you want without Acapela
#endif
	//that's all folks
	char a=' ';
	scanf(&a);
}
*/
#ifdef USE_ACAPELA
#ifndef _ACAPELAPLUG_
#define _ACAPELAPLUG_

#include <windows.h>
#include <string>
#include <map>
#include <vector>
#include "Acapela\iobabtts.h"

typedef struct {
	char* viseme;
	long duration;
} ACAPELA_VISEME;

typedef struct {
	long num;
	long when;
} ACAPELA_TM;


class AcapelaPlug{
//static field :
private:
	static bool initialized;
	static std::string pathAcapela;
	static std::map<std::string,std::string> langMap;
	static LPBABTTS babtts;
public:
	static void initAcapela();
	static char* sapi2greta[22];
	static AcapelaPlug* current;
	static BabTtsError WINAPI acapelaEvents(LPBABTTS lpBabTTS, unsigned long Msg, unsigned long dwCallbackInstance, unsigned long dwMsgInfo);

//members :
private:
	std::string voice;
	std::string audioFileName;
	long timecount;
	std::vector<ACAPELA_TM> tmList;
	std::vector<ACAPELA_VISEME> visemeList;

public:
	AcapelaPlug(std::string audioFile);
	bool setVoice(std::string language, std::string gender);
	bool setVoice(std::string name);
	void launch(std::string textSAPI);
	void getAudio(char** buffer,int* size);
	std::string getAudioFileName(){return audioFileName;}
	std::vector<ACAPELA_TM> getTimeMarkerList() {return tmList;}
	std::vector<ACAPELA_VISEME> getVisemeList() {return visemeList;}
};
#endif //ifndef _ACAPELAPLUG_
#endif //ifdef USE_ACAPELA