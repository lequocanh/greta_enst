//
//////////////////////////////////////////////////////////////////////

#include "RealtimeBML.h"
#include "FAPwriter.h"
#include "RandomGen.h"
#include "FileNames.h"
#include "IdleMovements.h"
#include "bapanimationjoiner.h"
#include <string>
#include <vector>
#include <math.h>
#include <time.h>

#include <stdio.h>


#include <iostream>
#include <fstream>

#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;

#define MAXFRAMESENT 10

extern IniManager inimanager;
extern FileNames filenames;
RandomGen *randogen;
RandomGen *rando;

//timeat 1000?
int DELAY = 300;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RealtimeBML::RealtimeBML(std::string name, std::string host, int port, std::string GretaName) : Psydule(name, host, port)
{
	randogen = new RandomGen();

	previous_time=0;
	std::list<std::string> datatypes;
	fapframes=0;
	bapframes=0;

	fapframeswithoutnoise=0;
 	filenames.Base_File="output/RealtimeBML";
	/*this->name=name;
	this->host=host;
	this->port=port;*/
	this->GretaName=getGretaName(GretaName);


	datatypes.push_back(this->GretaName+CLOCK_MESSAGE_HEADER);
	datatypes.push_back(this->GretaName+BML_MESSAGE_HEADER);
			
	if(Register(this->GretaName+GRETA_WHITEBOARD,datatypes)!=0)
	{
		printf("registered to Psyclone\n");
	}
	else
	{
		printf("could not register to Psyclone\n");
		exit(0);
	}

	FPS=25.0f;

	NoEndFaceSignals = false;

	ReadMessage(ReceiveMessage(2200));

	last_fap_frame_time=pc.GetTime();
	last_bap_frame_time=pc.GetTime();


	if(inimanager.GetValueInt("REALTIME_GESTUREENGINE")==0)
		last_bap_frame=0;
	else
		last_bap_frame=new BAPFrame();

	DELAY = inimanager.GetValueInt("REALTIME_DELAY");

}

RealtimeBML::~RealtimeBML()
{
	//fclose(fid);
	//delete(commit);
}

void RealtimeBML::MainLoop()
{
	printf("\nRealtimeBML running...\n");
	long long lasttime=pc.GetTime();
	long long time;
	
	while(true)
	{
		Sleep(5);

		CommunicationMessage *msg_last = NULL;
		msg_last=ReceiveMessage(5);
		ReadMessage(msg_last);

		SendIdle();

		std::list<GretaLogger*>::iterator iterlog;

		for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
		{
			std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
			if((*iterlog)->getLevel()=="debug")
				this->log->debug(msg);
			if((*iterlog)->getLevel()=="info")
				this->log->info(msg);
			if((*iterlog)->getLevel()=="warn")
				this->log->warn(msg);
			if((*iterlog)->getLevel()=="error")
				this->log->error(msg);
		}
		listLog.clear();
	}
}

int RealtimeBML::ReadMessage(CommunicationMessage *msg)
{
	std::string content;

	if (msg!=NULL)
	{

		
		if(strcmp(msg->getType().c_str(),(GretaName+CLOCK_MESSAGE_HEADER).c_str())==0)
		{
			int time;
			time=atoi(msg->getContent().c_str());		
			pc.SetTime(time);
			if (previous_time> time)
			{
				last_fap_frame_time=time;
				last_bap_frame_time=time;			
				fapframes=0;
				fapframeswithoutnoise=0;
				bapframes=0;
				all_bapframes=0;
			}	
			previous_time=time;
			return 1;
		}
		
		if(strcmp( msg->getType().c_str(), (GretaName+BML_MESSAGE_HEADER).c_str())==0)
		{
			
			//filenames.Phonemes_File="";
			content=msg->getContent();
			
					
			findCurrentFrame(fapframes);		

			if(bmlengine.Execute("",(char *)content.c_str(),0,last_bap_frame,&last_fap_frame,true)!=0)
			{
				if(inimanager.GetValueInt("REALTIME_GESTUREENGINE")==1)
					bmlengine.GetMPlanner()->GetCopyLastBAPFrame(last_bap_frame);
				//is an infinite signal?
				NoEndFaceSignals=false;
				
				
				//BE CAREFUL MIGHT NOT WORK PROPELLY WITH THIS LINE COMENTED
			
				if(inimanager.GetValueInt("MCDATA")==0)
				{
					NoEndFaceSignals=bmlengine.hasNoEndFaceSignals();
				}

				faceengine=bmlengine.GetFaceEngine();
				fapframes=faceengine->GetAnimation();
				fapframeswithoutnoise=faceengine->GetAnimationWithoutNoise();
		

				mplanner=bmlengine.GetMPlanner();
				bapframes=mplanner->GetAnimation();			

				if (NoEndFaceSignals==true)
				{	
					//cut the animation
					int size=fapframes->size();
					fapframes->resize(floor((double)(size/2)));
					fapframeswithoutnoise->resize(floor((double)(size/2)));

				}
	
				int start_time = pc.GetTime()+DELAY;
								
				if (bmlengine.speech!=0) 
				{				
					//sound_file_name.copy(iter->reference.c_str());													 				

					char str[20];
					std::string command;		
					
					sprintf(str,"%d",start_time+ (int) (bmlengine.speech_delay*1000));
										
					command="PLAYWAVAT ";
					command+=str;
					
					PostString(command.c_str(), GretaName+GRETA_WHITEBOARD, GretaName+REALTIME_PLAYER_COMMAND);
					//this->SendBinaryFile(filenames.Wav_File, GretaName+GRETA_WHITEBOARD, GretaName+DATA_WAV);					
										

					PostSpeechFromChar(bmlengine.content_of_speech, GretaName+GRETA_WHITEBOARD, GretaName+DATA_WAV,&bmlengine.speech_size);																	
					//free (bmlengine.content_of_speech);
					
				}
				SendKeyFramesToNao(start_time);
				SendFAPsBAPs(start_time,last_fap_frame_time,last_bap_frame_time);			
			}
		}
		return 1;
	}
	else
		return 0;
}

int RealtimeBML::SendIdle()
{
	
	if (pc.GetTime() > last_fap_frame_time) 
	{
		
		//if ((fapframes!=0)&&(fapframes->empty()==false)) lastframe=(*((fapframes->end())-1)).clone();
		findCurrentFrame(fapframeswithoutnoise);

		IdleMovements *idlemovements = new IdleMovements();
		//generate five seconds of continuos expressions
		idlemovements->generate(last_fap_frame,5.0);

		if (fapframes!=0)
			fapframes->clear();
		
		fapframes=idlemovements->GetAnimation();
		fapframeswithoutnoise=idlemovements->GetAnimationWithoutNoise();

		int start_time = 0;

		if (last_fap_frame_time==0.0f)
			start_time=pc.GetTime() + DELAY;
		else
			start_time = last_fap_frame_time;
	

		if(bapframes!=0)
			bapframes->clear();

		SendFAPsBAPs(start_time,last_fap_frame_time,last_bap_frame_time);
		
	}
	
	return 1;
}


void RealtimeBML::findCurrentFrame(FAPFrameVector *frames)
{

	last_fap_frame.ActivateAndSetAllFAPsTo(0);

	//if something was displayed before find a start frame of new animation
	if	((frames!=0) && (!frames->empty())) 
	{
								
		//it can be last frame of the prevoius animation				
		last_fap_frame=(*((frames->end())-3)).clone();
	

		//add condition if the animation is not finished yet
		//if (last_frame_time > pc.GetTime() + DELAY) {
								
		//or it can be the frame that is displayed at the moment
		int  index = (int)(last_fap_frame_time - pc.GetTime() -DELAY)*FPS/1000;				
						
		if (((*frames).size()>index)&&(index > (int)0 ))  
			last_fap_frame=(*frames)[(*frames).size()-index].clone();															
		
	}
				
}
void RealtimeBML::SendKeyFramesToNao(int start_frame_time)
{
	std::string msg, label;
	int frameind = 0;
	char tm[256];
	int i;
	int maxlength;
	clock_t leftarmtimeat, rightarmtimeat;
	
	
	BAPFrameVector* leftArmKeys = this->bmlengine.GetMPlanner()->GetLeftArmKeys();
	BAPFrameVector* rightArmKeys = this->bmlengine.GetMPlanner()->GetRightArmKeys();

	BAPAnimationJoiner paj;
	BAPFrameVector* KeyFrames = paj.Join(leftArmKeys,rightArmKeys );

	sprintf(tm,"%d",pc.GetTime());

	label=tm;

	while(frameind<KeyFrames->size())
	{
		msg="";
		msg+=label+"\n";

		//printf("label: %s\n", tm);

		for(i=0;i<5;i++) 
			{ 
				if(i+frameind < (*KeyFrames).size())
				{
					
					msg+=(*KeyFrames)[i+frameind]->WriteBAP();
				}
			}
			//int Psydule::PostString(std::string tosend, std::string whiteboard, std::string datatype)
			
			PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+BAP_KEY_FRAME);					
	
			frameind+=5;
	}
	
}

void RealtimeBML::SendFAPsBAPs(int start_frame_time,int &lastfapframetime,int &lastbapframetime)
{
	//int last_frame_number;
	std::string msg,label;
	int frameind;
	char tm[256];
	int i;
	int maxlength;
	clock_t faptimeat,baptimeat;


	//FAPwriter fw;

	//BAPwriter bw;
	//bw.WriteToFile(bapframes, "provabap.bap");

	if (start_frame_time==-1) faptimeat=pc.GetTime()+DELAY;
	else faptimeat=start_frame_time;

	baptimeat=faptimeat;

	sprintf(tm,"%d",pc.GetTime());

	label=tm;

	msg="DELETEDIFF ";

	msg+=label;

	msg+=" ";

	sprintf(tm,"%d",faptimeat-1);

	msg+=tm;


	//printf("MASSAGE TO SEND: \n%s\n", msg.c_str());
	// Send this message in order to start animations of Greta
	PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+REALTIME_PLAYER_COMMAND);

	msg="";

	frameind=0;

	maxlength=0;

	if((bapframes!=0)&&(fapframes!=0))
	{
		if((*bapframes).size()>(*fapframes).size())
			maxlength=(*bapframes).size();
		else
			maxlength=(*fapframes).size();
	}
	else
	{
		if(bapframes!=0)
			maxlength=(*bapframes).size();
		if(fapframes!=0)
			maxlength=(*fapframes).size();
	}

	while(frameind<maxlength)
	{
		if(fapframes!=0)
		{
			msg="";

			msg+=label+"\n";

			//printf("label: %s\n", tm);

			for(i=0;i<MAXFRAMESENT;i++)
			{
				if ((i+frameind)<(*fapframes).size())
				{
					(*fapframes)[i+frameind].framenumber=faptimeat;
					msg+=(*fapframes)[i+frameind].toString();
					//last_frame_number=(i+frameind);
				}
				else
					break;
				faptimeat+=40;
			}
			PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+FAP_FRAME);
		}

		if(bapframes!=0)
		{
			msg="";

			msg+=label+"\n";

			for(i=0;i<MAXFRAMESENT;i++)
			{

				if ((i+frameind)<(*bapframes).size())
				{
					(*bapframes)[i+frameind]->SetFrameNumber(baptimeat);
					msg+=(*bapframes)[i+frameind]->WriteBAP();
				}
				else
					break;
				baptimeat+=40;

				
			}

			PostString(msg, GretaName+GRETA_WHITEBOARD, GretaName+BAP_FRAME);
		}

		frameind+=MAXFRAMESENT;
	}

	//return last_frame_number/FPS*1000+faptimeat;
	lastfapframetime=faptimeat-40;
	lastbapframetime=baptimeat-40;
 
}

