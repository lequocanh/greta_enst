#include<cassert>

template<class T> 
class PolyIterator {
public:
	class implement	{
	public:
		virtual ~implement() { }
		virtual implement * clone() const = 0;
		virtual bool equal(const implement&) const = 0;
		virtual T get() const = 0;
		virtual void next() = 0;
		//virtual void prev() = 0;
	};

public:
	typedef std::forward_iterator_tag iterator_category;
	typedef T value_type ;
	typedef T & reference ;
	typedef T * pointer ;

	// take ownership of object	
	PolyIterator(implement * iter) : d_iter(iter) {}

	// Copy constructor
	PolyIterator(const PolyIterator& that) :
		d_iter((that.d_iter)->clone()) { }

	PolyIterator& operator=(const PolyIterator& that);

	~PolyIterator() { delete d_iter; }

	bool operator==(const PolyIterator& that) const
	{ same(*this,that); return this->d_iter->equal(*that.d_iter); }

	bool operator!=(const PolyIterator& that) const 
	{ return ! (*this == that); }

	T operator*() const { return d_iter->get(); }
	
	pointer operator->() const { return &d_iter->get();	}

	PolyIterator& operator++() { d_iter->next(); return *this; }


	template <class Iter>
	class usualimplement : public implement {
	public:
		typedef Iter actual_iterator;

		explicit usualimplement(Iter iter) : d_iter(iter) { }
		
		usualimplement * clone() const {
			return new usualimplement(*this);
		}
		
		bool equal(const implement& that) const {
			return (this->d_iter == 
				    static_cast<const usualimplement&>(that).d_iter);
		}

		T get() const { return *d_iter; } // should be T & (MSVC sux)

		void next() { ++d_iter; }

		//void prev() { --d_iter; }
	private:
		Iter d_iter;
	};

private:
	implement * d_iter; // dynamically created object

	static void same(const PolyIterator& lhs, const PolyIterator& rhs) {
		assert(typeid(*lhs.d_iter) == typeid(*rhs.d_iter));
	}
};
