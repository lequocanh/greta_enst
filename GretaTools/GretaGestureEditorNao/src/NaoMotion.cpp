#include "NaoMotion.h"
#include "XercesTool.h"
#include "IniManager.h"

#include "XMLDOMParser.h"
#include "XMLDOMTree.h"
/*
#include <iostream>
#include <alproxy.h>
#include <alptr.h>
#include <albroker.h>
#include <almodule.h>
#include <alloggerproxy.h>

#include <fstream>
*/
using namespace AL;

extern IniManager inimanager;	

/**********************************************/
NaoMotion::NaoMotion()
{
	//parentBrokerIP="127.0.0.1";
	//parentBrokerPort=9559;

 	parentBrokerIP=inimanager.GetValueString("NAO_HOST").c_str();
	parentBrokerPort=inimanager.GetValueInt("NAO_PORT");
}
/**********************************************/
void NaoMotion::motion(int x, int y, int z, float *hand_open, float *hand_dir, std::string side, float TMP)
{
	std::cout <<x<<":"<<y<<":"<<z<<":"<<joints[x][y][z][0]<<std::endl;
	std::cout <<x<<":"<<y<<":"<<z<<":"<<joints[x][y][z][1]<<std::endl;
	std::cout <<x<<":"<<y<<":"<<z<<":"<<joints[x][y][z][2]<<std::endl;
	std::cout <<x<<":"<<y<<":"<<z<<":"<<joints[x][y][z][3]<<std::endl;

	if(x>4) x=4;
	if(y>6) y=6;
	if(z>2) z=2;
	if(x<0) x=0;
	if(y<0) y=0;
	if(z<0) z=0;
	
	AL::ALValue names;
	AL::ALValue angleLists;

	if(side == "left_arm")
	{
		names.arrayPush(AL::ALValue("LElbowRoll"));
		names.arrayPush(AL::ALValue("LElbowYaw"));
		names.arrayPush(AL::ALValue("LShoulderPitch")); // khong doi cho ca 2 tay
		names.arrayPush(AL::ALValue("LShoulderRoll"));
		names.arrayPush(AL::ALValue("LHand"));
		names.arrayPush(AL::ALValue("LWristYaw"));
	 

		angleLists.arrayPush(AL::ALValue(joints[x][y][z][0]*PI/180));
		angleLists.arrayPush(AL::ALValue(joints[x][y][z][1]*PI/180));
		angleLists.arrayPush(AL::ALValue(joints[x][y][z][2]*PI/180));
		angleLists.arrayPush(AL::ALValue(joints[x][y][z][3]*PI/180));

		angleLists.arrayPush(AL::ALValue(*hand_open));
		angleLists.arrayPush(AL::ALValue(*hand_dir*PI/180));
	}else if(side == "right_arm")
	{
		names.arrayPush(AL::ALValue("RElbowRoll"));
		names.arrayPush(AL::ALValue("RElbowYaw"));
		names.arrayPush(AL::ALValue("RShoulderPitch")); // khong doi cho ca 2 tay
		names.arrayPush(AL::ALValue("RShoulderRoll"));

		names.arrayPush(AL::ALValue("RHand"));
		names.arrayPush(AL::ALValue("RWristYaw"));
	 
		angleLists.arrayPush(AL::ALValue((-1)*joints[x][y][z][0]*PI/180));
		angleLists.arrayPush(AL::ALValue((-1)*joints[x][y][z][1]*PI/180));
		angleLists.arrayPush(AL::ALValue(joints[x][y][z][2]*PI/180));
		angleLists.arrayPush(AL::ALValue((-1)*joints[x][y][z][3]*PI/180));

		angleLists.arrayPush(AL::ALValue((*hand_open)));
		angleLists.arrayPush(AL::ALValue(*hand_dir*PI/180*(-1)));
	}


    proxy->pCall("angleInterpolation", names, angleLists,movement_time,true);	


	
}
/**********************************************
* 000 = XEP YUpperEP ZFar, ... 462 = XOppC YLowerEP ZNear 
* joints[x][y][z] = ("LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll")
***********************************************/
void NaoMotion::LoadArmPositions()
{
	std::string name;
	float temp[4];
	const char *look_up_joints[]={"LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll"};
	std::string gestuary_path = "Nao/armpositions.xap";//"C:/Program Files (x86)/Aldebaran/SDK 1.6.0/share/choregraphe/libraries/positions/armpositions.xap";
	XMLDOMParser *xmlparser;
	xmlparser=new XMLDOMParser();
	
	XMLGenericTree *xmltree=xmlparser->ParseFile(gestuary_path);
	
	
	if(xmltree==0)	return 	;

	
	XMLGenericTree *chld1;
	int i=0;
	for(XMLGenericTree::iterator iter1=xmltree->begin();iter1!=xmltree->end();++iter1)
	{		
		chld1 = *iter1;		
		if (chld1->isTextNode()) continue;
		//std::cout << chld1->GetName()<<std::endl;
		
		XMLGenericTree *chld2;
		for(XMLGenericTree::iterator iter2=(*iter1)->begin(); iter2 != (*iter1)->end(); ++iter2)		
		{
			chld2=*iter2;
			if (chld2->isTextNode()) continue;
			//std::cout << chld2->GetName()<<std::endl;

			XMLGenericTree *chld3;
			if(chld2->GetName()=="Motors")
			{
				// children of Motors
				for(XMLGenericTree::iterator iter3=(*iter2)->begin(); iter3 != (*iter2)->end(); ++iter3)		
				{
					chld3 = *iter3;
					if (chld3->isTextNode()) continue;
					
					XMLGenericTree *chld4;					
					// children of Motor, that are name and value
					for(XMLGenericTree::iterator iter4=(*iter3)->begin(); iter4 != (*iter3)->end(); ++iter4)		
					{
						chld4 = *iter4;						
						if (chld4->isTextNode()) continue;
						
						XMLGenericTree *chld5;									
						if(chld4->GetName()=="name")
						{							
							for(XMLGenericTree::iterator iter5=(*iter4)->begin(); iter5 != (*iter4)->end(); ++iter5)		
							{						
								chld5=*iter5;							
								name = chld5->GetTextValue();							
							}
						}else
						{//const char *look_up[]={"LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll"};					
							XMLGenericTree *chld6;
							for(XMLGenericTree::iterator iter5=(*iter4)->begin(); iter5 != (*iter4)->end(); ++iter5)		
							{						
								chld6=*iter5;							
								//std::cout << chld6->GetTextValue()<< std::endl;							
								for(int i=0;i<4;i++)
									if(look_up_joints[i]==name)
									{
										std::cout << name<< "  "<<chld6->GetTextValue()<<std::endl;
										temp[i] = atof(chld6->GetTextValue().c_str());
									}
							}
						
						}
						
					}
					
				}
			}
			if(chld2->GetName()=="name")
			{
				std::cout << "lam viec voi ten o day: ";
				XMLGenericTree *chldname;
				for(XMLGenericTree::iterator iter=(*iter2)->begin(); iter != (*iter2)->end(); ++iter)		
				{						
					chldname=*iter;
					std::cout << chldname->GetTextValue()<<std::endl;
					
					int intReturn = atoi(chldname->GetTextValue().c_str());
					int x = intReturn / 100;
					int y = (intReturn - x*100) / 10;
					int z = (intReturn - x*100 - y*10);
					std::cout <<x<<":"<<y<<":"<<z<<endl;
					for(int i=0;i<4;i++) joints[x][y][z][i] = temp[i];
					// x = intReturn / 100; y = intReturn/100/10,..
					
				}

			}

		}
		i++; 
	}

	//test
	for(int x=0;x<5;x++)
		for(int y=0;y<7;y++)
			for(int z=0;z<3;z++)
			{
				std::cout << x <<":"<<y<<":"<<z<<":";
				for(int i=0;i<4;i++)
					std::cout<<joints[x][y][z][i]<<" - ";
				std::cout<<std::endl;
			}
//	std::cout << i<< std::endl;


	delete xmlparser;
	delete xmltree;	

}
/**********************************************/
void NaoMotion::SetIP(std::string ip)
{
	parentBrokerIP=ip;
}

/**********************************************/
void NaoMotion::Setport(int port)
{
	parentBrokerPort=port;
}

/**********************************************/
NaoMotion::NaoMotion(std::string IP, int Port)
{
	parentBrokerIP = IP;
	parentBrokerPort = Port;

	proxy = NULL;
	audioProxy = NULL;
	ALTextToSpeechProxy = NULL;
	ad = NULL;
}

/**********************************************/
void NaoMotion::Init()
{
	LoadArmPositions();
	movement_time = 0.7;
	// for MOTION
	try
	{
		proxy  = new AL::ALProxy("ALMotion", parentBrokerIP, parentBrokerPort);
		std::cout << "Loading MotionProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
	// This will throw an exception
	}
	catch( AL::ALError &err )
	{
		std::cout << err.toString() << std::endl;
	}

	// for AUDIO
	try
	{
		//audioProxy  = new AL::ALProxy("ALAudioPlayer", parentBrokerIP, parentBrokerPort);
		std::cout << "Loading AudioProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
	// This will throw an exception
	}
	catch( AL::ALError &err )
	{
		std::cout << err.toString() << std::endl;
	}

/*******************************/
	try
	{
		//ad  = new AL::ALProxy("ALAudioDevice", parentBrokerIP, parentBrokerPort);
		std::cout << "Loading AudioDeviceProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
	// This will throw an exception
	}
	catch( AL::ALError &err )
	{
		std::cout << err.toString() << std::endl;
	}

  /*
  const int outputBufferSize=11116384;   // maximum buffer size that ALAudioDevice can send
  const int numberOfOutputChannels=2; // number of output channels on Nao
  
  // --------- opening of the input file --------------------------------------------------------
  FILE *fInputWavFile;
  std::string pFileName = "C:/LEQUOCANH/GRETA/bin/buffer.wav";
  if ( ( fInputWavFile = fopen ( pFileName.c_str(), "rb" ) ) == NULL )
  {
      throw AL::ALError ( "ALHelloWorld", "helloWorld", "Failed to open input sound file." );
  }
  // --------- reading of the wav header ---------------------------------------------------------
  wavheader *wavHeader=new wavheader;
  fread(wavHeader,1,44,fInputWavFile);//fread(wavHeader,1,44,fInputWavFile);
  unsigned short nbOfChannels = wavHeader->NumChannels;
  unsigned long sampleRate = wavHeader->SampleRate;
  unsigned short bitsPerSample = wavHeader->BitsPerSample;
   
  // ------- set output sample rate of audiodevice -------------------------------------------------------------------
  
  ad->callVoid("setParameter",std::string("outputSampleRate"),(int) sampleRate); 
  
  // ------- go to the beginning of the audio data -------------------------------------------------------------------
  fseek (fInputWavFile , 44 , SEEK_SET );//fseek (fInputWavFile , 44 , SEEK_SET );
  
  // --------------- reading of the audio data
  short *fInputAudioData=new short[outputBufferSize*nbOfChannels]; // buffer to store the data contained in the wav file
  short *fStereoAudioData=new short[outputBufferSize*numberOfOutputChannels]; // buffer to construct stereo audio data 
  ALValue pDataBin; // ALValue where to store the stereo audio data to send

  while ( !feof ( fInputWavFile ) )
  {
    //Read samples from file
    int fNbOfInputSamples = fread ( fInputAudioData,bitsPerSample/8*nbOfChannels,outputBufferSize,fInputWavFile );

    if (nbOfChannels==1)
    {  
      // construction of stereo audio data in case the input file is mono   
      int i=0;
      for (int j=0;j<fNbOfInputSamples;j++)
      {
        fStereoAudioData[i]=fInputAudioData[j]; 
        fStereoAudioData[i+1]=fInputAudioData[j];
        i+=numberOfOutputChannels;
      }
      pDataBin.SetBinary( fStereoAudioData, fNbOfInputSamples*sizeof(short)*numberOfOutputChannels ); // transform audio data to binary samples
    }
    else
    {
      pDataBin.SetBinary( fInputAudioData, fNbOfInputSamples*sizeof(short)*numberOfOutputChannels ); // transform audio data to binary samples  
    }

    // ------------------------ send the stereo audio data to audiodevice module ----------------------------------------
    ad->call <bool> ("sendRemoteBufferToOutput",fNbOfInputSamples,pDataBin); 

  }
  
  fclose(fInputWavFile);
  ad->callVoid("stopAudioOut"); // to clean audio output buffer

/*******************************/

	// for Text to Speech
	try
	{
		//ALTextToSpeechProxy  = new AL::ALProxy("ALTextToSpeech", parentBrokerIP, parentBrokerPort);
		std::cout << "Loading ALTextToSpeech is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
	// This will throw an exception
	}
	catch( AL::ALError &err )
	{
		std::cout << err.toString() << std::endl;
	}
 //-------------- Joint values for initial position -----------
		// Feel free to experiment with these values
	float kneeAngle  = 40;
	float torsoAngle =  0;
	float wideAngle  =  0;
	std::string pNames = "Body";

		// set values in degree
	float initPosition26[] = 
	{0, 0, // Head
	120,  15, -90, -80, 0, 0, // LeftArm
	0,  wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2, -wideAngle, // LeftLeg
	0, -wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2,  wideAngle, // RightLeg
	120, -15,  90,  80, 0, 0}; // RightArm	

	// convert to radian
	InitPosition.resize(26,0.0);
	for(int i=0;i<26;i++)InitPosition[i] = (float)(initPosition26[i]*PI)/180 ;	
		
	//---------------- Joint values for relax position ------------
	ZeroPosition.resize(26,0.0);
	float PositionRad[] = {0.0, 0.015298, 1.43271, 0.12728, -0.675002, -1.07069, -1.65523, 0.0, -0.300625, -0.0783416, -0.760086, 2.18445, -1.2242, 0.103589, -0.300625, 0.0537223, -0.771299, 2.19323, -1.22272, -0.0998575, 1.55398, -0.00464396, 0.935698, 1.03549, 1.12285, 0.0};
	for(int i=0;i<26;i++) ZeroPosition[i]=PositionRad[i];

}

/**********************************************/
void NaoMotion::StiffnessOn()
{
	//We use the "Body" name to signify the collection of all joints
	std::string pNames = "Body";
	float pStiffnessLists = 1.0;
	float pTimeLists = 1.0;
	proxy->callVoid("stiffnessInterpolation", pNames, pStiffnessLists, pTimeLists);
}

/**********************************************/
void NaoMotion::StiffnessOff()
{
	//We use the "Body" name to signify the collection of all joints
	std::string pNames = "Body";
	float pStiffnessLists = 0.0;
	float pTimeLists = 1.0;
	proxy->callVoid("stiffnessInterpolation", pNames, pStiffnessLists, pTimeLists);
}

/**********************************************/
void NaoMotion::PoseInit()
{
	//----------- send the commands --------------
	std::string pNames = "Body";
	//We set the fraction of max speed
	float pMaxSpeedFraction = 0.2;
	//Ask motion to do this with a blocking call	
	proxy->pCall("angleInterpolation",pNames, InitPosition, movement_time,true);	
}

/**********************************************/
void NaoMotion::say(std::string filePath)
{
	audioProxy->pCall("playFile",std::string("/home/nao/wav/buffer.wav"));
	//ALTextToSpeechProxy->pCall("say",std::string("Hello, I am Nao robot"));
}
/**********************************************/
void NaoMotion::OpenHand()
{
		//Get the Number of Joints	
	vector<string> jointNames = proxy->call<vector<string>>("getJointNames",string("body"));
	int NumJoints = jointNames.size();
	if(NumJoints>22)
	{
		// put stiffness on LHand
		std::string pNames = "LHand";		
		// method 1		    
			float pMaxSpeedFraction = 0.5;
			proxy->callVoid("stiffnessInterpolation",pNames, 1.0, 1.0);
			proxy->callVoid("angleInterpolationWithSpeed",pNames, 0.5, pMaxSpeedFraction);
			proxy->callVoid("angleInterpolationWithSpeed",pNames, 0.0, pMaxSpeedFraction);
			SleepMs(500); // wait half a second

		// method 2
			proxy->callVoid("openHand",pNames);
			proxy->callVoid("closeHand",pNames);
	}
}

/**********************************************/
void NaoMotion::PoseZero()
{
	//----------- send the commands --------------
	std::string pNames = "Body";
	//We set the fraction of max speed
	float pMaxSpeedFraction = 0.2;
	//Ask motion to do this with a blocking call
	proxy->callVoid("angleInterpolationWithSpeed",pNames, ZeroPosition, pMaxSpeedFraction);	
}

/**********************************************/
void NaoMotion::make_motion_left_arm(vector<float> lVec6, float *lHand)
{
	proxy->pCall("positionInterpolation",string("LArm"),SPACE,lVec6,MASK,SECONDS,true);

	proxy->pCall("changeAngles",string("LHand"),*lHand,0.5);

}

/**********************************************/
void NaoMotion::make_motion_right_arm(vector<float> rVec6, float *rHand)
{
	proxy->pCall("positionInterpolation",string("RArm"),SPACE,rVec6,MASK,SECONDS,true);

	proxy->pCall("changeAngles",string("RHand"),*rHand,0.5); 
}

/**********************************************/
NaoMotion::~NaoMotion(void)
{
	this->PoseZero();
	delete proxy;
	//delete audioProxy;
	//delete ALTextToSpeechProxy;
	//delete ad;	
}
