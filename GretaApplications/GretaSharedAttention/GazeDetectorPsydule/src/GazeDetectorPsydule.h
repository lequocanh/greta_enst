// Shared-Attention system
// Christopher Peters


#if !defined(AFX_REACTIVELISTENEPSYDULE_H__8D6DB6DB_FC9A_432B_8450_4B941018BD20__INCLUDED_)
#define AFXREACTIVELISTENEPSYDULE_H__8D6DB6DB_FC9A_432B_8450_4B941018BD20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <vector>
#include <map>
#include <time.h>
#include "Psydule.h"
#include "InputData.h"
#include "RandomGen.h"
#include "XMLGenericTree.h"
//#include "Module.h"


/*! \brief generation of listener's reactive backchannel signals
	*
	* This class inherits from Psydule class to use all the functions
	* needed to communicate with a Psyclone whiteboared
	*
*/

class GazeDetectorPsydule
{

	private:

	Psydule *commint;
	std::string name, host;
	int port;
	std::list<std::string> datatypes;
	std::string GretaName;

	public:

	GazeDetectorPsydule(std::string name, std::string host, int port);
	virtual ~GazeDetectorPsydule();

	void checkForMessage(int timeout);	//wait ms timeout for a new message
	void sendAdminMessage(std::string msg);
	void sendGazeHeadDirMessage(float headDirx, float headDiry, float dist);

	void registerPsyduleConnection(void);
	
	bool connected;
};

#endif
