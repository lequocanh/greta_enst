//Copyright 1999-2008 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// NI_interface.cpp: implementation of the NI_interface class.
//
// Author: Etienne de Sevin etienne.de-sevin@telecom-paristech.fr
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#include "NInterface.h"
#include <windows.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>


bool affLeg[4];
int num;
std::string scrollValue = "1";
std::string scrollValueNumber = "100 %";
std::string scrollValueNumber1 = "100 %";
std::string scrollValueType = "100 %";
std::string scrollValueType1 = "100 %";
float manNIU = 0.0;
float manNumber = 0.0;
float manNumber1 = 0.0;
float manType = 0.0;
float manType1 = 0.0;
int actManualNIU = 0;
int actManualNumber = 0;
int actManualType = 0;
int actManualAuto = 0;
int actManualNUI = 0;
int actManualEmotion = 0;
int actManualNoM = 0;
int actManualNoBC = 0;
int actManualNoC = 0;
int actManualRandom = 0;


void NIcallback(void *v)
{
	Sleep(40);
	((NInterface *)v)->redraw();
}

void scrollVariation(Fl_Widget* o, void *v)
{
	//value between 0 and 1
	manNIU = -(((Fl_Slider*)o)->value()/10)+1;
	std::ostringstream out;
	//value beetween 1 and -1
	out << (manNIU*2)-1;
	scrollValue = out.str();
	//std::cout << scrollValue << "\n";
}

void scrollVariationNumber(Fl_Widget* o, void *v)
{
	//value between 0 and 1
	manNumber = -(((Fl_Slider*)o)->value()/10)+1;
	std::ostringstream out;
	//value beetween 1 and -1
	out << manNumber*100 << " %";   //(manNumber*2)-1;
	scrollValueNumber = out.str();
	//std::cout << scrollValue << "\n";
}

void scrollVariationNumber1(Fl_Widget* o, void *v)
{
	//value between 0 and 1
	manNumber1 = -(((Fl_Slider*)o)->value()/10)+1;
	std::ostringstream out;
	//value beetween 1 and -1
	out << manNumber1*100 << " %";   //(manNumber*2)-1;
	scrollValueNumber1 = out.str();
	//std::cout << scrollValue << "\n";
}

void scrollVariationType(Fl_Widget* o, void *v)
{
	//value between 0 and 1
	manType = -(((Fl_Slider*)o)->value()/10)+1;
	std::ostringstream out;
	//value beetween 1 and -1
	out << manType*100 << " %";   //(manNumber*2)-1;
	scrollValueType = out.str();
	//std::cout << scrollValue << "\n";
}

void scrollVariationType1(Fl_Widget* o, void *v)
{
	//value between 0 and 1
	manType1 = -(((Fl_Slider*)o)->value()/10)+1;
	std::ostringstream out;
	//value beetween 1 and -1
	out << manType1*100 << " %";   //(manNumber*2)-1;
	scrollValueType1 = out.str();
	//std::cout << scrollValue << "\n";
}

void activateInterest(Fl_Widget* o, void *v)
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualNIU = 1;
	else
		actManualNIU = 0;	
}

void activateNumber(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualNumber = 1;
	else
		actManualNumber = 0;
}


void activateType(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualType = 1;
	else
		actManualType = 0;
}

void activateAuto(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualAuto = 1;
	else
		actManualAuto = 0;
}

void activateNUI(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualNUI = 1;
	else
		actManualNUI = 0;
}

void activateEmotion(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualEmotion = 1;
	else
		actManualEmotion = 0;
}

void activateNoM(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualNoM = 1;
	else
		actManualNoM = 0;
}

void activateNoBC(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualNoBC = 1;
	else
		actManualNoBC = 0;
}

void activateNoC(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualNoC = 1;
	else
		actManualNoC = 0;
}

void activateRandom(Fl_Widget *o, void *v) 
{
	char actValue = ((Fl_Check_Button*)o)->value();
	if(actValue != NULL)
		actManualRandom = 1;
	else
		actManualRandom = 0;
}
void viewCurve(Fl_Widget *o, void *v) 
{

}

NInterface::NInterface():Fl_Double_Window(0,0,680,1000*2/3,"User State")
{
		for(int i=0; i<501; i++) listeNI[i]=0.0;
		for(int i=0; i<501; i++) listeNIHyst[i]=0.0;
		for(int i=0; i<501; i++) listeMoy[i]=0;
		for(int i=0; i<501; i++) listeAr[i]=0;
		for(int i=0; i<501; i++) listeVal[i]=0;
		for(int i=0; i<501; i++) listePot[i]=0;
		for(int i=0; i<4; i++) affLeg[i]=0;
		phase = "";
		anticipation = "";
		actionchosen = "No choice";
		agentstate_buffer = 0.0;
		hysteresis = 0.75;
		interPhase = "  ";
		NIU = 0.0;
		manualNIU = 0.0;
		oldManualNIU = 0.0;
		valueManualNIU = 1.0;
		activeManualNIU = 0;
		oldActiveManualNIU = 0;
		activeManualNumber = 0;
		oldActiveManualNumber = 0;
		manualNumber = 0.0;
		oldManualNumber = 0.0;
		oldManualNumber1 = 0.0;
		valueManualNumber = 1.0;
		valueManualNumber1 = 1.0;
		manualType = 0.0;
		oldManualType = 0.0;
		oldManualType1 = 0.0;
		valueManualType = 1.0;
		valueManualType1 = 1.0;
		activeManualType = 0;
		oldActiveManualType = 0;
		activeManualNoM = 0;
		activeManualNoBC = 0;
		activeManualNoC = 0;
		activeManualRandom = 0;
		activeManualAuto = 0;
		activeManualNUI = 0;
		activeManualEmotion = 0;
		oldActiveManualNoM = 0;
		oldActiveManualNoBC = 0;
		oldActiveManualNoC = 0;
		oldActiveManualRandom = 0;
		oldActiveManualAuto = 0;
		oldActiveManualNUI = 0;
		oldActiveManualEmotion = 0;
		NUIM=0.0;
		NUIMoy=0.0;


		Fl::add_idle(NIcallback, this);
		this->size_range(680,100,680,0);
		legende[0] = "Interest Level";
		legende[1] = "Arousal";
		legende[2] = "Valence";
		legende[3] = "Potency";
		num = 0;
		menu = new Fl_Menu_Bar(0, 0, 680, 25, "Menu");
		menu->add("User States", 0, viewCurve, (void*)this);
		menu->add("View", 0, viewCurve, (void*)this);

		scrollInterest = new Fl_Slider(580, 25, 15, 100);
		scrollInterest->callback((Fl_Callback*)scrollVariation);
		scrollInterest->scrollvalue(0, 1, 0, 11);
		scrollInterest->tooltip("If activated, you define manually the user's interest level");
		scrollNumber = new Fl_Slider(580, 25, 15, 100);
		scrollNumber->callback((Fl_Callback*)scrollVariationNumber);
		scrollNumber->scrollvalue(0, 1, 0, 11);
		scrollNumber->tooltip("If activated, you define manually the number of backchannels (Mimicry and Response Backchannels)");
		scrollNumber1 = new Fl_Slider(580, 25, 15, 100);
		scrollNumber1->callback((Fl_Callback*)scrollVariationNumber1);
		scrollNumber1->scrollvalue(0, 1, 0, 11);
		scrollNumber1->tooltip("If activated, you define manually the number of backchannels (Mimicry and Response Backchannels)");
		scrollType = new Fl_Slider(580, 25, 15, 100);
		scrollType->callback((Fl_Callback*)scrollVariationType);
		scrollType->scrollvalue(0, 1, 0, 11);
		scrollType->tooltip("If activated, you define manually the priority of backchannels (Mimicry and Response Backchannels) for the action selection");
		scrollType1 = new Fl_Slider(580, 25, 15, 100);
		scrollType1->callback((Fl_Callback*)scrollVariationType1);
		scrollType1->scrollvalue(0, 1, 0, 11);
		scrollType1->tooltip("If activated, you define manually the priority of backchannels (Mimicry and Response Backchannels) for the action selection");

		buttonInterest = new Fl_Check_Button(550, 130, 20, 20, "Interest Level");
		buttonInterest->callback((Fl_Callback*)activateInterest);
		buttonInterest->tooltip("If activated, you manually define the user's interest level");
		buttonNumber = new Fl_Check_Button(550, 130, 20, 20, "Number of BCs");
		buttonNumber->callback((Fl_Callback*)activateNumber);
		buttonNumber->tooltip("If activated, you define manually the number of backchannels (Mimicry and Response Backchannels)");
		buttonType = new Fl_Check_Button(550, 130, 20, 20, "Priority of BCs");
		buttonType->callback((Fl_Callback*)activateType);
		buttonType->tooltip("If activated, you define manually the priority of backchannels (Mimicry and Response Backchannels) for the action selection");
		buttonAuto = new Fl_Check_Button(550, 130, 20, 20, "AS + Personality");
		buttonAuto->callback((Fl_Callback*)activateAuto);
		buttonAuto->tooltip("If activated, the action selection takes into account the agent personnality (experimental)");
		buttonNUI = new Fl_Check_Button(550, 130, 20, 20, "AS + Interest");
		buttonNUI->callback((Fl_Callback*)activateNUI);
		buttonNUI->tooltip("If activated, the action selection takes into account the user's interest level (experimental)");
		buttonEmotion = new Fl_Check_Button(550, 130, 20, 20, "AS + Emotions");
		buttonEmotion->callback((Fl_Callback*)activateEmotion);
		buttonEmotion->tooltip("If activated, the action selection takes into account the user's emotions (experimental)");
		buttonNoM = new Fl_Check_Button(550, 130, 20, 20, "No Mimicry");
		buttonNoM->callback((Fl_Callback*)activateNoM);
		buttonNoM->tooltip("If activated, no Mimicry are played by the ECA");
		buttonNoBC = new Fl_Check_Button(550, 130, 20, 20, "No Backchannels");
		buttonNoBC->callback((Fl_Callback*)activateNoBC);
		buttonNoBC->tooltip("If activated, no Response Backchannels are played by the ECA");
		buttonNoC = new Fl_Check_Button(550, 130, 20, 20, "No Utterances");
		buttonNoC->callback((Fl_Callback*)activateNoC);
		buttonNoC->tooltip("If activated, no Utterances are played by the ECA");
		buttonRandom = new Fl_Check_Button(550, 130, 20, 20, "Random BCs");
		buttonRandom->callback((Fl_Callback*)activateRandom);
		buttonRandom->tooltip("If activated, backchannels (Mimicry and Response Backchannels) are chosen randomly");

		/*for(int i=0; i<2; i++)
		{
			scrollInterest[i] = new Fl_Slider(580, 250+25, 15, 100);
			scrollInterest[i]->scrollvalue(0, 1, 0, 11);
		}*/



		//scrollInterest->bounds(-1.0, 1.0);
		/*for(int i=0; i<4; i++) 
		{
			buttonLeg[i] = new Fl_Button(25,5, 150, 20, legende[i].c_str());
			buttonLeg[i]->callback(choiceGraph);
		}*/
}



NInterface::~NInterface()
{
	delete this;
}


void NInterface::drawLine(float x, float y, float x2, float y2) 
{
	 fl_line((int)x,(int)y,(int)x2,(int)y2);
}


void NInterface::draw() 
{
		if(manNIU != oldManualNIU)
		{
			 valueManualNIU = manNIU;
			 //std::cout << valueManualNIU << "\n";
		 }
		oldManualNIU = manNIU;

		if(actManualNIU != oldActiveManualNIU)
		{
			 activeManualNIU = actManualNIU;
			 //std::cout << activeManualNIU << "\n";
		 }
		oldActiveManualNIU = actManualNIU;
		 
		if(manNumber != oldManualNumber)
		{
			 valueManualNumber = manNumber;
			 //std::cout << valueManualNumber << "\n";
		 }
		oldManualNumber = manNumber;

		if(manNumber1 != oldManualNumber1)
		{
			 valueManualNumber1 = manNumber1;
			 //std::cout << valueManualNumber1 << "\n";
		 }
		oldManualNumber1 = manNumber1;

		if(actManualNumber != oldActiveManualNumber)
		 {
			 activeManualNumber = actManualNumber;
			 //std::cout << activeManualNumber << "\n";
		 }
		 oldActiveManualNumber = actManualNumber;

		if(manType != oldManualType)
		{
			 valueManualType = manType;
			 //std::cout << valueManualType << "\n";
		 }
		oldManualType = manType;

		if(manType1 != oldManualType1)
		{
			 valueManualType1 = manType1;
			 //std::cout << valueManualType1 << "\n";
		 }
		oldManualType1 = manType1;

		if(actManualType != oldActiveManualType)
		{
			 activeManualType = actManualType;
			 //std::cout << activeManualType << "\n";
		 }
		oldActiveManualType = actManualType;

		if(actManualAuto != oldActiveManualAuto)
		 {
			 activeManualAuto = actManualAuto;
			 //std::cout << activeManualAuto << "\n";
		 }
		 oldActiveManualAuto = actManualAuto;

		if(actManualNUI != oldActiveManualNUI)
		 {
			 activeManualNUI = actManualNUI;
			 //std::cout << activeManualNUI << "\n";
		 }
		 oldActiveManualNUI = actManualNUI;

		if(actManualEmotion != oldActiveManualEmotion)
		 {
			 activeManualEmotion = actManualEmotion;
			 //std::cout << activeManualEmotion << "\n";
		 }
		 oldActiveManualEmotion = actManualEmotion;

		 if(actManualNoM != oldActiveManualNoM)
		 {
			 activeManualNoM = actManualNoM;
			 //std::cout << activeManualNoM << "\n";
		 }
		 oldActiveManualNoM = actManualNoM;

		 if(actManualNoBC != oldActiveManualNoBC)
		 {
			 activeManualNoBC = actManualNoBC;
			 //std::cout << activeManualNoBC << "\n";
		 }
		 oldActiveManualNoBC = actManualNoBC;

		 if(actManualNoC != oldActiveManualNoC)
		 {
			 activeManualNoC = actManualNoC;
			 //std::cout << activeManualNoC << "\n";
		 }
		 oldActiveManualNoC = actManualNoC;

		 if(actManualRandom != oldActiveManualRandom)
		 {
			 activeManualRandom = actManualRandom;
			 //std::cout << activeManualRandom << "\n";
		 }
		 oldActiveManualRandom = actManualRandom;

		 Fl_Double_Window::draw();
		 fenX = 550;
		 fenY = this->h()-30;

		 int affgraphY = 0;
		 int nbgraphX = 4;
		 int margeXG = (fenX/20); //25
		 margeYsup = (fenY/100)*7; //10

		 scrollInterest->resize(fenX + (fenX/100)*8, margeYsup, 15, (fenY/7));
		 scrollNumber->resize(fenX + (fenX/100)*5, margeYsup+(fenY/4)*2, 15, (fenY/8));
		 scrollNumber1->resize(fenX + (fenX/100)*15, margeYsup+(fenY/4)*2, 15, (fenY/8));
		 scrollType->resize(fenX + (fenX/100)*5, margeYsup+(fenY/4), 15, (fenY/8));
		 scrollType1->resize(fenX + (fenX/100)*15, margeYsup+(fenY/4), 15, (fenY/8));
		 buttonInterest->resize(fenX, margeYsup+(fenY/6), 15, (fenY/100)*2);
		 buttonNumber->resize(fenX, margeYsup+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonType->resize(fenX, margeYsup+(fenY/6)+(fenY/4), 15, (fenY/100)*2);
		 buttonAuto->resize(fenX, margeYsup*2+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonNUI->resize(fenX, margeYsup*5/2+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonEmotion->resize(fenX, margeYsup*3+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonNoM->resize(fenX, margeYsup*7/2+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonNoBC->resize(fenX, margeYsup*4+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonRandom->resize(fenX, margeYsup*9/2+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);
		 buttonNoC->resize(fenX, margeYsup*5+(fenY/6)+(fenY/4)*2, 15, (fenY/100)*2);

		 fl_color(FL_BLUE);
		 fl_draw("Mean over 20 secs",margeXG+275, affgraphY+margeYsup);
		 fl_color(FL_RED);
		 fl_draw("Mean over 5 secs",margeXG+125, affgraphY+margeYsup);

		 fl_color(FL_BLACK);
		 for(int i = 0; i<nbgraphX; i++)
		 {
			//buttonLeg[i]->position(margeXG+10,affgraphY+2);
			//buttonLeg[i]->redraw();
			fl_draw(legende[i].c_str(), margeXG+10, affgraphY+margeYsup);

			 if(affLeg[i] == 0)
			 {
				 fl_line(margeXG,affgraphY+margeYsup,margeXG,affgraphY+(fenY/20)*4+margeYsup); 
				 fl_line(margeXG,affgraphY+(fenY/20)*4+margeYsup,fenX-margeXG,affgraphY+(fenY/20)*4+margeYsup);
				 
				 fl_line(margeXG-3,affgraphY+margeYsup,margeXG,affgraphY+margeYsup); 
				 fl_line(margeXG-3,affgraphY+(fenY/20)+margeYsup,margeXG,affgraphY+(fenY/20)+margeYsup);
				 fl_line(margeXG-3,affgraphY+(fenY/20)*2+margeYsup,margeXG,affgraphY+(fenY/20)*2+margeYsup); 
				 fl_line(margeXG-3,affgraphY+(fenY/20)*3+margeYsup,margeXG,affgraphY+(fenY/20)*3+margeYsup);
				 fl_line(margeXG-3,affgraphY+(fenY/20)*4+margeYsup,margeXG,affgraphY+(fenY/20)*4+margeYsup); 
				
				 fl_draw("1", margeXG/6, affgraphY+margeYsup+margeYsup/3);
				 fl_draw("0", margeXG/6, affgraphY+(fenY/20)*2+margeYsup+margeYsup/3); //115
				 fl_draw("-1", margeXG/6, affgraphY+(fenY/20)*4+margeYsup+margeYsup/3); //215
				 fl_draw("-20s", margeXG, affgraphY+(fenY/20)*4+margeYsup+(margeYsup/3+2));
				 fl_draw("0s", fenX-margeXG-margeXG/2, affgraphY+(fenY/20)*4+margeYsup+(margeYsup/3)+2);
			}			 
				 affgraphY = affgraphY + (fenY/4);

		 }

		/* for(int i = 0; i<nbgraphY; i++)
		 {

			 fl_line(affgraphY+25,10,affgraphY+25,210); 
			 fl_line(affgraphY+22,10,affgraphY+25,10); fl_draw("1", 6, affgraphY+15);
			 fl_line(affgraphY+22,60,affgraphY+25,60);
			 fl_line(affgraphY+22,110,affgraphY+25,110); fl_draw("0", 1, affgraphY+115);
			 fl_line(affgraphY+22,160,affgraphY+25,160);
			 fl_line(affgraphY+22,210,affgraphY+25,210); fl_draw("-1", 6, affgraphY+215);
			 fl_line(affgraphY+25,210,affgraphY+525,210);
			 affgraphY = affgraphY + 250;
		 }*/

		 fl_draw(scrollValue.c_str(),fenX + (fenX/100)*8, margeYsup-2);
		 fl_draw(scrollValueNumber.c_str(),fenX + (fenX/100)*3, margeYsup-2+(fenY/4)*2);
		 fl_draw(scrollValueNumber1.c_str(),fenX + (fenX/100)*13, margeYsup-2+(fenY/4)*2);
		 fl_draw("M",fenX + (fenX/100)*5, margeYsup+(fenY/6.5)+(fenY/4)*2);
		 fl_draw("RBC",fenX + (fenX/100)*14, margeYsup+(fenY/6.5)+(fenY/4)*2);
		 fl_draw("M",fenX + (fenX/100)*5, margeYsup+(fenY/6.5)+(fenY/4));
		 fl_draw("RBC",fenX + (fenX/100)*14, margeYsup+(fenY/6.5)+(fenY/4));
		 fl_draw(scrollValueType.c_str(),fenX + (fenX/100)*3, margeYsup-2+(fenY/4));
		 fl_draw(scrollValueType1.c_str(),fenX + (fenX/100)*13, margeYsup-2+(fenY/4));

		 //fl_draw(anticipation,285, 235);
		 //fl_draw(actionchosen,175, 265);

		
		if(affLeg[0] == 0)
		{
		 for(int i=0; i<500; i++) { //(fenX-margeX)/500*i
			 drawLine(i+25,(fenY/20)*4-(listeNI[i]*(fenY/5))+margeYsup,i+26,(fenY/20)*4+margeYsup-(listeNI[i+1]*(fenY/5)));
			 listeNI[i]=listeNI[i+1];
		 }

		 fl_color(FL_BLUE);
		 for(int i=0; i<500; i++) {
			 drawLine(i+25,(fenY/20)*4+margeYsup-(listeMoy[i]*(fenY/5)),i+26,(fenY/20)*4+margeYsup-(listeMoy[i+1]*(fenY/5)));
			 listeMoy[i]=listeMoy[i+1];
		 }
		
		 fl_color(FL_RED);
		 for(int i=0; i<500; i++) { //(fenX-margeX)/500*i
			 drawLine(i+25,(fenY/20)*4+margeYsup-(listeNIHyst[i]*(fenY/5)),i+26,(fenY/20)*4+margeYsup-(listeNIHyst[i+1]*(fenY/5)));
			 listeNIHyst[i]=listeNIHyst[i+1];
		 }
		}

		if(affLeg[1] == 0)
		{

		 fl_color(FL_BLACK);
		 for(int i=0; i<500; i++) {
			 drawLine(i+25,(fenY/20)*4+margeYsup-(listeAr[i]*(fenY/5))+(fenY/4),i+26,(fenY/20)*4+margeYsup-(listeAr[i+1]*(fenY/5))+(fenY/4));
			 listeAr[i]=listeAr[i+1];
		 }
		}

		if(affLeg[2] == 0)
		{

		 for(int i=0; i<500; i++) {
			 drawLine(i+25,(fenY/20)*4+margeYsup-(listeVal[i]*(fenY/5))+(fenY/2),i+26,(fenY/20)*4+margeYsup-(listeVal[i+1]*(fenY/5))+(fenY/2));
			 listeVal[i]=listeVal[i+1];
		 }
		}

		if(affLeg[3] == 0)
		{

		 for(int i=0; i<500; i++) {
			 drawLine(i+25,(fenY/20)*4+margeYsup-(listePot[i]*(fenY/5))+(fenY/4)*3,i+26,(fenY/20)*4+margeYsup-(listePot[i+1]*(fenY/5))+(fenY/4)*3);
			 listePot[i]=listePot[i+1];
		 }
		}
		this->redraw();
}

void NInterface::drawNI(float NIU, float MoyNI, const char* interPhase, const char* anticip, float priority, std::string typeChosen, const char* actchosen, float arousal, float valence, float potency)
{
	/*
	tracage du NIU dans une fenetre fltk
	*/
	if (MoyNI > 1)
		MoyNI = 1;
	if (MoyNI < 0)
		MoyNI = 0;
	if (NIU > 1 || NIU < 0) //enleve les mauvaises valeurs du NI
		NIU = agentstate_buffer;

	listeNI[500] = NIU;
	
	NUIM=0.0;
	NUIMoy=0.0;

	for(int i= 500; i > 375; i--)
		NUIM += listeNI[i]/125;
	listeNIHyst[500] = NUIM;

	for(int i= 500; i > 0; i--)
		NUIMoy += listeNI[i]/500;
	listeMoy[500] = NUIMoy;

	listeAr[500] = arousal;
	listeVal[500] = valence;
	listePot[500] = potency;
	//phase = interPhase;
	anticipation = anticip;
	actionchosen = actchosen;
	actionpriority = priority;

	if(typeChosen != "")
	{
		std::string *st = new std::string("Chosen Action : "+typeChosen);
		actchosen = st->c_str(); //affiche l'action choisie dans l'interface
	}
	/*if(skipped == 1)
	{
		std::string *st = new std::string("Action skipped");
		actchosen = st->c_str(); //affiche l'action choisie dans l'interface
	}*/

	agentstate_buffer = NIU;

}