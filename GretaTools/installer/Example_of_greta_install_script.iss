[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{A5D38287-062A-4CA3-9C68-406860D486BB}

; name of the application :
AppName=Greta
; name and version of the application :
AppVerName=Greta
; publisher of Greta :
AppPublisher=Catherine Pelachaud
; name of the directory in "program files" :
DefaultDirName={pf}\Greta ECA
; name in start menu :
DefaultGroupName=Greta ECA
; file witch contains the license text :
LicenseFile=".\License.txt"
; directory where the installer is build :
OutputDir=".\"
; name of the .exe :
OutputBaseFilename=greta_setup
; compression
Compression=lzma
SolidCompression=yes
; images during setup :
WizardImageFile=".\greta.bmp"
WizardSmallImageFile=".\Greta_Head.bmp"

[Languages]
; languages supported by the install program :
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "basque"; MessagesFile: "compiler:Languages\Basque.isl"
Name: "brazilianportuguese"; MessagesFile: "compiler:Languages\BrazilianPortuguese.isl"
Name: "catalan"; MessagesFile: "compiler:Languages\Catalan.isl"
Name: "czech"; MessagesFile: "compiler:Languages\Czech.isl"
Name: "danish"; MessagesFile: "compiler:Languages\Danish.isl"
Name: "dutch"; MessagesFile: "compiler:Languages\Dutch.isl"
Name: "finnish"; MessagesFile: "compiler:Languages\Finnish.isl"
Name: "french"; MessagesFile: "compiler:Languages\French.isl"
Name: "german"; MessagesFile: "compiler:Languages\German.isl"
Name: "hebrew"; MessagesFile: "compiler:Languages\Hebrew.isl"
Name: "hungarian"; MessagesFile: "compiler:Languages\Hungarian.isl"
Name: "italian"; MessagesFile: "compiler:Languages\Italian.isl"
Name: "norwegian"; MessagesFile: "compiler:Languages\Norwegian.isl"
Name: "polish"; MessagesFile: "compiler:Languages\Polish.isl"
Name: "portuguese"; MessagesFile: "compiler:Languages\Portuguese.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "slovak"; MessagesFile: "compiler:Languages\Slovak.isl"
Name: "slovenian"; MessagesFile: "compiler:Languages\Slovenian.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
; gets all contents of the bin folder :
Source: "..\bin\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
; creates a shortcut named "Greta" to Greta.exe in "start" menu :
Name: "{group}\Greta"; Filename: "{app}\Greta.exe"
; creates a shortcut to uninstall Greta :
Name: "{group}\{cm:UninstallProgram,Greta}"; Filename: "{uninstallexe}"
; creates a shortcut named "Greta" to Greta.exe on desktop :
Name: "{commondesktop}\Greta"; Filename: "{app}\Greta.exe"; Tasks: desktopicon
; creates a shortcut named "Greta" to Greta.exe on the quick launch menu :
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Greta"; Filename: "{app}\Greta.exe"; Tasks: quicklaunchicon

[Run]
; to start Greta.exe at the end of setup :
Filename: "{app}\Greta.exe"; Description: "{cm:LaunchProgram,Greta}"; Flags: nowait postinstall skipifsilent unchecked

