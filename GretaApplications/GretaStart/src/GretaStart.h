//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// GretaStart.h: interface for the GretaStart class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Round_Button.H>
#include <FL/Fl_Input.H>
#include <fl/Fl_Box.h>

class GretaStart : public Fl_Window  
{
public:

	GretaStart();
	virtual ~GretaStart();
	int handle(int e);
	void draw();

	Fl_Box *modules;
	Fl_Check_Button *listenerIntentPlanner;
	Fl_Check_Button *actionSelection;
	Fl_Check_Button *behaviourPlanner;
	Fl_Check_Button *behaviourRealizer;
	Fl_Check_Button *player;
	Fl_Check_Button *inter;
	Fl_Round_Button *debug;
	Fl_Round_Button *release;

	Fl_Box *communicationSystem;
	Fl_Round_Button *active;
	Fl_Round_Button *psyclone;

	Fl_Button *startGreta;
	Fl_Button *stopGreta;

	char *path;

	void StartSystem();
};
