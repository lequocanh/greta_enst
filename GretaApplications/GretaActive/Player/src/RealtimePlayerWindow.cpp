//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ClockModuleWindow.cpp: implementation of the ClockModuleWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "RealtimePlayerWindow.h"
//#include "PlayerReceiver.h"
#include "Timer.h"
#include "RealTimeAudio.h"
#include <windows.h>



using namespace semaine::cms::exceptions;

extern IniManager inimanager;
//extern semaine::components::greta::PlayerReceiver *playerreceiver;

//extern	"C" int play_wave(int,char*);


void realtimeplayeridle(void* v)
{
	RealtimePlayerWindow* player=((RealtimePlayerWindow*)v);

	Sleep(10);
	if(player->am->getFAPFrame(player->ff))
		player->glwindow->getAgent()->LoadFAPFrame(&player->ff);
	if(player->am->getBAPFrame(player->bf))
		player->glwindow->getAgent()->LoadBAPFrame(-1,&player->bf);
	player->am->playAudio();

	player->redraw();

}

int realtimeplayerreceiver(void *caller)
{
	RealtimePlayerWindow* player=((RealtimePlayerWindow*)caller);
	
	return 1;
}


RealtimePlayerWindow::RealtimePlayerWindow(CentralClock *pc):Fl_Double_Window(100,100,640,480,"Greta Realtime Player")
{
	this->size_range(320,240,0,0);
	this->pc = pc;
	RealTimeAudio::init();
	std::string host,GretaName;
	int port;
	x_old = x();
	y_old = y();
	w_old = w();
	h_old = h();
	full = false;

	/*output=new Fl_Multiline_Output(10,450,620,20);
	output->labelfont(FL_HELVETICA);
	output->labelsize(10);
	output->textfont(FL_HELVETICA);
	output->textsize(10);*/

	timeoutput=new Fl_Output(40,10,60,15,"time:");
	timeoutput->labelfont(FL_HELVETICA);
	timeoutput->labelsize(10);
	timeoutput->textfont(FL_HELVETICA);
	timeoutput->textsize(10);

	fapbufferoutput=new Fl_Output(180,10,60,15,"FAP buffer:");
	fapbufferoutput->labelfont(FL_HELVETICA);
	fapbufferoutput->labelsize(10);
	fapbufferoutput->textfont(FL_HELVETICA);
	fapbufferoutput->textsize(10);

	bapbufferoutput=new Fl_Output(320,10,60,15,"BAP buffer:");
	bapbufferoutput->labelfont(FL_HELVETICA);
	bapbufferoutput->labelsize(10);
	bapbufferoutput->textfont(FL_HELVETICA);
	bapbufferoutput->textsize(10);

	Fl::add_idle(realtimeplayeridle,this);
#ifdef PLAYEROGRE
	glwindow=new PlayerOgreView(10,35,620,410,"",true,true);
#else
	glwindow=new PlayerFLTKGLWindow(10,35,620,410,"",false,true);
#endif

	am = new AnimationManager(pc);
	am->start();
	


/*#ifdef PLAYEROGRE
	receiver = new semaine::components::greta::PlayerReceiver("PlayerOgre", false, false,this,fm,cm,&(pc));
#else
	receiver = new  semaine::components::greta::PlayerReceiver("Player", false, false,this,fm,cm,&(pc));
#endif*/

}

RealtimePlayerWindow::~RealtimePlayerWindow()
{
	//delete module;
}

void RealtimePlayerWindow::draw()
{
	
	char buffer[100];
	sprintf(buffer,"%d",pc->GetTime());
	timeoutput->value(buffer);
	sprintf(buffer,"%d",am->getFAPNumber());
	fapbufferoutput->value(buffer);
	sprintf(buffer,"%d",am->getBAPNumber());
	bapbufferoutput->value(buffer);
#ifndef PLAYEROGRE
	glwindow->redraw();
#endif
	Fl_Double_Window::draw();

}

void RealtimePlayerWindow::resize(int x, int y, int w, int h)
{
	Fl_Double_Window::resize(x, y, w, h);
	glwindow->resize(10,35,this->w()-20, this->h()-40);
	//this->redraw();

	if(full == false)
	{
		x_old = x;
		y_old = y;
		w_old = w;
		h_old = h;
	}
}


int RealtimePlayerWindow::handle(int e)
{
	if(Fl::event_button()==FL_LEFT_MOUSE)
	{
		if(e==FL_RELEASE)
		{
			full = true;
			x_old = x();
			y_old = y();
			w_old = w();
			h_old = h();

			this->fullscreen();
			//glwindow->width=400;
			glwindow->resize(10,35,this->w()-20, this->h()-40);
			//output->replace(10,this->h()-60,"");
			glwindow->redraw();
		}
	}
	if(Fl::event_button()==FL_RIGHT_MOUSE)
	{
		if(e==FL_RELEASE)
		{
			full = false;
			this->fullscreen_off(x_old, y_old, w_old, h_old);
			this->size_range(320,240,0,0);
			//output->replace(10,450,"");
			glwindow->resize(10,35,this->w()-20,this->h()-40);
			glwindow->redraw();
		}
	}


	return Fl_Window::handle(e);
}
