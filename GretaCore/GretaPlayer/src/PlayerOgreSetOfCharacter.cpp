#include "PlayerOgreSetOfCharacter.h"
#include "IniManager.h"

extern IniManager inimanager;

PlayerOgreSetOfCharacter::PlayerOgreSetOfCharacter(Ogre::SceneManager* ogreSceneManager_, Ogre::SceneNode* worldRootNode_, bool realtime_){
	ogreSceneManager = ogreSceneManager_;
	worldRootNode = worldRootNode_;
	realtime = realtime_;
	addAgent("GRETA");
	addAgent("GRETA","LISTENER");
}

void PlayerOgreSetOfCharacter::addAgent(std::string name, std::string type){
	std::string characterName = name+type;
	inimanager.ToUppercase(characterName);
	if(characterList.count(characterName)==0){
		bool b = characterDefinitionExist(name);
		if(characterDefinitionExist(name)){
			characterList[characterName] = new PlayerOgreAgent(ogreSceneManager,worldRootNode,characterName,name,realtime);
			characterList[characterName]->setVisible(false);
		}
		else{
			inimanager.ToUppercase(type);
			characterList[characterName] = characterList["GRETA"+type];
		}
	}
}

PlayerOgreAgent* PlayerOgreSetOfCharacter::getAgent(std::string name, std::string type){
	std::string characterName = name+type;
	inimanager.ToUppercase(characterName);
	if(characterList.count(characterName)==0){
		if(characterDefinitionExist(name))
			characterList[characterName] = new PlayerOgreAgent(ogreSceneManager,worldRootNode,name+type,name,realtime);
		else
			return NULL;
	}
	return characterList[characterName];
}

bool PlayerOgreSetOfCharacter::characterDefinitionExist(std::string name){
	std::string fileName = inimanager.Program_Path+"Ogre/"+name+".xml";
	return fopen(fileName.c_str(),"r")!=0;
}