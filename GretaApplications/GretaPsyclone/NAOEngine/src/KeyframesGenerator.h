#pragma once

#include "GestureEngine.h"


using namespace GestureSpace;

class KeyframesGenerator
{
public:
	KeyframesGenerator();
	~KeyframesGenerator();

	GestureSpace::Gesture *gesture; // gesture has two phases: phases and phasesAssym for right and left side correspondingly when the gesture is assymetric
	GestureSpace::GesturePhaseVector *phasesVector; // = &gesture->phases
	
	float *lHand;
	float *rHand;
	vector<float> lVec6;
	vector<float> rVec6;

	vector<vector<float>> leftPath;
	vector<vector<float>> rightPath;
	vector<float> rightTimes;
	vector<float> leftTimes;

public:
	void Interpret(GestureSpace::GestureVector* gestureVector);
	void Interpret(GestureSpace::Gesture *gesture);
	vector<float> Converter(GesturePhase *phase, vector<float> vecxyz, float *hand_open, float *hand_dir, float SPC, float TMP);
};