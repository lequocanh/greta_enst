//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// XMLGenericTree.h: interface for the XMLGenericTree class.
//
//////////////////////////////////////////////////////////////////////

//made by Maurizio Mancini m.mancini@iut.univ-paris8.fr manmau@yahoo.com

//this file describes a generic tree object
//which is the result of the parsing of an xml file

#if !defined(AFX_XMLGenericTree_H__5B7B5963_2060_11D9_9C92_EE4D16C4357B__INCLUDED_)
#define AFX_XMLGenericTree_H__5B7B5963_2060_11D9_9C92_EE4D16C4357B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <string>
#include <list>
#include <iostream>
#include <fstream>

#include "PolyIterator.h"

/*! \brief generic xml attribute
*
*
struct XMLAttribute{
	std::string name;
	std::string value;
};
*/

/*! \brief xml generic tree
*
* this class describes a generic tree object
* which is the result of the parsing of an xml file
* @see APMLTree
* @author Maurizio Mancini m.mancini@iut.univ-paris8.fr manmau@yahoo.com
*/

class XMLGenericTree {
public:
	// a polymorphic iterator implemenation
	typedef PolyIterator<XMLGenericTree *> iterator;

	virtual bool isTextNode() = 0;

	/*! \brief prints the tree under this node, complete with all attributes
	*
	* @param s is a string that is prefixed to each printed line
	* and for each recursive iteration of the function the string
	* becomes longer so the final output has a tree appearence;
	* s should be initialized to empty string
	*/
	virtual void Print() = 0;
	virtual void Save(std::string filename) = 0;
	
	/*! \brief adds a child to this node
	*
	* @param e is the tree to be added
	*/
	//virtual void addChild(XMLGenericTree *e) = 0;

	/*! \brief returns the node called with some name
	*
	* @param n the name of the wanted node
	* @return the pointer to the wanted node or 0 if not found
	*/
	virtual XMLGenericTree* FindNodeCalled(std::string n) = 0;
	/*! \brief returns the node called with some name
	*
	* @param name the name of the wanted node
	* @param nameSpace the namespace of the wanted node
	* @return the pointer to the wanted node or 0 if not found
	*/
	virtual XMLGenericTree* FindNodeCalled(std::string name, std::string nameSpace) = 0;
	

	/*! \brief returns number of child node of the node
	*
	*/
	int GetNumberOfChildren() {
		int i = 0;
		for(iterator it = begin(); it != end(); ++it, ++i) ;
		return i;
	}
	/*! \brief return the appropriate iterators
	*/
	virtual iterator begin() = 0;
	virtual iterator end() = 0;

	/**
	* Returns a pointer to the parent of this.
	* if this has not parent, it returns NULL
	* @return the parent node
	*/
	virtual XMLGenericTree* GetParent() = 0;

	/**
	* returns the name of the node.
	* if this is a text node, it return an empty string.
	* @return the name of the node
	*/
	virtual std::string GetName() = 0;

	/**
	* returns the text content only if this is a txt node
	* else, returns NULL.
	* @return text content
	*/
	virtual std::string GetTextValue() = 0;

	/**
	* returns the float value of a specified attribute.
	* @param n name of the attribute
	* @return the float value
	*/
	virtual float GetAttributef(std::string n) = 0;

	/**
	* returns the value of a specified attribute.
	* @param n name of the attribute
	* @return the string value
	*/
	virtual std::string GetAttribute(std::string n) = 0;

	/**
	* 
	* @param n name of the attribute
	* @return if the attribute exist
	*/
	virtual bool HasAttribute(std::string n) = 0;

	/**
	* Returns the tree in XML text format
	* @return the tree in XML text format
	*/
	virtual std::string ToString() = 0;





	/**
	* throws an exception if this is not a text node
	*
	* @param text the text to set
	* @throw excep::GRETAException
	*/
	virtual void SetTextValue(std::string text) = 0;

	/**
	* Changes the value of an attribute.
	* if the attribute does not existe, the function creates and adds it.
	* @param n name of the attribute
	* @param value the new value to set
	*/
	virtual void SetAttribute(std::string n, std::string value) = 0;

	/**
	* Copies and adds an existing tree as a child
	* @param child the tree to copy and add
	* @return a pointer to the child
	*/
	virtual XMLGenericTree* AddChild(XMLGenericTree* child) = 0;

	/**
	* Creates a text node as child
	* @param text the text content
	*/
	virtual void AddText(std::string text) = 0;
	/**
	* Creates and returns a childwith the specified name
	* @param name the name aff the new child
	* @return the new child
	*/
	virtual XMLGenericTree* CreateChild(std::string name) = 0;
	/**
	* Creates and returns a childwith the specified name an namespace
	* @param name the name off the new child
	* @param nameSpace the namespace off the child
	* @return the new child
	*/
	virtual XMLGenericTree* CreateChild(std::string name, std::string nameSpace) = 0;
};



#endif // !defined(AFX_XMLGenericTree_H__5B7B5963_2060_11D9_9C92_EE4D16C4357B__INCLUDED_)
