/*
 *  MessageFormatException.h
 *  semaine
 *
 *  Created by Marc Schröder on 09.09.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#ifndef MESSAGEFORMATEXCEPTION_H
#define MESSAGEFORMATEXCEPTION_H

#include "GRETAException.h"

namespace excep {

class MessageFormatException : public GRETAException
{
public:
		inline MessageFormatException(const std::string & text) throw() :
		GRETAException(text)
		{
		}
		
		inline ~MessageFormatException() throw() {}
};

} //excep namespace
#endif

