/**
 * Copyright Aldebaran Robotics
 */
#include "alhelloworld.h"

#include <iostream>
#include <alproxy.h>
#include <alptr.h>
#include <albroker.h>
#include <almodule.h>
#include <alloggerproxy.h>

#include <fstream>

using namespace AL;

//______________________________________________
// constructor
//______________________________________________
ALHelloWorld::ALHelloWorld(ALPtr<ALBroker> pBroker, const std::string& pName ): ALModule(pBroker, pName )
{
 // Describe the module here
  setModuleDescription( "An hello world app." );

  // Define callable methods with there description
  functionName( "helloWorld", getName(),  "allows the robot to play a wav file" );
  BIND_METHOD( ALHelloWorld::helloWorld );
  
  ad=getParentBroker()->getProxy("ALAudioDevice");
  
}

//______________________________________________
// destructor
//______________________________________________
ALHelloWorld::~ALHelloWorld()
{

}

/**
 * helloworld Function
 */
void ALHelloWorld::helloWorld(const std::string & pFileName)
{
  const int outputBufferSize=16384;   // maximum buffer size that ALAudioDevice can send
  const int numberOfOutputChannels=2; // number of output channels on Nao
  
  // --------- opening of the input file --------------------------------------------------------
  FILE *fInputWavFile;
  if ( ( fInputWavFile = fopen ( pFileName.c_str(), "rb" ) ) == NULL )
  {
      throw AL::ALError ( "ALHelloWorld", "helloWorld", "Failed to open input sound file." );
  }
  // --------- reading of the wav header ---------------------------------------------------------
  wavheader *wavHeader=new wavheader;
  fread(wavHeader,1,44,fInputWavFile);
  unsigned short nbOfChannels = wavHeader->NumChannels;
  unsigned long sampleRate = wavHeader->SampleRate;
  unsigned short bitsPerSample = wavHeader->BitsPerSample;
   
  // ------- set output sample rate of audiodevice -------------------------------------------------------------------
  ad->callVoid("setParameter",std::string("outputSampleRate"),(int) sampleRate); 
  
  // ------- go to the beginning of the audio data -------------------------------------------------------------------
  fseek (fInputWavFile , 44 , SEEK_SET );
  
  // --------------- reading of the audio data
  short *fInputAudioData=new short[outputBufferSize*nbOfChannels]; // buffer to store the data contained in the wav file
  short *fStereoAudioData=new short[outputBufferSize*numberOfOutputChannels]; // buffer to construct stereo audio data 
  ALValue pDataBin; // ALValue where to store the stereo audio data to send

  while ( !feof ( fInputWavFile ) )
  {
    //Read samples from file
    int fNbOfInputSamples = fread ( fInputAudioData,bitsPerSample/8*nbOfChannels,outputBufferSize,fInputWavFile );

    if (nbOfChannels==1)
    {  
      // construction of stereo audio data in case the input file is mono   
      int i=0;
      for (int j=0;j<fNbOfInputSamples;j++)
      {
        fStereoAudioData[i]=fInputAudioData[j]; 
        fStereoAudioData[i+1]=fInputAudioData[j];
        i+=numberOfOutputChannels;
      }
      pDataBin.SetBinary( fStereoAudioData, fNbOfInputSamples*sizeof(short)*numberOfOutputChannels ); // transform audio data to binary samples
    }
    else
    {
      pDataBin.SetBinary( fInputAudioData, fNbOfInputSamples*sizeof(short)*numberOfOutputChannels ); // transform audio data to binary samples  
    }

    // ------------------------ send the stereo audio data to audiodevice module ----------------------------------------
    ad->call <bool> ("sendRemoteBufferToOutput",fNbOfInputSamples,pDataBin); 

  }
  
  fclose(fInputWavFile);
  ad->callVoid("stopAudioOut"); // to clean audio output buffer
}


