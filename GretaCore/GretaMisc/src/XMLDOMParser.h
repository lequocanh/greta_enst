//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// XMLDOMParser.h: interface for the XMLDOMParser class.
//
//////////////////////////////////////////////////////////////////////

//made by Elisabetta Bevacqua

#if !defined(AFX_XMLDOMPARSER_H__5461A1E1_215D_11D9_9C92_E689F9D58C74__INCLUDED_)
#define AFX_XMLDOMPARSER_H__5461A1E1_215D_11D9_9C92_E689F9D58C74__INCLUDED_

#include "XercesTool.h"
#include "XMLDOMTree.h"

class XMLDOMParser  
{
public:
	/*! \brief class contructor
	*
	* creates and initializes the parser
	*/
	XMLDOMParser();
	/*! \brief class destructor
	* 
	*/
	virtual ~XMLDOMParser();

	class XMLDOMTree * ParseBuffer(const char *buffer);

	class XMLDOMTree * ParseBuffer(std::string buffer);

	class XMLDOMTree *ParseFile(std::string name);

	class XMLDOMTree *ParseFileWithXSD(std::string name,std::string xsdpath);

	class XMLDOMTree *ParseBufferWithXSD(const char *buffer,std::string xsdpath);

	void SetValidating(bool v);

protected:

private:
	std::string readFile(std::string path);
	bool validating;
};

#endif // !defined(AFX_XMLGENERICPARSER_H__5461A1E1_215D_11D9_9C92_E689F9D58C74__INCLUDED_)
