//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "cv.h"
#include "highgui.h"
#include "math.h"
#include "cvcam.h"

#include "GazeDetector.h"
#include "GazeDetectorPsydule.h"
#include "IniManager.h"

IniManager inimanager;
GazeDetectorPsydule* gdp;
SteliosGazeDetector* sgd;

void main()
{
	sgd = new SteliosGazeDetector();
	
	//connect to Psyclone
	std::string host;
	int port;

	inimanager.ReadIniFile("greta.ini");
	host=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	port=inimanager.GetValueInt("PSYCLONE_PORT");

	printf("\nIni file specifies Psyclone server %s: %d", host.c_str(), port);

	gdp = new GazeDetectorPsydule("Greta.GazeDetectorPsydule",host,port);

	long timeNow = clock();


	sgd->initialiseNewCapture();

	FrameResults frameResults;

	for (int i = 0; i < 1000000; i++)
	{
		frameResults = sgd->processNewFrame();
		if (frameResults.valid)
		{
			gdp->sendGazeHeadDirMessage(frameResults.ppx,frameResults.ppy,frameResults.dist_monitor);
			//printf("\npx=%3.3f\tpy=%3.3f\tppx=%3.3f\tppy=%3.3f\tdistance=%3.3f",frameResults.px,frameResults.py,frameResults.ppx,frameResults.ppy,frameResults.dist_monitor);
			printf("\nppx=%3.5f\tppy=%3.5f\tdistance=%3.5f",frameResults.ppx,frameResults.ppy,frameResults.dist_monitor);
		}
		else printf("\nInvalid frameresults");
	}
	getchar();

	sgd->finishCapture();




	/*
	//to a sample OpenCV capture
	int key = 0;
	CvCapture* capture = NULL;
	capture = cvCaptureFromCAM( -1 );

	if( !capture ) {
		fprintf( stderr, "ERROR: capture is NULL \n" );
		while(1);
		return;
	}

	//create the video output window
	cvNamedWindow( "output", CV_WINDOW_AUTOSIZE );
	IplImage* frame_or = cvQueryFrame( capture );

	while(1)
	{
		if( !cvGrabFrame( capture )) break;
		frame_or = cvRetrieveFrame( capture );
		if( !frame_or )	break;		

		cvShowImage( "output", frame_or );	

		//have to insert this, or else laptop camera capture will not work
		key=cvWaitKey(20); 

		//if its time for an update, send it
		if ((clock() - timeNow) > (CLOCKS_PER_SEC * 0.5))
		{
			timeNow = clock();
			//send random gaze coordinates
			gdp->sendGazeCoordsMessage(rand() %	1971 - 50, rand() %	1251 - 50);
		}
	}
	delete gdp;
	*/
}