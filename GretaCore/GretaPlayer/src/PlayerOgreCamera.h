#ifndef PLAYEROGRECAMERA_H
#define PLAYEROGRECAMERA_H
#include <ogre.h>


class PlayerOgreCamera{
public:
	PlayerOgreCamera(bool movable, Ogre::SceneManager *sceneManager, Ogre::SceneNode *parent, Ogre::String name);
	~PlayerOgreCamera();

	void setAspect(double width,double height){ mCamera->setAspectRatio(Ogre::Real(width) / Ogre::Real(height)); }
	void setPosition(double x,double y,double z){ mCamera->setPosition(x, y, z); }
	void lookAt(double x,double y,double z){ mCamera->lookAt(x, y, z); }
	void move(double x,double y,double z){ mCamera->moveRelative(Ogre::Vector3(x,y,z)); }
	void pitch(double angle){ pitchNode->pitch(Ogre::Radian(angle)); }
	void yaw(double angle){ yawNode->yaw(Ogre::Radian(angle)); }
	void roll(double angle){ mCamera->roll(Ogre::Radian(angle));}
	bool isMovable(){ return ismovable; }
	Ogre::SceneNode* getNode(){ return yawNode;}
	Ogre::Camera* getOgreCamera(){ return mCamera; }
	void resetPosNRot();
	double getPitchValue(){ return pitchNode->getOrientation().getPitch().valueRadians(); }
	double getYawValue(){ return yawNode->getOrientation().getYaw().valueRadians(); }
	double getRollValue(){ return mCamera->getOrientation().getRoll().valueRadians(); }
	double tan_FOV(){return tan(mCamera->getFOVy().valueRadians());}
	void setModeWireframe(){mCamera->setPolygonMode(Ogre::PM_WIREFRAME);}
	void setModeSolid(){mCamera->setPolygonMode(Ogre::PM_SOLID);}
	void setModePoints(){mCamera->setPolygonMode(Ogre::PM_POINTS);}
	void setBackgroundColour(double R, double G, double B){mCamera->getViewport()->setBackgroundColour(Ogre::ColourValue(R,G,B));}

private:
	Ogre::Camera	*mCamera;
	Ogre::SceneNode	*pitchNode;
	Ogre::SceneNode	*yawNode;
	bool			 ismovable;
};
#endif


