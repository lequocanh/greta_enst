//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "ZignTrackConverter.h"
#include "OptiTrackConverter.h"
#include "FAPwriter.h"

//#include "EnterfaceCaptureMotion.h"
//#include "IniManager.h"
//#include "DataContainer.h"

//DataContainer *datacontainer;
//IniManager inimanager;
//std::string ini_filename;


//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include "RealtimeBML.h"
#include "IniManager.h"
#include "DataContainer.h"
#include "XercesTool.h"



DataContainer *datacontainer;
IniManager inimanager;
std::string ini_filename;

int main(int argc, char ** argv)
{
	
	std::string GretaName;
	std::string host;
	int port;
	
	if(argc==2)
	{
		ini_filename=argv[1];
	} else {
		ini_filename="greta.ini";	
	}

	inimanager.ReadIniFile(ini_filename);
	datacontainer = new DataContainer();

	XercesTool::startupXMLTools();
	
	int code=datacontainer->initBMLEngine();
	if (code==0) {
		printf("Problem : out \n");
		exit(1);
	}

	host= inimanager.GetValueString("PSYCLONE_HOST").c_str();
	port= inimanager.GetValueInt("PSYCLONE_PORT");
	GretaName= inimanager.GetValueString("GRETA_NAME").c_str();
	

	RealtimeBML bmlengine(GretaName+REALTIMEBML_MODULE_NAME, host, port, GretaName);

	bmlengine.MainLoop();

	XercesTool::shutdownXMLTools();
	return 1;
}


int main(int argc, char ** argv){

	std::string filename;
	std::string directory;

	if (argc==2)
	{
		filename=argv[1];
		directory="";

	} else 
	if (argc==3)
	{
		filename=argv[1];
		directory=argv[2];

	} else {
        
		printf("First argument - filenamebase, second argument - directory name");
		
		return 1;		
	}

	/*
	Converter con1;
	con1.BReadMCMessage("c:\\zigntrack\\mouth.trc");
	FAPwriter fw1;
	fw1.WriteToFile(con1.converttoFAPs(),"c:\\zigntrack\\mouth.fap",25);

	Converter con2;
	con2.BReadMCMessage("c:\\zigntrack\\eyebrow.trc");
	FAPwriter fw2;
	fw2.WriteToFile(con2.converttoFAPs(),"c:\\zigntrack\\eyebrow.fap",25);

	Converter con3;
	con3.EReadMCMessage("c:\\zigntrack\\mouth.txt");
	FAPwriter fw3;
	fw3.WriteToFile(con3.EconverttoFAPs(),"c:\\zigntrack\\mouth2.fap",25);

	Converter con4;
	con4.EReadMCMessage("c:\\zigntrack\\rotation.txt");
	FAPwriter fw4;
	fw4.WriteToFile(con4.EconverttoFAPs(),"c:\\zigntrack\\rotation2.fap",25);
	
	Converter con5;
	con5.EReadMCMessage("c:\\zigntrack\\eyebrow.txt");
	FAPwriter fw5;
	fw5.WriteToFile(con5.EconverttoFAPs(),"c:\\zigntrack\\eyebrow2.fap",25);
	

	Converter con6;
	con6.EReadMCMessage("c:\\zigntrack\\Emotions_Ben_White.txt");
	FAPwriter fw6;
	fw6.WriteToFile(con6.EconverttoFAPs(),"c:\\zigntrack\\Emotions_Ben_White.fap",25);
	
	Converter con7;
	con7.EReadMCMessage("c:\\zigntrack\\Rotations_Ben_White.txt");
	FAPwriter fw7;
	fw7.WriteToFile(con7.EconverttoFAPs(),"c:\\zigntrack\\Rotations_Ben_White.fap",25);
	
	Converter con8;
	con8.EReadMCMessage("c:\\zigntrack\\Face_Ben_White.txt");
	FAPwriter fw8;
	fw8.WriteToFile(con8.EconverttoFAPs(),"c:\\zigntrack\\Face_Ben_White.fap",25);
	*/

	
	OptiTrackConverter con9;
	
	std::string path = directory + filename + ".txt";
	con9.readHeadMCMessage( (char*)path.c_str() );
	FAPwriter fw9;
	path = directory + filename+ ".fap";
	fw9.WriteToFile(con9.converttoFAPs(), path, 25);
	

//OptiTrackConverter con20;
//	
//	std::string path = "c:\\zigntrack\\Loic_Take2_corr_big_laugh2.txt";
//	con20.readHeadMCMessage( (char*)path.c_str() );
//
//	FAPwriter fw20;
//	path = "c:\\zigntrack\\Loic_Take2_corr_big_laugh2.fap";
//	fw20.WriteToFile(con20.converttoFAPs(), path, 25);
//	
//
//	
//OptiTrackConverter con21;
//	
//	path = "c:\\zigntrack\\PJ_Take3_corr_big_laugh.txt";
//	con21.readHeadMCMessage( (char*)path.c_str() );
//
//	FAPwriter fw21;
//	path = "c:\\zigntrack\\PJ_Take3_corr_big_laugh.fap";
//	fw21.WriteToFile(con21.converttoFAPs(), path, 25);
//
//	
//OptiTrackConverter con22;
//	
//	path = "c:\\zigntrack\\Stella_Take3_corr_big_laugh.txt";
//	con22.readHeadMCMessage( (char*)path.c_str() );
//
//	FAPwriter fw22;
//	path = "c:\\zigntrack\\Stella_Take3_corr_big_laugh.fap";
//	fw22.WriteToFile(con22.converttoFAPs(), path, 25);
//
//	
//OptiTrackConverter con23;
//	
//	path = "c:\\zigntrack\\Elisabetta_Take4_corr_big_laugh.txt";
//	con23.readHeadMCMessage( (char*)path.c_str() );
//
//	FAPwriter fw23;
//	path = "c:\\zigntrack\\Elisabetta_Take4_corr_big_laugh.fap";
//	fw23.WriteToFile(con23.converttoFAPs(), path, 25);
//
//
//	OptiTrackConverter con10;
//	
//	path = "c:\\zigntrack\\test_calibration_greta.txt";
//	con10.readMCMessage( (char*)path.c_str() );
//
//	FAPwriter fw10;
//	path = "c:\\zigntrack\\test_calibration_greta.fap";
//	fw10.WriteToFile(con10.converttoFAPs(), path, 25);

	/*OptiTrackConverter con9;
	
	std::string path = "c:\\zigntrack\\Loic2.txt";
	con9.readHeadMCMessage( (char*)path.c_str() );

	FAPwriter fw9;
	path = "c:\\zigntrack\\Loic2.fap";
	fw9.WriteToFile(con9.converttoFAPs(), path, 25);
	

	OptiTrackConverter con10;
	
	path = "c:\\zigntrack\\test_calibration_greta.txt";
	con10.readMCMessage( (char*)path.c_str() );

	FAPwriter fw10;
	path = "c:\\zigntrack\\test_calibration_greta.fap";
	fw10.WriteToFile(con10.converttoFAPs(), path, 25);
	*/

	//OptiTrackConverter con9;
	//
	//std::string path = "c:\\zigntrack\\Loic_Motion_nohead.txt";
	//con9.readMCMessage( (char*)path.c_str() );

	//FAPwriter fw9;
	//path = "c:\\zigntrack\\Loic_Motion_nohead.fap";
	//fw9.WriteToFile(con9.converttoFAPs(), path, 25);
	//

	/*
	Converter con10;
	con10.EReadMCMessage("c:\\zigntrack\\Rotations_Ben_White.txt");
	FAPwriter fw10;
	fw10.WriteToFile(con10.HeadRotationConverttoFAPs(), "c:\\zigntrack\\headrotationonly.fap",25);
	*/

	return 0;
}




//int main(int argc, char ** argv)
//{
//	
//	std::string GretaName;
//	std::string host;
//	int port;
//	
//	if(argc==2)
//	{
//		ini_filename=argv[1];
//	} else {
//		ini_filename="greta.ini";	
//	}
//
//	inimanager.ReadIniFile(ini_filename);
//
//	/*datacontainer = new DataContainer();
//	
//	int code=datacontainer->initBMLEngine();
//	if (code==0) {
//		printf("Problem : out \n");
//		exit(1);
//	}*/
//
//	host= inimanager.GetValueString("PSYCLONE_HOST").c_str();
//	port= inimanager.GetValueInt("PSYCLONE_PORT");
//	GretaName= inimanager.GetValueString("GRETA_NAME").c_str();
//	
//
//	MCServer bmlengine(GretaName+REALTIMEBML_MODULE_NAME, host, port, GretaName);
//
//	bmlengine.MainLoop();
//
//	return 1;
//}
