
#include <stdio.h>
#include <tchar.h>

#include <fstream>

#include <iostream>

#include "NaoGesture.h"

#include "NaoRobot.h"
#include "XercesTool.h"
#include "IniManager.h"
#include "DataContainer.h"

#ifndef _WIN32
#include <signal.h>
#endif

DataContainer * datacontainer;
IniManager inimanager;
std::string ini_filename;

int _tmain(int argc, char *argv[] )
{

	ini_filename="greta.ini";

	XercesTool::startupXMLTools();

	inimanager.ReadIniFile(ini_filename);

	datacontainer = new DataContainer();

	//load faces, gestures etc
	int code=datacontainer->initBMLEngine();	
	if (code==0) {
		printf("Problem : out \n");
		exit(1);
	}
 
	NaoGesture gesture;

	int n= 0;
	while((n!=13) && (n!=27))
	{
		if (kbhit()) 		
		{
			n=getch();	
			if(n==120) // The key X is pressed
			{
				gesture.LoadFile();
				gesture.PrintScreen();
				gesture.Interpret();
				
			}

			if(n==122) // The key Z is pressed
			{
				gesture.Print_angles();
			}

			if(n==13) break;
		}
		}


	
	// start here
//   NaoRobot my_robot;

//   my_robot.print_body_angles();
//   my_robot.Test();

   

   XercesTool::shutdownXMLTools();


	return 0;
}

