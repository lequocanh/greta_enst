
#include <stdio.h>
#include <tchar.h>

#include <fstream>

#include <iostream>

#include "NaoRobot.h"
//#include "NaoPlayer.h"
//#include "XercesTool.h"


#include <ALError.h>

//extern IniManager inimanager;	

string NaoRobot::jointnames[26];

                #define AXIS_MASK_X 1
                #define AXIS_MASK_Y 2 
                #define AXIS_MASK_Z 4 
                #define AXIS_MASK_WX 8
                #define AXIS_MASK_WY 16 
                #define AXIS_MASK_WZ 32
                #define AXIS_MASK_ALL 63
                #define AXIS_MASK_VEL 7
                #define AXIS_MASK_ROT 56

NaoRobot::NaoRobot()
{
  // Default parent broker IP
  std::string parentBrokerIP = "192.168.2.4";
  // Default parent broker port
  int parentBrokerPort = 9559;

// 	parentBrokerIP=inimanager.GetValueString("NAO_HOST").c_str();
//	parentBrokerPort=inimanager.GetValueInt("NAO_PORT");	

	
  				try
				{
					motionProxy  = new AL::ALProxy("ALMotion", parentBrokerIP, parentBrokerPort);
				// This will throw an exception
				}
				catch( AL::ALError &err )
				{
					std::cout << err.toString() << std::endl;
				}


				try
				{
					leds = new AL::ALProxy("ALLeds", parentBrokerIP, parentBrokerPort); 
				}
				catch( AL::ALError &err )
				{
					std::cout << err.toString() << std::endl;
				}
	leds = 0;
	cameraProxy = 0;
	ttsProxy = 0;

jointnames[0] = string("HeadYaw");
jointnames[1] = string("HeadPitch");
jointnames[2] = string("LShoulderPitch"); // shoulder_y = l_shoulder_twisting = a[36]
jointnames[3] = string("LShoulderRoll");  // shoulder_z = l_shoulder_abduct = a[34]
jointnames[4] = string("LElbowYaw");      // elbow_y = l_elbow_twisting = a[40]
jointnames[5] = string("LElbowRoll");	  // elbow_x = l_elbow_flexion = a[38]
jointnames[6] = string("LWristYaw");	  // wrist_y = l_wrist_twisting = a[46]
jointnames[7] = string("LHand");
jointnames[8] = string("LHipYawPitch");
jointnames[9] = string("LHipRoll");
jointnames[10] = string("LHipPitch");
jointnames[11] = string("LKneePitch");
jointnames[12] = string("LAnklePitch");
jointnames[13] = string("LAnkleRoll");
jointnames[14] = string("RHipYawPitch");
jointnames[15] = string("RHipRoll");
jointnames[16] = string("RHipPitch");
jointnames[17] = string("RKneePitch");
jointnames[18] = string("RAnklePitch");
jointnames[19] = string("RAnkleRoll");
jointnames[20] = string("RShoulderPitch");
jointnames[21] = string("RShoulderRoll");
jointnames[22] = string("RElbowYaw");
jointnames[23] = string("RElbowRoll");
jointnames[24] = string("RWristYaw");
jointnames[25] = string("RHand");

this->motion_StiffnessOn();



this->motion_PoseZero();
}

NaoRobot::~NaoRobot(void)
{	
	//int n = 0;
	//std::cout<<"Press Enter or ESC to exit!";
	//while((n!=13) && (n!=27))
	//{
	//	if (kbhit()) n=getch();			
	//}

	// Phai luon luon nam tay lai
	this->Relax();

	this->motion_StiffnessOff();

	if(motionProxy!=0)
		delete motionProxy;
	if(cameraProxy!=0)
		delete cameraProxy;
	if(ttsProxy!=0)
		delete ttsProxy;
	if(leds!=0)
		delete leds;
}
/*********************************************/
vector<float> NaoRobot::get_angles()
{
	return(motionProxy->call<vector<float>>("getBodyAngles"));
}

/*********************************************/
void NaoRobot::make_motion_left_arm(vector<float> lVec6, float *lHand)
{
	int INTERPOLATION_TYPE = 1; 
	int SECONDS = 2;
	int MASK = 7;
	int SPACE = 0; // Task space {SPACE_BODY = 0, SPACE_SUPPORT_LEG = 1 }
	motionProxy->pCall("gotoPosition",string("LArm"),SPACE,lVec6,MASK,SECONDS,INTERPOLATION_TYPE);

	motionProxy->pCall("gotoAngle",string("LHand"),*lHand,1,INTERPOLATION_TYPE);

}
void NaoRobot::make_motion_right_arm(vector<float> rVec6, float *rHand)
{
	int INTERPOLATION_TYPE = 1; 
	int SECONDS = 2;
	int MASK = 7;
	int SPACE = 0; // Task space {SPACE_BODY = 0, SPACE_SUPPORT_LEG = 1 }
	motionProxy->pCall("gotoPosition",string("RArm"),SPACE,rVec6,MASK,SECONDS,INTERPOLATION_TYPE);

	motionProxy->pCall("gotoAngle",string("RHand"),*rHand,SECONDS,INTERPOLATION_TYPE);
}
//make motion
void NaoRobot::make_motion(vector<float> lVec6, vector<float> rVec6, float *lHand, float *rHand )
{
	int INTERPOLATION_TYPE = 1; 
	int SECONDS = 2;
	int MASK = 7;
	int SPACE = 0; // Task space {SPACE_BODY = 0, SPACE_SUPPORT_LEG = 1 }

	lVec6[3] = 0;
	lVec6[4] = -3.14 * 90 / 180 ;
	lVec6[5] = 0;

	rVec6[3] = 0;
	rVec6[4] = -3.14 * 90 / 180 ;
	rVec6[5] = 0;

	motionProxy->pCall("gotoPosition",string("LArm"),SPACE,lVec6,MASK,SECONDS,INTERPOLATION_TYPE);

	motionProxy->pCall("gotoPosition",string("RArm"),SPACE,rVec6,MASK,SECONDS,INTERPOLATION_TYPE);

	motionProxy->pCall("gotoAngle",string("LHand"),*lHand,SECONDS,INTERPOLATION_TYPE);
	
	motionProxy->pCall("gotoAngle",string("RHand"),*rHand,SECONDS,INTERPOLATION_TYPE);


	//SleepMs(20);

}
//

// Some tests for the API gotoPosition
/*
void gotoPosition (string pChainName, int pSpace, vector<float> pPosition, int pAxisMask, float pDuration, int pInterpolationType)
Parameters

string pChainName

    Name of the chain. Could be: "Head", "LArm","RArm", "LLeg", "RLeg"

int pSpace

    Task space

vector<float> pPosition

    6D position array (x,y,z,wx,wy,wz) in meters and radians

int pAxisMask

    Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 

float pDuration

    Duration of the interpolation in seconds

int pInterpolationType

    Type of interpolation { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }

	float v[] = {0.1,0.4,0.5,0.6,0,0};
	int s = sizeof( v ) / sizeof( v[0] );
	vector<float> Position(v, &v[s]);

	motionProxy->pCall("gotoPosition",string("LArm") , 
			0,        // pSpace The task space {SPACE_BODY = 0, SPACE_SUPPORT_LEG = 1 }
			Position, // 6D position array (x,y,z,wx,wy,wz) in meters and radians
			7,        // Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 	
					  // (x,y,z,wx,wy,wz) = (1,2,4,8,16,32)
			2,		  // Duration of the interpolation in seconds
			1);       // Type of interpolation { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }

*/
void NaoRobot::Test()
{
	int INTERPOLATION_TYPE = 1; 
	int SECONDS = 2;
	int MASK = 7;
	int SPACE = 0; // Task space {SPACE_BODY = 0, SPACE_SUPPORT_LEG = 1 }
	int n = 0;
	float m = 0.02;
	int inversion = 1;
	vector<float> P1;

	vector<float> P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);
	for(int i=0;i<6;i++) std::cout << P[i] << ","; std::cout<<endl;

	
	motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);

//	P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);
//	for(int i=0;i<3;i++) std::cout << P[i] << ":"; 
//	for(int i=3;i<6;i++) std::cout << P[i]*(180)/3.14 << ":"; 
//	std::cout<<endl;

	//void setPosition ( std::string pChainName, int pSpace, 
     //                   vector<float> pPosition, int pAxisMask



//	float v[] = {0.0796568,-0.00019507,0.0611382,-2.29732,0.0675005,-1.45374};

//	int s = sizeof( v ) / sizeof( v[0] );
//	vector<float> Position(v, &v[s]);
	
	
//	motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,Position,MASK,SECONDS,INTERPOLATION_TYPE);
	
//	P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);
//	for(int i=0;i<6;i++) std::cout << P[i] << ":"; std::cout<<endl;



	this->motion_StiffnessOff();

	while((n!=13) && (n!=27))
	{
		if (kbhit()) 		
		{
			n=getch();	
			if(n==120) // The key X is pressed
			{
				this->motion_StiffnessOn();
				P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);				
				for(int i=0;i<6;i++) std::cout << P[i] << " : "; std::cout<<endl;

				P = motionProxy->call<vector<float>>("getPosition", string("RArm"), SPACE);				
				for(int i=0;i<6;i++) std::cout << P[i] << " : "; std::cout<<endl<<endl;
				this->motion_StiffnessOff();
			}
			if(n==122) // The key Z is pressed
			{
				this->motion_StiffnessOn();
				motionProxy->callVoid("gotoPosition",string("RArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);			
				this->motion_StiffnessOff();
			}
			if(n==118) // The key V is pressed
			{
				P[0]= 0.10;
				P[1]= 0.10;
					P[2]= 0.10;
					P[3]= 0.566695;
					P[4]= -1.50016;
					P[5]= -0.508454 ;
				this->motion_StiffnessOn();
				//int INTERPOLATION_TYPE = 1; 
				//int SECONDS = 2;
				//int MASK = 7;
				//int SPACE = 0; // Task space {SPACE_BODY = 0, SPACE_SUPPORT_LEG = 1 }
				motionProxy->callVoid("gotoPosition",string("RArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);
				this->motion_StiffnessOff();
			}
	
		}
	}
	return;


	while((n!=13) && (n!=27))
	{
		if (kbhit()) 		
		{
			n=getch();			
			std::cout << n << endl;

			if(n==118) // The key V is pressed
				inversion = inversion*(-1);

			if(n==120) // The key X is pressed
			{
				MASK = 1;			
				P[0] += m*inversion;
				motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);
			
				P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);				
				for(int i=0;i<3;i++) std::cout << P[i] << ":"; std::cout<<endl;

				//max: 0.198
			}

			if(n==121) // The key Y is pressed
			{
				MASK = 2;
				P[1] += m*inversion;
				motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);

				P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);
				for(int i=0;i<3;i++) std::cout << P[i] << ":"; std::cout<<endl;

				// max: (x,y,z) = (-0.0691, 0.3223, 0.3855)
			}

			if(n==122) // The key Z is pressed
			{
				MASK = 4;
				P[2] += m*inversion;
				motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);

				P = motionProxy->call<vector<float>>("getPosition", string("LArm"), SPACE);
				for(int i=0;i<3;i++) std::cout << P[i] << ":"; std::cout<<endl;

				// max: (x,y,z) = (-0.0526888, 0.156196, 0.522431)
			}

			if(n==72) // Mui ten len tren (quay theo Z)
			{
				cout<<"dwz orientation"<<endl;
				MASK = 32;
				P[5] += 20*m*inversion;
				motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);

			
			}

			if(n==80) // Mui ten xuong duoi (quay theo X)
			{
				cout<<"dwx orientation"<<endl;
				MASK = 8;
				P[3] += 20*m*inversion;
				motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);

			}

			if(n==77) // Mui ten sang phai (quay theo Y)
			{
				cout<<"dwy orientation"<<endl;
				MASK = 16;
				P[4] += 20*m*inversion;
				motionProxy->callVoid("gotoPosition",string("LArm"),SPACE,P,MASK,SECONDS,INTERPOLATION_TYPE);

			}
		}
	}

	std::cout << "Ket thuc thuc hien" <<endl;

	
}
//demo
void NaoRobot::motion_Demo()
{
	    		// Put Head Stifnness to 1.0
			this->motion_StiffnessOn();
  			SleepMs(2000);
			
			// get current position of the left shoulder pitch
			float LShoulderPitchPosition =  motionProxy->call<float>("getAngle",string("LShoulderPitch"));
			
			// Get the minAngle, maxAngle, and maxChangePerCycle for left shoulder pitch
			vector<float> LShoulderPitchLimits = motionProxy->call<vector<float>>("getJointLimits", string("LShoulderPitch"));

			//

			
		motionProxy->callVoid("setBalanceMode",1);//(motion.BALANCE_MODE_AUTO)

	float PositionRad1[] = 
	{-0.3, 0, // "HeadYaw", "HeadPitch"
	0.2, 0.2, -2.0, -1.30, 1.50, 0, //"LShoulderPitch", "LShoulderRoll", "LElbowYaw", "LElbowRoll", "LWristYaw", "LHand"
	0, 0, -0.8, 1.3, -0.6, 0, //"LHipYawPitch", "LHipRoll", "LHipPitch", "LKneePitch", "LAnklePitch", "LAnkleRoll"
	0, 0, -0.70, 1.30, -0.60, 0, //"RHipYawPitch", "RHipRoll", "RHipPitch", "RKneePitch", "RAnklePitch", "RAnkleRoll"
	1.20, 0, 0, 0, 0, 0};//"RShoulderPitch", "RShoulderRoll", "RElbowYaw", "RElbowRoll", "RWristYaw", "RHand"
	
	float PositionRad2[] = 
	{0.3, 0, // "HeadYaw", "HeadPitch"
	0.2, 0.2, -1.0, -1.30, 1.50, 0, //"LShoulderPitch", "LShoulderRoll", "LElbowYaw", "LElbowRoll", "LWristYaw", "LHand"
	0, 0, -0.8, 1.3, -0.6, 0, //"LHipYawPitch", "LHipRoll", "LHipPitch", "LKneePitch", "LAnklePitch", "LAnkleRoll"
	0, 0, -0.70, 1.30, -0.60, 0, //"RHipYawPitch", "RHipRoll", "RHipPitch", "RKneePitch", "RAnklePitch", "RAnkleRoll"
	1.20, 0, 0, 0, 0, 0};//"RShoulderPitch", "RShoulderRoll", "RElbowYaw", "RElbowRoll", "RWristYaw", "RHand"


	int size = sizeof( PositionRad1 ) / sizeof( PositionRad1[0] );
	
	
	this->motion_StiffnessOn();

	

	for(int i=0;i<2;i++)
	{
	vector<float> PositionVector1(PositionRad1, &PositionRad1[size]);
	// Send angles through a smooth interpolation of one second.
	motionProxy->callVoid("gotoBodyAnglesWithSpeed",PositionVector1 , 
        50,   // 90% of the minimum of max speed of each motors
        1);//motion.INTERPOLATION_SMOOTH)

	vector<float> PositionVector2(PositionRad2, &PositionRad2[size]);
	// Send angles through a smooth interpolation of one second.
	motionProxy->callVoid("gotoBodyAnglesWithSpeed",PositionVector2 , 
        40,   // 90% of the minimum of max speed of each motors
        1);//motion.INTERPOLATION_SMOOTH)

	}
  // This will do the same movement with the same timing on two joints
  // Each joint can have different angles and times, as long as the number
  // of times for each joint corresponds to the number of angles
  ALValue motors, allAngles, allTimes, angles, times;
  motors.arrayPush("HeadYaw"); 
  motors.arrayPush("HeadPitch");
  times.arrayPush(1.0f); // times in seconds
  times.arrayPush(2.0f);
  times.arrayPush(3.0f);
  allTimes.arrayPush(times); // HeadYaw times
  allTimes.arrayPush(times); // HeadPitch times
  angles.arrayPush(0.0f); // angles in radians
  angles.arrayPush(0.2f);
  angles.arrayPush(0.0f);
  allAngles.arrayPush(angles); // HeadYaw angles
  allAngles.arrayPush(angles); // HeadPitch angles
  int smoothInterpolation = 1;

  motionProxy->callVoid("doMove",motors,allAngles,allTimes,smoothInterpolation);



  this->Relax();

  motionProxy->callVoid("setBodyStiffness",0);

return;
			
			//// Movements
			//while(LShoulderPitchPosition > 0.4){
			//	LShoulderPitchPosition = LShoulderPitchPosition-0.4;
			//	motionProxy->callVoid("gotoAngle",string("LShoulderPitch"),
			//						LShoulderPitchPosition, // Target angles in radians
			//						0.4, //Duration of interpolation in seconds.
			//						1);//Type of interpolation to do. { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }
			//	std::cout << LShoulderPitchPosition << std::endl;
			//	
			//}

			//
			//while(LShoulderPitchPosition < LShoulderPitchLimits[1]-0.4){
			//	LShoulderPitchPosition = LShoulderPitchPosition+0.4;
			//	motionProxy->callVoid("gotoAngle",string("LShoulderPitch"),
			//						LShoulderPitchPosition, // Target angles in radians
			//						0.4, //Duration of interpolation in seconds.
			//						1);//Type of interpolation to do. { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }
			//	std::cout << LShoulderPitchPosition << std::endl;
			//}
			


			motionProxy->callVoid("gotoAngle",string("LShoulderPitch"),
									0.2, // Target angles in radians
									0.4, //Duration of interpolation in seconds.
									1);//Type of interpolation to do. { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }

	
			//get current position of the left wrist
			float LWristYawPosition = motionProxy->call<float>("getAngle", string("LWristYaw"));
			std::cout << "Vi tri co tay hien tai: " << LWristYawPosition << std::endl;
			motionProxy->callVoid("gotoAngle",string("LWristYaw"),
									1.58035, // Target angles in radians
									0.4, //Duration of interpolation in seconds.
									1);//Type of interpolation to do. { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }


			//open the fingers
			motionProxy->callVoid("openHand",string("LHand"));			

			// get current position of the elbow
			float LElbowYawPosition = motionProxy->call<float>("getAngle", string("LElbowYaw"));
			// Get the minAngle, maxAngle, and maxChangePerCycle for left elbow
			vector<float> LElbowYawLimits = motionProxy->call<vector<float>>("getJointLimits", string("LElbowYaw"));

			// Get the minAngle, maxAngle, and maxChangePerCycle for head
			vector<float> HeadYawLimits = motionProxy->call<vector<float>>("getJointLimits", string("HeadYaw"));

			for(int i=0;i<5; i++)
			{
			// Movements of Elbow
			motionProxy->callVoid("gotoAngle",string("LElbowYaw"),
									LElbowYawLimits[0], // Target angles in radians
									0.4, //Duration of interpolation in seconds.
									1);//Type of interpolation to do. { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }
		
			motionProxy->callVoid("gotoAngle",string("HeadYaw"),
									0.5,
									0.4,
									1);

			motionProxy->callVoid("gotoAngle",string("LElbowYaw"),
									LElbowYawLimits[0]+1, // Target angles in radians
									0.4, //Duration of interpolation in seconds.
									1);//Type of interpolation to do. { INTERPOLATION_LINEAR = 0, INTERPOLATION_SMOOTH = 1 }

			motionProxy->callVoid("gotoAngle",string("HeadYaw"),
									-0.5,
									0.4,
									1);

			}

			motionProxy->callVoid("closeHand",string("LHand"));			

			// Put Head Stifnness to 0.0
			this->motion_PoseInit();
			SleepMs(2000);
}

/*************************************/
void NaoRobot::print(vector<float> angles)
{
	vector<float>::iterator p = angles.begin();	
	while ( p < angles.end() )
		cout << *p++ << " ";
	cout << endl;
	
	/*
	cout.precision(3);
	for(int i=0;i<26;i++)
	{
		vector<float> angles = motionProxy->call<vector<float>>("getJointLimits",jointnames[i]);
		cout << "joint name: "<< jointnames[i] << ":"<<angles[0] <<"-"<<angles[0]*180<< ":" <<angles[1]<<"-"<<angles[1]*180 <<":"<<angles[2]<<endl;
	}
	*/
}
// print screen body angles
void NaoRobot::print_body_angles(void)
{
	this->motion_StiffnessOn();
	vector<float> angles = motionProxy->call<vector<float>>("getBodyAngles");
	this->print(angles);
}

// robot says	
void NaoRobot::text_to_speech(std::string text)
{
	//Sets the language to english
	ttsProxy->callVoid("setLanguage",string("French"));
	
	vector<string> t = ttsProxy->call<vector<string>>("getAvailableLanguages");

	vector<string>::iterator p = t.begin();
	cout << "List of languges that Nao can speak:" <<endl;
	while ( p < t.end() )
		cout << *p++ << " ";
	cout << endl;
	
	// Applies a pitch shifting to the voice
	//ttsProxy->callVoid("setParam",string("pitchShift"), 1.5);
	// Deactivates double voice
	//ttsProxy->callVoid("setParam",string("doubleVoice"), 0.0);

	cout<<"Lists the avaiable voices"<<endl;
	vector<string> av = ttsProxy->call<vector<string>>("getAvailableVoices");
	p = av.begin();	
	while ( p < av.end() )
		cout << *p++ << " ";
	cout << endl;

	// set feminine voice
	ttsProxy->callVoid("setVoice",string("Julie22Enhanced"));

	// Sets the sound level of the system (0-100)
	ttsProxy->callVoid("setSystemVolume",50);

	// Says a test string
	ttsProxy->callVoid("say",text);


}

// get images from two camers
void NaoRobot::get_cameras(void)
{

                // First you have to declare an ALValue to get the video buffer.
                ALValue image;
                // Then declare the size
                image.arraySetSize(7); 
                //Now you can get the data.
				std::string pGVMNameId = "my_GVM";
				pGVMNameId = cameraProxy->call<std::string>( "subscribe", pGVMNameId , 2, 11, 30 );


//while( true )
//{	
//	if (_kbhit()) break;
//}
  
{
                image = cameraProxy->call<ALValue>( "getImageRemote", pGVMNameId );

                // You can get some informations about the image.
                int width = (int) image[0];
                int height = (int) image[1];
                int nbLayers = (int) image[2];
                int colorSpace = (int) image[3];
                //long long timeStamp = ((long long)image[4])*1000000LL + (long long)image[5];
                // image[4] is the number of seconds, image[5] the number of microseconds
//        long long timeStamp = ((long long)(int)image[4])*1000000LL +
//            ((long long)(int)image[5]);
//        int seconds = (int)(timeStamp/1000000LL);
//        printf("Retrieved an image of dimensions %ix%i, color space %i,"
//               "with %i layers and a time stamp of %is \n",
//               width, height, colorSpace,nbLayers,seconds);

                // You can get the pointer to the image data and its size
                const unsigned char* dataPointer =  static_cast<const unsigned char*>(image[6].GetBinary());
                int size = image[6].getSize();

				// hien thi

  // show the image


  IplImage* img = cvCreateImage(cvSize(width,height),8,0);
  img->imageData = (char*)dataPointer;
 // create a window
  cvNamedWindow("mainWin", CV_WINDOW_AUTOSIZE); 
  cvMoveWindow("mainWin", 100, 100);   	
  cvShowImage("mainWin", img );

  // wait for a key
 cvWaitKey(0);


  // release the image
  cvReleaseImage(&img );
  
}
  
}
// make Nao go to the zero position
void NaoRobot::motion_PoseInit()
{	
		float PositionRad[] = 
		{0, 0, 
		90*(3.14/180), 20*(3.14/180), 0, 0, 0, 0, 
		0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 
		90*(3.14/180), (-20)*(3.14/180), 0, 0, 0, 0};
		//int data[] = {11, 12, 23, 34};
		int size = sizeof( PositionRad ) / sizeof( PositionRad[0] );
		//vector<int> vec( data, &data[ size ] );
		vector<float> PositionVector(PositionRad, &PositionRad[size]);

		// Send angles through a smooth interpolation of one second.
		motionProxy->callVoid("gotoBodyAnglesWithSpeed",PositionVector , 
			10,   // 30% of the minimum of max speed of each motors
			1);//motion.INTERPOLATION_SMOOTH)
}
// relax

void NaoRobot::Relax(void)
{
	motionProxy->callVoid("setBalanceMode",0);//(motion.BALANCE_MODE_OFF)

	float PositionRad[] = {0.0, 0.015298, 1.43271, 0.12728, -0.675002, -1.07069, -1.65523, 0.0, -0.300625, -0.0783416, -0.760086, 2.18445, -1.2242, 0.103589, -0.300625, 0.0537223, -0.771299, 2.19323, -1.22272, -0.0998575, 1.55398, -0.00464396, 0.935698, 1.03549, 1.12285, 0.0};
	int size = sizeof( PositionRad ) / sizeof( PositionRad[0] );
	vector<float> PositionVector(PositionRad, &PositionRad[size]);
	this->motion_StiffnessOn();
	motionProxy->callVoid("gotoBodyAnglesWithSpeed",PositionVector , 
        10,   // 30% of the minimum of max speed of each motors
        1);//motion.INTERPOLATION_SMOOTH)
}
// make Nao go to its initial position
void NaoRobot::motion_PoseZero(void)
{
	
	float PositionRad[] = 
	{0, 0, 
	3.14*70/180, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 
	3.14*70/180, 0, 0, 0, 0, 0};

	int size = sizeof( PositionRad ) / sizeof( PositionRad[0] );
	vector<float> PositionVector(PositionRad, &PositionRad[size]);
	this->motion_StiffnessOn();
	motionProxy->callVoid("gotoBodyAnglesWithSpeed",PositionVector , 
        10,   // 30% of the minimum of max speed of each motors
        1);//motion.INTERPOLATION_SMOOTH)
}

// Make Nao move his arms
void NaoRobot::motion_ArmsMotionBalancing(void)
{
	// Get the number of joints
	//std::vector<std::string>::size NumJoints =  motionProxy->call<vector<string>>("getBodyJointNames").size;
	
	std::vector<std::string> jointNames = motionProxy->call<vector<string>>("getBodyJointNames");

	int InitialPosition[22]	;
	int InitialPosition22[]
					= {   0,   0,                     // Head
                         120,  15,  -90, -80,         // LArm
                           0,   0,  -30,  60, -30, 0, // LLeg
                           0,   0,  -30,  60, -30, 0, // RLeg
						   120, -15,   90,  90 };        // RArm

	int InitialPosition26[]
					= {   0,   0,                     // Head
                         120,  15,  -90, -80,   0, 0, // LArm
                           0,   0,  -30,  60, -30, 0, // LLeg
                           0,   0,  -30,  60, -30, 0, // RLeg
						   120, -15,   90,  90,   0, 0}; // RArm

	if(jointNames.size() == 22){

		for(int i=0;i<22;i++) InitialPosition[i]=InitialPosition22[i];

	}else if(jointNames.size() == 26){
		for(int i=0;i<22;i++) InitialPosition[i]=InitialPosition26[i];
	} else{
		printf("Unexpected number of joints\n");
		exit(1);
	}

	// chuyen sang radian

	vector<float> t = vector<float>(22);

	for(int i=0;i<22;i++) 
		t.push_back (float(InitialPosition[i] / 180)); // TO_RAD = (PM_PI/180)


	motionProxy->callVoid("setBalanceMode",0);//(motion.BALANCE_MODE_OFF)

	
	// Smooth interpolation of 1s
	motionProxy->callVoid("gotoBodyAngles",  t, 1.0, 1);// motion.INTERPOLATION_SMOOTH)


//#*************************************************************
//# If you Uncomment these three lines the robot will automatically 
//# compensate arm motions
//#*************************************************************
//
//#motionProxy->setSupportMode(motion.SUPPORT_MODE_DOUBLE_RIGHT)
//#motionProxy->setBalanceMode(motion.BALANCE_MODE_AUTO)
//#time.sleep(1)
//
//# Intepolation time constant
float tInterpolation = 0.5;
vector<float> angleL;
vector<float> angleR;

if (jointNames.size() == 22) {
	angleL.push_back(0.0); angleL.push_back(0.0/180); angleL.push_back(-90.0/180);angleL.push_back(0.0);
	angleR.push_back(0.0); angleR.push_back(0.0/180); angleR.push_back(90.0/180);angleR.push_back(0.0);
}
else if (jointNames.size() == 26) {
	angleL.push_back(0.0); angleL.push_back(0.0/180); angleL.push_back(-90.0/180);angleL.push_back(0.0);angleL.push_back(0.0); angleL.push_back(0.0);
	angleR.push_back(0.0); angleR.push_back(0.0/180); angleR.push_back(90.0 /180);angleR.push_back(0.0);angleR.push_back(0.0); angleR.push_back(0.0);
}
else {
       printf("Unexpected number of Joints");
}

//#*************************************************************
//#  ------------------- ARM MOTION --------------------------
//#*************************************************************


//def moveArm(jointName, angle, isPost):
//    if isPost:
//        motionProxy->post.gotoAngle(jointName, angle * motion.TO_RAD, tInterpolation,
//                motion.INTERPOLATION_SMOOTH)
//    else:
//        motionProxy->gotoAngle(jointName, angle * motion.TO_RAD, tInterpolation,
//                motion.INTERPOLATION_SMOOTH)
//
//# Do the same movement 5 times
for(int i=0;i<5;i++){
	motionProxy->callVoid("post.gotoChainAngles",string("RArm"),angleR,tInterpolation,
		1);// motion.INTERPOLATION_SMOOTH
}
 
string joints[3] = {"LShoulderRoll", "LShoulderPitch", "LElbowYaw"};
int angles[3] = {90 , 90 , -90};

    //# Note that we synchronize all the "post" movements with a blocking call
for(int i=0;i<3;i++)
	motionProxy->callVoid(string("post.") + joints[i],angles[i]/180,tInterpolation,1);

motionProxy->callVoid("LElbowRoll", -90/180, tInterpolation,1);

motionProxy->callVoid("post.gotoChainAngles",string("LArm"), angleL, 
          tInterpolation,1);


joints[0] = "RShoulderRoll"; joints[1]="RShoulderPitch"; joints[2]="RElbowYaw";

angles[0] = -90; angles[1]= 90; angles[2]= 90;

for(int i=0;i<3;i++)
	motionProxy->callVoid(string("post.") + joints[i],angles[i]/180,tInterpolation,1);

motionProxy->callVoid("RElbowRoll", -90/180, tInterpolation,1);
    

//# Go to Start position

//# Don't forget to go back to BALANCE_MODE_OFF, or the legs will remain used by balancing
motionProxy->callVoid("setBalanceMode",0);
motionProxy->callVoid("gotoBodyAnglesWithSpeed",t, 50, 1);
}

// release
void NaoRobot::motion_StiffnessOff(void)
{
	// Setting body stiffness to 0.0
	motionProxy->callVoid("gotoBodyStiffness",0.0, 1.0, 0);
	
	
	//motionProxy->callVoid("setChainStiffness",string("LArm"),0.0);
	//motionProxy->callVoid("setChainStiffness",string("RArm"),0.0);
	

}

// Put all stifness
void NaoRobot::motion_StiffnessOn(void)
{
	motionProxy->callVoid("setBalanceMode",0);//(motion.BALANCE_MODE_OFF to full control)
	// Setting body stiffness to 1.0
	motionProxy->callVoid("gotoBodyStiffness",1.0, 1.0, 1);

	// Chi stiffness cho 2 tay thoi

	//motionProxy->callVoid("setChainStiffness",string("LArm"),1.0);
	//motionProxy->callVoid("setChainStiffness",string("RArm"),1.0);


/*
	// CHAIN

	# HEAD
#motionProxy->setChainStiffness('Head', 1.0);


#motionProxy->setChainStiffness('LArm', 1.0);
#motionProxy->setChainStiffness('RArm', 1.0);

# LEGS
#motionProxy->setChainStiffness('LLeg', 1.0);
#motionProxy->setChainStiffness('RLeg', 1.0);

#**************************
# JOINT
#**************************

# HEAD
#motionProxy->setJointStiffness('HeadPitch',1.0)
#motionProxy->setJointStiffness('HeadYaw',1.0)

# ARMS
#motionProxy->setJointStiffness('LShoulderPitch', 1.0)
#motionProxy->setJointStiffness('LShoulderRoll',  1.0)
#motionProxy->setJointStiffness('LElbowYaw',      1.0)
#motionProxy->setJointStiffness('LElbowRoll',     1.0)
#motionProxy->setJointStiffness('RShoulderPitch', 1.0)
#motionProxy->setJointStiffness('RShoulderRoll',  1.0)
#motionProxy->setJointStiffness('RElbowYaw',      1.0)
#motionProxy->setJointStiffness('RElbowRoll',     1.0)

# LEGS
#motionProxy->setJointStiffness('LHipYawPitch',   1.0)
#motionProxy->setJointStiffness('LHipRoll',       1.0)
#motionProxy->setJointStiffness('LHipPitch',      1.0)
#motionProxy->setJointStiffness('LKneePitch',     1.0)
#motionProxy->setJointStiffness('LAnklePitch',    1.0)
#motionProxy->setJointStiffness('LAnkleRoll',     1.0)
#motionProxy->setJointStiffness('RHipYawPitch',   1.0)
#motionProxy->setJointStiffness('RHipRoll',       1.0)
#motionProxy->setJointStiffness('RHipPitch',      1.0)
#motionProxy->setJointStiffness('RKneePitch',     1.0)
#motionProxy->setJointStiffness('RAnklePitch',    1.0)
#motionProxy->setJointStiffness('RAnkleRoll',     1.0)

print "Stiffness set to 1.0"

*/
}

/*
#
char ch;
#
 
#
cout << "key press detector : use �z�, �x� or arrow keys" << endl;
#
cout << "press CTRL+C to exit. \n\n" << endl;
#
 
#
while(true) // this is the main loop
#
        {
#
                if (_kbhit())
#
                {
#
                        // in case of a key is hit, do these
#
                        ch = getch();
#
                        switch(ch)
#
                        {
#
                        case �z� :
#
                                cout << "you pressed z" << endl;
#
                                break;
#
 
#
                        case �x� :
#
                                cout << "you pressed x" << endl;
#
                                break;
#
 
#
                        case �\0H� :
#
                                cout << "you pressed up arrow key" << endl;
#
                                break;
#
 
#
                        case �\0P� :
#
                                cout << "you pressed down arrow key" << endl;
#
                                break;
#
 
#
                        case �\0M� :
#
                                cout << "you pressed right arrow key" << endl;
#
                                break;
#
 
#
                        case �\0K� :
#
                                cout << "you pressed left arrow key" << endl;
#
                                break;
#
                        }
#
                        //below line displays the key code
#
                        cout << "key code=" << (int)ch << endl;
#
                        //exit (1); // in this example, it is "exit the program!"
#
                }
#
        }
*/