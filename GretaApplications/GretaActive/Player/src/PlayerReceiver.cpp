//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// PlayerReceiver.cpp: implementation of the PlayerReceiver class. 
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#include "PlayerReceiver.h"
#include "RealTimeAudio.h"

#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/exceptions/MessageFormatException.h>
#include <semaine/util/XMLTool.h>
#include <semaine/datatypes/xml/BML.h>
#include <semaine/datatypes/stateinfo/UserStateInfo.h>
#include <semaine/cms/message/SEMAINEStateMessage.h>


#include <cstdlib>
#include <sstream>
#include <string>
#include <decaf/lang/System.h>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;

using namespace semaine::cms::exceptions;
using namespace semaine::util;
using namespace semaine::datatypes::xml;
using namespace XERCES_CPP_NAMESPACE;

extern IniManager inimanager;

namespace semaine {
namespace components {
namespace greta {

PlayerReceiver::PlayerReceiver(const std::string & componentName, bool isInput, bool isOutput, RealtimePlayerWindow* rpw, CentralClock *pc) //, AnimationManager* am,//FramesManager* fm, CommandsManager* cm, CentralClock *pc) 
	throw(CMSException) :
	Component(componentName,isInput,isOutput)
{	
	finished = 0;
	this->pc = pc;
	pc->SetTime(meta.getTime());
	old_time=pc->GetTime();
	this->rpw=rpw;
	this->am=rpw->am;
	characterDisplaying = "Prudence";

	contextReceiver = new StateReceiver("semaine.data.state.context", semaine::datatypes::stateinfo::StateInfo::Type::ContextState); 
	receivers.push_back(contextReceiver);
	fapReceiver = new Receiver("semaine.data.synthesis.lowlevel.video.FAP"); 
	receivers.push_back(fapReceiver);
	bapReceiver = new Receiver("semaine.data.synthesis.lowlevel.video.BAP"); 
	receivers.push_back(bapReceiver);
	wavReceiver = new Receiver("semaine.data.synthesis.lowlevel.audio");
	receivers.push_back(wavReceiver);
	commandReceiver = new Receiver("semaine.data.synthesis.lowlevel.command");
	receivers.push_back(commandReceiver);
	callbackAudioSender = new XMLSender("semaine.callback.output.audio", "callback" , getName()); 
	senders.push_back(callbackAudioSender);
	callbackBAPSender = new XMLSender("semaine.callback.output.BAP", "callback" , getName()); 
	senders.push_back(callbackBAPSender);
	callbackFAPSender = new XMLSender("semaine.callback.output.FAP", "callback" , getName()); 
	senders.push_back(callbackFAPSender);

	this->start();


}

PlayerReceiver::~PlayerReceiver()
{
	delete fapReceiver;
	delete bapReceiver;
	delete wavReceiver;
	delete commandReceiver;
	delete callbackAudioSender;
	delete callbackBAPSender;
	delete callbackFAPSender;

}

void PlayerReceiver::customStartIO() throw(std::exception) 
{
	sound_time=0;
	RealTimeAudio::init();
	finished = 0;
	audiocreationID = 0;
	audioID = "";
	itercall = 0;
	fapcreationID = 0;
	fapID = "";
	bapcreationID = 0;
	bapID = "";
	old_time = 0;

	while(finished == 0)
		sleep(5);
}

void PlayerReceiver::setFinished(int finish) throw(std::exception) 
{
	finished = finish;
}


void PlayerReceiver::act() throw(CMSException)
{
	pc->SetTime(meta.getTime());
	long long time=pc->GetTime();
	if(time < old_time)
	{
		am->stop();
		am->incrementIDInHistory();
	}
	old_time = time;

}

void PlayerReceiver::react(SEMAINEMessage * m) throw(CMSException)
{
	if (m == NULL) {
		throw MessageFormatException("expected XML message, got "+std::string(typeid(*m).name()));
	}
	
	pc->SetTime(meta.getTime());
	long long time=pc->GetTime();
	if(time < old_time)
	{
		am->stop();
		am->incrementIDInHistory();
	}
	old_time = time;

	std::string content;
	long size;
	char * data;
	
	std::string type = m->getDatatype();
	
	const TextMessage * tm = dynamic_cast<const TextMessage*>(m->getMessage());
	const BytesMessage * bm = dynamic_cast<const BytesMessage*>(m->getMessage());

	if (tm != NULL) {
		content=tm->getText();
	} else if (bm != NULL) {
		const unsigned char * bodyBytes = bm->getBodyBytes();
		size = bm->getBodyLength();
		data = (char *) malloc(size);
		memcpy(data, bodyBytes, size);
	}

	if(type == "FAP") { 
		fapID = m->getContentID();
		//std::cout << "fapID";
		std::string animID = fapID.substr(0,fapID.find("_fap"));
		fapcreationID = m->getContentCreationTime();
		//std::cout << fapID <<" "<< pc->GetTime() <<"\n";
		am->addFAPFrames(animID,content);//fm->AddFAPFrame(content);
	 }

	if(type == "BAP") { 
		bapID = m->getContentID();
		bapcreationID = m->getContentCreationTime();
		am->addBAPFrames(bapID.substr(0,bapID.find("_bap")),content);//fm->AddBAPFrame(content);
	} 

	if(type == "AUDIO") { 
		std::ostringstream audioending;
		audioending << m->getContentID() <<"_audio";
		audioID = audioending.str();
		audiocreationID = m->getContentCreationTime();
		am->addAudio(m->getContentID(),data,size); //RealTimeAudio::load_wave(data,size);
	}

	if(type == "command") { 
		std::string command;
		command=content.c_str();
		//std::cout << command << "\n";
		if(command.find("STARTAT") != -1 || command.find("LIFETIME") != -1 || command.find("PRIORITY") != -1){
			
			std::string parser = command.substr(command.find("STARTAT")+8);
			parser.erase(parser.find_first_of("\n"));
			long long startat = atoi(parser.c_str());

			parser = command.substr(command.find("LIFETIME")+9);
			parser.erase(parser.find_first_of("\n"));
			long long lifetime = atoi(parser.c_str());

			parser = command.substr(command.find("PRIORITY")+9);
			parser.erase(parser.find_first_of("\n"));
			double priority = atof(parser.c_str());

			parser = command.substr(command.find("HASAUDIO")+9);
			parser.erase(parser.find_first_of("\n"));
			bool hasAudio = atof(parser.c_str());

			//std::cout << "HASAUDIO  "<< hasAudio << "\n";

			//std::cout << m->getContentID().c_str() << " " << pc->GetTime() <<"\n";
			am->setStartInfos(m->getContentID().substr(0,m->getContentID().find("_command")),priority, startat+pc->GetTime(), lifetime, hasAudio);
		}
	}

	if(type == "ContextState") { 
		SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(m);
		if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*m).name());
		semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
		if (state->hasInfo("character"))
		{
			std::string chname = state->getInfo("character");
			if(chname != characterDisplaying){
				characterDisplaying = chname;
				am->stop();
				rpw->glwindow->setCharacterSpeaker(chname);
			}
		}
	} 

	std::list<GretaLogger*>::iterator iterlog;

	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			log->debug(msg);
		if((*iterlog)->getLevel()=="info")
			log->info(msg);
		if((*iterlog)->getLevel()=="warn")
			log->warn(msg);
		if((*iterlog)->getLevel()=="error")
			log->error(msg);
	}
	listLog.clear();
}

void PlayerReceiver::audiostart(std::string id) throw(CMSException)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = writeFeedback("start", "audio", id, meta.getTime());
	callbackAudioSender->sendXML(doc, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, id, audiocreationID);
	std::ostringstream outlog;
	outlog << id <<" started playing "<<(meta.getTime()- audiocreationID)<<" ms after creation";
	std::string contentLog = outlog.str();
	log->debug(contentLog);
	//std::cout << "start  " << id << "\n";
	itercall = itercall + 1;
}

void PlayerReceiver::bapstart(std::string id) throw(CMSException)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = writeFeedback("start", "BAP", id, meta.getTime());
	//std::cout << "start  " << id << "\n";
	callbackBAPSender->sendXML(doc, meta.getTime(),SEMAINE_CMS_EVENT_SINGLE, id, bapcreationID); 
	itercall = itercall + 1;
}

void PlayerReceiver::fapstart(std::string id) throw(CMSException)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = writeFeedback("start", "FAP", id, meta.getTime());
	//std::cout << "start  " << id << "\n";
	callbackFAPSender->sendXML(doc, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, id, fapcreationID); 
	itercall = itercall + 1;
}

void PlayerReceiver::audioEnded(std::string id) throw(CMSException)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = writeFeedback("end", "audio", id, 0);
	//std::cout << "end  " << id << "\n";
	callbackAudioSender->sendXML(doc, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, id, audiocreationID); 
	itercall = itercall + 1;
}

void PlayerReceiver::fapEnded(std::string id) throw(CMSException)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = writeFeedback("end", "FAP", id, 0);
	//std::cout << "end  " << id  << "\n";
	callbackFAPSender->sendXML(doc, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, id, fapcreationID); 
	itercall = itercall + 1;
}

void PlayerReceiver::bapEnded(std::string id) throw(CMSException)
{
	XERCES_CPP_NAMESPACE::DOMDocument * doc = writeFeedback("end", "BAP", id, 0);
	//std::cout << "end  " << id  << "\n";
	callbackBAPSender->sendXML(doc, meta.getTime(), SEMAINE_CMS_EVENT_SINGLE, id, bapcreationID); 
	itercall = itercall + 1;
}

XERCES_CPP_NAMESPACE::DOMDocument* PlayerReceiver::writeFeedback(std::string type, std::string data, std::string id, int timer)
{
	std::string time;
	std::ostringstream oss;
	if(timer == 0) oss << meta.getTime();
	else oss << timer;
	time = oss.str();

	if(id == "") id = "unknown";
	XERCES_CPP_NAMESPACE::DOMDocument * doc = XMLTool::newDocument("callback", "http://www.semaine-project.eu/semaineml");
	DOMElement * root = doc->getDocumentElement();
	DOMElement * callback = XMLTool::appendChildElement(root, SemaineML.E_EVENT , "http://www.semaine-project.eu/semaineml");
	XMLTool::setAttribute(callback, "type", type);
	XMLTool::setAttribute(callback, "data",  data);
	XMLTool::setAttribute(callback, "id", id);
	XMLTool::setAttribute(callback, "time", time);

	return(doc);
}


} // namespace greta
} // namespace components
} // namespace semaine

