//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// 
//
//////////////////////////////////////////////////////////////////////

// #define RDEBUG

#include ".\FML-APML_AgentEngine.h"

#include <math.h>
#include "BML_AgentEngine.h"
#include "GretaXMLParser.h"
#include "NewConstraintsContainer.h"
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;

extern IniManager inimanager;
extern std::string ini_filename;
extern FileNames filenames;


extern FILE* gesture_log;
extern FILE* face_log;
extern RandomGen *randomgen;

extern DataContainer *datacontainer;

//#define OUTPUT

FMLAPML_AgentEngine::FMLAPML_AgentEngine(void)
{
	namespaceFML = "";
	namespaceBML = "";
	if(!inimanager.initialized)
		inimanager.ReadIniFile(ini_filename);

	if(randomgen==0)
		randomgen = new RandomGen();

	apml=0;
	commacts = std::vector<MMSystemSpace::CommunicativeIntention>() ;

	//logfile=fopen("logs/fml-apml_engine_log.html","w");

#ifdef OUTPUT
	fprintf(logfile,"<html>\n<head>\n</head>\n<body>\n");
#endif

	//moved from the execute
	//bqc=MMSystemSpace::BehaviorQualityComputation("mmsystem/behaviorqualifiers.xml");	

	bqc=MMSystemSpace::BehaviorQualityComputation(inimanager.GetValueString("BEHAVIORQUALIFIERS_FILE"));	
	mss=MMSystemSpace::MultimodalSignalSelection(inimanager.GetValueString("LEXICON_FILE"));


	// if PAD ALLOWED else null
	if(inimanager.GetValueInt("PAD_ALLOWED")==1)									
		padmapping1 = datacontainer->getPADMapping();
	else padmapping1 = NULL;

}

FMLAPML_AgentEngine::~FMLAPML_AgentEngine(void)
{
#ifdef OUTPUT
	fprintf(logfile,"</body>\n</html>\n");
#endif

	//fclose(logfile);

	if(apml!=0)
		delete apml;
	if(randomgen!=0)
	{
		delete randomgen;
		randomgen=0;
	}
}

int FMLAPML_AgentEngine::loadBaseline(std::string baselinefilename,char *buffer)
{
	if (baselinefilename!="")
	{
		bl.Load(baselinefilename,"");
		return 1;
	}
	else
	{
		bl.Load("",buffer);
		return 1;
	}
}
int FMLAPML_AgentEngine::loadBQC(std::string bqcfilename,char *buffer)
{
	if (bqcfilename!="")
	{
		bqc.LoadQualifiersList(bqcfilename,"");
		return 1;
	}
	else
	{
		bqc.LoadQualifiersList("",buffer);		
		return 1;
	}
	//	return 0;
}

int FMLAPML_AgentEngine::loadMMS(std::string mmsfilename,char *buffer)
{
	if (mmsfilename!="")
	{
		mss.LoadSelectionLexicon(mmsfilename,"", &mss.BehaviorSets);
		return 1;
	}
	else
	{
		mss.LoadSelectionLexicon("",buffer,&mss.BehaviorSets);
		return 1;	
	}
	//	return 0;
}

int FMLAPML_AgentEngine::LoadFMLAPML(std::string FMLAPMLfilename,char *buffer)
{
	//XMLDOMParser 
	XMLDOMParser *p;
	std::list<XMLGenericTree*>::iterator iterch;
	std::list<XMLGenericTree*>::iterator iterbl;

	filenames.BuildFileNames(FMLAPMLfilename);

	//To check the stack:
	//int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );
	//tmpFlag |= _CRTDBG_CHECK_ALWAYS_DF;
	//_CrtSetDbgFlag( tmpFlag );
	//char *m;
	//m=(char*)malloc(sizeof(char));

	p=new XMLDOMParser(); //XMLDOMParser();

	p->SetValidating(true);
	if(FMLAPMLfilename!="")
		apml=p->ParseFile(FMLAPMLfilename);
	else
		apml=p->ParseBuffer(buffer);
	//std::cout << apml->ToString();

	if(apml != NULL)
	{
		// Recording of the speech parameters
		XMLGenericTree * speechSubtree;
		XMLGenericTree * BCSubtree;
		// Search by local name and namespace if we can
		//XMLDOMTree * domTree = dynamic_cast<XMLDOMTree *>(apml);
		XMLDOMTree * domTree = (XMLDOMTree *)apml;
		if (domTree != NULL) {
			speechSubtree = domTree->FindNodeCalled("speech", namespaceBML);
			BCSubtree = domTree->FindNodeCalled("fml", namespaceFML);
		} else { // unused
			speechSubtree = apml->FindNodeCalled("speech");
			BCSubtree = apml->FindNodeCalled("fml");
		}
		if(speechSubtree!=0)
		{
			if(inimanager.GetValueString("TEXT_TO_SPEECH")=="activemary") speech.usingMary=0;
			if(inimanager.GetValueString("TEXT_TO_SPEECH")=="openmary") speech.usingMary=1;
			//if(speech.voice=="activemary") speech.usingMary=0;
			//if(speech.voice=="openmary") speech.usingMary=1;

			speech.StoreBML(speechSubtree,inimanager);

			speech.TemporizeTimeMarkers();

			speech.concretized=true;

			StoreCommunicativeIntentions(speechSubtree,"pitchaccent");
			StoreCommunicativeIntentions(speechSubtree,"boundary");


		}
		//BCSubtree = apml->FindNodeCalled("fml");
		//StoreCommunicativeIntentions(apml->FindNodeCalled("fml"));
		if(BCSubtree!=0)
		{
			StoreCommunicativeIntentions(BCSubtree);
		}
		//deactivate this


	}
	else
	{
		printf("Error parsing %s\n", FMLAPMLfilename.c_str());
		return 0;
	}

	delete apml;

	apml=0;

	//add a flag - do not use it if it is not necessary
	if(inimanager.GetValueInt("PAD_ALLOWED")==1) PADMap();

	return 1;
}

void FMLAPML_AgentEngine::StoreCommunicativeIntentions(XMLGenericTree *t,std::string tagsname)
{
	XMLGenericTree * chld;

	if(t==0)
		return;

	//for(iter=t->child.begin();iter!=t->child.end();iter++)
	for(XMLGenericTree::iterator iter = t->begin();iter != t->end(); ++iter)
	{
		chld = *iter;
		if (chld->isTextNode()) continue;
		if(chld->GetName()==tagsname)
		{
			MMSystemSpace::CommunicativeIntention ca;
			ca.id=chld->GetAttribute("id");

			ca.name=chld->GetName();

			if(ca.name=="emotion") 
			{
				ca.isemotion=true;
				ca.regulation=chld->GetAttribute("regulation");
			}

			if(ca.name=="emphasis")
				ca.isemphasis=true;

			if(ca.name=="pitchaccent")
				ca.ispitchaccent=true;

			ca.type=chld->GetAttribute("type");

			ca.start_sym=chld->GetAttribute("start");
/*
			if(chld->HasAttribute("end"))
				ca.dur_sym=chld->GetAttribute("end");
			else
				ca.dur_sym="0.3";*/
			
			ca.dur_sym=chld->GetAttribute("end");

			if(chld->HasAttribute("importance"))
				ca.importance=chld->GetAttributef("importance");

			//radek
			if(chld->HasAttribute("intensity"))
				ca.SetIntensity(atof(chld->GetAttribute("intensity").c_str()));

			if(chld->HasAttribute("target"))
				ca.target=chld->GetAttribute("target");
			commacts.push_back(ca);
		}
	}
}



void FMLAPML_AgentEngine::PADMap(){

	std::vector<MMSystemSpace::CommunicativeIntention> additionalcommacts;

	additionalcommacts = std::vector<MMSystemSpace::CommunicativeIntention>() ;

	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;

	for(iter=commacts.begin();iter!=commacts.end();iter++)
	{
		if (iter->isemotion==true) 
		{

			int tempp = iter->type.find("P=");
			int tempa = iter->type.find("A=");
			int tempd = iter->type.find("D=");

			if ( 
				(tempp>=0)
				&&
				(tempa>0)
				&&
				(tempd>0)
				)
			{
				float pvalue=0;
				float avalue=0;
				float dvalue=0;

				int templ = iter->type.find("P=");
				int tempr = iter->type.find("A=");

				std::string temp = iter->type.substr(templ+2,tempr-templ);
				pvalue=atof(temp.c_str());

				templ = iter->type.find("A=");
				tempr = iter->type.find("D=");

				temp = iter->type.substr(templ+2,tempr-templ);
				avalue=atof(temp.c_str());


				templ = iter->type.find("D=");

				temp = iter->type.substr(templ+2,iter->type.length());
				dvalue=atof(temp.c_str());

				if (pvalue>1.0) {
					printf("\n WARNING: P value of one of emotions is bigger than 1. I change to 1");
					pvalue=1.0;
				}
				if (avalue>1.0) {
					printf("\n WARNING: A-value of one of emotions is bigger than 1. I change to 1");
					avalue=1.0;
				}
				if (dvalue>1.0) {
					printf("\n WARNING: D-value of one of emotions is bigger than 1. I change to 1");
					dvalue=1.0;
				}

				if (pvalue<-1.0) {
					printf("\n WARNING: P-value of one of emotions is lower than -1. I change to -1");
					pvalue=-1.0;
				}
				if (avalue<-1.0) {
					printf("\n WARNING: A-value of one of emotions is lower than -1. I change to -1");
					avalue=-1.0;
				}
				if (dvalue<-1.0) {
					printf("\n WARNING: D-value of one of emotions is lower than -1. I change to -1");
					dvalue=-1.0;
				}

				#ifdef RDEBUG
					printf("\nP %f, A %f, D %f ", pvalue, avalue, dvalue);
				#endif

				//do the mapping

				//default value
				iter->type="unknown";

				std::map<std::string,PADElement*>::const_iterator iter2;

				PADElement* one=NULL;

				PADElement* two=NULL;

				for(iter2=padmapping1->padtable.begin();iter2!=padmapping1->padtable.end();iter2++)
				{
					float el_pvalue=iter2->second->pvalue;

					float el_avalue=iter2->second->avalue;

					float el_dvalue=iter2->second->dvalue;

					float el_xd=iter2->second->xd;

					float el_yd=iter2->second->yd;

					float el_zd=iter2->second->zd;

					//if the point is inside the iter zone
					if (
						( el_pvalue-el_xd < pvalue) && ( el_pvalue + el_xd > pvalue) &&
						( el_avalue-el_yd < avalue) && ( el_avalue + el_yd > avalue) &&
						( el_dvalue-el_zd < dvalue) && ( el_dvalue + el_zd > dvalue) 
						)
					{

						//order is important

						// if it is sedcond element that contains this point
						if ((one!=NULL) && (two==NULL))
						{
							two  = padmapping1->padtable[iter2->first];							
						}

						// if it is first element that contanins this point							
						if (one==NULL) one= padmapping1->padtable[iter2->first];


					}//end if "inside the iter" zone

				}//end of for


				//3 cases possible: 0, 1 and more than one entry

				//zero
				if ((one==NULL) && (two==NULL))
				{
					//find the closest

					PADElement *padelement;

					float min_distance=9.9;

					for(iter2=padmapping1->padtable.begin();iter2!=padmapping1->padtable.end();iter2++)
					{

						float el_pvalue=iter2->second->pvalue;

						float el_avalue=iter2->second->avalue;

						float el_dvalue=iter2->second->dvalue;


						float temp_value = sqrt(
							(el_pvalue-pvalue)*(el_pvalue-pvalue) + (el_avalue-avalue)*(el_avalue-avalue) + (el_dvalue-dvalue)*(el_dvalue-dvalue)
							);

						if (temp_value < min_distance) {
							padelement = padmapping1->padtable[iter2->first] ;
							min_distance=temp_value;
						}

					}//end of for

					iter->type=padelement->label;
					iter->isemotion=true;

					#ifdef RDEBUG
					printf("\nThe emotion is: %s" ,iter->type.c_str());
					#endif

				} //end if 0 entry

				else 

				{// start : one entry				

					if ((one!=NULL) && (two==NULL))
					{ 
						// that is easy
						iter->type=one->label;
						iter->isemotion=true;

						#ifdef RDEBUG
						printf("\nThe emotion is: %s" ,iter->type.c_str());
						#endif

					} //end if one entry 

					else 

					{ //both are in				

						// to do : change to complex expression

						one = NULL;
						two = NULL;

						float min_distance=9.9;
						float second_min_distance=9.99;

						for(iter2=padmapping1->padtable.begin();iter2!=padmapping1->padtable.end();iter2++)
						{
							float el_pvalue=iter2->second->pvalue;

							float el_avalue=iter2->second->avalue;

							float el_dvalue=iter2->second->dvalue;

							float el_xd=iter2->second->xd;

							float el_yd=iter2->second->yd;

							float el_zd=iter2->second->zd;

							//if the point is inside the iter zone
							if (
								( el_pvalue - el_xd < pvalue) && ( el_pvalue + el_xd > pvalue) &&
								( el_avalue - el_yd < avalue) && ( el_avalue + el_yd > avalue) &&
								( el_dvalue - el_zd < dvalue) && ( el_dvalue + el_zd > dvalue)
								)
							{

								float temp_value = sqrt(
									(el_pvalue-pvalue)*(el_pvalue-pvalue) + (el_avalue-avalue)*(el_avalue-avalue) + (el_dvalue-dvalue)*(el_dvalue-dvalue)
									);

								if (temp_value < min_distance) 
								{
									two = one;
									one = padmapping1->padtable[iter2->first] ;

									second_min_distance = min_distance;
									min_distance = temp_value;

								}

								if ( (temp_value > min_distance) && (temp_value < second_min_distance) )
								{
									two = padmapping1->padtable[iter2->first] ;
									second_min_distance=temp_value;
								}

							}//end of if

						}//end of for


						//	//display both!

						//	//IMPORTANT if there is PAD emotion it should not be overlaped
						//	//beacouse one PAD covers all the space

						//to do: change to multimodal complex expressions


						iter->type="complex:"+one->name_for_fl+"_and_"+two->name_for_fl;

						#ifdef RDEBUG
						printf("\nThe emotion is: %s" ,iter->type.c_str());
						#endif

						//	//the first is the same
						//	iter->type=one->label;
						//  iter->isemotion=true;

						//	//second is added at the end 

						//	MMSystemSpace::CommunicativeIntention * temp_ca = iter->clone();

						//  temp_ca->start =iter->start;
						//	temp_ca->duration = iter->duration;
						//	temp_ca->start_sym = iter->start_sym;
						//	temp_ca->dur_sym = iter->dur_sym;
						//	temp_ca->regulation = "felt";
						//	temp_ca->name = "emotion";																					
						//	temp_ca->type = two->label;
						//	temp_ca->isemotion=true;

						//	it is unique name
						//	temp_ca->id =temp_ca->type+temp_ca->id;
						//	additionalcommacts.push_back(*temp_ca);

					}//end of third condition												

			}//end of else

		}//END OF IF it's PAD notation

	}//END OF IF it's emotions

}//END OF FOR all emotion..

//add aditional commacts

std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter3;

//add complex facial expressions
for(iter3=additionalcommacts.begin();iter3!=additionalcommacts.end();iter3++)
{

	commacts.push_back(*(iter3->clone()));
}

//resort
std::sort(commacts.begin(),commacts.end());

}// END OF METHOD	



//this function should be rewrited for complex emotions alg


void FMLAPML_AgentEngine::ReviewEmotionalCommunicativeIntentions(){

	//suppose there is timeorder

	//suppose there it is concretized


	//std::vector<MMSystemSpace::CommunicativeIntention> commacts;

	std::vector<MMSystemSpace::CommunicativeIntention> additionalcommacts;
	additionalcommacts = std::vector<MMSystemSpace::CommunicativeIntention>() ;


	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;


	for(iter=commacts.begin();iter!=commacts.end();iter++)
	{
		if (iter->isemotion==true) 
		{


			//update commacts

			std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter2;

			for(iter2=commacts.begin();iter2!=commacts.end();iter2++)
			{

				if ( (iter2->isemotion==true) && (iter->type.compare(iter2->type) ) )
				{

					MMSystemSpace::CommunicativeIntention *temp;
					temp = NULL;


					//left include
					if ( 
						(iter->start <  iter2->start) 
						&&
						(iter->start + iter->duration  > iter2->start) 
						&&
						(iter->start + iter->duration  < iter2->start + iter2->duration )
						)
					{

#ifdef RDEBUG
						printf("\n 1st - start: %f, end %f ",iter->start, iter->start + iter->duration);
						printf("\n 2sd  - start: %f, end %f ",iter2->start, iter2->start + iter2->duration);
#endif

						float new_entry_start = iter2->start;
						float new_entry_duration = iter->start + iter->duration - new_entry_start;


						float tymczas = iter2->start + iter2->duration;

						//iter->start no change 

						iter->duration = iter2->start - iter->start;						

						iter2->start = new_entry_start + new_entry_duration;

						iter2->duration = tymczas - iter2->start;

						//<reference>affect=complex:sadness_and_anger</reference>
						//<reference>affect=complex:sadness_maskedby_joy</reference>
						//<reference>affect=complex:sadness_maskedby_neutral</reference>
						//<reference>affect=complex:neutral_maskedby_sadness</reference>
						//<reference>affect=complex:sadness_exaggerated</reference>
						//<reference>affect=complex:sadness_attenuated</reference>

#ifdef RDEBUG				
						printf("\n 1st  - start: %f, end %f ", iter->start, iter->start + iter->duration);
						printf("\n complex  - start: %f, end %f ", new_entry_start, new_entry_start + new_entry_duration);
						printf("\n 2nd  - start: %f, end %f ", iter2->start, iter2->start + iter2->duration);
#endif

						if ( ( iter->regulation.compare("felt")==0 ) &&  ( iter2->regulation.compare("felt")==0 ) )
						{

							// generate a new communicative intention

							temp = new CommunicativeIntention();

							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="superposed";

							temp->name="emotion";	

							std::string first_emotion_name = iter->type;
							std::string second_emotion_name = iter2->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_and_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_and_" + second_emotion_name + new_entry_start_string;


						}

						if ( (iter->regulation.compare("felt")==0 ) &&  (iter2->regulation.compare("fake") ==0 ) )
						{

							temp = new CommunicativeIntention();

							temp->name="emotion";	

							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="masked";

							std::string first_emotion_name = iter->type;
							std::string second_emotion_name = iter2->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_maskedby_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_maskedby_" + second_emotion_name + new_entry_start_string;


						}
						if ( (iter->regulation.compare("fake") ==0 ) &&  (iter2->regulation.compare("felt") ==0 ) )
						{

							temp = new CommunicativeIntention();

							temp->name="emotion";	

							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="masked";

							std::string first_emotion_name = iter2->type;
							std::string second_emotion_name = iter->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_maskedby_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_maskedby_" + second_emotion_name + new_entry_start_string;

						}

						if ( (iter->regulation.compare("fake") ==0 ) &&  (iter2->regulation.compare("fake") ==0 ) )
						{

							temp = new CommunicativeIntention();
							temp->name="emotion";	
							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="masked";

							std::string first_emotion_name = iter->type;
							std::string second_emotion_name = iter2->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_maskedby_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_maskedby_" + second_emotion_name + new_entry_start_string;

							printf("\n Warning: two fake emotions are overlaped\n");

						}

						//othercases


					}//end of left include


					//complete include
					if ( 
						( iter->start <  iter2->start ) 
						&&
						( iter->start + iter->duration  > iter2->start + iter2->duration )
						)
					{

						//3 expressions
						// 1. first - shorten the first emotion
						// 2. complex  - Add at the end the complex one
						// 3. first - SHORTEN THE SOCOND AND CHANGE THE NAME

						float new_entry_start = iter2->start;
						float new_entry_duration =   iter2->duration;

						float tymczas = iter->start + iter->duration;

						//iter->start - nonchanged

						iter->duration = new_entry_start - iter->start;						

						iter2->start = iter2->start + iter2->duration;

						iter2->duration = tymczas - iter2->start ;

						if ( ( iter->regulation.compare("felt")==0 ) &&  ( iter2->regulation.compare("felt")==0 ) )
						{

							// generate a new communicative intention

							temp = new CommunicativeIntention();
							temp->name="emotion";	
							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="superposed";

							std::string first_emotion_name = iter->type;
							std::string second_emotion_name = iter2->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_and_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_and_" + second_emotion_name + new_entry_start_string;


						}

						if ( (iter->regulation.compare("felt")==0 ) &&  (iter2->regulation.compare("fake") ==0 ) )
						{

							temp = new CommunicativeIntention();
							temp->name="emotion";	
							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="masked";

							std::string first_emotion_name = iter->type;
							std::string second_emotion_name = iter2->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_maskedby_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_maskedby_" + second_emotion_name + new_entry_start_string;


						}
						if ( (iter->regulation.compare("fake") ==0 ) &&  (iter2->regulation.compare("felt") ==0 ) )
						{

							temp = new CommunicativeIntention();
							temp->name="emotion";	
							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="masked";

							std::string first_emotion_name = iter2->type;
							std::string second_emotion_name = iter->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_maskedby_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_maskedby_" + second_emotion_name + new_entry_start_string;

						}

						if ( (iter->regulation.compare("fake") ==0 ) &&  (iter2->regulation.compare("fake") ==0 ) )
						{

							temp = new CommunicativeIntention();
							temp->name="emotion";	
							temp->start=new_entry_start;
							temp->duration=new_entry_duration;
							temp->regulation="masked";

							std::string first_emotion_name = iter->type;
							std::string second_emotion_name = iter2->type;

							char new_entry_start_string[100] ;

							sprintf(new_entry_start_string, "%G", new_entry_start);					

							temp->type="complex:"+first_emotion_name+"_maskedby_"+second_emotion_name;

							temp->id="complex:" + first_emotion_name +"_maskedby_" + second_emotion_name + new_entry_start_string;

							printf("\n Warning: two fake emotions are overlaped\n");

						}

						//othercases


					}//end of complete include


					//add element 

					if (temp!=NULL) additionalcommacts.push_back(*temp);					

				}//end of if

			}//end of for iter2

		}//end of if emotion

	}//end of for iter1


	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter3;

	//add complex facial expressions
	for(iter3=additionalcommacts.begin();iter3!=additionalcommacts.end();iter3++)
	{

		MMSystemSpace::CommunicativeIntention *temp = new CommunicativeIntention();

		temp->start=iter3->start;
		temp->duration=iter3->duration;
		temp->regulation=iter3->regulation;					
		temp->ref_type=iter3->ref_type;							
		temp->type=iter3->type;							
		temp->name=iter3->name;							
		temp->id=iter3->id;

		commacts.push_back(*temp);
	}

	//resort
	std::sort(commacts.begin(),commacts.end());

}


void FMLAPML_AgentEngine::StoreCommunicativeIntentions(XMLGenericTree *t)
{
	XMLGenericTree *chld;

	if(t==0)
		return;

	//for(iter=t->child.begin();iter!=t->child.end();iter++)
	for(XMLGenericTree::iterator iter = t->begin(); iter != t->end(); ++iter)
	{
		chld = *iter;
		//std::cout << chld->ToString();
		if (chld->isTextNode()) continue;
		MMSystemSpace::CommunicativeIntention ca;
		ca.id=chld->GetAttribute("id");
		ca.name=chld->GetName();

		if(ca.name=="emotion"){
			ca.isemotion=true;

			//check the attribute
			ca.regulation=chld->GetAttribute("regulation");
		}

		if(ca.name=="emphasis")
			ca.isemphasis=true;
		if(ca.name=="pitchaccent")
			ca.ispitchaccent=true;
		ca.type=chld->GetAttribute("type");
		if(ca.name=="world")
			ca.type=chld->GetAttribute("ref_type");
		ca.start_sym=chld->GetAttribute("start");
		ca.dur_sym=chld->GetAttribute("end");
		if(chld->HasAttribute("importance"))
			ca.importance=chld->GetAttributef("importance");

		//radek
		if(chld->HasAttribute("intensity"))
			ca.SetIntensity(atof(chld->GetAttribute("intensity").c_str()));


		if(chld->HasAttribute("target"))
			ca.target=chld->GetAttribute("target");
		ca.ref_type=chld->GetAttribute("ref_type");
		ca.ref_id=chld->GetAttribute("ref_id");
		ca.prop_type=chld->GetAttribute("prop_type");
		ca.prop_value=chld->GetAttribute("prop_value");
		commacts.push_back(ca);
		//std::cout<<" Communicative intention :"<<ca.name <<" This.start : "<< ca.start_sym<<" end : "<<ca.dur_sym << std::endl;

	}
}

int FMLAPML_AgentEngine::TemporizeCommunicativeIntentions(void)
{

	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;
	int loopcount;

	if(commacts.empty()==true)
		return 1;

	for(iter=commacts.begin();iter!=commacts.end();iter++)
	{
		loopcount=0;

		if((*iter).Temporize(commacts,&speech,&loopcount)==0)
			return 0;
		//std::cout<<" Communicative intention :"<<(*iter).name <<" This.start : "<< (*iter).start<<" end : "<<(*iter).duration <<" This.startTM : "<< (*iter).start_sym<<" endTM : "<<(*iter).dur_sym << std::endl;
	}

	return 1;
}

void FMLAPML_AgentEngine::ClearCommunicativeContexts(void)
{
	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;
	if(commacts.empty()==true)
		return;

	for(iter=commacts.begin();iter!=commacts.end();iter++)
		(*iter).CommunicativeContext.clear();	
	return;
}


void FMLAPML_AgentEngine::DefineCommunicativeContexts(void)
{
	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;
	if(commacts.empty()==true)
		return;

#ifdef OUTPUT
	fprintf(logfile,"<table border='1'>\n");
#endif

	for(iter=commacts.begin();iter!=commacts.end();iter++)
	{
		if((*iter).concretized==true)
			(*iter).DefineContext(commacts);

#ifdef OUTPUT
		fprintf(logfile,"<tr><td>\n");
		(*iter).Print(logfile);
		fprintf(logfile,"</tr></td>\n");
#endif

	}

#ifdef OUTPUT
	fprintf(logfile,"</table>\n");
#endif

}


void FMLAPML_AgentEngine::ReplaceCommunicativeContexts(std::string id, MMSystemSpace::MultimodalSignal *mmsignal)
{

	//cancel old context 	   
	ClearCommunicativeContexts();


	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter2;

	for(iter2=commacts.begin();iter2!=commacts.end();iter2++)
	{

		//if the element corresponds to id
		if((*iter2).id.compare(id)==0) 
		{

			//add new on the base of mmsignal

			std::vector<Signal>::iterator iter;

			if(mmsignal->empty()==true) break;

			for(iter=mmsignal->begin();iter!=mmsignal->end();iter++)
			{

				if((*iter).concretized==true)
				{
					MMSystemSpace::CommunicativeIntention * temp = new MMSystemSpace::CommunicativeIntention();

					temp->start=iter->start;
					temp->duration=iter->duration;
					temp->id=iter->id;
					temp->concretized=true;
					temp->instantiated=iter2->instantiated;
		

					//DEBUG IT
					temp->UsedModalities.push_back(iter->modality);

					//add this temp to the all commact contexts


					std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter3;

					for(iter3=commacts.begin();iter3!=commacts.end();iter3++)
					{
						//add : skip itself
						iter3->CommunicativeContext.push_back(temp);
					}
				}
			}//end for

			//mission completed
			return;

		}//end of if

	}//end of for

}


int FMLAPML_AgentEngine::reset()
{
	return 1;
}



//who use it????
std::string FMLAPML_AgentEngine::Execute(char *buffer)
{
	dl=MMSystemSpace::Dynamicline();	
	//apml = new XMLGenericTree();
	std::string bml;

	//XMLDOMParser
	XMLDOMParser *p;
	p=new XMLDOMParser(); //XMLDOMParser();
	p->SetValidating(true);
	apml=p->ParseBuffer(buffer);
	delete p;


	speech = MMSystemSpace::Signal();

	if(LoadFMLAPML("",buffer)==0)
		return "";


	if(TemporizeCommunicativeIntentions()==0)
		return "";

	std::sort(commacts.begin(),commacts.end());

	ReviewEmotionalCommunicativeIntentions();

	DefineCommunicativeContexts();

	bml="";

	int uid;

	uid=0;

	//here create domdocument and add recoursively the 

	bml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	bml=bml+"<!DOCTYPE bml SYSTEM \"bml.dtd\" []>\n";
	bml=bml+"<bml>\n";

	bml=bml+speech.GetBML(&uid);

	//TODO: check the following instruction for bugs
	//bl.SaveCSV(-1,preferencefile,csvgesturefile,csvtorsofile,csvheadfile,csvfacefile,csvgazefile);

	MMSystemSpace::CommunicativeIntention *contextact;

	std::vector<MMSystemSpace::MultimodalSignal*> *mmsignals;
	MMSystemSpace::MultimodalSignal *mmsignal;

	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;

	//for each commact
	for(iter=commacts.begin();iter!=commacts.end();iter++)
	{
		dl=bqc.ComputeDynamicline(&(*iter),bl);

		contextact=(*iter).GetActFromContext("emotion");

		if(contextact!=0)
			dl=bqc.ComputeDynamicline(contextact,dl);

		contextact=(*iter).GetActFromContext("emphasis");
		if(contextact!=0)
			dl=bqc.ComputeDynamicline(contextact,dl);

		contextact=(*iter).GetActFromContext("rheme");
		if(contextact!=0)
			dl=bqc.ComputeDynamicline(contextact,dl);

		//dl.SaveCSV((*iter).start,preferencefile,csvgesturefile,csvtorsofile,csvheadfile,csvfacefile,csvgazefile);

		//choose a list of available mms for a commact - the last parameter is a list of busymodalities

		if(inimanager.GetValueInt("MULTIMODAL_EMOTIONS")==1)
		{
			// in a case of MSE - everything is still possible
		
			//std::list<std::string> temp_list;
			std::string label = (*iter).name+"-"+(*iter).type;
			BehaviorSet s=mss.BehaviorSets[label];
			mmsignal=new MMSystemSpace::MultimodalSignal;
			
			mmsignals = new std::vector<MMSystemSpace::MultimodalSignal*>;

			//BehaviorSet s=mss.FromCAtoBehaviorSet(&dl);
						
			if (!(s.multimodalsignals.empty())) 
				(*mmsignal)=s.multimodalsignals[0];
			else mmsignal=NULL;
		

		}
		else
		{	
		//otherwise - standard procedure

			//this is a matirx of all combinations of signals - it may be huge
			mmsignals=mss.SelectMultimodalSignals((*iter),&dl,(*iter).GetContextUsedModalities());

			//chose one mms from the previous list 
			mmsignal=mss.SelectMultimodalSignal(mmsignals,(*iter),&dl);
	
		}

		if(mmsignal!=0)
		{

			//std::cout <<"."<< std::ends;	
			mmsignal->AssignNameToSignals((*iter).name);
			mmsignal->AddIntensity( (*iter).GetIntensity()   );
			mmsignal->AssignParameters(&dl);
			mmsignal->ShuffleShapes();
			mmsignal->AssignStrokesFromStressVector(&(*iter),&speech.stressingpoints);
			(*iter).UsedModalities=mmsignal->UsedModalities();

			if ((*iter).isemotion==true) 
			{

				//there is one mmsignal chosen with different modalities (all modalities)
				//time constraints should be considered and modalities should be ordered
				// if we want use two different signal on the same modality SelectMultimodalSignals should allow this before
				//thus we suppose we have a mmsignal that can contain different signals even for the same modality
				//now we can order them according to the time-constraints
				//each signal has its own start and end time
				//before using "addTimeConstraints" method all signals have the same start and end (see: temporize() )

				if(inimanager.GetValueInt("MULTIMODAL_EMOTIONS")==1)
				{
					mmsignal->addTimeConstraints((*iter).type.c_str(),0);

					//To DO : DEBUG it
					//it is changed becouse of new signals may be chosen					
					ReplaceCommunicativeContexts(iter->id, mmsignal);
				}

			}

			(*iter).instantiated=true;

			//each signal of a given multimodal signal is changed to bml separately

			//inviarlo su psyclone attaccando l'header
			bml=bml+mmsignal->GetBML(&uid);
		} 
		else {
			//means: 
			//	1) signal defined
			//  2) no free modality

			if ((mmsignals==NULL)||(mmsignals->empty()==true))
			{
				//only if iter->name == emotion ???
				//printf("I will try directly with the facial expression %s\n",(iter->name+"-"+iter->type).c_str());
				Signal *temp = new Signal();
				temp->duration=iter->duration;
				temp->start=iter->start;
				temp->id=iter->id;
				temp->type=iter->type;
				std::string aaa = "faceexp=";
				temp->reference=aaa+iter->type;
				temp->modality="face";
				temp->intensity=iter->GetIntensity();

				bml=bml+temp->GetBML(&uid);

			}
		}//end of else

	}//end of loop

	bml=bml+"</bml>\n";
	//std::cout <<"A calcule : "<< bml <<std::endl;
	FILE *f=fopen(filenames.FMLAPML_to_BML_File.c_str(),"w");
	//FILE *f=fopen((inimanager.Program_Path+"bml/from-fml-apml.xml").c_str(),"w");
	if(f!=0)
	{
		fprintf(f,"%s",bml.c_str());
		fclose(f);
	}


	//delete speech;	
	//delete bl;
	//delete dl;

	//commacts.erase();

	ClearCommunicativeContexts();
	commacts.clear();
	delete apml;

	return bml;
}

// call by greta Modular and greta realtime
std::string FMLAPML_AgentEngine::Execute(std::string FMLAPMLfilename,std::string baselinefilename,char *buffer, int realtime)
{
	speech.concretized=false;

	if(LoadFMLAPML(FMLAPMLfilename,buffer)==0)
	{
		ClearCommunicativeContexts();
		commacts.clear();
		speech.Clear();	
		dl.clear();
		bl.clear();
		delete apml;
		return "";
	}

	GretaLogger* l = new GretaLogger("Core_AgentEngine", "Start Fml-Apml", "debug"); 
	listLog.push_back(l);

	//	std::string bml;
	//speech.Clear();	
	//dl.clear();

	if(TemporizeCommunicativeIntentions()==0)
	{
		ClearCommunicativeContexts();
		commacts.clear();
		speech.Clear();	
		dl.clear();
		bl.clear();
		delete apml;
		return "";
	}

	std::sort(commacts.begin(),commacts.end());

	ReviewEmotionalCommunicativeIntentions();

	DefineCommunicativeContexts();

	this->loadBaseline(baselinefilename,"");


	//MOVED TO CONSTRUCTOR - TO BE VERIFIED!!!
	//bqc=MMSystemSpace::BehaviorQualityComputation("mmsystem/behaviorqualifiers.xml");	
	//mss=MMSystemSpace::MultimodalSignalSelection("mmsystem/"+inimanager.GetValueString("LEXICON_FILE"));

	//	bml="";

	int uid;

	uid=0;

	//	bml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	//	if (realtime==0)
	//		bml=bml+"<!DOCTYPE bml SYSTEM \"bml.dtd\" []>\n";
	//	else
	//		bml=bml+"<!DOCTYPE bml SYSTEM \"bml/bml.dtd\" []>\n";
	//	bml=bml+"<bml>\n";

	XMLDOMTree * bml = new XMLDOMTree("bml", "http://www.mindmakers.org/projects/BML");

	if(speech.concretized==true)
	{
		//bml=bml+speech.GetBML(&uid);
		speech.AddSubElement(bml, &uid);
	}

	//commentare questi file!!!
	/*	FILE *csvgesturefile;
	FILE *csvtorsofile;
	FILE *csvheadfile;
	FILE *csvfacefile;
	FILE *csvgazefile;
	FILE *preferencefile;
	csvgesturefile=fopen("logs/gesture.csv","w");
	csvtorsofile=fopen("logs/torso.csv","w");
	csvheadfile=fopen("logs/head.csv","w");
	csvfacefile=fopen("logs/face.csv","w");
	csvgazefile=fopen("logs/gaze.csv","w");
	preferencefile=fopen("logs/preference.csv","w");*/

	//TODO: check the following instruction for bugs
	//bl.SaveCSV(-1,preferencefile,csvgesturefile,csvtorsofile,csvheadfile,csvfacefile,csvgazefile);

	MMSystemSpace::CommunicativeIntention *contextact;

	std::vector<MMSystemSpace::MultimodalSignal*> *mmsignals;
	MMSystemSpace::MultimodalSignal *mmsignal;

	std::vector<MMSystemSpace::CommunicativeIntention>::iterator iter;

	//for each commact
	for(iter=commacts.begin();iter!=commacts.end();iter++)
	{
		dl=bqc.ComputeDynamicline(&(*iter),bl);

		contextact=(*iter).GetActFromContext("emotion");

		if(contextact!=0)
			dl=bqc.ComputeDynamicline(contextact,dl);

		contextact=(*iter).GetActFromContext("emphasis");
		if(contextact!=0)
			dl=bqc.ComputeDynamicline(contextact,dl);

		contextact=(*iter).GetActFromContext("rheme");
		if(contextact!=0)
			dl=bqc.ComputeDynamicline(contextact,dl);

		//dl.SaveCSV((*iter).start,preferencefile,csvgesturefile,csvtorsofile,csvheadfile,csvfacefile,csvgazefile);
		
		NewConstraintsContainer *constraintscontainer = datacontainer->getContraints();
		MultimodalEmotionConstraint *mec = constraintscontainer->getMultimodalEmotionConstraint((*iter).type.c_str());

		if((inimanager.GetValueInt("MULTIMODAL_EMOTIONS")==1) && (mec!=NULL))
		{
				// in a case of MSE - everything is still possible
		
			//std::list<std::string> temp_list;
			std::string label = (*iter).name+"-"+(*iter).type;
			BehaviorSet s=mss.BehaviorSets[label];
			mmsignal=new MMSystemSpace::MultimodalSignal;
			
			mmsignals = new std::vector<MMSystemSpace::MultimodalSignal*>;

			//BehaviorSet s=mss.FromCAtoBehaviorSet(&dl);
						
			if (!(s.multimodalsignals.empty())) 
				(*mmsignal)=s.multimodalsignals[0];
			else mmsignal=NULL;
		
			
		}
		else
		{	
		//otherwise - standard procedure

			mmsignals=mss.SelectMultimodalSignals((*iter),&dl,(*iter).GetContextUsedModalities());

			//chose one mms from the previous list 
			mmsignal=mss.SelectMultimodalSignal(mmsignals,(*iter),&dl);
		}
	

		if(mmsignal!=0)
		{
			mmsignal->AssignNameToSignals((*iter).name);
			mmsignal->AddIntensity( (*iter).GetIntensity()   );
			mmsignal->AssignParameters(&dl);
			mmsignal->ShuffleShapes();
			mmsignal->AssignStrokesFromStressVector(&(*iter),&speech.stressingpoints);
			//mmsignal->SaveCSV((*iter).start,preferencefile,csvgesturefile,csvtorsofile,csvheadfile,csvfacefile,csvgazefile);
			(*iter).UsedModalities=mmsignal->UsedModalities();
			(*iter).instantiated=true;

			if ((*iter).isemotion==true) 
			{

				//there is one mmsignal chosen with different modalities (all modalities)
				//time constraints should be considered and modalities should be ordered
				// if we want use two different signal on the same modality SelectMultimodalSignals should allow this before
				//thus we suppose we have a mmsignal that can contain different signals even for the same modality
				//now we can order them according to the time-constraints
				//each signal has its own start and end time
				//before using "addTimeConstraints" method all signals had the same start and end (see: temporize() )

				if(inimanager.GetValueInt("MULTIMODAL_EMOTIONS")==1)
				{
					mmsignal->addTimeConstraints((*iter).type.c_str(),0);

					//To DO : DEBUG it
					//it is changed becouse of new signals may be chosen
					//ClearCommunicativeContexts();
					ReplaceCommunicativeContexts(iter->id, mmsignal);
				}
			}

			//(*iter).instantiated=true;

#ifdef RDEBUG
			mmsignal->print();

#endif
			//each signal of a given multimodal signal is changed to bml separately
			//			bml=bml+mmsignal->GetBML(&uid);
			mmsignal->AddSubElements(bml, &uid);
		}

		else {
//		if  (mmsignal==0) and (iter type non ce in behavior set){
			//means: 
			//	1) signal defined
			//  2) no free modality

			if ((mmsignals==NULL) && iter->type != "")
				//if ((mmsignals==NULL)||(mmsignals->empty()==true))
			{
				//only if iter->name == emotion ???

#ifdef RDEBUG 					
				printf("\n I will try with the facial expressions for comm. int. %s\n",(iter->name+"-"+iter->type).c_str());
#endif
				//MAURIZIO 24-11-2009 (aggiunto if):
				if(inimanager.GetValueInt("MULTIMODAL_EMOTIONS")==1)
				{
					Signal *temp = new Signal();
					temp->duration=iter->duration;
					temp->start=iter->start;
					temp->id=iter->id;
					temp->type=iter->type;
					std::string aaa = "faceexp=";
					temp->reference=aaa+iter->type;
					temp->modality="face";
					temp->intensity=iter->GetIntensity();

					//			bml=bml+temp->GetBML(&uid);
					temp->AddSubElement(bml, &uid);
				}
			}

		}//end of else



	}

	/*fclose(csvgesturefile);
	fclose(csvtorsofile);
	fclose(csvheadfile);
	fclose(csvfacefile);
	fclose(csvgazefile);
	fclose(preferencefile);*/

	//	bml=bml+"</bml>\n";

#ifdef RDEBUG 					
	FILE *g=fopen("c:\\ss.xml","w");
#endif

	//dove la stringa viene creata
	std::string bmlString = bml->ToString();

	if(realtime==1)
	{
		ClearCommunicativeContexts();
		commacts.clear();
		speech.Clear();	
		bl.clear();
		dl.clear();		
		delete apml;

		return bmlString;

	}

	#ifdef RDEBUG 					

	if(g!=0)
	{
		fprintf(g,"%s",bmlString.c_str());
		fclose(g);
	}

	#endif

	FILE *f=fopen(filenames.FMLAPML_to_BML_File.c_str(),"w");

	//FILE *f=fopen((inimanager.Program_Path+"bml/from-fml-apml.xml").c_str(),"w");

	if(f!=0)
	{
		fprintf(f,"%s",bmlString.c_str());
		fclose(f);
	}

	//THIS VARIABLE HAS TO INSTANTIED HERE
	//IT IS USED ONLY BY GRETA MODULAR
	//IT SI NOT USED BY GRETA REALTIME
	BML_AgentEngine bae; //IT TAKE 0.3 seconds to inistanciate this variable

	//if(bae.Execute(inimanager.Program_Path+"bml/from-fml-apml.xml",0,1)==0)

	if(bae.Execute(filenames.FMLAPML_to_BML_File,0,1)==0)
	{
		printf("FMLAPML Engine:: Error running the BML engine\n");
		return "";
	}


	ClearCommunicativeContexts();
	commacts.clear();
	speech.Clear();	
	bl.clear();
	dl.clear();
	delete apml;


	return bmlString;
}

