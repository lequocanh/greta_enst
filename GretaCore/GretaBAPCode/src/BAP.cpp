//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BAP.cpp: implementation of the BAP class.
//
//////////////////////////////////////////////////////////////////////

//include "stdafx.h"


#include "BAP.h"





//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BAP::BAP(BAPType btype)
{
	mBAPType	= btype;
	mMask		= false;
	mBAPValue	= 0;
}

BAP::~BAP()
{

}

// default: return angle in radians

float BAP::GetAngleValue(AngleType t)
{
	if (t == degrees)
		return float(RTOD*(float(mBAPValue)/100000.0));
	else 
		return (float(mBAPValue)/100000.0f);
}

void BAP::SetAngleValue(AngleType a, float f)
{
	if (a == degrees)
		mBAPValue = int((DTOR*f)*100000.0);
	else 
		mBAPValue = int(f*100000.0);
}

