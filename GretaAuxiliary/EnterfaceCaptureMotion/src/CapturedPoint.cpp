
#include "CapturedPoint.h"


CapturedPoint::CapturedPoint(){
this->x=0.0;
this->y=0.0;
this->z=0.0;
this->label="";
}

CapturedPoint::CapturedPoint(std::string label){
	this->x=0.0;
	this->y=0.0;
	this->z=0.0;
	this->label="";
}

CapturedPoint::CapturedPoint(float x,float y,float z, std::string label){
	this->x=x;
	this->y=y;
	this->z=z;
	this->label=label;
}

CapturedPoint::CapturedPoint(float x,float y,float z ){
	this->x=x;
	this->y=y;
	this->z=z;
	this->label="";
}


