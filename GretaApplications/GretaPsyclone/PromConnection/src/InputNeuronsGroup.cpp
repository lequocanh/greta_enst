#include "InputNeuronsGroup.h"
#include <stdlib.h>
#include <iostream>

InputNeuronsGroup::InputNeuronsGroup(void)
{
	int i;
	inputTime=0;
	NumOfNeurons=0;
	neuronsList = (float*) malloc (sizeof(float));
	neuronsList[0]=0;
}

InputNeuronsGroup::~InputNeuronsGroup(void)
{
}

void InputNeuronsGroup::iniGroup(int numOfNeurons)
{
	NumOfNeurons=numOfNeurons;
	neuronsList = (float*) malloc (this->NumOfNeurons*sizeof(float));
	for(int i=0; i<this->NumOfNeurons; i++){
		this->neuronsList[i]=0;
	}
	inputTime=0;

}
void InputNeuronsGroup::updateGroup()
{
}

float InputNeuronsGroup::getActivation(int numero)
{
	return(neuronsList[numero]);
}

int InputNeuronsGroup::getNumOfNeurons(void)
{
	return this->NumOfNeurons;
}

int InputNeuronsGroup::getInputTime(void)
{
	return this->inputTime;
}

void InputNeuronsGroup::setInputTime(int InputTime)
{
	this->inputTime=InputTime;
}