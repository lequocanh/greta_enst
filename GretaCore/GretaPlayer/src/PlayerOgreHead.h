#ifndef PLAYEROGREHEAD_H
#define PLAYEROGREHEAD_H
#include <ogre.h>
#include "FAPFrame.h"
#include <stdarg.h>
#define PI_ 3.14159265
#define JAWSVERTICALMOVEMENT 0.006553

class PlayerOgreHead{
public:
	PlayerOgreHead(Ogre::SceneManager *sceneManager, Ogre::SceneNode *parent, Ogre::String name, Ogre::String faceMeshFileName, Ogre::String hairMeshFileName);
	~PlayerOgreHead();
	
	void linkToBone(Ogre::Bone *skull_,Ogre::Vector3 offset){headNode->setPosition(offset);skull=skull_; updateSkullLink();}
	/** set the position and orientation of the body's skull to the head */
	void updateSkullLink();

	void SwitchEyes(); // show/hide eyes, teeth, tongue and jaw
	void SwitchHair(); // show/hide hair
	void SwitchSkin(); // show/hide the face

	/** creates vertex pose animations for each FAP */
	void createFAPPoses(std::string fdpFileName, std::string indicesFileName,Ogre::SceneManager* sceneManager);

	void setFaceMaterial(Ogre::String MaterialName){face->setMaterialName(MaterialName);} //set material to the face
	void setEyesMaterial(Ogre::String MaterialName){l_eye->setMaterialName(MaterialName);r_eye->setMaterialName(MaterialName);} //set material to both eyes

	void scale(double factor){headPartsNode->scale(factor,factor,factor); }//MNS /= 2.0*factor;} //scales all the head
	void scaleEyes(double factor){l_EyeNode->scale(factor,factor,factor);r_EyeNode->scale(factor,factor,factor);} //scales left and right eyes
	void scaleHair(double factorx,double factory,double factorz){hairNode->scale(factorx,factory,factorz);} //scales hair
	void scaleJaw(double factor){jawNode->scale(factor,factor,factor);}// MNS /= factor;} //scales jaw

	void setScale(double factor){headPartsNode->setScale(factor,factor,factor); }
	void setScaleEyes(double factor){l_EyeNode->setScale(factor,factor,factor);r_EyeNode->setScale(factor,factor,factor);} 
	void setScaleHair(double factorx,double factory,double factorz){hairNode->setScale(factorx,factory,factorz);} 
	void setScaleJaw(double factor){jawNode->setScale(factor,factor,factor);}

	double getScale(){return headPartsNode->getScale().x;}
	double getScaleEyes(){return l_EyeNode->getScale().x;}
	double getScaleHair_X(){return hairNode->getScale().x;}
	double getScaleHair_Y(){return hairNode->getScale().y;}
	double getScaleHair_Z(){return hairNode->getScale().z;}
	double getScaleJaw(){return jawNode->getScale().x;}
	
	void setPosition(double x,double y,double z){headPartsNode->setPosition(x,y,z);} //set the position of head
	void setFacePosition(double x,double y,double z){faceNode->setPosition(x,y,z);}
	void setLeftEyePosition(double x,double y,double z){l_EyeNode->setPosition(x,y,z);} //set the position of the left eye
	void setRightEyePosition(double x,double y,double z){r_EyeNode->setPosition(x,y,z);} //set the position of the right eye
	void setJawPosition(double x,double y,double z){jawNode->setPosition(x,y,z);} //set the position of jaw
	void setHairPosition(double x,double y,double z){hairNode->setPosition(x,y,z);} //set the position of hair

	double getPosition_X(){return headPartsNode->getPosition().x;}
	double getPosition_Y(){return headPartsNode->getPosition().y;}
	double getPosition_Z(){return headPartsNode->getPosition().z;}

	double getLeftEyePosition_X(){return l_EyeNode->getPosition().x;}
	double getLeftEyePosition_Y(){return l_EyeNode->getPosition().y;}
	double getLeftEyePosition_Z(){return l_EyeNode->getPosition().z;}
	
	double getRightEyePosition_X(){return r_EyeNode->getPosition().x;}
	double getRightEyePosition_Y(){return r_EyeNode->getPosition().y;}
	double getRightEyePosition_Z(){return r_EyeNode->getPosition().z;}

	double getJawPosition_X(){return jawNode->getPosition().x;}
	double getJawPosition_Y(){return jawNode->getPosition().y;}
	double getJawPosition_Z(){return jawNode->getPosition().z;}

	double getHairPosition_X(){return hairNode->getPosition().x;}
	double getHairPosition_Y(){return hairNode->getPosition().y;}
	double getHairPosition_Z(){return hairNode->getPosition().z;}

	void pitchHair(double degree){hairNode->pitch(Ogre::Degree(degree));} 
	
	void addAccessory(Ogre::SceneManager *sceneManager,std::string meshName,std::string entityName, double x, double y, double z, double factor);

	Ogre::SceneNode *getRigthEyeNode(){return r_EyeNode;} //used to attach a camera to this eye
	Ogre::Vector3 *getFaceCenter();
	double getFaceHight();
	double getFaceDeep();
	/** applies FAP values to poses */
	void applyFAP(FAPFrame* fap);

private:
	Ogre::VertexPoseKeyFrame* manualKeyFrame;
	Ogre::AnimationState* manualAnimState;
	Ogre::SceneNode *headNode; //node of the head
	Ogre::SceneNode *rotationNode; //node to apply rotations
	Ogre::SceneNode *headPartsNode; //node containing all parts of the head

	Ogre::SceneNode *hairNode;
	Ogre::Entity *hair; //mesh of hair
	bool showHair; //flag of showing hair

	Ogre::SceneNode *faceNode;
	Ogre::Entity *face; //mesh of the face
	bool showFace; //flag of showing face skin
	Ogre::String faceMeshFile;
	Ogre::String faceEntityName;

	
	Ogre::SceneNode *jawNode;
	Ogre::Entity *sup_jaw;
	Ogre::Entity *glottis;
	Ogre::SceneNode *inf_jawNode;
	Ogre::Entity *inf_jaw;
	Ogre::Entity *tongue;
	Ogre::VertexPoseKeyFrame* tongueKeyFrame;
	Ogre::AnimationState* tongueAnimState;
	//double MNS; // used to displace jaw replaced by JAWSVERTICALMOVEMENT

	Ogre::SceneNode *l_EyeNode;
	Ogre::Entity *l_eye; //mesh of the left eye

	Ogre::SceneNode *r_EyeNode;
	Ogre::Entity *r_eye; //mesh of the right eye
	bool showEyes; //flag of showing eyes, teeth, tongue and jaw

	Ogre::Bone *skull; //pointer to the body's skull

	double FAPValues[69]; //to store the last values of FAPs
	int numberOfPoses;
	Ogre::Pose **poses; // array of poses
	int *indexOfPose; // array of indices of poses in the mesh
	int *FAPOfPoses; // array of FAPs corresponding to each pose

	//in the code of the old Greta player, some FAPs are applied under specific conditions.
	//they will be treated separately :
	Ogre::Pose* FAP_6; //not applied when FAP 53 != 0
	int index_Pose_FAP_6; //index of the pose in the mesh
	Ogre::Pose* FAP_7; //not applied when FAP 54 != 0
	int index_Pose_FAP_7; //index of the pose in the mesh
	Ogre::Pose* FAP_12_0;//not applied when FAP 59 != 0
	int index_Pose_FAP_12_0; //index of the pose in the mesh
	Ogre::Pose* FAP_12_1;//not applied when FAP 59 != 0 and when FAP 12 > 0;
	int index_Pose_FAP_12_1; //index of the pose in the mesh
	Ogre::Pose* FAP_13_0; //not applied when FAP 60 != 0
	int index_Pose_FAP_13_0; 
	Ogre::Pose* FAP_13_1; //not applied when FAP 60 != 0 and when FAP 13 > 0;
	int index_Pose_FAP_13_1;
	Ogre::Pose* FAP_17_; //applied with 3.5 when FAP 17 > 0
	int index_Pose_FAP_17_;
	Ogre::Pose* FAP_31_;//applied with absolute value
	int index_Pose_FAP_31_;
	Ogre::Pose* FAP_32_;//applied with absolute value
	int index_Pose_FAP_32_;
	Ogre::Pose* FAP_33_;//applied with absolute value
	int index_Pose_FAP_33_;
	Ogre::Pose* FAP_34_;//applied with absolute value
	int index_Pose_FAP_34_;
	Ogre::Pose* FAP_35_;//applied with absolute value
	int index_Pose_FAP_35_;
	Ogre::Pose* FAP_36_;//applied with absolute value
	int index_Pose_FAP_36_;
	//Ogre::Pose* FAP_31x37;//applied with fap 31 * fap 37
	//int index_Pose_FAP_31x37;
	//Ogre::Pose* FAP_32x38;//applied with fap 32 * fap 38
	//int index_Pose_FAP_32x38;
	Ogre::Pose* FAP_39_; //applied when FAP 39 > 0
	int index_Pose_FAP_39_;
	Ogre::Pose* FAP_40_; //applied when FAP 40 > 0
	int index_Pose_FAP_40_;
	Ogre::Pose* FAP_41_; //applied with -5 when FAP 41 > 0
	int index_Pose_FAP_41_;
	Ogre::Pose* FAP_42_; //applied with -5 when FAP 42 > 0
	int index_Pose_FAP_42_;
	Ogre::Pose* FAP_55_; //applied when FAP 55 < 0
	int index_Pose_FAP_55_;
	Ogre::Pose* FAP_56_; //applied when FAP 56 < 0
	int index_Pose_FAP_56_;
	Ogre::Pose* FAP_59_; //applied when FAP 59 > 0
	int index_Pose_FAP_59_;
	Ogre::Pose* FAP_60_; //applied when FAP 60 > 0
	int index_Pose_FAP_60_;


	//the folowing functions are used to create Ogre::Pose for the FAP animation
	void getVertices(std::vector<Ogre::Vector3> *vertices); // stores vertices coordinate of the face in the vector "vertices"

	static void readIndices(std::vector<std::vector<int>>* groups, std::string fileName);
	static void readFDPs(std::vector<std::vector<Ogre::Vector3>> *FDPs, std::vector<Ogre::Vector3> *vertices, Ogre::String fileName);
	static void clear(std::vector<bool> *v);
	static void clear(std::vector<bool> *v, std::vector<int> *indices);
	static void clear(std::vector<bool> *v, std::vector<std::vector<int>> *groups, int numberOfGroups,...);
	static void add(std::vector<bool> *v, std::vector<int> *indices);
	static void clearAndAdd(std::vector<bool> *v, std::vector<std::vector<int>> *groups, int numberOfGroups,...);

	enum axeType{X,Y,Z};
	void createPose2( Ogre::Pose *pose, std::vector<Ogre::Vector3> *geomety, std::vector<bool> *zones, Ogre::Vector3 *FDP, axeType axe, double x, double y, double z, double div, double coef);
	void createPose( Ogre::Pose *pose, std::vector<Ogre::Vector3> *geomety, std::vector<bool> *zones, Ogre::Vector3 *FDP, axeType axe, double x, double y, double z, double div, double coef, int power = 1,double pi_=PI_);
	void scalepose( Ogre::Pose *pose, std::vector<bool> *zones, axeType axe, double factor);
	void getPosesIndicesInMesh(Ogre::MeshPtr pMesh);
};
#endif
