//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorPlanner.cpp: implementation of the BehaviorPlanner class. 
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <time.h>
#include "UserSignals.h"

#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/message/SEMAINEStateMessage.h>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;

namespace semaine {
namespace components {
namespace greta {

UserSignals::UserSignals(const std::string & componentName, bool isInput, bool isOutput) throw(CMSException) :
	Component(componentName,isInput,isOutput)
{

	signalSender = new StateSender("semaine.data.state.user.behaviour", semaine::datatypes::stateinfo::StateInfo::Type::UserState, getName());
	senders.push_back(signalSender);
	difD = 0.0;
	waiting = 0;
	itt = 0;

	this->start();
}

UserSignals::~UserSignals()
{
	delete signalSender;
}


void UserSignals::act() throw(CMSException)
{
	if(waiting==0)
	{
		startD = clock();
		waiting = 1;
	}

	if(waiting==1) 
	{
		endD = clock();
		difD = endD - startD;
		difD = difD/CLOCKS_PER_SEC;
	}	

	if (difD >= 5) 
	{ 
		std::map<std::string,std::string> ciccio;
		if(itt == 0)
			ciccio["speaking"]= "false";
		if(itt == 1)
			ciccio["speaking"]= "true";
		if(itt == 2)
			ciccio["headGesture"]= "NOD";
		if(itt == 3)
			ciccio["headGesture"]= "SHAKE";
		itt = itt + 1;
		if(itt == 4)
			itt = 0;
		semaine::datatypes::stateinfo::UserStateInfo *state = new semaine::datatypes::stateinfo::UserStateInfo(ciccio);
		semaine::datatypes::stateinfo::StateInfo *stateinf = (semaine::datatypes::stateinfo::StateInfo*)state;
		signalSender->sendStateInfo(stateinf, this->meta.getTime());
		waiting = 0;
		difD = 0;
	}

	std::list<GretaLogger*>::iterator iterlog;

	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			log->debug(msg);
		if((*iterlog)->getLevel()=="info")
			log->info(msg);
		if((*iterlog)->getLevel()=="warn")
			log->warn(msg);
		if((*iterlog)->getLevel()=="error")
			log->error(msg);
	}
	listLog.clear();

}
	/*std::map<std::string,std::string> ciccio2;
	ciccio2["speaking"]= "true";
	semaine::datatypes::stateinfo::UserStateInfo *state2 = new semaine::datatypes::stateinfo::UserStateInfo(ciccio2);


	//signalSender->sendStateInfo(state2, this->meta.getTime());

	await(10, state2);

	std::map<std::string,std::string> ciccio3;
	ciccio3["headGesture"]= "NOD";
	semaine::datatypes::stateinfo::UserStateInfo *state3 = new semaine::datatypes::stateinfo::UserStateInfo(ciccio3);
	//signalSender->sendStateInfo(state3, this->meta.getTime());

	await(10, sta);

	std::map<std::string,std::string> ciccio4;
	ciccio4["headGesture"]= "SHAKE";
	semaine::datatypes::stateinfo::UserStateInfo *state4 = new semaine::datatypes::stateinfo::UserStateInfo(ciccio4);
	//signalSender->sendStateInfo(state4, this->meta.getTime());

}

void UserSignals::await(int seconds)
{
	
	/*clock_t endwait;
  endwait = clock () + seconds * CLOCKS_PER_SEC ;
  while (clock() < endwait) {}
}*/



} // namespace greta
} // namespace components
} // namespace semaine
