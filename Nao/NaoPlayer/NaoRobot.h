

#pragma once



#ifndef NAOROBOT_H
#define NAOROBOT_H

// ..::: Headers ::
#include "altypes.h"
#include "alptr.h"
#include "albrokermanager.h"
#include "altoolsmain.h"
#include "alxplatform.h"
#include "config.h"
#include <fstream>
#include <sstream>
#include <albroker.h>
#include <almodule.h>
#include <altools.h>
#include <alproxy.h>
#include <conio.h>
#include "alproxy.h"
#include "alledsproxy.h"
#include "alerror.h"
#include "alvisiondefinitions.h"
#include "alvisiondefinitions.h"
#include "opencv/highgui.h"
#include "opencv/cv.h"
#include "opencv/cvaux.h"


#define ALVALUE_STRING( val ) ((val.getType() == ALValue::TypeString) ? std::string(val) : std::string("") )
#define ALVALUE_DOUBLE( val ) ((val.getType() == ALValue::TypeDouble || val.getType() == ALValue::TypeInt) ? double(val) : 0.0 )
#define ALVALUE_INT( val ) ((val.getType() == ALValue::TypeInt || val.getType() == ALValue::TypeDouble) ? int(val) : 0)

#endif // NAOROBOT_H

class NaoRobot
{
public:
	NaoRobot();
	~NaoRobot(void);

protected:
	AL::ALProxy *motionProxy;
	AL::ALProxy *cameraProxy;
	AL::ALProxy *ttsProxy;
	AL::ALProxy *leds;

private:
	static string jointnames[26];

public:

	vector<float> get_angles();

	void make_motion_left_arm(vector<float> lVec6, float *lHand);
	void make_motion_right_arm(vector<float> rVec6, float *rHand);
	void make_motion(vector<float> lvec6, vector<float> rvec6, float *lHand, float *rHand);

	void motion_Demo(void);

	void motion_StiffnessOn(void);

	void motion_StiffnessOff(void);
	
	void motion_ArmsMotionBalancing(void);

	void motion_PoseInit(void);

	void motion_PoseZero(void);

	void Relax(void);

	void get_cameras(void);

	void text_to_speech(std::string text);

	void print_body_angles(void);

	void print(vector<float> data);

	void Test();
};
