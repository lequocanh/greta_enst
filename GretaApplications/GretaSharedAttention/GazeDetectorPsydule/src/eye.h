#include "cv.h"
#include <ctype.h>


#define WIND 26
#define WIND1 13
#define WIND2 36


unsigned char**	ucmatrix(int t1,int rows,int t2,int cols);
int *ivector(int nl,int nh);
double **dmatrix(int nrl,int nrh,int ncl,int nch);
unsigned char *ucvector(int nl,int nh);
double **dmatrix(int nrl,int nrh,int ncl,int nch);
double *dvector(int nl,int nh);
float **fmatrix(int nrl,int nrh,int ncl,int nch);

unsigned char**find_mouth(int &x, int &y,int xl, int yl, int xr, int yr, float** dirx_model,float** I_disx_model, unsigned char**orig,unsigned char**imh,int imageHeight,int imageWidth, int s1,int s2,int wind_size);


double ** ver_vectfield(unsigned char**input,int rows,int cols );
double ** hor_vectfield(unsigned char**input,int rows,int cols );
int*mouth_points(IplImage *forehead, unsigned char**R, unsigned char**G, unsigned char**B, int rows,int cols);
int*mouth_corners( unsigned char**binary, int rows,int cols);
int*mouth_center( unsigned char**binary, int rows,int cols);

unsigned char saturation( float r, float g, float b);
unsigned char** binarize( unsigned char**h, int rows,int cols);
float ang_dist(float p, float a, float b);

int* ProjFun(double start, double end, double a, unsigned char** area,int x1, int x2, int y1, int y2);


float **fmatrix(int nrl,int nrh,int ncl,int nch);
float**f_transpose(float**input,int rows,int cols);
float**f_resize(float**input,double t1,double t2,int rows,int cols);
float *fvector(int nl,int nh);
unsigned char free_ucmatrix(unsigned char **m,int nrl,int nrh,int ncl,int nch);

unsigned char free_ucmatrix(unsigned char **m,int nrl,int nrh,int ncl,int nch);
int free_fmatrix(float **m,int nrl,int nrh,int ncl,int nch);
int free_dmatrix(double **m,int nrl,int nrh,int ncl,int nch);
int free_vector(float *v,int nl,int nh);
int free_ivector(int *v,int nl,int nh);
unsigned char free_ucvector(unsigned char *v,int nl,int nh);
int find_charac(int &xa_or,int &ya_or,int &xa2_or,int &ya2_or, int &ws1,int &ws2, unsigned char** pictureR,unsigned char** pictureG,unsigned char** pictureB,
				int left,int up,int right,int down, int h1, int h2, int S1, int S2,
				int &xr_or, int &yr_or, int &xl_or, int &yl_or, int &mouth_upx, int &mouth_upy, int &mouth_downx, int &mouth_downy, int &r_eye_upx, int &r_eye_upy,int &r_eye_downx, int &r_eye_downy,int &l_eye_upx, int &l_eye_upy,int &l_eye_downx, int &l_eye_downy, int &rcornerx,int &rcornery,int &lcornerx, int &lcornery,int &ucenterx, int &dcenterx, int &centery, int &x13, int &y13 );

void tracker(double**track, unsigned char**prev,unsigned char**cur, double** init_points1, int rows,int cols);
int corner_correction(double**points, int height, int width, float &a, float &b);
int corner_correction_mouth(double**points, int height, int width, float &a, float &b);

double pint_line_dist(double a, double b, int x, int y);
int max_dist(float d1,float d2,float d3,float d4);


unsigned char**find_left_eye1(int &x_a2, int &y_a2,int x_position, int y_position, float** hor_coord,float** ver_coord, IplImage* orig,int imageHeight,int imageWidth, int S1,int S2,int wind_size);
IplImage*find_right_eye1(int &x_a, int &y_a,int h1,int h2, float** hor_coord,float** ver_coord,  IplImage* orig,int imageHeight,int imageWidth, int S1,int S2,int wind_size);

void vec2im(IplImage* src,unsigned char**dst, int cols,int rows);
void im2vec(unsigned char**src,IplImage* dst,int cols,int rows);


void imresize(unsigned char**input,unsigned char**output,int inrows,int incols,int outrows,int outcols);
void cv_distmap(unsigned char**input,unsigned char**output,int rows,int cols);

void darkpoint(unsigned char** area, int rows,int cols, int wind, int &xn, int &yn);
void skin(IplImage* forehead, int forheadcols, int forheadrows,  IplImage* face, int cols, int rows, IplImage* facemask );


void pshue(IplImage* src, IplImage* dst, int cols, int rows);
void pose(double &a,double &b,double &g, int fxr, int fyr, int fxl, int fyl, int fxm, int fym,int xr, int yr, int xl, int yl, int xm, int ym);

void gaze(float &x, float &y,IplImage *I) ;
void gaze2(float &x, float &y,IplImage *I);

int free_fvector(float *v,int nl,int nh);
int*eye_corners( unsigned char**binary, int rows,int cols, int xstart, int ystart);
void gaze_detector(double scale);
void eyebrow(int &x, int &y,int eyex, int eyey,int eyewidth,IplImage *I);
void nostrils(int &x, int &y,int eyex1, int eyey1,int eyex2, int eyey2, int eyewidth,IplImage *I);
void features_correct(double** prev_points,double** cur_points,IplImage *I);


void points_detect(double**points,IplImage*frame,unsigned char**inputR,unsigned char**inputG,unsigned char**inputB,
			int xr_or,int yr_or,int xl_or,int yl_or, 
			int mouth_upx,int  mouth_upy,int mouth_downx,int mouth_downy, 
			int r_eye_upx,int  r_eye_upy,int r_eye_downx,int r_eye_downy,
			int l_eye_upx,int  l_eye_upy,int l_eye_downx,int l_eye_downy,
			int rcornerx,int rcornery,int lcornerx,int lcornery,int mouth_ucenterx,int mouth_dcenterx,int mouth_centery,int xnose,int ynose);

void points_tracking(int NO_POINTS,double**points,double**fin_points,
					 IplImage *frame,unsigned char **inputGreyprev,unsigned char **inputGrey,int r_eye_downx,int r_eye_downy,int l_eye_downx,int l_eye_downy, int r_eye_upx,int r_eye_upy,int l_eye_upx,int l_eye_upy);

void points_gaze(float *px_m,float *py_m,float &px,float &py,int fr_c,int frame_count
,unsigned char **inputR,unsigned char **inputG,unsigned char **inputB,IplImage *frame,double **points,float pxfirstL,float pxfirstR,float pyfirstL,float pyfirstR);

void facedetect(CvPoint start,int w,int h ,int frame_count,IplImage* frame_or,IplImage *frame,unsigned char **inputGrey,unsigned char **inputR,unsigned char **inputG,unsigned char **inputB,int &Xup,int &Yup,int &Xdown,int &Ydown,int &faces);

void detect_track(CvPoint start,int w,int h ,int &flag,int frame_count,IplImage *frame, unsigned char **inputGrey,unsigned char **inputGreyprev,
				  double **points,double **fin_points, 
				  float* ppx_m,float* ppy_m, int &fr_c,
				  float &ppx,float &ppy,
				  float &px,float &py,float* px_m,float* py_m,float pxfirstL,float pxfirstR,float pyfirstL,float pyfirstR,
				  double **init_points, float &dist_monitor,int &xr_or,int  &yr_or,int  &xl_or,int  &yl_or );

void detect_and_draw( IplImage* img,int &fupx,int &fdownx,int &fupy,int &fdowny, int &faces_no );

void ellipsis(IplImage* face,int &fupx,int &fdownx,int &fupy,int &fdowny);

void points_gaze2(float *px_m,float *py_m,
float &px,float &py,int fr_c,int frame_count
,IplImage *frame,double **points,float pxfirstL,float pxfirstR,float pyfirstL,float pyfirstR
) ;


void on_mouse( int event, int x, int y, int flags, void* param );
