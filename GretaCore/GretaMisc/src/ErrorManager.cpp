/*
 *  ErrorManager.cpp
 *
 *  Created by Marc Schröder on 03.11.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#include "ErrorManager.h"
#include "xercesTool.h"

using namespace XERCES_CPP_NAMESPACE;

ErrorManager::ErrorManager(void):DefaultHandler()
{
}

ErrorManager::~ErrorManager(void)
{
}

void ErrorManager::warning(const SAXParseException &exc)
{
	std::cout << "XML warning : " << XercesTool::transcode(exc.getMessage()).c_str() << "\n";
}

void ErrorManager::error(const SAXParseException &exc)
{
	std::cout << "XML error : " << XercesTool::transcode(exc.getMessage()).c_str() << "\n";
	throw  exc;
}

void ErrorManager::fatalError(const SAXParseException &exc)
{
	std::cout << "fatal error in line " << exc.getLineNumber() << "\n";

	std::cout << XMLString::transcode(exc.getMessage()) << "\n";
	throw exc;
	//DefaultManager::fatalError(exc);
}






