// Shared-Attention system
// Christopher Peters

#ifndef _SHARED_ATTENTION_H
#define _SHARED_ATTENTION_H

extern int GLscreenXRes;
extern int GLscreenYRes;

enum CursorPositions
{
	CURSOR_CENTER = 0,

	CURSOR_LEFT = 1,
	CURSOR_RIGHT = 2,
	CURSOR_UP = 3, //top center
	CURSOR_DOWN = 4, //bottom center

	CURSOR_TOPLEFT = 5,
	CURSOR_TOPRIGHT = 6,
	CURSOR_BOTTOMRIGHT = 7,
	CURSOR_BOTTOMLEFT = 8
};

enum CursorStates
{
	CURSOR_READY = 0,
	CURSOR_RECORDING = 1
};

#define NUM_OBJECT_TYPES 4
static char* object_type_name[NUM_OBJECT_TYPES]= {
		"OBJECT_BACKGROUND","OBJECT_TABLE","OBJECT_GRETA","OBJECT_BOX"
};	
enum ObjectTypes {
	OBJECT_BACKGROUND = 0,
	OBJECT_TABLE = 1,
	OBJECT_GRETA = 2,
	OBJECT_BOX = 3
};

	struct BoundaryValues
	{
		float headTopVal;
		float headBottomVal;
		float headLeftVal;
		float headRightVal;
	};

#endif