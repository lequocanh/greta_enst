/**************************************************************************
 *
 *  Joint Limits Utility header file.
 *
 *  This file contains headers for utility functions needed by the joint
 *  limits system. We do not provide the actual funtions because it is
 *  expected that most people will already have these functions implemented
 *  in their own code. However, we do provide reference implementations for
 *  them in the comments, just in case.
 *
 **************************************************************************/

#ifndef JOINT_LIMITS_UTIL_H
#define JOINT_LIMITS_UTIL_H

#include "jointLimits.h"


#ifdef __cplusplus
extern "C" {
#endif

/* Range types. */

/**
 * Initialize the minimum and maximum Euler angles.
 */
void jtl_initMaxMin(Jtl_MaxMinPtr mm);


/***************** Matrix manipulation *************************/
/**
 * Vector dot Vector
 */
float jtl_vdv(float v1[3], float v2[3]);


/**
 * Vector cross Vector
 */
void jtl_vxv(float pt1[3], float pt2[3], float pt3[3]); 


/**
 * Vector plus Vector
 */
void jtl_vpv(float v1[3], float v2[3], float v3[3]);


/**
 * Copy v1 to v2 - 3 dimensions
 */
void jtl_cpv(float v1[3], float v2[3]);


/**
 * Copy v1 to v2 - 2 dimensions
 */
void jtl_cpv2(float v1[2], float v2[2]);


/**
 * Vector times a scaler
 */
void jtl_vxs(float v1[3], float s, float v2[3]);


/**
 * Vector minus Vector
 */
void jtl_vmv(float v1[3], float v2[3], float v3[3]);


/**
 * Average of two vectors
 */
void jtl_vave(float v1[3], float v2[3], float v3[3]);


/**
 * Triple scalar product
 *
 * vxvdv() is also known as ``triple scalar product'' and performs
 * (pt1 X pt2) dot pt3.  This is also the determinant of a 3x3 matrix
 * in which the rows are pt1, pt2, pt3 in that order.
 *
 * If the vertices of a triangle are 0 (origin), pt1 and pt2 in
 * counter-clockwise order, then vxvdv() is positive if and only if pt3
 * is on the same side of the plane of the triangle as the normal vector
 * points.  This may be faster than finding the plane equation first.
 *
 * If one vertex of a tetrahedron is at 0, and pt1, pt2, pt3 are the
 * other vertices in CLOCKwise order as viewed from 0, then vxvdv() / 6.0
 * is the volume.  For a paralleepiped, do not divide by 6.0.
 *
 */
float jtl_vxvdv(float pt1[3], float pt2[3], float pt3[3]); 


/**
 * The length of a vector
 */
float jtl_vlength(float v[3]);


/**
 * Rotates the z-axis of a given frame (given as a matrix) into
 * another vector (v) by rotating about an axis perpendicular to both,
 * and returns both the matrix that is the composite of this new
 * rotation and the frame rotation, and the zyx euler rotation that
 * the matrix codifies. This is usual, for example, if you want
 * to rotate a segment so that it aligns with another axis. 
 * assumes only rotation is important in the matrices.
 *
 * See below for additional helper functions.
 */
void jtl_rotztov(float framemat[4][4], float* v, float newmat[4][4],
  float *x, float *y, float *z, float hint[3]);


void get_zyx_rot_hint(float m_aafEntry[4][4],
					  float *rfZAngle,
					  float *rfYAngle,
					  float *rfXAngle,
					  float hint[3]);

/*** normalize a vector and return in second vector. also returns len */
float normvlen(float v1[3], float v2[3]);


/*** find a perpendicular axis to two vectors. Also normalizes it and checks
 * lengths, unlike make_perp
 */
int find_perpaxis(float u[4], float v[4], float n[3]);


/* normalize a vector and return in second vector*/
void jtl_normv(float v1[3], float v2[3]);


/* given the sine and cosine of the desired angle, and an arbitrary
 * rotation vector p, return the matrix that causes that amount of
 * rotation around that axis p
 */
void jtl_make_mat(float p[3], float c, float s, float mat[4][4]);


/*** MultiplyMatrices: result = m1*m2
 * result may be same matrix as either input
 */
void jtl_multiplyfMatrices (float result[4][4], float m1[4][4], float m2[4][4]);


/*** copyMatrix: result = copy of m*/
void copyMatrix (float result[4][4], float m[4][4]);


void jtl_get_zaxis_rotated(float rot[3], float axis[3]);


/* Memory handling. */
void *jtl_malloc(unsigned long num_bytes);


void jtl_free(void *p, unsigned long num_bytes);


float b_computeAngle(float c, float s);


#ifdef __cplusplus
} //extern "C" 
#endif
#endif /* JOINT_LIMITS_UTIL_H */


/* $Id: jointLimitsUtil.h,v 1.1 2007/05/14 11:05:45 mancini Exp $*/
/*
 * $Log: jointLimitsUtil.h,v $
 * Revision 1.1  2007/05/14 11:05:45  mancini
 * *** empty log message ***
 *
 * Revision 1.1  2005/01/21 12:27:32  mancini
 * starting version
 *
 * Revision 1.1  2004/05/25 08:30:11  bjoern
 * joint limit module
 *
 * Revision 1.1  2001/05/04 04:05:29  mslater
 * Initial revision
 *
 */
