/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gretaTests;

/**
 *
 * @author Andre
 */
public class PrintSender implements Sender{

    public void connect() {
        //just print, no connection
    }

    public void sendBML(String BML) {
       System.out.println(BML);
    }

    public void sendFML(String FML) {
        System.out.println(FML);
    }

    public boolean isConnected() {
        return true; 
    }

}
