@echo off
echo "COMPONENT START SCRIPT: starting semaine c++ component "tum.opensmile""
if "%CMS_URL%" == "" set CMS_URL=tcp://localhost:61616
echo "Connecting to ActiveMQ server at %CMS_URL%"

REM USE THE FOLLOWING LINE TO SEE A LIST OF AVAILABLE AUDIO DEVICES:
REM SEMAINExtract -C ..\auxiliary\opensmileSemaine.conf -listdevices 1
SEMAINExtract -C opensmileSemaine.conf
echo "COMPONENT START SCRIPT: component "tum.opensmile" exited"
pause

