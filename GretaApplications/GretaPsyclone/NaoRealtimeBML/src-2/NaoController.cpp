#include "NaoController.h"
#include "XercesTool.h"
#include "IniManager.h"

#include "XMLDOMParser.h"
#include "XMLDOMTree.h"

using namespace AL;

extern IniManager inimanager;
/**********************************************/
NaoController::NaoController()
{
	//parentBrokerIP="127.0.0.1";
	//parentBrokerPort=9559;

	run_on_virtualNAO=inimanager.GetValueInt("NAO_VIRTUAL");

 	parentBrokerIP=inimanager.GetValueString("NAO_HOST").c_str();
	parentBrokerPort=inimanager.GetValueInt("NAO_PORT");
	
	std::cout << "NAO's IP = " << this->parentBrokerIP <<std::endl;
	std::cout << "NAO's Port = " << this->parentBrokerPort <<std::endl;
	
	
	this->Init();

	
	this->StiffnessOn();

	this->PoseInit();

	AL::ALValue path;
	AL::ALValue times;
	float *lHand = new float(0.7);

	//AL::ALValue point1,point2,point3,point4,point5,point6;
	vector<AL::ALValue> point;
	point.resize(17);

	point[1].arrayPush(0.0);
	point[1].arrayPush(-0.02);
	point[1].arrayPush(0.0);
	point[1].arrayPush(0.0);
	point[1].arrayPush(0.0);
	point[1].arrayPush(0.0);


	point[2].arrayPush(0.0);
	point[2].arrayPush(0.0);
	point[2].arrayPush(0.01);
	point[2].arrayPush(0.0);
	point[2].arrayPush(0.0);
	point[2].arrayPush(0.0);

	point[3].arrayPush(0.0);
	point[3].arrayPush(0.08);
	point[3].arrayPush(0.0);
	point[3].arrayPush(0.0);
	point[3].arrayPush(0.0);
	point[3].arrayPush(0.0);

	point[4].arrayPush(0.0);
	point[4].arrayPush(0.0);
	point[4].arrayPush(-0.04);
	point[4].arrayPush(0.0);
	point[4].arrayPush(0.0);
	point[4].arrayPush(0.0);

	point[5].arrayPush(0.0);
	point[5].arrayPush(0.0);
	point[5].arrayPush(-0.02);
	point[5].arrayPush(0.0);
	point[5].arrayPush(0.0);
	point[5].arrayPush(0.0);

	point[6].arrayPush(0.0);
	point[6].arrayPush(0.0);
	point[6].arrayPush(0.01);
	point[6].arrayPush(0.0);
	point[6].arrayPush(0.0);
	point[6].arrayPush(0.0);

	point[7].arrayPush(0.0);
	point[7].arrayPush(0.0);
	point[7].arrayPush(-0.02);
	point[7].arrayPush(0.0);
	point[7].arrayPush(0.0);
	point[7].arrayPush(0.0);

	point[8].arrayPush(0.0);
	point[8].arrayPush(0.0);
	point[8].arrayPush(0.01);
	point[8].arrayPush(0.0);
	point[8].arrayPush(0.0);
	point[8].arrayPush(0.0);

	point[9].arrayPush(0.0);
	point[9].arrayPush(0.0);
	point[9].arrayPush(-0.02);
	point[9].arrayPush(0.0);
	point[9].arrayPush(0.0);
	point[9].arrayPush(0.0);

	point[10].arrayPush(0.0);
	point[10].arrayPush(0.0);
	point[10].arrayPush(0.01);
	point[10].arrayPush(0.0);
	point[10].arrayPush(0.0);
	point[10].arrayPush(0.0);

	point[11].arrayPush(0.0);
	point[11].arrayPush(0.0);
	point[11].arrayPush(-0.02);
	point[11].arrayPush(0.0);
	point[11].arrayPush(0.0);
	point[11].arrayPush(0.0);

	point[12].arrayPush(0.0);
	point[12].arrayPush(0.0);
	point[12].arrayPush(0.01);
	point[12].arrayPush(0.0);
	point[12].arrayPush(0.0);
	point[12].arrayPush(0.0);

	point[13].arrayPush(0.0);
	point[13].arrayPush(0.0);
	point[13].arrayPush(-0.02);
	point[13].arrayPush(0.0);
	point[13].arrayPush(0.0);
	point[13].arrayPush(0.0);

	point[14].arrayPush(0.0);
	point[14].arrayPush(0.0);
	point[14].arrayPush(0.01);
	point[14].arrayPush(0.0);
	point[14].arrayPush(0.0);
	point[14].arrayPush(0.0);

	point[15].arrayPush(0.0);
	point[15].arrayPush(0.0);
	point[15].arrayPush(-0.02);
	point[15].arrayPush(0.0);
	point[15].arrayPush(0.0);
	point[15].arrayPush(0.0);

	point[16].arrayPush(0.0);
	point[16].arrayPush(0.0);
	point[16].arrayPush(0.01);
	point[16].arrayPush(0.0);
	point[16].arrayPush(0.0);
	point[16].arrayPush(0.0);

	path.arrayPush(point[1]);
	path.arrayPush(point[2]);
	path.arrayPush(point[3]);
	path.arrayPush(point[4]);
	path.arrayPush(point[5]);
	path.arrayPush(point[6]);
	path.arrayPush(point[7]);
	path.arrayPush(point[8]);
	path.arrayPush(point[9]);
	path.arrayPush(point[10]);	
	path.arrayPush(point[11]);	
	path.arrayPush(point[12]);	
	path.arrayPush(point[13]);	
	path.arrayPush(point[14]);	
	path.arrayPush(point[15]);	
	path.arrayPush(point[16]);
	

	for(int i=0;i<16;i++)
		times.arrayPush(i+0.5);

//	motionProxy->pCall("positionInterpolation",string("LArm"),SPACE,path,MASK,times,false);
//	SleepMs(2000); // wait two seconds
//	this->PoseInit();

	//point1.arrayPush(0.0);
	//point1.arrayPush(-0.02);
	//point1.arrayPush(0.0);
	//point1.arrayPush(0.0);
	//point1.arrayPush(0.0);
	//point1.arrayPush(0.0);


	//point2.arrayPush(0.0);
	//point2.arrayPush(0.0);
	//point2.arrayPush(0.01);
	//point2.arrayPush(0.0);
	//point2.arrayPush(0.0);
	//point2.arrayPush(0.0);

	//point3.arrayPush(0.0);
	//point3.arrayPush(0.08);
	//point3.arrayPush(0.0);
	//point3.arrayPush(0.0);
	//point3.arrayPush(0.0);
	//point3.arrayPush(0.0);

	//point4.arrayPush(0.0);
	//point4.arrayPush(0.0);
	//point4.arrayPush(-0.04);
	//point4.arrayPush(0.0);
	//point4.arrayPush(0.0);
	//point4.arrayPush(0.0);

	//point5.arrayPush(0.0);
	//point5.arrayPush(0.0);
	//point5.arrayPush(-0.02);
	//point5.arrayPush(0.0);
	//point5.arrayPush(0.0);
	//point5.arrayPush(0.0);

	//point6.arrayPush(0.0);
	//point6.arrayPush(0.0);
	//point6.arrayPush(0.01);
	//point6.arrayPush(0.0);
	//point6.arrayPush(0.0);
	//point6.arrayPush(0.0);

	//path.arrayPush(point1);
	//path.arrayPush(point2);
	//path.arrayPush(point3);
	//path.arrayPush(point4);
	//path.arrayPush(point5);
	//path.arrayPush(point6);

	//times.arrayPush(0.5);
	//times.arrayPush(1.0);
	//times.arrayPush(2.0);
	//times.arrayPush(3.0);
	//times.arrayPush(4.0);
	//times.arrayPush(4.5);




}
/**********************************************
* 000 = XEP YUpperEP ZFar, ... 462 = XOppC YLowerEP ZNear 
* joints[x][y][z] = ("LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll")
***********************************************/
void NaoController::LoadArmPositions()
{
	std::string name;
	float temp[4];
	const char *look_up_joints[]={"LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll"};
	std::string gestuary_path = "Nao/armpositions.xap";//"C:/Program Files (x86)/Aldebaran/SDK 1.6.0/share/choregraphe/libraries/positions/armpositions.xap";
	XMLDOMParser *xmlparser;
	xmlparser=new XMLDOMParser();
	
	XMLGenericTree *xmltree=xmlparser->ParseFile(gestuary_path);
	
	
	if(xmltree==0)	return 	;

	
	XMLGenericTree *chld1;
	int i=0;
	for(XMLGenericTree::iterator iter1=xmltree->begin();iter1!=xmltree->end();++iter1)
	{		
		chld1 = *iter1;		
		if (chld1->isTextNode()) continue;
		//std::cout << chld1->GetName()<<std::endl;
		
		XMLGenericTree *chld2;
		for(XMLGenericTree::iterator iter2=(*iter1)->begin(); iter2 != (*iter1)->end(); ++iter2)		
		{
			chld2=*iter2;
			if (chld2->isTextNode()) continue;
			//std::cout << chld2->GetName()<<std::endl;

			XMLGenericTree *chld3;
			if(chld2->GetName()=="Motors")
			{
				// children of Motors
				for(XMLGenericTree::iterator iter3=(*iter2)->begin(); iter3 != (*iter2)->end(); ++iter3)		
				{
					chld3 = *iter3;
					if (chld3->isTextNode()) continue;
					
					XMLGenericTree *chld4;					
					// children of Motor, that are name and value
					for(XMLGenericTree::iterator iter4=(*iter3)->begin(); iter4 != (*iter3)->end(); ++iter4)		
					{
						chld4 = *iter4;						
						if (chld4->isTextNode()) continue;
						
						XMLGenericTree *chld5;									
						if(chld4->GetName()=="name")
						{							
							for(XMLGenericTree::iterator iter5=(*iter4)->begin(); iter5 != (*iter4)->end(); ++iter5)		
							{						
								chld5=*iter5;							
								name = chld5->GetTextValue();							
							}
						}else
						{//const char *look_up[]={"LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll"};					
							XMLGenericTree *chld6;
							for(XMLGenericTree::iterator iter5=(*iter4)->begin(); iter5 != (*iter4)->end(); ++iter5)		
							{						
								chld6=*iter5;							
								//std::cout << chld6->GetTextValue()<< std::endl;							
								for(int i=0;i<4;i++)
									if(look_up_joints[i]==name)
									{
										std::cout << name<< "  "<<chld6->GetTextValue()<<std::endl;
										temp[i] = atof(chld6->GetTextValue().c_str());
									}
							}
						
						}
						
					}
					
				}
			}
			if(chld2->GetName()=="name")
			{
				std::cout << "lam viec voi ten o day: ";
				XMLGenericTree *chldname;
				for(XMLGenericTree::iterator iter=(*iter2)->begin(); iter != (*iter2)->end(); ++iter)		
				{						
					chldname=*iter;
					std::cout << chldname->GetTextValue()<<std::endl;
					
					int intReturn = atoi(chldname->GetTextValue().c_str());
					int x = intReturn / 100;
					int y = (intReturn - x*100) / 10;
					int z = (intReturn - x*100 - y*10);
					std::cout <<x<<":"<<y<<":"<<z<<endl;
					for(int i=0;i<4;i++) joints[x][y][z][i] = temp[i];
					// x = intReturn / 100; y = intReturn/100/10,..
					
				}

			}

		}
		i++; 
	}

	//test
	for(int x=0;x<5;x++)
		for(int y=0;y<7;y++)
			for(int z=0;z<3;z++)
			{
				std::cout << x <<":"<<y<<":"<<z<<":";
				for(int i=0;i<4;i++)
					std::cout<<joints[x][y][z][i]<<" - ";
				std::cout<<std::endl;
			}
//	std::cout << i<< std::endl;


	delete xmlparser;
	delete xmltree;	

}
/**********************************************/
/**********************************************/
void NaoController::Init()
{
	LoadArmPositions();

	//--------------- Load MotionProxy ---------------
	try
	{
		motionProxy  = new AL::ALProxy("ALMotion", parentBrokerIP, parentBrokerPort);
		std::cout << "Loading MotionProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
	// This will throw an exception
	}
	catch( AL::ALError &err ) {	std::cout << err.toString() << std::endl; }

	if(run_on_virtualNAO==0)	
	{
		try //--------------- Load AudioPlayerProxy ----------
		{
			audioPlayerProxy  = new AL::ALProxy("ALAudioPlayer", parentBrokerIP, parentBrokerPort);
			std::cout << "Loading AudioPlayerProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
		// This will throw an exception
		}
		catch( AL::ALError &err ){	std::cout << err.toString() << std::endl; }	
	
		try //-------------- Load AudioDeviceProxy ------------
		{
			audioDeviceProxy  = new AL::ALProxy("ALAudioDevice", parentBrokerIP, parentBrokerPort);
			std::cout << "Loading ALAudioDeviceProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
		// This will throw an exception
		}
		catch( AL::ALError &err ){std::cout << err.toString() << std::endl;	}
	}

    //-------------- Joint values for initial position -----------
		// Feel free to experiment with these values
	float kneeAngle  = 40;
	float torsoAngle =  0;
	float wideAngle  =  0;
	std::string pNames = "Body";

		// set values in degree
	float initPosition26[] = 
	{0, 0, // Head
	120,  15, -90, -80, 0, 0, // LeftArm
	0,  wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2, -wideAngle, // LeftLeg
	0, -wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2,  wideAngle, // RightLeg
	120, -15,  90,  80, 0, 0}; // RightArm	

	// convert to radian
	InitPosition.resize(26,0.0);
	for(int i=0;i<26;i++)InitPosition[i] = (float)(initPosition26[i]*PI)/180 ;	
		
	//---------------- Joint values for relax position ------------
	ZeroPosition.resize(26,0.0);
	float PositionRad[] = {0.0, 0.015298, 1.43271, 0.12728, -0.675002, -1.07069, -1.65523, 0.0, -0.300625, -0.0783416, -0.760086, 2.18445, -1.2242, 0.103589, -0.300625, 0.0537223, -0.771299, 2.19323, -1.22272, -0.0998575, 1.55398, -0.00464396, 0.935698, 1.03549, 1.12285, 0.0};
	for(int i=0;i<26;i++) ZeroPosition[i]=PositionRad[i];
}

/**********************************************/
void NaoController::StiffnessOn()
{
	//We use the "Body" name to signify the collection of all joints
	std::string pNames = "Body";
	float pStiffnessLists = 1.0;
	float pTimeLists = 1.0;
	motionProxy->callVoid("stiffnessInterpolation", pNames, pStiffnessLists, pTimeLists);
}

/**********************************************/
void NaoController::StiffnessOff()
{
	//We use the "Body" name to signify the collection of all joints
	std::string pNames = "Body";
	float pStiffnessLists = 0.0;
	float pTimeLists = 1.0;
	motionProxy->callVoid("stiffnessInterpolation", pNames, pStiffnessLists, pTimeLists);
}

/**********************************************/
void NaoController::PoseInit()
{
//------------------------------ send the commands -----------------------------
//We use the "Body" name to signify the collection of all joints
std::string pNames = "Body";
//We set the fraction of max speed
float pMaxSpeedFraction = 0.2;
//Ask motion to do this with a blocking call
motionProxy->callVoid("angleInterpolationWithSpeed",pNames, InitPosition, pMaxSpeedFraction);
}

/**********************************************/
void NaoController::OpenHand()
{
		//Get the Number of Joints	
	vector<string> jointNames = motionProxy->call<vector<string>>("getJointNames",string("body"));
	int NumJoints = jointNames.size();
	if(NumJoints>22)
	{
		// put stiffness on LHand
		std::string pNames = "LHand";		
		// method 1		    
			float pMaxSpeedFraction = 0.5;
			motionProxy->callVoid("stiffnessInterpolation",pNames, 1.0, 1.0);
			motionProxy->callVoid("angleInterpolationWithSpeed",pNames, 0.5, pMaxSpeedFraction);
			motionProxy->callVoid("angleInterpolationWithSpeed",pNames, 0.0, pMaxSpeedFraction);
			SleepMs(2000); // wait two seconds

		// method 2
			motionProxy->callVoid("openHand",pNames);
			motionProxy->callVoid("closeHand",pNames);
	}
}
/**********************************************/
void NaoController::SendWavFile()//(char* wavFile, int size)
{
  const int outputBufferSize=11116384;   // maximum buffer size that ALAudioDevice can send
  const int numberOfOutputChannels=2; // number of output channels on Nao
  
  // --------- opening of the input file --------------------------------------------------------
  FILE *fInputWavFile;
  std::string pFileName = "C:/LEQUOCANH/GRETA/bin/output/buffer.wav";
  if ( ( fInputWavFile = fopen ( pFileName.c_str(), "rb" ) ) == NULL )
  {
      throw AL::ALError ( "ALHelloWorld", "helloWorld", "Failed to open input sound file." );
  }
  // --------- reading of the wav header ---------------------------------------------------------
  wavheader *wavHeader=new wavheader;
  fread(wavHeader,1,44,fInputWavFile);//fread(wavHeader,1,44,fInputWavFile);
  unsigned short nbOfChannels = wavHeader->NumChannels;
  unsigned long sampleRate = wavHeader->SampleRate;
  unsigned short bitsPerSample = wavHeader->BitsPerSample;
   
  // ------- set output sample rate of audiodevice -------------------------------------------------------------------
  
  audioDeviceProxy->callVoid("setParameter",std::string("outputSampleRate"),(int) sampleRate); 
  
  // ------- go to the beginning of the audio data -------------------------------------------------------------------
  fseek (fInputWavFile , 44 , SEEK_SET );//fseek (fInputWavFile , 44 , SEEK_SET );
  
  // --------------- reading of the audio data
  short *fInputAudioData=new short[outputBufferSize*nbOfChannels]; // buffer to store the data contained in the wav file
  short *fStereoAudioData=new short[outputBufferSize*numberOfOutputChannels]; // buffer to construct stereo audio data 
  ALValue pDataBin; // ALValue where to store the stereo audio data to send

  while ( !feof ( fInputWavFile ) )
  {
    //Read samples from file
    int fNbOfInputSamples = fread ( fInputAudioData,bitsPerSample/8*nbOfChannels,outputBufferSize,fInputWavFile );

    if (nbOfChannels==1)
    {  
      // construction of stereo audio data in case the input file is mono   
      int i=0;
      for (int j=0;j<fNbOfInputSamples;j++)
      {
        fStereoAudioData[i]=fInputAudioData[j]; 
        fStereoAudioData[i+1]=fInputAudioData[j];
        i+=numberOfOutputChannels;
      }
      pDataBin.SetBinary( fStereoAudioData, fNbOfInputSamples*sizeof(short)*numberOfOutputChannels ); // transform audio data to binary samples
    }
    else
    {
      pDataBin.SetBinary( fInputAudioData, fNbOfInputSamples*sizeof(short)*numberOfOutputChannels ); // transform audio data to binary samples  
    }

    // ------------------------ send the stereo audio data to audiodevice module ----------------------------------------
    audioDeviceProxy->call <bool> ("sendRemoteBufferToOutput",fNbOfInputSamples,pDataBin); 

  }
  
  fclose(fInputWavFile);
  audioDeviceProxy->callVoid("stopAudioOut"); // to clean audio output buffer
}
/**********************************************/
void NaoController::RelaxPose()
{
	//----------- send the commands --------------
	std::string pNames = "Body";
	//We set the fraction of max speed
	float pMaxSpeedFraction = 0.2;
	//Ask motion to do this with a blocking call
	motionProxy->callVoid("angleInterpolationWithSpeed",pNames, ZeroPosition, pMaxSpeedFraction);	
	this->StiffnessOff();
}

/**********************************************/
void NaoController::make_motion_left_arm_following_path(AL::ALValue path, AL::ALValue times, float *lHand)
{	
	motionProxy->pCall("changeAngles",string("LHand"),*lHand,0.5);

	motionProxy->pCall("positionInterpolation",string("LArm"),SPACE,path,MASK,times,true);
}
/**********************************************/
void NaoController::make_motion_right_arm_following_path(AL::ALValue path, AL::ALValue times, float *lHand)
{
	motionProxy->pCall("changeAngles",string("RHand"),*lHand,0.5);

	motionProxy->pCall("positionInterpolation",string("RArm"),SPACE,path,MASK,times,true);
}
/**********************************************/
void NaoController::move_left_arm_to_one_position(vector<float> lVec6, float *lHand, float time)
{
	motionProxy->pCall("positionInterpolation",string("LArm"),SPACE,lVec6,MASK,time,true);

	motionProxy->pCall("changeAngles",string("LHand"),*lHand,0.5);

}

/**********************************************/
void NaoController::move_right_arm_to_one_position(vector<float> rVec6, float *rHand, float time)
{
	motionProxy->pCall("positionInterpolation",string("RArm"),SPACE,rVec6,MASK,time,true);

	motionProxy->pCall("changeAngles",string("RHand"),*rHand,0.5); 
}

/**********************************************/
void NaoController::Test()
{
	//int myPoints[][3] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9} }; is ok but
	//int myPoints[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9} }; is not

	float leftPoints[][6]= {
	 {0.11350152641534805, 0.040545769035816193, 0.21521936357021332, 1.4294466972351074, -0.97878050804138184, -1.377147912979126},        // Point 1
     {0.1402309387922287, 0.052671797573566437, 0.21063148975372314, 1.0271948575973511, -0.94719630479812622, -0.89422804117202759},        // Point 2
     {0.11350152641534805, 0.040545769035816193, 0.21521936357021332, 1.4294466972351074, -0.97878050804138184, -1.377147912979126},        // Point 3
     {0.11201596260070801, 0.074587292969226837, 0.22946535050868988, 1.0888181924819946, -1.3362702131271362, -1.16478431224823},        // Point 4
     {0.145968958735466, 0.076986677944660187, 0.19971583783626556, 0.37995263934135437, -1.1250594854354858, -0.46690171957015991},        // Point 5
     {0.11201596260070801, 0.074587292969226837, 0.22946535050868988, 1.0888181924819946, -1.3362702131271362, -1.16478431224823},          // Point 6
	 {0.1195632591843605, 0.16069468855857849, 0.20406840741634369, -1.4562171697616577, -1.021564245223999, 1.2938979864120483},                 // Point 7 
     {0.15122996270656586, 0.14950211346149445, 0.18904995918273926, -0.9355924129486084, -0.9226306676864624, 0.74650120735168457},            // Point 8
     {0.1195632591843605, 0.16069468855857849, 0.20406840741634369, -1.4562171697616577, -1.021564245223999, 1.2938979864120483},               // Point 9
     {0.1195632591843605, 0.16069468855857849, 0.20406840741634369, -1.4562171697616577, -1.021564245223999, 1.2938979864120483},               // Point 10
     {0.11971893161535263, 0.070205189287662506, 0.22869342565536499, 0.9983789324760437, -1.2386798858642578, -1.0090234279632568},           // Point 11
     {0.10455054044723511, 0.054743926972150803, 0.2269672304391861, -0.50609499216079712, -1.2386798858642578, -1.0090234279632568},          // Point 12
     {0.088421188294887543, 0.06972040981054306, 0.23430192470550537, -2.1414685249328613, -1.2386798858642578, -1.0090234279632568},          // Point 13
     {0.13358628749847412, 0.070149831473827362, 0.0091027393937110901, 3.1025271415710449, 0.19470416009426117, -0.27959024906158447}};        // Point 14

	float rightPoints[][6]= {
    {0.11168927699327469, -0.13540804386138916, 0.22010029852390289, 1.4148390293121338, -1.2193089723587036, -1.3576724529266357},
	{0.1412547379732132, -0.1428678035736084, 0.18613491952419281, 0.93530601263046265, -1.010956883430481, -0.88953870534896851},
	{0.11168927699327469, -0.13540804386138916, 0.22010029852390289, 1.4148390293121338, -1.2193089723587036, -1.3576724529266357},
	{0.11017732322216034, -0.081690005958080292, 0.23183009028434753, -1.2684494256973267, -1.3709535598754883, 1.172272801399231},
	{0.14769615232944489, -0.086495645344257355, 0.20729939639568329, -0.44910845160484314, -1.1185369491577148, 0.34184464812278748},
	{0.11017732322216034, -0.081690005958080292, 0.23183009028434753, -1.2684494256973267, -1.3709535598754883, 1.172272801399231},
	{0.10604579746723175, -0.048633892089128494, 0.2290404886007309, -1.6916605234146118, -1.0356745719909668, 1.4329190254211426},
	{0.14461638033390045, -0.057281237095594406, 0.20412959158420563, -1.1034955978393555, -0.94249629974365234, 0.84050732851028442},
	{0.10604579746723175, -0.048633892089128494, 0.2290404886007309, -1.6916605234146118, -1.0356745719909668, 1.4329190254211426},
	{0.10604579746723175, -0.048633892089128494, 0.2290404886007309, -1.6916605234146118, -1.0356745719909668, 1.4329190254211426},
	{0.1129244789481163, -0.073763400316238403, 0.23241670429706573, -1.2999225854873657, -1.283232569694519, 1.1617684364318848},
	{0.099649213254451752, -0.056318521499633789, 0.22924436628818512, 0.2412031888961792, -1.283232569694519, 1.1617684364318848},
	{0.0814543217420578, -0.070474915206432343, 0.23522615432739258, 1.934172511100769, -1.283232569694519, 1.1617684364318848},
	{0.13536836206912994, -0.083840891718864441, 0.0038926079869270325, -3.1309926509857178, 0.24004983901977539, 0.14689832925796509}};        // Point 14

	float times[]      = {3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5};
	float lefttimes[]      = {2.983, 3.183, 3.283, 3.459, 3.659, 3.759, 3.901, 4.201, 5.50713, 5.60713, 6.08, 7.08 , 8.08, 8.18}; 
	float righttimes[]      ={2.983, 3.183, 3.283, 3.459, 3.659, 3.759, 3.901, 4.201, 5.41383, 5.51383, 6.08, 7.08 , 8.08, 8.18}; 

	this->test_leftPath.arrayReserve(14);	
	this->test_rightPath.arrayReserve(14);
	this->test_leftTimes.arrayReserve(14);
	this->test_rightTimes.arrayReserve(14);


	for(int i=0;i<14;i++) this->test_leftTimes.arrayPush((float)times[i]);
	for(int i=0;i<14;i++) this->test_rightTimes.arrayPush((float)times[i]);

	for(int i=0;i<14;i++)
	{		
		AL::ALValue p;
		p.arrayReserve(6);
		for(int j=0;j<6;j++) 
		{
			p.arrayPush((float)leftPoints[i][j]);
			this->test_leftPathv[i][j]=leftPoints[i][j];
		}
		
		this->test_leftPath.arrayPush(p);		
	}

	for(int i=0;i<14;i++)
	{
		AL::ALValue p;
		p.arrayReserve(6);
		for(int j=0;j<6;j++) 
		{
			p.arrayPush((float)rightPoints[i][j]);
			this->test_rightPathv[i][j]=rightPoints[i][j];
		}
		
		this->test_rightPath.arrayPush(p);	
	}

	
	float *lHand=new float(0.7f);

	//this->PlayFile();
	this->make_motion_left_arm_following_path(this->test_leftPath,this->test_leftTimes,lHand);
	this->make_motion_right_arm_following_path(this->test_rightPath,this->test_rightTimes,lHand);
	this->PoseInit();
	

}
/**********************************************/
void NaoController::Motion()
{
	//AL::ALValue rnames, lnames;
	//lnames.arrayPush(AL::ALValue("LElbowRoll"));
	//lnames.arrayPush(AL::ALValue("LElbowYaw"));
	//lnames.arrayPush(AL::ALValue("LShoulderPitch")); // khong doi cho ca 2 tay
	//lnames.arrayPush(AL::ALValue("LShoulderRoll"));

	//lnames.arrayPush(AL::ALValue("LHand"));
	//lnames.arrayPush(AL::ALValue("LWristYaw"));

	//rnames.arrayPush(AL::ALValue("RElbowRoll"));
	//rnames.arrayPush(AL::ALValue("RElbowYaw"));
	//rnames.arrayPush(AL::ALValue("RShoulderPitch")); // khong doi cho ca 2 tay
	//rnames.arrayPush(AL::ALValue("RShoulderRoll"));

	//rnames.arrayPush(AL::ALValue("RHand"));
	//rnames.arrayPush(AL::ALValue("RWristYaw"));


	AL::ALValue names;
	

	names.arrayPush(AL::ALValue("LElbowRoll"));
	names.arrayPush(AL::ALValue("LElbowYaw"));
	names.arrayPush(AL::ALValue("LShoulderPitch")); // khong doi cho ca 2 tay
	names.arrayPush(AL::ALValue("LShoulderRoll"));

	names.arrayPush(AL::ALValue("LHand"));
	names.arrayPush(AL::ALValue("LWristYaw"));

	names.arrayPush(AL::ALValue("RElbowRoll"));
	names.arrayPush(AL::ALValue("RElbowYaw"));
	names.arrayPush(AL::ALValue("RShoulderPitch")); // khong doi cho ca 2 tay
	names.arrayPush(AL::ALValue("RShoulderRoll"));

	names.arrayPush(AL::ALValue("RHand"));
	names.arrayPush(AL::ALValue("RWristYaw"));

	std::cout<<"rightsize="<<gestures.rightPath.size()<<std::endl;
	std::cout<<"leftsize="<<gestures.leftPath.size()<<std::endl;


	int x,y,z;
	float hand_open,hand_dir,time, prev_time=0;
	
	AL::ALValue angleArrayLists;
	AL::ALValue angleLists_LElbowRoll;
	AL::ALValue angleLists_LElbowYaw;
	AL::ALValue angleLists_LShoulderPitch;
	AL::ALValue angleLists_LShoulderRoll;
	AL::ALValue angleLists_LHand;
	AL::ALValue angleLists_LWristYaw;

	AL::ALValue angleLists_RElbowRoll;
	AL::ALValue angleLists_RElbowYaw;
	AL::ALValue angleLists_RShoulderPitch;
	AL::ALValue angleLists_RShoulderRoll;
	AL::ALValue angleLists_RHand;
	AL::ALValue angleLists_RWristYaw;

	AL::ALValue rtimes;	
	AL::ALValue ltimes;	
	AL::ALValue arrayTimes;

	prev_time=0;

	for(int k=0;k<gestures.leftPath.size();k++)
	{			
		x=gestures.leftPath[k][0];
		y=gestures.leftPath[k][1];
		z=gestures.leftPath[k][2];
		hand_open=gestures.leftPath[k][3];
		hand_dir=gestures.leftPath[k][4];
		time=gestures.leftPath[k][5]-prev_time;		
		if(time<=0.3) 
		{
			// continue; bo qua neu thoi gian nho qua
			time = 0.3;
			gestures.leftPath[k][5]=prev_time+time;
		}
		std::cout<<prev_time<<":"<<time<<std::endl;
		prev_time=gestures.leftPath[k][5];
		
		angleLists_LElbowRoll.arrayPush(AL::ALValue(joints[x][y][z][0]*PI/180));
		angleLists_LElbowYaw.arrayPush(AL::ALValue(joints[x][y][z][1]*PI/180));
		angleLists_LShoulderPitch.arrayPush(AL::ALValue(joints[x][y][z][2]*PI/180));
		angleLists_LShoulderRoll.arrayPush(AL::ALValue(joints[x][y][z][3]*PI/180));
		angleLists_LHand.arrayPush(AL::ALValue(hand_open));
		angleLists_LWristYaw.arrayPush(AL::ALValue(hand_dir*PI/180));

		ltimes.arrayPush(gestures.leftPath[k][5]);

	//AL::ALValue langleLists;
	//langleLists.arrayPush(AL::ALValue(joints[x][y][z][0]*PI/180));
	//langleLists.arrayPush(AL::ALValue(joints[x][y][z][1]*PI/180));
	//langleLists.arrayPush(AL::ALValue(joints[x][y][z][2]*PI/180));
	//langleLists.arrayPush(AL::ALValue(joints[x][y][z][3]*PI/180));
	//langleLists.arrayPush(AL::ALValue(hand_open));
	//langleLists.arrayPush(AL::ALValue(hand_dir*PI/180));

	//motionProxy->pCall("angleInterpolation", lnames, langleLists,time,true);	
	}
	prev_time=0;
	for(int k=0;k<gestures.rightPath.size();k++)
	{			
		x=gestures.rightPath[k][0];
		y=gestures.rightPath[k][1];
		z=gestures.rightPath[k][2];
		hand_open=gestures.rightPath[k][3];
		hand_dir=gestures.rightPath[k][4];
		time=gestures.rightPath[k][5]-prev_time;		
		if(time<=0.3) 
		{
			time = 0.3;
			gestures.rightPath[k][5]=prev_time+time;
		}
		std::cout<<prev_time<<":"<<time<<":"<<x<<":"<<y<<":"<<z<<std::endl;
		prev_time=gestures.rightPath[k][5];
		
		angleLists_RElbowRoll.arrayPush(AL::ALValue((-1)*joints[x][y][z][0]*PI/180));
		angleLists_RElbowYaw.arrayPush(AL::ALValue((-1)*joints[x][y][z][1]*PI/180));
		angleLists_RShoulderPitch.arrayPush(AL::ALValue(joints[x][y][z][2]*PI/180));
		angleLists_RShoulderRoll.arrayPush(AL::ALValue((-1)*joints[x][y][z][3]*PI/180));
		angleLists_RHand.arrayPush(AL::ALValue(hand_open));
		angleLists_RWristYaw.arrayPush(AL::ALValue(hand_dir*PI/180));

		rtimes.arrayPush(gestures.rightPath[k][5]);

	//AL::ALValue rangleLists;
	//rangleLists.arrayPush(AL::ALValue(joints[x][y][z][0]*PI/180));
	//rangleLists.arrayPush(AL::ALValue(joints[x][y][z][1]*PI/180));
	//rangleLists.arrayPush(AL::ALValue(joints[x][y][z][2]*PI/180));
	//rangleLists.arrayPush(AL::ALValue(joints[x][y][z][3]*PI/180));
	//rangleLists.arrayPush(AL::ALValue(hand_open));
	//rangleLists.arrayPush(AL::ALValue(hand_dir*PI/180));

	//motionProxy->pCall("angleInterpolation", rnames, rangleLists,time,true);	
	}

	angleArrayLists.arrayPush(angleLists_LElbowRoll);
	arrayTimes.arrayPush(ltimes);
	angleArrayLists.arrayPush(angleLists_LElbowYaw);
	arrayTimes.arrayPush(ltimes);
	angleArrayLists.arrayPush(angleLists_LShoulderPitch);
	arrayTimes.arrayPush(ltimes);
	angleArrayLists.arrayPush(angleLists_LShoulderRoll);
	arrayTimes.arrayPush(ltimes);
	angleArrayLists.arrayPush(angleLists_LHand);
	arrayTimes.arrayPush(ltimes);
	angleArrayLists.arrayPush(angleLists_LWristYaw);
	arrayTimes.arrayPush(ltimes);

	angleArrayLists.arrayPush(angleLists_RElbowRoll);
	arrayTimes.arrayPush(rtimes);
	angleArrayLists.arrayPush(angleLists_RElbowYaw);
	arrayTimes.arrayPush(rtimes);
	angleArrayLists.arrayPush(angleLists_RShoulderPitch);
	arrayTimes.arrayPush(rtimes);
	angleArrayLists.arrayPush(angleLists_RShoulderRoll);
	arrayTimes.arrayPush(rtimes);
	angleArrayLists.arrayPush(angleLists_RHand);
	arrayTimes.arrayPush(rtimes);
	angleArrayLists.arrayPush(angleLists_RWristYaw);
	arrayTimes.arrayPush(rtimes);

	if(run_on_virtualNAO==0)
	{
		audioPlayerProxy->pCall("playFile",std::string("/home/nao/wav/buffer.wav"));
		SleepMs(300);
	}
	
	motionProxy->callVoid("angleInterpolation", names, angleArrayLists,arrayTimes,true);	
	

	gestures.leftPath.clear();
	gestures.rightPath.clear();
	gestures.leftTimes.clear();
	gestures.rightTimes.clear();

	//SleepMs(2000); // wait two seconds
	this->PoseInit();

}
/**********************************************/
void NaoController::Animation()
{	
	AL::ALValue leftPath;
	AL::ALValue leftTimes;

	AL::ALValue rightPath;
	AL::ALValue rightTimes;

	float *lHand = new float(0.7);

	
	vector<AL::ALValue> rightPoint;
	vector<AL::ALValue> leftPoint;
	vector<float> ltimes;
	vector<float> rtimes;
	
	rightPoint.resize(gestures.rightPath.size());
	leftPoint.resize(gestures.leftPath.size());

	ltimes.resize(gestures.leftPath.size());
	rtimes.resize(gestures.rightPath.size());

//	for(int i=0;i<14;i++)for(int j=0;j<6;j++)	gestures.leftPath[i][j] = this->test_leftPathv[i][j];
//	for(int i=0;i<14;i++)for(int j=0;j<6;j++)	gestures.rightPath[i][j] = this->test_rightPathv[i][j];
	
		
	for(int k=0;k<gestures.leftPath.size();k++)
	{
		
		for(int i=0;i<6;i++)			
			leftPoint[k].arrayPush(gestures.leftPath[k][i]);

		leftPath.arrayPush(leftPoint[k]);

		ltimes[k] = (float)gestures.leftTimes[k];
		if(k>0) {ltimes[k] = (float)gestures.leftTimes[k] - (float)gestures.leftTimes[k-1];}

		if(k>0 && (float)gestures.leftTimes[k]==(float)gestures.leftTimes[k-1])
			(float)gestures.leftTimes[k]=(float)gestures.leftTimes[k]+0.5;
		leftTimes.arrayPush((float)gestures.leftTimes[k]);

//		this->move_left_arm_to_one_position(leftPoint[k],lHand,time);
	}

	for(int k=0;k<gestures.rightPath.size();k++)
	{
		for(int i=0;i<6;i++)			
			rightPoint[k].arrayPush(gestures.rightPath[k][i]);		

		rightPath.arrayPush(rightPoint[k]);

		rtimes[k] = (float)gestures.rightTimes[k];
		if(k>0) {rtimes[k] = (float)gestures.rightTimes[k] - (float)gestures.rightTimes[k-1];}

		if(k>0 && (float)gestures.rightTimes[k]==(float)gestures.rightTimes[k-1])
			(float)gestures.rightTimes[k]=(float)gestures.rightTimes[k]+0.5;

		rightTimes.arrayPush((float)gestures.rightTimes[k]);
		
//		this->move_right_arm_to_one_position(rightPoint[k],lHand,time);
	}


	// START HERE

	for(int i=0;i<gestures.leftPath.size();i++) 
	{
		this->move_left_arm_to_one_position(leftPoint[i],lHand,ltimes[i]);
		this->move_right_arm_to_one_position(rightPoint[i],lHand,rtimes[i]);


	}

//	this->PlayFile();
//	this->make_motion_right_arm_following_path(rightPath,rightTimes,lHand);

//	this->make_motion_left_arm_following_path(leftPath,leftTimes,lHand);
	
	gestures.leftPath.clear();
	gestures.rightPath.clear();
	gestures.leftTimes.clear();
	gestures.rightTimes.clear();

	//SleepMs(2000); // wait two seconds
	this->PoseInit();
/*
	this->make_motion_left_arm_following_path(path,times,lHand);
	SleepMs(2000); // wait two seconds
	this->PoseInit();

	for(int i=0;i<14;i++)
	{
		for(int j=0;j<6;j++)
			point[i].arrayPush(j+0.05);
		path.arrayPush(point[i]);
	}
	for(int i=0;i<14;i++)
		times.arrayPush(i+0.5);
*/
	/*
	std::vector<vector<float>>::iterator iter; //for(iter=gesture->phasesAssym.begin(); iter!=gesture->phasesAssym.end();iter++)
	for(iter=gestures.leftPath.begin();iter!=gestures.leftPath.end();iter++)
	{			
		int i=0;
		std::vector<float>::iterator it;
		for(it=(*iter).begin();it!=(*iter).end();it++)		
		{	
			i++;
			point[i].arrayPush((float)(*it)+0.001f); 			
		}		
		path.arrayPush(point[i]);		
	}

	
	std::vector<float>::iterator it;
	for(it=gestures.leftTimes.begin();it!=gestures.leftTimes.end();it++)
	{
		times.arrayPush((float)(*it)+0.001f); 		
	}
	*/


	
}

/**********************************************/
NaoController::~NaoController(void)
{
	this->RelaxPose();
	delete motionProxy;
	if(run_on_virtualNAO==0)
	{
		delete audioPlayerProxy;
		delete audioDeviceProxy;
	}

}
