//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorPlanner.cpp: implementation of the BehaviorPlanner class. 
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#include "BehaviorPlanner.h"
#include <iostream>
#include <fstream>
#include <string>

#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/datatypes/stateinfo/UserStateInfo.h>
#include <semaine/cms/message/SEMAINEStateMessage.h>
#include <semaine/datatypes/xml/BML.h>
#include <semaine/datatypes/xml/FML.h>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;


extern IniManager inimanager;

namespace semaine {
namespace components {
namespace greta {

BehaviorPlanner::BehaviorPlanner(const std::string & componentName, bool isInput, bool isOutput) throw(CMSException) :
	Component(componentName,isInput,isOutput)
{
	fmlReceiver = new FMLReceiver("semaine.data.action.selected.speechpreprocessed");
	receivers.push_back(fmlReceiver);
	//bmlReceiver = new BMLReceiver("semaine.data.action.selected.speechpreprocessed");
	//receivers.push_back(bmlReceiver);
	agentStateReceiver = new StateReceiver("semaine.data.state.agent", semaine::datatypes::stateinfo::StateInfo::AgentState);
	receivers.push_back(agentStateReceiver);
	agentReceiver = new StateReceiver("semaine.data.state.context", semaine::datatypes::stateinfo::StateInfo::ContextState);
	receivers.push_back(agentReceiver);
	bmlSender = new BMLSender("semaine.data.synthesis.plan", getName());
	senders.push_back(bmlSender);


	this->start();
}

BehaviorPlanner::~BehaviorPlanner()
{
	delete fmlReceiver;
	//delete bmlReceiver;
	delete agentStateReceiver;
	delete agentReceiver;
	delete bmlSender;
}

void BehaviorPlanner::customStartIO() throw(std::exception) 
{
	// Agent Engine is created once!!
	faae=new FMLAPML_AgentEngine();
	faae->namespaceBML="http://www.mindmakers.org/projects/BML";
	faae->namespaceFML="http://www.mindmakers.org/fml";

	agentname = "PRUDENCE";
	readBaselines();
	iterbml = 0;

}

void BehaviorPlanner::react(SEMAINEMessage * m) throw(CMSException)
{
	std::string bmlstring, bmlstring1;
	std::string inputText;

	//SEMAINEXMLMessage * xm = dynamic_cast<SEMAINEXMLMessage *>(m);
	SEMAINEXMLMessage * xm = (SEMAINEXMLMessage *)(m);
	if (xm == NULL) {
		throw MessageFormatException("expected XML message, got "+std::string(typeid(*m).name()));
	}

	if (m->getDatatype() == "BML") {
		inputText=xm->getText();
		bmlSender->sendXML(xm->getDocument(), meta.getTime(), xm->getEventType(), xm->getContentID(), xm->getContentCreationTime());
	}
	if (m->getDatatype() == "FML") {
		inputText=m->getText();
		if (inputText.find("speech") != -1)
		{
		bmlstring=CalculateFMLAnimation(inputText);
		//XERCES_CPP_NAMESPACE::DOMDocument * doc = XercesTool::parse(bmlstring);
		std::ostringstream out;
		out << xm->getContentID() << "_bml_" << iterbml;
		std::string contentID = out.str();
		bmlSender->sendTextMessage(bmlstring, meta.getTime(), xm->getEventType(), contentID, xm->getContentCreationTime());
		//bmlSender->sendXML(doc, meta.getTime(), xm->getEventType(), contentID, xm->getContentCreationTime());
		iterbml = iterbml + 1;
		} 
		else {
		bmlstring1=CalculateFMLAnimation(inputText);
		//XERCES_CPP_NAMESPACE::DOMDocument * doc1 = XercesTool::parse(bmlstring1);
		std::ostringstream out;
		out << xm->getContentID() << "_bml_" << iterbml;
		std::string contentID = out.str();
		bmlSender->sendTextMessage(bmlstring1, meta.getTime(), xm->getEventType(), contentID, xm->getContentCreationTime());
		//bmlSender->sendXML(doc1, meta.getTime(), xm->getEventType(), contentID, xm->getContentCreationTime());
		iterbml = iterbml + 1;
		}
	} 

	if (m->getDatatype() == "ContextState") 
	{
		SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(m);
		if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*m).name());
		semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
		if (state->hasInfo("character"))
		{
 			agentname = state->getInfo("character");
			inimanager.SetValueString("CHARACTER_SPEAKER",agentname);
/*			if(SALlexicon[agentname]!="")
			{
				faae->getMMS()->BehaviorSets.clear();
				faae->loadMMS("",(char*)SALlexicon[agentname].c_str());
			}
			else
			{
				faae->getMMS()->BehaviorSets.clear();
				faae->loadMMS("",(char*)SALlexicon["lexicon"].c_str());
			}*/
			inimanager.ToUppercase(agentname);
			//printf("%s\n", agentname.c_str());
		}
	}

	if ( m->getDatatype() == "UserState") 
	{
			//SEMAINEStateMessage * sm = dynamic_cast<SEMAINEStateMessage *>(m);
			SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(m);
			if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*m).name());
			semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
			if (state->hasInfo("character"))
			{
				//std::cout << " " << "interest" << "=" << state->getInfo("character");
			}
			if (state->hasInfo("listenerAgreement"))
			{
				//std::cout << " " << "interest" << "=" << state->getInfo("listenerAgreement");
			}
	}

	std::list<GretaLogger*>::iterator iterlog;

	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			log->debug(msg);
		if((*iterlog)->getLevel()=="info")
			log->info(msg);
		if((*iterlog)->getLevel()=="warn")
			log->warn(msg);
		if((*iterlog)->getLevel()=="error")
			log->error(msg);
	}
	listLog.clear();
}

std::string BehaviorPlanner::CalculateFMLAnimation(std::string content)
{
	std::string bml;
	std::string baseline;
	if(agentname!="")
		baseline="mmsystem/" + agentname +".xml";
	else
	{
		//printf("NO SAL CHARACTER DEFINED!!!\n");
		int pos=(int)(inimanager.GetValueString("BASELINE_DEFAULT").find("mmsystem"));
		if (pos!=std::string::npos)
			baseline=inimanager.GetValueString("BASELINE_DEFAULT").substr(pos,inimanager.GetValueString("BASELINE_DEFAULT").length());
		else
			baseline="mmsystem/baseline_default.xml";
	}
	bml=faae->Execute("", baseline.c_str(), (char*)content.c_str(), 1);
	
	return(bml);
}

void BehaviorPlanner::act(SEMAINEMessage * m) throw(CMSException)
{

}

void BehaviorPlanner::readBaselines()
{
	faae->getMMS()->LoadSelectionLexicon("mmsystem/lexicon_Poppy.xml","",&(faae->getMMS()->ActualBehaviorSets["POPPY"]));
	faae->getMMS()->LoadSelectionLexicon("mmsystem/lexicon_Prudence.xml","",&(faae->getMMS()->ActualBehaviorSets["PRUDENCE"]));
	faae->getMMS()->LoadSelectionLexicon("mmsystem/lexicon_Obadiah.xml","",&(faae->getMMS()->ActualBehaviorSets["OBADIAH"]));
	faae->getMMS()->LoadSelectionLexicon("mmsystem/lexicon_Spike.xml","",&(faae->getMMS()->ActualBehaviorSets["SPIKE"]));
}

} // namespace greta
} // namespace components
} // namespace semaine

