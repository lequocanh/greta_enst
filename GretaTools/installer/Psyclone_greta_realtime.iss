[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{2A557CA5-4F6A-4527-B512-4A079FD22546}

; name of the application :
AppName=Greta
; name and version of the application :
AppVerName=Greta realTime
; publisher of Greta :
AppPublisher=Catherine Pelachaud
; name of the directory in "program files" :
DefaultDirName={pf}\Greta_ECA
; name in start menu :
DefaultGroupName=Greta ECA
; file witch contains the license text :
LicenseFile=".\License.txt"
; directory where the installer is build :
OutputDir=".\"
; name of the .exe :
OutputBaseFilename=Greta_RealTime_setup
; compression
Compression=lzma
SolidCompression=yes
; images during setup :
WizardImageFile=".\greta.bmp"
WizardSmallImageFile=".\Greta_Head.bmp"

[Languages]
; languages supported by the install program :
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "basque"; MessagesFile: "compiler:Languages\Basque.isl"
Name: "brazilianportuguese"; MessagesFile: "compiler:Languages\BrazilianPortuguese.isl"
Name: "catalan"; MessagesFile: "compiler:Languages\Catalan.isl"
Name: "czech"; MessagesFile: "compiler:Languages\Czech.isl"
Name: "danish"; MessagesFile: "compiler:Languages\Danish.isl"
Name: "dutch"; MessagesFile: "compiler:Languages\Dutch.isl"
Name: "finnish"; MessagesFile: "compiler:Languages\Finnish.isl"
Name: "french"; MessagesFile: "compiler:Languages\French.isl"
Name: "german"; MessagesFile: "compiler:Languages\German.isl"
Name: "hebrew"; MessagesFile: "compiler:Languages\Hebrew.isl"
Name: "hungarian"; MessagesFile: "compiler:Languages\Hungarian.isl"
Name: "italian"; MessagesFile: "compiler:Languages\Italian.isl"
Name: "norwegian"; MessagesFile: "compiler:Languages\Norwegian.isl"
Name: "polish"; MessagesFile: "compiler:Languages\Polish.isl"
Name: "portuguese"; MessagesFile: "compiler:Languages\Portuguese.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "slovak"; MessagesFile: "compiler:Languages\Slovak.isl"
Name: "slovenian"; MessagesFile: "compiler:Languages\Slovenian.isl"
Name: "spanish"; MessagesFile: "compiler:Languages\Spanish.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Dirs]
; empty directories needed by Greta
Name: "{app}\logs"
Name: "{app}\output"
Name: "{app}\tmp"

[Files]
; bin\bml (only some examples)
Source: "..\..\bin\bml\bml.dtd"; DestDir: "{app}\bml";
Source: "..\..\bin\bml\score.dtd"; DestDir: "{app}\bml";
Source: "..\..\bin\bml\enterface_thierry.xml"; DestDir: "{app}\bml"; DestName: "bml_enterface_motioncapture.xml";
Source: "..\..\bin\bml\bml_example.xml"; DestDir: "{app}\bml";
Source: "..\..\bin\bml\myblog3d.xml"; DestDir: "{app}\bml"; DestName: "bml_myblog3d.xml";
Source: "..\..\bin\bml\thiscity-bml.xml"; DestDir: "{app}\bml"; DestName: "bml_thiscity.xml";
Source: "..\..\bin\bml\bml_threePiecesOfSky.xml"; DestDir: "{app}\bml";
; bin\bn
Source: "..\..\bin\bn\*"; DestDir: "{app}\bn"; Flags: ignoreversion recursesubdirs createallsubdirs
; bin\coart
Source: "..\..\bin\coart\*"; DestDir: "{app}\coart"; Flags: ignoreversion recursesubdirs createallsubdirs
; bin\example
Source: "..\..\bin\example\audio15.pho"; DestDir: "{app}\example";
Source: "..\..\bin\example\audio15.wav"; DestDir: "{app}\example";
Source: "..\..\bin\example\thierry.fap"; DestDir: "{app}\example";
Source: "..\..\bin\example\thierry.wav"; DestDir: "{app}\example";
Source: "..\..\bin\example\thierry_11.fap"; DestDir: "{app}\example";
; bin\fd
Source: "..\..\bin\fd\efe\*"; DestDir: "{app}\fd\efe"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "..\..\bin\fd\mfe\dtd\multimodal.dtd"; DestDir: "{app}\fd\mfe\dtd";
Source: "..\..\bin\fd\mfe\example.xml"; DestDir: "{app}\fd\mfe";
Source: "..\..\bin\fd\facelibrary.dtd"; DestDir: "{app}\fd";
Source: "..\..\bin\fd\facelibrary.xml"; DestDir: "{app}\fd";
; bin\fml (only some examples)
Source: "..\..\bin\fml\fml-apml.dtd"; DestDir: "{app}\fml";
Source: "..\..\bin\fml\fml-apml_what.xml"; DestDir: "{app}\fml"; DestName: "fml_what.xml";
Source: "..\..\bin\fml\fml-apml_when sorrows come.xml"; DestDir: "{app}\fml"; DestName: "fml_when sorrows come.xml";
Source: "..\..\bin\fml\fml-complex.xml"; DestDir: "{app}\fml"; DestName: "fml_complex.xml";
Source: "..\..\bin\fml\fml-pad.xml"; DestDir: "{app}\fml"; DestName: "fml_pad.xml";
Source: "..\..\bin\fml\szechuan.xml"; DestDir: "{app}\fml"; DestName: "fml_szechuan.xml";
Source: "..\..\bin\fml\thiscity.xml"; DestDir: "{app}\fml"; DestName: "fml_thiscity.xml";
; bin\gestures (no sub dirs)
Source: "..\..\bin\gestures\*"; DestDir: "{app}\gestures";
; bin\head
Source: "..\..\bin\head\*"; DestDir: "{app}\head";
; bin\listener
Source: "..\..\bin\listener\*"; DestDir: "{app}\listener"; Flags: ignoreversion recursesubdirs createallsubdirs
; bin\mmsystem
Source: "..\..\bin\mmsystem\xsd\*"; DestDir: "{app}\mmsystem\xsd";
;Source: "..\..\bin\mmsystem\baseline1.xml"; DestDir: "{app}\mmsystem";
;Source: "..\..\bin\mmsystem\baseline2.xml"; DestDir: "{app}\mmsystem";
;Source: "..\..\bin\mmsystem\baseline3.xml"; DestDir: "{app}\mmsystem";
;Source: "..\..\bin\mmsystem\baseline4.xml"; DestDir: "{app}\mmsystem";
;Source: "..\..\bin\mmsystem\baseline5.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\baseline_default.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\baseline_zero.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\behaviorqualifiers.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\behaviorqualifiers_zero.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\lexicon.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\NEGATIVE.xml"; DestDir: "{app}\mmsystem";
Source: "..\..\bin\mmsystem\POSITIVE.xml"; DestDir: "{app}\mmsystem";
; bin\Ogre
Source: "..\..\bin\Ogre\greta.xml"; DestDir: "{app}\Ogre";
Source: "..\..\bin\Ogre\character.dtd"; DestDir: "{app}\Ogre";
Source: "..\..\bin\Ogre\DLL\Plugin_OctreeSceneManager.dll"; DestDir: "{app}\Ogre\DLL";
Source: "..\..\bin\Ogre\DLL\RenderSystem_Direct3D9.dll"; DestDir: "{app}\Ogre\DLL";
Source: "..\..\bin\Ogre\DLL\RenderSystem_GL.dll"; DestDir: "{app}\Ogre\DLL";
Source: "..\..\bin\Ogre\media\body_materials1.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\body_materials2.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\body_materials3.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\body_materials4.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\body_woman.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\clouds.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\eye.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\eyelid.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\glottis.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\glottis.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\green_eye.jpg"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\greta_animated.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\greta_hair.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\inf_jaw.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\materials.material"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\sup_jaw.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\tongue.mesh"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\woman.skeleton"; DestDir: "{app}\Ogre\media";
Source: "..\..\bin\Ogre\media\terrain\*"; DestDir: "{app}\Ogre\media\terrain";
; bin\poses
Source: "..\..\bin\poses\*"; DestDir: "{app}\poses"; Flags: ignoreversion recursesubdirs createallsubdirs
; bin\torso
Source: "..\..\bin\torso\*"; DestDir: "{app}\torso"; Flags: ignoreversion recursesubdirs createallsubdirs
; bin\utils
Source: "..\..\bin\utils\WAIT.exe"; DestDir: "{app}\utils";
; bin
Source: "..\..\bin\complexemotions.xml"; DestDir: "{app}";
;Source: "..\..\bin\cv100.dll"; DestDir: "{app}";
;Source: "..\..\bin\cvaux100.dll"; DestDir: "{app}";
;Source: "..\..\bin\cxcore100.dll"; DestDir: "{app}";
;Source: "..\..\bin\cxts001.dll"; DestDir: "{app}";
;Source: "..\..\bin\darken.dll"; DestDir: "{app}";
Source: "..\..\bin\empty.bap"; DestDir: "{app}";
Source: "..\..\bin\empty.fap"; DestDir: "{app}";
Source: "..\..\bin\empty.wav"; DestDir: "{app}";
Source: "..\..\bin\greta_psyclone.ini"; DestDir: "{app}";
;Source: "..\..\bin\highgui100.dll"; DestDir: "{app}";
Source: "..\..\bin\HPTS++.dll"; DestDir: "{app}";
Source: "..\..\bin\iconv.dll"; DestDir: "{app}";
;Source: "..\..\bin\ipfDll1.dll"; DestDir: "{app}";
;Source: "..\..\bin\ipfDll2.dll"; DestDir: "{app}";
;Source: "..\..\bin\ipfDll3.dll"; DestDir: "{app}";
;Source: "..\..\bin\libeay32.dll"; DestDir: "{app}";
;Source: "..\..\bin\libguide40.dll"; DestDir: "{app}";
Source: "..\..\bin\libxml2.dll"; DestDir: "{app}";
;Source: "..\..\bin\license.txt"; DestDir: "{app}";
;Source: "..\..\bin\ml100.dll"; DestDir: "{app}";
Source: "..\..\bin\OgreCamera.ini"; DestDir: "{app}";
Source: "..\..\bin\OgreMain.dll"; DestDir: "{app}";
Source: "..\..\bin\Plugins.cfg"; DestDir: "{app}";
Source: "..\..\bin\POSE_GAZE.txt"; DestDir: "{app}";
;Source: "..\..\bin\pthreadGC2.dll"; DestDir: "{app}";
Source: "..\..\bin\README.txt"; DestDir: "{app}";
;Source: "..\..\bin\ruthSocketClient.dll"; DestDir: "{app}";
Source: "..\..\bin\SDL.dll"; DestDir: "{app}";
Source: "..\..\bin\SDL_net.dll"; DestDir: "{app}";
;Source: "..\..\bin\ssleay32.dll"; DestDir: "{app}";
Source: "..\..\bin\xerces-c_3_0.dll"; DestDir: "{app}";
Source: "..\..\bin\zlib1.dll"; DestDir: "{app}";
; realtime applications
Source: "..\..\bin\Psyclone_Clock.exe"; DestDir: "{app}"; DestName: "Clock.exe";
Source: "..\..\bin\Psyclone_BehaviorPlanner.exe"; DestDir: "{app}"; DestName: "BehaviorPlanner.exe";
Source: "..\..\bin\Psyclone_BehaviorRealizer.exe"; DestDir: "{app}"; DestName: "BehaviorRealizer.exe";
Source: "..\..\bin\Psyclone_Player.exe"; DestDir: "{app}"; DestName: "Player.exe";
Source: "..\..\bin\generic_bat_for_installer(do not delete).bat"; DestDir: "{app}"; DestName: "Greta_RealTime.bat";
;psyclone
Source: "..\..\bin\psyclone\GretaEVOSpec.xml"; DestDir: "{app}\psyclone";
Source: "..\..\bin\psyclone\readme.txt"; DestDir: "{app}\psyclone";
Source: "..\..\bin\psyclone\run.bat"; DestDir: "{app}\psyclone";
Source: "..\..\bin\psyclone\LICENSE.txt"; DestDir: "{app}\psyclone";
Source: "..\..\bin\psyclone\psyclone.exe"; DestDir: "{app}\psyclone";
Source: "..\..\bin\psyclone\QUICKSTART.txt"; DestDir: "{app}\psyclone";
Source: "..\..\bin\psyclone\readme.txt"; DestDir: "{app}\psyclone";
;interface
Source: "..\..\GretaAuxiliary\interface\dist\*"; DestDir: "{app}\BML_FML_LOADER";

[Icons]
; creates a shortcut named "Greta" to Greta.exe in "start" menu :
Name: "{group}\Greta RealTime"; Filename: "{app}\Greta_RealTime.bat"; WorkingDir: "{app}"
; creates a shortcut to uninstall Greta :
Name: "{group}\{cm:UninstallProgram,Greta RealTime}"; Filename: "{uninstallexe}"
; creates a shortcut named "Greta" to Greta.exe on desktop :
Name: "{commondesktop}\Greta RealTime"; Filename: "{app}\Greta_RealTime.bat"; WorkingDir: "{app}"; Tasks: desktopicon
; creates a shortcut named "Greta" to Greta.exe on the quick launch menu :
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Greta RealTime"; Filename: "{app}\Greta_RealTime.bat";WorkingDir: "{app}"; Tasks: quicklaunchicon

[Run]
; to start Greta.exe at the end of setup :
Filename: "{app}\Greta_RealTime.bat"; Description: "{cm:LaunchProgram,Greta}"; Flags: nowait postinstall skipifsilent unchecked

