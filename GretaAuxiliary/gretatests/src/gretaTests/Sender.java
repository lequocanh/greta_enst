/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gretaTests;

/**
 *
 * @author Andre
 */
public interface Sender {

    /**
     * Tries to connect to a specific whitBoard if it is not already the case.<br/>
     * (please, don't crash the programme)
     */
    public void connect();
    /**
     * Says if this sender is connected to a whiteboard
     * @return if this sender is connected
     */
    public boolean isConnected();
    /**
     * Sends the BML text to a whiteboard
     * @param BML the BML text to send
     */
    public void sendBML(String BML);
    /**
     * Sends the FML text to a whiteboard
     * @param FML the FML text to send
     */
    public void sendFML(String FML);

}
