// Shared-Attention system
// Christopher Peters

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <math.h>

#include "CommunicationMessage.h"
#include "GazeDetectorPsydule.h"
#include "IniManager.h"
#include "XMLGenericParser.h"
#include "SDL_thread.h"
#include "SDL_net.h"
#include "RandomGen.h"
#include "EngineParameter.h"

extern IniManager inimanager;
RandomGen *randomgen=0;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

GazeDetectorPsydule::GazeDetectorPsydule(std::string name, std::string host, int port) 
{
	//important line
	//commint = new Module(name, host, port);
	commint = new Psydule(name, host, port);

	printf("\nRegistering GazeDetector module (%s) with Psyclone server %s: %d ...", name.c_str(), host.c_str(), port);
	this->name=name;
	this->host=host;
	this->port=port;

	//this->GretaName=commint->getGretaName(GretaName);

	this->connected = false;
	//regaister the main psydule connection
	this->registerPsyduleConnection();
}

GazeDetectorPsydule::~GazeDetectorPsydule()
{
	this->sendAdminMessage("GazeDetectorPsydule.Shutdown");
}

void GazeDetectorPsydule::checkForMessage(int timeout)
{
	if (!connected) return;
	CommunicationMessage *msg = commint->ReceiveMessage(timeout);
	std::string content;
	double p;

	if(msg==NULL) return;

	content=msg->getContent();
	printf("\nGazeDetectorPsydule Recieved message with content %s", content.c_str());
}

void GazeDetectorPsydule::registerPsyduleConnection(void)
{
	std::list<std::string> datatypes;
	datatypes.push_back("Greta.Admin.Gaze");
	datatypes.push_back("Greta.Data.GazeRaw");
	datatypes.push_back("Greta.Data.GazeSCoords");
	
	if(commint->Register("Greta.Whiteboard",datatypes)!=0)
	{
		printf("\nRegistration successful!\n");
		this->connected = true;
		this->sendAdminMessage("GazeDetectorPsydule.Startup");
	}
	else
	{
		printf("\nError: Could not register to Psyclone\n");
		printf("\nPlease ensure that the Psyclone server is running\n");
		this->connected = false;
		//while(1);		
	}
}

void GazeDetectorPsydule::sendAdminMessage(std::string msg)
{
	if (this->connected) commint->PostString(msg, "Greta.Whiteboard", "Greta.Admin.Gaze");
	else printf("\nUnable to send Psyclone message: not connected");
}

std::string ftoa(const float x) {
  std::ostringstream o;
  if (!(o << x)) return "ERROR";
  return o.str();
}

std::string itoa(const int x) {
  std::ostringstream o;
  if (!(o << x)) return "ERROR";
  return o.str();
}

/*void GazeDetectorPsydule::sendRawGazeMessage(float ppx, float ppy)
{
	std::string xStr = ftoa(ppx);
	std::string yStr = ftoa(ppy);
	std::string msg = xStr + " " + yStr;
	this->PostString(msg, "Greta.Whiteboard", "Greta.Data.GazeRaw");
	printf("\nSend gaze head dir message %lf %lf", ppx,ppy);
}*/

void GazeDetectorPsydule::sendGazeHeadDirMessage(float headxDir, float headyDir, float dist)
{
	printf("\nSending gaze coords message %lf %lf", headxDir,headyDir);
	if (!connected) 
	{
		printf("\nUnable to send Psyclone message: not connected");
		return;
	}
	//std::string exStr = itoa(eyex);
	//std::string eyStr = itoa(eyey);
	std::string hxStr = ftoa(headxDir);
	std::string hyStr = ftoa(headyDir);
	std::string dStr = ftoa(dist);
	std::string msg = hxStr + " " + hyStr + " " + dStr; //exStr + " " + eyStr + " " + 
	commint->PostString(msg, "Greta.Whiteboard", "Greta.Data.GazeHeadDir");
}