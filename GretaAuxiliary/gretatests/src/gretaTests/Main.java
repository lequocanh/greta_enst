/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gretaTests;

/**
 *
 * @author Andre
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestFrame tf = new TestFrame();
        //tf.senders.add(new PrintSender());
        tf.senders.add(new PsycloneSender());
        tf.connect();
        tf.setVisible(true);
    }

}
