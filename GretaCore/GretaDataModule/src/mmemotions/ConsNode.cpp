//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ConsNode.cpp: implementation of the ConsNode class.
//
//////////////////////////////////////////////////////////////////////

/*
Info - the node can be : 

- two argment operator :

left!=null, right!=null, operator!=unknown
value=-1, start=0, end=0, type=unknown, arg_id=-1

- one argument operator :

left!=null, right==null, operator={exist,lessthan,morethan,not,equal}
value=-1, start=0, end=0, type=unknown, arg_id=-1


- id agrument :

left==null, right==null, operator==unknown
value=-1, start>0, end>0, , arg_id!=-1
type = unknown if operator one level up is two arg
type= {start,stop} of operator one level up is one arg

- number agrument :

left==null, right==null, operator==unknown
value!=-1, start=0, end=0, 
type=unknown, arg_id=-1

*/

#include "ConsNode.h"

ConsNode::ConsNode()
{

	this->start_time = -1.0;
	this->stop_time = -1.0 ;

	this->left = NULL;
	this->right = NULL;

	//if id =-1 means no argument only value
	this->arg_id=-1;

	this->my_type = none;
	this->my_oper = unknown;

	this->value=-1;	

}

ConsNode::~ConsNode()
{
	//TO DO : destroy subtrees
}


ConsNode* ConsNode::clone()
{

	ConsNode* temp = new ConsNode();
	
	//rekursja

	//ConsNode *left;
	if (this->left!=NULL) temp->setLeft( (this->left)->clone()) ;
	else temp->setLeft(NULL);

	//ConsNode *right; 
	if (this->right!=NULL) temp->setRight( (this->right)->clone()) ;
	else temp->setRight(NULL);

	//arg_id;	
	temp->setArg(this->arg_id);
		
	//float start_time;
	temp->setStartTime(this->start_time);

	//float stop_time;
	temp->setStopTime(this->stop_time);
	
	//float value;
	temp->setValue(this->value);

	//type my_type;
	temp->setArgType(this->my_type);

	//oper my_oper;
	temp->setConsNodeOperator( this->getConsNodeOperatorToString() );

	//copy concerns list
	if (!(this->concerns.empty() ) )
	
	{
		// all the id's of the cons	
		std::vector<int>::iterator concerns_iter;
			
		for(concerns_iter = this->concerns.begin(); concerns_iter!=this->concerns.end();concerns_iter++) 		
			temp->concerns.push_back(*concerns_iter);
	}
	
	return temp;

}

//the other exists in animation 
//if means that consnode needs also the current animation state

int ConsNode::exists(ConsNode* other)
{
    //ignore if contraditory
	if (this->arg_id==other->arg_id) return 1;

	if (other==NULL) return 0;
	
	//if this element has defined its start and end time it probably(?) means that it exists
	if ((other->getStartTime()>=0)&&(other->getStopTime()>0)) return 1;

	return 0;
}


//the other starts before this
//it should be called: isPreceded

int ConsNode::isPreceded(ConsNode* other){

	//other does not exist!
	if ((other->getStartTime()<0)||(other->getStopTime()<=0)) return 1;
	
	//I do not exist!
	if ((start_time<0)||(stop_time<0)) return 1;

	//if both conditions  (stop_time>0) and  && (stop_time=0) together - no difference

	if ( (other->getStartTime()>=0)&&(other->getStopTime()>0) && (start_time>=0) )
	{		
		
		// if other starts after I start - no satisfied
		if ( other->getStartTime() > start_time ) return 0;		

		//if other stops after I start - no satisfied
		if ( (other->getStartTime() + other->getStopTime() ) > start_time) return 0;

	}



	//satisfied by default
	return 1;
}


//this is bigger than other
//it should be called: isIncluded

int ConsNode::isIncluded(ConsNode* other){

	//ignore if contraditory
	if (this->arg_id==other->arg_id) return 1;

	//other does not exist!
	if ( (other->getStartTime()<0)||(other->getStopTime()<=0) ) return 1;
	
	//I do not exist! 		
	if ((start_time<0)||(stop_time<0)) return 1;
	
	//without the end I do not know if Im included or not

	//if both exist
	if ( (other->getStartTime()>=0)&&(other->getStopTime()>0) && (start_time>=0) && (stop_time>0))
	{		
		
		// if other starts after I start - no satisfied
		if (other->getStartTime()>start_time) return 0;		

		//if other stops before I stop - no satisfied
		if ( (other->getStartTime() + other->getStopTime()) < (start_time+stop_time)  ) return 0;

	}

	//if I only start
	if ( (other->getStartTime()>=0)&&(other->getStopTime()>0) && (start_time>=0) && (stop_time==0))
	{		
		
		// if other starts after I start - no satisfied
		if (other->getStartTime()>start_time) return 0;		

		// if other stops before I start - no satisfied
		if ( (other->getStartTime() + other->getStopTime() )  < start_time) return 0;		

	}

	//satisfied by default
	return 1;
}


// this and no other

int ConsNode::excludes(ConsNode* other){

	//other does not exist!
	if ( (other->getStartTime()<0) || (other->getStopTime()<=0) ) return 1;
	
	//I do not exist! 	
	if ((start_time<0)||(stop_time<0)) return 1;
	
	//if both exist
	if ( (other->getStartTime()>=0)&&(other->getStopTime()>0) && (start_time>=0) && (stop_time>0))
	{
	
		//start time is in other
		if ( (other->getStartTime()<start_time) && (start_time < ( other->getStartTime() + other->getStopTime() ) ) ) return 0;

		//stop time is in other
		if ((other->getStartTime()< (start_time + stop_time) ) && ( (start_time + stop_time) < (other->getStartTime() + other->getStopTime() ) ) ) return 0;
	
	}

	//if one exists
	if ( (other->getStartTime()>=0) && (other->getStopTime()>0) && (start_time>=0) && (stop_time==0) )
	{
	
		//start time is in other
		if ( (other->getStartTime()<start_time) && (start_time < ( other->getStartTime() + other->getStopTime() ) ) ) return 0;

	
	}


	return 1;
}

//the other starts before this
//it should be called: isLeftIncluded

int ConsNode::isLeftIncluded(ConsNode* other){

	//ignore if contraditory
	if (this->arg_id==other->arg_id) return 1;


	//other does not exist!
	if ((other->getStartTime()<0)||(other->getStopTime()<=0)) return 1;
	
	//I do not exist!
	if ((start_time<0)||(stop_time<0)) return 1;

	//if both exist
	if ( (other->getStartTime()>=0)&&(other->getStopTime()>0) && (start_time>=0) && (stop_time>0) )
	{		
		
		// if other starts after I start - no satisfied
		if (other->getStartTime() > start_time) return 0;		

		//if other stops before I start - no satisfied
		if ( (other->getStartTime() + other->getStopTime() ) < start_time) return 0;


		//if I stop before the other stops - no satisfied  - maybe not used ???
		if ( (other->getStartTime() + other->getStopTime()) > (start_time+stop_time)  ) return 0;

	}


	if ( (other->getStartTime()>=0)&&(other->getStopTime()>0) && (start_time>=0) && (stop_time==0) )
	{		
		
		// if other starts after I start - no satisfied
		if (other->getStartTime() > start_time) return 0;		

		//if other stops before I start - no satisfied
		if ( (other->getStartTime() + other->getStopTime() ) < start_time) return 0;

	}

	//par default
	return 1;
}

//enum oper {unknown,rigthinc,include,exclude,precede,exist, morethan,lessthan,equal, and, or, not};

void ConsNode::setConsNodeOperator(std::string type1)
{
	//par default unkown
	this->my_oper = unknown;

	//change it
	if (type1== "rightinc" ) this->my_oper = rigthinc;
	if (type1== "included" ) this->my_oper = include;
	if (type1== "excludes" ) this->my_oper = exclude;
	if (type1== "preceded" ) this->my_oper = precede;
	if (type1== "exists") this->my_oper = exist;
	if (type1== "morethan" ) this->my_oper = morethan;
	if (type1== "lessthan" ) this->my_oper = lessthan;
	if (type1== "equal" ) this->my_oper = equal;
	if (type1== "and" ) this->my_oper = and;
	if (type1== "or" ) this->my_oper = or;
	if (type1== "not" ) this->my_oper = not;
}

std::string ConsNode::getConsNodeOperatorToString()
{
	
	if (this->my_oper == rigthinc) return "rightinc";
	if (this->my_oper == include) return "included";
	if (this->my_oper == exclude) return "excludes";
	if (this->my_oper == precede) return "preceded"; 
	if (this->my_oper == exist)  return "exists";
	if (this->my_oper == morethan) return "morethan";
	if (this->my_oper == lessthan) return "lessthan";
	if (this->my_oper == equal) return "equal";
	if (this->my_oper == and)  return "and";
	if (this->my_oper == or)  return "or" ; 
	if (this->my_oper == not) return "not";

	return "unknown";
}



oper ConsNode::getConsNodeOperator()
{	
	return this->my_oper;
}

void ConsNode::setArg(int id1, std::string type1)
{
	this->arg_id=id1;

	//sprawdzic
	if (type1 == "start" ) this->my_type = start;
	if (type1 == "stop" ) this->my_type = end;	
	if (type1 == "none" ) this->my_type = none;	
}

void ConsNode::setArg(int id1)
{
	this->arg_id=id1;
	this->my_type = none;
	
}

int ConsNode::getArgId()
{
	return this->arg_id;	
}


//ConsNode::getArgType()
//{
//	if (this->my_type == start) return 0;
//	if (this->my_type == end) return 1;
//
//	return -1;
//}


std::string ConsNode::getArgType()
{	

	if (this->my_type == start) return "start";
	if (this->my_type == end) return "stop";

	return "";
}


void ConsNode::setValue(float value1)
{
	this->value=value1;
}

float ConsNode::getValue()
{
	return this->value;
}

int ConsNode::evaluate(int id,std::vector<ShortSignal*>* temp_animation)
{

	if ((this->left != NULL) && (this->right != NULL)) {

		//composed rule by an logical oporator

		//in this case sons are not final nodes

		if (this->my_oper == and ) return this->left->evaluate(id,temp_animation) && this->right->evaluate(id,temp_animation);
		if (this->my_oper == or ) return this->left->evaluate(id,temp_animation) || this->right->evaluate(id,temp_animation);

		// composed by any of other two

		//in this case sons are final nodes

		if (this->my_oper == rigthinc)
		{			
			//check do I have to disintguish left from the right

			if (this->left->getArgId()==id) return this->left->isLeftIncluded(this->right);			
			
			// should be something about left included, maybe
			//if (this->right->getArgId()==id) return this->right->isLeftIncluded(this->left);

			if (this->right->getArgId()==id){

				//if left has to preceded the right
				// and the left does exist
				//then the rule is false
				if ( (this->left->getStartTime()>=0) &&( this->left->getStopTime()>0) ) 
					return 0;
				//left does not exist thus element may be added
				//it does not exist becouse the element is empty, but it should be filled with the last apperance of the epression of this id
				else return 1;
			}
			return 0;
		}

		if (this->my_oper == include)
		{
			
			if (this->left->getArgId()==id) return  this->left->isIncluded(this->right);
			
			//deadlock
			//if (this->right->getArgId()==id) return this->right->isIncluded(this->left);

			if (this->right->getArgId()==id){

				//if left has to be included in the right
				// and the left does exist then the rule is false becouse the right cannot embrace the left (is too late)
				//otherwise no problem for right to occur

				//TO DO: but if the left is finished then maybe we can add a new right element?
				if ( 
					(this->left->getStartTime()>=0) &&
					( this->left->getStopTime()>0) &&
					( ( this->left->getStartTime() + this->left->getStopTime() ) < this->right->getStartTime() ) 
				   ) return 1;

				if ( (this->left->getStartTime()>=0) &&( this->left->getStopTime()>0)) 
					return 0;
				//left does not exist thus element may be added
				//it does not exist becouse the element is empty, but it should be filled with the last apperance of the epression of this id
				else return 1;
			}
			
			return 0;
		}

		//the only one that is symmetric
		if (this->my_oper == exclude)
		{

			//ok
			if (this->left->getArgId()==id) return this->left->excludes(this->right);
			if (this->right->getArgId()==id) return this->right->excludes(this->left);

			return 0;
		}

		if (this->my_oper == precede)
		{

			if (this->left->getArgId()==id) return  this->left->isPreceded(this->right);
			
			// should be something about left included, maybe
			//if (this->right->getArgId()==id) return this->right->isPreceded(this->left);

			if (this->right->getArgId()==id){

				//if left has to preceded the right
				// and the left does exist
				//then the rule is false
				if ( (this->left->getStartTime()>=0) &&( this->left->getStopTime()>0)) 
					return 0;
				//left does not exist thus element may be added
				//it does not exist becouse the element is empty, but it should be filled with the last apperance of the epression of this id
				else return 1;
			}

			return 0;
		}


		// WHAT IF y STOP is 0 

		if (this->my_oper == morethan)
		{
			//check is it right to change the operator
			//if (this->left->getArgId()==id) return  this->left->morethan(this->right);
			//if (this->right->getArgId()==id) return this->right->lessthan(this->left);

			float arg_a=0;
			float arg_b=0;

			if (this->left->my_type == start) arg_a=this->left->start_time;

			if ( (this->left->my_type == end) && (this->left->stop_time>0) ) arg_a =  (this->left->start_time + this->left->stop_time);
			if ( (this->left->my_type == end) && (this->left->stop_time==0) ) return 1; //cannot be analized

			if (this->left->my_type == none) arg_a=this->left->value;

			if (this->right->my_type == start) arg_b=this->right->start_time;
			if ( (this->right->my_type == end)  && (this->right->stop_time>0) ) arg_b = (this->right->start_time + this->right->stop_time);
			if ( (this->right->my_type == end)  && (this->right->stop_time==0) ) return 1; //cannot be analized

			// thus the value is absolute
			// start = 0
			// stop = 10 - i.e. it has to stop in 10 sec and it does not mean that its duration is 10 sec
			if (this->right->my_type == none) arg_b=this->right->value;


			//value unknown (return 2);
			if (arg_a==-1) return 1;
			if (arg_b==-1) return 1;


			//check the condition;
			if (arg_a > arg_b) return 1;

			return 0;
		}

		if (this->my_oper == lessthan)
		{

			//check is it right to change the operator
			//if (this->left->getArgId()==id) return  this->left->morethan(this->right);
			//if (this->right->getArgId()==id) return this->right->lessthan(this->left);

			float arg_a=0;
			float arg_b=0;

			if (this->left->my_type == start) arg_a=this->left->start_time;

			if ( (this->left->my_type == end) && (this->left->stop_time>0) ) arg_a= (this->left->start_time + this->left->stop_time);
			if ( (this->left->my_type == end) && (this->left->stop_time==0) ) return 1; //cannot be analized

			if (this->left->my_type == none) arg_a = this->left->value;

			if (this->right->my_type == start) arg_b = this->right->start_time;
			if ( (this->right->my_type == end)  && (this->right->stop_time>0) ) arg_b = (this->right->start_time + this->right->stop_time);
			if ( (this->right->my_type == end)  && (this->right->stop_time==0) ) return 1; //cannot be analized

			// thus the value is absolute
			// start = 0
			// stop = 10 - i.e. it has to stop in 10 sec and it does not mean that its duration is 10 sec
			if (this->right->my_type == none) arg_b = this->right->value;

			//value unknown (return 2);
			if (arg_a==-1) return 1;
			if (arg_b==-1) return 1;


			//check the condition;
			if (arg_a < arg_b) return 1;


			return 0;
		}

		if (this->my_oper == equal)
		{		

			//if (this->left->getArgId()==id) return  this->left->equal(this->right);
			//if (this->right->getArgId()==id) return this->right->equal(this->left);

			float arg_a=0;
			float arg_b=0;

			if (this->left->my_type == start) arg_a=this->left->start_time;

			if ( (this->left->my_type == end) && (this->left->stop_time>0) ) arg_a = (this->left->start_time + this->left->stop_time);
			if ( (this->left->my_type == end) && (this->left->stop_time==0) ) return 1; //cannot be analized

			if (this->left->my_type == none) arg_a=this->left->value;

			if (this->right->my_type == start) arg_b=this->right->start_time;
			if ( (this->right->my_type == end)  && (this->right->stop_time>0) ) arg_b = (this->right->start_time + this->right->stop_time);
			if ( (this->right->my_type == end)  && (this->right->stop_time==0) ) return 1; //cannot be analized

			// thus the value is absolute
			// start = 0
			// stop = 10 - i.e. it has to stop in 10 sec and it does not mean that its duration is 10 sec
			if (this->right->my_type == none) arg_b=this->right->value;

			//value unknown (return 2);
			if (arg_a==-1) return 1;
			if (arg_b==-1) return 1;

			//check the condition;
			
			//TODO : should plus minus = - otherwise it will dificult that happens
			if (arg_a - arg_b < 0.5) return 1;
			if (arg_b - arg_a < 0.5) return 1;

			return 0;
		}

		//the empty node is not possible if there are two arguments 
		//the empty node of the type arg is possible if there is only one son called cons

		//is it possible??? no it is not possible
		//if (this->oper1 == not) return  !(this->left->evaluate());



	}// and of if


	if ( (this->left != NULL) && (this->right == NULL) ) {

		//NoT
		if (this->my_oper == not)  return  ! ( this->left->evaluate(id,temp_animation) );

		//EXIST

		if (this->my_oper == exist) {			

			//ignore nosense
			if (this->left->arg_id == id) return 1;

			// loop all the list of mme			
			std::vector<ShortSignal*>::const_iterator iter;

			for(iter=temp_animation->begin();iter!=temp_animation->end();iter++)
			{			
				//is there there this->left->arg_id which has this->left->start> 0 and this->left->end > 0
				
				//this consnode is operator exists
				//his son is an argument of exist that why this-> left-> arg_id

				//bulls
				if ( (*iter)->id==this->left->arg_id ) return 1;
			
			}//end of for

			//otherwise false

			return 0;
		}

		// empty argument to be evaluated at lower level

		if (this->my_oper == unknown) return this->left->evaluate(id,temp_animation);
	
	}//end of left null rihgt null

	//NOT POSSIBLE!!!!
	//if (left == null) && (right != null) return ;

	if ((this->left == NULL) && (this->right == NULL))
	{

		//nothing ???

	}//end of two nulls


	//otherwise fails

	return 0;

}//end of method



void ConsNode::fill_left(int id,float start_time, float stop_time){

	if ((this->left==NULL) && (this->right==NULL))
	{

		//check if the node corresponds to 
		if (this->arg_id==id) {
			this->start_time = start_time;
			this->stop_time = stop_time;
		}

	}
	if (this->left!=NULL) left->fill_left(id,start_time,stop_time);

	//is it possible?
	if (this->right!=NULL) right->fill_left(id,start_time,stop_time);

}

void ConsNode::fill_right(int id,float start_time, float stop_time){

	if ((this->left==NULL) && (this->right==NULL))
	{

		//check if the node corresponds to 
		if (this->arg_id==id) {
			this->start_time = start_time;
			this->stop_time = stop_time;
		}

	}

	//Not possible
	//if (this->left!=null) left->fill_right(id,start_time,stop_time);

	if (this->right!=NULL) right->fill_right(id,start_time,stop_time);

}

void ConsNode::fill(int id,float start_time, float stop_time){

	if ((this->left==NULL) && (this->right==NULL))
	{

		//check if the node corresponds to 
		if (this->arg_id==id) {
			this->start_time = start_time;
			this->stop_time = stop_time;

		}

		return;
	}
	
	if (this->left!=NULL) left->fill(id,start_time,stop_time);
	if (this->right!=NULL) right->fill(id,start_time,stop_time);

}


void ConsNode::clean(){

	this->start_time = -1.0;
	this->stop_time = -1.0;
			
	if (this->left!=NULL) left->clean();
	if (this->right!=NULL) right->clean();

}


//not used????
bool ConsNode::booleanop(bool l, oper oper1 , bool r)
{

	//if (oper1 == "and") return l && r;
	//if (oper1 == "or") return l || r;
	//if (oper1 == "not") return l || r;

	return 0;

}
