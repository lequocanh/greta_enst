//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// RealtimeFML.h: interface for the RealtimeFML class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <vector>
#include <time.h>
#include "CommunicationMessage.h"
#include "CentralClock.h"
#include "FML-APML_AgentEngine.h"
#include "Psydule.h"



class RealtimeFML : public Psydule
{
public:
	RealtimeFML(std::string name, std::string host, int port, std::string GretaName);
	virtual ~RealtimeFML();

    
	int RealtimeFML::ReadMessage(CommunicationMessage *msg);
	std::string RealtimeFML::CalculateFMLAnimation(std::string content);
	void RealtimeFML::SendBML(std::string bml);

private:

	// Computer central clock
	CentralClock *pc;
	FMLAPML_AgentEngine *faae;

	std::string agentname;
	std::string GretaName;
	
};

