#pragma once

#include <string>
#include <vector>

class CapturedPoint
{

public:

	std::string label;
	float x;
	float y;
	float z;

	CapturedPoint();
	CapturedPoint(std::string label);
	CapturedPoint(float x,float y,float z, std::string label);
	CapturedPoint(float x,float y,float z);
	virtual ~CapturedPoint(){}

private:

 }
;


