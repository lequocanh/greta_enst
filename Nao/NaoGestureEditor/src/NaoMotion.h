#pragma once

#include <stdio.h>
#include "altypes.h"
#include "alptr.h"
#include <alproxy.h>
#include <conio.h>
#include <alerror.h>
#include <albroker.h>


using namespace std;

#define PI 3.14159265358979f

#define AXIS_MASK_X 1
#define AXIS_MASK_Y 2
#define AXIS_MASK_Z 4
#define AXIS_MASK_WX 8
#define AXIS_MASK_WY 16
#define AXIS_MASK_WZ 32

#define INTERPOLATION_TYPE 1
#define	SECONDS 2
#define	MASK  7
#define	SPACE 2 // Task space {SPACE_TORSO = 0, SPACE_WORLD = 1, SPACE_SUPPORT_LEG = 2 }

class NaoMotion 
{
  struct wavheader
  {
        char ChunkID[4]; // 32 bits
        long ChunkSize;  // 32 bits
        char Format[4];  // 32 bits
        char Subchunk1ID[4]; // 32 bits
        long Subchunk1Size; // 32 bits
        unsigned short AudioFormat; // 16 bits
        unsigned short NumChannels; // 16 bits
        unsigned long SampleRate; // 32 bits
        unsigned long ByteRate; // 32 bits
        unsigned short BlockAlign; // 16 bits
        unsigned short BitsPerSample; // 16 bits
        char Subchunk2ID[4]; // 32 bits
        long Subchunk2Size;  // 32 bits

  };

public:
	NaoMotion();
	NaoMotion(std::string parentBrokerIP, int parentBrokerPort);
	~NaoMotion(void);

private:
	std::string parentBrokerIP;
	int parentBrokerPort;

	AL::ALProxy *proxy;

	AL::ALProxy *audioProxy;

	AL::ALProxy *ad;

	AL::ALProxy *ALTextToSpeechProxy;

	AL::ALProxy *ALFrameManagerProxy;

	vector<float> ZeroPosition;
	vector<float> InitPosition;

	float joints[5][7][3][4];
public:
	void Init();
	void LoadArmPositions();
	void SetIP(std::string ip);
	void Setport(int port);
	void StiffnessOn();
	void StiffnessOff();
	void PoseInit();
	void PoseZero();
	void OpenHand();

	void Test();
public:

	void motion(int x, int y, int z, float *hand_open, float *hand_dir, std::string side);

	void make_motion_left_arm(vector<float> lVec6, float *lHand);
	void make_motion_right_arm(vector<float> rVec6, float *rHand);

	void say(std::string filePath);
};
