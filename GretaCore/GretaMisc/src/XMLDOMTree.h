//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// XMLDOMTree.h: interface for the XMLDOMTree class.
//
//////////////////////////////////////////////////////////////////////

//made by Maurizio Mancini m.mancini@iut.univ-paris8.fr manmau@yahoo.com

//this file describes a generic tree object
//which is the result of the parsing of an xml file

#if !defined(AFX_XMLDOMTree_H__5B7B5963_2060_11D9_9C92_EE4D16C4357B__INCLUDED_)
#define AFX_XMLDOMTree_H__5B7B5963_2060_11D9_9C92_EE4D16C4357B__INCLUDED_

#include "XercesTool.h"
#include "XMLGenericTree.h"

/*! \brief xml generic tree
*
*/

class XMLDOMTree : public XMLGenericTree  
{
private:
	typedef XMLGenericTree * element;
	typedef PolyIterator<element> polyit;
	typedef polyit::implement impl;

	class dom_iterator 
		: public impl {
	private:
		// to register newly created nodes, we keep a pointer
		// to the node whose childs we are iterating over
		XMLDOMTree *_parent;
		// The current child node
		class XERCES_CPP_NAMESPACE::DOMNode *_current;

		// a helper function lookinf for the next valid child
		XERCES_CPP_NAMESPACE::DOMNode *
		getNextValid(XERCES_CPP_NAMESPACE::DOMNode *);

	public:
		// now the implementation interface of the polymorphic
		// iterator template class:
		dom_iterator(XMLDOMTree *parent, 
		             class XERCES_CPP_NAMESPACE::DOMNode *start);
		virtual ~dom_iterator() { }
		virtual impl *clone() const;
		virtual bool equal(const impl&) const;
		virtual element get() const;
		virtual void next();
	};


	XMLDOMTree(XMLDOMTree *parent, 
		       XERCES_CPP_NAMESPACE::DOMElement *mydom);
	XMLDOMTree(XMLDOMTree *parent, 
		       XERCES_CPP_NAMESPACE::DOMText *mydom);

public:
	XMLDOMTree(std::string name, std::string nameSpace="");
	XMLDOMTree(XERCES_CPP_NAMESPACE::DOMDocument *mydom);
	XERCES_CPP_NAMESPACE::DOMNode * getDOMNode();

	bool isTextNode();
	
	/*! \brief prints the tree under this node, complete with all attributes
	*
	* @param s is a string that is prefixed to each printed line
	* and for each recursive iteration of the function the string
	* becomes longer so the final output has a tree appearence;
	* s should be initialized to empty string
	*/
	void Print();
	void Save(std::string filename);
	/*! \brief adds a child to this node
	*
	* @param e is the tree to be added
	*/
	//void addChild(XMLGenericTree *e);
	/*! \brief returns the node called with some name
	* This DOM-based implementation finds, more precisely, the first decendent node that either:
	* (a) if n=="text", the first text node;
	* (b) else, the first element with local name (without any namespace prefixes) n and with the same namespace as the current node.
	* @param n the name of the wanted node
	* @return the pointer to the wanted node or 0 if not found
	*/
	XMLGenericTree* FindNodeCalled(std::string n){
		return FindNodeCalled(n, XercesTool::getNamespaceURI(mynode));
	}
	XMLGenericTree* FindNodeCalled(std::string n, std::string nameSpace);


	
	std::string GetAttribute(std::string n);
	bool HasAttribute(std::string n);
	float GetAttributef(std::string n);
	/**
	* Changes the value of an attribute.
	* if the attribute does not existe, the function creates and adds it.
	* @param n name of the attribute
	* @param value the new value to set
	*/
	void SetAttribute(std::string n, std::string value);
	std::string ToString();

	virtual XMLGenericTree::iterator begin(); 
	virtual XMLGenericTree::iterator end();
	XMLGenericTree* GetParent();
	std::string GetName();
	std::string GetTextValue();
	void SetTextValue(std::string text);
	XMLGenericTree* AddChild(XMLGenericTree* child);
	void AddText(std::string text);
	/**
	* Creates and returns a childwith the specified name
	* @param name the name aff the new child
	* @return the new child
	*/
	XMLGenericTree* CreateChild(std::string name){
		return CreateChild(name, XercesTool::getNamespaceURI(mynode));
	}
	/**
	* Creates and returns a childwith the specified name an namespace
	* @param name the name off the new child
	* @param nameSpace the namespace off the child
	* @return the new child
	*/
	XMLGenericTree* CreateChild(std::string name, std::string nameSpace);

	/*! \brief class constructor
	*
	*/
	XMLDOMTree();
	/*! \brief class destructor
	*
	*/
	virtual ~XMLDOMTree();


private:
	void SaveToFile(FILE *outputfile);
	FILE *outputfile;

	XERCES_CPP_NAMESPACE::DOMNode *mynode;

	// a single linked list of all allocated XMLDOMTree nodes
	// belonging to the same Document (for cleaning up)
	XMLDOMTree *nextAllocedChild;
	XMLDOMTree * FindText();
	/**
	 * This method is specific to XMLDOMTree. It returns the first element node
	 * in the given subtree which has the given local name (without prefixes) and namespace uri.
	 */
	XMLDOMTree* FindElement(std::string name, const std::string & namespaceURI);

};

#endif // !defined(AFX_XMLGenericTree_H__5B7B5963_2060_11D9_9C92_EE4D16C4357B__INCLUDED_)
