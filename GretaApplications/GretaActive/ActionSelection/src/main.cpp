//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include <conio.h>

#include "IniManager.h"
#include "ActionSelectionConnector.h"
#include "NInterface.h"
#include "XercesTool.h"

#include <semaine/system/ComponentRunner.h>
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;

using namespace XERCES_CPP_NAMESPACE;

IniManager inimanager;
std::string ini_filename;
std::string directory;
NInterface *inter;

int main(int argc, char ** argv)
{
	XercesTool::startupXMLTools();
	XMLTool::startupXMLTools();
	if (argc==2)
	{
		ini_filename=argv[1];
		directory="";
	} else if (argc==3)
	{
		ini_filename=argv[1];
		directory=argv[2];
	} else {
        ini_filename="greta_activemq.ini";
		directory="";
	}
	
	ini_filename="greta_activemq.ini";
	inimanager.ReadIniFile(ini_filename);
	
	RandomGen *ran;
	ran=new RandomGen();
	
	try{
	std::list<semaine::components::Component *> comps;
	comps.push_back(new semaine::components::actionselectionconnector::ActionSelectionConnector("ActionSelection", false, false));
	semaine::system::ComponentRunner cr(comps);
	cr.go();
	inter = new NInterface();
	inter->show();
	//Fl::wait();
	Fl::run();
	cr.waitUntilCompleted();


	//semaine::components::actionselection::ActionSelection ASmodule = semaine::components::actionselection::ActionSelection("ActionSelection", false, false);
	//getch();
	}
	catch(CMSException & e) {
		printf("\n*********************************************************\nActiveMQ is not started. The application will be stopped\n*********************************************************");
		getch();
		//MessageBox(0, _T("ActiveMQ is not started. The application will be stopped"), _T("Warning"), MB_OK | MB_ICONWARNING);
		_exit(0);
	}
	XercesTool::shutdownXMLTools();
	XMLTool::shutdownXMLTools();
}



