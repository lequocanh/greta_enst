#ifndef PLAYEROGREVIEW_H
#define PLAYEROGREVIEW_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <FL/Fl_Window.H>
#include <FL/Fl_Output.H>
#include <ogre.h>
#include "PlayerOgreWorld.h"
#include "PlayerOgreCamera.h"
#include "IniManager.h"
#include "DGLAVI.h"

class PlayerOgreView : public Fl_Window{
public :
	PlayerOgreView();
	PlayerOgreView(int x, int y, int w, int h, const char *label, bool autoupdate=true, bool realtime=false);
	PlayerOgreView(int x, int y, int w, int h, const char *label, std::string filename, bool autoupdate=true, bool realtime=false);
	virtual ~PlayerOgreView();

	void initOgreSystem();
	void show();
	void resize(int,int,int,int);

	/**
	 * Sets the view of a specific camera.
	 * Currently, there is 5 cameras in the world :
	 * - 2 cameras per agents (around the agent called "free" and the view of its right eye called "eyes").
	 * - 1 free camera in the world.
	 *
	 * @param characterType must be "agent", "listener" or "world"
	 * @param cameraType must be "free" or "eyes"
	 */
	void loadCamera(std::string characterType, std::string cameraType);
	void setBackgroundColour(double R, double G, double B){ogreWorld->setBackgroundColour(R,G,B); pCamera->setBackgroundColour(R,G,B);}
	int handle(int event);
	void RemoveIdle();
	void MoveCamera(float px,float py,float pz,float rx,float ry,float rz);
	void SetOutputControl(Fl_Output*framenumber, Fl_Output*framerate);
	void SetCameraView(int i);
	void SaveCameraSettings(int i);

	void setCharacterSpeaker(std::string characterName);
	void setCharacterListener(std::string characterName){ogreWorld->setCharacterListener(characterName);}

	PlayerOgreAgent* getAgent(){return ogreWorld->getAgent();}
	PlayerOgreAgent* getListener(){return ogreWorld->getListener();}
	PlayerOgreWorld* getWorld(){return ogreWorld;}

protected:
	void draw();//this function needs to be protected. to call it, please use redraw().

private:
	PlayerOgreWorld		*ogreWorld;
    Ogre::RenderWindow	*ogreRenderWindow;
    Ogre::Viewport		*ogreViewport;
	PlayerOgreCamera	*pCamera;
	Ogre::PixelBox		*pib;
	Fl_Output* outputframenumber;
	Fl_Output* outputframerate;
	double posX,posY,previoustime,actualtime;
	short frameaccumulator;
	char usefulstring[20];
	bool capturing;


public:
	void StartCapture();
	void EndCapture();
	void invalidate();
	std::string filetoload;

private:
	bool realtime;
	GL2AVI::CAVIRecord avirecorder;
	void Initialize(int x, int y, int w, int h, bool autoupdate=true);
	IniManager camerasettings;

};
#endif
