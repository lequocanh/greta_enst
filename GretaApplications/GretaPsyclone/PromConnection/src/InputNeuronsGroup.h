//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// InputNeuronsGroup.h: simple two neurons oscillator
//   @author Ken Prepin ken.prepin@telecom-paristech.fr
//   @date 2008
//////////////////////////////////////////////////////////////////////

#pragma once

#if !defined(_INPUTNEURONSGROUP_H)
#define _INPUTNEURONSGROUP_H


class InputNeuronsGroup
{

public:
	/**
	* Class default constructor of the oscillator, with initialisation : neuronOne=1, neuronTwo=2 
	*
	*/

	InputNeuronsGroup();

	/**
	* Destructor of oscillator
	*
	*/
	~InputNeuronsGroup();

	/**
	* Initialise the Group of neuron during first use
	*
	*/
	void iniGroup(int numNeurons);

	/**
	* update the step by step functionning of the oscillator.
	*
	*/
	void updateGroup();
	/**
	* get the number of neurons of the group
	*
	*/

	int getNumOfNeurons();
	/**
	* get the oscillator activation (neuronOne)
	*
	*/
	float getActivation(int numeroNeuron);
	/**
	* List of neurons (actually, list of 'floats')
	*
	*/
	float *neuronsList;
	/**
	* get the time of inputs in milliseconds
	*
	*/
	int getInputTime();
	/**
	* set the time of inputs in milliseconds
	*
	*/
	void setInputTime(int inputTime);

private:

	/**
	* The two neurons constituing an oscillator, an inhibition modulating this oscillator, and the frequency parameter of this oscillator
	*
	*/
	int inputTime;
	int NumOfNeurons;
//	float inhibOsc, freqOsc;

};

#endif