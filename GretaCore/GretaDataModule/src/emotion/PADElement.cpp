//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ConsNode.h: interface for the ConsNode class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>

#include "PADElement.h"


PADElement::PADElement(){

		this->pvalue=0.0;
		this->avalue=0.0;
		this->dvalue=0.0;

		this->xd=0.1;
		this->yd=0.1;
		this->zd=0.1;
	
		//name in lexicon
		this->label="";

		//name of facial expression in fl
		this->name_for_fl ="";

	}



PADElement::PADElement(float pvalue1,
			   float avalue1,
			   float dvalue1,
			   float xd1,
			   float yd1,
			   float zd1,
			   std::string label1
				){

		this->pvalue = pvalue1;
		this->avalue = avalue1;
		this->dvalue = dvalue1;

		this->xd = xd1;
		this->yd = yd1;
		this->zd = zd1;
	
		this->label = label1;
		this->name_for_fl = label1;
	}


PADElement::PADElement(float pvalue1, float avalue1, float dvalue1, std::string label1){

		this->pvalue = pvalue1;
		this->avalue = avalue1;
		this->dvalue = dvalue1;

		this->xd = 0.3;
		this->yd = 0.3;
		this->zd = 0.3;
	
		this->label = label1;
			this->name_for_fl = label1;

	}

PADElement::PADElement(float pvalue1, float avalue1, float dvalue1, std::string label1, std::string label2){

		this->pvalue = pvalue1;
		this->avalue = avalue1;
		this->dvalue = dvalue1;

		this->xd = 0.3;
		this->yd = 0.3;
		this->zd = 0.3;
	
		this->label = label1;
			this->name_for_fl = label2;

	}


PADElement::~PADElement(){}