#include "KeyframesGenerator.h"

#include "IniManager.h"
#include "GestureEngine.h"
#include <stdio.h>
#include <string.h>
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <fl/fl_widget.h>
#include <FL/fl_draw.H>
#include <FL/Fl_File_Chooser.H>
#include <crtdbg.h>

#include <pthread.h>
#include <stdlib.h>

#include "DataContainer.h"
extern DataContainer *datacontainer;

using namespace GestureSpace;

float arm_x[]={0.12, 0.09, 0.06, 0.03, 0.0};
float arm_y[]={0.19, 0.16, 0.13, 0.10, 0.07,0.04,0.00};
float arm_z[]={0.10, 0.12, 0.14};

const char * arm_x_look_up[]={"XEP", "XP", "XC", "XCC", "XOppC"};
const char * arm_y_look_up[]={"YUpperEP", "YUpperP", "YUpperC", "YCC", "YLowerC","YLowerP","YLowerEP"};
const char * arm_z_look_up[]={"ZNear", "ZMiddle", "ZFar"};


const char * phasetype_look_up[]={"PHASETYPE_DEFAULT", "PREPARATION", "PRE_STROKE_HOLD","STROKE",
								"STROKE_START", "STROKE_END","POST_STROKE_HOLD", "RETRACTION"};

const char * form_look_up[]={"form_default","form_fist","form_open","form_point1","form_point2",
								"form_2apart","form_openapart"};

const char * palm_look_up[]={"PalmDefault","PalmUp","PalmDown","PalmInwards","PalmOutwards","PalmAway","PalmTowards","PalmNone"};

const char * fingerbase_look_up[]={"FBDefault","FBUp","FBDown","FBInwards","FBOutwards","FBAway","FBTowards","FBNone"};

KeyframesGenerator::KeyframesGenerator(void)//:cmlabs::JThread()
{
		
	gesture=new Gesture();
	phasesVector = &(gesture->phases);

	//ADDME: should we really start with 1 phase?
	GesturePhase *phase=new GesturePhase();
	phase->Start();
	phase->Finalize();
	phasesVector->push_back(phase);

	phase=new GesturePhase();
	phase->Start();
	phase->Finalize();
	gesture->phasesAssym.push_back(phase);

	lHand = new float(0);
	rHand = new float(0);

	lVec6.resize(6,0);
	rVec6.resize(6,0);
}
/**********************************************************/
void KeyframesGenerator::Interpret(GestureSpace::GestureVector *gestureVector)
{
	int nlPoints = 0;	
	int nrPoints = 0;

	GestureVector::iterator gestureIter;

	// count number of keyframes
	for(gestureIter=gestureVector->begin();gestureIter!=gestureVector->end();gestureIter++)
		if((*gestureIter)->GetDescription().find("REST")==string::npos)
	{
		(*gestureIter)->Print();
		
		if((*gestureIter)->GetSide() == 1 || (*gestureIter)->GetSide() == 3 || (*gestureIter)->GetSide() == 4 ) 
			nlPoints = nlPoints + (*gestureIter)->phases.size();	
		if((*gestureIter)->GetSide() == 0 || (*gestureIter)->GetSide() == 3 || (*gestureIter)->GetSide() == 4 ) 
			nrPoints = nrPoints + (*gestureIter)->phases.size();	
	}	
	
	std::cout <<"Number of left points: "<<nlPoints<<std::endl;
	std::cout <<"Number of right points: "<<nrPoints<<std::endl;
	this->leftPath.reserve(nlPoints);
	this->rightPath.reserve(nrPoints);
	this->leftTimes.reserve(nlPoints);
	this->rightTimes.reserve(nrPoints);

	//calculate cartesian positions for keyframes
	for(gestureIter=gestureVector->begin();gestureIter!=gestureVector->end();gestureIter++)
		if((*gestureIter)->GetDescription().find("REST")==string::npos)
	{			
		this->Interpret((*gestureIter));
	}	
	
}
/**********************************************************/
void KeyframesGenerator::Interpret(GestureSpace::Gesture *gesture)
{

	//std::cout << "Start calculation of joint angles ... " <<endl;
	std::vector<GesturePhase*>::iterator iter;
	float *hand_open = new float(0);
	float *hand_dir = new float(0);	
	vector<float> vectxyz;
	vectxyz.resize(3,0);	
	// two arms have different movements
	if(gesture->GetSide() == assym) 
	{
		// calculate for the left arm (phasesAssym)
		for(iter=gesture->phasesAssym.begin(); iter!=gesture->phasesAssym.end();iter++) 
		{
			vectxyz = this->Converter(*iter, vectxyz, hand_open,hand_dir, gesture->GetExpressivitySPC(), gesture->GetExpressivityTMP());			
			lVec6[0] = 	vectxyz[0];
			lVec6[1] = 	vectxyz[1];
			lVec6[2] = 	vectxyz[2];
			lVec6[3] = 	*hand_open;
			lVec6[4] = 	*hand_dir;
			lVec6[5] = 	(*iter)->GetAbsTime();	
			leftPath.push_back(lVec6);		
			

			std::cout <<"assym left type: "<<phasetype_look_up[(*iter)->GetType()]<<" time: "<<(*iter)->GetAbsTime()<<" space:";
			for(int i=0;i<6;i++) std::cout<<lVec6[i]<<"-";
			std::cout<<std::endl;			
		}

		// calculate for the right arm (phases)
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vectxyz = this->Converter(*iter, vectxyz, hand_open,hand_dir, gesture->GetExpressivitySPC(), gesture->GetExpressivityTMP());			
			rVec6[0] = 	vectxyz[0];
			rVec6[1] = 	vectxyz[1];
			rVec6[2] = 	vectxyz[2];
			rVec6[3] = 	*hand_open;
			rVec6[4] = 	*hand_dir*(-1);
			rVec6[5] = 	(*iter)->GetAbsTime();	
			rightPath.push_back(rVec6);		

			std::cout <<"assym right type: "<<phasetype_look_up[(*iter)->GetType()]<<" time: "<<(*iter)->GetAbsTime()<<" space:";
			for(int i=0;i<6;i++) std::cout<<rVec6[i]<<"-";
			std::cout<<std::endl;

			//this->my_robot.make_motion_right_arm(rVec6, rHand);
		}
	}
	
	// two arms have the same movements
	if(gesture->GetSide()==both_sides)
	{
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vectxyz = this->Converter(*iter, vectxyz, hand_open,hand_dir, gesture->GetExpressivitySPC(), gesture->GetExpressivityTMP());			
			rVec6[0] = 	vectxyz[0];
			rVec6[1] = 	vectxyz[1];
			rVec6[2] = 	vectxyz[2];
			rVec6[3] = 	*hand_open;
			rVec6[4] = 	*hand_dir;
			rVec6[5] = 	(*iter)->GetAbsTime();	
			rightPath.push_back(rVec6);		

			lVec6[0] = 	vectxyz[0];
			lVec6[1] = 	vectxyz[1];
			lVec6[2] = 	vectxyz[2];
			lVec6[3] = 	*hand_open;
			lVec6[4] = 	*hand_dir*(-1);
			lVec6[5] = 	(*iter)->GetAbsTime();	
			leftPath.push_back(lVec6);		
			
		}
	}

	// Only left arm
	if(gesture->GetSide()==l)
	{
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vectxyz = this->Converter(*iter, vectxyz, hand_open,hand_dir, gesture->GetExpressivitySPC(), gesture->GetExpressivityTMP());			
			lVec6[0] = 	vectxyz[0];
			lVec6[1] = 	vectxyz[1];
			lVec6[2] = 	vectxyz[2];
			lVec6[3] = 	*hand_open;
			lVec6[4] = 	*hand_dir;
			lVec6[5] = 	(*iter)->GetAbsTime();	
			leftPath.push_back(lVec6);		

			std::cout <<"left type: "<<phasetype_look_up[(*iter)->GetType()]<<" time: "<<(*iter)->GetAbsTime();
			//for(int i=0;i<6;i++) std::cout<<lVec6[i]<<"-";
			std::cout<<std::endl;
			//this->my_robot.make_motion_left_arm(lVec6, lHand);
		}
	}
	
	// Only right arm
	if(gesture->GetSide()==r)
	{
		for(iter=gesture->phases.begin(); iter!=gesture->phases.end(); iter++)
		{
			vectxyz = this->Converter(*iter, vectxyz, hand_open,hand_dir, gesture->GetExpressivitySPC(), gesture->GetExpressivityTMP());			
			rVec6[0] = 	vectxyz[0];
			rVec6[1] = 	vectxyz[1];
			rVec6[2] = 	vectxyz[2];
			rVec6[3] = 	*hand_open;
			rVec6[4] = 	*hand_dir*(-1);
			rVec6[5] = 	(*iter)->GetAbsTime();	
			rightPath.push_back(rVec6);		

			std::cout <<"right type: "<<phasetype_look_up[(*iter)->GetType()]<<" time: "<<(*iter)->GetAbsTime();
			//for(int i=0;i<6;i++) std::cout<<rVec6[i]<<"-";
			std::cout<<std::endl;
			//this->my_robot.make_motion_right_arm(rVec6, rHand);
		}

	}
}

/**********************************************************/
vector<float>  KeyframesGenerator::Converter(GesturePhase *phase, vector<float> vecxyz, float *hand_open, float *hand_dir, float SPC, float TMP)
{
	// form of hand
	if(phase->GetPHand()->type==shape_form) // Truong hop hinh dang cua ban tay duoc dinh nghia theo dang form
	{	
		BasicForm* pForm = (BasicForm*) phase->GetPHand()->shape;

		if(pForm->type == form_open || pForm->type == form_openapart) // close hand form_default, form_fist, form_open, form_point1, form_point2, form_2apart, form_openapart
		{
			*hand_open = 1; // open hand
		}
		else // close hand
		{
			*hand_open = 0;
		}
	}

	if((phase)->GetPHand()->type==shape_symbol) // Truong hop hinh dang cua ban tay duoc dinh nghia theo dang symbol
	{
		BasicSymbol* pSymbol = (BasicSymbol*) phase->GetPHand()->shape;
		
		if(pSymbol->type==symbol_1_open     // open 70%
			|| pSymbol->type==symbol_2_open
			|| pSymbol->type==symbol_3_open)
		{
			*hand_open = 0.7;
		}
		else  // symbol_1_close
			// open 30%
		{
			*hand_open = 0.3;
		}
	}		

	// position of wrist	
	int x = phase->GetPArm()->AbstractX;
	int y = phase->GetPArm()->AbstractY;
	int z = 1;
	if(phase->GetPArm()->AbstractZ == ZNear) z=2;
	else if(phase->GetPArm()->AbstractZ == ZFar) z=0;
		
	//add expressivity parameters [-1,1]
	if(SPC<0) // larger
	{
		x=x+1; 
		y=y+1;
		z=z+1;
	}
	if(SPC>0)  // smaller
	{
		x=x-1;
		y=y-1;
		z=z-1;
	}

	if(x<0) x=0;
	if(x>4) x=4;
	if(y<0) y=0;
	if(y>6) y=6;
	if(z<0) z=0;
	if(z>2) z=2;

	vecxyz[0] = x;
	vecxyz[1] = y;
	vecxyz[2] = z;

	// orientation of hand (i.e. left hand)
	*hand_dir = 0;

	int p = phase->GetPWrist()->GetFromPalm();
	if(phase->GetPWrist()->GetWristMagnitude()<1)
		p = phase->GetPWrist()->GetPalm();

	//std::cout << "WRIST: " << p << std::endl;

	if(p== PalmUp || p== PalmTowards)
			*hand_dir = -90;
	else
		if(p == PalmDown || p == PalmAway)
				*hand_dir = 90;	
		else
			if(p == PalmOutwards)
				*hand_dir = 104;
	

	return vecxyz;
}
/**********************************************************/
KeyframesGenerator::~KeyframesGenerator(void)
{
	free(lHand);
	free(rHand);	
}