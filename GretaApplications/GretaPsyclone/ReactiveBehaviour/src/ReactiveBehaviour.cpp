
#include "DataContainer.h"
#include "ReactiveBehaviour.h"
#include "IniManager.h"
#include "Timer.h"
#include <stdio.h>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;

extern DataContainer *datacontainer;
extern IniManager inimanager;
extern CentralClock *pc;

ReactiveBehaviour::ReactiveBehaviour()
{	
	initPsycloneConnection();

	listenerdata=datacontainer->getListenerData();

	agentstate=new AgentState(listenerdata->agentstate);
	behaviourselector=new BehaviourSelector(this, pc, agentstate);
	behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
	behaviourselector->start();


	reactivebehaviourtrigger=new ReactiveBehaviourTrigger(agentstate);

	while(true)
	{
		ReadInputMessages();
		ReadMessage(commint->ReceiveMessage(100));

		std::list<GretaLogger*>::iterator iterlog;

		for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
		{
			std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
			if((*iterlog)->getLevel()=="debug")
				this->commint->log->debug(msg);
			if((*iterlog)->getLevel()=="info")
				this->commint->log->info(msg);
			if((*iterlog)->getLevel()=="warn")
				this->commint->log->warn(msg);
			if((*iterlog)->getLevel()=="error")
				this->commint->log->error(msg);
		}
		listLog.clear();
	}
}

ReactiveBehaviour::~ReactiveBehaviour()
{
}


// Load the application parameter from the inimanager
int ReactiveBehaviour::initPsycloneConnection()
{	
	// Register Psyclone module adress
	moduleHost=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	// Register Psyclone module port
	modulePort=inimanager.GetValueInt("PSYCLONE_PORT");
	// Get Greta's Name
	GretaName=inimanager.GetValueString("GRETA_NAME").c_str();

	// Creation of the Psyclone or ActiveMQ inteface
	commint=new Psydule(GretaName+INPUTGRETA_MODULE_NAME,moduleHost,modulePort);
	this->GretaName=commint->getGretaName(GretaName);
	

	// registered packet list
	std::list<std::string> registeredGretaDatatypes;
	// register to Greta Clock messages.
	registeredGretaDatatypes.push_back(GretaName+CLOCK_MESSAGE_HEADER);
	// register to Caracterisation Baseline Message.
	registeredGretaDatatypes.push_back(GretaName+AGENTSTATE_MESSAGE_HEADER);
	registeredGretaDatatypes.push_back(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER);
	// register to System Feedback
	registeredGretaDatatypes.push_back(GretaName+SYSTEM_FEEDBACK_MESSAGE_HEADER);
	// register to Cognitive Backchannel
	registeredGretaDatatypes.push_back(GretaName+COGNITIVE_FML_MESSAGE_HEADER);

	
	// registration GretaWhiteboard
	if(commint->Register(GretaName+GRETA_WHITEBOARD,registeredGretaDatatypes)!=0)
	{
		std::cout << GretaName+INPUTGRETA_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_OK << std::endl;		
	}
	else
	{
		std::cout << GretaName+INPUTGRETA_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_FAILED<< std::endl;
		exit(0);
	}
	std::cout << "Registration to GretaWhiteboard succesful" << std::endl;


	// Creation of the Psyclone input module if it does not have already been created (by another instanciation of ReactiveBehaviour)
	commintInput=new Psydule(GretaName+INPUTWATSON_MODULE_NAME,moduleHost,modulePort);

	std::list<std::string> registeredInputDatatypes;
	// register to Input messages, Watson
	registeredInputDatatypes.push_back(INPUT_MESSAGE_HEADER);

	// registration InputWhiteboard
	if(commintInput->Register(INPUT_WHITEBOARD,registeredInputDatatypes)!=0)
	{
		std::cout << GretaName+INPUTWATSON_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_OK << std::endl;		
	}
	else
	{
		std::cout << GretaName+INPUTWATSON_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_FAILED<< std::endl;
		commintInput=0;
		exit(0);
	}
	std::cout << "Registration to InputeWhiteboard succesful" << std::endl;


	return 1;
}

int ReactiveBehaviour::ReadMessage(CommunicationMessage *msg)
{
	std::string content, type;
	int time;

	//marche pas avec le niveau d'interet
	//pc->SetTime(commint->get_time());

	if(msg==NULL) return(0);

	content=msg->getContent();
	type=msg->getType();
	if(strcmp(content.c_str(),"")!=0)
	{
		if(strcmp(type.c_str(),(GretaName+CLOCK_MESSAGE_HEADER).c_str())==0)
		{
			time=atoi(content.c_str());
			pc->SetTime(time);
		}
		else
		{
			//printf("message : %s /n",content.c_str());
			//if(strcmp(msg->type.c_str(),GretaName+COGNITIVE_FML_MESSAGE_HEADER)==0)
			//	behaviourselector->updateBackchannelRequest((std::string)content);
			//else
			if(strcmp(type.c_str(),(GretaName+SYSTEM_FEEDBACK_MESSAGE_HEADER).c_str())==0)
			{
				;//da modificare rispettando la forma usata per inviare feedback (indica se un backchannel � stato eseguito)
			}
			else
			if(strcmp(type.c_str(),(GretaName+AGENTSTATE_MESSAGE_HEADER).c_str())==0)
			{
				printf("mental state changed\n");
				agentstate->internalstate.LoadFromBuffer(content);
				behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
			}
			else
			if(strcmp(type.c_str(),(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER).c_str())==0)
			{
				std::string app=content;
/*				if(app.substr(0,app.find_first_of("="))=="emotion_state.emotion.name")
				{
						agentstate->setEmotionState(app.substr(app.find_first_of("=")+1));
				}
				else*/
				if(app.substr(0,app.find_first_of("="))=="mimicry_level.mimicry.value")
				{
					agentstate->internalstate.SetAttrValue("mimicry_level.mimicry.value", app.substr(app.find_first_of("=")+1));
					behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
	//				behaviourselector->setMimicryPercentage(agentstate->getValue("mimicry_level.mimicry.value"));
				}
				else
				if(app.substr(0,app.find_first_of("="))=="agent_identity.agent.name")
				{
					agentstate->setAgentName(app.substr(app.find_first_of("=")+1));
				}
				else
				if(app.substr(0,app.find_first_of("="))=="agent_turn")
				{
					agentstate->setAgentTurn(app.substr(app.find_first_of("=")+1));
				}
				else
					agentstate->setSingle(app, "");
			}
		}

		return 1;
	}
	return 0;
}

int ReactiveBehaviour::ReadInputMessages()
{
	CommunicationMessage *msg;
	std::string content="", type;
	char v[255];
	int time=0;
	int end_time=0;

	msg=commintInput->ReceiveMessage(10);
	if(msg==NULL) return 0;
	
	std::map<std::string,DataBackchannel*> bmlfml;

	time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000);
	end_time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000)+10;
	
	//while(msg!=NULL)
	while(end_time>time)
	{
		if(msg!=NULL)
		{
			type=msg->getType();
			if(strcmp(type.c_str(),INPUT_MESSAGE_HEADER)==0)
			{
				content=msg->getContent();
				if(agentstate->getAgentTurn()==0)
					reactivebehaviourtrigger->addMessage((std::string)content);
			}
		}
		msg=commintInput->ReceiveMessage(10);
		time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000);
	}
	
	if(reactivebehaviourtrigger->messageList.size()>0)
		bmlfml=reactivebehaviourtrigger->ReceiveMessages();


	if(bmlfml["bml"]!=NULL)
		behaviourselector->updateBackchannelRequest(bmlfml["bml"]);

	if(bmlfml["fml"]!=NULL)
		behaviourselector->updateBackchannelRequest(bmlfml["fml"]);

	return(1);
}

void ReactiveBehaviour::SendBehaviour(std::string bmlfml, std::string messagetype)
{
	commint->PostString(bmlfml,GretaName+GRETA_WHITEBOARD, messagetype);
}
std::string ReactiveBehaviour::getGretaName()
{	
	return GretaName;
}