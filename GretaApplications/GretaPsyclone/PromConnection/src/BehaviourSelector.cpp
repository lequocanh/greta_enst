//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// ReactiveBehaviour.cpp: implementation of the ReactiveBehaviour class.
//
//////////////////////////////////////////////////////////////////////


#include "PromConnection.h"
#include <string>
#include <vector>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include "BehaviourSelector.h"

//#include "XMLGenericParser.h"

#define MAX_DELAY 3000
#define MIN_DELAY 1500


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BehaviourSelector::BehaviourSelector()//:JThread()
{
	fileFlag=0;
}

BehaviourSelector::BehaviourSelector(PromConnection *rbp, CentralClock *pc, AgentState *as)//:JThread()
{
	this->rbp=rbp;
	this->agentstate=as;
	this->pc=pc;
	randgen = new RandomGen();
	GretaName=rbp->getGretaName();
	cpt=0;
	cptSentences=0;

	//InitSelector();
	mimicry=new DataBackchannel("MIMICRY");
	reactive=new DataBackchannel("REACTIVE");
	cognitive=new DataBackchannel("COGNITIVE");
	lastBackchannelSent=new DataBackchannel("LAST_BACKCHANNEL_SENT");
}

BehaviourSelector::~BehaviourSelector()
{
	delete randgen;
}

int BehaviourSelector::updateBackchannelRequest(DataBackchannel *db)
{
	DataBackchannel *databackchannel;

	databackchannel=FindRequest(db->type);
	if(databackchannel==NULL)
	{
		printf("Backchannel type %s not found", db->type.c_str());
		return 0;
	}

	if(db->priority<databackchannel->priority)
		databackchannel->CopyDataBackchannel(db);

	return 1;
}

DataBackchannel* BehaviourSelector::FindRequest(std::string type)
{
	if(type=="MIMICRY")
		return mimicry;
	if(type=="REACTIVE")
		return reactive;
	if(type=="COGNITIVE")
		return cognitive;
	return NULL;
}

std::string BehaviourSelector::selectBackchannelType()
{
	float randomvalue=randgen->GetRand01();
	if(randomvalue>=0 && randomvalue<=mimicryLevel)
		return(retrieveBackchannelrequest("MIMICRY"));
	else
		return(retrieveBackchannelrequest("REACTIVE"));
}

std::string BehaviourSelector::retrieveBackchannelrequest(std::string type)
{
	std::string behaviour="";
	DataBackchannel *db=FindRequest(type);

	if(type=="MIMICRY")
	{
		behaviour=WriteBML(db);
		if(behaviour!="")
		{
			printf("\nMIMICRY for event: %s\n\n", db->zone.c_str());
			lastBackchannelSent->CopyDataBackchannel(db);
		}
	}
	if(type=="REACTIVE")
	{
		behaviour=WriteFML(db);
		if(behaviour!="")
		{
			printf("\nBACKCHANNEL for event: %s\n\n", db->zone.c_str());
			lastBackchannelSent->CopyDataBackchannel(db);
		}
	}

	if(type=="COGNITIVE")
		; //da definire

	return behaviour;
}


/*void BehaviourSelector::run()
{
	std::string behaviour="";
	int il=0;	
	while(true)
	{
		Sleep(40);
		if(oscillActivation>0)
		{
		int time=pc->GetTime();
		behaviour=WriteSignalInBML("head","head=head_up",0,1);
		std::string bml=behaviour.substr(behaviour.find_first_of(";")+1);
		rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);

		printf("Behaviour sent au temps %i\n", time);
		Sleep(200);
		time=pc->GetTime();
		behaviour=WriteSignalInBML("head","head=head_down",0,2);
		bml=behaviour.substr(behaviour.find_first_of(";")+1);
		rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);
		Sleep(200);
		}
/*		behaviour=retrieveBackchannelrequest("COGNITIVE");
		if(behaviour=="")
			behaviour=selectBackchannelType();

	//	if(behaviour=="")
	//		printf("Non ci sono behaviour\n");//, behaviour.c_str());
		

		if(behaviour!="")
		{
			if(behaviour.substr(0,behaviour.find_first_of(";"))=="bml")
			{
				std::string bml=behaviour.substr(behaviour.find_first_of(";")+1);
				rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);
				il=(int)(MAX_DELAY-(MAX_DELAY-MIN_DELAY)*agentstate->getUserInterestLevel());
				if(il<0) il=1300;
				Sleep(1000+il);
				mimicry->CleanDataBackchannel();
				reactive->CleanDataBackchannel();
			}
			else
			{
				std::string fml=behaviour.substr(behaviour.find_first_of(";")+1);
				rbp->SendBehaviour(fml, GretaName+FML_MESSAGE_HEADER);
				il=(int)(MAX_DELAY-(MAX_DELAY-MIN_DELAY)*agentstate->getUserInterestLevel());
				if(il<0) il=1300;
				Sleep(1000+il);
				mimicry->CleanDataBackchannel();
				reactive->CleanDataBackchannel();
			}
		}
	
	}
}*/

std::string BehaviourSelector::WriteBML(DataBackchannel *db)
{
	std::string bml;
	int time=pc->GetTime();
	int index=1;
	
	if(db->referencesMap.size()<1)
		return("");

	bml="bml;";
	bml+="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	bml+="<!DOCTYPE bml SYSTEM \"bml/bml.dtd\" []>\n";
	bml+="<bml>\n";

	std::map<std::string, std::string>::iterator refiter;
	for(refiter=db->referencesMap.begin();refiter!=db->referencesMap.end();refiter++)
	{
		bml+=WriteSignalInBML((*refiter).first, (*refiter).second, time, index);
		index+=1;
	}
	bml+="</bml>\n";
	return(bml);
}

//std::string BehaviourSelector::WriteSignalInBML(DataBackchannel *db)
std::string BehaviourSelector::WriteSignalInBML(std::string modality, std::string reference, int time, int index)
{
	//Create BML for mimicry behaviour
	std::string bml="";
	char s[256];

	if(reference=="" || modality=="")
		return("");
	else
	{
	bml="bml;";
	bml+="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	bml+="<!DOCTYPE bml SYSTEM \"bml/bml.dtd\" []>\n";
	bml+="<bml>\n";

		sprintf(s,"%d",index);
		bml+="<" + modality + " id=\"sig" + (std::string)s + "\"  ";
		//sprintf(s,"%.3f",(float)(time+1500)/1000);
		sprintf(s,"%.3f",0.0);
		
		bml+="start=\"" + (std::string)s + "\" ";//>\n";
		//bml+="end=\"" + db->end + "\" stroke=\"1.0\">\n";
		bml+="end=\"0.3\" stroke=\"0.2\">\n";
		//
		bml+="<description level=\"1\" type=\"gretabml\">\n";
		bml+="<reference>" + reference + "</reference>\n";
		bml+="<intensity>0.50</intensity>\n";
		bml+="<FLD.value>0.70</FLD.value>\n";
		bml+="<PWR.value>0.20</PWR.value>\n";
		bml+="<REP.value>0.00</REP.value>\n";
		bml+="<SPC.value>0.00</SPC.value>\n";
		bml+="<TMP.value>0.40</TMP.value>\n";
		bml+="</description>\n";
		bml+="</" + modality + ">\n";
	bml+="</bml>\n";
		return(bml);
	}
}

std::string BehaviourSelector::WriteFML(DataBackchannel *db)
{
	int i=0;
	char s[256];
	std::string fml="";
	std::vector<std::string>::iterator itermp;

	if(db->communicativefunction.size()==0)
		return("");

	//sprintf(s,"%d",db->time);
	//fml=(std::string)s + ";fml;";
	//std::string check=(std::string)s + ";fml;";
	fml="fml;";
	
	fml+="<?xml version=\"1.0\"?>\n";
	fml+="<!DOCTYPE fml-apml SYSTEM \"apml/fml-apml.dtd\" []>\n";
	fml+="<fml-apml>\n";

	fml+="<fml>\n";

	for(itermp=db->communicativefunction.begin(); itermp!=db->communicativefunction.end(); itermp++)
	{
//			check+=(*itermp) + " ";
			i+=1;
			sprintf(s,"%d",i);
			fml+="<backchannel id=\"p" + (std::string)s + "\" ";
			fml+="type=\"" + (*itermp) + "\" ";
			//sprintf(s,"%.3f",(float)(pc->GetTime()+1500)/1000);
			sprintf(s,"%.3f",(float)0.0);
			//DEFINE END!!
			fml+="start=\"" + (std::string)s + "\" end=\"1.5\" ";
			//fml+="importance=\"" + 1.0/(float)vecCommFun.size() + "\"/>\n";
			fml+="importance=\"1.0\"/>\n";
	}
	
//	fml+="<emotion id=\"e\" type=\"" + db->emotionalstate + "\" regulation=\"felt\" start=\"0.0\" end=\"2.0\" importance=\"1.0\"/>\n";
	fml+="</fml>\n";
	fml+="</fml-apml>\n";

	//if(check!="")
	//	printf("%s\n", check.c_str());
	return(fml);
}



/*std::string BehaviourSelector::WriteSignalInBML(DataBackchannel *db)
{
	//Create BML for mimicry behaviour
	std::string bml="";
	char s[256];

	if(db->reference=="")
		return("");
	else
	{
		bml="bml;";
		//sprintf(s,"%d",db->time);
		//bml=(std::string)s + ";bml;";// + headdirection;
		//printf("%s\n",headdirection.c_str());

		bml+="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
		bml+="<!DOCTYPE bml SYSTEM \"bml/bml.dtd\" []>\n";
		bml+="<bml>\n";
		//DA DEFINIRE!!!!
		bml+="<" + db->modality + " id=\"h1\"  ";
		sprintf(s,"%.3f",(float)(pc->GetTime()+1500)/1000);
		
		bml+="start=\"" + (std::string)s + "\" ";//>\n";
		//bml+="end=\"" + db->end + "\" stroke=\"1.0\">\n";
		bml+="end=\"1.5\" stroke=\"1.0\">\n";
		//
		bml+="<description level=\"1\" type=\"gretabml\">\n";
		bml+="<reference>" + db->reference + "</reference>\n";
		bml+="<intensity>1.00</intensity>\n";
		bml+="<FLD.value>1.00</FLD.value>\n";
		bml+="<PWR.value>0.50</PWR.value>\n";
		bml+="<REP.value>0.00</REP.value>\n";
		bml+="<SPC.value>1.00</SPC.value>\n";
		bml+="<TMP.value>0.40</TMP.value>\n";
		bml+="</description>\n";
		bml+="</" + db->modality + ">\n";
		bml+="</bml>\n";

		return(bml);
	}
}
*/
void BehaviourSelector::ActionSpeech(InputNeuronsGroup* inputFromTalk)
{
	cpt--;
	if(inputFromTalk->getActivation(0)>0&& cpt<=0){
		this->Speech();
		cpt=5;//5;
	}

}
void BehaviourSelector::Action(InputNeuronsGroup** inputFromProm, int cptNbMessages, int timeFirstMessageOfNextAnim, int delayBeforeBMLSending)
{
	/* The 'cptNbMessages' messages must be integrated in one animation of length 'delayBeforeBMLSending' */
	std::string bml="";
	bml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
	bml+="<bml xmlns=\"http://www.mindmakers.org/projects/BML\">\n";
	for(int i=0; i<cptNbMessages;i++)
	{
		if (inputFromProm[i]->getActivation(0)>0)
	   {

			bml+="<speech id='s1' start='0' language='english' voice='openmary'  type='application/wav' text='bla'>\n";
			bml+="<description level='1' type='gretabml'>\n";
			bml+="<reference>";
			bml+="tmp/from-fml-apml-";
			bml+=GretaName;
			bml+=".pho";
			bml+="</reference>\n";
			bml+="</description>\n";
			bml+="<tm id='tm1'/>\n";
			switch(cptSentences){
				case 0:	bml+="The animated conversationnal agent Greta, \n"; break;
				case 1:	bml+="is developped by Catherine Pelachaud's team.\n"; break;
				case 2:	bml+="Greta is represented by a three Dee women, \n"; break;
				case 3:	bml+="compatible with the animation M peg four.\n"; break;
				case 4:	bml+="Greta is able to use various behaviours \n"; break;
				case 5:	bml+="She can use both verbal and non-verbal behaviour.\n"; break;
			}
			cptSentences++;
			if(cptSentences>5) cptSentences=0;
		//	bml+="<tm id='tm2'/>\n";
		//	bml+="bla\n";
			bml+="<tm id='tm2' time='0.1'/>\n";
		//	bml+="<pitchaccent id='xpa1' type='Hstar' start='s1:tm2' end='s1:tm4' level='medium' importance='0.5'/>\n";
		//	bml+="<boundary id='b1' type='HH' start='s1:tm4' end='1.0'/>\n";
			bml+="</speech>\n";
			break;
	   }
	}
	for(int i=0; i<cptNbMessages;i++)
	{
	   if ((inputFromProm[i])->getActivation(1)>0)
	   {	
		    printf("headNod\n");
		    char s[256];
			sprintf(s,"%d",i);
			bml+="	<head id=\""+ (std::string)s + "\"  ";
			//sprintf(s,"%.3f",5);
			//sprintf(s,"%.3f",(float)(pc->GetTime()/1000));
			sprintf(s,"%.3f",((float)(inputFromProm[i]->getInputTime()-timeFirstMessageOfNextAnim))/1000);/* Time relative to BML start, in seconds */
			bml+="start=\""+ (std::string)s + "\" end=\"0.3\">\n";
			bml+="		<description level=\"1\" type=\"gretabml\">\n";
			bml+="		<reference>head=head_nod2</reference>\n";
			bml+="		<intensity>1.00</intensity>\n";
			bml+="			<FLD.value>0.00</FLD.value>\n";
			bml+="			<PWR.value>0.00</PWR.value>\n";
			bml+="			<REP.value>0.00</REP.value>\n";
			bml+="			<SPC.value>0.00</SPC.value>\n";
			bml+="			<TMP.value>1.00</TMP.value>\n";
			bml+="		</description>\n";
			bml+="	</head>\n";
		
	   }
	}
	//for(int i=0; i<cptNbMessages;i++)
	//{
	//   if (inputFromProm[i]->getActivation(2)>0) 
	//   {
	//		bml+="<face id=\"emotion-0\"  start=\"0\" end=\"0.8\" stroke=\"0.6\">\n";
	//		bml+="<description level=\"1\" type=\"gretabml\">\n";
	//		bml+="<reference>mouth=smile</reference>\n";
	//		bml+="<intensity>1.00</intensity>\n";
	//		bml+="</description>\n";
	//		bml+="</face>\n";
	//	 
	//   }
	//}
	bml+="</bml>\n";
	rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);

}
void BehaviourSelector::ActionNonVerbal(InputNeuronsGroup* inputNonVerbal)
{
	
	if(inputNonVerbal->getActivation(1)>0)
		this->HeadNod();
	if(inputNonVerbal->getActivation(0)>0)
		this->Smile();

}

void BehaviourSelector::Speech()
{
	std::string fileNum	="0-";
	if(fileFlag)
	{
		fileNum="1-";
		fileFlag=0;
	}else
	{
		fileFlag=1;
	}
		
	std::string bml="";

/*	bml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	bml+="<!DOCTYPE bml SYSTEM \"bml.dtd\" []>";
	bml+="<bml>";*/
	bml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
	bml+="<bml xmlns=\"http://www.mindmakers.org/projects/BML\">\n";
	bml+="<speech id='s1' start='0.0' language='english' voice='openmary'  type='application/wav' text='bla'>\n";
	bml+="<description level='1' type='gretabml'>\n";
	bml+="<reference>";
	bml+="tmp/from-fml-apml-";
	bml+=GretaName;
	bml+=fileNum;
	bml+=".pho";
	bml+="</reference>\n";
	bml+="</description>\n";
	bml+="<tm id='tm1'/>\n";
	switch(cptSentences){
		case 0:	bml+="The animated conversationnal agent Greta, \n"; break;
		case 1:	bml+="is developped by Catherine Pelachaud's team.\n"; break;
		case 2:	bml+="Greta is represented by a three Dee women, \n"; break;
		case 3:	bml+="compatible with the animation M peg four.\n"; break;
		case 4:	bml+="Greta is able to use various behaviours \n"; break;
		case 5:	bml+="She can use both verbal and non-verbal behaviour.\n"; break;
	}
	cptSentences++;
	if(cptSentences>5) cptSentences=0;
//	bml+="blla \n";
//	bml+="<tm id='tm2'/>\n";
//	bml+="bla\n";
	bml+="<tm id='tm2' time='1'/>\n";
//	bml+="<pitchaccent id='xpa1' type='Hstar' start='s1:tm2' end='s1:tm4' level='medium' importance='0.5'/>\n";
//	bml+="<boundary id='b1' type='HH' start='s1:tm4' end='1.0'/>\n";
	bml+="</speech>\n";
	bml+="</bml>\n";
	rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);

}
void BehaviourSelector::HeadNod()
{
	std::string bml="";

	bml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
	bml+="<bml xmlns=\"http://www.mindmakers.org/projects/BML\">\n";
	bml+="	<head id=\"dg\"  start=\"0\" end=\"0.5\">\n";
	bml+="		<description level=\"1\" type=\"gretabml\">\n";
	bml+="		<reference>head=head_nod2</reference>\n";
	bml+="		<intensity>1.00</intensity>\n";
	bml+="			<FLD.value>0.00</FLD.value>\n";
	bml+="			<PWR.value>0.00</PWR.value>\n";
	bml+="			<REP.value>0.00</REP.value>\n";
	bml+="			<SPC.value>0.00</SPC.value>\n";
	bml+="			<TMP.value>1.00</TMP.value>\n";
	bml+="		</description>\n";
	bml+="	</head>\n";
	bml+="</bml>\n";

	rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);
	
}
void BehaviourSelector::Smile()
{
	std::string bml="";

	bml="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
	bml+="<bml xmlns=\"http://www.mindmakers.org/projects/BML\">\n";
	bml+="<face id=\"emotion-0\"  start=\"0\" end=\"1.5\" stroke=\"1\">\n";
	bml+="<description level=\"1\" type=\"gretabml\">\n";
	bml+="<reference>mouth=smile</reference>\n";
	bml+="<intensity>1.00</intensity>\n";
	bml+="</description>\n";
	bml+="</face>\n";
	bml+="</bml>\n";

	rbp->SendBehaviour(bml, GretaName+BML_MESSAGE_HEADER);

}

void BehaviourSelector::setOscActivation(float oscActivation)
{
	oscillActivation=oscActivation;
}