#-*- coding: iso-8859-15 -*-

''' Cartesian control: synchronization between speech and gestures for 3piecesofsky '''

import Config
import motion
import postureInit
import stiffnessOn
import time

def main():

    proxy = Config.loadProxy("ALMotion")
    #~ audio = Config.loadProxy("ALAudioPlayer") #this does not run with ChoreGraphe so it must be tested on the real NAO

    #Set NAO in stiffness On
    stiffnessOn.main()
    
    # send robot to Pose Init
    postureInit.main()

    space      = motion.SPACE_TORSO

    # Values of key-points of left arm in the Cartesian space corresponding to the symbolic gestures in the file 3piecesOfSky-1(2,3,5).txt
    leftpath       = [
     [0.113, 0.040, 0.215, 1.429, -0.978, -1.377],       # Point 1 
     [0.140, 0.052, 0.210, 1.027, -0.947, -0.894],       # Point 2
     [0.113, 0.040, 0.215, 1.429, -0.978, -1.377],       # Point 3
     [0.112, 0.074, 0.229, 1.088, -1.336, -1.164],       # Point 4
     [0.145, 0.076, 0.199, 0.379, -1.125, -0.466],       # Point 5
     [0.112, 0.074, 0.229, 1.088, -1.336, -1.164],       # Point 6
     [0.119, 0.160, 0.204, -1.456, -1.021, 1.293],       # Point 7 
     [0.151, 0.149, 0.189, -0.935, -0.922, 0.746],       # Point 8
     [0.119, 0.160, 0.204, -1.456, -1.021, 1.293],       # Point 9
     [0.119, 0.160, 0.204, -1.456, -1.021, 1.293],       # Point 10
     [0.119, 0.070, 0.228, 0.998, -1.238, -1.009],       # Point 11
     [0.104, 0.054, 0.226, -0.506, -1.238, -1.009],      # Point 12
     [0.088, 0.069, 0.234, -2.141, -1.238, -1.009],      # Point 13
     [0.133, 0.070, 0.009, 3.102, 0.194, -0.279]]        # Point 14

    # Values of key-points of right arm in the Cartesian space corresponding to the symbolic gestures in the file 3piecesOfSky-1(2,3,5).txt
    rightpath       = [
    [0.111, -0.135, 0.220, 1.414, -1.219, -1.357],
    [0.141, -0.142, 0.186, 0.935, -1.010, -0.889],
    [0.111, -0.135, 0.220, 1.414, -1.219, -1.357],
    [0.110, -0.081, 0.231, -1.268, -1.370, 1.172],
    [0.147, -0.086, 0.207, -0.449, -1.118, 0.341],
    [0.110, -0.081, 0.231, -1.268, -1.370, 1.172],
    [0.106, -0.048, 0.229, -1.691, -1.035, 1.432],
    [0.144, -0.057, 0.204, -1.103, -0.942, 0.840],
    [0.106, -0.048, 0.229, -1.691, -1.035, 1.432],
    [0.106, -0.048, 0.229, -1.691, -1.035, 1.432],
    [0.112, -0.073, 0.232, -1.299, -1.283, 1.161],
    [0.099, -0.056, 0.229, 0.241, -1.283, 1.161],
    [0.081, -0.070, 0.235, 1.934, -1.283, 1.161],
    [0.135, -0.083, 0.003, -3.130, 0.240, 0.146]]        # Point 14
    
    isAbsolute = True
    rightarm = "RArm"
    leftarm = "LArm"
    axisMask   = 63                             # control all (position and direction)
    fractionMaxSpeed = 0.1

    qLShoulderPitch = []
    qLShoulderRoll  = []
    qLElbowYaw      = []
    qLElbowRoll     = []
    qLWristYaw      = []
    qRShoulderPitch = []
    qRShoulderRoll  = []
    qRElbowYaw      = []
    qRElbowRoll     = []
    qRWristYaw      = []
    for i in range(14):
        proxy.post.positionInterpolation(leftarm, space, leftpath[i], axisMask, 2.0, isAbsolute)
        proxy.positionInterpolation(rightarm, space, rightpath[i], axisMask, 2.0, isAbsolute)
        time.sleep(0.1)
        angleLArm = proxy.getAngles(leftarm, False) # useSensors
        qLShoulderPitch.append(angleLArm[0])
        qLShoulderRoll.append(angleLArm[1])
        qLElbowYaw.append(angleLArm[2])
        qLElbowRoll.append(angleLArm[3])
        qLWristYaw.append(angleLArm[4])
        angleRArm = proxy.getAngles(rightarm, False) # useSensors
        qRShoulderPitch.append(angleRArm[0])
        qRShoulderRoll.append(angleRArm[1])
        qRElbowYaw.append(angleRArm[2])
        qRElbowRoll.append(angleRArm[3])
        qRWristYaw.append(angleRArm[4])
    
    # Les 3 durees marchent mais attention, le robot tombe avec la dernieres duree
    #~ timeTmp = [3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5] #seconds
    #~ timeTmp = [13.0, 13.5, 14.0, 14.5, 15.0, 15.5, 16, 16.5, 17, 17.5, 18, 18.5, 19, 19.5] #seconds
    timeTmp = [2.983, 3.183, 3.283, 3.459, 3.659, 3.759, 3.901, 4.201, 5.50713, 5.60713, 6.08, 7.08 , 8.08, 8.18]     
    nameDof = ["LShoulderPitch", "LShoulderRoll", "LElbowYaw", "LElbowRoll", "LWristYaw", "RShoulderPitch", "RShoulderRoll", "RElbowYaw", "RElbowRoll", "RWristYaw"]
    anglesArms = []
    anglesArms.append(qLShoulderPitch)
    anglesArms.append(qLShoulderRoll)
    anglesArms.append(qLElbowYaw)
    anglesArms.append(qLElbowRoll)
    anglesArms.append(qLWristYaw)
    anglesArms.append(qRShoulderPitch)
    anglesArms.append(qRShoulderRoll)
    anglesArms.append(qRElbowYaw)
    anglesArms.append(qRElbowRoll)
    anglesArms.append(qRWristYaw)
    
    times = []
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    times.append(timeTmp)
    print anglesArms
    proxy.angleInterpolation(nameDof, anglesArms, times, True) # is absolute

    postureInit.main()

if __name__ == "__main__":
    main()
