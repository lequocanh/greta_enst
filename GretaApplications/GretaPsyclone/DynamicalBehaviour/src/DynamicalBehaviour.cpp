
#include "DataContainer.h"
#include "DynamicalBehaviour.h"
#include "IniManager.h"
#include "Timer.h"
#include <stdio.h>
#include "XercesTool.h"
#include "XMLDOMTree.h"
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;
extern DataContainer *datacontainer;
extern IniManager inimanager;
extern CentralClock *pc;

DynamicalBehaviour::DynamicalBehaviour()
{	
	initPsycloneConnection();

	listenerdata=datacontainer->getListenerData();

	agentstate=new AgentState(listenerdata->agentstate);
	behaviourselector=new BehaviourSelector(this, pc, agentstate);
	behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
	behaviourselector->start();

    neurOscill=new neuronalOscillator();

	Greta2HeadMoves=0;
	HumanHeadMoves=0;
	headPosition=0;
	
	reactivebehaviourtrigger=new ReactiveBehaviourTrigger(agentstate);

	while(true)
	{
		
		Sleep(100);
		neurOscill->updateOscillator(); // � l'avenir l'oscillateur devra se g�rer seul en prenant en compte l'horloge.
		Dynamic2action();
		Greta2HeadMoves=0;
		HumanHeadMoves=0;
		ReadMessage(commint->ReceiveMessage(5));
		ReadInputMessages();
		neurOscill->setInhib((Greta2HeadMoves+HumanHeadMoves)*0.01);

		std::list<GretaLogger*>::iterator iterlog;

		for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
		{
			std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
			if((*iterlog)->getLevel()=="debug")
				this->commint->log->debug(msg);
			if((*iterlog)->getLevel()=="info")
				this->commint->log->info(msg);
			if((*iterlog)->getLevel()=="warn")
				this->commint->log->warn(msg);
			if((*iterlog)->getLevel()=="error")
				this->commint->log->error(msg);
		}
		listLog.clear();
		
	}
}

DynamicalBehaviour::~DynamicalBehaviour()
{
}


// Load the application parameter from the inimanager
int DynamicalBehaviour::initPsycloneConnection()
{	
	// Register Psyclone module adress
	moduleHost=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	// Register Psyclone module port
	modulePort=inimanager.GetValueInt("PSYCLONE_PORT");
	// Get Greta's Name
	GretaName=inimanager.GetValueString("GRETA_NAME").c_str();
	// Get Greta2's Name
	Greta2Name=inimanager.GetValueString("GRETA2_NAME").c_str();

	// Creation of the Psyclone inteface
	commint=new Psydule(GretaName+INPUTGRETA_MODULE_NAME,moduleHost,modulePort);
	
	// registered packet list
	std::list<std::string> registeredGretaDatatypes;
	// register to Greta Clock messages.
	registeredGretaDatatypes.push_back(GretaName+CLOCK_MESSAGE_HEADER);
	// register to Caracterisation Baseline Message.
	registeredGretaDatatypes.push_back(GretaName+AGENTSTATE_MESSAGE_HEADER);
	registeredGretaDatatypes.push_back(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER);
	// register to System Feedback
	registeredGretaDatatypes.push_back(GretaName+SYSTEM_FEEDBACK_MESSAGE_HEADER);
	// register to Cognitive Backchannel
	registeredGretaDatatypes.push_back(GretaName+COGNITIVE_FML_MESSAGE_HEADER);
	// register to other Greta's BML
	registeredGretaDatatypes.push_back(Greta2Name+BML_MESSAGE_HEADER);

	
	// registration GretaWhiteboard
	if(commint->Register(GretaName+GRETA_WHITEBOARD,registeredGretaDatatypes)!=0)
	{
		std::cout << GretaName+INPUTGRETA_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_OK << std::endl;		
	}
	else
	{
		std::cout << GretaName+INPUTGRETA_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_FAILED<< std::endl;
		commint=0;
		return 0;
	}
	std::cout << "Registration to GretaWhiteboard succesful" << std::endl;

	


// Creation of the Psyclone input module if it does not have already been created (by another instanciation of DynamicalBehaviour)
	moduleInput=new Psydule(GretaName+INPUTWATSON_MODULE_NAME,moduleHost,modulePort);

	std::list<std::string> registeredInputDatatypes;
		// register to Input messages, Watson
		registeredInputDatatypes.push_back(INPUT_MESSAGE_HEADER);

		// registration InputWhiteboard
		if(moduleInput->Register(INPUT_WHITEBOARD,registeredInputDatatypes)!=0)
		{
			std::cout << GretaName+INPUTWATSON_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_OK << std::endl;		
		}
		else
		{
			std::cout << GretaName+INPUTWATSON_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_FAILED<< std::endl;
			
			return 0;
		}
		std::cout << "Registration to InputeWhiteboard succesful" << std::endl;
		


	return 1;
}

void DynamicalBehaviour::Dynamic2action()
{
	float oscillActivation;
	float oscInhib;
	oscillActivation=neurOscill->getActivation();
	oscInhib=neurOscill->getInhib();
	printf("* Oscillateur * activation : %f, inhibition : %f\n", oscillActivation, oscInhib);
	SYSTEMTIME st;
	GetSystemTime(&st);
	int milliSec=st.wHour*3600000+st.wMinute*60000+st.wSecond*1000+st.wMilliseconds;
	FILE *OpenFile=fopen("C:/Users/Ken/Manips/OscillateursCouples/TwoGretas/FichierResGreta1","a"); // Results recording
	fprintf(OpenFile,"%i %f %f\n", milliSec, oscillActivation, oscInhib);
	fclose(OpenFile);
	behaviourselector->setOscActivation(oscillActivation);

	




/*	char v[255];
	
	std::map<std::string,DataBackchannel*> bmlfml;

	reactivebehaviourtrigger->addMessage(content);
	
	if(reactivebehaviourtrigger->messageList.size()>0)
		bmlfml=reactivebehaviourtrigger->ReceiveMessages();

	if(bmlfml["bml"]!=NULL)
		behaviourselector->updateBackchannelRequest(bmlfml["bml"]);

	if(bmlfml["fml"]!=NULL)
		behaviourselector->updateBackchannelRequest(bmlfml["fml"]);
*/
}


int DynamicalBehaviour::ReadMessage(CommunicationMessage *msg)
{
	std::string content, type;
	int time;
	if(msg==NULL) return(0);

	content=msg->getContent();
	type=msg->getType();
	if(strcmp(content.c_str(),"")!=0)
	{
		if(strcmp(type.c_str(),(GretaName+CLOCK_MESSAGE_HEADER).c_str())==0)
		{
			time=atoi(content.c_str());
			pc->SetTime(time);
		}
		else
		{
			//printf("message : %s /n",content.c_str());
			//if(strcmp(msg->type.c_str(),GretaName+COGNITIVE_FML_MESSAGE_HEADER)==0)
		//	behaviourselector->updateBackchannelRequest((std::string)content);
		//else

	// Used when two Gretas run on the same computer.

		if(strcmp(type.c_str(),(Greta2Name+BML_MESSAGE_HEADER).c_str())==0)
		{
			Greta2HeadMoves=testGretaHeadMoves((char *)content.c_str());
		} 



/* ***** Unused in Dynamical Behaviour *****

		if(strcmp(type.c_str(),(GretaName+SYSTEM_FEEDBACK_MESSAGE_HEADER).c_str())==0)
		{
			;//   da modificare rispettando la forma usata per inviare feedback (indica se un backchannel � stato eseguito)
		}
		else
		if(strcmp(type.c_str(),(GretaName+AGENTSTATE_MESSAGE_HEADER).c_str())==0)
		{
			agentstate->internalstate.LoadFromBuffer(content);
			behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
		}

		else
		if(strcmp(type.c_str(),(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER).c_str())==0)
		{
			std::string app=content;
			if(app.substr(0,app.find_first_of("="))=="emotion_state.emotion.name")
			{
					agentstate->setEmotionState(app.substr(app.find_first_of("=")+1));
			}
			else
			if(app.substr(0,app.find_first_of("="))=="mimicry_level.mimicry.value")
			{
				agentstate->internalstate.SetAttrValue("mimicry_level.mimicry.value", app.substr(app.find_first_of("=")+1));
				behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
//				behaviourselector->setMimicryPercentage(agentstate->getValue("mimicry_level.mimicry.value"));
			}
			if(app.substr(0,app.find_first_of("="))=="agent_identity.agent.name")
			{
				agentstate->setAgentName(app.substr(app.find_first_of("=")+1));
			}
			else
				agentstate->setSingle(app, "");
		}*/
		}
		return 1;
	}
	return 0;
}

int DynamicalBehaviour::ReadInputMessages()
{
	CommunicationMessage *msg=new CommunicationMessage();
	std::string content="", type;
	//	char v[255];
	int time=0;
	int end_time=0;

    HumanHeadMoves=0;


	//	std::map<std::string,DataBackchannel*> bmlfml;

	time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000);
	end_time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000)+100;

	//while(msg!=NULL)
	while(end_time>time)	
	{
		if(HumanHeadMoves==0)
		{
			msg=moduleInput->ReceiveMessage(10);

			if(msg!=NULL)
			{
				type=msg->getType();
				if(strcmp(type.c_str(),INPUT_MESSAGE_HEADER)==0)
				{
					content=msg->getContent();
					HumanHeadMoves=testHumanHeadMoves(content);
				}

				msg=moduleInput->ReceiveMessage(10);
				time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000);
			}
			else
			{
				return(1);
			}
		}
		else
		{
			return(1);
		}
	}
	// Using of backchannel rules: not used in DynamicalBehavior.
	/*	
	if(reactivebehaviourtrigger->messageList.size()>0)
	bmlfml=reactivebehaviourtrigger->ReceiveMessages();


	if(bmlfml["bml"]!=NULL)
	behaviourselector->updateBackchannelRequest(bmlfml["bml"]);

	if(bmlfml["fml"]!=NULL)
	behaviourselector->updateBackchannelRequest(bmlfml["fml"]);
	*/
	return(1);
}

void DynamicalBehaviour::SendBehaviour(std::string bmlfml, std::string messagetype)
{
	commint->PostString(bmlfml,GretaName+GRETA_WHITEBOARD, messagetype);
	commint->PostString(bmlfml,Greta2Name+GRETA_WHITEBOARD, messagetype);
}
std::string DynamicalBehaviour::getGretaName()
{	
	return GretaName;
}

neuronalOscillator* DynamicalBehaviour::getNeurOscill()
{
	return neurOscill;
}

float DynamicalBehaviour::testGretaHeadMoves(char * buffer)
{
	float Greta2Moves=0;
	
	//XMLGenericParser *p;
	XMLDOMParser *p;
	XMLGenericTree *t;
	//std::list<XMLGenericTree*>::iterator iterch;
	//std::list<XMLGenericTree*>::iterator iterbl;

	//p=new XMLGenericParser();
	p=new XMLDOMParser();


	p->SetValidating(true);
	t=p->ParseBuffer(buffer);


	if(t!=0)
	{
		for(XMLGenericTree::iterator iterch=t->begin();iterch!=t->end();++iterch)
		{			
			if ((*iterch)->isTextNode()) continue;
			std::string modality=(*iterch)->GetName();
			if(modality=="head")
			{
				Greta2Moves=(float)1;	
			}

		}
	}
	else
	{
		printf("Error parsing BML in DynamicalBehavior \n");
		return 0;
	}

	return Greta2Moves;

}

float DynamicalBehaviour::testHumanHeadMoves(std::string content)
{
	float HumanMoves=0;
	if(strcmp(content.c_str(),"")!=0)
	{
		std::string name, state;
		std::string c;
		std::string::size_type pos=0,pos1=0; 
		c=content.c_str();
		pos=c.find_first_of(";",0);
		name = c.substr(0, pos);
		printf("Name %s ", name.c_str());	
		//tempString = std::string(_strupr((char *)tempString.c_str()));
		
		//if(newinputdata->name=="POSES")
		//	HeadMovement(newinputdata);
		
		if(strcmp(name.c_str(),"Head:Nods")==0)
		{
			
			pos1=c.find_first_of(';',pos+1);
			pos=c.find_first_of(';',pos1+1);
			state = c.substr(pos1+1, pos-1-pos1);
//			printf("State %s ", state.c_str());	
			if(strcmp(state.c_str(),"state:1")==0)
			{
				HumanMoves=(float)1;		
			}
		}

		if(strcmp(name.c_str(),"Head:Poses")==0)
		{
			double position=0;
			pos1=c.find("rx:",0);
			pos=c.find_first_of(";",pos1+1);
//			printf("Rotation %s \n",(c.substr(pos1+3, pos-pos1-3)).c_str());
			position = atof((c.substr(pos1+3, pos-3-pos1)).c_str());
//			printf("Position %Lf et headPosition %Lf \n", position, headPosition);	
			if(std::abs(position-headPosition)>0.05)
			{
				HumanMoves=(float)1;	
				headPosition=position;
			}
		}

	}
//	printf("Human moves : %f \n",HumanMoves); 
	return HumanMoves;
}

void DynamicalBehaviour::CheckAdaptRules(InputData *data)
{
	std::map<std::string, AdaptRules>::iterator iterrule;
	std::vector<Implications>::iterator iterimp;
	std::vector<Operations>::iterator itercons;

	iterrule = listenerdata->mapAdaptRules.find(data->name);


	//check all implication in the rule
	for(iterimp=(*iterrule).second.vecImplication.begin();iterimp!=(*iterrule).second.vecImplication.end();iterimp++)
	{
		if((*iterimp).EvaluateImplication(data)==1)
		{
			for(itercons=(*iterimp).vecConsequence.begin();itercons!=(*iterimp).vecConsequence.end(); itercons++)
				(*itercons).ComputeOperation(data);
		}
	}
	
	for(itercons=(*iterrule).second.vecModify.begin();itercons!=(*iterrule).second.vecModify.end(); itercons++)
		(*itercons).ComputeOperation(data);
}
