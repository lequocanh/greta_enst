//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ConsNode.h: interface for the ConsNode class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>

//#include  "MultimodalSignal.h"

/**
* class :ConsNode
*
*/


class ShortSignal
{
public:

	std::string label;
	float start;
	float end;
	int id;

	ShortSignal(){
		label="";
		start=-1;
		end=-1;
		id=-1;
	}

	virtual ~ShortSignal(){}

};

enum oper {unknown,rigthinc,include,exclude,precede,exist, morethan,lessthan,equal, and, or, not};

//enum relational {rigthinc,include,exclude,precede,exist};
//enum artmetic {more,less,equal};
//enum logical {and, or, not};

enum type {none, start, end};

class ConsNode 
{

 public:

	std::vector<int> concerns;

 private:
	
    //left argument
	ConsNode *left;

	//right argument
	ConsNode *right;
	
	//id the argument
	int arg_id;	

	//value (if left and right == null)
	float start_time;

	//value (if left and right == null)
	float stop_time;

	//value (if left and right == null)
	float value;
	
	//unknown
	//start
	//stop
	type my_type;
	
	//operator
	oper my_oper;
	
public:

	/**
	* contructor 
	*
	*/

	ConsNode ();

	/**
	* destructor 
	*/

	virtual ~ConsNode ();

	/**
	*  
	* verifies a condition
	*
	* @param other - left argument
	*/

	int exists(ConsNode* other);

	/**
	*  
	* verifies a condition
	*
	* @param other - left argument
	*/

	int isPreceded(ConsNode* other);

	/**
	*  
	* verifies a condition
	*
	* @param other - left argument
	*/

	int isIncluded(ConsNode* other);

	/**
	*  
	* verifies a condition
	*
	* @param other - left argument
	*/

	int excludes(ConsNode* other);

	/**
	*  
	* verifies a condition
	*
	* @param other - left argument
	*/

	int isLeftIncluded(ConsNode* other);

	/**
	*  
	* sets the operator type for the node
	*
	* @param  type1 - operator name
	*/

	void setConsNodeOperator(std::string type1);
	
	/**
	*  
	* sets the operator type for the node
	*
	* @return operator
	*/

	std::string getConsNodeOperatorToString();

	/**
	*  
	* gets the operator type of the node
	*
	* @return operator type
	*/


	oper getConsNodeOperator();
	

	/**
	*  
	* sets the node
	*
	* @param  id1 - signal id
	* @param  type1 - cons type (start/stop/unknown)
	*/

	void setArg(int id1, std::string type1);
	
	/**
	*  
	* sets the node
	*
	* @param  id1 - signal id
	*/

	void setArg(int id1);

	/**
	*  
	* 
	*
	* @return 
	*/

	void setLeft(ConsNode *temp){	
		//check maybe clone?	
		left=temp;
	}	
	
	/**
	*  
	* sets the node
	*
	* @param  id1 - signal id
	*/

	void setRight(ConsNode *temp){		
		//check maybe clone?	
		right=temp;
	}


	/**
	*  
	* gets the left son
	*
	* @return 
	*/

	ConsNode* getLeft(){return left;}	
	
	/**
	*  
	* gets the right son
	*
	* @param  id1 - signal id
	*/

	ConsNode* getRight(){return right;}	


	/**
	*  
	* sets the node
	*
	* @param  id1 - signal id
	*/

	int getArgId();

	/**
	*  
	* 
	*
	* @return 
	*/


	std::string getArgType();

	/**
	*  
	* 
	*
	* @return 
	*/


	void setValue(float value1);

	/**
	*  
	* 
	*
	* @return 
	*/

	float getValue();


	/**
	*  evaluates a cons tree
	*
	* @param id - ??? 
	* @param temp_animation - list of signals that were chosen
	* @return int 1 if satisfied, 1 if dont know, 0 if not satisfied, 0 if exception
	*/

	int evaluate(int id, std::vector<ShortSignal*>* temp_animation);

	/**
	*  evaluates an aperator
	* 
	*
	* @return int 1 if satisfied, 1 if dont know, 0 if not satisfied, 0 if exception
	*/

	bool booleanop(bool l, oper oper1 , bool r);


	/**
	*  fill a tree
	* 
	*
	* @return void
	*/

	void ConsNode::fill(int id,float start_time, float stop_time);

	
	/**
	*  clear the start and end times
	* 
	*
	* @return void
	*/

	void ConsNode::clean();


	/**
	*  fills left part of the rule
	* 
	*
	* @return void
	*/

	void fill_left(int id,float start_time, float stop_time);
	
	/**
	*  fills right part of the rule, used?
	* 
	*
	* @return void
	*/
	void fill_right(int id, float start_time, float stop_time);

    /**
	*  
	* 
	*
	* @return start time
	*/
	float getStartTime() {return start_time;}
	
	/**
	*  
	* 
	*
	* @return stop time
	*/
	float getStopTime() {return stop_time;}

	/**
	*  
	* 
	*
	* @return void
	*/

	void setStartTime(float time) {
		this->start_time=time;
	}
	
	/**
	*  
	* 
	*
	* @return void
	*/
	void setStopTime(float time) {
		this->stop_time=time;
	}

    /**
	*  copies element
	* 
	*
	* @return a new element
	*/
	ConsNode* clone();


	/**
	*  self-explained
	* 
	*
	* @param mtemp a type
	*/

void setArgType(type mtemp) {	
	
	this->my_type=mtemp;}

};// end of class declaration