// Shared-Attention system
// Christopher Peters


#ifndef _ATTENTION_H
#define _ATTENTION_H

#include <stdio.h>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>

#include "objects.h"
#include "fixation.h"

class VAO;

//an attention entry ties together multiple fixations
struct AttentionEntry
{
	long startTime;
	long endTime;
	VAO *vao;

	bool outsideScreen;
	int whereOutside;

	bool atUser;
};

class VAS;
class VAO	//virtual attention object - outside of screen can also be represented by one
{
	public:
		VAO(Named3DObject *named3dobj, bool outsideScreen = false);
		~VAO();

		void update(long currentTime, bool sameVAOAsPreviousFixation = false); //adds a new entry to this VAO
			//sameAsPrevious means that this VAO was the same one paid attention to during the last fixation as well

		Named3DObject *named3dobj;
		std::vector<AttentionEntry *> lookatEntries;
		long totalTimeLookedAt;
		
		int TimeLookedAt;

		VAS *vas;	//VAS that this VAO is a member of, NULL if none

		bool outsideScreen; //does this VAO represent the outside area of the screen?
};

class VAS	//virtual attention set
{
	public:
		VAS();
		~VAS();
	

};

//one of these for the user and one for the agent
class AttentionTimeLine
{
	public:
		AttentionTimeLine();
		~AttentionTimeLine();

		std::vector<AttentionEntry *> attentionEntries; 
};

#endif