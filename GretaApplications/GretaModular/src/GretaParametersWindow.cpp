//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// GretaParametersWindow.cpp: implementation of the GretaParametersWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "GretaParametersWindow.h"
#include <stdio.h>
#include <string.h>
#include <list>
#include <string>
#include <fl/fl_widget.h>
#include <FL/fl_draw.H>
#include <FL/Fl_File_Chooser.H>
#include "APML_AgentEngine.h"
#include "FML-APML_AgentEngine.h"
#include "BML_AgentEngine.h"
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;
extern FILE *all_logfile;

extern IniManager inimanager;
extern std::string ini_filename;

void selectedliptensionlight(Fl_Widget* w, GretaParametersWindow* v)
{
	v->liptensionnormal->clear();
	v->liptensionstrong->clear();
	v->liptensionlight->set();

}
void selectedliptensionnormal(Fl_Widget* w, GretaParametersWindow* v)
{
	v->liptensionlight->clear();
	v->liptensionstrong->clear();
	v->liptensionnormal->set();

}
void selectedliptensionstrong(Fl_Widget* w, GretaParametersWindow* v)
{
	v->liptensionnormal->clear();
	v->liptensionlight->clear();
	v->liptensionstrong->set();

}

void selectedarticulationhypo(Fl_Widget* w, GretaParametersWindow* v)
{
	v->articulationmedium->clear();
	v->articulationhyper->clear();
	v->articulationhypo->set();

}
void selectedarticulationmedium(Fl_Widget* w, GretaParametersWindow* v)
{
	v->articulationhypo->clear();
	v->articulationhyper->clear();
	v->articulationmedium->set();

}
void selectedarticulationhyper(Fl_Widget* w, GretaParametersWindow* v)
{
	v->articulationmedium->clear();
	v->articulationhypo->clear();
	v->articulationhyper->set();

}
void selectedlipaddemotion(Fl_Widget* w, GretaParametersWindow* v)
{
	v->lipdataemotion->clear();
	v->lipaddemotion->set();

}
void selectedlipdataemotion(Fl_Widget* w, GretaParametersWindow* v)
{
	v->lipaddemotion->clear();
	v->lipdataemotion->set();

}

void selectedgazebn(Fl_Widget* w, GretaParametersWindow* v)
{
	if(((Fl_Button*)w)->value()==0)
	{
		v->MAX_T_S1_L1->hide();
		v->MAX_T_S1->hide();
		v->MAX_T_L1->hide();
		v->MAX_T_S0->hide();
		v->MAX_T_L0->hide();
		v->DT->hide();
		v->LIMIT->hide();
		v->SCALE->hide();
	}
	else
	{
		v->MAX_T_S1_L1->show();
		v->MAX_T_S1->show();
		v->MAX_T_L1->show();
		v->MAX_T_S0->show();
		v->MAX_T_L0->show();
		v->DT->show();
		v->LIMIT->show();
		v->SCALE->show();
		v->gazediagram->value(0);
		v->gazediagram->hide();
	}
}

void selectedarminterpeuler(Fl_Widget* w, GretaParametersWindow* v)
{
	v->arminterpquat->clear();
	v->arminterppath->clear();
	v->arminterpeuler->set();

}
void selectedarminterpquat(Fl_Widget* w, GretaParametersWindow* v)
{
	v->arminterpeuler->clear();
	v->arminterppath->clear();
	v->arminterpquat->set();

}
void selectedarminterppath(Fl_Widget* w, GretaParametersWindow* v)
{
	v->arminterpeuler->clear();
	v->arminterpquat->clear();
	v->arminterppath->set();

}
void selectedwristinterpeuler(Fl_Widget* w, GretaParametersWindow* v)
{
	v->wristinterpquat->clear();
	v->wristinterpeuler->set();

}
void selectedwristinterpquat(Fl_Widget* w, GretaParametersWindow* v)
{
	v->wristinterpeuler->clear();
	v->wristinterpquat->set();
}
void selectedOAC(Fl_Widget* w, GretaParametersWindow* v)
{
	char value[255];
	sprintf_s(value,255,"%.2f",v->OAC->value());
	v->OACoutput->value(value);
}

void selectedTMP(Fl_Widget* w, GretaParametersWindow* v)
{
	char value[255];
	sprintf_s(value,255,"%.2f",v->TMP->value());
	v->TMPoutput->value(value);
}
void selectedSPC(Fl_Widget* w, GretaParametersWindow* v)
{
	char value[255];
	sprintf_s(value,255,"%.2f",v->SPC->value());
	v->SPCoutput->value(value);
}
void selectedFLD(Fl_Widget* w, GretaParametersWindow* v)
{
	char value[255];
	sprintf_s(value,255,"%.2f",v->FLD->value());
	v->FLDoutput->value(value);
}
void selectedPWR(Fl_Widget* w, GretaParametersWindow* v)
{
	char value[255];
	sprintf_s(value,255,"%.2f",v->PWR->value());
	v->PWRoutput->value(value);
}

void selectedREP(Fl_Widget* w, GretaParametersWindow* v)
{
	char value[255];
	sprintf_s(value,255,"%.2f",v->REP->value());
	v->REPoutput->value(value);
}
void selectedstartengine(Fl_Widget* w, GretaParametersWindow* v)
{
	v->SaveParameters();

	inimanager.ReadIniFile(ini_filename);
	
	if(strcmp(v->APMLfilename->value(),"")!=0)
	{
		APML_AgentEngine AAE;
		AAE.CalculateTurn((char*)v->APMLfilename->value());
		v->FMLAPMLfilename->value("");
		v->BMLfilename->value("");
		v->baselinefilename->value("");
	}
	
	if((strcmp(v->FMLAPMLfilename->value(),"")!=0)&&(strcmp(v->baselinefilename->value(),"")!=0))
	{
		FMLAPML_AgentEngine AAE;
		//AAE.Execute("d:/Greta/Propre/Greta/bin/apml/fml-apml_what.xml","d:/Greta/Propre/Greta/bin/mmsystem/baseline1.xml","");
		AAE.Execute((char*)v->FMLAPMLfilename->value(),(char*)v->baselinefilename->value(),0,0);

		v->PrintLog();
	
		v->APMLfilename->value("");
		v->BMLfilename->value("");
		
	}
	
	if((strcmp(v->FMLAPMLfilename->value(),"")!=0)&&(strcmp(v->baselinefilename->value(),"")==0))
	{
		std::string tymczas=inimanager.Program_Path+inimanager.GetValueString("BASELINE_DEFAULT");
		v->baselinefilename->value(tymczas.c_str());
		FMLAPML_AgentEngine AAE;
		//AAE.Execute("d:/Greta/Propre/Greta/bin/apml/fml-apml_what.xml","d:/Greta/Propre/Greta/bin/mmsystem/baseline1.xml","");
		AAE.Execute((char*)v->FMLAPMLfilename->value(),(char*)v->baselinefilename->value(),0,0);

		v->PrintLog();

		v->APMLfilename->value("");		
		v->BMLfilename->value("");
	}

	if(strcmp(v->BMLfilename->value(),"")!=0)
	{
		BML_AgentEngine BAE;
		BAE.Execute(v->BMLfilename->value(),0,1);

		v->PrintLog();

		v->FMLAPMLfilename->value("");
		v->APMLfilename->value("");
		v->baselinefilename->value("");
	}

}

void selectedstartplayer(Fl_Widget* w, GretaParametersWindow* v)
{
	v->SaveParameters();
	if(!inimanager.initialized)
		inimanager.ReadIniFile(ini_filename);		

	Fl::gl_visual(FL_RGB|FL_DOUBLE|FL_DEPTH);

	/*
	GlutMaster			*glutMaster;
	Player				*AgentPlayer;

	glutMaster   = new GlutMaster();

	if(v->capturevideo->value()==1)
		AgentPlayer  = new Player(glutMaster,true,false,0);
	else
		AgentPlayer  = new Player(glutMaster,false,false,0);
	
	while(!AgentPlayer->player_finished)
	{
		glutCheckLoop();
	}

	//delete AgentPlayer;
	delete glutMaster;
	*/
	
	if((PlayerFLTKWindow*)v->player!=0)
		if(!((PlayerFLTKWindow*)(v->player))->shown())
		{
			((PlayerFLTKWindow*)(v->player))->hide();
			delete ((PlayerFLTKWindow*)(v->player));
			v->player=0;
		}
	
	if(v->player==0){
		v->player=new PlayerFLTKWindow();
		((PlayerFLTKWindow*)(v->player))->show();
	}
}

void selectedquit(Fl_Widget* w, GretaParametersWindow* v)
{
	if((PlayerFLTKWindow*)v->player!=0)
		((PlayerFLTKWindow*)(v->player))->hide();
	v->SaveParameters();
	v->hide();
}

void selectedbrowseAPML(Fl_Widget* w, GretaParametersWindow* v)
{
	printf("%s\n", inimanager.Program_Path.c_str());
	Fl_File_Chooser *chooser;
	chooser= new Fl_File_Chooser((inimanager.Program_Path+"apml").c_str(),"APML files (*.xml)",FL_SINGLE,"select an APML file");
	chooser->show();
	while (chooser->shown())
		Fl::wait();
	//char path[1024];
	//fl_filename_relative(path,1024,chooser->value());
	//v->filename->value(path);
	v->APMLfilename->value(chooser->value());
	delete chooser;
}

void selectedbrowseFMLAPML(Fl_Widget* w, GretaParametersWindow* v)
{
	Fl_File_Chooser *chooser;
	chooser= new Fl_File_Chooser((inimanager.Program_Path+"fml").c_str(),"FML-APML files (*.xml)",FL_SINGLE,"select an APML file");
	chooser->show();
	while (chooser->shown())
		Fl::wait();
	//char path[1024];
	//fl_filename_relative(path,1024,chooser->value());
	//v->filename->value(path);
	v->FMLAPMLfilename->value(chooser->value());
	delete chooser;
}

void selectedbrowsebaseline(Fl_Widget* w, GretaParametersWindow* v)
{
	Fl_File_Chooser *chooser;
	chooser= new Fl_File_Chooser((inimanager.Program_Path+"mmsystem").c_str(),"baseline files (*.xml)",FL_SINGLE,"select a baseline file");
	chooser->show();
	while (chooser->shown())
		Fl::wait();
	//char path[1024];
	//fl_filename_relative(path,1024,chooser->value());
	//v->filename->value(path);
	v->baselinefilename->value(chooser->value());
	delete chooser;
}

void selectedbrowseBML(Fl_Widget* w, GretaParametersWindow* v)
{
	Fl_File_Chooser *chooser;
	chooser= new Fl_File_Chooser((inimanager.Program_Path+"bml").c_str(),"BML files (*.xml)",FL_SINGLE,"select a BML file");
	chooser->show();
	while (chooser->shown())
		Fl::wait();
	//char path[1024];
	//fl_filename_relative(path,1024,chooser->value());
	//v->filename->value(path);
	v->BMLfilename->value(chooser->value());
	delete chooser;
}

/*
void selectedfaceanimation_normal(Fl_Widget* w, GretaParametersWindow* v)
{
	v->faceanimation_TestMau->clear();
	v->faceanimation_normal->set();
}

void selectedfaceanimation_TestMau(Fl_Widget* w, GretaParametersWindow* v)
{
	v->faceanimation_normal->clear();
	v->faceanimation_TestMau->set();

}
*/

void selectedmaryconnect(Fl_Widget* w, GretaParametersWindow* v)
{

}

void selectedmary(Fl_Widget* w, GretaParametersWindow* v)
{
	v->euler->clear();
	v->acapela->clear();
	v->festival->clear();
}

void selectedfestival(Fl_Widget* w, GretaParametersWindow* v)
{
	v->euler->clear();
	v->acapela->clear();
	v->mary->clear();
}

void selectedeuler(Fl_Widget* w, GretaParametersWindow* v)
{
	v->festival->clear();
	v->acapela->clear();
	v->mary->clear();
}

void selectedacapela(Fl_Widget* w, GretaParametersWindow* v)
{
	v->festival->clear();
	v->euler->clear();
	v->mary->clear();
}

void selectedrealspeech(Fl_Widget* w, GretaParametersWindow* v)
{
	v->euler->clear();
	v->festival->clear();
	v->acapela->clear();
	v->mary->clear();
}

void GretaParametersWindow::SaveParameters()
{
	inimanager.SetValueString("PLAYER_WIN_WIDTH",std::string(playerwidth->value()));
	inimanager.SetValueString("PLAYER_WIN_HEIGHT",std::string(playerheight->value()));
	inimanager.SetValueString("SPEECH_STRETCH",std::string(speechstretch->value()));

	inimanager.SetValueString("HEAD_NOISE",std::string(headnoise->value()));
	
	if(conflicts->value()==1)
		inimanager.SetValueString("CONFLICTS_RESOLVER",std::string("1"));
	else
		inimanager.SetValueString("CONFLICTS_RESOLVER",std::string("0"));

	if(festival->value()==1)
		inimanager.SetValueString("FESTIVAL",std::string("1"));
	else
		inimanager.SetValueString("FESTIVAL",std::string("0"));
	inimanager.SetValueString("FESTIVAL_LANGUAGE",festival_language->text());

	if(mary->value()==1)
		inimanager.SetValueString("MARY",std::string("1"));
	else
		inimanager.SetValueString("MARY",std::string("0"));

	inimanager.SetValueString("MARY_LANGUAGE",mary_language->text());

	if(acapela->value()==1)
		inimanager.SetValueString("ACAPELA",std::string("1"));
	else
		inimanager.SetValueString("ACAPELA",std::string("0"));

	inimanager.SetValueString("ACAPELA_LANGUAGE",acapela_language->text());

	if(euler->value()==1)
		inimanager.SetValueString("EULER",std::string("1"));
	else
		inimanager.SetValueString("EULER",std::string("0"));

	inimanager.SetValueString("EULER_LANGUAGE",euler_language->text());

	/*
	if(euler_frenchmale->value()==1)
		inimanager.SetValueString("EULER_LANGUAGE",std::string("FRENCH_MALE1"));
	if(euler_frenchfemale->value()==1)
		inimanager.SetValueString("EULER_LANGUAGE",std::string("FRENCH_FEMALE1"));*/

	//if(realspeech->value()==1)
	//	inimanager.SetValueString("REALSPEECH",std::string("1"));
	//else
	//	inimanager.SetValueString("REALSPEECH",std::string("0"));

	if(gazebn->value()==1)
		inimanager.SetValueString("GAZE_MODEL_BN",std::string("1"));
	else
		inimanager.SetValueString("GAZE_MODEL_BN",std::string("0"));
	
	inimanager.SetValueString("ENGINE_MAX_T_S1_L1",std::string(MAX_T_S1_L1->value()));
	inimanager.SetValueString("ENGINE_MAX_T_S1",std::string(MAX_T_S1->value()));
	inimanager.SetValueString("ENGINE_MAX_T_L1",std::string(MAX_T_L1->value()));
	inimanager.SetValueString("ENGINE_MAX_T_S0",std::string(MAX_T_S0->value()));
	inimanager.SetValueString("ENGINE_MAX_T_L0",std::string(MAX_T_L0->value()));
	inimanager.SetValueString("ENGINE_DT",std::string(DT->value()));
	inimanager.SetValueString("ENGINE_SCALE",std::string(SCALE->value()));

	inimanager.SetValueInt("DRAW_GAZE_CURVES",gazediagram->value());

	if(liptensionlight->value()==1)
		inimanager.SetValueString("ENGINE_LIP_WEIGHT",std::string("LIGHT"));
	if(liptensionnormal->value()==1)
		inimanager.SetValueString("ENGINE_LIP_WEIGHT",std::string("NORMAL"));
	if(liptensionstrong->value()==1)
		inimanager.SetValueString("ENGINE_LIP_WEIGHT",std::string("STRONG"));

	if(articulationhypo->value()==1)
		inimanager.SetValueString("ENGINE_LIP_FLOW",std::string("BOUND"));
	if(articulationmedium->value()==1)
		inimanager.SetValueString("ENGINE_LIP_FLOW",std::string("MEDIUM"));
	if(articulationhyper->value()==1)
		inimanager.SetValueString("ENGINE_LIP_FLOW",std::string("FREE"));

	if(lipaddemotion->value()==1)
		inimanager.SetValueString("LIP_EMOTION_MODEL",std::string("ADD"));
	else
		if(lipdataemotion->value()==1)
			inimanager.SetValueString("LIP_EMOTION_MODEL",std::string("DATA"));
		else
			inimanager.SetValueString("LIP_EMOTION_MODEL",std::string(""));

	std::string lipparams;
	lipparams="";
	if(lipparLSO->value()==1)
		lipparams+="LSO,";
	if(lipparJAW->value()==1)
		lipparams+="JAW,";
	if(lipparLSW->value()==1)
		lipparams+="LSW,";
	if(lipparULP->value()==1)
		lipparams+="ULP,";
	if(lipparLLP->value()==1)
		lipparams+="LLP,";
	if(lipparCRL->value()==1)
		lipparams+="CRL,";
	if(lipparCRR->value()==1)
		lipparams+="CRR,";
	if(lipparams=="")
		lipparams="NONE";
	inimanager.SetValueString("ENGINE_LIP_PARAMETERS",lipparams);

	inimanager.SetValueString("ENGINE_EXPR_OAC",std::string(OACoutput->value()));
	inimanager.SetValueString("ENGINE_EXPR_SPC",std::string(SPCoutput->value()));
	inimanager.SetValueString("ENGINE_EXPR_TMP",std::string(TMPoutput->value()));
	inimanager.SetValueString("ENGINE_EXPR_FLD",std::string(FLDoutput->value()));
	inimanager.SetValueString("ENGINE_EXPR_PWR",std::string(PWRoutput->value()));
	inimanager.SetValueString("ENGINE_EXPR_REP",std::string(REPoutput->value()));
	
	if(arminterpeuler->value()==1)
		inimanager.SetValueString("ENGINE_ARM_INTERP_TYPE",std::string("0"));

	if(arminterpquat->value()==1)
		inimanager.SetValueString("ENGINE_ARM_INTERP_TYPE",std::string("1"));

	if(arminterppath->value()==1)
		inimanager.SetValueString("ENGINE_ARM_INTERP_TYPE",std::string("2"));
	
	if(wristinterpeuler->value()==1)
		inimanager.SetValueString("ENGINE_WRIST_ORIENTATION_INTERP_TYPE",std::string("0"));

	if(wristinterpquat->value()==1)
		inimanager.SetValueString("ENGINE_WRIST_ORIENTATION_INTERP_TYPE",std::string("1"));

	if(jointslimits->value()==1)
		inimanager.SetValueString("ENGINE_USE_JOINT_LIMITS",std::string("1"));
	else
		inimanager.SetValueString("ENGINE_USE_JOINT_LIMITS",std::string("0"));

	if(torsonoise->value()==1)
		inimanager.SetValueString("ENGINE_USE_PERLIN_TORSO_NOISE",std::string("1"));
	else
		inimanager.SetValueString("ENGINE_USE_PERLIN_TORSO_NOISE",std::string("0"));

	/*
	if(preloadagent->value()==1)
		inimanager.SetValueString("PLAYER_PRELOADAGENT",std::string("1"));
	else
		inimanager.SetValueString("PLAYER_PRELOADAGENT",std::string("0"));
	*/

	if(onlyface->value()==1)
		inimanager.SetValueString("PLAYER_ONLYFACE",std::string("1"));
	else
		inimanager.SetValueString("PLAYER_ONLYFACE",std::string("0"));

	inimanager.SetValueString("CHARACTER_SPEAKER",characters_speaker[character_speaker->value()]);

	inimanager.SetValueString("CHARACTER_LISTENER",characters_listener[character_listener->value()]);

	/*
	if(faceanimation_TestMau->value()==1)
		inimanager.SetValueInt("FACEANIMATIONTESTMAU",1);
	else
		inimanager.SetValueInt("FACEANIMATIONTESTMAU",0);
	*/

	if(showlistener->value()==1)
		inimanager.SetValueInt("PLAYER_SHOWLISTENER",1);
	else
		inimanager.SetValueInt("PLAYER_SHOWLISTENER",0);

	if(showlistenersubwindow->value()==1)
		inimanager.SetValueInt("PLAYER_LISTENERSUBWINDOW",1);
	else
		inimanager.SetValueInt("PLAYER_LISTENERSUBWINDOW",0);

	inimanager.WriteIniFile(ini_filename);

}

void gretaparameterswindowclosed(GretaParametersWindow* v)
{
}

GretaParametersWindow::GretaParametersWindow():Fl_Double_Window(100,100,600,520,"greta agent 2.1")
{
	Fl::visual(FL_DOUBLE|FL_INDEX);

	int i;
	char value[255];

	if(!inimanager.initialized)
		inimanager.ReadIniFile(ini_filename);		

	if(!inimanager.initialized)
		printf("couldn't read inifile\n");

	fl_font(FL_HELVETICA,8);
	fl_color(255,255,255);
	conflicts=new Fl_Check_Button(145,5,20,20,"resolve face conficts");
	conflicts->align(FL_ALIGN_LEFT);
	conflicts->value(inimanager.GetValueInt("CONFLICTS_RESOLVER"));

	
	//speechbox->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT|FL_ALIGN_TOP);

	
	speechstretch=new Fl_Input(110,25,50,20,"speech stretch");
	speechstretch->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("SPEECH_STRETCH"));
	speechstretch->value(value);

	//speechbox=new Fl_Box(FL_BORDER_BOX,5,60,164,64,"");

	festival=new Fl_Check_Button(2,70,20,20,"Festival");
	festival->align(FL_ALIGN_TOP_LEFT);
	festival->value(inimanager.GetValueInt("FESTIVAL"));
	festival->callback((Fl_Callback *)selectedfestival,this);

	festival_language=new Fl_Choice(25,70,150,20);
	festival_language->add("US_ENGLISH_FEMALE1");
	festival_language->add("US_ENGLISH_MALE1");
	festival_language->add("US_ENGLISH_MALE2");
	festival_language->add("ITALIAN");
	festival_language->add("POLISH");
	festival_language->add("SWEDISH");
	festival_language->value(festival_language->find_item(inimanager.GetValueString("FESTIVAL_LANGUAGE").c_str()));

	mary=new Fl_Check_Button(2,105,20,20,"Mary");
	mary->align(FL_ALIGN_TOP_LEFT);
	mary->value(inimanager.GetValueInt("MARY"));
	mary->callback((Fl_Callback *)selectedmary,this);

	mary_language=new Fl_Choice(25,105,150,20);
	mary_language->add("ENGLISH_FEMALE1");
	mary_language->add("ENGLISH_MALE1");
	mary_language->add("GERMAN_FEMALE1");
	mary_language->add("GERMAN_MALE1");
	mary_language->value(mary_language->find_item(inimanager.GetValueString("MARY_LANGUAGE").c_str()));

	
	acapela=new Fl_Check_Button(2,140,20,20,"Acapela");
	acapela->align(FL_ALIGN_TOP_LEFT);
	acapela->value(inimanager.GetValueInt("ACAPELA"));
	acapela->callback((Fl_Callback *)selectedacapela,this);

	acapela_language=new Fl_Choice(25,140,150,20);
	acapela_language->add("english", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Lucy");
	acapela_language->add("Peter", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("us-english", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Heather");
	acapela_language->add("Ryan", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("french", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Julie");
	acapela_language->add("Bruno", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("italian", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Chiara");
	acapela_language->add("Vittorio", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("german", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Julia");
	acapela_language->add("Klaus", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("spanish", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Maria");
	acapela_language->add("Antonio", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("polish", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Ania", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("dutch", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Femke");
	acapela_language->add("Max", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("swedish", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Emma");
	acapela_language->add("Elin", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("arabic", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Salma");
	acapela_language->add("Youssef", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("greek", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Dimitris", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("turkish", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Ipek", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("russian", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Alyona", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->add("portuguese ", 0, NULL, 0, FL_MENU_INACTIVE);
	acapela_language->add("Celia", 0, NULL, 0, FL_MENU_DIVIDER);
	acapela_language->value(acapela_language->find_item(inimanager.GetValueString("ACAPELA_LANGUAGE").c_str()));


	euler=new Fl_Check_Button(2,175,20,20,"Euler");
	euler->align(FL_ALIGN_TOP_LEFT);
	euler->value(inimanager.GetValueInt("EULER"));
	euler->callback((Fl_Callback *)selectedeuler,this);

	euler_language=new Fl_Choice(25,175,150,20);
	euler_language->add("FRENCH_FEMALE1");
	euler_language->add("FRENCH_MALE1");
	euler_language->value(euler_language->find_item(inimanager.GetValueString("EULER_LANGUAGE").c_str()));


//	realspeech=new Fl_Check_Button(145,170,20,20,"realspeech");
//	realspeech->align(FL_ALIGN_LEFT);
//	realspeech->value(inimanager.GetValueInt("REALSPEECH"));
//	realspeech->callback((Fl_Callback *)selectedrealspeech,this);

	lipbox=new Fl_Box(FL_BORDER_BOX,5,192,164,230,"");

	liptensionlight=new Fl_Toggle_Button(10,215,50,20,"light");
	liptensionlight->callback((Fl_Callback *)selectedliptensionlight,this);
	liptensionnormal=new Fl_Toggle_Button(60,215,50,20,"normal");
	liptensionnormal->callback((Fl_Callback *)selectedliptensionnormal,this);
	liptensionstrong=new Fl_Toggle_Button(110,215,50,20,"strong");
	liptensionstrong->callback((Fl_Callback *)selectedliptensionstrong,this);
	if(inimanager.GetValueString("ENGINE_LIP_WEIGHT")=="LIGHT")
		liptensionlight->value(1);
	if(inimanager.GetValueString("ENGINE_LIP_WEIGHT")=="NORMAL")
		liptensionnormal->value(1);
	if(inimanager.GetValueString("ENGINE_LIP_WEIGHT")=="STRONG")
		liptensionstrong->value(1);

	articulationhypo=new Fl_Toggle_Button(10,265,50,20,"hypo");
	articulationhypo->callback((Fl_Callback *)selectedarticulationhypo,this);
	articulationmedium=new Fl_Toggle_Button(60,265,50,20,"medium");
	articulationmedium->callback((Fl_Callback *)selectedarticulationmedium,this);
	articulationhyper=new Fl_Toggle_Button(110,265,50,20,"hyper");
	articulationhyper->callback((Fl_Callback *)selectedarticulationhyper,this);
	if(inimanager.GetValueString("ENGINE_LIP_FLOW")=="BOUND")
		articulationhypo->value(1);
	if(inimanager.GetValueString("ENGINE_LIP_FLOW")=="MEDIUM")
		articulationmedium->value(1);
	if(inimanager.GetValueString("ENGINE_LIP_FLOW")=="FREE")
		articulationhyper->value(1);

	//insert new buttons for emtion model
	lipaddemotion=new Fl_Toggle_Button(10,320,75,20,"Add Emo");
	lipaddemotion->callback((Fl_Callback *)selectedlipaddemotion,this);
	lipdataemotion=new Fl_Toggle_Button(85,320,75,20,"Data Emo");
	lipdataemotion->callback((Fl_Callback *)selectedlipdataemotion,this);
	if(inimanager.GetValueString("LIP_EMOTION_MODEL")=="ADD")
		lipaddemotion->value(1);
	if(inimanager.GetValueString("LIP_EMOTION_MODEL")=="DATA")
		lipdataemotion->value(1);
	lipdataemotion->deactivate();


	lipparLSO=new Fl_Toggle_Button(10,370,50,20,"LSO");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("LSO")!=std::string::npos)
		lipparLSO->value(1);
	lipparJAW=new Fl_Toggle_Button(60,370,50,20,"JAW");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("JAW")!=std::string::npos)
		lipparJAW->value(1);
	lipparLSW=new Fl_Toggle_Button(110,370,50,20,"LSW");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("LSW")!=std::string::npos)
		lipparLSW->value(1);
	lipparULP=new Fl_Toggle_Button(10,390,50,20,"ULP");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("ULP")!=std::string::npos)
		lipparULP->value(1);
	lipparLLP=new Fl_Toggle_Button(60,390,50,20,"LLP");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("LLP")!=std::string::npos)
		lipparLLP->value(1);
	lipparCRL=new Fl_Toggle_Button(110,390,50,20,"CRL");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("CRL")!=std::string::npos)
		lipparCRL->value(1);
	lipparCRR=new Fl_Toggle_Button(10,390,50,20,"CRR");
	if(inimanager.GetValueString("ENGINE_LIP_PARAMETERS").find("CRR")!=std::string::npos)
		lipparCRR->value(1);

	gazebox=new Fl_Box(FL_BORDER_BOX,180,25,170,220,"");

	gazebn=new Fl_Check_Button(330,28,20,20,"gazeBN");
	gazebn->align(FL_ALIGN_LEFT);
	gazebn->callback((Fl_Callback *)selectedgazebn,this);
	gazebn->value(inimanager.GetValueInt("GAZE_MODEL_BN"));

	MAX_T_S1_L1=new Fl_Input(290,80,50,20,"Max t mutual");
	MAX_T_S1_L1->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_MAX_T_S1_L1"));
	MAX_T_S1_L1->value(value);

	MAX_T_S1=new Fl_Input(290,100,50,20,"Max t sp.lookat");
	MAX_T_S1->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_MAX_T_S1"));
	MAX_T_S1->value(value);

	MAX_T_L1=new Fl_Input(290,120,50,20,"Max t lis.lookat");
	MAX_T_L1->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_MAX_T_L1"));
	MAX_T_L1->value(value);

	MAX_T_S0=new Fl_Input(290,140,50,20,"Max t sp.lookaw.");
	MAX_T_S0->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_MAX_T_S0"));
	MAX_T_S0->value(value);

	MAX_T_L0=new Fl_Input(290,160,50,20,"Max t lis.lookaw.");
	MAX_T_L0->align(FL_ALIGN_LEFT);
	sprintf_s(value,"%.2f",inimanager.GetValueFloat("ENGINE_MAX_T_L0"));
	MAX_T_L0->value(value);

	DT=new Fl_Input(290,180,50,20,"Min t state tr.");
	DT->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_DT"));
	DT->value(value);

	LIMIT=new Fl_Input(290,200,50,20,"Min prob.");
	LIMIT->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_LIMIT"));
	LIMIT->value(value);

	SCALE=new Fl_Input(290,220,50,20,"Scale factor");
	SCALE->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("ENGINE_SCALE"));
	SCALE->value(value);

	gazediagram=new Fl_Check_Button(320,220,12,12,"draw gaze curves");
	gazediagram->align(FL_ALIGN_LEFT);
	gazediagram->value(inimanager.GetValueInt("DRAW_GAZE_CURVES"));
	

	if(gazebn->value()==1)
	{
		gazediagram->hide();
	}

	if(gazebn->value()==0)
	{
		MAX_T_S1_L1->hide();
		//MAX_T_S0->hide();
		//MAX_T_L0->hide();
		DT->hide();
		LIMIT->hide();
		SCALE->hide();

	}

	
	headnoise=new Fl_Input(230,247,45,20,"headnoise");
	headnoise->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%.2f",inimanager.GetValueFloat("HEAD_NOISE"));
	headnoise->value(value);
	headnoise->labelfont(FL_HELVETICA);
	headnoise->labelsize(10);
	headnoise->textfont(FL_HELVETICA);
	headnoise->textsize(10);

	torsonoise=new Fl_Check_Button(335,250,12,12,"torsonoise");
	torsonoise->align(FL_ALIGN_LEFT);
	torsonoise->value(inimanager.GetValueInt("ENGINE_USE_PERLIN_TORSO_NOISE"));
	torsonoise->labelfont(FL_HELVETICA);
	torsonoise->labelsize(10);


	armsbox=new Fl_Box(FL_BORDER_BOX,180,270,170,125,"");

	jointslimits=new Fl_Check_Button(325,280,10,10,"joits limits");
	jointslimits->align(FL_ALIGN_LEFT);
	jointslimits->value(inimanager.GetValueInt("ENGINE_USE_JOINT_LIMITS"));

	arminterpeuler=new Fl_Toggle_Button(190,320,50,20,"euler");
	arminterpeuler->callback((Fl_Callback *)selectedarminterpeuler,this);
	if(inimanager.GetValueInt("ENGINE_ARM_INTERP_TYPE")==0)
		arminterpeuler->value(1);
	arminterpquat=new Fl_Toggle_Button(240,320,50,20,"quat");
	arminterpquat->callback((Fl_Callback *)selectedarminterpquat,this);
	if(inimanager.GetValueInt("ENGINE_ARM_INTERP_TYPE")==1)
		arminterpquat->value(1);
	arminterppath=new Fl_Toggle_Button(290,320,50,20,"ik path");
	arminterppath->callback((Fl_Callback *)selectedarminterppath,this);
	if(inimanager.GetValueInt("ENGINE_ARM_INTERP_TYPE")==2)
		arminterppath->value(1);

	wristinterpeuler=new Fl_Toggle_Button(190,367,50,20,"euler");
	wristinterpeuler->callback((Fl_Callback *)selectedwristinterpeuler,this);
	if(inimanager.GetValueInt("ENGINE_WRIST_ORIENTATION_INTERP_TYPE")==0)
		wristinterpeuler->value(1);
	wristinterpquat=new Fl_Toggle_Button(240,367,50,20,"quat");
	wristinterpquat->callback((Fl_Callback *)selectedwristinterpquat,this);
	if(inimanager.GetValueInt("ENGINE_WRIST_ORIENTATION_INTERP_TYPE")==1)
		wristinterpquat->value(1);

	/*
	faceanimation_normal=new Fl_Toggle_Button(180,400,80,20,"good faceanim");
	faceanimation_normal->callback((Fl_Callback *)selectedfaceanimation_normal,this);
	faceanimation_normal->value(0);
	faceanimation_normal->labelfont(FL_HELVETICA);
	faceanimation_normal->labelsize(10);

	faceanimation_TestMau=new Fl_Toggle_Button(260,400,90,20,"TestMau faceanim");
	faceanimation_TestMau->callback((Fl_Callback *)selectedfaceanimation_TestMau,this);
	faceanimation_TestMau->labelfont(FL_HELVETICA);
	faceanimation_TestMau->labelsize(10);
	faceanimation_TestMau->value(1);
	*/

	APMLfilename=new Fl_Input(75,425,450,15,"APML file");
	APMLfilename->align(FL_ALIGN_LEFT);
	APMLfilename->labelfont(FL_HELVETICA);
	APMLfilename->labelsize(9);
	APMLfilename->textfont(FL_HELVETICA);
	APMLfilename->textsize(9);

	browseAPML=new Fl_Button(530,425,55,15,"browse");
	browseAPML->callback((Fl_Callback *)selectedbrowseAPML,this);
	browseAPML->labelfont(FL_HELVETICA);
	browseAPML->labelsize(9);
	
	FMLAPMLfilename=new Fl_Input(75,445,205,15,"FML-APML file");
	FMLAPMLfilename->align(FL_ALIGN_LEFT);
	FMLAPMLfilename->labelfont(FL_HELVETICA);
	FMLAPMLfilename->labelsize(9);
	FMLAPMLfilename->textfont(FL_HELVETICA);
	FMLAPMLfilename->textsize(9);

	browseFMLAPML=new Fl_Button(290,445,55,15,"browse");
	browseFMLAPML->callback((Fl_Callback *)selectedbrowseFMLAPML,this);
	browseFMLAPML->labelfont(FL_HELVETICA);
	browseFMLAPML->labelsize(9);
	
	baselinefilename=new Fl_Input(385,445,140,15,"baseline");
	baselinefilename->align(FL_ALIGN_LEFT);
	baselinefilename->labelfont(FL_HELVETICA);
	baselinefilename->labelsize(9);
	baselinefilename->textfont(FL_HELVETICA);
	baselinefilename->textsize(9);

	browsebaseline=new Fl_Button(530,445,55,15,"browse");
	browsebaseline->callback((Fl_Callback *)selectedbrowsebaseline,this);
	browsebaseline->labelfont(FL_HELVETICA);
	browsebaseline->labelsize(9);
	
	BMLfilename=new Fl_Input(75,465,450,15,"BML file");
	BMLfilename->align(FL_ALIGN_LEFT);
	BMLfilename->labelfont(FL_HELVETICA);
	BMLfilename->labelsize(9);
	BMLfilename->textfont(FL_HELVETICA);
	BMLfilename->textsize(9);

	browseBML=new Fl_Button(530,465,55,15,"browse");
	browseBML->callback((Fl_Callback *)selectedbrowseBML,this);
	browseBML->labelfont(FL_HELVETICA);
	browseBML->labelsize(9);
	
	expressivitybox=new Fl_Box(FL_BORDER_BOX,375,25,210,245,"");

	OAC=new Fl_Slider(380,30,150,15,"OAC");
	OAC->type(FL_HOR_NICE_SLIDER);
	//OAC->box(FL_NO_BOX);
	OAC->bounds(0,1);
	OAC->step(0.1);
	OAC->value(0.5);
	OAC->callback((Fl_Callback *)selectedOAC,this);
	OAC->value(inimanager.GetValueFloat("ENGINE_EXPR_OAC"));
	OACoutput=new Fl_Output(540,30,40,20);
	sprintf_s(value,255,"%.2f",OAC->value());
	OACoutput->value(value);
	
	SPC=new Fl_Slider(380,70,150,15,"SPC");
	SPC->type(FL_HOR_NICE_SLIDER);
	//SPC->box(FL_NO_BOX);
	SPC->bounds(-1,1);
	SPC->step(0.1);
	SPC->value(0);
	SPC->callback((Fl_Callback *)selectedSPC,this);
	SPC->value(inimanager.GetValueFloat("ENGINE_EXPR_SPC"));
	SPCoutput=new Fl_Output(540,70,40,20);
	sprintf_s(value,255,"%.2f",SPC->value());
	SPCoutput->value(value);

	TMP=new Fl_Slider(380,110,150,15,"TMP");
	TMP->type(FL_HOR_NICE_SLIDER);
	//TMP->box(FL_NO_BOX);
	TMP->bounds(-1,1);
	TMP->step(0.1);
	TMP->value(0);
	TMP->callback((Fl_Callback *)selectedTMP,this);
	TMP->value(inimanager.GetValueFloat("ENGINE_EXPR_TMP"));
	TMPoutput=new Fl_Output(540,110,40,20);
	sprintf_s(value,255,"%.2f",TMP->value());
	TMPoutput->value(value);

	FLD=new Fl_Slider(380,150,150,15,"FLD");
	FLD->type(FL_HOR_NICE_SLIDER);
	//FLD->box(FL_NO_BOX);
	FLD->bounds(-1,1);
	FLD->step(0.1);
	FLD->value(0);
	FLD->callback((Fl_Callback *)selectedFLD,this);
	FLD->value(inimanager.GetValueFloat("ENGINE_EXPR_FLD"));
	FLDoutput=new Fl_Output(540,150,40,20);
	sprintf_s(value,255,"%.2f",FLD->value());
	FLDoutput->value(value);

	PWR=new Fl_Slider(380,190,150,15,"PWR");
	PWR->type(FL_HOR_NICE_SLIDER);
	//PWR->box(FL_NO_BOX);
	PWR->bounds(-1,1);
	PWR->step(0.1);
	PWR->value(0);
	PWR->callback((Fl_Callback *)selectedPWR,this);
	PWR->value(inimanager.GetValueFloat("ENGINE_EXPR_PWR"));
	PWRoutput=new Fl_Output(540,190,40,20);
	sprintf_s(value,255,"%.2f",PWR->value());
	PWRoutput->value(value);

	REP=new Fl_Slider(380,230,150,15,"REP");
	REP->type(FL_HOR_NICE_SLIDER);
	//REP->box(FL_NO_BOX);
	REP->bounds(-1,1);
	REP->step(0.1);
	REP->value(0);
	REP->callback((Fl_Callback *)selectedREP,this);
	REP->value(inimanager.GetValueFloat("ENGINE_EXPR_REP"));
	REPoutput=new Fl_Output(540,230,40,20);
	sprintf_s(value,255,"%.2f",REP->value());
	REPoutput->value(value);


	playerbox=new Fl_Box(FL_BORDER_BOX,375,280,210,140,"");

	playerwidth=new Fl_Input(450,290,35,15,"player width");
	//playerwidth->label("player width");
	playerwidth->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%i",inimanager.GetValueInt("PLAYER_WIN_WIDTH"));
	playerwidth->value(value);
	playerwidth->labelfont(FL_HELVETICA);
	playerwidth->labelsize(10);
	playerwidth->textfont(FL_HELVETICA);
	playerwidth->textsize(10);


	playerheight=new Fl_Input(540,290,35,15,"height");
	//playerheight->label("height");
	playerheight->align(FL_ALIGN_LEFT);
	sprintf_s(value,255,"%i",inimanager.GetValueInt("PLAYER_WIN_HEIGHT"));
	playerheight->value(value);
	playerheight->labelfont(FL_HELVETICA);
	playerheight->labelsize(10);
	playerheight->textfont(FL_HELVETICA);
	playerheight->textsize(10);

	/*
	preloadagent=new Fl_Check_Button(464,320,20,20,"preload agent");
	preloadagent->align(FL_ALIGN_LEFT);
	preloadagent->value(inimanager.GetValueInt("PLAYER_PRELOADAGENT"));
	*/
	showlistener=new Fl_Check_Button(473,305,20,20,"show listener");
	showlistener->align(FL_ALIGN_LEFT);
	showlistener->value(inimanager.GetValueInt("PLAYER_SHOWLISTENER"));
	showlistener->labelfont(FL_HELVETICA);
	showlistener->labelsize(10);

	onlyface=new Fl_Check_Button(563,305,20,20,"only face");
	onlyface->align(FL_ALIGN_LEFT);
	onlyface->value(inimanager.GetValueInt("PLAYER_ONLYFACE"));
	onlyface->labelfont(FL_HELVETICA);
	onlyface->labelsize(10);

	showlistenersubwindow=new Fl_Check_Button(563,320,20,20,"listener subwindow");
	showlistenersubwindow->align(FL_ALIGN_LEFT);
	showlistenersubwindow->value(inimanager.GetValueInt("PLAYER_LISTENERSUBWINDOW"));
	showlistenersubwindow->labelfont(FL_HELVETICA);
	showlistenersubwindow->labelsize(10);

	//capturevideo=new Fl_Check_Button(450,320,20,20,"capture video");
	//capturevideo->align(FL_ALIGN_LEFT);

	numcharacters_speaker=6;
	characters_speaker[0]="greta";
	characters_speaker[1]="greta_hairhigh";
	characters_speaker[2]="poppy";
	characters_speaker[3]="spike";
	characters_speaker[4]="obadiah";
	characters_speaker[5]="prudence";
	character_speaker=new Fl_Choice(455,342,120,18,"speaker");
	character_speaker->align(FL_ALIGN_LEFT);
	character_speaker->add(characters_speaker[0].c_str());
	character_speaker->add(characters_speaker[1].c_str());
	character_speaker->add(characters_speaker[2].c_str());
	character_speaker->add(characters_speaker[3].c_str());
	character_speaker->add(characters_speaker[4].c_str());
	character_speaker->add(characters_speaker[5].c_str());
	character_speaker->labelfont(FL_HELVETICA);
	character_speaker->labelsize(10);
	character_speaker->textfont(FL_HELVETICA);
	character_speaker->textsize(10);
	for(i=0;i<numcharacters_speaker;i++)
		if(characters_speaker[i]==inimanager.GetValueString("CHARACTER_SPEAKER"))
			character_speaker->value(i);

	numcharacters_listener=4;
	characters_listener[0]="greta";
	characters_listener[1]="greta_hairhigh";
	characters_listener[2]="girl";
	characters_listener[3]="sinkman";
	character_listener=new Fl_Choice(455,362,120,18,"listener");
	character_listener->align(FL_ALIGN_LEFT);
	character_listener->add(characters_speaker[0].c_str());
	character_listener->add(characters_speaker[1].c_str());
	character_listener->add(characters_speaker[2].c_str());
	character_listener->add(characters_speaker[3].c_str());
	character_listener->labelfont(FL_HELVETICA);
	character_listener->labelsize(10);
	character_listener->textfont(FL_HELVETICA);
	character_listener->textsize(10);
	for(i=0;i<numcharacters_listener;i++)
		if(characters_listener[i]==inimanager.GetValueString("CHARACTER_LISTENER"))
			character_listener->value(i);

	startplayer=new Fl_Button(380,385,200,30,"start player");
	startplayer->callback((Fl_Callback *)selectedstartplayer,this);
	startplayer->labelfont(FL_HELVETICA);
	startplayer->labelsize(13);

//	facemodel->value(inimanager.GetValueString("CHARACTER_SPEAKER").c_str());

	

	//MARYConnect=new Fl_Button(375,440,120,20,"MARY Connect");
	//MARYConnect->callback((Fl_Callback *)selectedmaryconnect,this);

	startengine=new Fl_Button(10,485,340,30,"start engine");
	startengine->callback((Fl_Callback *)selectedstartengine,this);
	
	
	quit=new Fl_Button(375,485,210,30,"save and exit");
	quit->callback((Fl_Callback *)selectedquit,this);
	
	player=0;

	//this->callback((Fl_Callback *)gretaparameterswindowclosed,this);
	this->callback((Fl_Callback *)selectedquit,this);
}

GretaParametersWindow::~GretaParametersWindow()
{
	if(player!=0)
		delete player;
}

int GretaParametersWindow::handle(int e)
{
	return Fl_Window::handle(e);
}

void GretaParametersWindow::draw()
{
	Fl_Window::draw();
	//fl_draw("language",12,78);
	fl_draw("lip tension",12,208);
	fl_draw("lip articulation",12,258);
	fl_draw("lip emotion model",12,313);
	fl_draw("draw lip graph of",12,363);
	fl_draw("arm interpolation",190,313);
	fl_draw("wrist interpolation",190,360);
}

void GretaParametersWindow::PrintLog()
{
	std::list<GretaLogger*>::iterator iterlog;
	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			fprintf(all_logfile,"DEBUG   %s\n", msg.c_str());
		if((*iterlog)->getLevel()=="info")
			fprintf(all_logfile,"INFO   %s\n", msg.c_str());
		if((*iterlog)->getLevel()=="warn")
			fprintf(all_logfile,"WARNING   %s\n", msg.c_str());
		if((*iterlog)->getLevel()=="error")
			fprintf(all_logfile,"ERROR   %s\n", msg.c_str());
	}
	listLog.clear();
}