#pragma once 

#include "CapturedFrame.h"
#include "FAPFrame.h"

#include <vector>

class Converter
{

public:

	std::vector<CapturedFrame*> capturedframes;
	
	int framerate;

	int **fslimits;

	virtual bool readMCMessage(char *mcfilename);

    virtual std::vector<FAPFrame> *converttoFAPs();
			
	Converter();	

	virtual ~Converter();

	bool readmask(std::string filename);

	float mask[69];

}
;