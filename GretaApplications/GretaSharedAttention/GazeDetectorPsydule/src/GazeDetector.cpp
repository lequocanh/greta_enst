// Shared-Attention system
// Christopher Peters

//Wrapper for Stelios' (NTUA) gaze detection library into C++ class

#include "gazedetector.h"

SteliosGazeDetector::SteliosGazeDetector(void)
{
	this->storage = NULL;
	this->frame_orig = NULL;
	this->frame = NULL;
	this->frame_out = NULL;
	this->capture = NULL; 
	this->initialised = false;
	this->inputFromAVI = false;
	this->AVIwriter1 = NULL;

	this->workingPath = NULL;
	
	this->scale = 0.8;
	this->NO_POINTS = 20;
	this->fps=25;

	this->showOutput = false;
	this->saveToFile = false;
    this->stream_out = NULL;
	this->file_type=".txt";

	this->init_points=NULL;
	this->points=NULL;
	this->fin_points=NULL;
    this->inputGrey=NULL;
    this->inputGreyprev=NULL;
	this->count=-1;

	this->xl_or = this->yl_or = this->xr_or = this->yr_or=10;
	this->pxfirstL = this->pxfirstR = this->pyfirstL = this->pyfirstR=0;
	this->flag = -1;

	this->frameCount = 0;
	this->fr_c = 0;
}

SteliosGazeDetector::~SteliosGazeDetector(void)
{
	this->finishCapture();
}

void SteliosGazeDetector::finishCapture(void)
{
	if (this->writeToAVI)
	{
		cvReleaseVideoWriter(&this->AVIwriter1);
	}

	if (saveToFile)
	{
		fclose(this->stream_out);
	}

	cvReleaseImage(&this->frame_out);
	cvcamStop();
	cvcamExit();
	cvReleaseCapture(&this->capture);
	cvDestroyWindow( "output" );
	cvReleaseImage(&this->frame);
	
	//CHRIS: these lines crash the program
	//free_ucmatrix(this->inputGrey,0,this->frame->height,0,this->frame->width);
	//free_ucmatrix(this->inputGreyprev,0,this->frame->height,0,this->frame->width);
}

void SteliosGazeDetector::initialiseNewCapture(bool _inputFromAVI, bool _showOutput, bool _writeToAVI, bool _saveToFile)
{
	printf("\nInitialised SteliosGazeDetector - ");
	//this->workingPath = getcwd(NULL, 0); // or _getcwd
	if ( this->workingPath != NULL) printf("Working path is %s\n", this->workingPath);

	this->inputFromAVI = _inputFromAVI;
	this->showOutput = _showOutput;
	this->writeToAVI = _writeToAVI;
	this->saveToFile = _saveToFile;

	if (this->writeToAVI)
	{
		this->AVIwriter1 = cvCreateAVIWriter("gazeOuput.avi", CV_FOURCC('M','S','V','C'),  3, cvSize((frame_orig->width)*scale, (frame_orig->height)*scale));
	}

	if (this->saveToFile)
	{
		strcat(video_name,file_type);
		stream_out = fopen(video_name, "w" );
	}
	
	if (this->inputFromAVI)
	{
		printf("Name of the input avi (e.g. *.avi):\t");
		scanf("%s",this->video_name);
		this->capture = cvCaptureFromAVI(this->video_name); 
	}
	else
	{
		this->capture = cvCaptureFromCAM( CV_CAP_ANY );
		//CvCapture* capture= cvCreateCameraCapture( -1 );
	}

	if(!capture) {
		fprintf(stderr, "ERROR: capture is NULL - ensure a compatible webcam is connected\n" );
		getchar();
		return;
	}

	//create the video output window
	if (this->showOutput) 
	{	
		printf("\nOutput window created");
		cvNamedWindow( "output", CV_WINDOW_AUTOSIZE );
	}


    this->frame_orig = cvQueryFrame(this->capture);
	this->frame= cvCreateImage(cvSize((int)((frame_orig->width)*scale),(int)((frame_orig->height)*scale)),IPL_DEPTH_8U, frame_orig->nChannels);
    this->frame_out = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 3);

	printf("\nProcessing input %d x %d with scale %lf", frame_orig->width, frame_orig->height, this->scale);

	this->px_m=fvector(0,10000);
	this->py_m=fvector(0,10000);
	this->ppx_m=fvector(0,10000);
	this->ppy_m=fvector(0,10000);

	this->frameResults.px = 0.0;
	this->frameResults.py = 0.0;
	this->frameResults.ppy = 0.0;
	this->frameResults.ppx = 0.0;
	this->frameResults.dist_monitor = 0.0;
	this->frameResults.valid = false;

	//this->faces = 0;
	this->interval=0;
	this->frameCount = 0;
	this->fr_c = 0;

	this->init_points=dmatrix(0,2,0,2);
	this->points=dmatrix(0,2,0,NO_POINTS);
	this->fin_points=dmatrix(0,2,0,NO_POINTS);
    this->inputGrey=ucmatrix(0,frame->height,0,frame->width);
    this->inputGreyprev=ucmatrix(0,frame->height,0,frame->width);
	this->count=-1;

	this->xl_or = this->yl_or = this->xr_or = this->yr_or = 10;
	this->pxfirstL = this->pxfirstR = this->pyfirstL = this->pyfirstR = 0;
	this->flag = -1;

	this->initialised = true;
}

const FrameResults& SteliosGazeDetector::processNewFrame(void)
{
	// Create a window in which the captured images will be presented
	//cvNamedWindow( "mywindow", CV_WINDOW_AUTOSIZE );
	//Show the image captured from the camera in the window and repeat
	//-------------------------------------------------
	this->frameResults.valid = false;

	// Get one frame
	this->frame_orig = cvQueryFrame(this->capture);
	if(!this->frame_orig) {
	  fprintf( stderr, "ERROR: frame is null...\n" );
	  getchar();
	  return this->frameResults;
	}
	
	//------------------------------------------------
	this->frameCount++;
	if(!cvGrabFrame(this->capture)) return this->frameResults;
	this->frame_orig = cvRetrieveFrame(this->capture);
	if(!this->frame_orig) return this->frameResults;

    if( this->frame_orig->origin == IPL_ORIGIN_TL ){
        cvCopy(this->frame_orig, this->frame_orig, 0);
	}
    else{
        cvFlip(this->frame_orig, this->frame_orig, 0);
	}

	cvResize(this->frame_orig,this->frame,CV_INTER_LINEAR); //bilinear interpolation used by default
	cvFlip(this->frame_orig,this->frame_orig,0);
    cvCopy(this->frame,this->frame_out);

	//stelios method !!!!!

	detect_track(cvPoint(0,0),this->frame_orig->width,this->frame_orig->height, this->flag, this->frameCount, this->frame, this->inputGrey, this->inputGreyprev,
		this->points, this->fin_points, this->ppx_m, this->ppy_m, fr_c, this->frameResults.ppx, this->frameResults.ppy, this->frameResults.px, this->frameResults.py,
		this->px_m, this->py_m, this->pxfirstL, this->pxfirstR, this->pyfirstL, this->pyfirstR, this->init_points, this->frameResults.dist_monitor,
		this->xr_or, this->yr_or, this->xl_or, this->yl_or);

	if((this->frameResults.dist_monitor > 0.99) && (this->frameResults.dist_monitor < 1.01))
	{
		// this->frameResults.ppx=0;
		this->frameResults.ppy=0;
		//xr_or=xr_or;
		this->yr_or=this->points[1][4];
		//xl_or=xl_or;
		this->yl_or=this->points[1][5];
	}

	if (this->showOutput)
	{
		cvLine(this->frame_out, cvPoint((this->yr_or+this->yl_or)/2,(this->xr_or+this->xl_or)/2), 
			cvPoint((this->yr_or+this->yl_or)/2-(int)100*this->frameResults.ppy,(this->xr_or+this->xl_or)/2-(int)100*this->frameResults.ppx), 
			CV_RGB(255,255,255), 2, 8, 0 );

		//cvLine(frame, point1, point2, line colour, line thickness, CV_AA, 0)

		cvLine(this->frame_out, cvPoint((this->yr_or+this->yl_or)/2,(this->xr_or+this->xl_or)/2), 
			cvPoint((this->yr_or+this->yl_or)/2-(int)3*this->frameResults.px,(this->xr_or+this->xl_or)/2-(int)3*this->frameResults.py), 
			CV_RGB(0,0,0), 2, 8, 0 );

		//cvLine(this->frame_out, cvPoint(this->yr_or, this->xr_or), cvPoint(this->yl_or, this->xl_or), 
		//	CV_RGB(255,0,0), 2, 8, 0 );

		//cvRectangle(this->frame_out, cvPoint(yr_or,yl_or), cvPoint(xr_or,xl_or), cvScalar(255,0,0), 1);

		cvShowImage( "output", this->frame_out );
	}
	if (this->writeToAVI)
	{
		cvWriteToAVI(this->AVIwriter1, this->frame_out);
	}

	if (this->saveToFile)
	{
		//output results for this frame
		fprintf( stream_out, "frame %d (fr_c %d)\t", this->frameCount, this->fr_c);
		//fprintf( stream_out, "flag=%d\t",flag);
		fprintf( stream_out, "%f\t", this->frameResults.px );
		fprintf( stream_out, "%f\t", this->frameResults.py );
		fprintf( stream_out, "%f\t", this->frameResults.ppx );
		fprintf( stream_out, "%f\t", this->frameResults.ppy );
		fprintf( stream_out, "%f\n", this->frameResults.dist_monitor );
	}

	if (fr_c==0){  
		this->pxfirstR=this->frameResults.px;
		this->pxfirstL=this->frameResults.px;
		this->pyfirstR=this->frameResults.py;
		this->pyfirstL=this->frameResults.py;
	}

	int key=cvWaitKey(10);	//need this in order for window to display contents correctly

	this->fr_c++;

	this->frameResults.valid = true;
	return this->frameResults;
}