package demo;
/**
 * Class to represent a demo
 * @author Magalie
 *
 */
public class DemoExample {

	
	private String _textUserTrigger;
	private String _textProgrammDTrigger;
	private int _id; 
	private String _bmlFmlText;
	
	private boolean _bmlType;
	
	private String _textTellByGreta;
	
	public DemoExample(String t, String t2, boolean bml){
		this._textUserTrigger=t;
		this._textProgrammDTrigger=t2;
		this._bmlType=bml;
		
	}
	
	/**
	 * Method that set the text that Greta should say
	 * @param s the text that Greta should say
	 */
	public void settextTellByGreta(String s){
		this._textTellByGreta=s;
	}
	
	/**
	 * Method that return the text said by Greta 
	 * @return text that Greta should say 
	 */
	public String gettextTellByGreta(){
		return this._textTellByGreta;
	}
	
	/**
	 * 
	 * @param t text with BML or FML associated to the demo
	 */
	public void setBmlFmlText(String t){
		this._bmlFmlText=t;
	}
	
	/**
	 * return the keyword sent by the dialog engine to trigger the demo
	 */
	public String getTextProgrammDTrigger(){
		return this._textProgrammDTrigger;
	}
	
	/**
	 * 
	 * @return the text with FML or BML tag corresponding to the demo
	 */
	public String getBmlFml(){
		return this._bmlFmlText;
	}
	
	/**
	 * 
	 * @return true if the demo is in BML, else false
	 */
	public boolean isBML(){
		return this._bmlType;
	}
}
