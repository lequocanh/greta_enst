#pragma once

#include <stdio.h>
#include "altypes.h"
#include "alptr.h"
#include <almotionProxy.h>
#include <conio.h>
#include <alerror.h>
#include <albroker.h>

#include "NaoGesture.h"

using namespace std;



#define PI 3.14159265358979f

#define AXIS_MASK_X 1
#define AXIS_MASK_Y 2
#define AXIS_MASK_Z 4
#define AXIS_MASK_WX 8
#define AXIS_MASK_WY 16
#define AXIS_MASK_WZ 32

#define INTERPOLATION_TYPE 1
#define	SECONDS 1
#define	MASK  63
#define	SPACE 0 // Task space {SPACE_TORSO = 0, SPACE_WORLD = 1, SPACE_SUPPORT_LEG = 2 }
#define isAbsolute 0

class NaoController
{
  struct wavheader
  {
        char ChunkID[4]; // 32 bits
        long ChunkSize;  // 32 bits
        char Format[4];  // 32 bits
        char Subchunk1ID[4]; // 32 bits
        long Subchunk1Size; // 32 bits
        unsigned short AudioFormat; // 16 bits
        unsigned short NumChannels; // 16 bits
        unsigned long SampleRate; // 32 bits
        unsigned long ByteRate; // 32 bits
        unsigned short BlockAlign; // 16 bits
        unsigned short BitsPerSample; // 16 bits
        char Subchunk2ID[4]; // 32 bits
        long Subchunk2Size;  // 32 bits

  };
public:
	NaoController();
	~NaoController(void);

private:
	int run_on_virtualNAO;

	std::string parentBrokerIP;
	int parentBrokerPort;

	AL::ALProxy *motionProxy;
	AL::ALProxy *audioPlayerProxy;
	AL::ALProxy *audioDeviceProxy;

	vector<float> ZeroPosition;
	vector<float> InitPosition;

	float joints[5][7][3][4];

	float test_leftPathv[14][6];
	float test_rightPathv[14][6];
	AL::ALValue test_leftPath;
	AL::ALValue test_rightPath;
	AL::ALValue test_leftTimes;
	AL::ALValue test_rightTimes;

public:
	NaoGesture gestures;

public:
	void Init();	
	void LoadArmPositions();
	void Test();
	void StiffnessOn();
	void StiffnessOff();
	void PoseInit();
	void RelaxPose();
	void OpenHand();

	void SendWavFile();//(char* wavFile, int size);
	void Animation();
	void Motion();
	
public:
	void move_left_arm_to_one_position(vector<float> lVec6, float *lHand, float time);
	void move_right_arm_to_one_position(vector<float> rVec6, float *rHand, float time);
 
	void make_motion_left_arm_following_path(AL::ALValue path, AL::ALValue times, float *lHand);
	void make_motion_right_arm_following_path(AL::ALValue path, AL::ALValue times, float *lHand);
};
