//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// MultimodalEmotionConstraint.h: interface for the MultimodalEmotionConstraint class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

//#include "Constraint.h"
#include "ConstraintSignal.h"
#include "ConsNode.h"

#include <string>
#include <vector>
#include <map>



/**
* class :MultimodalEmotionConstraint
*
*/

class MultimodalEmotionConstraint 
{

private:

	/**
	* container of old-type constraints
	*/

	//std::vector<Constraint> *constraints;
	
	/**
	* cointaire of new-type constraits
	*/
	std::vector<ConsNode*> *cons_trees;

	/**
	* = behavior set
	*/
	std::vector<ConstraintSignal> *signals;
	
	/**
	* 
	*/
	std::string name;

public:


	/**
	* contructor 
	*
	*/

	MultimodalEmotionConstraint ();

	/**
	* destructor 
	*/

	virtual ~MultimodalEmotionConstraint ();


	/**
	*  
	* 
	*
	* @return 
	*/

	std::string getName();

	/**
	*  
	* 
	*
	* @param  name1
	*/

	void setName(std::string name1);

	/**
	*  
	* 
	*
	* @param  constraint1
	*/

	//void addConstraint(Constraint constraint1);	

	/**
	*  
	* gets vector of constraints of the old type
	*
	* @return 
	*/

	//std::vector<Constraint>* getConstraints();

	/**
	*  
	* 
	*
	* @return 
	* @param  name
	*/

	ConstraintSignal* getConstraintSignal(std::string name) ;

	/**
	*  
	* 
	*
	* @param  constraintsignal
	*/

	void addConstraintSignal(ConstraintSignal constraintsignal) ;

	/**
	*  
	* 
	*
	* @param constraint
	*/

	//void addConstraint(Constraint *constraint) ;

	/**
	*  
	* @return 
	* @param  name
	*/

	int getIdOfSignal(std::string name);

	/**
	*  
	* get behavior sets 
	*
	* @return 
	*/

	std::vector<ConstraintSignal>* getConstraintSignals() ;

	/**
	*  
	* get contraints of one emotion
	*
	* @return 
	* @param  name emotional lavel
	*/

	//std::vector<Constraint>*  getConstraintsOfOneSignal(std::string name) ;

	/**
	*  
	* get all constraints of one signal name (new version)
	*
	* @return vector of node_trees containg all cons for a given signal name
	* @param  name of signal
	*/

	std::vector<ConsNode*>*  getNewConstraintsOfOneSignal(std::string name) ;

	/**
	*  
	* adds new type of constraints
	*
	* @param consnode cons_tree
	*/

	void addNewConstraint(ConsNode* consnode) ;

	/**
	*  
	* gets vector of constraints of the new type
	*
	* @return 
	*/

	std::vector<ConsNode*>* getNewConstraints();

	void fill_right(int id,float start_time, float stop_time);

	void fill(int id, float start_time, float stop_time);

	void clean();

private:

	void find_adequate(ConsNode*  consnode, ConsNode*  temp);


};

/** 
* List of MultimdalEmotionConstraint objects.
* 
*/

typedef std::map<std::string,MultimodalEmotionConstraint> MultimodalEmotionConstraints;
