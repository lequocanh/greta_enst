#include "PlayerOgreAgent.h"
#include "IniManager.h"
#include "XMLDOMParser.h"

extern	"C" int load_wave(char *,char*);
extern	"C" int play_wave(int,char*);
extern	"C" void stop_wave(char*);
extern	"C" void close_wave(char*);
extern	"C" int get_wave_time(char*);
extern  "C" int get_audio_len(char*);

extern IniManager inimanager;

PlayerOgreAgent::PlayerOgreAgent(Ogre::SceneManager *sceneManager,Ogre::SceneNode *rootNode,std::string name_,std::string character,bool realtime_):skeleton(0),body(0){
	name = name_;

	std::string characterfilename = inimanager.Program_Path+"Ogre/"+character+".xml";
	XMLDOMParser *parser = new XMLDOMParser();
	XMLGenericTree *partstree = parser->ParseFile(characterfilename);
	if(partstree==0){
		printf("Can't parse %s.\n",characterfilename.c_str());
		//so we load Greta
		partstree = parser->ParseFile(inimanager.Program_Path+"Ogre/greta.xml");
	}
	partstree = partstree->FindNodeCalled("CHARACTER");

	agentNode = rootNode->createChildSceneNode(name+"_agentNode");
	
	XMLGenericTree *node;

	//load the body
	
	node = partstree->FindNodeCalled("BODYFILES");
	bodyNode = agentNode->createChildSceneNode(name+"_bodyNode");
	body = sceneManager->createEntity(name+"_body", node->GetAttribute("mesh"));
	if(node->GetAttribute("texture_file")!="")
		body->setMaterialName(node->GetAttribute("texture_file"));
	showBody = true;
	bodyNode->attachObject(body);
	skeleton = new PlayerOgreBodySkeleton(body->getSkeleton());
	//set the minimum coordinate of the body as the origine point of the agent
	Ogre::Vector3 bodyGap(
		-body->getWorldBoundingBox(true).getCenter().x,
		-body->getWorldBoundingBox(true).getMinimum().y,
		-body->getWorldBoundingBox(true).getCenter().z);
	bodyNode->setPosition(bodyGap);
	

	//load the head
	node = partstree->FindNodeCalled("HEADFILES");
	head = new PlayerOgreHead(sceneManager,agentNode,name,
			node->GetAttribute("vertices"),
			node->GetAttribute("hair"));

	if(node->GetAttribute("texture_file")!="")
		head->setFaceMaterial(node->GetAttribute("texture_file"));
	if(node->GetAttribute("eyes_texture")!="")
		head->setEyesMaterial(node->GetAttribute("eyes_texture"));

	//scale eyes
	node = partstree->FindNodeCalled("EYE_SCALE");
	head->scaleEyes(atof(node->GetAttribute("factor").c_str()));

	//eyes positions
	//	left
	node = partstree->FindNodeCalled("LEFT_EYE");
	head->setLeftEyePosition(	atof(node->GetAttribute("x").c_str()),
								atof(node->GetAttribute("y").c_str()),
								atof(node->GetAttribute("z").c_str()));
	//	right
	node = partstree->FindNodeCalled("RIGHT_EYE");
	head->setRightEyePosition(	atof(node->GetAttribute("x").c_str()),
								atof(node->GetAttribute("y").c_str()),
								atof(node->GetAttribute("z").c_str()));
	//jaw position
	node = partstree->FindNodeCalled("TEETHS-TONGUE");
	head->setJawPosition(	atof(node->GetAttribute("x").c_str()),
							atof(node->GetAttribute("y").c_str()),
							atof(node->GetAttribute("z").c_str()));
	head->scaleJaw(atof(node->GetAttribute("scale").c_str()));


	//hair position and scale
	node = partstree->FindNodeCalled("HAIRPOSITION");
	if(node!=NULL){
		head->scaleHair(atof(node->GetAttribute("scalex").c_str()),
						atof(node->GetAttribute("scaley").c_str()),
						atof(node->GetAttribute("scalez").c_str()));
		head->setHairPosition(	atof(node->GetAttribute("x").c_str()),
								atof(node->GetAttribute("y").c_str()),
								atof(node->GetAttribute("z").c_str()));
		head->pitchHair(atof(node->GetAttribute("pitch").c_str()));
	}

	//face position
	node = partstree->FindNodeCalled("FACEPOSITION");
	if(node!=NULL){
		head->setFacePosition(	atof(node->GetAttribute("x").c_str()),
								atof(node->GetAttribute("y").c_str()),
								atof(node->GetAttribute("z").c_str()));
	}

	//scale head
	node = partstree->FindNodeCalled("HEADPOSITION");
	head->scale(atof(node->GetAttribute("scale").c_str()));
	
	//link head and body
	head->setPosition(	atof(node->GetAttribute("x").c_str()),
						atof(node->GetAttribute("y").c_str()),
						atof(node->GetAttribute("z").c_str()));
	if(body!=0)
		head->linkToBone(body->getSkeleton()->getBone("skullbase"),bodyGap);

	//load accessories
	int countAccessories = 0;
	char charnum [30];

	for(XMLGenericTree::iterator it=partstree->begin(); it != partstree->end();++it)
	{
		XMLGenericTree *iter = *it;
		if (iter->isTextNode()) continue;
		if(iter->GetName()=="FACE_ACCESSORY"){
			head->addAccessory(sceneManager,iter->GetAttribute("mesh"),name+"face_object_"+itoa(countAccessories,charnum,10),
				atof(iter->GetAttribute("x").c_str()),
				atof(iter->GetAttribute("y").c_str()),
				atof(iter->GetAttribute("z").c_str()),
				atof(iter->GetAttribute("scale").c_str()));
			++countAccessories;
		}
	}

	//create poses faor facial animation
	node = partstree->FindNodeCalled("HEADFILES");
	head->createFAPPoses(inimanager.Program_Path+"characters/"+node->GetAttribute("fdps"), inimanager.Program_Path+"characters/"+node->GetAttribute("indices"),sceneManager);

	//Cameras
	agentCamera = new PlayerOgreCamera(true,sceneManager,agentNode,name+"_freeCamera");
		//cropping agentCamera :
	Ogre::Vector3 headCenter = *(head->getFaceCenter());
	Ogre::Vector3 headPosition = headCenter - agentNode->_getDerivedPosition();
	double heeadHight = head->getFaceHight();
	double headDeep = head->getFaceDeep();
	if(inimanager.GetValueInt("PLAYER_ONLYFACE")){
		agentCamera->setPosition(
			headPosition.x,
			headPosition.y,
			headPosition.z+headDeep/2.0+heeadHight/agentCamera->tan_FOV());
		agentCamera->lookAt(
			headCenter.x,
			headCenter.y,
			headCenter.z);
		agentCamera->pitch(0.1);
	}
	else{
		agentCamera->setPosition(
			headPosition.x,
			headPosition.y - heeadHight,
			headPosition.z + headDeep*1.5 + heeadHight*3.0/agentCamera->tan_FOV());
		agentCamera->lookAt(
			headCenter.x,
			headCenter.y - heeadHight,
			headCenter.z);
		agentCamera->pitch(-0.10);
	}

	eyesCamera = new PlayerOgreCamera(false,sceneManager,head->getRigthEyeNode(),name+"_eyeCamera");
	eyesCamera->setPosition(0, 0, 5);
	eyesCamera->getOgreCamera()->yaw(Ogre::Degree(180));

	//
	audioenabled=true;
	FAPs = new FAPData();
	BAPs = new BAPData();
	WRITEAVI=false;
	agent_created=clock();
	realtime = realtime_;
	maxframes=0;
	status="idle";
	AssignFile("empty");
}

void PlayerOgreAgent::StopTalking(){
	if(status!="talking")
		return;
	status="ready";
	stoppingtime=local_time;
	if(!WRITEAVI){
		stop_wave((char*)name.c_str());
		close_wave((char*)name.c_str());
	}
}

void PlayerOgreAgent::StartTalking(){
	if((maxframes>1)&&((status=="ready")||(status=="idle"))){
		RestartAnimation();
	}
}

void PlayerOgreAgent::RestartAnimation(){
		audio = audioenabled && !WRITEAVI && load_wave((char*)wav_filename.c_str(),(char*)name.c_str());
		status="talking";
		if(!WRITEAVI){
			if(audio)
				play_wave(AnimationActualFrame*((1.0f/animationfps)*1000),(char*)name.c_str());
			else
				started_animation=local_time-(AnimationActualFrame/animationfps);
		}
}

void PlayerOgreAgent::AssignFile(char* filename){
	StopTalking();
	
	openedfile=std::string(filename);
	status="readingfile";
	
	fap_filename=openedfile+".fap";
	bap_filename=openedfile+".bap";
	wav_filename=openedfile+".wav";


	if(skeleton!=0){
		if(BAPs->ReadBAPFile((char*)bap_filename.c_str())==0){
			BAPs->ReadBAPFile("empty.bap");
			BAPs->fps=0;
		}
	}
	else
		BAPs->fps=0;
	
	initFAPs();


	if (!check_fps()){
		printf("\nNo animation to show...");
		return;
	}

	AnimationActualFrame=0;
	BAPframeDisplayed = -1;
	FAPframeDisplayed = -1;
	if(strcmp(filename,"empty")!=0)
		status="ready";
	else
		status="idle";

	LoadFAPFrameIndex(1);
	if(skeleton!=0){
		skeleton->Reset();
		LoadBAPFrame(1,0);
	}
}

bool PlayerOgreAgent::check_fps(){
	animationfps = FAPs->fps > BAPs->fps ? FAPs->fps : BAPs->fps;
	if (animationfps == 0)
		return false;

	maxframes = 0;
	if (FAPs->fps > 0)
		maxframes = FAPs->numberofframes * animationfps / FAPs->fps;
	if (BAPs->fps > 0){
		int bapsmaxframes = BAPs->numberofframes * animationfps / BAPs->fps;
		maxframes = maxframes > bapsmaxframes ? maxframes : bapsmaxframes;
	}
	return maxframes > 0;
}

void PlayerOgreAgent::EnableAudio(bool enable){
	audioenabled = enable;
}

void PlayerOgreAgent::LoadFAPFrameIndex(int index){

	//printf("%i ", FAPs->frames[100].GetFAP(5) );
	
	if (FAPs->numberofframes!=0 && index!=FAPframeDisplayed){
		FAPframeDisplayed = index;
	
		if(index<FAPs->numberofframes)
			LoadFAPFrame(&(FAPs->frames[index]));
		else
			LoadFAPFrame(&(FAPs->frames[FAPs->numberofframes-1]));
	}
}

void PlayerOgreAgent::LoadBAPFrame(int index,BAPFrame *inputBAPFrame){
	if(skeleton==0)
		return;
	bool found = false;
	BAPFrame *thisframe;

	if(inputBAPFrame==0){
		if (BAPs->numberofframes!=0 && index!=BAPframeDisplayed){
			found=true;
			BAPframeDisplayed = index;
			if(index<BAPs->numberofframes)
				thisframe=BAPs->frames[index];
			else
				thisframe=BAPs->frames[BAPs->numberofframes-1];
			
		}
	}
	else{
		thisframe=inputBAPFrame;
		found=true;
	}
	
	if(found){
		skeleton->ApplyBAPFrame(thisframe);
		head->updateSkullLink();
	}
}

bool PlayerOgreAgent::initFAPs(){
	if(FAPs->header_file_init((char*)fap_filename.c_str())==0){
		FAPs->numberofframes=1;
		FAPs->fps=0;
		return false;
	}
	if(FAPs->ReadFapFile((char*)fap_filename.c_str())==0)
		return false;
	return true;
}
void PlayerOgreAgent::OneFrameUp(){
	if(status!="ready")
		return;
	if(AnimationActualFrame<(maxframes-1))
		AnimationActualFrame++;
}

void PlayerOgreAgent::OneFrameDown(){
	if(((maxframes>1)&&(status=="idle")) || (status=="ready"))
		if(AnimationActualFrame>0){
			AnimationActualFrame--;
			if(status=="idle")
				status="ready";
	}
}

void PlayerOgreAgent::update(){


	if(realtime)
		return;
	int audioat;
	int fapframe_index = 0;//frame counter for the face
	int BAPFrame_index = 0;//frame counter for the body


	local_time=((float)clock()-agent_created)/CLOCKS_PER_SEC;

	if(!WRITEAVI){
		if(audio && (status=="talking")){
			audioat=get_wave_time((char*)name.c_str());
			if(audioat<get_audio_len((char*)name.c_str()))
				AnimationActualFrame=get_wave_time((char*)name.c_str())/(1000.0f/animationfps);
			else{
				if(maxframes>(get_audio_len((char*)name.c_str())/(1000.0f/animationfps))){
					started_animation=local_time-((float)AnimationActualFrame/animationfps);
					audio=false;
				}
			}
		}
		if((!audio && status=="talking"))
			AnimationActualFrame=(local_time-started_animation)*animationfps;
	}
	else{
		if(AnimationActualFrame>=(maxframes-1)){
			AnimationActualFrame=maxframes-1;
			StopTalking();
		}
		else
			AnimationActualFrame++;
	}

	if(AnimationActualFrame>(maxframes-1) && (status!="readingfile") && (status!="ready")){
		AnimationActualFrame=maxframes-1;
		StopTalking();
	}


	if(animationfps!=0)
		BAPFrame_index = AnimationActualFrame * BAPs->fps / animationfps;
	else
		BAPFrame_index = 0;

	if(animationfps!=0)
		fapframe_index = AnimationActualFrame * FAPs->fps / animationfps;
	else
		fapframe_index = 0;
	LoadFAPFrameIndex(fapframe_index);
	LoadBAPFrame(BAPFrame_index,0);
}

void PlayerOgreAgent::ReloadFile(){
	StopTalking();
	AnimationActualFrame=0;
	AssignFile((char*)openedfile.c_str());
}


void PlayerOgreAgent::SwitchSkeleton(){
	if(body!=0){
		showBody = !showBody;
		body->setVisible(showBody);
	}
}