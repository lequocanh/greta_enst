// Shared-Attention system
// Christopher Peters


#if !defined(AFX_REACTIVELISTENEPSYDULE_H__8D6DB6DB_FC9A_432B_8450_4B941018BD20__INCLUDED_)
#define AFXREACTIVELISTENEPSYDULE_H__8D6DB6DB_FC9A_432B_8450_4B941018BD20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif

#include <iostream>
#include <fstream>
#include <Windows.h>
#include <list>
#include <string>
#include <vector>
#include <map>
#include <time.h>

#include "InputData.h"
#include "RandomGen.h"
#include "XMLGenericTree.h"
#include "glViewer.h"
#include "Psydule.h"

struct PsycloneMessage
{
	std::string type;
	std::string data;
	bool handled;
};

class SharedAttentionPsydule {
 
	
public:

	Psydule *commint;


	SharedAttentionPsydule(Scene *scene,std::string name, std::string host, int port);
	virtual ~SharedAttentionPsydule();

	int readMessage(CommunicationMessage *msg);
	void sendAdminMessage(std::string msg);

	void checkForMessage(int timeoutMS);
 
	bool registerPsyduleConnection(void);

	std::list<std::string> datatypes;

	void deleteAllRecievedMessages(void);
	std::vector<PsycloneMessage *> recievedMessages;

	void enableCalibrateMode(void);

	std::string host;
	std::string name;
	int port;

	long lastMessageCheck;	//last time we checked for a message
	long messageCheckFrequencySec;	//check for a waiting message every x

	bool connected;

	Scene *myscene;
	void createCurrentFixation(int xScreen, int yScreen, bool outsideLeft = false, bool outsideRight = false, bool outsideTop = false, bool outsideBottom = false);
void convertGazeDirToScreenCoords(float xDir, float yDir);
std::string *getSelectedIDObjectName(void);
int selectedID;
int getSelectedID(void);

FixationList *fixationList;
FixationEntry *currentFixation;

};

#endif
