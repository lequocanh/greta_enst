/***************************** License ************************************

  Copyright (c) 2005 by Communicative Machines
  http://www.cmlabs.com   All rights reserved.
  
  This library is released under a 
  Communicative Machines Public Binary Library (PBL) License version 1.0
  which should be included with the distribution that included this file.

  The library is free software in the sense that you can use and 
  redistribute it under the terms of the PBL as published by Communicative 
  Machines; http://www.cmlabs.com/licenses/pbl/

  Information about OpenAIR is available at
  http://www.mindmakers.org/openair

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
  OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/
#define AIRVERSION "Psyclone 1.1.7.nc 2007(c)Communicative Machines"
#define BINVERSION "1.1.7"
#define LICENSENAME "Communicative Machines Non-Commercial (NC) License"
#define LICENSEVERSION "1.0"
#define LICENSETYPE "NC"
#define LICENSEKEY ""
#define LICENSEPERIOD "6 months from first use"
#define CUSTOMERNAME "Non-Commercial Single User"
#define CUSTOMERSHORT "nc"
#define CUSTOMERADDRESS1 ""
#define CUSTOMERADDRESS2 ""
#define PRODUCTNAME "nc"
#define PRODUCTID "psyclone"
#define INDEXFILE "index.html"
#define TEXTLINE1 "This software is for short-term non-commercial use only"
#define TEXTLINE2 "Parts of this software are patent pending"
#define TEXTLINE3 "Use PsyclonePro for industrial-strength options and tools"
#define LICENSOR "Communicative Machines Incorporated"
#define BUILDDATE "Sun Oct 21 20:49:38 2007"
#define BUILDTIME ""
#define BUILDSYSTEM "CYGWIN_NT-5.1 LAP0019 i686 unknown unknown Cygwin"
