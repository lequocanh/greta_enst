//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// ReactiveBehaviour.cpp: implementation of the ReactiveBehaviour class.
//
//////////////////////////////////////////////////////////////////////


#include "ListenerIntentPlanner.h"
#include "BehaviourSelector.h"
#include <semaine/datatypes/xml/FML.h>
#include "GretaLogger.h"
#include <sstream>

extern std::list<GretaLogger*> listLog;


#define MAX_DELAY 2500
#define MIN_DELAY 1500


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BehaviourSelector::BehaviourSelector():cmlabs::JThread()
{
}

BehaviourSelector::BehaviourSelector(ListenerIntentPlanner *rbp, CentralClock *pc, AgentState *as):cmlabs::JThread()
{
	this->rbp=rbp;
	this->agentstate=as;
	this->pc=pc;
	randgen = new RandomGen();

	//log = semaine::cms::CMSLogger::getLog(rbp->getName());

	//InitSelector();
	mimicry=new DataBackchannel("MIMICRY");
	reactive=new DataBackchannel("REACTIVE");
	cognitive=new DataBackchannel("COGNITIVE");
	lastBackchannelSent=new DataBackchannel("LAST_BACKCHANNEL_SENT");
	iterfml = 0;
}

BehaviourSelector::~BehaviourSelector()
{
	delete randgen;
}

int BehaviourSelector::updateBackchannelRequest(DataBackchannel *db)
{
	DataBackchannel *databackchannel;
	GretaLogger* l;
	std::string msg;

	databackchannel=FindRequest(db->type);
	if(databackchannel==NULL)
	{
		msg = "Backchannel type " + db->type + " not found";
		l = new GretaLogger("ListenerIntentPlanner", msg, "error"); 
		listLog.push_back(l);
		return 0;
	}

	msg = "Backchannel type " + db->type + " found";
	l = new GretaLogger("ListenerIntentPlanner", msg, "info"); 
	listLog.push_back(l);

	if(db->priority<databackchannel->priority)
		databackchannel->CopyDataBackchannel(db);

	return 1;
}

DataBackchannel* BehaviourSelector::FindRequest(std::string type)
{
	if(type=="MIMICRY")
		return mimicry;
	if(type=="REACTIVE")
		return reactive;
	if(type=="COGNITIVE")
		return cognitive;
	return NULL;
}

void BehaviourSelector::run()
{
	std::string behaviour="";
	int il=0;
	int withActionSelection=1;

	XERCES_CPP_NAMESPACE::DOMDocument* bmlfml=NULL;

	//etienne
	if (withActionSelection==1)
	{
		//std::string behaviour="";
		std::string bml = "";
		std::string fml = "";
		char NIU[16] = "";
		char PRIORITYBC[16] = "";
		char PRIORITYM[16] = "";
		std::string BCtype = "";

		while(true)
		{
			Sleep(5);	
			//behaviour="";
			bmlfml=NULL;
			//sprintf(NIU, "%f", agentstate->getUserInterestLevel());
			//rbp->SendBehaviour(NIU, GretaName+NIU_MESSAGE_HEADER);

			if(reactive->communicativefunction.size()>0)
			{
				bmlfml=WriteFMLTree(reactive);
				//behaviour=WriteFML(reactive);
				//if(behaviour!="")
				if(bmlfml!=NULL)
				{
					printf("\n\nBACKCHANNEL for event: %s\n\n", reactive->zone.c_str());
					std::vector<std::string>::iterator itermp;
					for(itermp=reactive->communicativefunction.begin(); itermp!=reactive->communicativefunction.end(); itermp++)
						printf("%s ", (*itermp).c_str());
					BCtype = "REACTIVE";
					lastBackchannelSent->CopyDataBackchannel(reactive);
					std::ostringstream out;
					out << "fml_lip_" << iterfml;
					std::string contentID = out.str();
					long contentCreationTime = pc->GetTime();
					long userTime = contentCreationTime;
					rbp->getFMLSender()->sendXML(bmlfml, userTime, SEMAINE_CMS_EVENT_SINGLE, contentID, contentCreationTime);
					GretaLogger* l = new GretaLogger("ListenerIntentPlanner", "backchannel FML sent", "debug"); 
					listLog.push_back(l);
					//log->debug("backchannel FML sent");
					//mimicry->CleanDataBackchannel();
					reactive->CleanDataBackchannel();
					iterfml = iterfml + 1;
				}
			}

			if(mimicry->referencesMap.size()>=1)
			{
				bmlfml=WriteBMLTree(mimicry);
				//behaviour=WriteBML(mimicry);
				//if(behaviour!="")
				if(bmlfml!=NULL)
				{
					printf("\nMIMICRY for event: %s\n\n", mimicry->zone.c_str());
					BCtype = "MIMICRY";
					lastBackchannelSent->CopyDataBackchannel(mimicry);
					std::ostringstream out;
					out << "fml_lip_" << iterfml;
					std::string contentID = out.str();
					long contentCreationTime = pc->GetTime();
					long userTime = contentCreationTime;
					//std::cout << contentID<<"\n";
					//std::cout << userTime<<"\n";
					//std::cout << contentCreationTime;
					rbp->getBMLSender()->sendXML(bmlfml, userTime, SEMAINE_CMS_EVENT_SINGLE, contentID, contentCreationTime);
					GretaLogger* l = new GretaLogger("ListenerIntentPlanner", "backchannel BML sent", "debug"); 
					listLog.push_back(l);
					//log->info("backchannel BML sent");
					mimicry->CleanDataBackchannel();
					//reactive->CleanDataBackchannel();
					iterfml = iterfml + 1;
				}
			}
		}
	}
	else
	{
		/*
		while(true)
		{
			Sleep(5);
			behaviour=retrieveBackchannelrequest("COGNITIVE");
			if(behaviour=="")
				behaviour=selectBackchannelType();

		//	if(behaviour=="")
		//		printf("Non ci sono behaviour\n");//, behaviour.c_str());
			

			if(behaviour!="")
			{
				if(behaviour.substr(0,behaviour.find_first_of(";"))=="bml")
				{
					std::string bml=behaviour.substr(behaviour.find_first_of(";")+1);
					rbp->getBMLSender()->sendTextMessage(bml, rbp->sendTime());
					il=(int)(MAX_DELAY-(MAX_DELAY-MIN_DELAY)*agentstate->getUserInterestLevel());
					if(il<0) il=1300;
					Sleep(1000+il);
					mimicry->CleanDataBackchannel();
					reactive->CleanDataBackchannel();
				}
				else
				{
					std::string fml=behaviour.substr(behaviour.find_first_of(";")+1);
					//printf("send fml %s\n", fml.c_str());
					printf("send fml\n");
					rbp->getFMLSender()->sendTextMessage(fml, rbp->sendTime());
					il=(int)(MAX_DELAY-(MAX_DELAY-MIN_DELAY)*agentstate->getUserInterestLevel());
					if(il<0) il=1200;
					//printf("%d\n", il);
					Sleep(700+il);
					mimicry->CleanDataBackchannel();
					reactive->CleanDataBackchannel();
				}
			}
		}*/
	}
}


XERCES_CPP_NAMESPACE::DOMDocument* BehaviourSelector::WriteBMLTree(DataBackchannel *db)
{
	int index=1;
	char s[256];

	if(db->referencesMap.size()<1)
		return 0;

	XERCES_CPP_NAMESPACE::DOMDocument * bml = XMLTool::newDocument("bml", "http://www.mindmakers.org/projects/BML");
	XERCES_CPP_NAMESPACE::DOMElement * bmlRoot = bml->getDocumentElement();


	std::map<std::string, std::string>::iterator refiter;
	for(refiter=db->referencesMap.begin();refiter!=db->referencesMap.end();refiter++)
	{
		if((*refiter).first!="" && (*refiter).second!="")
		{
			sprintf(s,"s%d",index);
			DOMElement * temp = XMLTool::appendChildElement(bmlRoot, (*refiter).first);
			XMLTool::setAttribute(temp, "id", s);
			XMLTool::setAttribute(temp, "start", "0.0");
			XMLTool::setAttribute(temp, "end", "1.8");
			XMLTool::setAttribute(temp, "stroke", "1.0");
			DOMElement * tempdes = XMLTool::appendChildElement(temp, "description");
			XMLTool::setAttribute(tempdes, "level", "1");
			XMLTool::setAttribute(tempdes, "type", "gretabml");
			DOMElement * tempref = XMLTool::appendChildElement(tempdes, "reference");
			XMLTool::setTextContent(tempref, (*refiter).second);
			
			index+=1;
		}
	}
	//std::cout << XMLTool::dom2string(bml);
	return (bml);
}

/*
std::string BehaviourSelector::WriteBML(DataBackchannel *db)
{
	std::string bml;
	int time=pc->GetTime();
	int index=1;
	
	if(db->referencesMap.size()<1)
		return("");

	bml="bml;";
	bml+="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
	bml+="<!DOCTYPE bml SYSTEM \"bml/bml.dtd\" []>\n";
	bml+="<bml>\n";

	std::map<std::string, std::string>::iterator refiter;
	for(refiter=db->referencesMap.begin();refiter!=db->referencesMap.end();refiter++)
	{
		bml+=WriteSignalInBML((*refiter).first, (*refiter).second, time, index);
		index+=1;
	}
	bml+="</bml>\n";
	return(bml);
}

//std::string BehaviourSelector::WriteSignalInBML(DataBackchannel *db)
std::string BehaviourSelector::WriteSignalInBML(std::string modality, std::string reference, int time, int index)
{
	//Create BML for mimicry behaviour
	std::string bml="";
	char s[256];

	if(reference=="" || modality=="")
		return("");
	else
	{
		sprintf(s,"%d",index);
		bml+="<" + modality + " id=\"sig" + (std::string)s + "\"  "; // < MODALITY id="sigINDEX" start="0.000" end="1.5" stroke="1.0">
		//sprintf(s,"%.3f",(float)(time+1500)/1000);
		sprintf(s,"%.3f",0.0);
		
		bml+="start=\"" + (std::string)s + "\" ";//>\n";
		//bml+="end=\"" + db->end + "\" stroke=\"1.0\">\n";
		bml+="end=\"1.5\" stroke=\"1.0\">\n";
		//
		bml+="<description level=\"1\" type=\"gretabml\">\n";
		bml+="<reference>" + reference + "</reference>\n";
		bml+="<intensity>1.00</intensity>\n";
		bml+="<FLD.value>0.70</FLD.value>\n";
		bml+="<PWR.value>0.20</PWR.value>\n";
		bml+="<REP.value>0.00</REP.value>\n";
		bml+="<SPC.value>0.00</SPC.value>\n";
		bml+="<TMP.value>0.40</TMP.value>\n";
		bml+="</description>\n";
		bml+="</" + modality + ">\n";
		return(bml);
	}
}*/


XERCES_CPP_NAMESPACE::DOMDocument* BehaviourSelector::WriteFMLTree(DataBackchannel *db)
{
	int i=0;
	char s[256];

	std::vector<std::string>::iterator itermp;

	if(db->communicativefunction.size()==0)
		return 0;

	XERCES_CPP_NAMESPACE::DOMDocument * fmlApml = XMLTool::newDocument("fml-apml", "");
	XERCES_CPP_NAMESPACE::DOMElement * fmlApmlRoot = fmlApml->getDocumentElement();
	XERCES_CPP_NAMESPACE::DOMElement * fmlRoot = XMLTool::createElement(fmlApml, semaine::datatypes::xml::FML::E_FML, semaine::datatypes::xml::FML::namespaceURI);
	fmlApmlRoot->appendChild(fmlRoot);
	XMLTool::setAttribute(fmlRoot, "id", "fml1");

	for(itermp=db->communicativefunction.begin(); itermp!=db->communicativefunction.end(); itermp++)
	{
		sprintf(s,"b%d",i);
		DOMElement * temp = XMLTool::appendChildElement(fmlRoot, "backchannel");
		XMLTool::setAttribute(temp, "id", s);
		XMLTool::setAttribute(temp, "type", (*itermp));
		XMLTool::setAttribute(temp, "start", "0.0");
		XMLTool::setAttribute(temp, "end", "1.8");
		XMLTool::setAttribute(temp, "importance", "1.0");
		i+=1;
	}
	//std::cout << XMLTool::dom2string(fml);
	return (fmlApml);
}

/*
std::string BehaviourSelector::WriteFML(DataBackchannel *db)
{
	int i=0;
	char s[256];
	std::string fml="";
	std::vector<std::string>::iterator itermp;

	if(db->communicativefunction.size()==0)
		return("");

	//sprintf(s,"%d",db->time);
	//fml=(std::string)s + ";fml;";
	//std::string check=(std::string)s + ";fml;";
	fml="fml;";
	
	fml+="<?xml version=\"1.0\"?>\n";
	fml+="<!DOCTYPE fml-apml SYSTEM \"apml/fml-apml.dtd\" []>\n";
	fml+="<fml-apml>\n";

	fml+="<fml>\n";

	for(itermp=db->communicativefunction.begin(); itermp!=db->communicativefunction.end(); itermp++)
	{
//			check+=(*itermp) + " ";
			i+=1;
			sprintf(s,"%d",i);
			fml+="<backchannel id=\"p" + (std::string)s + "\" ";
			fml+="type=\"" + (*itermp) + "\" ";
			//sprintf(s,"%.3f",(float)(pc->GetTime()+1500)/1000);
			sprintf(s,"%.3f",(float)0.0);
			//DEFINE END!!
			fml+="start=\"" + (std::string)s + "\" end=\"1.8\" ";
			//fml+="importance=\"" + 1.0/(float)vecCommFun.size() + "\"/>\n";
			fml+="importance=\"1.0\"/>\n";
	}
	
//	fml+="<emotion id=\"e\" type=\"" + db->emotionalstate + "\" regulation=\"felt\" start=\"0.0\" end=\"2.0\" importance=\"1.0\"/>\n";
	fml+="</fml>\n";
	fml+="</fml-apml>\n";

	//if(check!="")
	//	printf("%s\n", check.c_str());
	return(fml);
}*/


