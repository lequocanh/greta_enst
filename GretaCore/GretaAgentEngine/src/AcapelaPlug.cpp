#include "AcapelaPlug.h"

#ifdef USE_ACAPELA

#define _BABTTSDYN_IMPL_
#include "Acapela\ifBabTTSDyn.h"
#undef _BABTTSDYN_IMPL_

#include "SDL\SDL_audio.h"
#include <algorithm>
#include "IniManager.h"

bool AcapelaPlug::initialized;
char* AcapelaPlug::sapi2greta[22];
std::string AcapelaPlug::pathAcapela;
std::map<std::string,std::string> AcapelaPlug::langMap;
AcapelaPlug* AcapelaPlug::current;
LPBABTTS AcapelaPlug::babtts;

extern IniManager inimanager;

void AcapelaPlug::initAcapela(){
	if(!initialized){
		pathAcapela = inimanager.GetValueString("ACAPELAPATH") + "/AcaTts.dll";
		BabTtsInitDllEx(pathAcapela.c_str());
		BabTTS_Init();
		babtts = BabTTS_Create();
		initialized = true;
		sapi2greta[0] = "#";
		sapi2greta[1] = "a";
		sapi2greta[2] = "O1";
		sapi2greta[3] = "o";
		sapi2greta[4] = "E1";
		sapi2greta[5] = "e";
		sapi2greta[6] = "i1";
		sapi2greta[7] = "u";
		sapi2greta[8] = "o1";
		sapi2greta[9] = "a1";
		sapi2greta[10] = "o+i";
		sapi2greta[11] = "a+i";
		sapi2greta[12] = "h";
		sapi2greta[13] = "r";
		sapi2greta[14] = "l";
		sapi2greta[15] = "s";
		sapi2greta[16] = "tS";
		sapi2greta[17] = "d";
		sapi2greta[18] = "f";
		sapi2greta[19] = "t";
		sapi2greta[20] = "k";
		sapi2greta[21] = "p";

		langMap["english"] = "British";
		langMap["gbenglish"] = "British";
		langMap["englishgb"] = "British";
		langMap["en"] = "British";
		langMap["gb"] = "British";
		langMap["british"] = "British";
		langMap["usenglish"] = "USEnglish";
		langMap["englishus"] = "USEnglish";
		langMap["us"] = "USEnglish";
		langMap["french"] = "French";
		langMap["francais"] = "French";
		langMap["fr"] = "French";
		langMap["italian"] = "Italian";
		langMap["italiano"] = "Italian";
		langMap["it"] = "Italian";
		langMap["german"] = "German";
		langMap["arabic"] = "Arabic";
		langMap["belgiandutch"] = "BelgianDutch";
		langMap["brazilian"] = "Brazilian";
		langMap["canadianfrench"] = "CanadianFrench";
		langMap["quebequois"] = "CanadianFrench";
		langMap["czech"] = "Czech";
		langMap["danish"] = "Danish";
		langMap["dutch"] = "Dutch";
		langMap["faroese"] = "Faroese";
		langMap["finnish"] = "Finnish";
		langMap["greek"] = "Greek";
		langMap["icelandic"] = "Icelandic";
		langMap["norwegian"] = "Norwegian";
		langMap["polish"] = "Polish";
		langMap["portuguese"] = "Portuguese";
		langMap["russian"] = "Russian";
		langMap["spanish"] = "Spanish";
		langMap["swedish"] = "Swedish";
		langMap["turkish"] = "Turkish";
	}
}

BabTtsError WINAPI AcapelaPlug::acapelaEvents(LPBABTTS lpBabTTS, unsigned long Msg, unsigned long dwCallbackInstance, unsigned long dwMsgInfo){
	if(current != NULL){
		if(Msg == BABTTS_MSG_PHONE){
			unsigned long dur,vis;
			unsigned long param = dwMsgInfo;
			BabTTS_PhoGetDuration(lpBabTTS, param,&dur);
			BabTTS_PhoGetViseme(lpBabTTS, param,&vis);
			
			//sometime, form some voice, the value of the viseme is fake.
			//so, we warp it to 0 :
			int vis_ = vis;
			if(vis_<0 || 21<vis_)
				vis = 0;

			current->timecount += dur;
			ACAPELA_VISEME _vis;
			_vis.duration = dur;
			_vis.viseme = sapi2greta[vis];
			current->visemeList.push_back(_vis);
		}
		else if(Msg == BABTTS_MSG_USERMRK){
			ACAPELA_TM tm;
			tm.num = dwMsgInfo;
			tm.when = current->timecount;
			current->tmList.push_back(tm);
		}
	}
	return E_BABTTS_NOERROR;
}

AcapelaPlug::AcapelaPlug(std::string audioFile){
	initAcapela();
	audioFileName = audioFile;
}

bool AcapelaPlug::setVoice(std::string language, std::string gender){

	// convert the gender
	std::string gendertomap = std::string(gender);
	std::transform(gendertomap.begin(), gendertomap.end(),gendertomap.begin(), tolower);
	BabTtsGender targetGender = BABTTS_GENDER_NEUTRAL;
	if(gendertomap.find("female")!=-1)
		targetGender = BABTTS_GENDER_FEMALE;
	else if(gendertomap.find("male")!=-1)
		targetGender = BABTTS_GENDER_MALE;

	// convert the language
	std::string languagetomap = std::string(language);
	std::transform(languagetomap.begin(), languagetomap.end(),languagetomap.begin(), tolower);
	std::string targetLanguage = langMap[languagetomap];
	if(targetLanguage=="") 
		targetLanguage=language;//try the original
	std::string tempLanguage;

	// find the voice
	BabTtsError babError=E_BABTTS_NOERROR; 
	BABTTSINFO voiceInfo; 
	long tempnumber=BabTTS_GetNumVoices();
	for(int i=0;i<tempnumber;++i) { 
		char szVoice[50]; 
		szVoice[0]='\0'; 
		memset(&voiceInfo,0,sizeof(BABTTSINFO)); 
		babError=BabTTS_EnumVoices(i,szVoice); 
		if(babError!=E_BABTTS_NOERROR) { 
			printf("BabTTS_EnumVoices error: %d\n",babError); 
			return false; 
		}
		babError=BabTTS_GetVoiceInfo(szVoice,&voiceInfo); 
		if(babError!=E_BABTTS_NOERROR) { 
			printf("BabTTS_GetVoiceInfo error: %d\n",babError); 
			return false; 
		}
		tempLanguage = std::string(voiceInfo.szLanguage);
		printf("%s %s %d\n",voiceInfo.szSpeaker,voiceInfo.szLanguage,voiceInfo.Gender);
		if((voiceInfo.Gender==targetGender) && (tempLanguage==targetLanguage)){
			voice = std::string(voiceInfo.szName);
			return true;
		}
	}
	printf("Can't find an Acapela's voice : %s %s\n",targetLanguage.c_str(), gender.c_str()); 
	return false;
}

bool AcapelaPlug::setVoice(std::string name){
	std::string tempName;
	// find the voice
	BabTtsError babError=E_BABTTS_NOERROR; 
	BABTTSINFO voiceInfo; 
	long tempnumber=BabTTS_GetNumVoices();
	for(int i=0;i<tempnumber;++i) { 
		char szVoice[50]; 
		szVoice[0]='\0'; 
		memset(&voiceInfo,0,sizeof(BABTTSINFO)); 
		babError=BabTTS_EnumVoices(i,szVoice); 
		if(babError!=E_BABTTS_NOERROR) { 
			printf("BabTTS_EnumVoices error: %d\n",babError); 
			return false; 
		}
		babError=BabTTS_GetVoiceInfo(szVoice,&voiceInfo); 
		if(babError!=E_BABTTS_NOERROR) { 
			printf("BabTTS_GetVoiceInfo error: %d\n",babError); 
			return false; 
		}
		tempName = std::string(voiceInfo.szSpeaker);

		inimanager.ToUppercase(tempName);
		inimanager.ToUppercase(name);
		if(tempName==name){
			voice = std::string(voiceInfo.szName);
			return true;
		}
	}
	printf("Can't find an Acapela's voice : %s\n",name.c_str()); 
	return false;
}


void AcapelaPlug::getAudio(char** buffer,int* size){
	SDL_AudioSpec spec;
	Uint8 * bufferFromSDL;
	Uint32 sizeFromSDL=0;
	SDL_LoadWAV(audioFileName.c_str(),&spec,&bufferFromSDL,&sizeFromSDL);

	(*buffer) = (char*) malloc ( sizeof(char) * (sizeFromSDL) ); 
	for(int i=0; i<sizeFromSDL;++i)
		(*buffer)[i] = (char)( bufferFromSDL[i]);
	(*size) = sizeFromSDL;
	SDL_CloseAudio();
}

void AcapelaPlug::launch(std::string textSAPI){
	current = this;
	timecount=0;
	BabTtsError error;
	error = BabTTS_Open(babtts,voice.c_str(),BABTTS_USEDEFDICT);
	if(error != E_BABTTS_NOERROR)
		printf("Error while opening voice %s: %d\n",voice.c_str(),error);
	else{
		error = BabTTS_SetSettings(babtts,BABTTS_PARAM_MSGMASK,BABTTS_EVENT_PHONE|BABTTS_EVENT_USERMRK);
		if(error!=E_BABTTS_NOERROR)
			printf("Error BabTTS_SetSettings: %d\n",error);
		error = BabTTS_SetCallback(babtts,(void*)(acapelaEvents),BABTTS_CB_FUNCTION);
		if(error!=E_BABTTS_NOERROR)
			printf("Error BabTTS_SetCallback: %d\n",error);
		error=BabTTS_Write(babtts,textSAPI.c_str(),audioFileName.c_str(), BABTTS_FILE_WAV|BABTTS_TAG_SAPI|BABTTS_TXT_ANSI);
		if(error!=E_BABTTS_NOERROR)
			printf("Error BabTTS_Write: %d\n",error);
	}
	current = NULL;
}

#endif //ifdef USE_ACAPELA
