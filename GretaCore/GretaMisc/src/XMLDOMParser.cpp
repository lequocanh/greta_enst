//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// XMLDOMParser.cpp: implementation of the XMLDOMParser class.
//
//////////////////////////////////////////////////////////////////////

#include <fstream>
#include <iostream>
#include <string>
#include "XMLDOMParser.h"
#include "XMLDOMTree.h"
#include <xercesc/sax/SAXParseException.hpp>

using namespace XERCES_CPP_NAMESPACE;

//for general comments refer to the header file

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

XMLDOMParser::XMLDOMParser()
{
	validating=true;
}

XMLDOMParser::~XMLDOMParser()
{
}
XMLDOMTree * XMLDOMParser::ParseBuffer(std::string buffer){
	try{
		DOMDocument *dom = XercesTool::parse(buffer);
		XMLDOMTree *tree = new XMLDOMTree(dom);

		return tree;
	}
	catch(XERCES_CPP_NAMESPACE::SAXParseException exp){
		return 0;
	}
}
XMLDOMTree * XMLDOMParser::ParseBuffer(const char *buffer)
{
	return ParseBuffer((std::string)buffer);
}

XMLDOMTree * XMLDOMParser::ParseFile(std::string name)
{
	std::string mystring=readFile(name);
	if(mystring=="")
		printf("Parsing impossible : file is empty\n");

	try{
		DOMDocument *dom = XercesTool::parseFile(name, validating);
		XMLDOMTree *tree = new XMLDOMTree(dom);

		return tree;
	}
	catch(XERCES_CPP_NAMESPACE::SAXParseException exp){
		return 0;
	}
}

XMLDOMTree * XMLDOMParser::ParseBufferWithXSD(const char *buffer,std::string xsdpath)
{
	std::string mystring=(std::string)buffer;

	DOMDocument *dom = XercesTool::parseAndValidate(mystring, xsdpath, false);

	XMLDOMTree *tree = new XMLDOMTree(dom);

	return tree;
}

XMLDOMTree * XMLDOMParser::ParseFileWithXSD(std::string name,std::string xsdpath)
{
	std::string mystring=readFile(name);

	return ParseBufferWithXSD(mystring.c_str(), xsdpath);
}

void XMLDOMParser::SetValidating(bool v)
{
	this-> validating=v;
}

std::string XMLDOMParser::readFile(std::string path)
{
	std::string buf;
	std::string line;
	std::ifstream in(path.c_str());

	while(std::getline(in,line))
		buf += line;
	//std::cout << "read: " << buf << "\n";
	return buf;
}





