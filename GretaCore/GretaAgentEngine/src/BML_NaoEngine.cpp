//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// /BML Engine.cpp: implementation of the /BML Engine class.
//
//////////////////////////////////////////////////////////////////////

//BML Engine for Greta
//http://twiki.isi.edu/Public/BMLSpecification

#include ".\BML_NaoEngine.h"
#include "APMLCommunicativeActTimingCalc.h"
#include "IniManager.h"
#include "HeadEngine.h"
#include "TorsoEngine.h"
#include "RandomGen.h"
#include "FileNames.h"
#include "bapanimationjoiner.h"
#include "FAPAnimationJoiner.h"
#include "BAPwriter.h"
#include "XMLDOMParser.h"
//#include <crtdbg.h>
//#include <math.h>


extern IniManager inimanager;
extern std::string ini_filename;
extern FileNames filenames;

extern FILE* gesture_log;
extern FILE* face_log;
extern RandomGen *randomgen;


BML_NaoEngine::BML_NaoEngine(void)
{
	signals.clear();
	this->phofile="";
	
	if(inimanager.initialized==false)
	{
		if (ini_filename!="")
			inimanager.ReadIniFile(ini_filename);
		else
			inimanager.ReadIniFile("greta_psyclone.ini");
	}

	gesturetorsolength=0.0f;
	if(randomgen==0)
		randomgen = new RandomGen();

	te = new TorsoSpace::TorsoEngine();
	he = new HeadSpace::HeadEngine();
	mMPlanner=0;
	faceengine=0;


	speech_size=0;
}

BML_NaoEngine::~BML_NaoEngine(void)
{
	if(randomgen!=0)
	{
		delete randomgen;
		randomgen=0;
	}
	if(te!=0)
	{
		delete te;
		te=0;
	}
	if(he!=0)
	{
		delete he;
		he=0;
	}
	if(mMPlanner!=0)
	{
		delete mMPlanner;
		mMPlanner=0;
	}
	if(faceengine!=0)
	{
		delete faceengine;
		faceengine=0;
	}
}

bool BML_NaoEngine::hasNoEndFaceSignals() 
{ 
	return noend_facesignals;
}


int BML_NaoEngine::Execute(std::string BMLfilename,char *buffer,bool writetofile,BAPFrame* startbf,FAPFrame* startff,bool isrealtime)
{

	    content_of_speech="";

	    filenames.Base_File="";	
	    filenames.Phonemes_File="";
		filenames.Script_File="";
		filenames.Text_File="";
		filenames.Wav_File="";
		filenames.Turns_File="";
	

	std::string fapfilename,bapfilename;


	
	if(BMLfilename=="")
	{
		fapfilename="buffer";
		filenames.Base_File=inimanager.Program_Path+"output/"+fapfilename;
		fapfilename=inimanager.Program_Path+"output/"+fapfilename+".fap";

		bapfilename="buffer";
		bapfilename=inimanager.Program_Path+"output/"+bapfilename+".bap";

		filenames.Fap_File=fapfilename;
		filenames.Bap_File=bapfilename;
		
	}
	else
	{
		filenames.BuildFileNames(BMLfilename);
	}

	

	if(LoadBML(BMLfilename,buffer,isrealtime)==0)
		return 0;

	// IT SHOULD STOP HERE FOR THIS PROCEDURE (AND TAKE TAKE THE REST IN A SEPARATED PROCEDURE)
	// BECAUSE OTHER AGENTS (I.E. NAO ROBOT) DOES NOT NEED THE REST.
	

			noend_facesignals=false;
			
			//very special case:
			//check if the signal is infinite
			//TO DO: what if there is an infinitive signal and the finite one?
			std::vector<MMSystemSpace::Signal>::iterator iter;
			for(iter=signals.begin();iter!=signals.end();iter++) 
			{			
				if((iter->noend==true)&&(((*iter).modality=="head")||((*iter).modality=="face")||((*iter).modality=="gaze")))
					noend_facesignals=true;			          
			}			
		
/*
	faceheadlength=0;


	bool useheadengine;

	useheadengine=inimanager.GetValueInt("USE_HEADENGINE")!=0;

	if(useheadengine==true)
	{
		if(GenerateHead("")==0)
		{
			this->signals.clear();
			return 0;
		}
	}

	FAPAnimationJoiner faj;

	if(GenerateFace("",startff,isrealtime)==0)
	{
		this->signals.clear();
		return 0;
	}

	
	if(useheadengine==true)
	{
		faj.Join(faceengine->GetAnimation(),he->GetAnimation());
	}

	
	if(writetofile)
	{
		FAPwriter fw;
		fw.WriteToFile(faceengine->GetAnimation(),filenames.Fap_File,25);
	}

	if(GenerateTorso(filenames.Bap_File)==0)
	{
		this->signals.clear();
		return 0;
	}

	
	//int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );
	//tmpFlag |= _CRTDBG_CHECK_ALWAYS_DF;
	//_CrtSetDbgFlag( tmpFlag );
*/
	if(GenerateGestures(filenames.Bap_File,startbf)==0)
	{
		this->signals.clear();
		return 0;
	}

/*	
	BAPAnimationJoiner baj;

	if(writetofile)
	{
		BAPWriter bw;
		bw.WriteToFile(baj.Join(mMPlanner->GetAnimation(),te->GetAnimation()),filenames.Bap_File);
		
	}
	else
	{
		baj.Join(mMPlanner->GetAnimation(),te->GetAnimation());
	}

*/
	this->signals.clear();

	return 1;
}

FaceEngine* BML_NaoEngine::GetFaceEngine()
{
	return this->faceengine;
}

GestureSpace::GesturePlanner* BML_NaoEngine::GetGPlanner()
{
	return this->mGPlanner;
}

GestureSpace::MotorPlanner* BML_NaoEngine::GetMPlanner()
{
	return this->mMPlanner;
}

int BML_NaoEngine::GenerateGestures(std::string bapfilename,BAPFrame* startbf)
{	

	bool realtime_gestureengine;
	int numberOfGesture = 0;
	realtime_gestureengine = inimanager.GetValueInt("REALTIME_GESTUREENGINE") == 1;

	gesture_log=fopen("logs/gesture_engine_log.txt","w");

	float start_time;

	start_time=0.0f;
	gesturetorsolength+=1.0f;


	mGPlanner=new GestureSpace::GesturePlanner();

	mMPlanner=new GestureSpace::MotorPlanner();

	mGPlanner->Init();

	if(!realtime_gestureengine)
	{
		mGPlanner->Request("REST=LEGS_BOTH", -3.0, -2.5, -2.1, -2, -2, -2, -2, -2);
	}

	std::vector<MMSystemSpace::Signal>::iterator iter;
	gestures.clear();
	for(iter=signals.begin();iter!=signals.end();iter++)
	{
		if((*iter).modality=="gesture")
		{
			numberOfGesture++;
			
			gestures.push_back((*iter));

			
			
			//printf("gesture %s timings %f,%f,%f\n",(*iter).name.c_str(),(*iter).start,((*iter).start+(*iter).end)/2.0f,(*iter).end);
			if((*iter).strokes.empty())
			{
				//(*iter).strokes.push_back((*iter).duration/2.0f); // It's realy arbitrary ???
				(*iter).strokes.push_back(-((*iter).start + 1.0)); // would prefer negative absolute time value (Gesture planner manages this)

			}
			if((*iter).strokes.size()==1)
				mGPlanner->Request(
				ToUpper((*iter).reference).c_str(),
				(*iter).start,
				(*iter).start+(*iter).strokes[0],
				(*iter).start+(*iter).duration,
				(*iter).GetParamValuef("SPC.value"),
				(*iter).GetParamValuef("TMP.value"),
				(*iter).GetParamValuef("FLD.value"),
				(*iter).GetParamValuef("PWR.value"),
				(*iter).GetParamValuef("REP.value"),
				false
				);
			else
			{
				std::vector<float>::iterator strokiter;
				for(strokiter=(*iter).strokes.begin();strokiter!=(*iter).strokes.end();strokiter++)
				{
					(*strokiter)+=(*iter).start;
				}
				float str=(*iter).strokes[0];
				(*iter).strokes.erase((*iter).strokes.begin());
				mGPlanner->RequestWithRepetition(
					ToUpper((*iter).reference).c_str(),
					(*iter).start,
					str,
					(*iter).start+(*iter).duration,
					(*iter).GetParamValuef("SPC.value"),
					(*iter).GetParamValuef("TMP.value"),
					(*iter).GetParamValuef("FLD.value"),
					(*iter).GetParamValuef("PWR.value"),
					(*iter).GetParamValuef("REP.value"),
					(*iter).strokes,
					false);
			}

			
			if(((*iter).start+(*iter).duration+2)>gesturetorsolength)
				gesturetorsolength=(*iter).start+(*iter).duration+2;
		}

	}

	// hien thi xem co dung gesture ko
	for(int i=0;i<gestures.size();i++)
		std::cout<<gestures[i].reference<<std::endl;

	if(realtime_gestureengine && numberOfGesture<1) //when we have no gestures, we don't need to continue
		return 1;

	if(!realtime_gestureengine)
		mGPlanner->Request("REST=LEGS_BOTH", gesturetorsolength+1.50,gesturetorsolength+1.60,gesturetorsolength+1.75,-2,-2, -2, -2, -2);
	mGPlanner->InsertRestKeys(0);

	mMPlanner->Init(mGPlanner->GetGestureVector(),int(start_time*GLOBAL_FPS),int((gesturetorsolength+1.75)*GLOBAL_FPS));


	fclose(gesture_log);

	return 1;
}

int BML_NaoEngine::GenerateFace(std::string fapfilename,FAPFrame* startff,bool isrealtime)
{
	float attack,decay,sustain,release;
	CommunicativeAct* first_comm_act,*comm_act;

	CommunicativeActTimingCalc timingscalculator(this);

	face_log=fopen("logs/face_engine_log.txt","w");

	bool useheadengine;

	useheadengine=inimanager.GetValueInt("USE_HEADENGINE")!=0;

	//printf("face engine started\n");

	comm_act=new CommunicativeAct();

	comm_act->function=(char*)malloc(5*sizeof(char));
	sprintf(comm_act->function,"APML");
	comm_act->value=(char*)malloc(1*sizeof(char));
	sprintf(comm_act->value,"");

	first_comm_act=comm_act;

	faceengine=new FaceEngine(first_comm_act,comm_act,true);

	//add a first (non neutral) frame of interpolation if it is neccessary
	if (startff!=0)
		faceengine->setFirstFrame(startff,true);

	//it is supposed that there is only one speech act in each bml	
	speech_delay=0.0f;

	std::vector<MMSystemSpace::Signal>::iterator iter;

	for(iter=signals.begin();iter!=signals.end();iter++)
	{
		if(((*iter).modality=="face")||((*iter).modality=="gaze")||((*iter).modality=="head"))
		{
			if(useheadengine==true)
				continue;
			std::string classname,instancename;
			if((*iter).reference!="")
			{
				classname=(*iter).reference.substr(0,(*iter).reference.find_first_of("="));
				instancename=(*iter).reference.substr((*iter).reference.find_first_of("=")+1);
			}
			else
			{
				classname=(*iter).type;
				if(classname=="NOD")
				{
					classname="HEAD";
					instancename="NOD";
				}
				if(classname=="SHAKE")
				{
					classname="HEAD";
					instancename="SHAKE";
				}
				if(classname=="TOSS")
				{
					classname="HEAD";
					instancename="TOSS";
				}
				if(classname=="ORIENT")
				{
					classname="DEICTIC";
					instancename=(*iter).direction;
				}

			}
			comm_act->next=new CommunicativeAct();
			comm_act=(CommunicativeAct*)comm_act->next;
			comm_act->function=(char*)malloc((classname.length()+1)*sizeof(char));
			sprintf(comm_act->function,"%s",classname.c_str());
			comm_act->value=(char*)malloc((instancename.length()+1)*sizeof(char));
			sprintf(comm_act->value,"%s",instancename.c_str());
			comm_act->start_time=(*iter).start;
			comm_act->dur_time=(*iter).duration;//-(*iter).start;
			comm_act->activation=1;

			comm_act->setIntensity((*iter).GetIntensity());

			comm_act->SetExpressivitySPC((*iter).GetParamValuef("SPC.value"));
			comm_act->SetExpressivityTMP((*iter).GetParamValuef("TMP.value"));
			comm_act->SetExpressivityFLD((*iter).GetParamValuef("FLD.value"));
			comm_act->SetExpressivityPWR((*iter).GetParamValuef("PWR.value"));
			comm_act->SetExpressivityREP((*iter).GetParamValuef("REP.value"));

			timingscalculator.CalcAttackDecaySustainRelease(comm_act,&attack,&decay,&sustain,&release);
			comm_act->AddPhase(caphase_attack,attack);
			comm_act->AddPhase(caphase_decay,decay);
			comm_act->AddPhase(caphase_sustain,sustain);
			comm_act->AddPhase(caphase_release,release);

			comm_act->type=(*iter).modality;

			comm_act->conflict_comm_act_list=0;
			comm_act->next=0;

			if(((*iter).start+(*iter).duration)>faceheadlength)
				faceheadlength=(*iter).start+(*iter).duration;
		}

		//it is supposed that there is only one sppech act in each bml	

		if((*iter).modality=="speech")
		{
			//if we use pho file (eg. not a case for usingmary 3 and usingmary4

			////do not change this line
			
			if ( (*iter).usingMary==2) {
				int kup = (*iter).reference.find(".pho");
				//			
				if( ((*iter).reference!="") && ( kup > 0 )  )			
					filenames.Phonemes_File=(*iter).reference;
				else 
					filenames.Phonemes_File="";
			}

			if(
				( (*iter).start!=0 ) &&( speech_delay==0.0f ) &&( isrealtime ) 
				)
				speech_delay=(*iter).start;
		}
	}


//	if(this->hasSpeech()==false)
//		filenames.Phonemes_File="";

	if(faceengine->CalculateTurn(fapfilename,faceheadlength+1.0f,0,0, 1,speech_delay)==0)
	{
		delete faceengine;
		//printf("face engine ended with errors\n");
		fclose(face_log);
		return 0;
	}

	//printf ("faceheadlength %d : \n", faceheadlength);

	//printf("face engine ended\n");

	fclose(face_log);

	return 1;
}

int BML_NaoEngine::GenerateTorso(std::string bapfilename)
{
	te->Cleanup();

	float start_time;

	start_time=0.0f;
	gesturetorsolength=0.0f;

	//printf("torso engine started\n");

	if(!signals.empty())
	{
		std::vector<MMSystemSpace::Signal>::iterator iter;
		for(iter=signals.begin();iter!=signals.end();iter++)
		{
			if((*iter).modality=="torso")
			{
				if((*iter).strokes.empty())
				{
					(*iter).strokes.push_back((*iter).duration/2.0f);
				}

				te->ScheduleTorsoGesture((*iter),(*iter).reference);

				if(((*iter).start+(*iter).duration)>gesturetorsolength)
					gesturetorsolength=(*iter).start+(*iter).duration;
			}
		}

		te->RenderAnimation(0,gesturetorsolength+1,25);
	}


	//printf("torso engine ended\n");

	return 1;
}

int BML_NaoEngine::GenerateHead(std::string fapfilename)
{
	he->Cleanup();

	float start_time;

	start_time=0.0f;

	//printf("head engine started\n");

	if(!signals.empty())
	{
		std::vector<MMSystemSpace::Signal>::iterator iter;
		for(iter=signals.begin();iter!=signals.end();iter++)
		{
			if((*iter).modality=="head")
			{
				if((*iter).strokes.empty())
				{
					(*iter).strokes.push_back((*iter).duration/2.0f);
				}

				he->ScheduleHeadGesture((*iter),(*iter).reference);

				if(((*iter).start+(*iter).duration)>faceheadlength)
					faceheadlength=(*iter).start+(*iter).duration;
			}
		}

		he->RenderAnimation(0,faceheadlength+1,25);
	}


	//printf("head engine ended\n");

	return 1;
}


std::string BML_NaoEngine::ToUpper(std::string s)
{
	std::string r;
	int c;
	r="";
	for(int i=0; i<(int)s.length(); i++)
	{
		c=toupper(s[i]);
		std::string character(1,c);
		r=r+character;
	}
	return r;
}

/*int BML_NaoEngine::Translate(XMLGenericTree *t)
{
	int usingMary=1;

	char duration[256];
	XMLGenericTree * p;

	//for(iterch=t->child.begin();iterch!=t->child.end();iterch++)
	for(XMLGenericTree::iterator iterch=t->begin();iterch!=t->end();++iterch)
	{
			p = *iterch;
			if (p->isTextNode()) continue;
			if(p->GetName()=="mark")
			{
				// TODO: this is the only place where this method is
				// called. Should be removed in favor of a more read-only
				// interface for XMLGenericTree, and does not work
				// for XMLDOMTree anyway
				p->ModifyAttributeName("name", "id");
			}
			if(p->GetName()=="mary:syllable")
			{
				usingMary=0;
			}
			if(p->GetName()=="mary:ph")
			{
				sprintf(duration, "%d", (int)(p->GetAttributef("d")*1000));
				p->SetAttribute("d",(std::string)duration);

				sprintf(duration, "%d", (int)(p->GetAttributef("end")*1000));
				p->SetAttribute("end",(std::string)duration);
			}
			
			usingMary*=Translate(p);
		}
	return usingMary;
}*/

int BML_NaoEngine::LoadBML(std::string BMLfilename,char *buffer,bool isrealtime)
{

	//printf("%s LoadBML" ,filenames.Phonemes_File.c_str());

	//XMLDOMParser
	XMLDOMParser *p;
	XMLGenericTree *t;
	XMLGenericTree *app;
	//int iCh;
	std::list<XMLGenericTree*>::iterator iterbl;
	XMLGenericTree *chld;

	speech=0;
	
	p=new XMLDOMParser(); //XMLDOMParser();

	//XML MODIFICATION
	//to read namespaces we avoid validation
	//p->SetValidating(true);
	p->SetValidating(false);
	if(BMLfilename!="")
		t=p->ParseFile(BMLfilename);
	else
		t=p->ParseBuffer(buffer);

	signals.clear();

	if(t!=0)
	{
				
//		for(iterch=t->child.begin();iterch!=t->child.end();iterch++)
		for(XMLGenericTree::iterator iterch=t->begin();iterch!=t->end();++iterch)
		{
			chld = *iterch;
			if (chld->isTextNode()) continue;
			MMSystemSpace::Signal s;
			//decode bml-to signal
			if(s.StoreBML(chld,inimanager)==0)
			{ 
				delete t;
				return 0;
			}
			
			//SPEECH PARAMETERS UPDATE
			if(s.modality=="speech")
			{		
				if(inimanager.GetValueString("TEXT_TO_SPEECH")=="activemary") s.usingMary=0;
				if(inimanager.GetValueString("TEXT_TO_SPEECH")=="openmary") s.usingMary=1;	
//				if(s.voice=="activemary") s.usingMary=0;
//				if(s.voice=="openmary") s.usingMary=1;			
				//THE VALUES OF usingMary may be changed here; two more cases are considered
				s.updateUsingMary();
			}
		 	signals.push_back(s);
		}
	}
	else
	{
		printf("Error parsing %s\n", BMLfilename.c_str());
		return 0;
	}
	
	std::sort(signals.begin(),signals.end());
	
	int  ln;

	speech=0;
	std::vector<MMSystemSpace::Signal>::iterator itersig;
	for(itersig=signals.begin();itersig!=signals.end();itersig++)
	{

		if((*itersig).modality=="speech")
		{
			speech=&(*itersig);

			//generates pho and does the temporalisation - four cases are distinguished 

			//in some cases (e.g. 1 it asks openmary to send the file
			//in other cases (I.E. 0, 2, 3) It reads the data from the files

			if(speech->TemporizeTimeMarkers((*itersig).start)==0)
			{
				delete t;
				return 0;
			}
	
			//adds the wave file

			//semaine
			if(speech->usingMary==0)
			{
				//who knows

			}

			//default - we need the output from the openmary
			if(speech->usingMary==1)
			{
				if(filenames.Wav_File=="")
					filenames.Wav_File=filenames.Base_File+".wav";


				//if engine is real time
				//use "the speech without wav file" procedure
				if(isrealtime)
				{
					//initial size of the speech is 0
					speech_size=0;
					content_of_speech=speech->GenerateSpeechToChar(&speech_size);				
					if(content_of_speech==0)
					{
						speech_size=0;
						delete t;
						return 0;
					}
				} 
				else
				{
					//offline version : use openmary to generate wav and save it to file

					if(speech->GenerateSpeech(filenames.Wav_File)==0)
					{
						delete t;
						return 0;
					}
				}
			}


			//pho file - the wave is elsewere
			if(speech->usingMary==2)
			{
				if(filenames.Wav_File=="") 
				{
					//if speech-> reference empty throw error
					 
					//std::string temp = filenames.Base_File+"/../../";
					
					std::string temp = speech->reference;
        					
					filenames.Wav_File =  temp.replace(temp.length()-4,4,".wav");

				}	
				
				//if it is realtime read this file and put it into char
				if(isrealtime)
				{
	speech_size=0;

					FILE * pFile;
					char c;

					std::string path = inimanager.Program_Path;
					std::string filetemp = filenames.Wav_File;
					filetemp = path+filetemp; 
					//printf("wav file %s", filetemp.c_str());
					pFile=fopen ( filetemp.c_str() ,"rb");

					//initial size of the speech is 0				

					fseek(pFile, 0, SEEK_END); // seek to end of file
					int file_size = ftell(pFile); // get current file pointer
					fseek(pFile, 0, SEEK_SET);

					//printf("The file size in bytes is %ld\n", file_size);					

					if (pFile==NULL) printf("no file");
					
					else
					{							
						content_of_speech = (char*) malloc ( file_size*sizeof(char));
						for (int i=0;i<file_size;i++)
						{							
							c = getc (pFile);	

							//content_of_speech = (char*) realloc (content_of_speech, speech_size* sizeof(char));
							content_of_speech[i]=c;			
						}

						fclose (pFile);

					}

					content_of_speech = (char*) realloc (content_of_speech, (file_size+2) * sizeof(char));

					content_of_speech[++file_size]=EOF;

					speech_size=file_size;

			
					if(content_of_speech==0)
					{
						speech_size=0;
						delete t;
						return 0;
					}
				}
				// else we are in offline version; the file will be read when neeeded

			}

			//openmarout file - the wave is elsewere
			if(speech->usingMary==3)
			{
		
				if(filenames.Wav_File=="") 
				{
					//if speech-> reference empty throw error
					 
					//std::string temp = filenames.Base_File+"/../../";
					
					std::string temp = speech->reference;
        					
					filenames.Wav_File =  temp.replace(temp.length()-4,4,".wav");

				}	
				//if(filenames.Wav_File=="") 
				//{

				//	std::string aaa = speech->reference;
				//	filenames.Wav_File = aaa.replace( aaa.find_first_of(".xml") , 4 ,".wav" );
				//
				//}	
		
				//if it is realtime read this file and put it into char
		
				if(isrealtime)
				{
					//initial size of the speech is 0				
					speech_size=0;

					FILE * pFile;
					char c;

					//std::string temp = "c:\\code\\greta\\bin\\" ;
					std::string uutemp = filenames.Wav_File;
					pFile=fopen ( uutemp.c_str() ,"rb");


					fseek(pFile, 0, SEEK_END); // seek to end of file
					int file_size = ftell(pFile); // get current file pointer
					fseek(pFile, 0, SEEK_SET);

					//printf("The file size in bytes is %ld\n", file_size);					

					if (pFile==NULL) printf("no file");
					else
					{							
						content_of_speech = (char*) malloc ( file_size*sizeof(char));
						for (int i=0;i<file_size;i++)
						{							
							c = getc (pFile);	

							//content_of_speech = (char*) realloc (content_of_speech, speech_size* sizeof(char));
							content_of_speech[i]=c;			
						}

						fclose (pFile);

					}

					content_of_speech = (char*) realloc (content_of_speech, (file_size+2) * sizeof(char));

					content_of_speech[++file_size]=EOF;

					speech_size=file_size;

					
					if(content_of_speech==0)
					{
						speech_size=0;
						delete t;
						return 0;
					}
				}
				// else we are in offline version; the file will be read when neeeded

			}//end of usingMary=3
			
			
			//only the wav file no lips movement!!!
			if(speech->usingMary==4)
			{
								
				//if(filenames.Wav_File=="") 
				//{
					//if speech-> reference empty throw error
					 
					//std::string temp = filenames.Base_File+"/../../";
					
					std::string temp = speech->reference;
        					
					filenames.Wav_File =  temp.replace(temp.length()-4,4,".wav");

				//}	
				
				//if it is realtime read this file and put it into char
				if(isrealtime)
				{
					//initial size of the speech is 0				
					speech_size=0;

					FILE * pFile;
					char c;

					std::string path = inimanager.Program_Path;
					std::string filetemp = filenames.Wav_File;
					filetemp = path+filetemp; 
					//printf("wav file %s", filetemp.c_str());
					pFile=fopen ( filetemp.c_str() ,"rb");

					fseek(pFile, 0, SEEK_END); // seek to end of file
					int file_size = ftell(pFile); // get current file pointer
					fseek(pFile, 0, SEEK_SET);

					//printf("The file size in bytes is %ld\n", file_size);					

					if (pFile==NULL) printf("no file");
					
					else
					{							
						content_of_speech = (char*) malloc ( file_size*sizeof(char));
						for (int i=0;i<file_size;i++)
						{							
							c = getc (pFile);	

							//content_of_speech = (char*) realloc (content_of_speech, speech_size* sizeof(char));
							content_of_speech[i]=c;			
						}

						fclose (pFile);

					}

					content_of_speech = (char*) realloc (content_of_speech, (file_size+2) * sizeof(char));

					content_of_speech[++file_size]=EOF;

					speech_size=file_size;

			
				if(content_of_speech==0)
					{
						speech_size=0;
						delete t;
						return 0;
					}
				}
				
				// else we are in offline version; the file will be read when neeeded

				//very importnant: NO PHONEMS FILE!!!
				filenames.Phonemes_File="";

			}//end of usingMary==4

		}
	}

	
	//std::cout <<" content_of_speech = " <<content_of_speech<<" time =" << speech->start <<":"<<speech->duration_sym <<std::endl;

	
	for(itersig=signals.begin();itersig!=signals.end();itersig++)
	{
		ln=0;		
		(*itersig).Temporize(signals, speech, &ln);

		/*
		std::cout<< (*itersig).id <<" : "; // A unique id for the signal.
		std::cout << (*itersig).modality << " : " ; //The modality on which the signal should be emitted.
		std::cout<< (*itersig).type << " : "; //The type of the signal (Depending on the modality on which the signal is produced this parameter will contain different things. For example for the gesture modality the type indicates if the gesture is a deictic, iconic, etc. For the face is inicates which part of the face is used to perform a facial expression.)
		std::cout<< (*itersig).shape << " : ";//Specific for the gesture modality.
		std::cout<< (*itersig).posture<<" : ";//Specific for the torso modality.
		std::cout<<(*itersig).reference<<std::endl;//This is an external reference to a repository containing the description of the signal.
		std::cout<<(*itersig).start<<" : "; //start time (absolute)
		std::cout<<(*itersig).duration<<std::endl;//Duration of the signal, relative to the start time.
		std::cout<<(*itersig).BMLcode<<std::endl;//corresponding bml code
		*/
	}
	


	delete p;
	delete t;	
	return 1;
}

bool BML_NaoEngine::hasSpeech()
{
	std::vector<MMSystemSpace::Signal>::iterator itersig;
	for(itersig=signals.begin();itersig!=signals.end();itersig++)
	{
		if((*itersig).modality=="speech")
		{
			return true;
		}
	}
	return false;
}
