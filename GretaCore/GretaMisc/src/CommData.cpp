

#include "CommData.h"

CommData::CommData()
{
	size=0;
	data="";
}

void CommData::setData(char *Data)
{
	data=Data;
}

void CommData::setSize(long Size)
{
	size=Size;
}

char* CommData::getData()
{
	return data;
}

long CommData::getSize()
{
	return size;
}

