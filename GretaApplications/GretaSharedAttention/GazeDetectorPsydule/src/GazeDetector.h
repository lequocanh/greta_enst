// Shared-Attention system
// Christopher Peters

// Wrapper for Stelios' (NTUA) gaze detection library into C++ class

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>

#include "cv.h"
#include "highgui.h"
#include "cvcam.h"

#include "eye.h"

struct FrameResults
{
	float px;
	float py;
	float ppy;
	float ppx;
	float dist_monitor;
	bool valid;
};

class SteliosGazeDetector
{
	public:

		SteliosGazeDetector(void);
		~SteliosGazeDetector(void);

		void initialiseNewCapture(bool _inputFromAVI = false, bool _showOutput = true, bool _writeToAVI = false, bool _saveToFile = false);
		void finishCapture(void);

		FrameResults frameResults;

		const FrameResults& processNewFrame(void);
	
	private:

		IplImage* frame_orig;
		IplImage* frame;
		IplImage* frame_out;
		CvCapture* capture;
		CvMemStorage* storage;
		CvVideoWriter *AVIwriter1;
			
		int NO_POINTS;
		//int faces;
		
		int frameCount;
		int fr_c;

		double scale;

		bool initialised;
		char *workingPath;
		char video_name[100];

		bool showOutput;	//show the output frames in an OpenCV window
		bool inputFromAVI;	//input from an AVI file or a camera
		bool writeToAVI;	//write the output frames to an avi file
		bool saveToFile;	//save the frame results to a txt file

		FILE *stream_out;
		char *file_type;

		float *px_m;
		float* py_m;
		float* ppx_m;
		float* ppy_m;
		int interval;
		int fps;

		double** init_points;
		double** points;
		double** fin_points;
		unsigned char **inputGrey;
		unsigned char **inputGreyprev;
		int count;

		int xl_or; 
		int yl_or; 
		int xr_or;
		int yr_or;
		int flag;
		float pxfirstL;
		float pxfirstR;
		float pyfirstL;
		float pyfirstR;
};