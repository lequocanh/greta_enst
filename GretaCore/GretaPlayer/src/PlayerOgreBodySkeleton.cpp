#include "PlayerOgreBodySkeleton.h"
#include "BAPConverter.h"


PlayerOgreBodySkeleton::PlayerOgreBodySkeleton(Ogre::Skeleton * skel){
	BAPConverter::init();
	bodySkeleton = skel;
	numBones = bodySkeleton->getNumBones();
	values = (double**) malloc(sizeof(double*)*bodySkeleton->getNumBones());
	mask = (bool*) malloc(sizeof(bool)*bodySkeleton->getNumBones());
	originalOrientation = (Ogre::Quaternion*) malloc(sizeof(Ogre::Quaternion)*bodySkeleton->getNumBones());
	originalderivedOrientation = (Ogre::Quaternion*) malloc(sizeof(Ogre::Quaternion)*bodySkeleton->getNumBones());
	for(int i=0;i<numBones;i++){
		mask[i] = false;
		values[i] = (double*) malloc(sizeof(double)*3);
		for(int j=0;j<3;j++)
			values[i][j] = 0;

		originalOrientation[i] = bodySkeleton->getBone(i)->getOrientation();
		if(bodySkeleton->getBone(i)->getParent() != NULL){
			originalderivedOrientation[i] = bodySkeleton->getBone(i)->getParent()->_getDerivedOrientation();
		}
		else{
			originalderivedOrientation[i] = Ogre::Quaternion();
		}
	}
	for(int i=0;i<numBones;i++){
		//initialization of the mapping
		int BAPofXRotation = BAPConverter::MJointToBAP(bodySkeleton->getBone(i)->getName() + ".rotateX");
		int BAPofYRotation = BAPConverter::MJointToBAP(bodySkeleton->getBone(i)->getName() + ".rotateY");
		int BAPofZRotation = BAPConverter::MJointToBAP(bodySkeleton->getBone(i)->getName() + ".rotateZ");
		BAPtoBONE[BAPofXRotation][0] = i;
		BAPtoBONE[BAPofXRotation][1] = 0;
		BAPtoBONE[BAPofYRotation][0] = i;
		BAPtoBONE[BAPofYRotation][1] = 1;
		BAPtoBONE[BAPofZRotation][0] = i;
		BAPtoBONE[BAPofZRotation][1] = 2;

		//to control the skeleton
		bodySkeleton->getBone(i)->setManuallyControlled(true);
	}
}

void PlayerOgreBodySkeleton::ApplyBAPFrame(BAPFrame * bf){
	for(unsigned int i=0;i<bf->mBAPVector.size();i++){
		if(bf->mBAPVector[i].GetMask()){
			values[BAPtoBONE[i][0]][BAPtoBONE[i][1]] = bf->mBAPVector[i].GetAngleValue();
			mask[BAPtoBONE[i][0]] = true;
		}
	}
	applyValues();
}

void PlayerOgreBodySkeleton::Reset(){
	for(int i=0;i<numBones;i++){
		mask[i] = false;
		for(int j=0;j<3;j++){
			mask[i] = mask[i] || values[i][j]!=0;
			values[i][j] = 0;
		}
	}
	applyValues();
}

void PlayerOgreBodySkeleton::applyValues(){
	for(int i=0; i<numBones; i++){
		if(mask[i]){
			Ogre::Quaternion q = 
				Ogre::Quaternion(Ogre::Radian(values[i][2]),Ogre::Vector3(0,0,1)) *
				Ogre::Quaternion(Ogre::Radian(values[i][1]),Ogre::Vector3(0,1,0)) * 
				Ogre::Quaternion(Ogre::Radian(values[i][0]),Ogre::Vector3(1,0,0));
			q = originalderivedOrientation[i].Inverse() * q * originalderivedOrientation[i] * originalOrientation[i];
			bodySkeleton->getBone(i)->setOrientation(q);
			mask[i] = false;
			
		}
	}
	bodySkeleton->getBone(0)->_update(true,false);
}