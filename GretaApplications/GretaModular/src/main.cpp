//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "APML_AgentEngine.h"
#include "DataContainer.h"
#include "IniManager.h"
#include "GretaParametersWindow.h"
#include "XercesTool.h"

#ifdef USE_ACAPELA
	#include "AcapelaPlug.h"
	#include <stdio.h>
#endif

#include <FL/Fl.H>
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;


#pragma warning( disable : 4100 )
#pragma warning( disable : 4311 )

DataContainer *datacontainer;
IniManager inimanager;
std::string ini_filename,directory;

FILE *all_logfile;

#ifdef NDEBUG
#include <shellapi.h>
bool cvtLPW2stdstring(std::string& s, const LPWSTR pw, UINT codepage = CP_ACP){
	bool res = false;
	char* p = 0;
	int bsz;
	bsz = WideCharToMultiByte(codepage, 0, pw,-1,0,0,0,0);
	if (bsz > 0) {
		p = new char[bsz];
		int rc = WideCharToMultiByte(codepage,0,pw,-1,p,bsz,0,0);
		if (rc != 0) {
			p[bsz-1] = 0;
			s = p;
			res = true;
		}
	}
	delete [] p;
	return res;
}

int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInstance,
     LPSTR     lpszCmdParm,
     int       nCmdShow
)
{
	LPWSTR *szArglist;
	int nArgs;
	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if (nArgs==2){
		cvtLPW2stdstring(ini_filename,szArglist[1]);
		directory="";
	}
	else{
		if(nArgs==3){
			cvtLPW2stdstring(ini_filename,szArglist[1]);
			cvtLPW2stdstring(directory,szArglist[2]);
		}

#else
int main (int argc, char *argv[]){
	if (argc==2){
		ini_filename=argv[1];
		directory="";
	}
	else{
		if (argc==3){
			ini_filename=argv[1];
			directory=argv[2];
		} 
#endif
		else {
			ini_filename="greta.ini";
			directory="";
		}
	}
	all_logfile=fopen("logs/GretaModular.txt","w");

	XercesTool::startupXMLTools();

	//datacontainer needs some data..
	inimanager.ReadIniFile(ini_filename);
	
	datacontainer = new DataContainer();

	#ifdef USE_ACAPELA
		AcapelaPlug::initAcapela();
	#endif

	//load faces, gestures etc
	
	int code=datacontainer->initAll(directory);
	
	if (code==0) {
		printf("Press any key to exit \n");
		float top;
		scanf ("%f", &top); 
		printf("Out.. \n");
		exit(1);
	}

	
	/*
	GlutMaster gm;
	Player *player;
	player=new Player(&gm,false,false,0);
	while(!player->player_finished)
	{
	glutCheckLoop();
	}
	APML_AgentEngine AAE;
	//AAE.CalculateTurn("input/business.xml");
	*/

	GretaParametersWindow *window;
	window = new GretaParametersWindow();

	//window->set_modal();

	window->show();

	Fl::run();

	XercesTool::shutdownXMLTools();

	fclose(all_logfile);

	delete window;
}