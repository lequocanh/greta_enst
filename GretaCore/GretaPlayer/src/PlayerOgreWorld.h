#ifndef PLAYEROGREWORLD_H
#define PLAYEROGREWORLD_H

#include <ogre.h>
#include "PlayerOgreAgent.h"
#include "PlayerOgreSetOfCharacter.h"
#include "XMLGenericTree.h"

class PlayerOgreWorld{
public:
	PlayerOgreWorld();
	~PlayerOgreWorld();
	void initWorld();
	void initAgents(bool realtime);
	void update();
	Ogre::Root* getRoot(){ return ogreRoot; }
	Ogre::SceneManager* getSceneManager(){ return ogreSceneManager; }
	PlayerOgreAgent* getAgent(){ return agent; }
	PlayerOgreAgent* getListener(){ return listener; }
	//void setSky(){ogreSceneManager->setSkyDome(true, "CloudySky", 5, 3,2000);}
	void setScene(std::string sceneName);

	void setCharacterSpeaker(std::string characterName);
	void setCharacterListener(std::string characterName);

	void setBackgroundColour(double r, double g, double b){background->r=r;background->g=g;background->b=b;}
	Ogre::ColourValue *getBackgroundColour(){return background;}

	PlayerOgreCamera* getCamera(std::string characterType, std::string cameraType);
private:
    Ogre::Root					*ogreRoot;
    Ogre::SceneManager			*ogreSceneManager;
	Ogre::SceneNode				*worldRootNode;
	Ogre::ColourValue			*background;
	PlayerOgreAgent				*agent;
	PlayerOgreAgent				*listener;
	PlayerOgreCamera			*freeCamera;
	PlayerOgreSetOfCharacter	*agentList;
	void setCharacter(PlayerOgreAgent** theAgent, std::string characterName,std::string type="");
	static void setObjectSetting(Ogre::SceneNode* objectNode, XMLGenericTree* position, XMLGenericTree* orientation, XMLGenericTree* scale);
//TODO
public:
	void LoadWorld(std::string filename){}
};
#endif


