//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include "RealtimePlayerWindow.h"
#include "IniManager.h"
#include "RandomGen.h"
#include "FL/Fl.H"
#include "FileNames.h"
#include "XercesTool.h"
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;

FILE *all_logfile;

//#include <windows.h>
//#include <stdio.h>
//#include <shellapi.h>

FILE *agent_log;
FileNames filenames;
IniManager inimanager;
RandomGen *randomgen;
std::string ini_filename;
 


bool cvtLPW2stdstring(std::string& s, const LPWSTR pw, UINT codepage = CP_ACP)

{

bool res = false;

char* p = 0;

int bsz;

bsz = WideCharToMultiByte(codepage, 0, pw,-1,0,0,0,0);

if (bsz > 0) {

p = new char[bsz];

int rc = WideCharToMultiByte(codepage,0,pw,-1,p,bsz,0,0);

if (rc != 0) {

p[bsz-1] = 0;

s = p;

res = true;

}

}

delete [] p;

return res;

}

int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInstance,
     LPSTR     lpszCmdParm,
     int       nCmdShow
)
{
   LPWSTR *szArglist;
   int nArgs;

   std::string ini_filename="";

   szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	   		   		 
	if (nArgs==2)
	{
		cvtLPW2stdstring(ini_filename,szArglist[1]);
	
	} else {
	
		ini_filename="greta_psyclone.ini";
	}
	 
	printf(" I load s% : ",ini_filename);

	all_logfile=fopen("logs/Psyclone_Player.txt","w");
	
	XercesTool::startupXMLTools();

	inimanager.ReadIniFile(ini_filename);
	
	randomgen=new RandomGen();
	
	RealtimePlayerWindow rpw;
	
	rpw.show();
	
	//try {
		Fl::run();
		_exit(0);
	   XercesTool::shutdownXMLTools();
	   fclose(all_logfile);
	
	//} catch(){}
}
