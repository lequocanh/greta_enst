/*
 *  ErrorManager.h
 *  semaine
 *
 *  Created by Marc Schröder on 03.11.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#ifndef ERRORMANAGER_H
#define ERRORMANAGER_H

#include "MessageFormatException.h"
#include "SystemConfigurationException.h"

#include <list>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>

#include <xercesc/sax2/DefaultHandler.hpp>


#if defined(XERCES_NEW_IOSTREAMS)
  #include <iostream>
#else
  #include <iostream.h>
#endif


class ErrorManager : public XERCES_CPP_NAMESPACE::DefaultHandler
{
public:
	ErrorManager(void);
	virtual ~ErrorManager(void);
	void warning(const XERCES_CPP_NAMESPACE::SAXParseException &exc);
	void error(const XERCES_CPP_NAMESPACE::SAXParseException &exc);
	void fatalError(const XERCES_CPP_NAMESPACE::SAXParseException &exc);

private:

	
};

#endif

