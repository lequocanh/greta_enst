#include "NaoMotion.h"

/**********************************************/
NaoMotion::NaoMotion()
{
	parentBrokerIP="127.0.0.1";
	parentBrokerPort=9559;
}

/**********************************************/
void NaoMotion::SetIP(std::string ip)
{
	parentBrokerIP=ip;
}

/**********************************************/
void NaoMotion::Setport(int port)
{
	parentBrokerPort=port;
}

/**********************************************/
NaoMotion::NaoMotion(std::string IP, int Port)
{
	parentBrokerIP = IP;
	parentBrokerPort = Port;
}

/**********************************************/
void NaoMotion::Init()
{
	try
	{
		proxy  = new AL::ALProxy("ALMotion", parentBrokerIP, parentBrokerPort);
		std::cout << "Loading MotionProxy is successful from the IP " << parentBrokerIP << " and the Port " << parentBrokerPort << endl;
	// This will throw an exception
	}
	catch( AL::ALError &err )
	{
		std::cout << err.toString() << std::endl;
	}

	// initialize variables
	float PositionRad[] = 
	{0, 0, 
	3.14*70/180, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 
	3.14*70/180, 0, 0, 0, 0, 0};

	int size = sizeof( PositionRad ) / sizeof( PositionRad[0] );
	vector<float> PositionVector(PositionRad, &PositionRad[size]);

	ZeroPosition=PositionVector;

	// 
		// Feel free to experiment with these values
	float kneeAngle  = 40;
	float torsoAngle =  0;
	float wideAngle  =  0;
	std::string pNames = "Body";
	//----------- prepare the angles -----------
	//Get the Number of Joints	
	vector<string> jointNames = proxy->call<vector<string>>("getJointNames",pNames);
	int NumJoints = jointNames.size();

	// Define The Initial Position
	float initPosition22[] = 
	{0, 0, // Head
    120,  15, -90, -80, // LeftArm
    0,  wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2, -wideAngle, // LeftLeg
	0, -wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2,  wideAngle, // RightLeg
	120, -15,  90,  80}; // RightArm

	// If we have hands, we need to add angles for wrist and open/close hand
	float initPosition26[] = 
	{0, 0, // Head
    120,  15, -90, -80, 0, 0, // LeftArm
    0,  wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2, -wideAngle, // LeftLeg
	0, -wideAngle, -kneeAngle/2-torsoAngle, kneeAngle, -kneeAngle/2,  wideAngle, // RightLeg
	120, -15,  90,  80, 0, 0}; // RightArm

	// Convert to radians in the case of 26
		
	vector<float> initPos(26,0.0);
	if(NumJoints>22)
	{
		for(int i=0;i<NumJoints;i++) 
		{
			initPos[i] = (float)(initPosition26[i]*3.141)/180 ;	
		}
	}
	else
	{
		std::cout << "This version of Nao is not compatible !" << endl;
		exit(0);
	}
	InitPosition = initPos;
}

/**********************************************/
void NaoMotion::StiffnessOn()
{
	//We use the "Body" name to signify the collection of all joints
	std::string pNames = "Body";
	float pStiffnessLists = 1.0;
	float pTimeLists = 1.0;
	proxy->callVoid("stiffnessInterpolation", pNames, pStiffnessLists, pTimeLists);
}

/**********************************************/
void NaoMotion::StiffnessOff()
{
	//We use the "Body" name to signify the collection of all joints
	std::string pNames = "Body";
	float pStiffnessLists = 0.0;
	float pTimeLists = 1.0;
	proxy->callVoid("stiffnessInterpolation", pNames, pStiffnessLists, pTimeLists);
}

/**********************************************/
void NaoMotion::PoseInit()
{
	//----------- send the commands --------------
	std::string pNames = "Body";
	//We set the fraction of max speed
	float pMaxSpeedFraction = 0.5;
	//Ask motion to do this with a blocking call	
	proxy->pCall("angleInterpolationWithSpeed",pNames, InitPosition, pMaxSpeedFraction);	
}

/**********************************************/
void NaoMotion::OpenHand()
{
		//Get the Number of Joints	
	vector<string> jointNames = proxy->call<vector<string>>("getJointNames",string("body"));
	int NumJoints = jointNames.size();
	if(NumJoints>22)
	{
		// put stiffness on LHand
		std::string pNames = "LHand";		
		// method 1		    
			float pMaxSpeedFraction = 0.5;
			proxy->callVoid("stiffnessInterpolation",pNames, 1.0, 1.0);
			proxy->callVoid("angleInterpolationWithSpeed",pNames, 0.5, pMaxSpeedFraction);
			proxy->callVoid("angleInterpolationWithSpeed",pNames, 0.0, pMaxSpeedFraction);
			SleepMs(500); // wait half a second

		// method 2
			proxy->callVoid("openHand",pNames);
			proxy->callVoid("closeHand",pNames);
	}
}

/**********************************************/
void NaoMotion::PoseZero()
{
	//----------- send the commands --------------
	std::string pNames = "Body";
	//We set the fraction of max speed
	float pMaxSpeedFraction = 0.2;
	//Ask motion to do this with a blocking call
	proxy->callVoid("angleInterpolationWithSpeed",pNames, ZeroPosition, pMaxSpeedFraction);	
}

/**********************************************/
void NaoMotion::make_motion_left_arm(vector<float> lVec6, float *lHand)
{
	proxy->pCall("positionInterpolation",string("LArm"),SPACE,lVec6,MASK,SECONDS,true);

	proxy->pCall("changeAngles",string("LHand"),*lHand,0.5);

}

/**********************************************/
void NaoMotion::make_motion_right_arm(vector<float> rVec6, float *rHand)
{
	proxy->pCall("positionInterpolation",string("RArm"),SPACE,rVec6,MASK,SECONDS,true);

	proxy->pCall("changeAngles",string("RHand"),*rHand,0.5); 
}

/**********************************************/
NaoMotion::~NaoMotion(void)
{
}
