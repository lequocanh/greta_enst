//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include "RealtimeBML.h"
#include "IniManager.h"
#include "DataContainer.h"
#include "XercesTool.h"
#include "GretaLogger.h"
std::list<GretaLogger*> listLog;

FILE *all_logfile;

DataContainer *datacontainer;
IniManager inimanager;
std::string ini_filename;

int main(int argc, char ** argv)
{
	
	std::string GretaName;
	std::string host;
	int port;
	
	if(argc==2)
	{
		ini_filename=argv[1];
	} else {
		ini_filename="greta_psyclone.ini";	
	}

	all_logfile=fopen("logs/Psyclone_BehaviorRealizer_log.txt","w");

	inimanager.ReadIniFile(ini_filename);
	datacontainer = new DataContainer();

	XercesTool::startupXMLTools();	
	
	int code=datacontainer->initBMLEngine();
	if (code==0) {
		printf("Problem : out \n");
		fclose(all_logfile);
		exit(1);
	}

	host= inimanager.GetValueString("PSYCLONE_HOST").c_str();
	port= inimanager.GetValueInt("PSYCLONE_PORT");
	GretaName= inimanager.GetValueString("GRETA_NAME").c_str();
	

	RealtimeBML bmlengine(GretaName+REALTIMEBML_MODULE_NAME, host, port, GretaName);

	bmlengine.MainLoop();

	XercesTool::shutdownXMLTools();

	fclose(all_logfile);
	return 1;
}
