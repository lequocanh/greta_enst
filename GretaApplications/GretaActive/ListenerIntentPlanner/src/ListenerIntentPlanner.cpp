//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ListenerIntentPlanner.cpp: implementation of the ListenerIntentPlanner class. 
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#include "ListenerIntentPlanner.h"

#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/message/SEMAINEStateMessage.h>
#include <semaine/datatypes/stateinfo/UserStateInfo.h>
#include "GretaLogger.h"

extern std::list<GretaLogger*> listLog;


extern DataContainer *datacontainer;
extern IniManager inimanager;
extern CentralClock *pc;


ListenerIntentPlanner::ListenerIntentPlanner(const std::string & componentName, bool isInput, bool isOutput) throw(CMSException) :
	Component(componentName,isInput,isOutput)
{
	dialogstateReceiver = new Receiver("semaine.data.state.dialog"); 
	contextReceiver = new StateReceiver("semaine.data.state.context", semaine::datatypes::stateinfo::StateInfo::Type::ContextState); 
	receivers.push_back(contextReceiver);
	USReceiver = new StateReceiver("semaine.data.state.user.behaviour", semaine::datatypes::stateinfo::StateInfo::Type::UserState);
	receivers.push_back(USReceiver);
	agentStateReceiver = new StateReceiver("semaine.data.state.agent", semaine::datatypes::stateinfo::StateInfo::Type::AgentState);
	receivers.push_back(agentStateReceiver);
	fmlSender = new FMLSender("semaine.data.action.candidate.function", getName());
	senders.push_back(fmlSender);
	bmlSender = new BMLSender("semaine.data.action.candidate.behaviour", getName());
	senders.push_back(bmlSender);

	this->start();
}

	
ListenerIntentPlanner::~ListenerIntentPlanner()
{
	delete dialogstateReceiver;
	delete contextReceiver;
	delete agentStateReceiver;
	delete fmlSender;
	delete bmlSender;
}

void ListenerIntentPlanner::customStartIO() throw(std::exception) 
{
	start_speaking=-1; 
	stop_speaking=-1;

	userpresent=false;

	listenerdata=datacontainer->getListenerData();

	agentstate=new AgentState(listenerdata->agentstate);
	behaviourselector=new BehaviourSelector(this, pc, agentstate);
	behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
	behaviourselector->start();


	reactivebehaviourtrigger=new ReactiveBehaviourTrigger(agentstate);

}

void ListenerIntentPlanner::react(SEMAINEMessage * m) throw (CMSException)
{
	if (m == NULL) {
		throw MessageFormatException("expected XML message, got "+std::string(typeid(*m).name()));
	}

	SEMAINEStateMessage *sm = (SEMAINEStateMessage *)(m);
	if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*m).name());
	semaine::datatypes::stateinfo::StateInfo * state = sm->getState();

	if(state==NULL) return;

	std::string type = m->getDatatype();
	
	pc->SetTime(meta.getTime());

	bool isUserState = type == "UserState";
	bool isAgentState = type == "AgentState";
	bool isContextState = type == "ContextState";

	if(isUserState) 
	{ 
		if(state!=NULL && agentstate->getAgentTurn()==0 && userpresent==true)
		{
			//info about user's emotional state
			if (state->hasInfo("valence"))
			{
				std::string user_valence = state->getInfo("valence");
				//std::cout << "valence: " << user_valence << "\n";
			}
			if (state->hasInfo("arousal"))
			{
				std::string user_arousal = state->getInfo("arousal");
				//std::cout << "arousal: " << user_arousal << "\n";
			}
			if (state->hasInfo("potency"))
			{
				std::string user_potency = state->getInfo("potency");
				//std::cout << "potency: " << user_potency << "\n";
			}
			if (state->hasInfo("category"))
			{
				std::string user_category = state->getInfo("category");
				//std::cout << "category: " << user_category << "\n";
			}


			//info about user's events
			if (state->hasInfo("pitch"))
			{
				std::string pitch = state->getInfo("pitch");
				//std::cout << pitch;
				pitch= "speech:" + pitch + ";";
				reactivebehaviourtrigger->addMessage(pitch);
			}

			if (state->hasInfo("speaking"))
			{
				std::string speaking = state->getInfo("speaking");
				if(speaking=="true") {
					start_speaking=clock();
				}
				else 
				{
					stop_speaking=clock();
					start_speaking=-1;
				}

				if((start_speaking!=-1 && stop_speaking!=-1) && ((start_speaking-stop_speaking)/CLOCKS_PER_SEC)>0.6)
				{
					reactivebehaviourtrigger->addMessage("speech:silence;");
					stop_speaking=-1;
				}
			}

			if (state->hasInfo("headGesture"))
			{
				std::string headGesture = state->getInfo("headGesture");
				if(headGesture=="NOD") reactivebehaviourtrigger->addMessage("head:head_nod;");
				else 
					if(headGesture=="SHAKE") reactivebehaviourtrigger->addMessage("head:head_shake;");
			}
		}
	} 

	if(isAgentState)
	{ 

		if (state->hasInfo("listenerAgreement"))
		{
			std::string agreement = state->getInfo("listenerAgreement");
			agentstate->internalstate.SetAttrValue("listener_function.agreement.value", agreement);
		}

		if (state->hasInfo("listenerDisagreement"))
		{
			std::string disagreement = state->getInfo("listenerDisagreement");
			agentstate->internalstate.SetAttrValue("listener_function.disagreement.value", disagreement);
		}


		if (state->hasInfo("listenerAcceptance"))
		{
			std::string acceptance = state->getInfo("listenerAcceptance");
			agentstate->internalstate.SetAttrValue("listener_function.acceptance.value", acceptance);
		}

		if (state->hasInfo("listenerRefusal"))
		{
			std::string refusal = state->getInfo("listenerRefusal");
			agentstate->internalstate.SetAttrValue("listener_function.refusal.value", refusal);
		}


		if (state->hasInfo("listenerBelief"))
		{
			std::string belief = state->getInfo("listenerBelief");
			agentstate->internalstate.SetAttrValue("listener_function.belief.value", belief);
		}

		if (state->hasInfo("listenerDisbelief"))
		{
			std::string disbelief = state->getInfo("listenerDisbelief");
			agentstate->internalstate.SetAttrValue("listener_function.disbelief.value", disbelief);
		}


		if (state->hasInfo("listenerLiking"))
		{
			std::string liking = state->getInfo("listenerLiking");
			agentstate->internalstate.SetAttrValue("listener_function.liking.value", liking);
		}

		if (state->hasInfo("listenerDisliking"))
		{
			std::string disliking = state->getInfo("listenerDisliking");
			agentstate->internalstate.SetAttrValue("listener_function.disliking.value", disliking);
		}


		if (state->hasInfo("listenerUnderstanding"))
		{
			std::string understanding = state->getInfo("listenerUnderstanding");
			agentstate->internalstate.SetAttrValue("listener_function.understanding.value", understanding);
		}

		if (state->hasInfo("listenerNoUnderstanding"))
		{
			std::string no_understanding = state->getInfo("listenerNoUnderstanding");
			agentstate->internalstate.SetAttrValue("listener_function.no_understanding.value", no_understanding);
		}


		if (state->hasInfo("listenerInterest"))
		{
			std::string interest = state->getInfo("listenerInterest");
			agentstate->internalstate.SetAttrValue("listener_function.interest.value", interest);
		}

		if (state->hasInfo("listenerNoInterest"))
		{
			std::string no_interest = state->getInfo("listenerNoInterest");
			agentstate->internalstate.SetAttrValue("listener_function.no_interest.value", no_interest);
		}
	}

	if (isContextState) 
	{
		if (state->hasInfo("userPresent"))
		{
			std::string present = state->getInfo("userPresent");
			if(present=="absent")
				userpresent=false;
			else
				userpresent=true;
		}
		if (state->hasInfo("character"))
		{
			std::string chname = state->getInfo("character");
			inimanager.SetValueString("CHARACTER_SPEAKER",chname);
			inimanager.ToUppercase(chname);
			if(chname=="PRUDENCE") inimanager.SetValueString("MARY_VOICE","dfki-prudence");
			else if(chname=="POPPY") inimanager.SetValueString("MARY_VOICE","dfki-poppy");
			else if(chname=="SPIKE") inimanager.SetValueString("MARY_VOICE","dfki-spike");
			else if(chname=="OBADIAH") inimanager.SetValueString("MARY_VOICE","dfki-obadiah");
			agentstate->setAgentName(chname);
			chname="listener/" + chname +".xml";
			if(listenerdata->LoadAgentState(chname)!=0)
				agentstate->internalstate.LoadFromBuffer(listenerdata->agentstate);
		}
	}

	//code for log informations
	std::list<GretaLogger*>::iterator iterlog;

	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			log->debug(msg);
		if((*iterlog)->getLevel()=="info")
			log->info(msg);
		if((*iterlog)->getLevel()=="warn")
			log->warn(msg);
		if((*iterlog)->getLevel()=="error")
			log->error(msg);
	}
	listLog.clear();
}

void ListenerIntentPlanner::act() throw (CMSException)
{
	std::map<std::string,DataBackchannel*> bmlfml;

	if(reactivebehaviourtrigger->messageList.size()>0)
		bmlfml=reactivebehaviourtrigger->ReceiveMessages();


	if(bmlfml["bml"]!=NULL)
		behaviourselector->updateBackchannelRequest(bmlfml["bml"]);

	if(bmlfml["fml"]!=NULL)
		behaviourselector->updateBackchannelRequest(bmlfml["fml"]);

}
