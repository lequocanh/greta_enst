//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// PlayerFLTKWindow.cpp: implementation of the PlayerFLTKWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "PlayerFLTKWindow.h"
#include <stdio.h>
#include <string.h>
#include <list>
#include <string>
#include <fl/fl_widget.h>
#include <FL/Fl_File_Chooser.H>
#include <FL/Fl_Draw.h>

#include "IniManager.h"

extern IniManager inimanager;

	static LPRECT rRegion;
	static    HDIB     hDib; 
	static RECT captRegion; 

void selectedmenu(Fl_Widget* w, void* v)
{

	if(strcmp(((PlayerFLTKWindow*)v)->menu->text(),"about")==0)
	{
	}
	if(strcmp(((PlayerFLTKWindow*)v)->menu->text(),"quit")==0)
	{
		((PlayerFLTKWindow*)v)->hide();
		((PlayerFLTKWindow*)v)->glw->RemoveIdle();
	}
	if(strcmp(((PlayerFLTKWindow*)v)->menu->text(),"load BAP-FAP file")==0)
	{
		Fl_File_Chooser *chooser;
		chooser= new Fl_File_Chooser((inimanager.Program_Path+"output").c_str(),"BAP-FAP files (*.*ap)",FL_SINGLE,"select a BAP or FAP file");
		chooser->show();
		while (chooser->shown())
			Fl::wait();
		std::string s;
		s="";
		if(chooser->value()!=0)
			s=chooser->value();
		if(s!="")
		{
			s=s.substr(0,s.find_last_of("."));
			((PlayerFLTKWindow*)v)->glw->getAgent()->AssignFile((char*)s.c_str());
			if(((PlayerFLTKWindow*)v)->glw->getListener()!=0)
				((PlayerFLTKWindow*)v)->glw->getListener()->AssignFile((char*)(s+"_listener").c_str());
		}
		delete chooser;
	}
	if(strcmp(((PlayerFLTKWindow*)v)->menu->text(),"refresh file")==0)
	{
		((PlayerFLTKWindow*)v)->glw->getAgent()->StopTalking();
		((PlayerFLTKWindow*)v)->glw->getAgent()->SetCurrentFrame(0);
		((PlayerFLTKWindow*)v)->glw->getAgent()->ReloadFile();
	}
	if(strcmp(((PlayerFLTKWindow*)v)->menu->text(),"load environment")==0)
	{
		Fl_File_Chooser *chooser;
		chooser= new Fl_File_Chooser((inimanager.Program_Path+"environments").c_str(),"environment files (*.xml)",FL_SINGLE,"select an environment file");
		chooser->show();
		while (chooser->shown())
			Fl::wait();
		std::string s;
		s="";
		if(chooser->value()!=0)
			s=chooser->value();
		if(s!="")
		{
			((PlayerFLTKWindow*)v)->glw->getWorld()->LoadWorld(s);
			//((PlayerFLTKWindow*)v)->glw2->world->LoadWorld(s);
		}
		delete chooser;
	}
	if(strcmp(((PlayerFLTKWindow*)v)->menu->text(),"show faces")==0)
	{
#ifndef PLAYEROGRE
		if (((PlayerFLTKWindow*)v)->glw->listenerinsubwindow==1)
			((PlayerFLTKWindow*)v)->glw->listenerinsubwindow=0;
		else
			((PlayerFLTKWindow*)v)->glw->listenerinsubwindow=1;
#endif
	}
}

void selectedplay(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->StartTalking();
	if(((PlayerFLTKWindow*)v)->glw->getListener()!=0)
		((PlayerFLTKWindow*)v)->glw->getListener()->StartTalking();
}

void selectedstop(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->StopTalking();
	if(((PlayerFLTKWindow*)v)->glw->getListener()!=0)
		((PlayerFLTKWindow*)v)->glw->getListener()->StopTalking();
	
}

void selectedframeback(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->StopTalking();
	((PlayerFLTKWindow*)v)->glw->getAgent()->OneFrameDown();
	if(((PlayerFLTKWindow*)v)->glw->getListener()!=0)
	{
		((PlayerFLTKWindow*)v)->glw->getListener()->StopTalking();
		((PlayerFLTKWindow*)v)->glw->getListener()->OneFrameDown();
	}
}

void selectedframenext(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->StopTalking();
	((PlayerFLTKWindow*)v)->glw->getAgent()->OneFrameUp();
	if(((PlayerFLTKWindow*)v)->glw->getListener()!=0)
	{
		((PlayerFLTKWindow*)v)->glw->getListener()->StopTalking();
		((PlayerFLTKWindow*)v)->glw->getListener()->OneFrameUp();
	}
}

void selectedrewind(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->StopTalking();
	((PlayerFLTKWindow*)v)->glw->getAgent()->SetCurrentFrame(0);
	if(((PlayerFLTKWindow*)v)->glw->getListener()!=0)
	{
		((PlayerFLTKWindow*)v)->glw->getListener()->StopTalking();
		((PlayerFLTKWindow*)v)->glw->getListener()->SetCurrentFrame(0);
	}
}

void selectedhair(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->SwitchHair();
}

void selectedskin(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->SwitchSkin();
}

void selectedifapu(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->SwitchIFAPU();
}

void selectedeyes(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->SwitchEyes();
}

void selectedskeleton(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->getAgent()->SwitchSkeleton();
}

void selectedstartcapture(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->StartCapture();
	/*
	if(!((PlayerFLTKWindow*)v)->capturing)
		((PlayerFLTKWindow*)v)->Capture();
	else
		((PlayerFLTKWindow*)v)->EndCapture();
		*/
}
void selectedendcapture(Fl_Widget* w, void* v)
{
	((PlayerFLTKWindow*)v)->glw->EndCapture();
	/*
	if(!((PlayerFLTKWindow*)v)->capturing)
		((PlayerFLTKWindow*)v)->Capture();
	else
		((PlayerFLTKWindow*)v)->EndCapture();
		*/
}

void gretaplayerwindowclosed(void* v)
{
	((PlayerFLTKWindow*)v)->hide();
	((PlayerFLTKWindow*)v)->glw->RemoveIdle();
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////




PlayerFLTKWindow::PlayerFLTKWindow(std::string filename):Fl_Double_Window(10,20,inimanager.GetValueInt("PLAYER_WIN_WIDTH")+200,inimanager.GetValueInt("PLAYER_WIN_HEIGHT")+70,"Greta player")
{
	Initialization();
#ifdef PLAYEROGRE
	glw=new PlayerOgreView(200,35,490,430,"test",filename);
#else
	glw=new PlayerFLTKGLWindow(200,35,490,430,"test",filename);
#endif
	glw->SetOutputControl(framenumber,framerate);
}

PlayerFLTKWindow::PlayerFLTKWindow():Fl_Double_Window(10,20,inimanager.GetValueInt("PLAYER_WIN_WIDTH")+205,inimanager.GetValueInt("PLAYER_WIN_HEIGHT")+70,"Greta player")
{
	Initialization();
#ifdef PLAYEROGRE
	glw=new PlayerOgreView(200,35,490,430,"test");
#else
	glw=new PlayerFLTKGLWindow(200,35,490,430,"test");
#endif
	glw->SetOutputControl(framenumber,framerate);
}

void PlayerFLTKWindow::Initialization()
{
	Fl::visual(FL_DOUBLE|FL_INDEX);

	int i;

	this->color(fl_rgb_color(204,204,204));
	this->size_range(440,450,0,0,1,1,0);
	//channels=new ChannelsImage(5,35,100,500-45,"");
	//glw2=new PlayerFLTKGLWindowView(20,250,150,120,"vision");
	//glw->glwview=glw2;
	
	menu=new PlayerFLTKMenu(0,0,this->w(),30);

	//disattivate : changed to "1" because of the bug
	menu->add("player/load environment",0,(Fl_Callback *)selectedmenu,this,1);
	//disattivate : changed to "1" because of the bug
	menu->add("player/show faces",0,(Fl_Callback *)selectedmenu,this,1);
	menu->add("player/quit",0,(Fl_Callback *)selectedmenu,this,0);
	menu->add("agent/load BAP-FAP file",0,(Fl_Callback *)selectedmenu,this,0);
	menu->add("agent/refresh file",0,(Fl_Callback *)selectedmenu,this,0);
	menu->add("?/about",0,(Fl_Callback *)selectedmenu,this,0);
	//this->end();

	button_rewind=new Fl_Button(200,468,30,30,"@|<");
	button_rewind->callback((Fl_Callback *)selectedrewind,this);
	button_frameback=new Fl_Button(235,468,30,30,"@<<");
	//button_frameback->callback((Fl_Callback *)selectedframeback,this);
	button_stop=new Fl_Button(270,468,30,30,"@square");
	button_stop->callback((Fl_Callback *)selectedstop,this);
	button_play=new Fl_Button(305,468,30,30,"@>");
	button_play->callback((Fl_Callback *)selectedplay,this);
	button_framenext=new Fl_Button(340,468,30,30,"@>>");
	//button_framenext->callback((Fl_Callback *)selectedframenext,this);
	/*
	button1=new Fl_Button(110,450,100,40);
	button1image=new Fl_JPEG_Image("button1.jpg");
	button1->label("ciao!");
	button1->image(button1image);
	button2=new Fl_Button(215,450,100,40);
	button2->label("bottone");
	this->end();
	*/


	facebox=new Fl_Box(FL_BORDER_BOX,5,35,190,100,"rendering");
	facebox->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT|FL_ALIGN_TOP);

	button_hair=new Fl_Check_Button(170,35,20,20,"show hair");
	button_hair->align(FL_ALIGN_LEFT);
	button_hair->value(1);
	button_hair->callback((Fl_Callback *)selectedhair,this);

	button_skin=new Fl_Check_Button(170,60,20,20,"show head skin");
	button_skin->align(FL_ALIGN_LEFT);
	button_skin->value(1);
	button_skin->callback((Fl_Callback *)selectedskin,this);

	button_eyesteeths=new Fl_Check_Button(170,85,20,20,"show eyes/teeths/tongue");
	button_eyesteeths->align(FL_ALIGN_LEFT);
	button_eyesteeths->value(1);
	button_eyesteeths->callback((Fl_Callback *)selectedeyes,this);

	button_skeleton=new Fl_Check_Button(170,110,20,20,"show body skin");
	button_skeleton->align(FL_ALIGN_LEFT);
	button_skeleton->value(1);
	button_skeleton->callback((Fl_Callback *)selectedskeleton,this);

	//button_ifapu=new Fl_Check_Button(170,115,20,20,"ifapu");
	//button_ifapu->align(FL_ALIGN_LEFT);
	//button_ifapu->value(1);
	//button_ifapu->callback((Fl_Callback *)selectedifapu,this);

	button_startcapture=new Fl_Button(375,468,60,30,"capture");
	button_startcapture->callback((Fl_Callback *)selectedstartcapture,this);

	//button_endcapture=new Fl_Button(75,145,60,30,"end capture");
	//button_endcapture->callback((Fl_Callback *)selectedendcapture,this);

	framenumber=new Fl_Output(90,150,100,30,"frame n.");
	framenumber->textfont(FL_HELVETICA);
	framenumber->textsize(12);
	framenumber->align(FL_ALIGN_LEFT);

	framerate=new Fl_Output(90,185,100,30,"frame rate");
	framerate->textfont(FL_HELVETICA);
	framerate->textsize(14);
	framerate->align(FL_ALIGN_LEFT);
	//framenumber->label("frame n.");

	for(i=0;i<3;i++)
	{
		char *number;
		number=(char*)malloc(sizeof(char*)*3);
		sprintf_s(number,3,"%d",i+1);
		button_savecamera[i]=new Fl_Button(90+25*i,400,20,20,number);
	}

	for(i=0;i<3;i++)
	{
		char *number;
		number=(char*)malloc(sizeof(char*)*3);
		sprintf_s(number,3,"%d",i+1);
		button_loadcamera[i]=new Fl_Button(90+25*i,425,20,20,number);
	}

	

	this->callback((Fl_Callback *)gretaplayerwindowclosed,this);

	position_x=0;
	position_y=0;
	width=700;
	height=400;
	
	quietversion=false;

	capturing=false;

	isfullscreen=false;

	goingback=false;

	goingnext=false;


}

PlayerFLTKWindow::~PlayerFLTKWindow()
{
	delete glw;
	
	delete menu;
	delete button_play;
	delete button_stop;
	delete button_frameback;
	delete button_framenext;
	delete button_rewind;
	delete button_hair;
	delete button_skin;
	delete button_eyesteeths;
	delete button_skeleton;
	delete facebox;
	delete button_startcapture;
	delete framenumber;
	delete framerate;
	delete button_savecamera[0];
	delete button_savecamera[1];
	delete button_savecamera[2];
	delete button_loadcamera[0];
	delete button_loadcamera[1];
	delete button_loadcamera[2];
}

int PlayerFLTKWindow::handle(int event)
{
	int i;
	
	if((event==FL_PUSH)&&(Fl::event_button()==FL_LEFT_MOUSE)){
		for(i=0;i<3;i++){
			if(Fl::event_inside(button_loadcamera[i])){
				button_loadcamera[i]->value(1);//push the button
				button_loadcamera[i]->take_focus();
				return 1;
			}
			if(Fl::event_inside(button_savecamera[i])){
				button_savecamera[i]->value(1);//push the button
				button_savecamera[i]->take_focus();
				return 1;
			}
		}

		if(Fl::event_inside(button_frameback)){
			button_frameback->value(1);//push the button
			button_frameback->take_focus();
			goingback=true;
			return 1;
		}

		if(Fl::event_inside(button_framenext)){
			button_framenext->value(1);//push the button
			button_framenext->take_focus();
			goingnext=true;
			return 1;
		}
	}

	if((event==FL_RELEASE)&&(Fl::event_button()==FL_LEFT_MOUSE))
	{
		button_frameback->value(0);//release the button
		button_framenext->value(0);//release the button
		goingback=false;
		goingnext=false;
		for(i=0;i<3;i++){
			if(Fl::event_inside(button_loadcamera[i]) && button_loadcamera[i]->value()==1){
				button_loadcamera[i]->value(0);//release the button
				glw->SetCameraView(i);
				return 1;
			}
			button_loadcamera[i]->value(0);//release the button
			if(Fl::event_inside(button_savecamera[i]) && button_savecamera[i]->value()==1){
				button_savecamera[i]->value(0);//release the button
				glw->SaveCameraSettings(i);
				return 1;
			}
			button_savecamera[i]->value(0);//release the button
		}
	}

	if(event==FL_KEYUP )
	{
		if((!isfullscreen) && (Fl::event_key()==FL_Enter || Fl::event_key() == 102))//key 'f'
		{
			position_x=x();
			position_y=y();
			width=w();
			height=h();
			fullscreen();
			glw->fullscreen();
			isfullscreen=true;
			return 1;
		}
		if(isfullscreen && (Fl::event_key()==FL_BackSpace || Fl::event_key() == 102))//key 'f'
		{
			glw->fullscreen_off(200,35,width-205,height-70);
			fullscreen_off(position_x,position_y,width,height);
			size_range(440,450,0,0,1,1,0);
			isfullscreen=false;
			return 1;
		}
		if(Fl::event_key() == 112)//key 'p'
		{
		//rewind
			glw->getAgent()->StopTalking();
			glw->getAgent()->SetCurrentFrame(0);
			if(glw->getListener()!=0){
					glw->getListener()->StopTalking();
					glw->getListener()->SetCurrentFrame(0);
			}
		//and play
			glw->getAgent()->StartTalking();
			if(glw->getListener()!=0)
				glw->getListener()->StartTalking();
			return 1;
		}
	}

	return Fl_Double_Window::handle(event);
}

void PlayerFLTKWindow::draw()
{
	if(goingback==true)
	{
		glw->getAgent()->StopTalking();
		glw->getAgent()->OneFrameDown();
		if(glw->getListener()!=0)
		{
			glw->getListener()->StopTalking();
			glw->getListener()->OneFrameDown();
		}
	}
	if(goingnext==true)
	{
		glw->getAgent()->StopTalking();
		glw->getAgent()->OneFrameUp();
		if(glw->getListener()!=0)
		{
			glw->getListener()->StopTalking();
			glw->getListener()->OneFrameUp();
		}
	}
	if(isfullscreen==false)
	{
		menu->size(this->w(),30);
		glw->size(this->w()-205,this->h()-70);
		glw->invalidate();
		button_rewind->position(200,this->h()-32);
		button_frameback->position(235,this->h()-32);
		button_stop->position(270,this->h()-32);
		button_play->position(305,this->h()-32);
		button_framenext->position(340,this->h()-32);
		button_startcapture->position(375,this->h()-32);
		framenumber->position(465,this->h()-32);
	}

	Fl_Window::draw();

	fl_font(FL_HELVETICA,11);
	fl_color(0,0,0);
	fl_draw("save camera",22,415);
	fl_draw("recall camera",22,440);
	
	if(capturing)
	{
		if(strcmp(glw->getAgent()->GetStatus(),"ready")==0)
			this->EndCapture();
		if(strcmp(glw->getAgent()->GetStatus(),"talking")==0)
		{
			//HANDLE dibh;

			//dibh=DIBCaptureRegion();

			//AddFrame(dibh);
			
		}
	}

}

void PlayerFLTKWindow::SetQuiet(bool q)
{
	quietversion=q;
	if(q==true)
		menu->hide();
}

void PlayerFLTKWindow::LoadFile(std::string filebasename)
{
	glw->filetoload=filebasename;
}

/*
HANDLE PlayerFLTKWindow::DIBCaptureRegion()
{

	captRegion.left = x();
	captRegion.top = y();
	captRegion.right = x()+w();
	captRegion.bottom = y()+h();
	rRegion = &captRegion;

	hDib = CopyScreenToDIB(rRegion);

	return hDib;
}
*/

void PlayerFLTKWindow::Capture()
{
	if(strcmp(glw->getAgent()->GetStatus(),"ready")==0)
	{
		AviInit("output.avi",w(),h());
		glw->getAgent()->StopTalking();
		glw->getAgent()->WRITEAVI=true;
		glw->getAgent()->SetCurrentFrame(0);
		glw->getAgent()->StartTalking();
		capturing=true;
		printf("started capturing\n");
	}
}

void PlayerFLTKWindow::EndCapture()
{

	if(glw->getAgent()!=0)
	{
		if(strcmp(glw->getAgent()->GetStatus(),"ready")==0)
			if(capturing==true)
			{
				printf("1\n");
				//printf("wave filename is %s\n",glw->agent->GetWavFileName());			
				capturing=false;
				printf("ended capturing\n");
				glw->getAgent()->WRITEAVI=false;
				glw->getAgent()->StopTalking();
				glw->getAgent()->SetCurrentFrame(0);
				printf("2\n");
				AddWAV(glw->getAgent()->GetWavFileName());
				printf("3\n");
				AviClose();
			}
	}
}
