//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// ActionSelection.cpp: implementation of the ActionSelection class.
//
// Author: Etienne de Sevin etienne.de-sevin@telecom-paristech.fr
//
//////////////////////////////////////////////////////////////////////



#include "ActionSelectionConnector.h"
#include <semaine/cms/message/SEMAINEMessage.h>
#include <semaine/datatypes/stateinfo/UserStateInfo.h>
#include <semaine/cms/message/SEMAINEStateMessage.h>
#include <semaine/datatypes/xml/BML.h>
#include <semaine/datatypes/xml/FML.h>
#include "GretaLogger.h"
#include "XercesTool.h"

extern std::list<GretaLogger*> listLog;
extern NInterface *inter;

namespace semaine {
namespace components {
namespace actionselectionconnector {

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ActionSelectionConnector::ActionSelectionConnector(const std::string & componentname, bool isInput, bool isOutput) throw(CMSException):
	Component(componentname, isInput, isOutput)
{
		initConnection();
}

	ActionSelectionConnector::~ActionSelectionConnector() 	
{
	delete fmlReceiver;
	delete bmlReceiver;
	delete bmlSender;
	delete fmlSender;
	delete UilReceiver;
	delete agentSpeakingReceiver;
	delete agentTurnReceiver;

}

void ActionSelectionConnector::react(SEMAINEMessage * m) throw(CMSException)
{
	ReadMessage(m);
	choiceSend();

	std::list<GretaLogger*>::iterator iterlog;

	for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
	{
		std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
		if((*iterlog)->getLevel()=="debug")
			log->debug(msg);
		if((*iterlog)->getLevel()=="info")
			log->info(msg);
		if((*iterlog)->getLevel()=="warn")
			log->warn(msg);
		if((*iterlog)->getLevel()=="error")
			log->error(msg);
	}
	listLog.clear();

	inter->drawNI(AS.getNIU(), AS.getMoyNIU(), AS.getinterPhase(), AS.getanticipNIU(), AS.getPriority(), AS.getTypeChosen(), AS.getactchosen(), arousal, valence, potency);
	AS.anticipationNIU(); 
}

void ActionSelectionConnector::act() throw(CMSException)
{
	Sleep(5);
	choiceSend();
	inter->drawNI(AS.getNIU(), AS.getMoyNIU(), AS.getinterPhase(), AS.getanticipNIU(), AS.getPriority(), AS.getTypeChosen(), AS.getactchosen(), arousal, valence, potency);
	//AS.anticipationNIU(); 
	if(inter->activeManualNIU == 0)
		AS.setNIU(NIU);
	if(inter->activeManualNIU == 1)
	{
		AS.setNIU(((inter->valueManualNIU))); 
		//std::cout << AS.getNIU() << "\n";
	}

}

void ActionSelectionConnector::customStartIO() throw(std::exception) 
{
	choice = "";
	NIU = 0.0;
	choice = "";
	fapend = 0;
	audioend = 0;
	fapendtime = 0;
	timestart = 0;
	speaking = "";
	agentTurn = "";
	waiting = 0;
	difD = 0.0;
	hascontent = 0;
	for(int i=0; i<50; i++) idaction[i]="";
	for(int i=0; i<50; i++) eventaction[i]="";
	for(int i=0; i<50; i++) startaction[i]=0;
	iterXML = 0;
	arousal = 0.0;
	valence = 0.0;
	potency = 0.0;
	old_NIU = 0.0;
	hyst = 0.25;
	randgen= new RandomGen;

}
void ActionSelectionConnector::choiceSend() throw(CMSException)
{
	if(waiting == 0 && hascontent == 1)
	//if(hascontent == 1)
	{
		choice = AS.choice();

		int nchosen = AS.getNchosen();
		//XERCES_CPP_NAMESPACE::DOMDocument * doc = XercesTool::parse(AS.getBMLFMLsend());
		if(choice == "REACTIVE")
			fmlSender->sendTextMessage(AS.getBMLFMLsend(), meta.getTime(), eventaction[nchosen], idaction[nchosen], startaction[nchosen]);
			//fmlSender->sendXML(doc, meta.getTime(), eventaction[nchosen], idaction[nchosen], startaction[nchosen]); //SEMAINE_CMS_EVENT_SINGLE
		if(choice == "COGNITIVE")
			fmlSender->sendTextMessage(AS.getBMLFMLsend(), meta.getTime(), eventaction[nchosen], idaction[nchosen], startaction[nchosen]);
			//fmlSender->sendXML(doc, meta.getTime(), eventaction[nchosen], idaction[nchosen], startaction[nchosen]);
		if(choice == "MIMICRY")
			bmlSender->sendTextMessage(AS.getBMLFMLsend(), meta.getTime(), eventaction[nchosen], idaction[nchosen], startaction[nchosen]);
			//bmlSender->sendXML(doc, meta.getTime(), eventaction[nchosen], idaction[nchosen], startaction[nchosen]);
		if(choice != "")
		{
			waiting = 1;
			/*#ifdef _DEBUG
				std::cout << "\nDEMARRER\n";
			#endif*/
			iterXML = 0;
			time(&startD);
			hascontent = 0;
		}
	}
	if(waiting==1)
	{
		time(&endD);
		difD = difftime(endD, startD);
		//difD = difD/CLOCKS_PER_SEC;
		//std::cout << difD << "\n";
	}	

	if (waiting==1 && difD >= 2) //waiting==1 && audioend == 1 || waiting==1 && fapend == 1 || 
	{
		/*#ifdef _DEBUG
			std::cout << "\nFINI  " << difD << "\n";
		#endif*/
		difD = 0.0;
		waiting = 0;
	}

	//AS.drawNI();
}

void ActionSelectionConnector::initConnection() throw(CMSException)
{	
	fmlReceiver = new FMLReceiver("semaine.data.action.candidate.function");
	receivers.push_back(fmlReceiver); 
	bmlReceiver = new BMLReceiver("semaine.data.action.candidate.behaviour");
	receivers.push_back(bmlReceiver);
	bmlSender = new BMLSender("semaine.data.action.selected.behaviour", getName());
	senders.push_back(bmlSender);
	fmlSender = new FMLSender("semaine.data.action.selected.function", getName());
	senders.push_back(fmlSender);
	UilReceiver = new StateReceiver("semaine.data.state.user.behaviour", semaine::datatypes::stateinfo::StateInfo::Type::UserState);
	receivers.push_back(UilReceiver);
	//agentSpeakingReceiver = new StateReceiver("semaine.data.state.agent", semaine::datatypes::stateinfo::StateInfo::Type::AgentState);
	//receivers.push_back(agentSpeakingReceiver);
	agentTurnReceiver = new StateReceiver("semaine.data.state.dialog", semaine::datatypes::stateinfo::StateInfo::Type::DialogState);
	receivers.push_back(agentTurnReceiver);
	contextReceiver = new StateReceiver("semaine.data.state.context", semaine::datatypes::stateinfo::StateInfo::Type::ContextState); 
	receivers.push_back(contextReceiver);
	//audioReceiver = new Receiver("semaine.callback.output.audio");
	//receivers.push_back(audioReceiver);
	//FAPReceiver = new Receiver("semaine.callback.output.FAP");
	//receivers.push_back(FAPReceiver);
	//commandReceiver = new Receiver("semaine.data.synthesis.lowlevel.command");
	//receivers.push_back(commandReceiver);
	
	//this->start();

}


void ActionSelectionConnector::ReadMessage(SEMAINEMessage *msg) throw(CMSException) 
{

	if (msg == NULL) {
		throw MessageFormatException("expected XML message, got "+std::string(typeid(*msg).name()));
	}

	std::string type=msg->getDatatype();

	/*if(type == "command") 
	{ 
		std::string command;
		command = "";
		command=msg->getText();

		if (command.find("TIMEFAPEND") != -1 || (command.find("TIMESTART") != -1))
		{
			if (command.find("TIMEFAPEND") != -1)
			{
				fapendtime = atoi(command.substr(command.find("TIMEFAPEND")+11).c_str());
			}
			if (command.find("TIMESTART") != -1)
			{
				timestart = atoi(command.substr(command.find("TIMESTART")+10).c_str());
			}	
		}
	}*/
	

	/*if (type == "callback") 
	{
		std::string callback;
		callback=msg->getText();

		if (callback.find("audio") != -1)
		{
			if (callback.find("end") != -1)
			{
				audioend = 1;
			}
			if (callback.find("start") != -1)
			{
				audioend = 0;
			}
		}
		if (callback.find("FAP") != -1)
		{
			if (callback.find("end") != -1)
			{
				fapend = 1;
			}
			if (callback.find("start") != -1)
			{
				fapend = 0;
			}
		}
	}*/

	//AS.NIU = 0.5; //because I had no UIL input	
	if (type == "UserState") 
	{
		//SEMAINEStateMessage * sm = dynamic_cast<SEMAINEStateMessage *>(message);
		SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(msg);
		if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*msg).name());
		semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
		if (state->hasInfo("interest"))
		{
			std::string chname = "";
			chname = state->getInfo("interest");
			NIU = atof(chname.c_str());
			//permet d'ajuster a l'intervalle [0, 1] (avant [-1, 1])
			#ifdef _DEBUG
			if(inter->activeManualNIU == 0)
				printf("\nDetected Interest level: %f ", NIU);
			#endif
			NIU = (NIU+1.0)/2.0;
			if (NIU > 1)
				NIU = 1;
			if (NIU < 0)
				NIU = 0;
			//hysteresis pour avoir un NIU plus fluide
			//NIU = (1-hyst)*old_NIU + hyst*NIU;
			//AS.setNIU(NIU);
			//ajout dans act() pour les courbes
			old_NIU = NIU;
		}
		
		if (state->hasInfo("valence"))
		{
			std::string chname = "";
			chname = state->getInfo("valence");
			valence = atof(chname.c_str());
			valence = (valence+1.0)/2.0;
			if (valence > 1)
				valence = 1;
			if (valence < 0)
				valence = 0;
			AS.setUserValence(valence);
			/*#ifdef _DEBUG
				printf("\nDetected valence: %f ", valence);
			#endif*/		
		}
		if (state->hasInfo("arousal"))
		{
			std::string chname = "";
			chname = state->getInfo("arousal");
			arousal = atof(chname.c_str());
			arousal = (arousal+1.0)/2.0;
			if (arousal > 1)
				arousal = 1;
			if (arousal < 0)
				arousal = 0;
			AS.setUserArousal(arousal);
			/*#ifdef _DEBUG
				printf("\nDetected arousal: %f ", arousal);
			#endif*/		
		}
		if (state->hasInfo("potency"))
		{
			std::string chname = "";
			chname = state->getInfo("potency");
			potency = atof(chname.c_str());
			potency = (potency+1.0)/2.0;
			if (potency > 1)
				potency = 1;
			if (potency < 0)
				potency = 0;
			/*#ifdef _DEBUG
				printf("\nDetected potency: %f ", potency);
			#endif*/		
		}

		if (state->hasInfo("speaking"))
		{
			speaking = state->hasInfo("speaking");
			//std::cout << " " << "speaking" << "=" << state->getInfo("speaking");
		}

		/*if (state->hasInfo("emotion-quadrant"))
		{
			std::cout << " " << "emotion-quadrant" << "=" << state->getInfo("emotion-quadrant");
		}
		if (state->hasInfo("facialExpression"))
		{
			std::cout << " " << "facialExpression" << "=" << state->getInfo("facialExpression");
		}
		if (state->hasInfo("headGesture"))
		{
			std::cout << " " << "headGesture" << "=" << state->getInfo("headGesture");
		}*/
	}

	/*if (type == "AgentState") 
	{
		SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(msg);
		if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*msg).name());
		semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
		if (state->hasInfo("turnTakingIntention"))
		{
			speaking = state->getInfo("turnTakingIntention");
			printf("\nAgent is speaking %s", speaking.c_str());
		}	
	}*/

	if (type == "DialogState") 
	{
		SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(msg);
		if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*msg).name());
		semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
		if (state->hasInfo("agentTurnState"))
		{
			agentTurn = state->getInfo("agentTurnState");
			std::cout << "\nTurn Agent " << agentTurn << "\n";
		}	
	}
	
	if(type == "ContextState") { 
		SEMAINEStateMessage * sm = (SEMAINEStateMessage *)(msg);
		if (sm == NULL) throw MessageFormatException(std::string("Expected state message, got a ")+typeid(*msg).name());
		semaine::datatypes::stateinfo::StateInfo * state = sm->getState();
		if (state->hasInfo("character"))
		{
			agentname = state->getInfo("character");
			float stable;
			float extroverted;
			if(agentname == "Poppy")
			{
				extroverted = 1.0; //1
				stable = 0.5; //0.875
				AS.setStability(stable);
				AS.setExtroversion(extroverted);
				AS.setAgentArousal(1.0);
				AS.setAgentValence(1.0);
			}
			if(agentname == "Spike")
			{
				extroverted = 0.5; //0.875
				stable = -1.0; //0.5
				AS.setStability(stable);
				AS.setExtroversion(extroverted);
				AS.setAgentArousal(1.0);
				AS.setAgentValence(-1.0);
			}
			if(agentname == "Prudence")
			{
				extroverted = -0.5; //0.625
				stable = 1.0;  //1
				AS.setStability(stable);
				AS.setExtroversion(extroverted);
				AS.setAgentArousal(0.0);
				AS.setAgentValence(0.0);
			}
			if(agentname == "Obadiah")
			{
				extroverted = -1.0; //0.5
				stable = -0.5;  //0.625
				AS.setStability(stable);
				AS.setExtroversion(extroverted);
				AS.setAgentArousal(-1.0);
				AS.setAgentValence(-1.0);
			}
		}
	} 

	if (type == "FML") 
	{		
		SEMAINEXMLMessage * xm = (SEMAINEXMLMessage *)(msg);	
		std::string content = xm->getText();


		if (xm->getContentID().find("uap") != -1)
		{
			/*#ifdef _DEBUG
				std::cout << "\nUtterances\n";
			#endif*/
			hascontent = 1;
			//fmlSender->sendTextMessage(content, msg->getUsertime(), msg->getEventType(), xm->getContentID(), xm->getContentCreationTime());
			AS.setPriorityRule(2);
			AS.setBCtype("COGNITIVE");
			AS.setContent(content.c_str());
			//if(AS.receiveC() == true){}
			AS.receiveC();
			idaction[iterXML] = xm->getContentID();
			eventaction[iterXML] = xm->getEventType();
			startaction[iterXML] = xm->getContentCreationTime();
			iterXML = iterXML + 1;
		}

		if(inter->activeManualNoC == 1)
			agentTurn = "false";

		//backchannels seulement si listener
		if (xm->getContentID().find("lip") != -1 && agentTurn == "false") // && speaking == "true") 
		{
			//fmlSender->sendTextMessage(content, msg->getUsertime(), msg->getEventType());
			/*#ifdef _DEBUG
				std::cout << "\nFML\n";
			#endif
			#ifdef _DEBUG
				std::cout << "REACTIVE"<<"\n";
			#endif*/
			hascontent = 1;
			AS.setPriorityRule(1);
			AS.setBCtype("REACTIVE");
			AS.setContent(content.c_str());
			//if(AS.receiveBC() == true){}
			AS.receiveBC();
			idaction[iterXML] = xm->getContentID();
			eventaction[iterXML] = xm->getEventType();
			startaction[iterXML] = xm->getContentCreationTime();
			iterXML = iterXML + 1;
		}
	}

	if(inter->activeManualNoC == 1)
		agentTurn = "false";

	//backchannels seulement si listener
	if (type == "BML" && agentTurn == "false") // && speaking == "true") 
	{
		SEMAINEXMLMessage * xm = (SEMAINEXMLMessage *)(msg);	
		std::string content = xm->getText();

		//bmlSender->sendTextMessage(BMLFMLsend, meta.getTime(), message->getEventType());
		/*#ifdef _DEBUG
			std::cout << "\nMIMICRY\n";
		#endif*/
		hascontent = 1;
		AS.setBCtype("MIMICRY");
		AS.setPriorityRule(1);
		AS.setContent(content.c_str());
		//if(AS.receiveM() == true){}
		AS.receiveM();
		idaction[iterXML] = xm->getContentID();
		eventaction[iterXML] = xm->getEventType();
		startaction[iterXML] = xm->getContentCreationTime();
		iterXML = iterXML + 1;
	}

	AS.setZone("Head");

}


} // namespace dummy
} // namespace components
} // namespace semaine
