/*
 *  Element.h
 *  semaine
 *
 *  Created by Marc Schröder on 12.09.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

//#ifndef SEMAINE_COMPONENTS_COMPONENT_H
//#define SEMAINE_COMPONENTS_COMPONENT_H

#include <list>
#include <queue>

#include "CommunicationMessage.h"

#include <semaine/config.h>

#include <decaf/lang/System.h>
#include <decaf/util/concurrent/Concurrent.h>
#include <decaf/util/concurrent/Mutex.h>

#include <cms/CMSException.h>

#include <semaine/cms/SEMAINEMessageAvailableListener.h>
#include <semaine/cms/receiver/Receiver.h>
#include <semaine/cms/sender/Sender.h>
#include <semaine/components/meta/MetaMessenger.h>


using namespace decaf::lang;
using namespace cms;
using namespace semaine::cms;
using namespace semaine::cms::receiver;
using namespace semaine::cms::sender;
using namespace semaine::components::meta;

/**
 * The base class for all components.
 * Subclasses need to:
 * <ul>
 * <li>create receivers and/or senders in their constructor;
 * <li>implement {@link #act()} and/or {@link #react(SEMAINEMessage)}
 * to do something meaningful.</li>
 * </ul>
 * @author marc
 *
 */
class Element : public SEMAINEMessageAvailableListener
{
public:
	static const std::string STATE_STARTING;
	static const std::string STATE_READY;
	static const std::string STATE_STOPPED;
	static const std::string STATE_FAILURE;
	
	virtual ~Element();
	
	const std::string getName() { return name; }
	
	/**
	 * Method called from the corresponding receiver thread, to notify us
	 * that a message is available.
	 */
	void messageAvailableFrom(Receiver * const source);

	/**
	 * The main method executed for this Thread.
	 */
	CommunicationMessage* Element::receive();
	
	void requestExit();
	
	
	std::list<Receiver *> receivers;
	std::list<Sender *> senders;
	std::queue<Receiver *> inputWaiting;
	CMSLogger * log;
	bool _exitRequested;
	std::string state;
	MetaMessenger meta;
	int waitingTime;
	decaf::util::concurrent::Mutex mutex;

	bool isInput;
	bool isOutput;
	
	Element(const std::string & componentName) throw (CMSException);

	bool exitRequested();
	void Element::startIO() throw(CMSException);

private:
	const std::string name;
	

};


//#endif

