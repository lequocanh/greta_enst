#include "glcontainer.h"
#include "IniManager.h"

extern IniManager inimanager;

void GLContainerWindowclosed(void* v)
{
}

GLContainer::GLContainer(void):Fl_Window(700,200,320,200,"feedback window")
{
	inimanager.SetValueString("CHARACTER_SPEAKER","greta");
	inimanager.SetValueInt("PLAYER_ONLYFACE",0);
	inimanager.SetValueInt("PLAYER_SHOWLISTENER",0);
	glwindow=new ViewWindow(5,5,w()-10,h()-10,"test");
	this->callback((Fl_Callback *)GLContainerWindowclosed,this);
	this->size_range(320,200,1280,1024,1,1,0);
}

GLContainer::~GLContainer(void)
{	
	glwindow->RemoveIdle();
	delete ((ViewWindow*)glwindow);
}

void GLContainer::Play(std::string s)
{
	glwindow->getAgent()->EnableAudio(false);
	glwindow->getAgent()->AssignFile((char*)s.c_str());
	glwindow->getAgent()->StartTalking();
}

void GLContainer::draw()
{
	glwindow->size(w()-10,h()-10);
	Fl_Window::draw();
}
void GLContainer::show(){
	Fl_Window::show();
#ifdef GESTUREEDITOROGRE
	glwindow->setBackgroundColour(0.5,0.5,0.5);
#endif
}