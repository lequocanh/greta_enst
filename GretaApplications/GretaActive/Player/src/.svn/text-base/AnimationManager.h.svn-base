#ifndef ANIMATIONMANAGER_DEFINED
#define ANIMATIONMANAGER_DEFINED

//#include "PlayerReceiver.h"
#include "JThread.h"
#include "Animation.h"
#include "CentralClock.h"



class AnimationHistory{
private:
	std::string id_;
	_time_ when_;
	animstat stat_;
public:
	AnimationHistory(std::string id, _time_ when, animstat stat){ id_=id; when_=when; stat_=stat;}
	std::string getID(){return id_;}
	_time_ getTime(){return when_;}
	animstat getStat(){return stat_;}
	/** be carefull! */
	void incrementID(){ id_ += "*"; }
};



class AnimationManager : public cmlabs::JThread
{
private:
	//semaine::components::greta::PlayerReceiver *playrec;
	std::vector<AnimationHistory*> history;
	std::vector<Animation*> queue;
	std::vector<Animation*> waitingQueue;
	Animation* toPlay; //playing animation
	int indexFAPtoPlay;
	bool fapPlaying;
	int indexBAPtoPlay;
	bool bapPlaying;
	int audioToPlay; //0 = not started, 1 = playing, 2 = ended;
	_time_ started_at;
	Animation* recent; //animation recently used
	int maxsize;
	CentralClock *clock;
	cmlabs::JMutex mutex;
	Animation* createAnimation(std::string id); //creates a new animation in the waiting queue
	AnimationHistory* findInHistory(std::string id);
	void waitingQueueToReadyQueue(std::string id);
	Animation* getAnimationAlsoPlaying(std::string id);
	void checkNextAnimationToPlay();

//TODO : send informations (optional)
	/** called by checkNextAnimationToPlay() */
	void sendStarted(std::string id);

	/** called by playAudio() */
	void sendAudioStarted(std::string id);

	/** called by getFAPFrame(...) */
	void sendFAPStarted(std::string id);

	/** called by getBAPFrame(...) */
	void sendBAPStarted(std::string id);

	/** called by audioEnded() */
	void sendAudioEnded(std::string id);

	/** called by getFAPFrame(...) */
	void sendFAPEnded(std::string id);

	/** called by getBAPFrame(...) */
	void sendBAPEnded(std::string id);
	
	/** called by checkNextAnimationToPlay() */
	void sendEnded(std::string id);

	/** called by run() */
	void sendDead(std::string id);

	/** called by deleteAnimation(...)
	* success: 0=successful, 1=failed playing, 2=failed ended, 3=failed dead, 4=failed already deleted, 5=failed unknown
	*/
	void sendDeleted(std::string id, int success);

	/** called by stop() */
	void sendStopped(std::string id, BAPFrame bapframe, FAPFrame fapframe);

//\TODO
public:
	/*
	* CONSTRUCTORS
	*/
	AnimationManager(CentralClock *centralClock, int maximal_number_of_animations=50);
	~AnimationManager();

	//void setReceiver(semaine::components::greta::PlayerReceiver* playrec_){this->playrec = playrec_;}

	/**
	* returns and removes (in the queue) the first Animation
	* @return the first animation in queue
	*/
	Animation* pullout(){ return pullout(0); }

	/**
	* returns and removes (in the queue) the Animation at the specified index
	* @param index index in queue
	* @return the animation at the specified index in queue
	*/
	Animation* pullout(int index);

	/**
	* adds an animation in the queue.
	* the animation will be placed in the correct position in the queue according to the operator < of Animation class
	* @param anim the animation to pull on
	* @return its index in the queue
	*/
	int pullon(Animation* anim);

	/**
	* converts FAP frames from text format to binary format and adds all frames in the specific animation.
	* @param id the target animation name (id)
	* @param textFrames the frames to add in text format
	*/
	void addFAPFrames(std::string id, std::string textFrames);

	/**
	* adds a binary FAP frame in the specific animation.
	* @param id the target animation name (id)
	* @param Frame the frame to add
	*/
	void addFAPFrame(std::string id, FAPFrame frame);

	/**
	* converts BAP frames from text format to binary format and adds all frames in the specific animation.
	* @param id the target animation name (id)
	* @param textFrames the frames to add in text format
	*/
	void addBAPFrames(std::string id, std::string textFrames);
	
	/**
	* adds a binary BAP frame in the specific animation.
	* @param id the target animation name (id)
	* @param Frame the frame to add
	*/
	void addBAPFrame(std::string id, BAPFrame frame);

	/**
	* adds audio buffer in the specific animation.
	* @param id the target animation name (id)
	* @param bufferAudio the audio buffer
	* @param bufferSize the size of the audio buffer
	*/
	void addAudio(std::string id, char* bufferAudio, int bufferSize);

	/**
	* Sets priority, start time and life time to a specified animation
	* @param id the target animation name (id)
	* @param priority the priority of the animation
	* @param startTime when the animation must begin
	* @param lifeTime the maximum offset to start the animation
	*/
	void setStartInfos(std::string id, double priority, _time_ startTime, _time_ lifeTime, bool hasAudio);

	/**
	* Deletes a specified animation.
	* The animation can be deleted only if it is waitting in queue.
	* @param id the target animation name (id)
	*/
	void deleteAnimation(std::string id);

	/**
	* Stop the current animation and clear all the queue
	*/
	void stop();

	/**
	* called by the function start() of cmlabs::JThread
	*/
	void run();

	/*
	* GET
	*/
	Animation* getAnimation(std::string id);
	int getFAPNumber(){return toPlay==NULL ? 0 : toPlay->getFAPFramesNumber()-indexFAPtoPlay;}
	int getBAPNumber(){return toPlay==NULL ? 0 : toPlay->getBAPFramesNumber()-indexBAPtoPlay;}

	/**
	* gives the actual BAP frame to play
	* @param frame the frame to modify
	* @return if the frame is updated
	*/
	bool getBAPFrame(BAPFrame &frame);

	/**
	* gives the actual FAP frame to play
	* @param frame the frame to modify
	* @return if the frame is updated
	*/
	bool getFAPFrame(FAPFrame &frame);

	void playAudio();

	/**
	* only for RealTimeAudio to notify the end off audio playing
	*/
	void audioEnded();

	/**
	* adds the character '*' at the and of all identifiants in the history
	* be carefull when using it !
	*/
	void incrementIDInHistory();


};



#endif
