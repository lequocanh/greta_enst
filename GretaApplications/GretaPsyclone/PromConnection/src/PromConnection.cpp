
#include "DataContainer.h"
#include "PromConnection.h"
#include "IniManager.h"
#include "Timer.h"
#include <stdio.h>
#include "XercesTool.h"
#include "XMLDOMTree.h"
#include "GretaLogger.h"
#include <time.h>

extern std::list<GretaLogger*> listLog;
extern DataContainer *datacontainer;
extern IniManager inimanager;
extern CentralClock *pc;

PromConnection::PromConnection()
{	
	initPsycloneConnection();

	listenerdata=datacontainer->getListenerData();

	agentstate=new AgentState(listenerdata->agentstate);
	behaviourselector=new BehaviourSelector(this, pc, agentstate);
	behaviourselector->mimicryLevel=agentstate->getValue("mimicry_level.mimicry.value");
//	behaviourselector->start();
	inputFromTalk=new InputNeuronsGroup();
	inputNonVerbal=new InputNeuronsGroup();
	messageTest=new CommunicationMessage();
	delayBeforeBMLSending=2800; /* 'n' milli-seconds delay between two BML sending */
	timeFirstMessageOfNextAnim=24*3600*1000;/* initialisation to the longest duration possible (one full day) */
	cptNbMessages=0;
	for (int i=0; i<50;i++)	{this->bufferInput[i]=new InputNeuronsGroup();} /* Creation buffer de taille 'I' */
	reactivebehaviourtrigger=new ReactiveBehaviourTrigger(agentstate);
	
	
	while(true)
	{
//		Sleep(2000);
//		behaviourselector->Smile();
		//Sleep(3000);
		//behaviourselector->Speech();

		/***** Creation of messageTest *****/
		//Sleep(500);
		//messageTest->setType((GretaName+".Data.fromProm").c_str());
		//char s[256];
		//int nonverb=0;
		//if((rand()%100)<10)nonverb=1; /* 20% of chance to have to perform non-verbal behavior */
		//sprintf(s,"3 %i %i %i\n",1,nonverb,0);
		//messageTest->setContent(s);   /* messages are received regularly, independently from the cpttimestep */
		//printf(s);
		/**********************************/
		Dynamic2action();

		
		//ReadMessage(messageTest);				/* Command for testing the programm with self-made message 'messageTest' */
		ReadMessage(commint->ReceiveMessage(5));/* Default command (when it no more a test phase */
	//	ReadInputMessages();					/* Added if a specific 'Whiteboard' is necessary */
		GetSystemTime(&st);
		systemTimeMS=1000*((st.wHour+2/*2hours lag between Paris hour and System*/)*3600+st.wMinute*60+st.wSecond)+st.wMilliseconds;
		
		if (timeFirstMessageOfNextAnim+delayBeforeBMLSending<=systemTimeMS)
		{
			printf("behaviourselector->action\n");
			behaviourselector->Action(&(bufferInput[0]),this->cptNbMessages, this->timeFirstMessageOfNextAnim, this->delayBeforeBMLSending);
			timeFirstMessageOfNextAnim=24*3600*1000;/* re-initialisation to the longest duration possible (one full day) */	
			cptNbMessages=0;
		}


		std::list<GretaLogger*>::iterator iterlog;

		for(iterlog=listLog.begin();iterlog!=listLog.end(); iterlog++)
		{
			std::string msg=(*iterlog)->getProject() + ": " + (*iterlog)->getMessage();
			if((*iterlog)->getLevel()=="debug")
				this->commint->log->debug(msg);
			if((*iterlog)->getLevel()=="info")
				this->commint->log->info(msg);
			if((*iterlog)->getLevel()=="warn")
				this->commint->log->warn(msg);
			if((*iterlog)->getLevel()=="error")
				this->commint->log->error(msg);
		}
		listLog.clear();
		
	}
}

PromConnection::~PromConnection()
{
}


// Load the application parameter from the inimanager
int PromConnection::initPsycloneConnection()
{	
	
	// Register Psyclone module adress
	moduleHost=inimanager.GetValueString("PSYCLONE_HOST").c_str();
	// Register Psyclone module port
	modulePort=inimanager.GetValueInt("PSYCLONE_PORT");
	// Get Greta's Name
	GretaName=inimanager.GetValueString("GRETA_NAME").c_str();

	// Creation of the Psyclone interface
	commint=new Psydule(GretaName+".PromConnection",moduleHost,modulePort);
	
	// registered packet list
	std::list<std::string> registeredGretaDatatypes;
	// register to Greta Clock messages.
	registeredGretaDatatypes.push_back(GretaName+CLOCK_MESSAGE_HEADER);
	// register to Caracterisation Baseline Message.
	registeredGretaDatatypes.push_back(GretaName+AGENTSTATE_MESSAGE_HEADER);
	registeredGretaDatatypes.push_back(GretaName+UPDATEAGENTSTATE_MESSAGE_HEADER);
	// register to System Feedback
	registeredGretaDatatypes.push_back(GretaName+SYSTEM_FEEDBACK_MESSAGE_HEADER);
	// register to Cognitive Backchannel
	registeredGretaDatatypes.push_back(GretaName+COGNITIVE_FML_MESSAGE_HEADER);

	/* register specific to PromConnection */
	// register to inputFromTalk
	registeredGretaDatatypes.push_back(GretaName+".Data.PromTalk");
	// register to inputNonVerbal
	registeredGretaDatatypes.push_back(GretaName+".Data.PromNonVerbal");
    // register to input fromProm
	registeredGretaDatatypes.push_back(GretaName+".Data.fromProm");
	




	// registration GretaWhiteboard
	if(commint->Register(GretaName+GRETA_WHITEBOARD,registeredGretaDatatypes)!=0)
	{
		std::cout << GretaName+INPUTGRETA_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_OK << std::endl;		
	}
	else
	{
		std::cout << GretaName+INPUTGRETA_MODULE_NAME << " : " <<LOG_MESSAGE_CONNEXION_FAILED<< std::endl;
		commint=0;
		return 0;
	}
	std::cout << "Registration to GretaWhiteboard succesful" << std::endl;

	return 1;
}

void PromConnection::Dynamic2action()
{
/*	float oscillActivation;
	float oscInhib;
	oscillActivation=neurOscill->getActivation();
	oscInhib=neurOscill->getInhib();
	printf("* Oscillateur * activation : %f, inhibition : %f\n", oscillActivation, oscInhib);
	SYSTEMTIME st;
	GetSystemTime(&st);
	int milliSec=st.wHour*3600000+st.wMinute*60000+st.wSecond*1000+st.wMilliseconds;
	FILE *OpenFile=fopen("C:/Users/Ken/Manips/OscillateursCouples/TwoGretas/FichierResGreta1","a"); // Results recording
	fprintf(OpenFile,"%i %f %f\n", milliSec, oscillActivation, oscInhib);
	fclose(OpenFile);
	behaviourselector->setOscActivation(oscillActivation);
*/
}


int PromConnection::ReadMessage(CommunicationMessage *msg)
{
	std::string content, type;
	long postedTime, receivedTime;
	int time;
	if(msg==NULL) return(0);
	content=msg->getContent();
	type=msg->getType();
	postedTime=msg->getPostedTime();
	receivedTime=msg->getReceivedTime();
	GetSystemTime(&st);
		systemTimeMS=1000*((st.wHour+2/*2hours lag from system*/)*3600+st.wMinute*60+st.wSecond)+st.wMilliseconds;
// essai system time et receivedTime 9/04/10
//		printf("SystemTimeMS: %d    receivedTime: %li\n", systemTimeMS, receivedTime);
// fin de l'essai 9/04/10

	if(strcmp(content.c_str(),"")!=0)
	{
		int numOfFromPromNeurons=this->bufferInput[0]->getNumOfNeurons();
		if(strcmp(type.c_str(),(GretaName+CLOCK_MESSAGE_HEADER).c_str())==0 && numOfFromPromNeurons!=0)
		{
			time=atoi(content.c_str());
			pc->SetTime(time);
			printf(" Update pcTime \n");
		}
		else if(strcmp(msg->getType().c_str(),(GretaName+".Data.fromProm").c_str())==0)
		{		
			if(timeFirstMessageOfNextAnim==24*3600*1000)
			{
				if (receivedTime!=0) timeFirstMessageOfNextAnim=receivedTime;
				else timeFirstMessageOfNextAnim=systemTimeMS;
			}
			if(strcmp(content.c_str(),"")!=0){	
				if(numOfFromPromNeurons==0){
					numOfFromPromNeurons = (int)atoi(content.c_str());			/* the size of these groups may vary, depending */
					printf("initialisation 'bufferInput', numOfFromPromNeurons = %i\n", numOfFromPromNeurons); 					
					for (int i=0; i<20; i++){
					this->bufferInput[i]->iniGroup(numOfFromPromNeurons);	
					}                                                               /* on the variety of non-verbal behaviours and  */
				}																	/* on the model for language					*/
				int firstLength=0;
				for(int i=0; i<numOfFromPromNeurons; i++){
					firstLength=content.find(" ",1)+1;
					content.erase(0,firstLength);
					this->bufferInput[cptNbMessages]->neuronsList[i]=atoi(content.c_str());
				}
				if(receivedTime!=0) {
					this->bufferInput[cptNbMessages]->setInputTime(receivedTime);
					printf("Recording of message with 'ReceivedTime': %li \n",this->bufferInput[cptNbMessages]->getInputTime());
				}
				else {
					this->bufferInput[cptNbMessages]->setInputTime(systemTimeMS);
					printf("Recording of message with 'systeTimeMS': %li \n",this->bufferInput[cptNbMessages]->getInputTime());
				}
				cptNbMessages++;
			}
		}
		else if(strcmp(msg->getType().c_str(),(GretaName+".Data.PromTalk").c_str())==0)
		{
			if(strcmp(content.c_str(),"")!=0){	
				if(this->inputFromTalk->getNumOfNeurons()==0){
					int numOfSpeechNeurons =(int)atoi(content.c_str());			/* The size of these groups may vary, depending */
					printf("initialisation 'inputFromTalk', numOfSpeechNeurons = %i\n", numOfSpeechNeurons);
					this->inputFromTalk->iniGroup(numOfSpeechNeurons);			/* on the variety of non-verbal behaviours and  */
				}																/* on the model for language					*/
				int firstLength=0;
				for(int i=0; i<this->inputFromTalk->getNumOfNeurons(); i++){
					firstLength=content.find(" ",1)+1;
					content.erase(0,firstLength);
					this->inputFromTalk->neuronsList[i]=atoi(content.c_str());
				}
				behaviourselector->ActionSpeech(this->inputFromTalk);
			}

		}
		else if(strcmp(msg->getType().c_str(),(GretaName+".Data.PromNonVerbal").c_str())==0)
		{
			if(strcmp(content.c_str(),"")!=0){	
				if(this->inputNonVerbal->getNumOfNeurons()==0){
					int numOfNonVerbalNeurons = (int)atoi(content.c_str());			/* the size of these groups may vary, depending */
					printf("initialisation 'inputNonVerbal', numOfNonVerbalNeurons = %i\n", numOfNonVerbalNeurons); 					
					this->inputNonVerbal->iniGroup(numOfNonVerbalNeurons);			/* on the variety of non-verbal behaviours and  */
				}																	/* on the model for language					*/
				int firstLength=0;
				for(int i=0; i<this->inputNonVerbal->getNumOfNeurons(); i++){
					firstLength=content.find(" ",1)+1;
					content.erase(0,firstLength);
					this->inputNonVerbal->neuronsList[i]=atoi(content.c_str());
				}
				behaviourselector->ActionNonVerbal(this->inputNonVerbal);
			}
		}
		
		return 1;
	}
	return 0;
}

int PromConnection::ReadInputMessages()
{
	CommunicationMessage *msg=new CommunicationMessage();
	std::string content="", type;
	//	char v[255];
	int time=0;
	int end_time=0;

	//	std::map<std::string,DataBackchannel*> bmlfml;

	time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000);
	end_time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000)+100;

	//while(msg!=NULL)
	while(end_time>time)	
	{
/*		if(HumanHeadMoves==0)
		{
			msg=moduleInput->ReceiveMessage(10);

			if(msg!=NULL)
			{
				type=msg->getType();
				if(strcmp(type.c_str(),INPUT_MESSAGE_HEADER)==0)
				{
					content=msg->getContent();
					HumanHeadMoves=testHumanHeadMoves(content);
				}

				msg=moduleInput->ReceiveMessage(10);
				time=(int)(((float)clock()/CLOCKS_PER_SEC)*1000);
			}
			else
			{
				return(1);
			}
		}
		else
		{
			return(1);
		}*/
	}

	return(1);
}

void PromConnection::SendBehaviour(std::string bmlfml, std::string messagetype)
{
	commint->PostString(bmlfml,(GretaName+GRETA_WHITEBOARD).c_str(), messagetype);
}
std::string PromConnection::getGretaName()
{	
	return GretaName;
}


void PromConnection::CheckAdaptRules(InputData *data)
{
	std::map<std::string, AdaptRules>::iterator iterrule;
	std::vector<Implications>::iterator iterimp;
	std::vector<Operations>::iterator itercons;

	iterrule = listenerdata->mapAdaptRules.find(data->name);


	//check all implication in the rule
	for(iterimp=(*iterrule).second.vecImplication.begin();iterimp!=(*iterrule).second.vecImplication.end();iterimp++)
	{
		if((*iterimp).EvaluateImplication(data)==1)
		{
			for(itercons=(*iterimp).vecConsequence.begin();itercons!=(*iterimp).vecConsequence.end(); itercons++)
				(*itercons).ComputeOperation(data);
		}
	}
	
	for(itercons=(*iterrule).second.vecModify.begin();itercons!=(*iterrule).second.vecModify.end(); itercons++)
		(*itercons).ComputeOperation(data);
}
