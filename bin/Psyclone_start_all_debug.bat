rem -- starting psyclone
cd psyclone\
start /min psyclone spec="GretaEVOSpec.xml" verbose=0
cd ..
utils\WAIT.exe 3

rem -- starting Greta
start Psyclone_ClockDebug.exe
start Psyclone_ListenerIntentPlannerDebug.exe
start Psyclone_BehaviorPlannerDebug.exe
start Psyclone_BehaviorRealizerDebug.exe
::start Psyclone_WoZ.exe
start Psyclone_PlayerDebug.exe

cd ..\GretaAuxiliary\interface\dist
java -classpath ./;./AbsoluteLayout.jar;./JavaAIRPlug.jar;./interface.jar interfacepkg.NewJFrame

:: the console wait for the end of the java interface
:: when we close it, all applications are killed :

taskkill /f /im Psyclone_PlayerDebug.exe
::taskkill /f /im Psyclone_WoZ.exe
taskkill /f /im Psyclone_BehaviorRealizerDebug.exe
taskkill /f /im Psyclone_BehaviorPlannerDebug.exe
taskkill /f /im Psyclone_ListenerIntentPlannerDebug.exe
taskkill /f /im Psyclone_ClockDebug.exe
taskkill /f /im psyclone.exe



