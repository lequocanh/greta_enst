/**
 * Copyright (C) 2008 DFKI GmbH. All rights reserved.
 * Use is subject to license terms -- see license.txt.
 */
package interfacepkg;

import java.io.IOException;
import javax.jms.JMSException;

import eu.semaine.components.Component;
import eu.semaine.jms.sender.BMLSender;
import eu.semaine.jms.sender.FMLSender;
import org.w3c.dom.Document;
import eu.semaine.util.XMLTool;
import org.xml.sax.SAXException;

/**
 * @author marc
 *
 */
public class InterfaceConnector extends Component
{

    public NewJFrame gui;
    private BMLSender bmlsender;
    private FMLSender fmlsender;
    private int iterbml = 0;
    private int iterfml = 0;
 
    /**
	 * @param componentName
	 * @throws JMSException
	 */
    public InterfaceConnector() throws JMSException
	{
            super("NewJFrame", false, false);
            bmlsender = new BMLSender("semaine.data.synthesis.plan", getName());
            senders.add(bmlsender); // so it can be started etc
            fmlsender = new FMLSender("semaine.data.action.selected.function", getName());
            senders.add(fmlsender); // so it can be started etc
            gui = new NewJFrame(this);
            start();
            gui.setVisible(true);
	}

        /*@Override
        protected void react(SEMAINEMessage m)
        throws Exception
        {
            SEMAINEStateMessage xm = (SEMAINEStateMessage) m;
            StateInfo state = xm.getState();
            if (state.hasInfo("userPresent")) {
            	isUserPresent = "present".equals(state.getInfo("userPresent"));
            }
            if (state.hasInfo("character")) {
            	currentCharacter = state.getInfo("character");
            }
            gui.updateWhoIsPresent();
        }*/

     /*public  void sendBML(String BML)
	throws JMSException
	{
            doc.setTextContent(BML);
            //root = doc.getDocumentElement();
            //root.setTextContent(BML);
            System.out.println("Recu");
            bmlsender.sendXML(doc, meta.getTime());
	}

        public void sendFML(String FML)
	throws JMSException
	{
            doc.setTextContent(FML);
            System.out.println("Recu");
            bmlsender.sendXML(doc, meta.getTime());
	}*/

     public  void sendBML(String BML)
	throws JMSException, SAXException, IOException
	{
            Document doc = XMLTool.string2Document(BML);
            bmlsender.sendXML(doc, meta.getTime(), "bml_manual"+(iterbml++), meta.getTime());
            System.out.println("Recu");
            //bmlsender.sendTextMessage(BML, meta.getTime());

	}

        public void sendFML(String FML)
	throws JMSException, SAXException, IOException
	{
            Document doc = XMLTool.string2Document(FML);
            fmlsender.sendXML(doc, meta.getTime(), "fml_manual"+(iterfml++), meta.getTime());
            System.out.println("Recu");
            //fmlsender.sendTextMessage(FML, meta.getTime());
	}

}
