//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorPlanner.h: interface for the BehaviorPlanner class.
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#ifndef SEMAINE_COMPONENTS_GRETA_BEHAVIORPLANNER_H
#define SEMAINE_COMPONENTS_GRETA_BEHAVIORPLANNER_H

#include "FML-APML_AgentEngine.h"

#include <cms/CMSException.h>
#include <map>

#include <semaine/components/Component.h>
#include <semaine/cms/sender/Sender.h>
#include <semaine/cms/receiver/Receiver.h>
#include <semaine/cms/sender/BMLSender.h>
#include <semaine/cms/receiver/FMLReceiver.h>
#include <semaine/cms/receiver/BMLReceiver.h>
#include <semaine/cms/receiver/StateReceiver.h>

static std::string USER = "user";

namespace semaine {
namespace components {
namespace greta {

class BehaviorPlanner : public Component
{
public:
	BehaviorPlanner(const std::string & componentName, bool isInput, bool isOutput) throw (CMSException);
	virtual ~BehaviorPlanner();

	
protected:
	virtual void react(SEMAINEMessage * m) throw (CMSException);
	virtual void act(SEMAINEMessage * m) throw(CMSException);
	void customStartIO() throw(std::exception);

private:
	FMLReceiver * fmlReceiver;
	BMLReceiver * bmlReceiver;
	StateReceiver * agentStateReceiver;
	StateReceiver * agentReceiver;
	BMLSender * bmlSender;
	std::string CalculateFMLAnimation(std::string content);

	FMLAPML_AgentEngine *faae;

	std::string agentname;

	void readBaselines();
	long iterbml;
};

} // namespace greta
} // namespace components
} // namespace semaine


#endif

