#ifndef PLAYEROGRESETOFCHARACTER_H
#define PLAYEROGRESETOFCHARACTER_H

#include "PlayerOgreAgent.h"
#include <ogre.h>

class PlayerOgreSetOfCharacter{

public:
	PlayerOgreSetOfCharacter(Ogre::SceneManager* ogreSceneManager_, 
							 Ogre::SceneNode* worldRootNode_, 
							 bool realtime_=false);
	void addAgent(std::string name, std::string type="");//la variable type permet de distinguer, par exemple, une Greta "speaker" d'une Greta "listener"
	PlayerOgreAgent* getAgent(std::string name, std::string type="");
private:
	std::map<std::string, PlayerOgreAgent*> characterList;
	Ogre::SceneManager* ogreSceneManager;
	Ogre::SceneNode* worldRootNode;
	bool realtime;
	bool characterDefinitionExist(std::string name);
};
#endif
