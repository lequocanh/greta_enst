#pragma once

#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <vector>
#include <time.h>

#include "CommunicationInterface.h"

//#include "Signal.h"

#include "FapFrame.h"
#include "BapFrame.h"
#include "IniManager.h"
#include "CentralClock.h"
#include "Module.h"

#include "CapturedFrame.h"

 
class MCServer
{
public:

	CommunicationInterface *commint;
	std::string name, host;
	int port;

	MCServer(std::string name, std::string host, int port,std::string GretaName);

	virtual ~MCServer();

	int ReadMessage(CommunicationMessage *msg);

	int SendIdle();

	void MainLoop();

	void SendFAPsBAPs(int start_frame_time,int &lastfapframetime,int &lastbapframetime);

	std::list<std::string> datatypes;

	//std::vector<MMSystemSpace::Signal> signals;

	//CommunicationInterface *Module;

	//FaceEngine *faceengine;

	//GestureSpace::MotorPlanner *mplanner;

	FAPFrameVector *fapframes;
	
	//for infinite movements we need another copy of the vector of frames
	
	FAPFrameVector *fapframeswithoutnoise;
	
	BAPFrameVector *bapframes;
	
	CentralClock pc;

	//std::vector<FAPFrame> *all_fapframes;
	
	std::vector<BAPFrame> *all_bapframes;

	//BML_AgentEngine bmlengine;

	float FPS;

	bool NoEndFaceSignals;
	
	int last_fap_frame_time;
	
	int last_bap_frame_time;

	bool send_sound;

	std::string sound_file_name;

	FAPFrame*  findCurrentFrame(FAPFrameVector *frames);

	//FAPFrame last_fap_frame;

	BAPFrame *last_bap_frame;

	FILE *fid;

	std::vector<CapturedFrame*> *capturedframes;

private:

	std::string GretaName;

	int previous_time;

};

