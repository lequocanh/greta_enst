// CLOCK.h: interface for the CLOCK class.
//
//////////////////////////////////////////////////////////////////////

#pragma once


#include <iostream>
#include <fstream>
#include <string>

#include "CentralClock.h"
#include "Psydule.h"


class ClockPsydule : public Psydule
{
public:
	ClockPsydule(std::string name, std::string host, int port, std::string GretaName);
	virtual ~ClockPsydule();

	void SendTime();

	void Reset();

	void setGretaName(std::string gretaName);

	std::string name,host,GretaName;
	int port;
	std::string clockstring;
	CentralClock pc;
	int sentat;
};
