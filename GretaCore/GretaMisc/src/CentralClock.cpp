//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include "CentralClock.h"
//#include "Timer.h"
#include "time.h"

CentralClock::CentralClock()
{
	SetTime(0);
}


CentralClock::~CentralClock()
{
}

void CentralClock::SetTime(long long t)
{
	synchronizedat=(long long)(((float)clock()/CLOCKS_PER_SEC)*1000);
	externaltime=t;
	time=(long long)(((float)clock()/CLOCKS_PER_SEC)*1000)-synchronizedat+externaltime;
	time=(time/40)*40;
}
	

long long CentralClock::GetTime()
{
	time=(long long)(((float)clock()/CLOCKS_PER_SEC)*1000)-synchronizedat+externaltime;
	time=(time/40)*40;
	return time;
}