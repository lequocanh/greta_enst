#include "PlayerReceiver.h"
#include "IniManager.h"
#include "RealTimeAudio.h"

#include <semaine/cms/exceptions/MessageFormatException.h>

#include <cstdlib>
#include <sstream>
#include <decaf/lang/System.h>

using namespace semaine::cms::exceptions;

extern IniManager inimanager;

namespace semaine {
namespace components {
namespace greta {


	PlayerReceiver::PlayerReceiver(const std::string & componentName, bool isInput, bool isOutput,FramesManager* fm, CommandsManager* cm,CentralClock *pc) throw(CMSException) :
	Component(componentName,isInput,isOutput)
{
	this->fm=fm;
	this->pc=pc;
	this->cm=cm;
	sound_time=0;

	fapReceiver = new Receiver("semaine.data.synthesis.lowlevel.video.FAP", "FAP", getName()); 
	receivers.push_back(fapReceiver);
	bapReceiver = new Receiver("semaine.data.synthesis.lowlevel.video.BAP", "BAP", getName()); 
	receivers.push_back(bapReceiver);
	wavReceiver = new Receiver("semaine.data.synthesis.lowlevel.audio", "WAV", getName());
	receivers.push_back(wavReceiver);
	commandReceiver = new Receiver("semaine.data.synthesis.lowlevel.command", "command", getName());
	receivers.push_back(commandReceiver);

	pc.SetTime(meta.getTime());
	lasttime=meta.getTime();

}


PlayerReceiver::~PlayerReceiver()
{
	delete fapReceiver;
	delete bapReceiver;
	delete wavReceiver;
	delete commandReceiver;
}

void PlayerReceiver::react(SEMAINEMessage * m) throw(CMSException)
{
	long long time;
	std::string content;
//	SEMAINEXMLMessage * xm = (SEMAINEXMLMessage *)(m);
	if (xm == NULL) {
		throw MessageFormatException("expected XML message, got "+std::string(typeid(*m).name()));
	}

	time=meta.getTime();
	pc.SetTime(time);

	if(time<lasttime)
	{
		fm->lastbapframe=time;
		fm->lastfapframe=time;
	}
	lasttime=time;

	content=xm->getText();
	bool isFAP = xm->getDatatype() == "FAP";
	bool isBAP = xm->getDatatype() == "BAP";
	bool isWAV = xm->getDatatype() == "WAV";
	bool iscommand = xm->getDatatype() == "command";

	if(isFAP) { 
		fm->AddFAPFrame(content);
	} else 
	if(isBAP) { 
		fm->AddBAPFrame(content);
	} else 
	if(isWAV) { 
		RealTimeAudio::load_wave(msg->getData(),msg->getSize());
	} else 
	if(iscommand) { 
		std::string command;
		command=content.c_str();
		cm->AddCommand(command.substr(0,command.find_first_of(" ")),
			command.substr(command.find_first_of(" ")+1));
	}
}

} // namespace greta
} // namespace components
} // namespace semaine