//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// HeadGestuary.cpp: implementation of the  HeadGestuary class.
//
//////////////////////////////////////////////////////////////////////

#include "HeadGestuary.h"
#include "HeadGesture.h"

using namespace HeadSpace;

HeadGestuary::HeadGestuary(){}

HeadGestuary::~HeadGestuary(){
	gestuary.clear();
}

std::map<std::string,HeadGesture> HeadGestuary::getGestuary(){return gestuary;}

int HeadGestuary::LoadGestuary(std::string gestuaryfilename)
{
	XMLDOMParser p;
	XMLGenericTree *t;
	bool found_stroke;

	p.SetValidating(true);
	t=p.ParseFile(gestuaryfilename);
	if(t==0)
	{
		delete t;
		return 0;
	}

	XMLGenericTree *chld;
//	std::list<XMLGenericTree*>::iterator iter;

//	for(iter=t->child.begin();iter!=t->child.end();iter++)
	for(XMLGenericTree::iterator iter=t->begin();iter!=t->end();++iter)
	{
		chld = *iter;
		if (chld->isTextNode()) continue;

		found_stroke=false;

		HeadGesture tg(chld->GetAttribute("id"));

		XMLGenericTree *headnode;
		headnode=chld->FindNodeCalled("head");
		if(headnode==NULL) continue;
//		std::list<XMLGenericTree*>::iterator iter2;
//		for(iter2=headnode->child.begin();iter2!=headnode->child.end();iter2++)
		XMLGenericTree *hc;
		for(XMLGenericTree::iterator hc_iter=headnode->begin();
			hc_iter!=headnode->end();
			++hc_iter)
		{
			hc = *hc_iter;
			if (hc->isTextNode()) continue;

			HeadGesturePhase tp;
			if(!hc->HasAttribute("phase")) continue;
			tp.type=tp.GetPhaseId(hc->GetAttribute("phase"));
			if(tp.type==stroke)
				found_stroke=true;
			if(tp.type==unknown_phase)
			{		

			//#ifdef OUTPUT	
				printf("HeadEngine::Error unknown phase %s in gesture %s\n",
					hc->GetAttribute("phase").c_str(),tg.id.c_str());
		//	#endif
			}
			else
			{
				if(tp.type==preparation)
				{
				}
//				std::list<XMLGenericTree*>::iterator iter3;
//				for(iter3=(*iter2)->child.begin();iter3!=(*iter2)->child.end();iter3++)
				XMLGenericTree *c3;
				for(XMLGenericTree::iterator c3it = hc->begin(); c3it != hc->end(); ++c3it)
				{
					c3 = *c3it;
					if (c3->isTextNode()) continue;

					if(c3->GetName()=="timing")
					{
						tp.Tdefault=c3->GetAttributef("default");
						tp.Tmin=c3->GetAttributef("min");
						tp.Tmax=c3->GetAttributef("max");
						tp.Tduration=c3->GetAttributef("duration");
						tp.Tfixed=(int)c3->GetAttributef("fixed");
					}
					if(c3->GetName()=="point")
					{
						tp.AddPoint((int)(c3->GetAttributef("x")),
							(int)(c3->GetAttributef("y")),
							(int)(c3->GetAttributef("z")));
					}
				}
				tg.phases.push_back(tp);
			}
		}
		if(found_stroke==true)
			this->gestuary[tg.id]=tg;
		else
		{
	//#ifdef OUTPUT
			printf("HeadEngine::Warning Skipping gesture %s (no stroke phase found)\n",tg.id.c_str());
	//#endif
		}
	}

	delete t;
	return 1;
}
