package demo;

import java.util.*;
import java.io.*;

/**
 * Class corresponding to a database of predefined demo (predefined BML or FML file) 
 * @author Magalie
 *
 */
public class DataBaseDemo {

	/* List of the existing demo */
	private List <DemoExample>_demo;
	
	public DataBaseDemo(){
		_demo=new ArrayList<DemoExample>();
		this.constructExample();
	}
	
	public void addExample(DemoExample e){
		this._demo.add(e);
		
	}
	
	/**
	 * Method that adds the demo to the list of demo
	 */
	public void constructExample(){

		DemoExample thisCity=new DemoExample("Talk about a city","this-city", true);
		String bmlThisCity=lectureFichier("demoBML/thiscity-bmlPourJava.xml");
		thisCity.setBmlFmlText(bmlThisCity);
		thisCity.settextTellByGreta("This city in Szechuan was completely devastated by the earthquake in May two thousands and eight Very few buildings survived intact");
		this._demo.add(thisCity);
		
		
		
		DemoExample whatAreYouDoing=new DemoExample("I should not be here","what-are-you-doing", true);
		String bmlwhatAreYouDoing=lectureFichier("demoBML/WhatAreYouDoingHere.xml");
		whatAreYouDoing.settextTellByGreta("what are you doing here? did you pay attention to what I said or not? you should take all of your stuff and get out of this place immediately.");
		whatAreYouDoing.setBmlFmlText(bmlwhatAreYouDoing);
		
		this._demo.add(whatAreYouDoing);
		

		DemoExample threePieces=new DemoExample("Tell me a poem","threePiecesOfSky", true);
		String bmlthreePieces=lectureFichier("demoBML/bml_threePiecesOfSky.xml");
		threePieces.settextTellByGreta("A long time ago, a spring evening, three little pieces of night broke away from the sky and fall down on the earth.");
		threePieces.setBmlFmlText(bmlthreePieces);
		
		this._demo.add(threePieces);
		
		DemoExample OneTwoThree=new DemoExample("count","One-two-three", true);
		String bmlOneTwoThree=lectureFichier("demoBML/123.xml");
		OneTwoThree.settextTellByGreta("Yes of course one two three that s all");
		OneTwoThree.setBmlFmlText(bmlOneTwoThree);
		
		this._demo.add(OneTwoThree);
		
		/* Demo on emotion */
		
		DemoExample angerS=new DemoExample("anger","angerS", false);
		String fmlangerS=lectureFichier("demoBML/angerS.xml");
		angerS.settextTellByGreta("I'm so angry!");
		angerS.setBmlFmlText(fmlangerS);
		
		this._demo.add(angerS);
		
		DemoExample panic_fearS=new DemoExample("panic-fear","panic_fearS", false);
		String fmlpanic_fearS = lectureFichier("demoBML/panicFearS.xml");
		panic_fearS.settextTellByGreta("I'm afraid!");
		panic_fearS.setBmlFmlText(fmlpanic_fearS);
		
		this._demo.add(panic_fearS);
		
		DemoExample prideS=new DemoExample("pride","prideS", false);
		String fmlprideS = lectureFichier("demoBML/prideS.xml");
		prideS.settextTellByGreta("I'm proud!");
		prideS.setBmlFmlText(fmlprideS);
		
		this._demo.add(prideS);
		
		
		DemoExample joyS=new DemoExample("joy","joyS", false);
		String fmljoyS = lectureFichier("demoBML/joyS.xml");
		joyS.settextTellByGreta("I'm so happy!");
		joyS.setBmlFmlText(fmljoyS);
		
		this._demo.add(joyS);
		
		DemoExample anxietyS=new DemoExample("anxiety","anxietyS", false);
		String fmlanxietyS = lectureFichier("demoBML/anxietyS.xml");
		anxietyS.settextTellByGreta("I feel anxiety");
		anxietyS.setBmlFmlText(fmlanxietyS);
		
		this._demo.add(anxietyS);
		
		DemoExample reliefS=new DemoExample("relief","reliefS", false);
		String fmlreliefS = lectureFichier("demoBML/reliefS.xml");
		reliefS.settextTellByGreta("I feel better now");
		reliefS.setBmlFmlText(fmlreliefS);
		
		this._demo.add(reliefS);
		
		
		
	}

	
	/**
	 * Method that return the demo to execute given a keyword, null if no demo exist with this keyword 
	 */
	public DemoExample getDemoToExecute(String messageProgramD){
		for(int i=0;i<this._demo.size();i++){
			if(messageProgramD.contains((this._demo.get(i)).getTextProgrammDTrigger())){
				return this._demo.get(i);
			}
		}
		
		return null;
	}
	
	/**
	 * Method that reads the file of the demo
	 * @param nomFichier name of the file 
	 * @return the BML message
	 */
	public String lectureFichier(String nomFichier){
			StringBuffer bmlText = new StringBuffer();
			String fichier = nomFichier;
			
			try{
				InputStream ips=new FileInputStream(fichier); 
				InputStreamReader ipsr=new InputStreamReader(ips);
				BufferedReader br=new BufferedReader(ipsr);
				String ligne;
				while ((ligne=br.readLine())!=null){
					bmlText.append(ligne);
				}
				br.close(); 
			}		
			catch (Exception e){
				System.out.println(e.toString());
			}
			
			return bmlText.toString();
	}
	
}
