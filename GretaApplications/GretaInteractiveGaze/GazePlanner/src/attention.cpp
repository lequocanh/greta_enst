// Shared-Attention system
// Christopher Peters

#include "attention.h"

VAO::VAO(Named3DObject *named3dobj, bool outsideScreen)
{
	this->lookatEntries.clear();
	this->totalTimeLookedAt = 0;
	
	this->TimeLookedAt = 0;
	
	this->named3dobj = named3dobj;
	this->vas = NULL;
	this->outsideScreen = outsideScreen;
}

VAO::~VAO(void)
{


}
///!!!!!
void VAO::update(long time, bool sameVAOAsPreviousFixation)
{
	int numEntries = this->lookatEntries.size();
	//if it is the same one as previously, then we don't need to make a new AttentionEntry
	if (numEntries > 0 && sameVAOAsPreviousFixation)
	{
		//printf("\nSame VAO as previously selected");
		this->totalTimeLookedAt += time - this->lookatEntries[numEntries - 1]->endTime;
		
		this->TimeLookedAt += time - this->lookatEntries[numEntries - 1]->endTime;
		this->lookatEntries[numEntries - 1]->endTime = time;
	}
	else
	{
		//printf("\nNot same VAO as previously selected: making new attention entry");
		AttentionEntry *ae = new AttentionEntry;
		ae->atUser = false;
		ae->vao = this;
		ae->outsideScreen = this->outsideScreen;
		ae->startTime = time;
		ae->endTime = time;
		ae->whereOutside = -1;
		//a single glance counts as less time looked at
		this->lookatEntries.push_back(ae);
	}
	if (this->named3dobj != NULL) printf("\nUpdated VAO for 3D object (%d) %s: total time looked at: %d", this->named3dobj->objectID, this->named3dobj->name->c_str(), this->totalTimeLookedAt);
	else printf("\nUpdated VAO for *OUTSIDE* 3D object");

}