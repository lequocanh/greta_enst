#include "PlayerOgreCamera.h"


PlayerOgreCamera::PlayerOgreCamera(bool movable, Ogre::SceneManager *sceneManager, Ogre::SceneNode *parent, Ogre::String name){
	ismovable = movable;
	mCamera = sceneManager->createCamera(name);
	mCamera->setNearClipDistance(1);
	yawNode = parent->createChildSceneNode(name+"_yawNode",Ogre::Vector3(0,0,0));
	pitchNode = yawNode->createChildSceneNode(name+"_pitchNode",Ogre::Vector3(0,0,0));
	pitchNode->attachObject(mCamera);
	mCamera->setFOVy(Ogre::Degree(30)); //Field of view
	
}
PlayerOgreCamera::~PlayerOgreCamera(){
}
void PlayerOgreCamera::resetPosNRot(){
	mCamera->setPosition(0,0,0);
	Ogre::Quaternion q(Ogre::Degree(0),Ogre::Vector3(1,0,0));
	mCamera->setOrientation(q);
	pitchNode->setOrientation(q);
	yawNode->setOrientation(q);
}

