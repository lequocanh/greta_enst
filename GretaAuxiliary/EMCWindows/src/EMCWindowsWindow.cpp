//Copyright 1999-2009 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330index, Boston, MA  02111-1307  USA

// EMCWindowsWindow.cpp: implementation of the EMCWindowsWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "EMCWindowsWindow.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <list>
#include <string>
#include <fl/fl_widget.h>
#include "IniManager.h"

#include "OptiTrackConverter.h"

extern IniManager inimanager;

OptiTrackConverter con9;

void setCB(Fl_Widget*w, void* v)
{
	int i;
	
	for(i=3;i<65;i++)
	{
		con9.mask[i]=0;

		if(i!=27 && i!=28 && i!=29 && i!=30)
		{
			((EMCWindowsWindow*)v)->fapcounter[i]->value(con9.mask[i]);

				char str[40];
				float f = con9.mask[i];
				sprintf(str, "%f", f);				
			((EMCWindowsWindow*)v)->lframe[i]->value(str);
		}
	}

}


extern FILE *face_log;

void selectedmenu(Fl_Widget* w, void* v)
{
	

	if(strcmp(((EMCWindowsWindow*)v)->menu->text(),"Save Schema")==0)
	{
		printf("saveschema");
		((EMCWindowsWindow*)v)->SaveSchema();
		
	}
	if(strcmp(((EMCWindowsWindow*)v)->menu->text(),"Load Schema")==0)
	{
		printf("load schema");

		std::string filename = ((EMCWindowsWindow*)v)->LoadSchema();
		
		//con9.readmask(filename);
		//setCB(w,v);
		
	}
	if(strcmp(((EMCWindowsWindow*)v)->menu->text(),"Load FAPfile")==0)
	{
		printf("loadfapfile");

		((EMCWindowsWindow*)v)->LoadFAPFile();
	}
	
	if(strcmp(((EMCWindowsWindow*)v)->menu->text(),"Quit")==0)
	{
		((EMCWindowsWindow*)v)->hide();
		delete ((EMCWindowsWindow*)v);
	}
}




void resetCB(Fl_Widget*w, void* v)
{
	int i;
	
	for(i=3;i<65;i++)
	{
		if(i!=27 && i!=28 && i!=29 && i!=30)
		{
			((EMCWindowsWindow*)v)->fapcounter[i]->value(0);
			((EMCWindowsWindow*)v)->lframe[i]->value("55.00");
		}
	}
	 
	
	for(i=0;i<1;i++)
	{
		((EMCWindowsWindow*)v)->lipscounter[i]->value(0);
		((EMCWindowsWindow*)v)->llips[i]->value("0.00");
	}

	
	((EMCWindowsWindow*)v)->CopyFAPValuesToFAPFrame();
	((EMCWindowsWindow*)v)->instances->value(0);
	((EMCWindowsWindow*)v)->probability->value(0);
	((EMCWindowsWindow*)v)->framenumber->value(0);
	
	//((EMCWindowsWindow*)v)->CopyFAPValuesToFAPFrame();
	((EMCWindowsWindow*)v)->LoadFaceFrame(((EMCWindowsWindow*)v)->fapcounter[i]->value());
}


void selectedclass(Fl_Widget*w, void* v)
{
	
	resetCB(w,v);
	
	((EMCWindowsWindow*)v)->LoadFaceFrame(0);
	((EMCWindowsWindow*)v)->LoadInstances();
}

void selectedinstance(Fl_Widget*w, void* v)
{
	//((EMCWindowsWindow*)v)->CopyFAPValuesToFAPFrame();
//	((EMCWindowsWindow*)v)->LoadFaceFrame(((EMCWindowsWindow*)v)->framenumber->value());
	((EMCWindowsWindow*)v)->LoadFaceFrame(0);
	((EMCWindowsWindow*)v)->UpdateFAPValues(((EMCWindowsWindow*)v)->GetSelectedFAPFrame(0));
}

void changedMOUTHgroup(Fl_Widget*w, void* v)
{	
	int i;
	if(((EMCWindowsWindow*)v)->mouth->value()==1)
	{
		((EMCWindowsWindow*)v)->eyes_eyebrows->value(0);
		((EMCWindowsWindow*)v)->head->value(0);
		((EMCWindowsWindow*)v)->cheek->value(0);
		for(i=3; i<65; i++)
		{
			if((i>=4 && i<18) || (i>=51 && i<61))
			{
				((EMCWindowsWindow*)v)->fapcounter[i]->show();
				((EMCWindowsWindow*)v)->lframe[i]->show();
				((EMCWindowsWindow*)v)->fapcounter[i]->activate();
				((EMCWindowsWindow*)v)->lframe[i]->activate();
			}
			else
			{
				if(i!=27 && i!=28 && i!=29 && i!=30)
				{
					((EMCWindowsWindow*)v)->fapcounter[i]->hide();
					((EMCWindowsWindow*)v)->lframe[i]->hide();
					((EMCWindowsWindow*)v)->fapcounter[i]->deactivate();
					((EMCWindowsWindow*)v)->lframe[i]->deactivate();
				}
			}
		}
	}
	else
		((EMCWindowsWindow*)v)->mouth->value(1);
	((EMCWindowsWindow*)v)->redraw();

	//if (((EMCWindowsWindow*)v)->IndexB!=0)
	//	((EMCWindowsWindow*)v)->fapcounter[((EMCWindowsWindow*)v)->IndexB]->value(atof(((EMCWindowsWindow*)v)->lframe[((EMCWindowsWindow*)v)->IndexB]->value()));
}

void changedEYESgroup(Fl_Widget*w, void* v)
{	
	int i;
	if(((EMCWindowsWindow*)v)->eyes_eyebrows->value()==1)
	{
		((EMCWindowsWindow*)v)->mouth->value(0);
		((EMCWindowsWindow*)v)->head->value(0);
		((EMCWindowsWindow*)v)->cheek->value(0);
		for(i=3; i<65; i++)
		{
			if((i>=19 && i<27) || (i>=31 && i<39))
			{
				((EMCWindowsWindow*)v)->fapcounter[i]->show();
				((EMCWindowsWindow*)v)->lframe[i]->show();
				((EMCWindowsWindow*)v)->fapcounter[i]->activate();
				((EMCWindowsWindow*)v)->lframe[i]->activate();
			}
			else
			{
				if(i!=27 && i!=28 && i!=29 && i!=30)
				{
					((EMCWindowsWindow*)v)->fapcounter[i]->hide();
					((EMCWindowsWindow*)v)->lframe[i]->hide();
					((EMCWindowsWindow*)v)->fapcounter[i]->deactivate();
					((EMCWindowsWindow*)v)->lframe[i]->deactivate();
				}
			}
		}
	}
	else
		((EMCWindowsWindow*)v)->eyes_eyebrows->value(1);
	((EMCWindowsWindow*)v)->redraw();

	//if (((EMCWindowsWindow*)v)->IndexB!=0)
	//	((EMCWindowsWindow*)v)->fapcounter[((EMCWindowsWindow*)v)->IndexB]->value(atof(((EMCWindowsWindow*)v)->lframe[((EMCWindowsWindow*)v)->IndexB]->value()));
}

void changedHEADgroup(Fl_Widget*w, void* v)
{	
	int i;
	if(((EMCWindowsWindow*)v)->head->value()==1)
	{
		((EMCWindowsWindow*)v)->eyes_eyebrows->value(0);
		((EMCWindowsWindow*)v)->mouth->value(0);
		((EMCWindowsWindow*)v)->cheek->value(0);
		for(i=3; i<65; i++)
		{
			if((i>=48 && i<51))
			{
				((EMCWindowsWindow*)v)->fapcounter[i]->show();
				((EMCWindowsWindow*)v)->lframe[i]->show();
				((EMCWindowsWindow*)v)->fapcounter[i]->activate();
				((EMCWindowsWindow*)v)->lframe[i]->activate();
			}
			else
			{
				if(i!=27 && i!=28 && i!=29 && i!=30)
				{
					((EMCWindowsWindow*)v)->fapcounter[i]->hide();
					((EMCWindowsWindow*)v)->lframe[i]->hide();
					((EMCWindowsWindow*)v)->fapcounter[i]->deactivate();
					((EMCWindowsWindow*)v)->lframe[i]->deactivate();
				}
			}
		}
	}
	else
		((EMCWindowsWindow*)v)->head->value(1);
	((EMCWindowsWindow*)v)->redraw();
	//if (((EMCWindowsWindow*)v)->IndexB!=0)
	//	((EMCWindowsWindow*)v)->fapcounter[((EMCWindowsWindow*)v)->IndexB]->value(atof(((EMCWindowsWindow*)v)->lframe[((EMCWindowsWindow*)v)->IndexB]->value()));
}

void changedCHEEKgroup(Fl_Widget*w, void* v)
{	
	int i;
	if(((EMCWindowsWindow*)v)->cheek->value()==1)
	{
		((EMCWindowsWindow*)v)->eyes_eyebrows->value(0);
		((EMCWindowsWindow*)v)->mouth->value(0);
		((EMCWindowsWindow*)v)->head->value(0);
		for(i=3; i<65; i++)
		{
			if((i==3) || (i==18) || (i>=39 && i<48) || (i>=61 && i<65))
			{
				((EMCWindowsWindow*)v)->fapcounter[i]->show();
				((EMCWindowsWindow*)v)->lframe[i]->show();
				((EMCWindowsWindow*)v)->fapcounter[i]->activate();
				((EMCWindowsWindow*)v)->lframe[i]->activate();
			}
			else
			{
				if(i!=27 && i!=28 && i!=29 && i!=30)
				{
					((EMCWindowsWindow*)v)->fapcounter[i]->hide();
					((EMCWindowsWindow*)v)->lframe[i]->hide();
					((EMCWindowsWindow*)v)->fapcounter[i]->deactivate();
					((EMCWindowsWindow*)v)->lframe[i]->deactivate();
				}
			}
		}
	}
	else
		((EMCWindowsWindow*)v)->cheek->value(1);
	((EMCWindowsWindow*)v)->redraw();
	//if (((EMCWindowsWindow*)v)->IndexB!=0)
	//	((EMCWindowsWindow*)v)->fapcounter[((EMCWindowsWindow*)v)->IndexB]->value(atof(((EMCWindowsWindow*)v)->lframe[((EMCWindowsWindow*)v)->IndexB]->value()));
}


void changedFAPvalue(Fl_Widget*w, void* v)
{

	char value[255];
	
	if (((EMCWindowsWindow*)v)->Index!=0)
	{
		sprintf(value,"%.2f",((EMCWindowsWindow*)v)->fapcounter[((EMCWindowsWindow*)v)->Index]->value());

	   //printf("%s",value);

		((EMCWindowsWindow*)v)->lframe[((EMCWindowsWindow*)v)->Index]->value(value);
	}


	((EMCWindowsWindow*)v)->CopyFAPValuesToFAPFrame();
	//((EMCWindowsWindow*)v)->LoadFaceFrame(((EMCWindowsWindow*)v)->framenumber->value());
}

void changeValueFAP(Fl_Widget*w, void* v)
{	
	
	if (((EMCWindowsWindow*)v)->IndexB!=0)
		((EMCWindowsWindow*)v)->fapcounter[((EMCWindowsWindow*)v)->IndexB]->value(atof(((EMCWindowsWindow*)v)->lframe[((EMCWindowsWindow*)v)->IndexB]->value()));

	((EMCWindowsWindow*)v)->CopyFAPValuesToFAPFrame();
	//((EMCWindowsWindow*)v)->LoadFaceFrame(((EMCWindowsWindow*)v)->framenumber->value());
}

 

void changedLipParametersvalue(Fl_Widget*w, void* v)
{
	
	int i, ind;
	char value[255];
	float coef=1.0f;
	
	if (((EMCWindowsWindow*)v)->IndexLIPS!=-1)
	{
		sprintf(value,"%.2f",((EMCWindowsWindow*)v)->lipscounter[((EMCWindowsWindow*)v)->IndexLIPS]->value());
		((EMCWindowsWindow*)v)->llips[((EMCWindowsWindow*)v)->IndexLIPS]->value(value);
	}
	
	
	((EMCWindowsWindow*)v)->glutw->getAgent()->SetCurrentFrame(atoi(value));	

 	((EMCWindowsWindow*)v)->currentframe=atoi(value);
	
}
 

void changedframenumber(Fl_Widget*w, void* v)
{
	((EMCWindowsWindow*)v)->LoadFaceFrame(((EMCWindowsWindow*)v)->framenumber->value());
	((EMCWindowsWindow*)v)->UpdateFAPValues(((EMCWindowsWindow*)v)->GetSelectedFAPFrame(((EMCWindowsWindow*)v)->framenumber->value()));
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

EMCWindowsWindow::EMCWindowsWindow():Fl_Window(10,20,905,690,"EMCTOOL")
{
	int i;

	maskfilename="";

	fapfilename="";

	totalnumberframes=0;
	currentframe=1;

	this->size_range(375,250,905,690,1,1,0);
	
	face_log=fopen("logs/face_dictionary_log.txt","w");

	menu=new EMCWindowsMenu(0,0,100,20);

	menu->add("File/Load FAPfile",0,(Fl_Callback *)selectedmenu,this,0);
	
	menu->add("File/Load Schema",0,(Fl_Callback *)selectedmenu, this, 0);

	menu->add("File/Save Schema",0,(Fl_Callback *)selectedmenu, this, 0);
	
	menu->add("File/Quit",0,(Fl_Callback *)selectedmenu,this,0);
	
	
	menu->box(FL_NO_BOX);

	boxFAPS = new Fl_Box(FL_BORDER_BOX, 10, 100, 390, 340,"FAPS");
	boxFAPS->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);

	bounds[3][0]=-10; bounds[3][1]=10;
	bounds[4][0]=-10; bounds[4][1]=10;
	bounds[5][0]=-10; bounds[5][1]=10;
	bounds[6][0]=-10; bounds[6][1]=10;
	bounds[7][0]=-10; bounds[7][1]=10;
	bounds[8][0]=-10; bounds[8][1]=10;
	bounds[9][0]=-10; bounds[9][1]=10;
	bounds[10][0]=-10; bounds[10][1]=10;
	bounds[11][0]=-10; bounds[11][1]=10;
	bounds[12][0]=-10; bounds[12][1]=10;
	bounds[13][0]=-10; bounds[13][1]=10;
	bounds[14][0]=-10; bounds[14][1]=10;
	bounds[15][0]=-10; bounds[15][1]=10;
	bounds[16][0]=-10; bounds[16][1]=10;
	bounds[17][0]=-10; bounds[17][1]=10;
	bounds[18][0]=-10; bounds[18][1]=10;
	bounds[19][0]=-10; bounds[19][1]=10;
	bounds[20][0]=-10; bounds[20][1]=10;
	bounds[21][0]=-10; bounds[21][1]=10;
	bounds[22][0]=-10; bounds[22][1]=10;
	bounds[23][0]=-10; bounds[23][1]=10;
	bounds[24][0]=-10; bounds[24][1]=10;
	bounds[25][0]=-10; bounds[25][1]=10;
	bounds[26][0]=-10; bounds[26][1]=10;
	bounds[27][0]=-10; bounds[27][1]=10;
	bounds[28][0]=-10; bounds[28][1]=10;
	bounds[29][0]=-10; bounds[29][1]=10;
	bounds[30][0]=-10; bounds[30][1]=10;    
	bounds[31][0]=-10; bounds[31][1]=10;
	bounds[32][0]=-10; bounds[32][1]=10;
	bounds[33][0]=-10; bounds[33][1]=10;
	bounds[34][0]=-10; bounds[34][1]=10;
	bounds[35][0]=-10; bounds[35][1]=10;
	bounds[36][0]=-10; bounds[36][1]=10;
	bounds[37][0]=-10; bounds[37][1]=10;
	bounds[38][0]=-10; bounds[38][1]=10;
	bounds[39][0]=-10; bounds[39][1]=10;
	bounds[40][0]=-10; bounds[40][1]=10;
	bounds[41][0]=-10; bounds[41][1]=10;
	bounds[42][0]=-10; bounds[42][1]=10;
	bounds[43][0]=-10; bounds[43][1]=10;
	bounds[44][0]=-10; bounds[44][1]=10;
	bounds[45][0]=-10; bounds[45][1]=10;
	bounds[46][0]=-10; bounds[46][1]=10;
	bounds[47][0]=-10; bounds[47][1]=10;
	bounds[48][0]=-10; bounds[48][1]=10;
	bounds[49][0]=-10; bounds[49][1]=10;
	bounds[50][0]=-10; bounds[50][1]=10;
	bounds[51][0]=-10; bounds[51][1]=10;
	bounds[52][0]=-10; bounds[52][1]=10;
	bounds[53][0]=-10; bounds[53][1]=10;
	bounds[54][0]=-10; bounds[54][1]=10;
	bounds[55][0]=-10; bounds[55][1]=10;
	bounds[56][0]=-10; bounds[56][1]=10;
	bounds[57][0]=-10; bounds[57][1]=10;
	bounds[58][0]=-10; bounds[58][1]=10;
	bounds[59][0]=-10; bounds[59][1]=10;
	bounds[60][0]=-10; bounds[60][1]=10;
	bounds[61][0]=-10; bounds[61][1]=10;
	bounds[62][0]=-10; bounds[62][1]=10;
	bounds[63][0]=-10; bounds[63][1]=10;
	bounds[64][0]=-10; bounds[64][1]=10;
	bounds[65][0]=0; bounds[65][1]=0;
	bounds[66][0]=0; bounds[66][1]=0;
	bounds[67][0]=0; bounds[67][1]=0;
	bounds[68][0]=0; bounds[68][1]=0;

	int x;

	for(i=4;i<61;i++)
	{
		if((i>3&&i<18)||(i>50&&i<61))
		{
			if(i>3&&i<12)
			{
				fapcounter[i]=new Fl_Slider(20,120+(i-4)*42,70,16,GetLabel(i));
				lframe[i]=new Fl_Input(91, 120+(i-4)*42,40,16, "");
			}
			else
				if(i>11&&i<53)
				{
					if(i>11&&i<18)
						x=12;
					else
						x=45;
					fapcounter[i]=new Fl_Slider(143,120+(i-x)*42,70,16,GetLabel(i));
					lframe[i]=new Fl_Input(214, 120+(i-x)*42,40,16, "");
				}
				else
					if(i>52&&i<61)
					{
						fapcounter[i]=new Fl_Slider(267,120+(i-53)*42,70,16,GetLabel(i));
						lframe[i]=new Fl_Input(338, 120+(i-53)*42,40,16, "");
					}
			
			fapcounter[i]->type(FL_HOR_NICE_SLIDER);
			fapcounter[i]->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
			fapcounter[i]->bounds(bounds[i][0],bounds[i][1]);
			fapcounter[i]->step(0.1);
			fapcounter[i]->value(0.0);
			
			lframe[i]->textsize(10);
			lframe[i]->value("0.00");
			fapcounter[i]->callback((Fl_Callback *)changedFAPvalue,this);
			lframe[i]->callback((Fl_Callback *)changeValueFAP,this);
		}
	}

	for(i=3;i<65;i++)
	{
		if((i==3) || (i==18) || (i>38&&i<48) || (i>60&&i<65))
		{
			if((i==3) || (i==18) || (i>38&&i<42))
			{
				if(i==3)
					x=3;
				else
				if(i==18)
					x=17;
				else
				if(i>38&&i<42)
					x=37;
				fapcounter[i]=new Fl_Slider(20,120+(i-x)*42,70,16,GetLabel(i));
				lframe[i]=new Fl_Input(91, 120+(i-x)*42,40,16, "");
			}
			else
				if(i>41&&i<47)
				{
					fapcounter[i]=new Fl_Slider(143,120+(i-42)*42,70,16,GetLabel(i));
					lframe[i]=new Fl_Input(214, 120+(i-42)*42,40,16, "");
				}
				else
					if((i==47) || (i>60&&i<65))
					{
						if(i==47)
							x=47;
						else
							x=60;
						fapcounter[i]=new Fl_Slider(267,120+(i-x)*42,70,16,GetLabel(i));
						lframe[i]=new Fl_Input(338, 120+(i-x)*42,40,16, "");
					}
			
			fapcounter[i]->type(FL_HOR_NICE_SLIDER);
			fapcounter[i]->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
			fapcounter[i]->bounds(bounds[i][0],bounds[i][1]);
			fapcounter[i]->step(0.1);
			
			lframe[i]->textsize(10);
			lframe[i]->value("0.00");
			fapcounter[i]->callback((Fl_Callback *)changedFAPvalue,this);
			lframe[i]->callback((Fl_Callback *)changeValueFAP,this);
		}
	}

	for(i=19;i<39;i++)
	{
		if((i>18&&i<27) || (i>30&&i<39))
		{
			if(i>18&&i<25)
			{
				fapcounter[i]=new Fl_Slider(20,120+(i-19)*42,70,16,GetLabel(i));
				lframe[i]=new Fl_Input(91, 120+(i-19)*42,40,16, "");
			}
			else
				if((i>24&&i<27) || (i>30&&i<35))
				{
					if(i>24&&i<27)
						x=25;
					else
						x=29;
					fapcounter[i]=new Fl_Slider(143,120+(i-x)*42,70,16,GetLabel(i));
					lframe[i]=new Fl_Input(214, 120+(i-x)*42,40,16, "");
				}
				else
					if(i>34&&i<39)
					{
						fapcounter[i]=new Fl_Slider(267,120+(i-35)*42,70,16,GetLabel(i));
						lframe[i]=new Fl_Input(338, 120+(i-35)*42,40,16, "");
					}
			
			fapcounter[i]->type(FL_HOR_NICE_SLIDER);
			fapcounter[i]->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
			fapcounter[i]->bounds(bounds[i][0],bounds[i][1]);
			fapcounter[i]->step(0.1);
			
			lframe[i]->textsize(10);
			lframe[i]->value("0.00");
			fapcounter[i]->callback((Fl_Callback *)changedFAPvalue,this);
			lframe[i]->callback((Fl_Callback *)changeValueFAP,this);
		}
	}

	for(i=48;i<51;i++)
	{
		if(i==48)
		{
			fapcounter[i]=new Fl_Slider(20,120+(i-48)*42,70,16,GetLabel(i));
			lframe[i]=new Fl_Input(91, 120+(i-48)*42,40,16, "");
		}
		else
			if(i==49)
			{
				fapcounter[i]=new Fl_Slider(143,120+(i-49)*42,70,16,GetLabel(i));
				lframe[i]=new Fl_Input(214, 120+(i-49)*42,40,16, "");
			}
			else
				if(i==50)
				{
					fapcounter[i]=new Fl_Slider(267,120+(i-50)*42,70,16,GetLabel(i));
					lframe[i]=new Fl_Input(338, 120+(i-50)*42,40,16, "");
				}
		
		fapcounter[i]->type(FL_HOR_NICE_SLIDER);
		fapcounter[i]->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
		fapcounter[i]->bounds(bounds[i][0],bounds[i][1]);
		fapcounter[i]->step(0.1);
		
		lframe[i]->textsize(10);
		lframe[i]->value("0.00");
		fapcounter[i]->callback((Fl_Callback *)changedFAPvalue,this);
		lframe[i]->callback((Fl_Callback *)changeValueFAP,this);
	}

	for(i=3;i<65;i++)
	{
		if((i>=4 && i<18) || (i>=51 && i<61))
		{
			lframe[i]->show();
			fapcounter[i]->show();
		}
		else
		{
			if(i!=27 && i!=28 && i!=29 && i!=30)
			{
				lframe[i]->hide();
				fapcounter[i]->hide();
			}
		}
	}

	mouth=new Fl_Round_Button(10,40,16,16,"Mouth");
	mouth->align(FL_ALIGN_RIGHT);
	mouth->callback((Fl_Callback *)changedMOUTHgroup,this);
	mouth->value(1);
	
	eyes_eyebrows=new Fl_Round_Button(10,60,16,16,"Eyes and Eyebrows");
	eyes_eyebrows->align(FL_ALIGN_RIGHT);
	eyes_eyebrows->callback((Fl_Callback *)changedEYESgroup,this);

	head=new Fl_Round_Button(170,60,16,16,"Head");
	head->align(FL_ALIGN_RIGHT);
	head->callback((Fl_Callback *)changedHEADgroup,this);

	cheek=new Fl_Round_Button(170,40,16,16,"Cheeks, Jaw, Tongue and Nose");
	cheek->align(FL_ALIGN_RIGHT);
	cheek->callback((Fl_Callback *)changedCHEEKgroup,this);

	//LIP PARAMETERS
	 			
		lipscounter[0]=new Fl_Slider(30,505+ 42,600,56,"nr");		 
		lipscounter[0]->type(FL_HOR_NICE_SLIDER);
		lipscounter[0]->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
		lipscounter[0]->bounds(0, totalnumberframes);		
		lipscounter[0]->step(0.1);
		llips[0]=new Fl_Input(631, 505+42,40,56, "");
		llips[0]->textsize(10);
		llips[0]->value("0.00");
		//llips[0]->deactivate();
		lipscounter[0]->callback((Fl_Callback *)changedLipParametersvalue,this);
		 
	
#ifdef PLAYEROGRE
	inimanager.SetValueInt("PLAYER_ONLYFACE",1);
	inimanager.SetValueInt("PLAYER_TERRAIN",0);
#endif

	glutw=new Viewer(420,20,470,400,"test");
	
	reset = new Fl_Button(620, 430, 80, 25, "Reset");
	reset->callback((Fl_Callback *)resetCB,this);

	classes=new Fl_Choice(440,480,100,20,"class:");
	classes->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);
	
	instances=new Fl_Choice(570,480,100,20,"instances:");
	instances->align(FL_ALIGN_TOP | FL_ALIGN_LEFT);

	framenumber=new Fl_Counter(700,480,90,20,"variety of exp:");

	framenumber->type(FL_SIMPLE_COUNTER);
	framenumber->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
	framenumber->step(1);

	framenumber->bounds(0,0);

	framenumber->callback((Fl_Callback *)changedframenumber,this);

	probability=new Fl_Value_Output(820,480,50,20,"prob:");
	probability->align(FL_ALIGN_LEFT | FL_ALIGN_TOP);
	
	//LoadLibrary();

	position_x=0;
	position_y=0;
	width=500;
	height=200;
	
}

EMCWindowsWindow::~EMCWindowsWindow()
{
	fclose(face_log);
}


int EMCWindowsWindow::handle(int event)
{
	int i;
	if(Fl::event_button()==FL_LEFT_MOUSE)
	{
		if(event==FL_PUSH)
		{
			for (i=3; i<65; i++)
			{
				if(i!=27 && i!=28 && i!=29 && i!=30)
				{
					if(Fl::event_inside(fapcounter[i]))
					{
						if(fapcounter[i]->visible())
						{
							Index=i;
							break;
						}
					}
					else
						Index=0;
					
					if(Fl::event_inside(lframe[i]))
					{
						if(lframe[i]->visible())
						{
							IndexB=i;
							break;
						}
					}
					else
						IndexB=0;	
				}
			}
			 
			for (i=0; i<1; i++)
			{
				if(Fl::event_inside(lipscounter[i]))
				{
					IndexLIPS=i;
					break;
				}
				else
					IndexLIPS=-1;
			}
		}
	}

	return Fl_Window::handle(event);
}

void EMCWindowsWindow::LoadInstances()
{	
	this->instances->clear();
	std::string s;
	s=classes->text();
	std::list<FaceExpression>::iterator iter;
	instances->add("",0,(Fl_Callback *)selectedinstance,this,0);
	for(iter=facelibrary.FExpressions.begin();iter!=facelibrary.FExpressions.end();iter++)
	{
		if(s==(*iter).classname)
			instances->add((*iter).instance.c_str(),0,(Fl_Callback *)selectedinstance,this,0);
	}
	instances->value(0);

}

void EMCWindowsWindow::LoadFaceFrame(int n)
{
	FaceExpression *selectedexpression;
	FAPFrame *frame;
	
	selectedexpression=GetSelectedExpression();


	frame=GetSelectedFAPFrame(n);

	if(frame!=0)
	{
		glutw->getAgent()->LoadFAPFrame(frame);
		framenumber->value(n);
		framenumber->range(0,selectedexpression->GetNumberOfFrames()-1);
		probability->value(frame->probability);
	}
}

void EMCWindowsWindow::UpdateFAPValues(FAPFrame *f)
{
	char value[256];

	int i;
	if(f!=NULL)
	{
		
		 
	 

		for(i=3; i<65; i++)
		{
			if(i!=27 && i!=28 && i!=29 && i!=30)
			{
				//fapcounter[i]->value(f->FAPs[i].value);
				//fapcounter[i]->set_changed();
				fapcounter[i]->value(((f->FAPs[i]).value));
				sprintf(value,"%.2f",fapcounter[i]->value());
				lframe[i]->value(value);
			}
		}
	}
	//this->redraw();
}


void EMCWindowsWindow::CopyFAPValuesToFAPFrame()
{
	int i;

	//load again animation ?

	glutw->filetoload=fapfilename;
    //glutw->redraw();
	glutw->getAgent()->AssignFile((char*)fapfilename.c_str());
    glutw->filetoload="";
			
	std::vector<FAPFrame>::iterator iter1;

	int index = 0;

	for(iter1=glutw->getAgent()->FAPs->frames.begin();iter1!=glutw->getAgent()->FAPs->frames.end();iter1++)
	{

		for(i=3; i<65; i++)
		{
			if(i!=27 && i!=28 && i!=29 && i!=30)
			{
				 
					int temp = 	((FAPFrame)(*iter1)).GetFAP(i);

					int temp2 = fapcounter[i]->value();

					int temp3=temp*temp2;

					if (temp!=temp3)
					{
	 					iter1->SetFAP(i,(temp3));
	 				    
					}
		 
			}//END IF
		
		}//END FOR
		
		//glutw->getAgent()->LoadFAPFrameIndex(index++);

	}//END FOR
	 	 
	glutw->filetoload="";

	glutw->getAgent()->SetCurrentFrame(currentframe);
	
}//END OF METHOD


std::string EMCWindowsWindow::LoadSchema()
{

	std::string filename="";

	ow = new OpenWin("Load Schema");
		ow->set_modal();
		ow->show();
		while (ow->shown())
			Fl::wait();

	filename = ow->nameEmotion->value();	
	
	//filename="c:\\ely.txt";

	con9.readmask(filename);
	

	for (int i=3;i<65;i++)
	{			
		if(i!=27 && i!=28 && i!=29 && i!=30){

			char *str = (char*)malloc(40);
			
			sprintf(str,"%f",con9.mask[i]);
			
			lframe[i]->value(str);

			fapcounter[i]->value(con9.mask[i]);
	}
	}


	maskfilename=filename;

	CopyFAPValuesToFAPFrame();

	return filename;
	}

int EMCWindowsWindow::LoadFAPFile()
{

	ow = new OpenWin("Load FAPFile WITHOUT .fap");
	ow->set_modal();
	ow->show();
	while (ow->shown())
		Fl::wait();

	std::string filename = ow->nameEmotion->value();	
	
	//for the debug purpose only:
	//filename="c:\\thierry_11";

	//glutw->filetoload=filename.c_str();
	//glutw->redraw();
	glutw->getAgent()->AssignFile((char*)filename.c_str());

	glutw->getAgent()->SetCurrentFrame(1);
	currentframe=1;

	totalnumberframes = glutw->getAgent()->GetTotalNumberOfFrames();

	lipscounter[0]->bounds(0, totalnumberframes);		
	
	glutw->filetoload="";

	fapfilename=filename;

	return 1;
}


FAPFrame * EMCWindowsWindow::GetSelectedFAPFrame(int n)
{
	FAPFrame *frame;

	frame= &(glutw->getAgent()->FAPs->frames[glutw->getAgent()->GetCurrentFrame()]);
		
	return frame;
	
}

void EMCWindowsWindow::SaveSchema()
{

	FILE *fapfile;
 
	if(!(fapfile = fopen(maskfilename.c_str(),"w"))) 
  	{
		printf("ERROR: can't create %s\n",maskfilename.c_str());
		return  ;
  	}
  
   //fap 1
   fprintf(fapfile,"%f ",1.0);
   fprintf(fapfile,"\n");

   //fap 2
   fprintf(fapfile,"%f ",1.0);
   fprintf(fapfile,"\n");

	for (int i=3;i<65;i++)
	{			
		if(i!=27 && i!=28 && i!=29 && i!=30){

				fprintf(fapfile,"%f ",fapcounter[i]->value());
				fprintf(fapfile,"\n");
			
			//lframe[i]->value(str);

			
			}
		else 
		{
		
			fprintf(fapfile,"%f ",1.0);
			fprintf(fapfile,"\n");
		
		}
	}

	//fap 65
   fprintf(fapfile,"%f ",1.0);
   fprintf(fapfile,"\n");

   //fap 66
   fprintf(fapfile,"%f ",1.0);
   fprintf(fapfile,"\n");

   //fap 67
   fprintf(fapfile,"%f ",1.0);
   fprintf(fapfile,"\n");

   //fap 68
    fprintf(fapfile,"%f ",1.0);
    fprintf(fapfile,"\n");

	fclose(fapfile); 

}

FaceExpression * EMCWindowsWindow::GetSelectedExpression()
{
	std::string cl,in;


	FaceExpression *faceexpression;

	faceexpression=0;

	cl=classes->text();
	in=instances->text();

	if((cl!="")&&(in!=""))
		faceexpression=facelibrary.GetExpression(cl,in);

	return NULL;
}


char* EMCWindowsWindow::GetLabel(int fap)
{
	switch(fap)
	{
	case 1: return("1"); break;
	case 2: return("2"); break;
	case 3: return("3   (2.1 y)"); break;
	case 4: return("4   (2.2 y)"); break;
	case 5: return("5   (2.3 y)"); break;
	case 6: return("6   (2.4 x)"); break;
	case 7: return("7   (2.5 x)"); break;
	case 8: return("8   (2.6 y)"); break;
	case 9: return("9   (2.7 y)"); break;
	case 10: return("10   (2.9 y)"); break;
	case 11: return("11   (2.8 y)"); break;
	case 12: return("12   (2.4 y)"); break;
	case 13: return("13   (2.5 y)"); break;
	case 14: return("14   (2.1 z)"); break;
	case 15: return("15   (2.1 x)"); break;
	case 16: return("16   (8.2 z)"); break;
	case 17: return("17   (8.1 z)"); break;
	case 18: return("18   (2.10 y)"); break;
	case 19: return("19   (3.1 y)"); break;
	case 20: return("20   (3.2 y)"); break;
	case 21: return("21   (3.3 y)"); break;
	case 22: return("22   (3.4 y)"); break;
	case 23: return("23   (3.5 x)"); break;
	case 24: return("24   (3.6 x)"); break;
	case 25: return("25   (3.5 y)"); break;
	case 26: return("26   (3.6 y)"); break;
	case 27: return("27   (--)"); break;
	case 28: return("28   (--)"); break;
	case 29: return("29   (pupil sx)"); break;
	case 30: return("30   (pupil dx)"); break;
	case 31: return("31   (4.1 y)"); break;
	case 32: return("32   (4.2 y)"); break;
	case 33: return("33   (4.3 y)"); break;
	case 34: return("34   (4.4 y)"); break;
	case 35: return("35   (4.5 y)"); break;
	case 36: return("36   (4.6 y)"); break;
	case 37: return("37   (4.1 x)"); break;
	case 38: return("38   (4.2 x)"); break;
	case 39: return("39   (5.1 x)"); break;
	case 40: return("40   (5.2 x)"); break;
	case 41: return("41   (5.3 y)"); break;
	case 42: return("42   (5.4 y)"); break;
	case 43: return("43   (6.1 y)"); break;
	case 44: return("44   (6.1 z)"); break;
	case 45: return("45   (6.2 y)"); break;
	case 46: return("46   (6.3 xy)"); break;
	case 47: return("47   (6.4 xy)"); break;
	case 48: return("48   (rotation x)"); break;
	case 49: return("49   (rotation y)"); break;
	case 50: return("50   (rotation z)"); break;
	case 51: return("51   (8.1 y)"); break;
	case 52: return("52   (8.2 y)"); break;
	case 53: return("53   (8.3 x)"); break;
	case 54: return("54   (8.4 x)"); break;
	case 55: return("55   (8.5 y)"); break;
	case 56: return("56   (8.6 y)"); break;
	case 57: return("57   (8.7 y)"); break;
	case 58: return("58   (8.8 y)"); break;
	case 59: return("59   (8.3 y)"); break;
	case 60: return("60   (8.4 y)"); break;
	case 61: return("61   (9.1 x)"); break;
	case 62: return("62   (9.2 x)"); break;
	case 63: return("63   (9.3 y)"); break;
	case 64: return("64   (9.3 x)"); break;
	case 65: return("65   (10.1 y)"); break;
	case 66: return("66   (10.2 x)"); break;
	case 67: return("67   (10.3 y)"); break;
	case 68: return("68   (10.4 x)"); break;
	default: return("");
	}
}