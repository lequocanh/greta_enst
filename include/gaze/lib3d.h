/*****************************************************/
/******************** "lib3d.h" **********************/
/********  Header file for EIKONA3D Library  *********/
/*****************************************************/

#ifndef _IN_LIB3D_H_
#define _IN_LIB3D_H_

/******************************************************************/
/***************** LIBRARY BUILD OPTIONS **************************/
/******************************************************************/

/*
If the libary is built for a Windows platform, include WINDOWS.H
It is assumed that _WINDOWS is defined by default from the compiler
in case of compiling for any Windows platform.
*/
#ifdef _WINDOWS
#include <windows.h>
#endif

/*
Undefine _FLAT_MEMORY_ for Win16 platform.
Define _FLAT_MEMORY_ for Win32 and UNIX platforms.
It is assumed that _WINDOWS is defined by default from the compiler
in case of compiling for any Windows platform.
It is also assumed that WIN32 is defined by default from the compiler
in case of compiling specifically for Win32 platform.
*/
#ifdef _WINDOWS
#ifdef _WIN32
#define _FLAT_MEMORY_
#else
#undef _FLAT_MEMORY_
#endif
#else
#define _FLAT_MEMORY_
#endif

/*
If _FLAT_MEMORY_ is defined, make necessary changes to enable
normal building of the library.
*/
#ifdef _FLAT_MEMORY_
#define far
#define _fmalloc malloc
#define _fcalloc calloc
#define _frealloc realloc
#define _ffree free
#define _fmemcpy memcpy
#define _fstrcpy strcpy
#endif

/*
If _ERROR_CHECKING_ is defined, error checking is performed
normally in all library routines. It is recommended that it is
defined when producing the release version.
This symbol does not affect the checking for memory allocation errors.
This symbol can be undefined for testing procedures, as it makes
the routines to run faster, by replacing the next uncommented
statement with the following code:
#ifdef _ERROR_CHECKING_
#undef _ERROR_CHECKING_
#endif
*/
#define _ERROR_CHECKING_

/*
If _INTERNAL_ONLY_ is defined, error checking is not performed
in the internal library routines and they are defined as static.
This symbol can be undefined to make these routines open to the
users, by replacing the next uncommented statement with 
the following code:
#ifdef _INTERNAL_ONLY_
#undef _INTERNAL_ONLY_
#endif
*/
#define _INTERNAL_ONLY_

/*
If _INTERNAL_ONLY_ is not defined, which means that the internal
routines are open to the users, they should not be defined as static.
For this reason _STATIC_ precedes their definitions to face both cases.
*/
#ifdef _INTERNAL_ONLY_
#define _STATIC_ static
#else
#define _STATIC_
#endif


/******************************************************************/
/***************** CONSTANT DEFINITIONS ***************************/
/******************************************************************/

/* Maximum length of the information field for each Volume */
#define MAXVOLUMEINFO 150

/* Possible values for 'VOLUME.datatype' field*/
#define UNUSED_VOLUME 0x0000   /* unused Volume */
#define UCVAL         0x0001   /* unsigned char datatype in Volume */
#define FVAL          0x0002   /* float         datatype in Volume */
#define IVAL          0x0004   /* integer       datatype in Volume */ 

/* Possible values for 'VOLUME.colortype' field*/
#define CHANNELS_1    0x0001   /* 1 channel used in Volume */
#define CHANNELS_3    0x0002   /* 3 channels used in Volume */


/******************************************************************/
/***************** MACRO DEFINITIONS ******************************/
/******************************************************************/

/*
Macro to be used for correct rounding of the result of
the division of two POSITIVE integer values.
(casting to the required type of the result is recommended)
*/
#define divround(X,Y)  ( floor(((double)(X)/(double)(Y))+0.5) )


/******************************************************************/
/***************** TYPE DEFINITIONS *******************************/
/******************************************************************/

/* Type definitions for basic 2D structures */
typedef unsigned char * *   image;
typedef float         * *  matrix;
typedef int           * * imatrix;

/* Type definitions for basic 3D structures */
typedef unsigned char * * *   image_sequence;
typedef float         * * *  matrix_sequence;
typedef int           * * * imatrix_sequence;

/* Type definition for Volume structure */
typedef struct
{
	/* Definition of a union to keep pointers to the 3D data in memory.
	     ucval is used if datatype==UCVAL
	     fval  is used if datatype==FVAL
	     ival  is used if datatype==IVAL
	   The arrays have three elements, one for each channel.
	     Only the first element is used if colortype==CHANNELS_1
	     All three elements are used    if colortype==CHANNELS_3 */
	union
	{
		image_sequence ucval[3];
		matrix_sequence fval[3];
		imatrix_sequence ival[3];
	} mem;
	/* In case of building for a Windows 3.* platform, the handles of
	   allocated memory units for the 3D data are also kept for
	   each channel. The memory unit is the memory for a frame line. */
#ifdef _WINDOWS
#ifndef _WIN32
	HANDLE * * hand[3];
#endif
#endif
	int datatype;  /* possible values: UNUSED_VOLUME, UCVAL, FVAL, IVAL */
	int colortype; /* possible values: CHANNELS_1, CHANNELS_3 */
	int depth;        /* z dimension: number of Volume frames */
	int height;       /* y dimension: number of rows per frame */
	int width;        /* x dimension: number of columns per frame */
	char info[MAXVOLUMEINFO];  /* information field to keep user notes */
	/* VOI definition:
	   (left,top,low) is the first point with the smaller indices,
	   (right,bottom,high) is the last point with the greater indices. */
	struct
	{
		int left;
		int right;
		int top;
		int bottom;
		int low;
		int high;
	} VOI;
} VOLUME;

/* Type definition of a structure to keep voxel coordinates and cost */
typedef struct pixel3d
{
	int frame; 
	int row; 
	int column; 
	int cost;
} CPIXEL3D;

/* Type definition of a structure useful for creating a stack of voxels */
typedef struct node3d
{
	CPIXEL3D info; 
	struct node3d * link;
} NODE3D; 


/******************************************************************/
/***************** FUNCTION PROTOTYPES ****************************/
/******************************************************************/


/*---------------------------------*/
/* routines defined in "build3d.c" */
/*---------------------------------*/

typedef void (*INITIALIZEVOLUME_PROC)(VOLUME *,int,int,int,int,int);
typedef void (*FILL_IN_VOI_PROC)(VOLUME *,int, int, int , int,int , int);
typedef int (*CREATEVOLUME_PROC)(VOLUME *);
typedef void (*DESTROYVOLUME_PROC)(VOLUME *);
typedef int (*CHECKVOLUME_PROC)(VOLUME *,int,int);
typedef int (*CHECKIFUCBINARY_PROC)(VOLUME *);
typedef int (*CHECKIFVOLUMECLEAR_PROC)(VOLUME *,float []);

/*------------------------------*/
/* routines defined in "io3d.c" */
/*------------------------------*/

typedef int (*FROMDISKUCFI3D_PROC)(VOLUME * ,char* ,int ,char ,char *[],int , int , int );
typedef int (*TODISKUCFI3D_PROC)(VOLUME * ,char* ,int ,char ,char *[]);
typedef int (*DUMPUCFI3D_PROC)(VOLUME * ,char* );
typedef int (*READVOLUME_PROC)(VOLUME * ,char* ,int ,int ,int ,int );
typedef int (*WRITEVOLUME_PROC)(VOLUME * ,char*,int ,int );
typedef int (*CHECKIMAGESEQTIFF_PROC)(char* ,int ,char *,int ,int *,int *,unsigned char *);
typedef int (*READIMAGESEQTIFF_PROC)(VOLUME * ,char* ,int ,char *,int );
typedef int (*WRITEIMAGESEQTIFF_PROC)(VOLUME * ,char* ,int ,char *,unsigned short, unsigned short );

/*-------------------------------*/
/* routines defined in "bas3d.c" */
/*-------------------------------*/

typedef int (*CLEARVOLUME_PROC)(VOLUME * ,float []);
typedef int (*COPYVOLUME_PROC)(VOLUME * , VOLUME * ,int ,int ,int );
typedef int (*COPYFRAME2IMAGE_PROC)(VOLUME * ,int ,image [],int ,int );
typedef int (*COPYIMAGETOFRAME_PROC)(VOLUME * ,int ,image [],int ,int ,int , int , int , int );
typedef int (*UCVOLUME2FVOLUME_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*FVOLUME2UCVOLUME_PROC)(VOLUME * , VOLUME * ,int ,int ,int );
typedef int (*FVOLUME2UCVOLUME_NORM_PROC)(VOLUME * , VOLUME * ,int ,int ,int );
typedef int (*OR3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*XOR3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*AND3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );

/*---------------------------------*/
/* routines defined in "manip3d.c" */
/*---------------------------------*/

typedef int (*ROTATEVOL2D_PROC)(VOLUME * ,VOLUME * ,float ,int ,int ,int ,int ,int ,int );
typedef int (*ROTATEVOL3D_PROC)(VOLUME * ,VOLUME * ,float ,float ,int ,int ,int ,int ,int ,int ,int );
typedef int (*SYMMETRIC3D_PROC)(VOLUME * ,VOLUME * );

/*----------------------------------*/
/* routines defined in "intrpl3d.c" */
/*----------------------------------*/

typedef int (*ZOOM3D_PROC)(VOLUME * ,VOLUME * ,int ,int ,int ,int );
typedef int (*DECIM3_PROC)(VOLUME * ,VOLUME * ,int ,int ,int ,int );

typedef int (*INTERPOLATIONNEARESTNEIGHBOR3D_PROC)(VOLUME * ,VOLUME * ,int ,float ,float ,float ,int ,int ,int );
typedef int (*INTERPOLATIONLINEAR3D_PROC)(VOLUME * ,VOLUME * ,int ,float ,float ,float ,int ,int ,int );
typedef int (*INTERPOLATIONFRAMELINEAR3D_PROC)(VOLUME * ,VOLUME * ,int ,float ,int ,int ,int );

/*----------------------------------*/
/* routines defined in "imageseq.c" */
/*----------------------------------*/

typedef int (*FRAMEINTERPOLATIONBINARY_PROC)(VOLUME * ,VOLUME * ,int ,int ,int ,int ,int );
/*********************************************************************/
/******* WARNING: 'FrameInterpolationGrayscale()' is not needed ******/
/******* since it is covered by interpolation routines          ******/
/*********************************************************************/
typedef int (*FRAMEINTERPOLATIONGRAYSCALE_PROC)(VOLUME * ,VOLUME * ,int ,int ,int ,int );  /* better covered by InterpolationFrameLinear3d() */
/*********************************************************************/
/******* WARNING: 'FrameContourObjectFill()' can be left out    ******/
/******* since it is very specialized and not used in EIKONA3D  ******/
/*********************************************************************/
typedef int (*FRAMECONTOUROBJECTFILL_PROC)(VOLUME * ,VOLUME * );
typedef int (*FRAMEZEROPADDING_PROC)(VOLUME * ,VOLUME * ,int ,int ,int ,int );

/*--------------------------------*/
/* routines defined in "filt3d.c" */
/*--------------------------------*/

typedef int (*HIST3_PROC)(VOLUME * ,vector []);
typedef int (*CDFHIST3_PROC)(vector ,vector );
typedef int (*HISTEQ3_PROC)(VOLUME * ,VOLUME * ,vector []);
typedef int (*HISTOGRAMEQUAL3D_PROC)(VOLUME * ,VOLUME * );

typedef int (*CONV3_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*CONVOLUTION3_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*OVERLAP_ADD3_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,int ,int ,int );

typedef int (*MOV_AVE3_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*MEDIAN3_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*RUN_MEDIAN3_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*WEI_MEDIAN3_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*MULTISTAGE_MEDIAN3_PROC)(VOLUME * ,VOLUME * ,int );
typedef int (*RUN_MAX3_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*RUN_MIN3_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*L_FILTER3_PROC)(VOLUME * ,VOLUME * ,vector ,int ,int ,int );
typedef int (*LOCAL_ADAPT_FILT3_PROC)(VOLUME * ,VOLUME * ,float ,int ,int ,int );

typedef int (*WIENERFREQUENCY3_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,float );

/*--------------------------------*/
/* routines defined in "edge3d.c" */
/*--------------------------------*/

typedef int (*SOBEL_3D_PROC)(VOLUME * ,VOLUME * );
typedef int (*COMPAS3D_PROC)(VOLUME * ,VOLUME * ,int ,int );
typedef int (*LAPLACE_3D_PROC)(VOLUME * ,VOLUME * );
typedef int (*RANGE_3D_PROC)(VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*HOUGH_3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,int );

/*********************************************************************/
/******* WARNING: 'edge_follow_3d()' should be changed before     ****/
/******* it is inserted in the library (it doesn't work normally) ****/
/*********************************************************************/
typedef int (*EDGE_FOLLOW_3D)(VOLUME * ,VOLUME * ,int ,int ,int ,int ,int ,int );
/*********************************************************************/
/******* WARNING: 'modedg3d()' should be changed before         ******/
/******* it is inserted in the library (it finds a random path) ******/
/*********************************************************************/
typedef int (*MODEDG3D_PROC)(VOLUME * ,VOLUME * ,int ,int ,int ,int ,int ,int ,int );

/*--------------------------------*/
/* routines defined in "list3d.c" */
/*--------------------------------*/

typedef int (*PUSH_3D_PROC)(NODE3D ** ,CPIXEL3D );
typedef CPIXEL3D (*POP_3D_PROC)(NODE3D ** );

/*----------------------------------*/
/* routines defined in "region3d.c" */
/*----------------------------------*/

typedef int (*THRESHOLD_3D_PROC)(VOLUME * ,VOLUME * ,int );
typedef int (*REGION3D_GROW_PROC)(VOLUME * ,VOLUME * ,int ,int);
typedef int (*REGION3D_MERGE_PROC)(VOLUME * ,VOLUME * ,int ,int );
typedef int (*REGION3D_SPLIT_PROC)(VOLUME * ,VOLUME * ,unsigned long *,int );
typedef int (*REGION3D_SPLITMERGE_PROC)(VOLUME * ,VOLUME * ,int *,int );
typedef int (*GRASS3D_LABEL_PROC)(VOLUME * ,VOLUME * ,int *,vector ,int );

/*----------------------------------*/
/* routines defined in "metric3d.c" */
/*----------------------------------*/

typedef int (*COUNTNONZEROPOINTS3D_PROC)(VOLUME * ,unsigned long int *);
typedef int (*COUNTNONZEROSURFACEPOINTS3D_PROC)(VOLUME * ,unsigned long int *);
typedef int (*FINDBOUNDINGBOX3D_PROC)(VOLUME * ,int *,int *,int *,int *,int *,int *);
typedef int (*FINDMINMAXINTENSITY_PROC)(VOLUME  *,int *,int *);

/*---------------------------------*/
/* routines defined in "trans3d.c" */
/*---------------------------------*/

typedef int (*LRCFFT3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,int ,int , int , int );
typedef int (*VRFFT3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,int ,int , int , int );
typedef int (*FFT3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,int ,int , int , int );

typedef int (*CORRELATION3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,int , int , int );
typedef int (*BLACKMAN_TUKEY_PSD3D_PROC)(VOLUME * ,VOLUME * ,int , int , int );

typedef int (*DCT3D_PROC)(VOLUME * , VOLUME * );
typedef int (*IDCT3D_PROC)(VOLUME * , VOLUME * );

typedef int (*REIM2MAGNPHASE3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*MAGNPHASE2REIM3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * ,VOLUME * ,int ,int ,int );
typedef int (*FFT2IMAGE3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * , int , int , int );

/*---------------------------------*/
/* routines defined in "morph3d.c" */
/*---------------------------------*/

typedef int (*MINKOWSKIADDITION3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*MINKOWSKISUBTRACTION3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*BDILATE3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*BERODE3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*BOPENING3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
typedef int (*BCLOSING3D_PROC)(VOLUME * ,VOLUME * ,VOLUME * );
/*********************************************************************/
/******* WARNING: the following rouitnes in morph3d.c can be     *****/
/******* left out of the library (they are not used in EIKONA3D) *****/
/*********************************************************************/
typedef int (*PRODUCESE3D_PROC)(VOLUME * ,int );
typedef int (*BFINDMAXINSCRIBABLE3D_PROC)(VOLUME * ,VOLUME * ,int *,VOLUME * ,VOLUME * );
typedef int (*BSHAPEDECOMPOSITION3D_PROC)(VOLUME * ,VOLUME * ,int [],VOLUME * * ,int ,int *,VOLUME * );
typedef int (*BSEGROUPSHAPEDECOMP3D_PROC)(VOLUME * ,VOLUME * * ,int ,int [],int [],VOLUME * * ,int ,int *,VOLUME * );

/*----------------------------------*/
/* routines defined in "galldisp.c" */
/*----------------------------------*/

typedef int (*PRODUCEFRAMEGALLERY3D_PROC)(VOLUME * ,VOLUME * ,int ,int );

/*----------------------------------*/
/* routines defined in "visual3d.c" */
/*----------------------------------*/

typedef int (*PROVOLI2D_PROC)(VOLUME * ,image [],int ,int ,float ,int ,unsigned char []);
typedef int (*PROVOLI3D_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int ,unsigned char []);
typedef int (*PROVOLIAVRG2D_PROC)(VOLUME * ,image [],int ,int ,float ,int );
typedef int (*PROVOLIAVRG3D_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int );
typedef int (*PROVOLIMAX2D_PROC)(VOLUME * ,image [],int ,int ,float ,int );
typedef int (*PROVOLIMAX3D_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int );
typedef int (*TOMIORTH_PROC)(VOLUME * ,image [],int ,int ,int ,int );
typedef int (*TOMIRAND1_PROC)(VOLUME * ,image [],int ,int ,float ,float ,float ,float ,int );
typedef int (*TOMIRAND2_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int ,float );
typedef int (*PROVOLI2DBINARY_PROC)(VOLUME * ,image [],int ,int ,float ,int ,unsigned char []);
typedef int (*PROVOLI3DBINARY_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int ,unsigned char []);
typedef int (*PROVOLI3DSURFACE_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int ,unsigned char [],int );
typedef int (*PROVOLI3DSURFACECOLOR_PROC)(VOLUME * ,image [],int ,int ,float ,float ,int ,unsigned char [],int );


/****** Functions available for external call ******/
typedef int (*BUILDCHAINS_PROC)(VOLUME * ,char *, int ,unsigned char **);
typedef int (*SPLISED_PROC)(char *, char *, int ,int ,int ,float ,int *, float , float );
typedef int (*SHOW_LAYER_PROC)(char *,char *,char *,float ,int ,int ,int );
typedef int (*SHOW_LINES_PROC)(char *,char *,float ,int ,int ,int );
typedef void (*DELETECHAINFILES_PROC)(char *,int ,int );
typedef void (*DELETETRIANGLEFILES_PROC)(char *,int ,int );

#endif  //matches with:  #ifndef _IN_LIB3D_H_
