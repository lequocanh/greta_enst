/**
 * Copyright Aldebaran Robotics
 */

#ifndef ALHelloWorld_H
#define ALHelloWorld_H

#include <alptr.h>
#include <almodule.h>

namespace AL
{
  class ALBroker;
}

/**
 * DESCRIBE YOUR CLASS HERE
 */
class ALHelloWorld : public AL::ALModule
{
  struct wavheader
  {
        char ChunkID[4]; // 32 bits
        long ChunkSize;  // 32 bits
        char Format[4];  // 32 bits
        char Subchunk1ID[4]; // 32 bits
        long Subchunk1Size; // 32 bits
        unsigned short AudioFormat; // 16 bits
        unsigned short NumChannels; // 16 bits
        unsigned long SampleRate; // 32 bits
        unsigned long ByteRate; // 32 bits
        unsigned short BlockAlign; // 16 bits
        unsigned short BitsPerSample; // 16 bits
        char Subchunk2ID[4]; // 32 bits
        long Subchunk2Size;  // 32 bits

  };
  
  public:

    /**
     * Default Constructor.
     */
    ALHelloWorld(AL::ALPtr<AL::ALBroker> pBroker, const std::string& pName );

    /**
     * Destructor.
     */
    virtual ~ALHelloWorld();

    /**
     * helloWorld, print "hello world!" to the ALLogger module
     */
    void helloWorld( const std::string & pFileName );
    
    AL::ALPtr<AL::ALProxy> ad;
};
#endif // ALHelloWorld_H

