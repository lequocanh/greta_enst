//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// neuronalOscillator.h: simple two neurons oscillator
//   @author Ken Prepin ken.prepin@telecom-paristech.fr
//   @date 2008
//////////////////////////////////////////////////////////////////////

#pragma once

#if !defined(NEURONALOSC_H)
#define _NEURONALOSC_H


class neuronalOscillator
{

public:
	/**
	* Class default constructor of the oscillator, with initialisation : neuronOne=1, neuronTwo=2 
	*
	*/

	neuronalOscillator();

	/**
	* Constructor of the oscillator, with initialisation : neuronOne=1, neuronTwo=2 and its frequency freq
	*
	*/
	neuronalOscillator(float freq);

	/**
	* Destructor of oscillator
	*
	*/
	~neuronalOscillator();

	/**
	* update the step by step functionning of the oscillator.
	*
	*/
	void updateOscillator();

	/**
	* get the oscillator activation (neuronOne)
	*
	*/
	float getActivation();

	/**
	* get the oscillator inhibition (inhibition)
	*
	*/
	float getInhib(void);

	/**
	* set the oscillator inhibition (inhibOsc)
	*
	*/
	void setInhib(float inhib);

	/**
	* get the oscillator frequency (freqOsc)
	*
	*/
	float getFreq();

	/**
	* set the oscillator frequency (freqOsc)
	*
	*/
	void setFreq(float freq);

private:

	/**
	* The two neurons constituing an oscillator, an inhibition modulating this oscillator, and the frequency parameter of this oscillator
	*
	*/

	float neuronOne, neuronTwo, neuronThree, neuronFour, inhibOsc, freqOsc;

};

#endif