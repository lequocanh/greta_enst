#include <string>
#include "CommunicationMessage.h"

CommunicationMessage::CommunicationMessage()
{
	content="";
	type="";
	data=NULL;
	size=0;
	postedTime=0;
	receivedTime=0;
}

CommunicationMessage::~CommunicationMessage()
{
}

std::string CommunicationMessage::getContent()
{
	return this->content;
}

std::string CommunicationMessage::getType()
{
	return this->type;
}

char* CommunicationMessage::getData()
{
	return this->data;
}

long CommunicationMessage::getSize()
{
	return this->size;
}

void CommunicationMessage::setContent(std::string content)
{
	this->content=content;
}

void CommunicationMessage::setType(std::string type)
{
	this->type=type;
}

void CommunicationMessage::setData(char *data)
{
	this->data=data;
}

void CommunicationMessage::setSize(long size)
{
	this->size=size;
}

void CommunicationMessage::setPostedTime(long PostedTime)
{
	this->postedTime=PostedTime;
}
void CommunicationMessage::setReceivedTime(long ReceivedTime)
{
	this->receivedTime=ReceivedTime;
}
long CommunicationMessage::getPostedTime()
{
	return this->postedTime;
}
long CommunicationMessage::getReceivedTime()
{
	return this->receivedTime;
}
