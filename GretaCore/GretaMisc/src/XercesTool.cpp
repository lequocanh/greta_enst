/*
 *  XercesTool.cpp
 *  semaine
 *
 *  Created by Marc Schröder on 03.11.08.
 *  Copyright 2008 DFKI GmbH. All rights reserved.
 *
 */

#include "XercesTool.h"
#include "ErrorManager.h"
#include <typeinfo>

using namespace XERCES_CPP_NAMESPACE;

XercesDOMParser * XercesTool::parser;
XercesDOMParser * XercesTool::XSDparser;
DOMImplementationLS * XercesTool::impl;

void XercesTool::startupXMLTools()
throw (excep::SystemConfigurationException)
{
    XMLPlatformUtils::Initialize();
	parser = new XercesDOMParser();
	parser->setErrorHandler(new ErrorManager());
    //parser->setValidationScheme(XercesDOMParser::Val_Always);
	parser->setValidationScheme(XercesDOMParser::Val_Never);
	parser->setLoadExternalDTD(false);
	parser->setDoNamespaces(true);

	XSDparser = new XercesDOMParser();
	XSDparser->setErrorHandler(new ErrorManager());
	//XSDparser->setValidationScheme(XercesDOMParser::Val_Auto);
	XSDparser->setValidationScheme(XercesDOMParser::Val_Never);
	XSDparser->setLoadExternalDTD(false);
	XSDparser->setExitOnFirstFatalError(true);
	XSDparser->setValidationConstraintFatal(true);
	XSDparser->setDoNamespaces(true);
	XSDparser->setDoSchema(true);
	XSDparser->setValidationSchemaFullChecking(true);

	XMLCh tempStr[100];
	XMLString::transcode("LS", tempStr, 99);
	DOMImplementation * anImpl = DOMImplementationRegistry::getDOMImplementation(tempStr);
	impl = dynamic_cast<DOMImplementationLS *>(anImpl);
	if (impl == NULL) {
		throw new excep::SystemConfigurationException(std::string("DOM impl is not a DOMImplementationLS, but a ")+typeid(*anImpl).name());
	}
}

void XercesTool::shutdownXMLTools()
{
	delete parser;
	delete XSDparser;
	XMLPlatformUtils::Terminate();
}



DOMImplementation * XercesTool::getDOMImplementation()
{
	return (DOMImplementation *) impl;
}

DOMDocument * XercesTool::parseFile(const std::string filename, bool validation)
{
	if(validation)
		parser->setValidationScheme(XercesDOMParser::Val_Always);
	else
		parser->setValidationScheme(XercesDOMParser::Val_Never);
	parser->parse(filename.c_str());
	DOMDocument * document = parser->getDocument();
	return document;
}

DOMDocument * XercesTool::parse(const std::string & xmlAsText, bool validation)
{
		//std::cerr << "Message text: " << std::endl << xmlAsText << std::endl;
		const char * msgTextC = xmlAsText.c_str();
		MemBufInputSource* memIS = new MemBufInputSource((const XMLByte *)msgTextC, strlen(msgTextC), "test", false);
		if(validation)
			parser->setValidationScheme(XercesDOMParser::Val_Always);
		else
			parser->setValidationScheme(XercesDOMParser::Val_Never);
		parser->parse(*memIS);
		DOMDocument * document = parser->getDocument();
		return document;
}

DOMDocument * XercesTool::parseAndValidate(const std::string & xmlAsText, const std::string & schemaLocation, bool useNamespace)
{
	if(useNamespace==true)
		XSDparser->setExternalSchemaLocation(schemaLocation.c_str());
	else
		XSDparser->setExternalNoNamespaceSchemaLocation(schemaLocation.c_str());

	const char * msgTextC = xmlAsText.c_str();
	MemBufInputSource* memIS = new MemBufInputSource((const XMLByte *)msgTextC, strlen(msgTextC), "test", false);
    XSDparser->parse(*memIS);
	
	DOMDocument *document = XSDparser->getDocument();	
	delete memIS;
	return document;
}


DOMDocument * XercesTool::newDocument(const std::string & rootTagname, const std::string & aNamespace)
{
	XMLCh * xmlNamespaceURI = XMLString::transcode(aNamespace.c_str());
	XMLCh * xmlQualifiedName = XMLString::transcode(rootTagname.c_str());
	DOMImplementation * dom = getDOMImplementation();
	DOMDocument * doc = dom->createDocument(xmlNamespaceURI, xmlQualifiedName, NULL);
	XMLString::release(&xmlNamespaceURI);
	XMLString::release(&xmlQualifiedName);
	return doc;
}

DOMDocument * XercesTool::newDocument(const std::string & rootTagname, const std::string & aNamespace, const std::string & version)
{
	DOMDocument * doc = newDocument(rootTagname, aNamespace);
	DOMElement * root = doc->getDocumentElement();
    setAttribute(root, "version", version);
	return doc;
}


DOMElement * XercesTool::createElement(DOMDocument * doc, const std::string & elementName)
{
	const XMLCh * xmlNamespaceURI = doc->getDocumentElement()->getNamespaceURI();
	XMLCh * xmlElementName = XMLString::transcode(elementName.c_str());
	DOMElement * e = doc->createElementNS(xmlNamespaceURI, xmlElementName);
	XMLString::release(&xmlElementName);
	return e;
}

DOMElement * XercesTool::createElement(DOMDocument * doc, const std::string & elementName, const std::string & aNamespace)
{
	XMLCh * xmlNamespaceURI = XMLString::transcode(aNamespace.c_str());
	XMLCh * xmlElementName = XMLString::transcode(elementName.c_str());
	DOMElement * e = doc->createElementNS(xmlNamespaceURI, xmlElementName);
	XMLString::release(&xmlNamespaceURI);
	XMLString::release(&xmlElementName);
	return e;
}

DOMText * XercesTool::createTextNode(DOMDocument * doc, const std::string & textContent)
{
	XMLCh * xmlTextContent = XMLString::transcode(textContent.c_str());
	DOMText * t = doc->createTextNode(xmlTextContent);
	XMLString::release(&xmlTextContent);
	return t;
}

DOMElement * XercesTool::appendChildElement(DOMNode * node,  const std::string & childName)
{
	const XMLCh * xmlNamespaceURI = node->getNamespaceURI();
	XMLCh * xmlElementName = XMLString::transcode(childName.c_str());
	DOMElement * newElement = node->getOwnerDocument()->createElementNS(xmlNamespaceURI, xmlElementName);
	DOMElement * e = (DOMElement *) node->appendChild(newElement);
	XMLString::release(&xmlElementName);
	return e;
}

DOMElement * XercesTool::appendChildElement(DOMNode * node,  const std::string & childName, const std::string & childNamespace)
{
	XMLCh * xmlNamespaceURI = XMLString::transcode(childNamespace.c_str());
	XMLCh * xmlElementName = XMLString::transcode(childName.c_str());
	DOMElement * newElement = node->getOwnerDocument()->createElementNS(xmlNamespaceURI, xmlElementName);
	DOMElement * e = (DOMElement *) node->appendChild(newElement);
	XMLString::release(&xmlNamespaceURI);
	XMLString::release(&xmlElementName);
	return e;
}

DOMText * XercesTool::appendChildTextNode(DOMNode * node, const std::string & textContent)
{
	return (DOMText *) node->appendChild(createTextNode(node->getOwnerDocument(), textContent));
}


const std::string XercesTool::getTextContent(DOMNode * node)
{
	return transcode(node->getTextContent());
}

void XercesTool::setTextContent(DOMNode * node, std::string text)
{
	XMLCh * txt = XMLString::transcode(text.c_str());
	node->setTextContent(txt);
	XMLString::release(&txt);
}


DOMElement * XercesTool::getFirstElementByTagNameNS(DOMElement * element, const std::string & childName, const std::string & childNamespace)
{
	XMLCh * xmlChildName = XMLString::transcode(childName.c_str());
	XMLCh * xmlNamespaceURI = XMLString::transcode(childNamespace.c_str());
	DOMNodeList * nl = element->getElementsByTagNameNS(xmlNamespaceURI, xmlChildName);

	if(nl->getLength()==0)
		return NULL;
	
	DOMElement * first = (DOMElement *)nl->item(0);
	XMLString::release(&xmlChildName);
	XMLString::release(&xmlNamespaceURI);
	return first;
}

DOMElement * XercesTool::getFirstElementByLocalNameNS(DOMElement * element, const std::string & childName, const std::string & childNamespace)
{
	if (element == NULL) return NULL;
	XMLCh * xmlChildName = XMLString::transcode(childName.c_str());
	XMLCh * xmlNamespaceURI = XMLString::transcode(childNamespace.c_str());
	DOMNodeIterator * ni = element->getOwnerDocument()->createNodeIterator(element, DOMNodeFilter::SHOW_ELEMENT, NULL, false);
	DOMElement * elt;
	while ((elt = (DOMElement *) ni->nextNode()) != NULL) {
		if (XMLString::equals(elt->getLocalName(), xmlChildName) &&
			XMLString::equals(elt->getNamespaceURI(), xmlNamespaceURI)) {
				// found the node
				break;
		}
	}
	XMLString::release(&xmlChildName);
	XMLString::release(&xmlNamespaceURI);
	return elt; // the element if found, NULL otherwise
}

std::vector<DOMNode *> XercesTool::getElementAndTextChildren(DOMElement * element)
{
	std::vector<DOMNode *> alist;
	if (element == NULL) return alist;
	DOMNodeList * nl = element->getChildNodes();
	int num = nl->getLength();
	int i;
	for (i=0; i<num; i++)
	{
		DOMNode * n = nl->item(i);
		if (n->getNodeType() == DOMNode::ELEMENT_NODE
			|| n->getNodeType() == DOMNode::TEXT_NODE)
			alist.push_back(n);
	}
	return alist;
}

DOMElement * XercesTool::getChildElementByTagNameNS(DOMNode * node, const std::string & childName, const std::string & childNamespace)
{
	XMLCh * xmlChildName = XMLString::transcode(childName.c_str());
	XMLCh * xmlNamespaceURI = XMLString::transcode(childNamespace.c_str());
	DOMNodeList * nl = node->getChildNodes();
	DOMElement * e = NULL;
	for (int i=0, max=nl->getLength(); i<max; i++) {
		DOMNode * n = nl->item(i);
		if (n->getNodeType() == DOMNode::ELEMENT_NODE
		  && XMLString::equals(n->getNodeName(), xmlChildName)
		  && XMLString::equals(n->getNamespaceURI(), xmlNamespaceURI)) {
			e = (DOMElement *) n;
			break;
		}
	}
	XMLString::release(&xmlChildName);
	XMLString::release(&xmlNamespaceURI);
	return e;
}

DOMElement * XercesTool::needChildElementByTagNameNS(DOMNode * node, const std::string & childName, const std::string & childNamespace)
throw(excep::MessageFormatException)
{
	DOMElement * e = getChildElementByTagNameNS(node, childName, childNamespace);
	if (e == NULL) {
		const std::string nodeNamespace = getNamespaceURI(node);
		bool sameNamespace = nodeNamespace == childNamespace;
		throw excep::MessageFormatException("Node '"+getNodeName(node)+"' in namespace '"+
					nodeNamespace+" needs to have a child '"+childName+"' in "+
					(sameNamespace ? "the same namespace" : "namespace '"+childNamespace+"'"));
	}
	return e;
}

std::list<DOMElement *> * XercesTool::getChildrenByTagNameNS(DOMNode * node, const std::string & childName, const std::string & childNamespace)
throw(excep::MessageFormatException)
{
	std::list<DOMElement *> * children = new std::list<DOMElement *>();
	XMLCh * xmlChildName = XMLString::transcode(childName.c_str());
	XMLCh * xmlNamespaceURI = XMLString::transcode(childNamespace.c_str());
	DOMNodeList * nl = node->getChildNodes();
	for (int i=0, max=nl->getLength(); i<max; i++) {
		DOMNode * n = nl->item(i);
		if (n->getNodeType() == DOMNode::ELEMENT_NODE
		  && XMLString::equals(n->getNodeName(), xmlChildName)
		  && XMLString::equals(n->getNamespaceURI(), xmlNamespaceURI)) {
			children->push_back((DOMElement *)n);
		}
	}
	XMLString::release(&xmlChildName);
	XMLString::release(&xmlNamespaceURI);
	return children;
}


const std::string XercesTool::getNamespaceURI(DOMNode * node)
{
	return transcode(node->getNamespaceURI());
}

const std::string XercesTool::getNodeName(DOMNode * node)
{
	return transcode(node->getNodeName());
}

const std::string XercesTool::getTagName(DOMElement * e)
{
	return transcode(e->getTagName());
}


const std::string XercesTool::getLocalName(DOMElement * e)
{
	return transcode(e->getLocalName());
}

const std::string XercesTool::getPrefix(DOMElement * e)
{
	return transcode(e->getPrefix());
}

void XercesTool::setPrefix(DOMElement * e, const std::string & prefix)
{
	XMLCh * pref = XMLString::transcode(prefix.c_str());
	e->setPrefix(pref);
	XMLString::release(&pref);
}


bool XercesTool::hasAttribute(DOMElement * e, const std::string & attributeName)
{
	XMLCh * xmlAttributeName = XMLString::transcode(attributeName.c_str());
	bool b = e->hasAttribute(xmlAttributeName);
	XMLString::release(&xmlAttributeName);
	return b;
}


const std::string XercesTool::getAttribute(DOMElement * e, const std::string & attributeName)
{
	XMLCh * xmlAttributeName = XMLString::transcode(attributeName.c_str());
	std::string s = transcode(e->getAttribute(xmlAttributeName));
	XMLString::release(&xmlAttributeName);
	return s;
}

const std::string XercesTool::needAttribute(DOMElement * e, const std::string & attributeName)
throw(excep::MessageFormatException)
{
	XMLCh * xmlAttributeName = XMLString::transcode(attributeName.c_str());
	if (!e->hasAttribute(xmlAttributeName)) {
		throw excep::MessageFormatException("Element '"+getTagName(e)+"' in namespace '"+
					getNamespaceURI(e)+"' needs an attribute '"+attributeName+"'");
	}
	std::string s = transcode(e->getAttribute(xmlAttributeName));
	XMLString::release(&xmlAttributeName);
	return s;
}


void XercesTool::setAttribute(DOMElement * e, const std::string & attributeName, const std::string & value)
{
	XMLCh * xmlAttributeName = XMLString::transcode(attributeName.c_str());
	XMLCh * xmlAttributeValue = XMLString::transcode(value.c_str());
	e->setAttribute(xmlAttributeName, xmlAttributeValue);
	XMLString::release(&xmlAttributeName);
	XMLString::release(&xmlAttributeValue);
}



const std::string XercesTool::transcode(const XMLCh * xmlString)
{
	if (xmlString == NULL) return std::string("");
	char * chars =XMLString::transcode(xmlString);
	std::string s = std::string(chars);
	XMLString::release(&chars);
	return s;
}


const std::string XercesTool::dom2string(const DOMNode * dom)
{
	if (dom == NULL) return std::string("");
	try {
		XMLCh utf8[10];
		XMLString::transcode("UTF-8", utf8, 9);
		MemBufFormatTarget * outTarget = new MemBufFormatTarget();
#if (_XERCES_VERSION < 30000)
		DOMWriter * writer = impl->createDOMWriter();
		// set "format-pretty-print" to true:
		writer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);
		writer->setEncoding(utf8);
		writer->writeNode(outTarget, *dom);
		const XMLByte * buf = outTarget->getRawBuffer();
		//int len = outTarget->getLen();
		std::string result((const char *)buf);
		writer->release();
#else
		DOMLSSerializer * serializer = impl->createLSSerializer();
		DOMConfiguration * config = serializer->getDomConfig();
		// set "format-pretty-print" to true:
		config->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
		DOMLSOutput * output = impl->createLSOutput();
		output->setEncoding(utf8);
		output->setByteStream(outTarget);
		serializer->write(dom, output);
		const XMLByte * buf = outTarget->getRawBuffer();
		//int len = outTarget->getLen();
		std::string result((const char *)buf);
		serializer->release();
		output->release();
#endif
		return result;
	} catch (XMLException &xe) {
		char * err = XMLString::transcode(xe.getMessage());
		std::cerr << err << std::endl;
		XMLString::release(&err);
		throw excep::SystemConfigurationException("Cannot initialise XML system");
	} catch (DOMException &de) {
		char * err = XMLString::transcode(de.getMessage());
		std::cerr << err << std::endl;
		XMLString::release(&err);
		throw excep::SystemConfigurationException("Cannot initialise XML system");
	}

}

DOMText * XercesTool::getFirstTextNode(DOMNode *nd)
{
	if (nd==NULL) return NULL;
	//DOMDocumentTraversal * dt = (DOMDocumentTraversal) nd->getOwnerDocument();
	DOMDocument *dt = nd->getOwnerDocument();
	if (dt==NULL) return NULL;
	DOMNodeIterator * ni = dt->createNodeIterator(nd, DOMNodeFilter::SHOW_TEXT, NULL, false);
	if (ni==NULL) return NULL;
	DOMText * text = (DOMText *) ni->nextNode();
	return text;
}

