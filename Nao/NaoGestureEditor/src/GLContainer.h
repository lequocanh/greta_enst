#pragma once
#define GESTUREEDITOROGRE


#include <string>
#include <FL/Fl_Window.H>
#ifdef GESTUREEDITOROGRE
#include "PlayerOgreView.h"
#define ViewWindow PlayerOgreView
#else
#include "PlayerFLTKGLWindow.h"
#define ViewWindow PlayerFLTKGLWindow
#endif


class GLContainer : public Fl_Window  
{
public:
	GLContainer(void);
	~GLContainer(void);
	ViewWindow *glwindow;
	void Play(std::string s);
	void draw();
	void show();
};
