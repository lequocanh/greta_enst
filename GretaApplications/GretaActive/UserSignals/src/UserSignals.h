//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorPlanner.h: interface for the BehaviorPlanner class.
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#ifndef SEMAINE_COMPONENTS_GRETA_USERSIGNALS_H
#define SEMAINE_COMPONENTS_GRETA_USERSIGNALS_H

#include <time.h>

#include <cms/CMSException.h>

#include <semaine/components/Component.h>
#include <semaine/cms/sender/StateSender.h>
#include <semaine/datatypes/stateinfo/UserStateInfo.h>

namespace semaine {
namespace components {
namespace greta {

class UserSignals : public Component
{
public:
	UserSignals(const std::string & componentName, bool isInput, bool isOutput) throw (CMSException);
	virtual ~UserSignals();

	
protected:
	virtual void act() throw (CMSException);
	//void UserSignals::await(int seconds);

private:
	StateSender * signalSender;
	int waiting;
	double difD;
	time_t startD;
	time_t endD;
	int itt;



};

} // namespace greta
} // namespace components
} // namespace semaine


#endif

