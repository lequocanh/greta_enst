#-*- coding: iso-8859-15 -*-

''' Cartesian control: synchronization between speech and gestures for 3piecesofsky '''

import config
import motion

def main():


    proxy = config.loadProxy("ALMotion")
    
    #Set NAO in stiffness On
    config.StiffnessOn(proxy)	
    config.PoseInit(proxy)	
    names = ["LElbowRoll", "LElbowYaw", "LShoulderPitch","LShoulderRoll"]
    angleLists = [[-89.2077*3.14/180], [-100.023*3.14/180], [24.7831*3.14/180] , [1.5993*3.14/180]]
    timeLists = [[1.0], [1.0], [1.0], [1.0]]
    isAbsolute = True
	
	#names = ["LHipYawPitch", "LHipRoll", "LHipPitch", "LKneePitch", "LAnklePitch", "LAnkleRoll", "RHipYawPitch", "RHipRoll", "RHipPitch", "RKneePitch", "RAnklePitch", "RAnkleRoll"]
	
    proxy.angleInterpolation(names, angleLists, 1.0, isAbsolute)
	
	
if __name__ == "__main__":
    main()
