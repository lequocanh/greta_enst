//Copyright 1999-2008 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// ActionSelection.cpp: implementation of the ActionSelection class.
//
// Author: Etienne de Sevin etienne.de-sevin@telecom-paristech.fr
//
//////////////////////////////////////////////////////////////////////

#include "ActionSelection.h"
#include <fstream>
#include <time.h>
#include "RandomGen.h"
#include <iostream>
#include <math.h>


#define MAX_DELAY 3000
#define MIN_DELAY 1500

std::ofstream output("logs/action_selection.txt");
extern NInterface *inter;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ActionSelection::ActionSelection()
{

	typeChosen="";
	old_typeChosen="";
	old_zoneBC = "";
	old_zoneM = "";
	anticipNIU= "";
	actchosen = "";
	BCtype = "";
	bml = "";
	fml = "";
	zone = "Head";
	BMLFMLsend = "";
	variationNIU=0.0;
	satisfactionNIU=0.0;
	varNIU=0.0;
	satisNIU=0.0;
	averageNI=0.0;
	il=0;
	it=0;
	numend=5;
	skipped = 0;
	MoyNI=0.0;
	iprob=0;
	iter_react=0;
	anticiploop=0;
	satisfactionloop=0;
	inFront=0;
	leavingFront=2;
	beginning=0;
	difD=0.0;
	priority=0.0;
	priorityRule=0.0;
	calc_priority=0.0;
	timeChosen=0.0;
	NIU=0.5;
	old_NIU=0.5;
	iter_action=0;
	iter_NI=0;
	iter_satis=0;
	average_NUI=0.0;
	som_NUI=0.0;
	probBC=0.0;
	probM=0.0;
	probC=0.0;
	randomBC=0.0;
	randomvalue = 0;
	number_chosen = 0;
	difEnding = 0.0;
	for(int i=0; i<50; i++) candidate_actions_zone[i]="";
	for(int i=0; i<50; i++) candidate_actions[i]="";
	for(int i=0; i<50; i++) candidate_actions_priority[i]=0.0;
	for(int i=0; i<50; i++) storeFMLBML[i]="";
	for(int i=0; i<15; i++) behaviour_old[i]="";
	randgen= new RandomGen;
	srand(time(NULL));
	printoutput = 0;
	speaking = 0;
	hyst = 0.5;
	extroverted = 0.0;
	stability = 0.0;
	userValence = 0.0;
	userArousal = 0.0;
	agentValence = 0.0;
	agentArousal = 0.0;


}

ActionSelection::~ActionSelection() 	
{
}

bool ActionSelection::receiveBC()
{
	/*
	reception des backchannels reactifs
	*/
	if (BCtype!= "")
	{
		if (old_zoneBC != zone) //enleve les BC identiques a la suite
		{
			if (zone=="trigger-head_infront") //existe pas pour mimicry
			{
				inFront = 1;
				leavingFront = 0;
				#ifdef _DEBUG
					printf("\ninFront: %i \n", inFront);
				#endif
			}
			else
				inFront = 0;
			
			randomBC = randgen->GetRand01(); 
			probabilityBC();
			if(randomBC <= probBC) // || zone=="trigger-head_nod" || zone=="trigger-head_shake") //prend en compte la prob d'avoir un BC
			{
				candidate_actions[iter_action]=BCtype;
				candidate_actions_zone[iter_action]=zone;
				storeFMLBML[iter_action]=content;
				//std::cout << NIU << "\n";

				//default value
				candidate_actions_priority[iter_action] = 0.8;
				if(inter->activeManualType == 1)
				{
					candidate_actions_priority[iter_action] = inter->valueManualType1; 
					//std::cout << "BC " << inter->valueManualType1;
				}
				if(inter->activeManualAuto == 1 && inter->activeManualType == 0)
				{
					candidate_actions_priority[iter_action] = normalize(stability, -1.0, 1.0, 1.0, 0.5); //+ normalize(NIU, 0.0, 1.0, 1.0, 0.5))/2;  //((1-((stability+1)/2))+(1-NIU))/2;   0.8; //  //(((-NIU + 0.55)*(NIU - 0.7))+0.9); //*(priorityRule*2/3);
					//std::cout << "Auto ";
				}					
				if(inter->activeManualNUI == 1 && inter->activeManualType == 0)
				{
					candidate_actions_priority[iter_action] = (candidate_actions_priority[iter_action] + normalize(NIU, 0.0, 1.0, 1.0, 0.5))/2;  //((1-((stability+1)/2))+(1-NIU))/2;   0.8; //  //(((-NIU + 0.55)*(NIU - 0.7))+0.9); //*(priorityRule*2/3);
					//std::cout << "Auto1 ";
				}					
				if(inter->activeManualEmotion == 1 && inter->activeManualType == 0)
				{
					#ifdef _DEBUG
						std::cout << "\nuserArousal " << userArousal;
						std::cout << "\nagentArousal " << agentArousal;
						std::cout << "\nuserValence " << userValence;
						std::cout << "\nagentValence " << agentValence;
					#endif
					float dist = sqrt((userArousal-agentArousal)*(userArousal-agentArousal) +(userValence-agentValence)*(userValence-agentValence));
					float distBC = normalize(dist, 0, 2*sqrt(2.0), 1.0, 0.5);
					#ifdef _DEBUG
						std::cout << "\nuserArousal " << userArousal;
						std::cout << "\nagentArousal " << agentArousal;
						std::cout << "\nuserValence " << userValence;
						std::cout << "\nagentValence " << agentValence;
						std::cout << "\ndiffBC "<< distBC;
					#endif
					candidate_actions_priority[iter_action] = (candidate_actions_priority[iter_action]+distBC)/2; // + normalize(userArousal, 0.0, 1.0, 1.0, 0.5))/2;  //((1-((stability+1)/2))+(1-NIU))/2;   0.8; //  //(((-NIU + 0.55)*(NIU - 0.7))+0.9); //*(priorityRule*2/3);
					//std::cout << "Auto2 ";
				}					

				//si c'est un backchannel avec du speech -> priorite haute
				//if (content.find("speech=") != -1) 
				//	NIU = 1;
				//if(inFront == 1)
				//	candidate_actions_priority[iter_action] = candidate_actions_priority[iter_action] + 0.05;
				
				if(candidate_actions_priority[iter_action] > 1)
					candidate_actions_priority[iter_action]=1;
				
				skipped = 0;
				
				#ifdef _DEBUG
					printf("\nDetection BACKCHANNEL: %s %f \n", zone.c_str(), candidate_actions_priority[iter_action]);
				#endif
				//if (i2 == 1)
				//	printf("BC stored %s\n", reactBC->zone.c_str());
				
				iter_action = iter_action + 1;
				
				return true;
			}
			else
			{
				#ifdef _DEBUG
					printf("\nBACKCHANNEL skipped: %s %f %f\n", zone.c_str(), probBC, randomBC);
				#endif
				if(skipped == 0 && printoutput == 1)
				{
					output << "skipped";
					output << "		";
					output << "BC";
					output << "		";
					output << probBC;
					output << "\n";
					skipped = 1;
				}
				return false;
			}
			//old_zoneBC = zone; //pas d'information sur les zones

		}
	}
	else
		printf("Error detection");
}

bool ActionSelection::receiveM()
{
	/*
	Reception des mimicry
	*/
	
	if (BCtype!= "")
	{
		if (old_zoneM != zone) //enleve les M identiques a la suite
		{
			if(leavingFront == 0 && inFront==0)
			{
				leavingFront = 1;
				#ifdef _DEBUG
					printf("\nleavingFront: %i \n", leavingFront);
				#endif
			}		
			
			randomBC = randgen->GetRand01(); 
			probabilityBC();
			if(randomBC <= probM) // || zone=="trigger-head_nod" || zone=="trigger-head_shake") //prend en compte la prob d'avoir un M
			{
				candidate_actions[iter_action]=BCtype;
				candidate_actions_zone[iter_action]=zone;
				storeFMLBML[iter_action]=content;
				
				//default value
				candidate_actions_priority[iter_action] = 0.9; //    //(((0.7*NIU - 0.5)*(-0.8*NIU + 0.8))+0.9); //*(priorityRule*2/3);
				
				if(inter->activeManualType == 1)
				{
					candidate_actions_priority[iter_action] = inter->valueManualType; 
					//std::cout << "M " << inter->valueManualType;
				}
				if(inter->activeManualAuto == 1 && inter->activeManualType == 0)
				{
					candidate_actions_priority[iter_action] = normalize(stability, -1.0, 1.0, 0.5, 1.0); //+ normalize(NIU, 0.0, 1.0, 0.5, 1.0))/2;  //((1-((stability+1)/2))+(1-NIU))/2;   0.8; //  //(((-NIU + 0.55)*(NIU - 0.7))+0.9); //*(priorityRule*2/3);
					//std::cout << "\nAuto \n";
				}
				if(inter->activeManualNUI == 1 && inter->activeManualType == 0)
				{
					candidate_actions_priority[iter_action] = (candidate_actions_priority[iter_action] + normalize(NIU, 0.0, 1.0, 0.5, 1.0))/2;  
					//std::cout << "\nAuto \n";
				}

				if(inter->activeManualEmotion == 1 && inter->activeManualType == 0)
				{
					float dist = sqrt((userArousal-agentArousal)*(userArousal-agentArousal) +(userValence-agentValence)*(userValence-agentValence));
					float distM = normalize(dist, 0, 2*sqrt(2.0), 0.5, 1.0);
					#ifdef _DEBUG
						std::cout << "\nuserArousal " << userArousal;
						std::cout << "\nagentArousal " << agentArousal;
						std::cout << "\nuserValence " << userValence;
						std::cout << "\nagentValence " << agentValence;
						std::cout << "\ndiffM " << distM;
					#endif
					candidate_actions_priority[iter_action] = (candidate_actions_priority[iter_action] + distM)/2; // + normalize(userArousal, 0.0, 1.0, 0.5, 1.0))/2;  
					//std::cout << "\nAuto2 \n";
				}

				if(leavingFront == 1)
				{
					candidate_actions_priority[iter_action] = candidate_actions_priority[iter_action] + 0.05;
					leavingFront = 2;
				}
				if(candidate_actions_priority[iter_action] > 1)
					candidate_actions_priority[iter_action]=1;
				
				skipped = 0;
				//if (i2 == 1)
				//	printf("M stored %s \n", mimicryBC->zone.c_str());
				#ifdef _DEBUG
					printf("\nDetection MIMICRY: %s %f \n", zone.c_str(), candidate_actions_priority[iter_action]);
				#endif

				iter_action = iter_action + 1;

				return true;
			}
			else
			{
				#ifdef _DEBUG
					printf("\nMIMICRY skipped: %s %f %f\n", zone.c_str(), probM, randomBC);
				#endif
				if(skipped == 0 && printoutput == 1)
				{
					output << "skipped";
					output << "		";
					output << "MIMICRY";
					output << "		";
					output << probM;
					output << "\n";
					skipped = 1;
				}
				return false;
			}
			//old_zoneM = zone; //pas d'information sur les zones
		}
	}
	else
		printf("Error detection");

}

bool ActionSelection::receiveC()
{
	/*
	reception des backchannels cognitifs
	*/
	if (BCtype!= "")
	{
		randomBC = randgen->GetRand01(); 
		probabilityBC();
		if(randomBC <= probC) //prend en compte la prob d'avoir un BC cognitif
		{
			candidate_actions[iter_action]=BCtype;
			candidate_actions_zone[iter_action]=zone;
			storeFMLBML[iter_action]=content;
			candidate_actions_priority[iter_action] = 1.0;  //NIU +0.25; // *(priorityRule/2);
			#ifdef _DEBUG
				printf("\nDetection COGNITIVE: %s %f \n", zone.c_str(), candidate_actions_priority[iter_action]);
			#endif
			if(candidate_actions_priority[iter_action] > 1)
				candidate_actions_priority[iter_action]=1;
			iter_action = iter_action + 1;
			//if (i2 == 1)
			//	printf("Cognitive stored\n");
			return true;
		}
		else
		{
			#ifdef _DEBUG
				printf("\nCognitive BC skipped %f %f\n", probC, randomBC);
			#endif
			return false;
		}
	}
	else
		printf("Error detection");
}

void ActionSelection::anticipationNIU()
{
	/*
	Anticipation du NIU et niveau de satisfaction
	*/
	anticiploop=2;
	satisfactionloop=10;

	if (NIU != old_NIU)
	{
		iter_NI = iter_NI + 1;
		iter_satis = iter_satis + 1;
		variationNIU = variationNIU + (NIU - old_NIU); 
		satisfactionNIU = satisfactionNIU + (NIU - old_NIU); 
		averageNI = averageNI + NIU;
	}
	/*
	Variations locales
	*/
	if (iter_NI > anticiploop)  //variations locales
	{
		//printf("\ndifference: %f\n", (NIU - anticipNIU));
		variationNIU = variationNIU/anticiploop;
		varNIU = variationNIU;
		//printf("\nvariation: %f \n", variationNIU);
		iter_NI = 0;
		variationNIU = 0.0;
	}
	/*
	Estimation sur le long terme
	*/
	if (iter_satis > satisfactionloop) 
	{
		satisfactionNIU = satisfactionNIU/satisfactionloop;
		satisNIU = satisfactionNIU;
		MoyNI = averageNI/satisfactionloop;
		//printf("\nmoyenne: %f\n", MoyNI);
		iter_satis = 0;
		satisfactionNIU = 0.0;
		averageNI = 0.0;
	}
	/*
	Envoie une action si NIU faible (BC a definir)
	*/
	//if (MoyNI > 0.75)
	//	beginning = 1;
	//if (beginning == 0)
	//	probBC = probBC + 0.1;

	/*****************
	//A definir 
	******************/
	/*
	if (NIU <= 0.5 && NIU >= 0.25 && satisNIU < 0 && varNIU < 0 && iter_react == 0) // && beginning == 1)
	{
		candidate_actions[iter_action]="ANTICIPATION";
		#ifdef _DEBUG
			printf("\nDetection: ANTICIPATION\n");
		#endif
		storeFMLBML[iter_action]="";
		candidate_actions_priority[iter_action] = 0.75;
		iter_action = iter_action + 1;
		iter_react = 1; //remis a 0 lors du choix, permet d'en avoir un seul par choix
	}
	*/

	/*
	Stop l'interaction si NIU tres bas
	*/
	if (candidate_actions[0]!= "" && MoyNI < 0.25 && satisNIU < 0 && varNIU < 0 && iter_react == 0) // && beginning == 1)
	{
		/*#ifdef _DEBUG
			printf("\nDetection: Ending the interaction \n");
		#endif*/
		//for(int i=0; i<50; i++) possible_actions[i]=NULL; //empeche toute action
	}
	old_NIU = NIU;
}

void ActionSelection::probabilityBC()
{
	/*
	Probabilite du BC
	*/
	//probBC = (1.4*NIU-0.6)*(1.4*NIU-0.8)+0.5;
	//fixe car pas sur de garder la probabilite
	//std::cout << inter->activeManualNumber << "\n";

	//le nombre de BC peut que diminuer
	//float extro = (extroverted+1)/2; // passage de [-1;1] a [0;1]
	//float probExtro = (extroverted+3)/4; // passage de [-1;1] a [0.5;1]
	
	//default value
	probBC = 1.0; // //(0.7*NIU-0.5)*(-1.3*NIU+0.8)+0.9;
	probM = 1.0; //
	probC = 1.0;

	if(inter->activeManualNumber == 1)
	{
		//std::cout << "Number ";
		probM = inter->valueManualNumber;
		probBC = inter->valueManualNumber1;
		//std::cout << "BC  " << probBC << "M  " << probM;

	}

	if(inter->activeManualAuto == 1 && inter->activeManualNumber == 0)
	{
		float probExtro = normalize(extroverted, -1.0, 1.0, 0.5, 1.0); // passage de [-1;1] a [0.5;1]

		probBC = probExtro; //(probExtro+probNIU)/2;  //(probExtro+probNIU)/2; 1.0; // //(0.7*NIU-0.5)*(-1.3*NIU+0.8)+0.9;
		probM = probExtro;  //1.0; //
	}
	else if(inter->activeManualNUI == 1 && inter->activeManualNumber == 0)
	{
		float probNIU = normalize(NIU, 0.0, 1.0, 1.0, 0.5); // passage de [0;1] a [0.5;1]
		probBC = (probBC+probNIU)/2;
	}

	if(inter->activeManualRandom == 1 && inter->activeManualNumber == 0)
	{
		//std::cout << "Random";
		probBC = normalize(randgen->GetRand01(), 0.0, 1.0, 0.5, 1.0);
		probM = normalize(randgen->GetRand01(), 0.0, 1.0, 0.5, 1.0);
	}

	/*if(inter->activeManualType == 1)
	{
		std::cout << "Type";
		std::cout << "BC  " << inter->valueManualType1 << "M  " << inter->valueManualType;
	}*/

	if(inter->activeManualNoM == 1)
	{
		//std::cout << "NoM";
		probM = 0.0;
	}

	if(inter->activeManualNoBC == 1)
	{
		//std::cout << "NoBC";
		probBC = 0.0;
	}

	if(inter->activeManualNoC == 1)
	{
		//std::cout << "NoC";
		probC = 0.0;
	}

	
	/*
	Variation de la probabilite en fonction de l'anticipation
	*/
	if (satisNIU < 0 && varNIU < 0 && MoyNI < 0.65)
		probBC = probBC - varNIU*2.5; //max valeur = 0.1 souvent 0.025

	/*
	Si head_infront augmentation de la prob de BC car l'utilisateur regarde Greta
	*/
	if (inFront == 1)
		probBC = probBC + 0.1;
	if (probBC >=1)
		probBC = 1;

	/*
	Stop l'interaction si l'utilisateur n'est plus interesse au bout de 5 BCs
	*/
	if (NIU <= 0.25 && typeChosen!="" && iprob < numend)
	{
		iprob = iprob + 1;
		//printf("\nEnding the interaction \n");
	}

	if (NIU <= 0.25 && typeChosen!="" && iprob >= numend)
	{
		//for(int i=0; i<50; i++) candidate_actions[i]=""; //empeche toute action
		//printf("\nEnd of the interaction \n");
	}

	if (NIU > 0.25 && typeChosen!="")
		iprob = 0;

}

void ActionSelection::personalityBC()
{
	/*
	personality
	*/
	for(int i=0; i<50; i++)
	{
		//if(candidate_actions[i] == "MIMICRY")
		//	std::cout << "personality";
	}
}

std::string ActionSelection::choice() 
{
	anticipationNIU(); //anticipation du NIU
	probabilityBC();//probalite de BC	
	//Phase(); //phase d'interet de l'utilisateur
	//NIU = (1-hyst)*old_NIU + hyst*NIU;
	typeChosen = "";
	priority = 0.0;

	//std::cout << personality;

	/*
	simulation de BC cognitifs
	*/
	//randomvalue=randgen->GetRand01();
	//if (randomvalue <= 0.001 && candidate_actions[0]!= "")
		//sendCtoAS(cognitive);

	if(candidate_actions[0]!= "") // && waiting==0) //&& iprob < numend
	{
		#ifdef _DEBUG
		if (satisNIU < 0)
			printf("\nAnticipation: loosing interest: %f \n", satisNIU);
		if (satisNIU > 0)
			printf("\nAnticipation: gaining interest: %f \n", satisNIU);
		if (varNIU < 0)
			printf("Anticipation: less intersting %f \n", varNIU);
		if (varNIU > 0)
			printf("Anticipation: more interesting %f \n", varNIU);
		#endif

		for(int i=0; i<50; i++)
		{
			if (candidate_actions[i]!= "")
				//if(debug == 1)
				//	printf("Actions stored %s %i \n", candidate_actions[i].c_str(), i);

			if (candidate_actions[i]!= "" && candidate_actions_priority[i] >= priority)  
			{
				priority = candidate_actions_priority[i];
				//permet d'ajuster a l'intervalle [-1, 1] (avant [0, 1])
				priority = (priority*2)-1;
				typeChosen = candidate_actions[i];
				number_chosen = i;
				BMLFMLsend = storeFMLBML[i];
			/*}
			if (candidate_actions[i]!= "") 
			{*/
				#ifdef _DEBUG
					printf("\nCandidate Action: %s %f %f \n", candidate_actions[i].c_str(), ((candidate_actions_priority[i]*2)-1), candidate_actions_priority[i]);
				#endif
			}
		}

		#ifdef _DEBUG
			printf("\n\nACTION CHOSEN: %s %f \n\n", typeChosen.c_str(), priority);
		#endif

		for(int i=0; i<50; i++) candidate_actions[i]="";
		iter_action = 0;
		iter_react = 0; //remise a 0 de l'action par anticipation
		startD = clock();

		#ifdef _DEBUG
		if (satisNIU < 0 && varNIU < 0 && MoyNI < 0.75)
			printf("\nIncreasing Prob of BC %f\n", varNIU*2.5);
		#endif

		il=(int)(MAX_DELAY-(MAX_DELAY-MIN_DELAY)*NIU)/1000;
		if(il<0) il=(int)1300/1000;


		if(printoutput == 1)
		{
			output << typeChosen;
			output << "		";
			output << NIU;
			output << "		";
			output << priority;
			output << "\n";
		}

		//if (typeChosen == "ANTICIPATION" || typeChosen == "COGNITIVE") 
		//	typeChosen = "";

		if (typeChosen == "REACTIVE")
		{
			//printf("REACTIVE_CHOSEN");
			return "REACTIVE";
			//fmlSender->sendTextMessage(BMLFMLsend, meta.getTime(), message->getEventType());
		}
		if (typeChosen == "COGNITIVE")
		{
			//printf("REACTIVE_CHOSEN");
			return "COGNITIVE";
			//fmlSender->sendTextMessage(BMLFMLsend, meta.getTime(), message->getEventType());
		}

		if (typeChosen == "MIMICRY")
		{
			//printf("MIMICRY_CHOSEN");
			return "MIMICRY";
			//bmlSender->sendTextMessage(BMLFMLsend, meta.getTime(), message->getEventType());
		}
	
		//permet de laisser de temps de faire une action
		//si action pendant ce temps, elle sont stockees et utilisees au prochain choix

	}
		
	for(int i=0; i<50; i++) candidate_actions_zone[i].clear();
	for(int i=0; i<50; i++) candidate_actions[i].clear();
	for(int i=0; i<50; i++) candidate_actions_priority[i]=0.0;
	for(int i=0; i<50; i++) storeFMLBML[i].clear();
	for(int i=0; i<15; i++) behaviour_old[i].clear();

	/*if(waiting==1) // tant que l'action n'est pas finie
	{
		endD = clock();
		difD = endD - startD;
		difD = difD/CLOCKS_PER_SEC;
	}	

	if (difD >= 0) //fin de l'action
	{
		waiting=0;
		//printf("\n end of the backchannel %f\n", difD);
		difD = 0;*/
		it = 0;
		il = 0;
	//}
	return "";
}

void ActionSelection::Phase()
{
	//a definir
}

float ActionSelection::normalize(float value, float minI1, float maxI1, float minI2, float maxI2)
{
	return (value-minI1)*(maxI2-minI2)/(maxI1-minI1)+minI2;
}

std::string ActionSelection::getZone(){return zone;}
void ActionSelection::setZone(std::string zone1){zone = zone1;}

float ActionSelection::getPriorityRule(){return priorityRule;}
void ActionSelection::setPriorityRule(float priorityRule1){priorityRule = priorityRule1;}

float ActionSelection::getPriority(){return priority;}
void ActionSelection::setPriority(float priority1){priority = priority1;}

std::string ActionSelection::getBCtype(){return BCtype;}
void ActionSelection::setBCtype(std::string BCtype1){BCtype = BCtype1;}

float ActionSelection::getNIU(){return NIU;}
void ActionSelection::setNIU(float NIU1){NIU = NIU1;}

std::string ActionSelection::getContent(){return content;}
void ActionSelection::setContent(std::string content1){content = content1;}

std::string ActionSelection::getBMLFMLsend(){return BMLFMLsend;}
void ActionSelection::setBMLFMLsend(std::string BMLFMLsend1){BMLFMLsend = BMLFMLsend1;}

float ActionSelection::getStability(){return stability;}
void ActionSelection::setStability(float stable){stability = stable;}

float ActionSelection::getExtroversion(){return extroverted;}
void ActionSelection::setExtroversion(float extro){extroverted = extro;}

float ActionSelection::getAgentArousal(){return agentArousal;}
void ActionSelection::setAgentArousal(float agentArous){agentArousal = agentArous;}

float ActionSelection::getUserArousal(){return userArousal;}
void ActionSelection::setUserArousal(float userArous){userArousal = userArous;}

float ActionSelection::getAgentValence(){return agentValence;}
void ActionSelection::setAgentValence(float agentValen){agentValence = agentValen;}

float ActionSelection::getUserValence(){return userValence;}
void ActionSelection::setUserValence(float userValen){userValence = userValen;}

int ActionSelection::getNchosen(){return number_chosen;}
void ActionSelection::setNchosen(int num){number_chosen = num;}

std::string ActionSelection::getTypeChosen(){return typeChosen;}
void ActionSelection::setTypeChosen(std::string typeChosen1){typeChosen = typeChosen1;}

float ActionSelection::getMoyNIU(){return MoyNI;}
void ActionSelection::setMoyNIU(float moni){MoyNI = moni;}

const char* ActionSelection::getanticipNIU(){return anticipNIU;}
void ActionSelection::setanticipNIU(const char* antni){anticipNIU = antni;}

const char* ActionSelection::getinterPhase(){return interPhase;}
void ActionSelection::setinterPhase(const char* inph){interPhase = inph;}

const char* ActionSelection::getactchosen(){return actchosen;}
void ActionSelection::setactchosen(const char* actch){actchosen = actch;}
