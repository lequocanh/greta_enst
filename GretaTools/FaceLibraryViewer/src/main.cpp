//Copyright 1999-2005 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <stdio.h>
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/glut.H>
#include "DataContainer.h"
#include "BAPConverter.h"

#include "IniManager.h"
#include "FaceLibraryViewerWindow.h"

#include "XercesTool.h"

FaceLibraryViewerWindow *window;

IniManager inimanager;
std::string ini_filename;

DataContainer *datacontainer;

void idle()
{
	window->glutw->redraw();
}

#ifdef NDEBUG
#include <shellapi.h>
bool cvtLPW2stdstring(std::string& s, const LPWSTR pw, UINT codepage = CP_ACP){
	bool res = false;
	char* p = 0;
	int bsz;
	bsz = WideCharToMultiByte(codepage, 0, pw,-1,0,0,0,0);
	if (bsz > 0) {
		p = new char[bsz];
		int rc = WideCharToMultiByte(codepage,0,pw,-1,p,bsz,0,0);
		if (rc != 0) {
			p[bsz-1] = 0;
			s = p;
			res = true;
		}
	}
	delete [] p;
	return res;
}

int WINAPI WinMain(
     HINSTANCE hInstance,
     HINSTANCE hPrevInstance,
     LPSTR     lpszCmdParm,
     int       nCmdShow
)
{
	LPWSTR *szArglist;
	int nArgs;
	std::string ini_filename="";
	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
	if (nArgs==2)
		cvtLPW2stdstring(ini_filename,szArglist[1]);
#else
int main (int argc, char *argv[]){
	std::string ini_filename="";
	if (argc==2)
		ini_filename=argv[1];
#endif
	else
		ini_filename="greta.ini";

		//datacontainer needs some data..
	inimanager.ReadIniFile(ini_filename);
	inimanager.SetValueInt("PLAYER_ONLYFACE",1);
	inimanager.SetValueInt("PLAYER_TERRAIN",0);
	inimanager.SetValueInt("PLAYER_SHOWLISTENER",0);

	BAPConverter::init();

	XercesTool::startupXMLTools();

	datacontainer = new DataContainer();

	//load faces, gestures etc
	int code=datacontainer->initBMLEngine();	
	if (code==0) {
		printf("Problem : out \n");
		exit(1);
	}

	window = new FaceLibraryViewerWindow();
	
	window->show();
	
	window->glutw->show();
	glutIdleFunc(idle);
  
  return Fl::run();
}