//Copyright 1999-2009 Catherine Pelachaud - c.pelachaud@iut.univ-paris8.fr
//
//This file is part of Greta.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


// Psydule.cpp: implementation of the Psydule class.
//
//////////////////////////////////////////////////////////////////////

#include "Psydule.h"
#include <time.h>

//extern FileNames filenames;
//extern Uint32 wav_length;
//extern Uint8 *wav_buffer;
//extern int very_long;
//extern char* very_long_speech;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


Psydule::Psydule(std::string name, std::string host, int port) : CommunicationInterface()
{
	log = new Logger();
	this->name=name;
	this->port=port;
	printf("connection: %s, %s, %i ", name.c_str(), host.c_str(), port);
	this->plug1 = new CppAIRPlug( JString(name.c_str()), JString(host.c_str()), port);	
}

Psydule::~Psydule()
{
}

int Psydule::Register(std::string whiteboard, std::string datatype)
{
	std::list<std::string> types;
	types.push_back(datatype);
	return Register(whiteboard,types);
}

int Psydule::RawRegister(std::string rawregistrationstring)
{
	if (!plug1->init())
	{
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name.c_str());
		return 0;
	}
	if (plug1->isServerAvailable())
	{
		plug1->sendRegistration(JString(rawregistrationstring.c_str()));
		plug1->start();
	}
	else {
		printf("Could not connect to Psyclone for registering %s...\n\n", name.c_str());
		return 0;
	}
	printf("Psyclone: Connected to whiteboard\n");
	return 1;
}

int Psydule::Register(std::string whiteboard, std::list<std::string> datatypes)
{

	if (!plug1->init()) {
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name.c_str());
		//MessageBox(0,"Could not connect to Psyclone.\nPlease, run Psyclone before launching this Module.","Greta warning", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}

	JString xml = "\
	   <module name=\"";
	   xml+= JString(this->name.c_str());
	   xml+="\">\
	      <spec>\
	         <context name=\"Psyclone.System.Ready\">\
	            <phase id=\"";
	   xml+= JString(this->name.c_str());
	   xml+=".MainPhase\">\
					<triggers>\
					";
	   std::list<std::string>::iterator iter;	
					for(iter=datatypes.begin();iter!=datatypes.end();iter++)
					{
						xml+="<trigger from=\"";
						xml+=JString (whiteboard.c_str());
						xml=xml+"\" type=\"" + JString ((*iter).c_str()) + "\"/>\
							";
					}
					xml=xml+"</triggers>\
				</phase>\
	         </context>\
	      </spec>\
	   </module>\
	";

	if (plug1->isServerAvailable())
	{
		plug1->sendRegistration(xml);
		plug1->start();
	}
	else {
		printf("Could not connect to Psyclone for registering %s...\n\n", name.c_str());
		return 0;
	}
	printf("Psyclone: Connected to %s\n", whiteboard.c_str());
	return 1;
}

int Psydule::RegisterSender(std::map<std::string, std::string> nametype)
{
	return 1;
}

CommunicationMessage* Psydule::ReceiveMessage(int timeout)
{
	Message* triggerMessage;
	CommunicationMessage *cm1 = new CommunicationMessage();

	if (!plug1->isServerAvailable()) {
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name);
		return 0;
	}
	if ((triggerMessage = plug1->waitForNewMessage(timeout)) != NULL)
	{
		if(triggerMessage->object!=NULL)
		{
			DataSample *ds=(DataSample*)triggerMessage->object;
			cm1->setData(ds->data);
			cm1->setSize(ds->size);
		}

		int postedHour=atoi(triggerMessage->getPostedTime().printTimeMS().substring( 0, 2 ).c_str());
		int postedMin=atoi(triggerMessage->getPostedTime().printTimeMS().substring( 3, 5 ).c_str());
        int postedSec=atoi(triggerMessage->getPostedTime().printTimeMS().substring( 6, 8 ).c_str());
        int postedMs=atoi(triggerMessage->getPostedTime().printTimeMS().substring( 9, 12 ).c_str());
		cm1->setPostedTime((postedHour*3600+postedMin*60+postedSec)*1000+postedMs);

		int receivedHour=atoi(triggerMessage->getReceivedTime().printTimeMS().substring( 0, 2 ).c_str());
		int receivedMin=atoi(triggerMessage->getReceivedTime().printTimeMS().substring( 3, 5 ).c_str());
        int receivedSec=atoi(triggerMessage->getReceivedTime().printTimeMS().substring( 6, 8 ).c_str());
        int receivedMs=atoi(triggerMessage->getReceivedTime().printTimeMS().substring( 9, 12 ).c_str());
		cm1->setReceivedTime((receivedHour*3600+receivedMin*60+receivedSec)*1000+receivedMs);
		/* WARNING: the ReceivedTime seems not to be updated correctly every time !  */
		/* (actually it cannot be trusted when it concerns the 'Wakeup messages', it */
		/* has to be tested for other messages                                       */

	    cm1->setContent(triggerMessage->getContent().c_str()); 
		cm1->setType(triggerMessage->getType().c_str());
		return cm1;
	}
	else
	{
		return NULL;
	}
}


std::string Psydule::ReceiveString(int timeout)
{
	Message* triggerMessage;

	if (!plug1->isServerAvailable()) {
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name);
		return 0;
	}
	if ((triggerMessage = plug1->waitForNewMessage(timeout)) != NULL)
	{
		std::string content;
		content=triggerMessage->getContent().c_str();
		return content;
	}
	else
		return "";
}

long long Psydule::get_time()
{
	return -1;
}

std::string Psydule::getGretaName(std::string GretaName)
{
	return GretaName;
}


int Psydule::PostFile(std::string filename, std::string whiteboard, std::string datatype)
{
	std::string line;
	JString tosend="";

	std::ifstream inputfile(filename.c_str());
	if(inputfile.is_open())
	{
		while((inputfile.rdstate()&std::ios::eofbit)==0)
		{
			std::getline(inputfile,line,'\n');
			if(line!="")
			{
				tosend+=line.c_str();
				tosend+="\n";
			}
		}
	}
	else
	{
		printf("File %s not found\n", filename.c_str());
		return 0;
	}
	inputfile.close();

	if (!plug1->isServerAvailable()) {
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name);
		return 0;
	}

	plug1->postMessage(JString (whiteboard.c_str()),JString (datatype.c_str()),tosend,"","");

	return 1;
}

int Psydule::PostString(std::string tosend, std::string whiteboard, std::string datatype)
{
	if (!plug1->isServerAvailable()) {
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name);
		return 0;
	}
	
	plug1->postMessage(JString(whiteboard.c_str()),JString(datatype.c_str()),tosend.c_str(),"","");

	return 1;
}

int Psydule::PostSpeechFromChar(char* tosend, std::string whiteboard, std::string datatype, int *size)
{
	if ((*size)==0) return 0;
	if (!plug1->isServerAvailable()) 
	{
		printf("Could not connect to Psyclone for registering %s...\n\n", this->name);
		return 0;
	}
	
	//if there is no speech for some reason
	//if ( (*size)==0) return 0; 

	DataSample *ds;

	ds=new DataSample("Raw","");
	
	//TO DO : CHANGE IT
	ds->size=(*size);	
	ds->data=tosend;


	//ds->data=very_long_speech;
	//ds->size=very_long;		

	Message* msg = new Message(JString(this->name.c_str()) , JString(whiteboard.c_str()), JString(datatype.c_str()));

	msg->object=ds;

	plug1->postMessage(JString(whiteboard.c_str()),msg,"");
	
	return 1;
} 


int Psydule::SendFAPs(std::string fapfilename, std::string whiteboard, std::string datatype)
{
	this->PostFile(fapfilename,whiteboard,datatype);
	return 1;
}

int Psydule::SendBAPs(std::string bapfilename, std::string whiteboard, std::string datatype)
{
	PostFile(bapfilename,whiteboard,datatype);
	return 1;
}


int Psydule::SendBinaryFile(std::string binfilename, std::string whiteboard, std::string datatype)
{
	std::string line;
	JString tosend="";
	int size;
	
	FILE *inputfile;

	inputfile=fopen(binfilename.c_str(),"rb");

	DataSample *ds;

	int i;
	i=0;

	if(inputfile!=0)
	{

		fseek(inputfile, 0, SEEK_END);
		size = ftell(inputfile);
		fseek(inputfile, 0, SEEK_SET);
		
		ds=new DataSample("Raw","");
		ds->data=(char*)malloc(size+1);

		while(!feof(inputfile))
		{
			ds->data[i]=(char)fgetc(inputfile);
			i++;
		}
	}
	else
	{
		printf("Binary file %s not found\n", binfilename.c_str());
		return 0;
	}

	fclose(inputfile);
	
	ds->size=i;

	Message* msg = new Message(JString(this->name.c_str()), JString(whiteboard.c_str()), JString(datatype.c_str()));
	
	msg->object=ds;
	
	plug1->postMessage(JString(whiteboard.c_str()),msg,"");

	//delete ds->data;
	
	return 1;
}


int Psydule::WriteBinaryFile(std::string filename, CommunicationMessage *msg)
{
	//save data to a wave file
	FILE *f;
	char *audio;
	long size;
	f=fopen(filename.c_str(),"wb");
	if(f!=0)
	{
		audio=msg->getData();
		size=msg->getSize();
		if(audio!=NULL)
		{
			for(int i=0;i<size;i++)
				fputc(audio[i],f);
		}
		else
		{
			printf("ERROR: received empty wavefile!\n");
			return 0;
		}
		fclose(f);
	}
	else
		return 0;
	return 1;
}

