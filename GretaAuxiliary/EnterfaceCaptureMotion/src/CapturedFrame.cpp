#include "CapturedFrame.h"
#include "CapturedPoint.h"


CapturedFrame::CapturedFrame(){
	id=0;
	time=0.0;
}

CapturedFrame::CapturedFrame(int id,float time){
	this->id=id;
	this->time=time;
}

void CapturedFrame::addPoint(float x, float y, float z, int id, std::string name){
	CapturedPoint *temp =new CapturedPoint(x,y,z,name);
	points[id]=temp;
}
