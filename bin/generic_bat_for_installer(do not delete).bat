echo Psyclone
cd .\psyclone\
start psyclone spec="GretaEVOSpec.xml" verbose=0
cd ..
utils\WAIT.exe 2
echo starting Greta :
start Clock.exe
utils\WAIT.exe 1
start BehaviorPlanner.exe
utils\WAIT.exe 1
start BehaviorRealizer.exe
utils\WAIT.exe 1
start Player.exe
::utils\WAIT.exe 1
::start Psyclone_WoZ.exe
utils\WAIT.exe 1
cd .\BML_FML_LOADER
java -classpath ./;./AbsoluteLayout.jar;./JavaAIRPlug.jar;./interface.jar interfacepkg.NewJFrame

:: the console wait for the end of the java interface
:: when we close it, all applications are killed :

taskkill /f /im Player.exe
::taskkill /f /im Psyclone_WoZ.exe
taskkill /f /im BehaviorRealizer.exe
taskkill /f /im BehaviorPlanner.exe
taskkill /f /im ListenerIntentPlanner.exe
taskkill /f /im Clock.exe
taskkill /f /im psyclone.exe
