//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ConsNode.h: interface for the ConsNode class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>

//#include  "MultimodalSignal.h"

/**
* class :PADElement
*
*/


class PADElement
{

public:

	float pvalue;
	float avalue;
	float dvalue;

	float xd;
	float yd;
	float zd;

	std::string label;
	
	std::string name_for_fl;

	/**
	*  
	* 
	* destructor
	*/

	virtual ~PADElement();

	/**
	*  
	* 
	* constructor
	* 
	*/

	PADElement();

   /**
	*  
	* 
	* constructor
	* @param pvalue1 - P value
	* @param avalue1 - A value
	* @param dvalue1 - D value
	* @param xd1 - x - dimension
	* @param yd1 - y - dimension
	* @param zd1 - z - dimension
	* @param label1 - label in lexicon
	*
	*/
	
  PADElement(float pvalue1,float avalue1,
			   float dvalue1,
			   float xd1,
			   float yd1,
			   float zd1,
			   std::string label1
				);

  /**
	* 
	* constructor
	*
	* @param pvalue1 - P value
	* @param avalue1 - A value
	* @param dvalue1 - D value
	* @param xd1 - x - dimension
	* @param yd1 - y - dimension
	* @param zd1 - z - dimension
	* @param label1 - label in lexicon	
	*
	*/

  PADElement(float pvalue1,float avalue1,float dvalue1,std::string label1);

    /**
	* 
	* constructor
	*
	* @param pvalue1 - P value
	* @param avalue1 - A value
	* @param dvalue1 - D value
	* @param xd1 - x - dimension
	* @param yd1 - y - dimension
	* @param zd1 - z - dimension
	* @param label1 - label in lexicon	
	* @param label2 - label in fd	
	*
	*/

  PADElement(float pvalue1,float avalue1,float dvalue1,std::string label1,std::string label2);

};
