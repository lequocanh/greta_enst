
#pragma once


#include <string>


class CommunicationMessage {
public :
	CommunicationMessage();
	virtual ~CommunicationMessage();
	std::string CommunicationMessage::getContent();
	std::string CommunicationMessage::getType();
	char* CommunicationMessage::getData();
	long CommunicationMessage::getSize();

	void CommunicationMessage::setContent(std::string content);
	void CommunicationMessage::setType(std::string type);
	void CommunicationMessage::setData(char *data);
	void CommunicationMessage::setSize(long size);
	void CommunicationMessage::setPostedTime(long postedTime);
	void CommunicationMessage::setReceivedTime(long receivedTime);
	long CommunicationMessage::getPostedTime();
	long CommunicationMessage::getReceivedTime();

private :		
	std::string type;
	std::string content;

	char *data;
	long size;
	long postedTime; 
	long receivedTime;

};


