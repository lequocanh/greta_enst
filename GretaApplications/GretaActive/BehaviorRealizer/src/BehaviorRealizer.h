//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// BehaviorRealizer.h: interface for the BehaviorRealizer class.
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#ifndef SEMAINE_COMPONENTS_GRETA_BEHAVIORREALIZER_H
#define SEMAINE_COMPONENTS_GRETA_BEHAVIORREALIZER_H


#include <semaine/components/Component.h>
#include <semaine/cms/sender/Sender.h>
#include <semaine/cms/receiver/Receiver.h>
//#include <semaine/cms/receiver/BMLReceiver.h>
#include "FML-APML_AgentEngine.h"


#include "FapFrame.h"
#include "BapFrame.h"
#include "FaceEngine.h"
#include "IniManager.h"
#include "CentralClock.h"
#include "BML_AgentEngine.h"

#include <semaine/cms/message/SEMAINEXMLMessage.h>

namespace semaine {
namespace components {
namespace greta {

class BehaviorRealizer : public Component
{
public:

	BehaviorRealizer(const std::string & componentName, bool isInput, bool isOutput) throw (CMSException);
	virtual ~BehaviorRealizer();

	int SendIdle();
	void SendFAPsBAPs(int start_frame_time,int &lastfapframetime,int &lastbapframetime, std::string idbml, double priority, long long startat, long long lifetime);

	std::vector<MMSystemSpace::Signal> signals;

	FaceEngine *faceengine;
	GestureSpace::MotorPlanner *mplanner;
	FAPFrameVector *fapframes;
	
	//for infinite movements we need another copy of the vector of frames
	FAPFrameVector *fapframeswithoutnoise;
	BAPFrameVector *bapframes;
	CentralClock pc;

	//std::vector<FAPFrame> *all_fapframes;
	std::vector<BAPFrame> *all_bapframes;

	BML_AgentEngine bmlengine;

	float FPS;

	bool NoEndFaceSignals;
	int last_fap_frame_time;
	int last_bap_frame_time;

	
	bool send_sound;
	std::string sound_file_name;
	void  findCurrentFrame(FAPFrameVector *frames);
	FAPFrame last_fap_frame;
	BAPFrame *last_bap_frame;

protected:
	virtual void react(SEMAINEMessage * m) throw (CMSException);
	virtual void act() throw (CMSException);
	void customStartIO() throw(std::exception);

private:
	Receiver * bmlReceiver;
	Receiver * bmlBCReceiver;
	Sender * fapSender;
	Sender * bapSender;
	Sender * commandSender;

	int previous_time;
	long long lasttime;
	bool initialization;
	long itercommand;
	long iterfap;
	long iterbap;
	long iterfapidle;
	long itercommandidle;
	long iterbapidle;
	long iteridle;
	long bmlcreationID;
	std::string bmlID;
	int idledone;
	time_t startD;
	time_t endD;
	float difD;
	float randidle;
	double priority;
	long long startat;
	long long lifetime;
	bool hasAudio;

	int orphan_cont;

};

} // namespace greta
} // namespace components
} // namespace semaine


#endif