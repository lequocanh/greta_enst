package org.aitools.programd.configurations;

import com.cmlabs.air.JavaAIRPlug;
import com.cmlabs.air.Message;
import com.cmlabs.air.Time;
import org.aitools.programd.Core;

public class ConnexionProgramD {

	private JavaAIRPlug plug;
	private String ip_number;
	private Core _core;
	private Message _incomingMessage;
	
	public ConnexionProgramD(Core core){
		this.ip_number="127.0.0.1";
		this._core=core;
		
	}
	public void run() {
        this.connect(ip_number,10000);                
        
    }
	
	// connect and wait for message
	public void connect(String host, int port) {
        
        plug = new JavaAIRPlug("SimpleTextSender", host, port);
        
        if (!plug.init()) {
            System.out.println("Could not connect to the Server on " + host + " on port " + port + ". Exit.");
            System.exit(1);
        }
        
        
        if (!plug.openTwoWayConnectionTo("Greta.Whiteboard")) {
            System.out.println("Could not open callback connection to Greta.Whiteboard. Exit.");
            System.exit(1);
        }
        
        String xml = "<module name=\"SimpleTextSender\" allowselftriggering=\"yes\">"+
                "              <spec>"+
                "                 <context name=\"Psyclone.System.Ready\">"+
                 "   <phase id=\"Phase One\">" +
                "                       <post to=\"Greta.Whiteboard\" type=\"Greta.Data.TEXT.OUT\" />" +
                "						<trigger from=\"Greta.Whiteboard\" type=\"Greta.Data.TEXT.IN\" />" +
                "              </spec>"+
                "           </module>";
        
        
        if (!plug.sendRegistration(xml)) {
            System.out.println("Could not register for messages. Exit.");
            System.exit(1);
        } else {
            System.out.println("Ready to send messages...");
        }
        
        
        
        // receive message
         Thread th=new Thread(){
        	public void run(){
        
        		while (true) {
        			if ((_incomingMessage = plug.waitForNewMessage(50)) != null) {
        				//System.out.println("--PROGRAMD: "+_incomingMessage.content+ " type of message " + _incomingMessage.type + " from " +  _incomingMessage.from);
        				
        				
        				//send response of the agent
        				String responseAgent=_core.getResponseAgent(_incomingMessage.content);
        				
        				process(responseAgent);
        			}
        		}
        	}
        };
        th.start();
	}
	
	//send only BML message
	public void process(String send) {
        
       
            Time now = new Time();
            
            Message msg_ans = new Message("SimpleTextSender","Greta.Whiteboard", "Greta.Data.TEXT.OUT");
            
            try{
                
                msg_ans.content = send;
                                
                boolean posted = plug.postOutputMessage(msg_ans);
                                
                if (!posted) {
                    System.out.println("I could not post message...");                
                }  else {
                    now = new Time();
                    System.out.println("Message posted at:" + now.printTime());     
                }
            }
            
            catch (Exception myexception){
                
                System.out.println("I could not generate answer..."+ myexception);                
        }
    }// end of process
    
}