//Copyright 1999-2009 Catherine Pelachaud - catherine.pelachaud@telecom-paristech.fr
//
//This file is part of Greta 2009.
//
//Greta is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//Greta is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Greta; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

// ListenerIntentPlanner.h: interface for the ListenerIntentPlanner class.
// Created by Elisabetta Bevacqua 30.06.09
//////////////////////////////////////////////////////////////////////

#ifndef SEMAINE_COMPONENTS_GRETA_LISTENERINTENTPLANNER_H
#define SEMAINE_COMPONENTS_GRETA_LISTENERINTENTPLANNER_H

#include "DataContainer.h"
#include "ReactiveBehaviourTrigger.h"
#include "BehaviourSelector.h"
#include "listener\ListenerData.h"
#include "AgentState.h"

#include <semaine/components/Component.h>
#include <semaine/cms/message/SEMAINEXMLMessage.h>
#include <semaine/cms/message/SEMAINEMessage.h>
#include <semaine/cms/sender/Sender.h>
#include <semaine/cms/sender/BMLSender.h>
#include <semaine/cms/sender/FMLSender.h>
#include <semaine/cms/receiver/StateReceiver.h>

using namespace semaine::components;


static std::string USER = "user";

class ListenerIntentPlanner : public Component
{
public:
	/** 
    * Class contructor.
    * 
    */
	ListenerIntentPlanner();
	ListenerIntentPlanner(const std::string & componentName, bool isInput, bool isOutput) throw (CMSException);
	/** 
    * Class default destructor.
    * 
    */
	~ListenerIntentPlanner();


	void SendBehaviour(std::string bmlfml, std::string messagetype);
	
	BMLSender * getBMLSender() {return bmlSender;};
	FMLSender * getFMLSender() {return fmlSender;};
	long long sendTime() {return meta.getTime();};

	/** 
    * Selector, it selects the backchannel that the agent must performs
    * 
    */
	BehaviourSelector *behaviourselector;
	/** 
    * Contains info about the agent's state
    * 
    */
	AgentState *agentstate;

protected:
	virtual void react(SEMAINEMessage * m) throw (CMSException);
	virtual void act() throw (CMSException);
	void customStartIO() throw(std::exception);


private:

	/** 
    * Contains info about the listener: analysis rules, backchannel rules, internal state
    * 
    */
	ListenerData *listenerdata;
	/** 
    * Trigger for mimicry and reactive backchannel
    * 
    */
	ReactiveBehaviourTrigger *reactivebehaviourtrigger;

	StateReceiver * contextReceiver;
	StateReceiver * USReceiver;
	Receiver * dialogstateReceiver;
	StateReceiver * agentStateReceiver;
	FMLSender * fmlSender;
	BMLSender * bmlSender;
	time_t start_speaking;
	time_t stop_speaking;

	bool userpresent;

};

#endif
