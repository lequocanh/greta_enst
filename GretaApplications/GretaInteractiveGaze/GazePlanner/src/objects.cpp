// Shared-Attention system
// Christopher Peters

#include "objects.h"

Named3DObject::Named3DObject(std::string name, int type, float posx, float posy, float posz, float r, float g, float b)
{
		static int IDgen = 0;		//unique identifier
		this->objectID = IDgen;
		IDgen ++;

		this->name = new std::string;
		this->name->assign(name);
		this->type = type;
		this->position[0] = posx;
		this->position[1] = posy;
		this->position[2] = posz;
		this->colour[0] = r;
		this->colour[1] = g;
		this->colour[2] = b;

		this->height = 0.1;
		this->width = 0.1;
}


Named3DObject::Named3DObject(std::string name, int type, float posx, float posy, float posz, float height, float width)
{
		static int IDgen = 0;		//unique identifier
		this->objectID = IDgen;
		IDgen ++;

		this->name = new std::string;
		this->name->assign(name);
		this->type = type;
		this->position[0] = posx;
		this->position[1] = posy;
		this->position[2] = posz;
		this->colour[0] = 0;
		this->colour[1] = 0;
		this->colour[2] = 0;
		
		this->height = height;
		this->width = width;
}


Named3DObject::~Named3DObject()
{
	delete this->name;
}

void Named3DObject::drawBox(void)
{
	//float height = 0.11;
	//float width = 0.08;

	glColor3f(this->colour[0],this->colour[1],this->colour[2]);
	glBegin(GL_POLYGON);
		glVertex3f(-width,height,0.0);
		glVertex3f(-width,-height,0.0);			
		glVertex3f(width,-height,0.0);
		glVertex3f(width,height,0.0);
	glEnd();
}

void Named3DObject::drawTable(void)
{
		glRotatef(-45.0,1.0,0.0,0.0);
		glColor3f(this->colour[0],this->colour[1],this->colour[2]);
		glBegin(GL_POLYGON);
			glVertex3f(-1.0,0.0,0.0);
			glVertex3f(-1.0,0.0,-1.0);			
			glVertex3f(1.0,0.0,-1.0);
			glVertex3f(1.0,0.0,0.0);
		glEnd();
}

void Named3DObject::drawBackground(void)
{
		float size = 100.0;
		glColor3f(this->colour[0],this->colour[1],this->colour[2]);
		glBegin(GL_POLYGON);
			glVertex3f(-size,size,0.0);
			glVertex3f(-size,-size,0.0);			
			glVertex3f(size,-size,0.0);
			glVertex3f(size,size,0.0);
		glEnd();
}

void Named3DObject::draw(void)
{
	glLoadName(this->objectID);
	
	glPushMatrix();
		
	glTranslatef(this->position[0], this->position[1], this->position[2]);
	
	switch(this->type)
		{
		case OBJECT_BOX:
			this->drawBox();
			break;
		case OBJECT_BACKGROUND:
			this->drawBackground();
			break;
		case OBJECT_TABLE:
			this->drawTable();
			break;
		case OBJECT_GRETA: 
			
			break;
	}
	glPopMatrix();
}