#pragma once

#include <string>
#include <vector>
#include <map>

#include "CapturedPoint.h"

class CapturedFrame {

public:

	//std::vector<CapturedPoint*> *points;

	std::map<int,CapturedPoint*> points;

	int id;
	float time;

	CapturedFrame();
	CapturedFrame(int id, float time);
	virtual ~CapturedFrame(){}

	void addPoint(float x, float y, float z, int id, std::string name);
	
	std::map<int,CapturedPoint*> getPoints(){
		return points;
	}

private:

}

;

