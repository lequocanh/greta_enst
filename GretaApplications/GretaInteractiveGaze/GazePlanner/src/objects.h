// Shared-Attention system
// Christopher Peters

#ifndef _OBJECTS_H
#define _OBJECTS_H

#include <stdio.h>
#include <string>

#include <FL/Gl.h>
#include <Gl/Glu.h>
#include "sharedattention.h"

class VAO;
class Named3DObject
{
	public:
		Named3DObject(std::string name, int type, float posx, float posy, float posz, float r, float g, float b);
		Named3DObject(std::string name, int type, float posx, float posy, float posz, float height, float width);
		~Named3DObject();

		int getID(void){return this->objectID;}
		std::string *getName(void){return this->name;}

		//void update(void);

		void drawBox(void);
		void drawTable(void);
		void drawBackground(void);
		void draw(void);

		int objectID;		//unique identifier
		std::string *name;
		int type;			
		float position[3];
		float colour[3];
		float width;
		float height;

		VAO *vao;	//the corresponding VAO for this object
};

#endif